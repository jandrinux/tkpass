<?php

/**
* BoletaGen
*/
class BoletaGen
{
	public $token;
	public $url;	
	var $numero;

	function __construct()
	{
		include("config.php");
        $this->url = URL;
        $this->token = TOKEN;
	}

	function generar($afecto, $lineas,$descuento) {
	
                    
		if (!DESARROLLO)
         {
            $client = new SoapClient($this->url, array('encoding'=>'ISO-8859-1'));
	       	$envio  = array(
					'CodigoToken' => $this->token,
					'lineas' =>$lineas,
					'afecto' => $afecto,
					'descuento'=>$descuento);

			$data = $client->BoletaGen($envio);
			return $data->BoletaGenResult;
		}

		$a = new paso();
		$a->Message = "DESARROLLO";
		$a->numero= 0;
		return $a;
		
	}

}

class NotaCreditoGen
{
	public $token;
	public $url;	

	function __construct()
	{
		include("config.php");
        $this->url = URL;
        $this->token = TOKEN;
	}

	function generar($afecto, $lineas,$descuento,$referencia,$folio) {
		$client = new SoapClient($this->url, array('encoding'=>'ISO-8859-1'));
		$envio  = array(
					'CodigoToken' => $this->token,
					'lineas' =>$lineas,
					'afecto' => $afecto,
					'descuento'=>$descuento,'referencia'=>$referencia,'folio'=>$folio);

		if (!DESARROLLO) {
		$data = $client->BoletaGen($envio);

		return $data->BoletaGenResult;
		}
	return "0";
		
	}

}
/**
* Clase Linea de Articulos
*/
class item extends stdClass
{
	public $Linea;
	public $codigoMannager;
	public $Descripcion;
	public $Cantidad;
	public $PrecioUnitario;
	public $Descuento;
}

class paso extends stdClass
{
	public $Message;
	public $numero;
}

?>