<?php
session_start();

include('../../../Clases/MPDF/mpdf.php');
require ("../../../Clases/ClaseConexion.inc.php");
include( "../../../Clases/class.TemplatePower.inc.php"); 
require ("../../../Clases/ClaseUtil.inc.php");
require ("../../../Clases/class.smtp.php");
require ("../../../Clases/class.phpmailer.php");

function comprobar_email($email){ 
     $mail_correcto = 0; 
     //compruebo unas cosas primeras 
     if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){ 
        if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) { 
           //miro si tiene caracter . 
           if (substr_count($email,".")>= 1){ 
              //obtengo la terminacion del dominio 
              $term_dom = substr(strrchr ($email, '.'),1); 
              //compruebo que la terminación del dominio sea correcta 
              if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){ 
                 //compruebo que lo de antes del dominio sea correcto 
                 $antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
                 $caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
                 if ($caracter_ult != "@" && $caracter_ult != "."){ 
                    $mail_correcto = 1; 
                 } 
              } 
           } 
        } 
     } 
     if ($mail_correcto) 
        return 1; 
     else 
        return 0; 
 }
?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="http://www.turistik.cl/wp-content/themes/delegate/transtyle.css" media="screen" />

<script>
function Volver(volver)
{
//alert(volver);
document.location.href = volver;
}
</script>

</head>
<body>
<table align="center" bgcolor="#FFFFFF">
<tr>
   <td>
	<center><img src="../../../../turistik-3pisos_.png" border="0"></center>
   </td>
</tr>
<tr>	
	<td>
		<br>
		Estimado Cliente, el proceso de compra se efectuo exitosamente.<br> El voucher de compra fue enviado a su correo electronico <br>
		<center><b><?php echo htmlentities($_GET['email']);?></b> en formato PDF. 
		<br><br>
		
		Gracias por Preferir Turistik
		</center>
	</td>
</tr>
<tr>
<td align="center"> 
<br><br><br>
<center>
<input onClick="Volver('<? echo '../form_'.$_GET['excursion'].'.php'?>')" id="volverUp" type="button" name="Submit" value="Volver" />
</center>
</td>
</tr>
</body>
</html>

<?php
if(!empty($_GET['voucher'])){

$miConexion = new ClaseConexion;
$_Util=new Util;
$miConexion->Conectar();
	$queryL=$miConexion->EjecutaSP("Consulta_Voucher","'".$_GET['voucher']."'");
	while ($rowL = mysql_fetch_assoc($queryL))
	{
		if($rowL['OTRO_LUGAR_RE']=='0')
		{
		$lugares=' '.$rowL['DESCRIPCION_HL'];
		}
		if($rowL['LUGAR_RE']=='0')
		{
		$lugares=' '.$rowL['OTRO_LUGAR_RE'];
		}
		$observacion_re=$rowL['OBSERVACION_RE'];
		$cod_vendedor=$rowL['CODIGO_VENDEDOR_RE'];
		
		$cantidad = 1;		
		$precio=$rowL['PRECIO_RE'];	
		$monto=$precio*$cantidad;
		$monto=number_format($monto, 0, ",", ".");
		
	   $CODIGO_RE = $rowL['CODIGO_RE'];
	   $CODIGO_EO = $rowL['CODIGO_EO'];  
	   $TELEFONO_RE = $rowL['TELEFONO_RE'];
	   $PASAJERO_RE = $rowL['PASAJERO_RE'];
	   $TIPO_RE = $rowL['TIPO_RE'];
	   $FECHA_RE = $rowL['FECHA_RE'];
	   $FECHA_REALIZACION_RE = $rowL['FECHA_REALIZACION_RE'];
	   $ESTADO_RESERVA_ER = $rowL['ESTADO_RESERVA_ER'];
	   $COD_USUA = $rowL['COD_USUA'];
	   $COD_CIERRE = $rowL['COD_CIERRE'];
	   $LOGIN_USUA_CIERRE = $rowL['LOGIN_USUA_CIERRE'];
	   $CODIGO_DIST = $rowL['CODIGO_DIST'];
	   $PASAPORTE_RE = $rowL['PASAPORTE_RE'];
	   $VOUCHER_RE = $rowL['VOUCHER_RE'];
	   $HOTEL_RE = $rowL['HOTEL_RE'];
	   $HABITACION_RE = $rowL['HABITACION_RE'];
	   $NACIONALIDAD_RE = $rowL['NACIONALIDAD_RE'];
	   $BOLETA_RE = $rowL['BOLETA_RE'];
	   $FECHA_BOLETA_RE = $rowL['FECHA_BOLETA_RE'];
	   $USUARIO_BOLETA_RE = $rowL['USUARIO_BOLETA_RE'];
	  //$SUCURSAL_VENTA = $rowL['SUCURSAL_VENTA'];
	  //$TARJETA_TTS = $rowL['TARJETA_TTS'];	
	  //$forma_pago = $rowL['FOP'];		
	}
mysql_free_result($queryL); 
mysql_close();	

/*	switch($forma_pago){
	case "C":
	$forma_pago = "Tarjeta de Credito";
	break;
	
	case "D":
	$forma_pago = "Tarjeta de Debito";
	break;
	
	case "E":
	$forma_pago = "Efectivo";
	break;
	}*/

      function justificarTexto($texto, $car) {
      if(strlen($texto) > $car) {  // comprobamos que el texto tiene mas de 50 caracteres
        $texto = wordwrap($texto,$car,"<br />",1);  } // inserta el salto a los 50 caracteres
      else $texto=$texto; // si no es mas largo de 50 caracteres, se deja igual
      return $texto;      
     }

	if(1==1)
	{
	$subtitulo='VOUCHER DE SERVICIO PAGADO';
	}
	else
	{
	$subtitulo='VOUCHER DE SERVICIO POR PAGAR';	
	}
	$subtitulos=$subtitulo.'<br>'.$_GET['voucher'];
	
	$miConexion = new ClaseConexion;
    $_Util=new Util;
	$miConexion= new ClaseConexion;
	$miConexion->Conectar();
	$queryT1=$miConexion->EjecutaSP("Consulta_Excursion_Codigo","'".$CODIGO_EO."'");
	while ($rowT1 = mysql_fetch_assoc($queryT1))
	{
	$resumen=$rowT1['RESUMEN_EO'];
	$nombre_excursion=$rowT1['NOMBRE_EO'];
	}
	mysql_free_result($queryT1); 
    mysql_close();
	
	$miConexion->Conectar();
	$queryCorreo=$miConexion->EjecutaSP("Consulta_Correo_Excursion","'".$_GET['excursion']."'");
	while ($rowCorreo = mysql_fetch_assoc($queryCorreo))
	{
		$Direccion_correo=$rowCorreo['CORREO_EO'];
	}
	mysql_free_result($queryCorreo); 
    mysql_close();
		
$html='
<style type="text/css">
.celda_bordes {
	font-family: Arial;
	font-size: 2px;
	line-height: 2px;
	color: #000000;
	background-color: #FFFFFF;
	background-repeat: repeat-x;
	border: 1px solid #c3c3c3;
}
.celda_peque&ntilde;a {
	font-family: Arial;
	font-size: 10px;
	line-height: 16px;
	color: #000000;
	background-color: #FFFFFF;
	background-repeat: repeat-x;
}
</style>
<body>
<table align="center" bgcolor="#c3c3c3" width="98%"><tr><td class="celda_bordes">
<table align="center" bgcolor="#FFFFFF">
<tr><td align="center"><img src="../../../../turistik/Imagenes/turistik-3pisos_.png" border="0"></td></tr>
<tr><td align="center" class="celda_peque&ntilde;a"><b>'.$VOUCHER_RE.'</b></td></tr>
<tr><td align="center">
</td></tr>
<tr><td>
<table>
<tr>
<td width="20" class="celda_peque&ntilde;a"></td><td class="celda_peque&ntilde;a"><br><br><b>Estimado(a) '.$PASAJERO_RE.'</b><br></td><td width="20"></td>
</tr>
<tr>
	<td width="20" class="celda_peque&ntilde;a"></td>
	<td class="celda_peque&ntilde;a">
	Agradecemos su compra de:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>'.$cantidad. 
	' TICKET(s) '.htmlentities($nombre_excursion).'</b><br>El servicio ser&aacute; utilizado:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>'.$FECHA_RE.'</b>
	<br />Lugar de Salida / Place of Departure:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>'.$lugares.'</b><br>Tel&eacute;fono de contacto Cliente:&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b><u>'.$TELEFONO_RE.'</u></b><br/>
	Monto Total de la Compra:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>$'.$precio.' PESOS CHILENOS.</b>
	<br>	
	</td>
	<td width="20"></td>	
	</tr>
	<tr><td colspan="2"></td></tr>		
	</table>
<table>
	<tr><td width="20" align="center" ></td>
	<td align="justify" class="celda_peque&ntilde;a"><br><br>'.htmlentities($resumen).'<br>
	<br><strong><br>INFORMACION RELEVANTE: Para hacer uso del servicio deber&aacute;s llevar una copia de esta confirmaci&oacute;n / Esta compra no es reembolsable / Cambios son permitidos hasta 48 horas antes de la hora del servicio / Despu&eacute;s de realizar tu tour agradecer&iacute;amos tus comentarios a trav&eacute;s de www.turistik.cl/comments / Si tienes alguna duda o inquietud, por favor ll&aacute;manos al (56-2) 220-1000.</strong>
	<br><strong><br><br>IMPORTANT INFORMATION: To use our services you must carry a copy of this confirmation / This purchase is non refundable / Changes are allowed up to 48 hours prior to the time of the service / After taking the tour we would appreciate your comments through www.turistik.cl/comments / If you have any questions or doubts, please call us at (56-2) 220-1000.
</strong>
</td><td width="20"></td></tr>
	</table></td></tr>
<tr><td></td></tr>
</td></tr></table>
</td>
</tr>
</table>
</body>';

$mpdf=new mPDF('c');
$mpdf->SetDisplayMode('fullpage');
$stylesheet = file_get_contents('mpdfstyleA4.css');
$mpdf->WriteHTML($stylesheet,1);
$stylesheet = file_get_contents('mpdfstylePaged.css');
$mpdf->WriteHTML($html);

$dir = "../../../../archivos_PDF";
$nombre_archivo= $_GET['voucher'];

if (!file_exists($dir)){
mkdir($dir,0777,true);
}
$documento = ''.$nombre_archivo.'.pdf';
$ruta = $dir.'/'.$documento;
$mpdf->Output($ruta,'F');
$_SESSION["RUTA"] = $ruta;

//$mpdf->Output();

include("envia_email_admin.php");

$res = comprobar_email($_GET['email']);
if($res == 1){
include("envia_email_usu.php");
}
unlink($ruta);
}
?>
