<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/style_login.css" />
<script src="../../js/jquery.js" type="text/javascript"></script>
<script src="../../js/ajax.js" type="text/javascript"></script>

<title>Turistik -Login</title>
<script language="javascript">

<!-- LOGIN -->

$(document).ready(function()
{
	$("#login_form").submit(function()
	{
		//remove all the class add the messagebox classes and start fading
		$("#msgbox").removeClass().addClass('messagebox').text('Validando....').fadeIn(1000);
		//check the username exists or not from ajax
		$.post("valida_login.php",{ rut:$('#rut').val(),cantidad:$('#cantidad').val(),pass:$('#pass').val(),suc:$('#suc').val(),rand:Math.random() } ,function(data)
        {
		  if(data=='yes') //if correct login detail
		  {
		  	$("#msgbox").fadeTo(200,0.1,function()  //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('Loging correcto, transfiriendo.....').addClass('messageboxok').fadeTo(900,1,
              function()
			  { 
			    // var cant = document.getElementById("cantidad").value;
			     //redirect to secure page
				 window.location.href='Excursiones.php';
			  });
			  
			});
		  }
		  else 
		  {
		  	$("#msgbox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
        			if (data=='no-a')
                    {
                          $(this).html('Acceso Denegado a Turistik Pass...').addClass('messageboxerror').fadeTo(900,1);
                    }
                    else
                    {
                         $(this).html('Rut o password incorrecto...').addClass('messageboxerror').fadeTo(900,1);
                        
                    }
			});		
          }
				
        });
 		return false; //not to post the  form physically
	});
	//now call the ajax also focus move from 
	$("#password").blur(function()
	{
		$("#login_form").trigger('submit');
	});
});

</script>
<script type="text/javascript">
function setFocus(){
     document.getElementById("rut").focus();
}

</script>


<!-- FONDO NUEVO -->

<style type="text/css">
html, body {margin:0; padding:0; width:100%; height:100%; overflow:hidden; }
body {font-family: 'Arial', serif;}
#background{position:absolute; z-index:1; width:100%; height:100%;}
#fixed {position:absolute; top:25px; left:10px; width:160px;  color:#333; padding:10px;}
#scroller {position:absolute; width:100%; height:100%; top:0; left:0; overflow:auto; z-index:2;} 
#content {padding:20px 20px 20px 0px;}
p {font-size:16px; text-align:justify; line-height:25px; text-shadow: 0 1px 2px rgba(0,0,0,0.5), 0px -1px 2px rgba(255,255,255,0.8);}
.Col {width:25%; float:left; padding:2%; min-width:300px;}
</style>
<body>
<div>
	<img id="background" src="img/bg.png" alt="" title=""></div>
<div id="scroller">
<div id="content">

<!-- FIN FONDO NUEVO -->



<!-- FONDO ANTIGUO -->
<!-- background-image: url(img/bg.jpg);  -->

<style type="text/css">
body {
    font-family: Arial;
  
}
.messagebox{
	
    top:415px ;
	width:10px;
    margin-left:0px;
    position:absolute;
    float:left ;
	padding:3px;
    
}
.messageboxok{

	width:auto;
    float:left ;
	padding:3px;
	font-weight:bold;
	color:#236E18;
	
}
.messageboxerror{

	width:auto;
    float:left ;
	padding:3px;
    font-weight:bold;
	color:#CC0000;
}
</style>
</head>

<body onload="setFocus()">

<div id="stylized" class="login">
<!--id="login_form"-->
  <form  method="post" id="login_form" action="" >
  <div class="logo"><img src="img/logo_turistik.png" width="50" height="60" /></div>
  
   <h1>Bienvenido a Turistik Pass </h1>

    <p></p>

	<label title="Ejemplo 16874256">Rut: *</label><p1>(sin digito verificador)</p1> 

    <input type="text" name="rut" id="rut" placeholder="Ej. 16874256"  onkeypress="return isNumberKey(event)"  />  

    <label for="password">Password: *</label>
    <input type="password" name="pass" id="pass" />
    
    <label for="cantidad">Cantidad Pax: *</label>
    <input type="text" maxlength="2" size="5" name="cantidad" id="cantidad" onkeypress="return isNumberKey(event)" />
    <input type="hidden" name="suc" id="suc" value="45c48cce2e2d7fbdea1afc51c7c6ad26" />
    
    <span id="msgbox" style="display:none"></span>  
     <input name="login" class="button2" type="submit" value="Login" >
    <!--<input align="right" name="comprar" type="submit" class="BotonTurist_Celest" value="Login" />-->
    
    
    
   <div class="spacer"></div>

  </form>

 </div>

</body>

</html>