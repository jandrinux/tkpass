<?php
    session_start();
    include("menus.php");
    $_Util=new Util;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Promociones</title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>

<script>
function modificar_estado_promocion(codigo)
{
    miPopup = window.open("../PHP/promociones/modificar_estado_promocion.php?codigo="+codigo+"","miwin","top=0,left=0,width=0,height=0,scrollbars=no,titlebar=no,location=no");
}

function seleccionar_excursiones(codigo)
{
    miPopup = window.open("../PHP/promociones/combinar_excursiones.php?codigo="+codigo+"","miwin","top=0,left=500,width=500,height=800,scrollbars=yes,titlebar=no,location=no")
}

function seleccionar_clientes(codigo)
{
    miPopup = window.open("../PHP/promociones/combinar_clientes.php?codigo="+codigo+"","miwin","top=0,left=500,width=500,height=500,scrollbars=yes,titlebar=no,location=no")
}
function seleccionar_lugar(codigo)
{
    miPopup = window.open("../PHP/promociones/combinar_lugar_rec.php?codigo="+codigo+"","miwin","top=0,left=500,width=500,height=800,scrollbars=yes,titlebar=no,location=no")
}
function seleccionar_nacionalidad(codigo)
{
    miPopup = window.open("../PHP/promociones/combinar_nacionalidad.php?codigo="+codigo+"","miwin","top=0,left=500,width=500,height=800,scrollbars=yes,titlebar=no,location=no")
}

function seleccionar_sucursal(codigo)
{
    miPopup = window.open("../PHP/promociones/combinar_sucursal.php?codigo="+codigo+"","miwin","top=0,left=500,width=500,height=800,scrollbars=yes,titlebar=no,location=no")
}


function seleccionar_combo(codigo)
{
    miPopup = window.open("../PHP/promociones/seleccion_combo.php?codigo="+codigo+"","miwin","top=0,left=500,width=350,height=550,scrollbars=yes,titlebar=no,location=no")

}

</script>
</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">
  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr height="50"><td><br></td></tr>
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/promociones.png" width="120" height="70" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Promociones y Combos</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    
  </table>
  
    


<br /><br />


<form name="form" method="post" onSubmit="agregar_promociones(); return false" action="">
<table width="400" border="0" cellspacing="0" style="PADDING-TOP: 3px;TEXT-ALIGN: leftPADDING-BOTTOM: 3px;FONT-FAMILY: Arial;FONT-SIZE: 8pt;">
  <tr>
    <td width="150" class="celdalazul_peque">Nombre : (*)</td>
    <td class="celdalCELEST"><input type="text" name="nombre" id="nombre" class="celdalCELEST"  /></td>
  </tr>
  <tr>
    <td width="150" class="celdalazul_peque">Descripcion : </td>
    <td class="celdalCELEST"><textarea name="descripcion" cols="23" id="descripcion" ></textarea></td>
  </tr>
  <tr>
  <tr>
    <td width="150" class="celdalazul_peque">Cant. Excurciones : (*)</td>
    <td class="celdalCELEST">
        <input type="text" name="cant" id="cant" class="seleccionTurist_90" onkeypress="return validarP3(event);" />
     </td>    
  </tr>
  <tr>
    <td width="150" class="celdalazul_peque">Dias Expiraci&oacute;n : (*)</td>
    <td class="celdalCELEST">
        <input type="text" name="dias" id="dias" class="seleccionTurist_90" onkeypress="return validarP3(event);"/>
     </td>    
  </tr>

    <td colspan="2" class="celdalCELEST" > 
	<center>        
      <input type="submit" class="BotonTurist_Celest" value="Aceptar" />
    </td> 
  </tr> 


</table>
</form>

<div id="resultado_listar"  style="margin:0px 0px 10px 0px; float:left;"><? include('promociones/CONSULTA_promociones.php'); ?></div>


</body>
</html>


