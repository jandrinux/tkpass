<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:16px;
	top:159px;
	width:823px;
	height:231px;
	z-index:13;
}
#Layer3 {
	position:absolute;
	left:18px;
	top:60px;
	width:314px;
	height:62px;
	z-index:14;
}
-->
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Ingreso Usuario</title>
<style type="text/css">
<!--
#Layer2 {	position:absolute;
	left:831px;
	top:53px;
	width:132px;
	height:116px;
	z-index:1;
}
-->
</style>
<script type="text/JavaScript">

function Popup()
{
 miPopup = window.open("../PHP/Ingreso_Ubicacion.php","miwin","top=200,left=350,width=550,height=200,scrollbars=yes,titlebar=no,location=no");
 miPopup.focus();	
}
function pres_num()
	{
		if((event.keyCode<48) || (event.keyCode>57))
			{
				event.returnValue = false;
			}		
	}
	
function validar()
	{
    	// Primer paso: Obtener el rut que ingreso el usuario
   		 var rutCompleto = document.form1.rutusuario.value;
		// Eliminamos los caracteres raros, espacios, puntos, guiones.
    	// Pasamos a minusculas, y separamos el rut y el digito verificador
    	rutCompl = rutCompleto.toLowerCase();
   		var dv=document.form1.dvusuario.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
   		 valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   		 valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
        		alert("El Rut no es Valido.Ingreselo Nuevamente");
        		document.form1.rutusuario.value="";
				document.form1.dvusuario.value="";
				document.form1.rutusuario.focus();
   			 }
	}

function verificarDV(rut, dv)
	{
   		var multiplicador = 9;
    	var aux = rut;
    	var suma = 0;
    	while(aux > 0)
			{
        		var unidades = aux % 10 ;
        		aux = (aux - unidades) / 10;
        		suma += (multiplicador * unidades);
        		multiplicador--;
        		if (multiplicador < 4)
					{
            			multiplicador = 9;
       				 }
    		}
    	digito = suma % 11;
    	if (digito == 10)
			{
        		digito = "k";
    		}
    	if (dv == digito)
			{
        		return true;
    		}
    	else 
			{
       			 return false;
    		}
	} 

function isEmailAddress(email)
	{
		var s = email;
		var filter=/^[A-Za-z][A-Za-z0-9_]*@[A-Za-z0-9_]+\.[A-Za-z0-9_.]+[A-za-z]$/;
		if (s.length == 0 ) return true;
		if (filter.test(s))
		return true;
		else
		alert("Ingrese una direcci�n de correo v�lida");
		document.form1.emailusuario.focus();
		return false;
	}
function ModificarTipoUsuario()
	{
		miPopup = window.open("../PHP/ListaTipoUsuario.php?condicion=2","tipousuario","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
		
function ModificarDistribuidor()
	{
		miPopup = window.open("../PHP/Lista_Distribuidor.php","cue","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
function ModificarUsuarioSistema()
	{
		miPopup = window.open("../PHP/Lista_Usuario_Sistema.php","cue","top=200,left=400,width=350,height=200,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}	
function pres_espacio()
	{
		if(event.keyCode==32)
			{
				event.returnValue = false;
				return;
			}
		
		event.returnValue = true;
	}		
	
	
function BuscarUsuario()
	{
		//alert(document.form1.rutusuario.value);
		if (document.form1.rutusuario.value=='' && document.form1.dvusuario.value=='')
			{
				alert("Debe Ingresar el Rut y el digito verificador para realizar la busqueda");
				document.form1.rutusuario.focus();
				return;
			}
		// Primer paso: Obtener el rut que ingreso el usuario
   		 var rutCompleto = document.form1.rutusuario.value;
		// Eliminamos los caracteres raros, espacios, puntos, guiones.
    	// Pasamos a minusculas, y separamos el rut y el digito verificador
    	rutCompl = rutCompleto.toLowerCase();
   		var dv=document.form1.dvusuario.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
   		 valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   		 valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
        		alert("El Rut no es Valido.Ingreselo Nuevamente");
        		document.form1.rutusuario.value="";
				document.form1.dvusuario.value="";
				document.form1.rutusuario.focus();
   			 }
		document.form1.buscar.value=1;
		document.form1.submit();
	}
function ModificarUsuario()
	{
		if (document.form1.rutusuario.value=='')
			{
				alert("Debe Ingresar el Rut del Usuario que desea Modificar");
				document.form1.rutusuario.focus();
				return;
			}
		if (document.form1.dvusuario.value=='')
			{
				alert("Debe Ingresar el Digito Verificador del Rut del usuario que desea modificar");
				document.form1.dvusuario.focus();
				return;
			}
			
		if (document.form1.usuario.value=='')
			{
				alert("Debe Ingresar el Usuario que desea Modificar");
				document.form1.usuario.focus();
				return;
			}
		if (document.form1.password.value=='')
			{
				alert("Debe Ingresar la Contrase�a para el usuario que desea Modificar");
				document.form1.password.focus();
				return;
			}
		
		
		if (document.form1.tipo_usuario.value=='')
			{
				alert("Debe Ingresar el Tipo de Usuario que desea Modificar");
				document.form1.tipo_usuario.focus();
				return;
			}
		if (document.form1.nombreusuario.value=='')
			{
				alert("Debe Ingresar el Nombre");
				document.form1.nombreusuario.focus();
				return;
			}
		if (document.form1.apellido.value=='')
			{
				alert("Debe Ingresar el Apellido");
				document.form1.apellido.focus();
				return;
			}
		
		if (document.form1.direccion.value=='')
			{
				alert("Debe Ingresar la Direcci�n del usuario que desea modificar");
				document.form1.direccion.focus();
				return;
			}
		if (document.form1.comuna.value=='')
			{
				alert("Debe Ingresar la Comuna a la cual pertenece el usuario");
				document.form1.comuna.focus();
				return;
			}
		if (document.form1.ciudad.value=='')
			{
				alert("Debe Ingresar la Ciudad a la cual pertenece el usuario");
				document.form1.ciudad.focus();
				return;
			}
		if (document.form1.distribuidor.value=='')
			{
				alert("Debe Ingresar el Distribuidor");
				document.form1.distribuidor.focus();
				return;
			}
		if (document.form1.usuariosistema.value=='')
			{
				alert("Debe Ingresar si Usuario del Sistema");
				document.form1.usuariosistema.focus();
				return;
			}
		if (document.form1.cuentacorrinte.value=='')
			{
				alert("Debe Ingresar Cuenta Corriente para el usuario");
				document.form1.cuentacorrinte.focus();
				return;
			}
		if (document.form1.cuentacontable.value=='')
			{
				alert("Debe Ingresar Cuenta Contable para el usuario");
				document.form1.cuentacontable.focus();
				return;
			}
		if (document.form1.banco.value=='')
			{
				alert("Debe Ingresar el Banco");
				document.form1.banco.focus();
				return;
			}
		if (document.form1.estado.value=='')
			{
				alert("Debe Ingresar el Estado del Usuario que desea Modificar");
				document.form1.estado.focus();
				return;
			}
		
		if (document.form1.VALIDACION.value==1)
			{
				if (document.form1.codigomanager.value=='')
					{
						alert("Debe Ingresar el Codigo Manager para este tipo de Usuario");
						document.form1.codigomanager.focus();
						return;
					}
			}
    	var rutCompleto = document.form1.rutusuario.value;
		rutCompl = rutCompleto.toLowerCase();
    	var dv=document.form1.dvusuario.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
    	valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   	 	valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
       	 		alert("El Rut no es Valido.Ingreselo Nuevamente");
        		document.form1.rutusuario.value="";
				document.form1.dvusuario.value="";
    		}
		
		document.form1.guardar.value="1";
		document.form1.submit();
	}
function ModificarEstado()
	{
		miPopup = window.open("../PHP/Lista_Estados.php","cue","top=200,left=400,width=350,height=200,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
function CuentaContable()
	{
		miPopup = window.open("../PHP/Lista_CuentaContable.php","cuentacontable","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
function Limpiar()
	{
		document.form1.rutusuariovalue="";
		document.form1.dvusuario.value="";
		document.form1.tipousuario.value="0";
		document.form1.tipocuenta.value="0";
		document.form1.usuario.value="";
		document.form1.passwordusuario.value="";
		document.form1.nombreusuario.value="";
		document.form1.apellido.value="";
		document.form1.direccion.value="";
		document.form1.telefono.value="";
		document.form1.comuna.value="";
		document.form1.ciudad.value="";
		//document.form1.contactodistribuidor.value="";
		document.form1.distribuidor.value="0";
		document.form1.cuentacorriente.value="";
		document.form1.email.value="";
		document.form1.celular.value="";
	//	document.form1.usuariosistema.value="0";
	}
function ModificarBanco()
	{
		miPopup = window.open("../PHP/ListaBancos.php","bancos","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();

	}
function AsociarPromotor()
	{
		if (document.form1.promotor.value==0)
			{
				alert("Debe Seleccionar el Promotor que desea Asociar");
				document.form1.promotor.focus();
				return
			}
		document.form1.asociar.value=1;
		document.form1.submit();
		
	}
function Filtrar()
	{
		//alert(document.form1.tipo_usuario.value);
		if (document.form1.promotor.value==0)
			{
				alert("Debe Seleccionar el Promotor para realizar la busqueda");
				document.form1.promotor.focus();
				return;
			}
		document.form1.guardar2.value=1;
		document.form1.submit();
	}
function SubmitFormulario()
	{
		document.form1.guardar2.value=1;
		document.form1.submit();
	}
</script>
</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<form id="form1" name="form1" method="post" action="">
  <div class="TituloPlomo" id="Layer3">
  <table width="267" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="83" rowspan="4"><img src="../Imagenes/Promotor_80x80png.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="171"><p>&nbsp;</p>      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong>Asociar Promotor </strong><strong></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha">{fecha} </span></td>
    </tr>
    
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
  </table>
</div>
<div id="etiqutaprimera" style="position:absolute; left:21px; top:159px; width:537px; height:35px; z-index:3; border:0pt black solid;">
  <table width="546" border="0" cellspacing="0">
    <tr>
      <td width="169" class="celdalazul">Promotor</td>
      <td width="373" class="celdalCELEST"><select name="promotor" class="seleccionTurist_180" onchange="SubmitFormulario();" >
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : PROMOTOR -->
            <option value="{COD_USUA}" class="seleccion2" {SELECCIONAR}>{NOMBRE}</option>
            <!-- END BLOCK : PROMOTOR -->
        </select>&nbsp;
        <!-- <img src="../Imagenes/120px-Crystal_Clear_action_build19x19.png" width="19" height="19"  onclick="Filtrar()" alt="Buscar Asociaciones" style="cursor:hand"/>-->
        <input type="hidden" name="asociar" value="0" />
		 <input type="hidden" name="guardar2" value="0"/>
		 <input name="Submit" type="button" class="BotonTurist_Celest" value="Asociar" onclick="AsociarPromotor()" /></td>
    </tr>
  </table>
</div>
<div id="etiquetasegunda" style="position:absolute; left:24px; top:212px; width:565px; height:324px; z-index:3; border:0pt black solid; overflow:auto">
  <table width="544" border="0" cellspacing="0">
    <tr>
      <td width="88" class="celdalazul">Seleccionar </td>
      <td width="452" class="celdalazul">Descripci&oacute;n Distribuidor </td>
      <td width="452" class="celdalazul">Promotor</td>
    </tr>
	<!-- START BLOCK : distribuidores -->
    <tr>
      <td class="celdalCELEST"><label>
        <div align="center">
          <input type="checkbox" name="checksol[]" value="{CODIGODISTRIBUIDOR}" {CHECKEADO} />
          </div>
      </label></td>
      <td class="celdalCELEST" width="452">{CODIGODISTRIBUIDOR}-{NOMBREDISTRIBUIDOR}&nbsp;</td>
      <td class="celdalCELEST" width="452">{PROMOTOR_DIST}&nbsp;</td>
    </tr>
	<!-- END BLOCK : distribuidores -->
  </table>
</div>
<div id="etiquetasegunda" style="position:absolute; left:25px; top:211px; width:564px; height:30px; z-index:3; border:0pt black solid;">
  <table width="544" border="0" cellspacing="0">
    <tr>
      <td width="88" class="celdalazul">Seleccionar </td>
      <td width="452" class="celdalazul">Descripci&oacute;n Distribuidor </td>
      <td width="452" class="celdalazul">Promotor</td>
    </tr>
	</table>
  </div>
  
  <div id="etiquetatercera" style="position:absolute; left:637px; top:184px; width:86px; height:25px; z-index:3; border:0pt black solid;">
    <label></label>
</div>
</form>

</body>
</html>

