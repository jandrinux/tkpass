<script type="text/javascript">
function carrito()
{
    window.close();
}

$(document).ready(function(){
    $("#tipoReserva").change(function(){ 
        var tab = $('#radio').val();
        $.post("CARGA_dinamica.php?a=7",{ id:$(this).val(), tab:tab },function(data){$("#select_fop").html(data);})
        estado_pendiente();
    });

})



function combos(afecto,exento,combo,afecto_combo,exento_combo,id_promo, total_sin_combo, comision, total_combo_adt, total_combo_chd)
{
    

    $('#id_combo').val(combo);
    $('#id_promo').val(id_promo);
    
    if(combo == 1) 
    {
   
        $('#resumen_promocion').hide();
        $('#resumen_combo').show();
        
        var af = $('#valor_afecto').val();
        var ex = $('#valor_exento').val();
        var subtotal = $('#subtotal').val();
        
              
        if (total_sin_combo == 0)
        {
          val_afecto = afecto_combo;
          val_exento = exento_combo;      
          total_tarjeta = val_afecto + val_exento;        
        }
        else
        {
          val_afecto = parseInt(afecto) + parseInt(afecto_combo);
          val_exento = parseInt(exento)+parseInt(exento_combo); 
          total_tarjeta = total_sin_combo + afecto_combo + exento_combo;         
        }

        
         $('#comision_promocion').val(comision);
         $('#total_combo_adt').val(total_combo_adt);
         $('#total_combo_chd').val(total_combo_chd);
        
        dscto_combo = subtotal - total_tarjeta;
        
      
        dscto_combo = formatNumber(dscto_combo,'');
        $('#dscto_combo').val('$ '+dscto_combo);
        
        
        t_tarjeta = formatNumber(total_tarjeta,'');
        $('#tot_tarjeta').val('$ '+t_tarjeta);
        
        t_efectivo = formatNumber(total_tarjeta,'');
        $('#tot_efectivo').val(t_efectivo);
        
        tot_afecto = formatNumber(val_afecto,'');
        $('#total_afecto').val('$ '+val_afecto);
        
        tot_exento = formatNumber(val_exento,'');
        $('#total_exento').val('$ '+val_exento);
        
        
    }
    else
    {
        var af2 = $('#valor_afecto_normal').val();
        var ex2 = $('#valor_exento_normal').val();
        t_efectivo = parseInt(af2)+parseInt(ex2);
        
        t_efectivo = formatNumber(t_efectivo,'');
        $('#tot_efectivo').val('$ '+t_efectivo);

        $('#resumen_promocion').show();
        $('#resumen_combo').hide();
    }
        
}


function telefono_beneficio()
{

    var radio = $("#radio").val();
    var cant = $("#cantidad").val();
 
    if (radio == 3)
    {
        $("#select_fop option[value='W']").remove();
        
        for (var i = 0; i < cant; i++)
        
        {
            var n = "check" + i; 

            if (document.getElementById(n).checked)
            {
                    benef = $("#"+n).val();
                    if (benef == 2 || benef == 6)
                    {
                        document.getElementById('fieldset_beneficio').style.display = '';
                        $("#select_fop option[value='E']").remove();
                    }
                    else
                    {
                        document.getElementById('fieldset_beneficio').style.display = 'none';
                        $("#select_fop").append("<option value='E'>Efectivo</option>");
                        
                    }
            }

        }
        

        var subtotal = document.getElementById('subtotal').value;
        subtotal = subtotal.replace('$,','');
    
        var ds = document.getElementById('dcto_beneficio');
        ds.style.display = 'none';
        var cantidad = document.getElementById('cantidad').value;

    
         for (var i = 0; i < cantidad; i++)
        {
           var n = "check" + i; 
       
           if (document.getElementById(n).checked)
           {
               var id = document.getElementById(n).value;
              
               
               $("#msgbox").removeClass().html('<p><img src="../Imagenes/load-input.gif" width="35" height="10" /></p>').fadeIn(1000);
    
                $.post("consulta_descuento_beneficio.php",{ id:id },function(data)
                { 
                   
                    if (data != '')
                    {
                          
                        div = document.getElementById('msgbox');
                        div.style.display = 'none';
                        ds.style.display = '';
                        
                        total_tarjeta = parseInt(subtotal) - parseInt(data);
                        total = formatNumber(data,''); 
                        total_tarjeta = formatNumber(total_tarjeta,''); // funcion en archivo ../js/ajax.js
        
                        document.getElementById('dcto_beneficio').value = "$ "+total;
                        document.getElementById('tot_tarjeta').value = "$ "+total_tarjeta;
                        document.getElementById('tot_efectivo').value = "$ "+total_tarjeta;
                        
                       // document.getElementById('total_redondeado').value = "$ "+ total_tarjeta;
                   
                         //RECALCULA EL VALOR EXENTO CON EL DESCUENTO*********************************
                           var ta =  $("#xTA").html();
                           ta = ta.replace('$ ','');
                           ta = ta.replace(',','');
                           var te = (subtotal - ta) - data;
                           $("#xTE").html("$ " + formatNumber(te,''));
                         //****************************************************
                        return false;
                    }
    
                })
    
           }
           
        }
     
    }
}


function calcular_descuento()
{
   //debugger;
    dscto_cordinador();
    estado_pendiente();
    format_input();
    info_boleta_tr();
    //ocultar_boletas();
    telefono_beneficio();
      
}

function format_input()
{
    $("#titulo4Ultimos").hide();
    $("#ultimos_digitos").hide();
    $("#tituloTipoTarjeta").hide();
    $("#tipo_tarjeta").hide();
    
    document.getElementById('total_efectivo').style.display = 'none';
    document.getElementById('total_tarjeta').style.display = '';
    document.getElementById("tituloTBK").style.visibility='hidden';
    document.getElementById("info_boleta").style.visibility='hidden';
}


function estado_pendiente()
{

    var radio = document.getElementById("radio").value;
    var selecTipo = document.getElementById("tipoReserva").value;
   // var suc = <?=$_SESSION['codigo_suc'];?>;
    var fop = $("#select_fop").val();
    

        if(selecTipo == 1 )
        {
          
            //reservas CC
            document.getElementById("continuar").style.visibility='visible';   
            document.getElementById("codSocio").style.visibility='hidden'; 
            document.getElementById("img_socio").style.visibility='hidden';
            document.getElementById("titulo_mayorista").style.visibility='hidden';
            document.getElementById("nombre_mayorista").style.visibility='hidden';
            document.getElementById("tituloCodSocio").style.visibility='hidden';
        


            if (radio == 2 || radio == 3 || radio == 5)
            {

                    document.getElementById("codSocio").style.visibility='hidden'; 
                    document.getElementById("img_socio").style.visibility='hidden';
                    document.getElementById("tituloCodSocio").style.visibility='hidden';
                
            }
            if (suc == 8)
            {
                
                     document.getElementById("codSocio").style.visibility='visible'; 
                     document.getElementById("img_socio").style.visibility='visible'; 
                     document.getElementById("tituloCodSocio").style.visibility='visible';    
                                 
            }
            
            if (fop != "S")
            {
                document.getElementById('select_fop').disabled = false; 
                document.getElementById("tipo_mayorista").value = 0;
                document.getElementById("codSocio").value = '';
            }

            $("#descuento").show(); 
            $("#promocion").show(); 
            
          if("1" == radio  || "2" == radio || "5" == radio)
          {
             $("#tipoReserva option[value='4']").remove();
            
          }
          
      }    

       if(selecTipo == 2)
       {
        //Reservas PP
            document.getElementById("continuar").style.visibility='hidden';
            document.getElementById("titulo_mayorista").style.visibility='hidden';
            document.getElementById("nombre_mayorista").style.visibility='hidden';
            document.getElementById("codSocio").style.visibility='visible'; 
            document.getElementById("img_socio").style.visibility='visible';
            document.getElementById("tituloCodSocio").style.visibility='visible';

            
            if (radio == 3)
            {
                document.getElementById("codSocio").style.visibility='hidden'; 
                document.getElementById("img_socio").style.visibility='hidden';
                document.getElementById("tituloCodSocio").style.visibility='hidden';
            }

            document.getElementById('select_fop').disabled = false; 
            document.getElementById("tipo_mayorista").value = 0;
            document.getElementById("codSocio").value = '';
            

            $("#descuento").show(); 
            $("#promocion").show(); 

            
        }

        // REQ RA
        if(selecTipo == 3)
        {
            
            if (radio == 4)
            {
                document.getElementById("codSocio").style.visibility='visible'; 
                document.getElementById("img_socio").style.visibility='visible';
                document.getElementById("tituloCodSocio").style.visibility='visible'; 
                
                    $.post("consulta_excursiones_comisionado.php",function(data)  //consulta por precios_comisionados al ser una reserva RA
                    {
                
                        if (data != 0)
                        {

                            valor_afecto_repl = af.replace(/[^a-zA-Z 0-9.]+/g,"");
                            
                            var valor_exento = data-valor_afecto_repl;
                            
                            valor_exento = formatNumber(valor_exento,'');
                            $('#tot_exento').val("$ "+valor_exento);
                            
                            data = formatNumber(data,'');
                            if ($("#tot_efectivo").is(":visible")) {
                                 $('#tot_efectivo').val("$ "+data);
                            }
                            if ($("#tot_tarjeta").is(":visible")) {
                                $('#tot_tarjeta').val("$ "+data);
                            }

                        }

                    })

            }
            else
            {
                document.getElementById("codSocio").style.visibility='hidden'; 
                document.getElementById("img_socio").style.visibility='hidden';
                document.getElementById("tituloCodSocio").style.visibility='hidden';
            }
            
           
       
        }
        else
        {
            if (radio == 4)
            {

                $('#tot_efectivo').val(ef);
                $('#tot_exento').val(ex);
                
                
            }
        }

        
        if(selecTipo == 4 )
        {
            //reservas CC
            document.getElementById("titulo_mayorista").style.visibility='visible';
            document.getElementById("nombre_mayorista").style.visibility='visible';  
            document.getElementById("codSocio").style.visibility='visible'; 
            document.getElementById("img_socio").style.visibility='hidden';
            document.getElementById("tituloCodSocio").style.visibility='visible';

            $("#descuento").hide(); 
            $("#promocion").hide(); 

       
        }
        
        if (radio == 4)
        {
            var descuento = new DescuentoEspecial();
            descuento.comboDescuentoEsp();
        }

}

function DescuentoEspecial(){
  

    this.comboDescuentoEsp = function(){
      var option = "<option value='0'>-- SELECCIONE --</option>";
        $('[name="descuento_especial"]').append(option);
            $.getJSON("lista_descuento_especial_json.php",function(data){
             
       
                if (radio == 4 && cod_tusua == 3 && data != "no" )
                {
                    $('#tr_dscto_especial').show();
                    
                    $.each(data , function(index, dscto){
                         option = "<option value='" + dscto.descuento + "'>" + dscto.descuento+"%</option>";
                        $('[name="descuento_especial"]').append(option);
                    });
                }
                else
                {
                    $('#tr_dscto_especial').hide();
                }
    
             
            });

        }   

}

function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}


function redondea()
{

    var fop = $("#select_fop").val();
    var valor_restar = 0;

    if (fop == "E")
    {

        var total_e = $("#tot_efectivo").val(); 
        var total_t = $("#tot_tarjeta").val(); 
    
        total_e = replaceAll(total_e, "$", "" ); //total_e.replace('$ ','');
        total_t = replaceAll(total_t, "$", "" );//total_t.replace('$ ','');

        number_e = replaceAll(total_e, ",", "" ); //total_e.split('.').join('');
        number_t = replaceAll(total_t, ",", "" );//total_t.split('.').join('');

        valor_restar = number_t - number_e;
        
        resto = number_e.slice(-3);

        
        if (resto < 500)
        {
            dif = resto;
            number_e = number_e - resto;

        }
        else
        {
            dif = resto - 500;
            number_e = number_e - dif;
        }

        e = formatNumber(number_e,'');
        $('#tot_efectivo').val('$ '+e);
        $('#valor_redondeo').val(dif);
        
    }
    else
    {
        $('#valor_redondeo').val(0);
    }

    
}


function info_boleta_tr()
{

    var select = $("#select_fop").val();
    var radio = $("#radio").val();
    var tipo_reserva = $("#tipoReserva").val();
    


    //ocultar_boletas();
    
    if (radio == 1)
    {
        var total_af = $("#total_af").val();
        var total_e = $("#tot_efectivo").val(); 
        var total_t = $("#tot_tarjeta").val(); 

        total_e = total_e.replace('$ ','');
        total_t = total_t.replace('$ ','');

        total_e = total_e.replace(',','');
        total_t = total_t.replace(',','');

    
        if(select == "C" || select == "D" || select == "S")
        {
            var total_exento = parseInt(total_t) - parseInt(total_af);
        }
        else
        {
            var total_exento = parseInt(total_e) - parseInt(total_af);   
        }

        total_exen = formatNumber(total_exento,''); 
        $("#tot_exento").val('$ '+total_exen);

    }

    if(select == "C" || select == "TC")
    {
    
        document.getElementById('total_efectivo').style.display = 'none';
        document.getElementById('total_tarjeta').style.display = '';
        
        document.getElementById("tituloTBK").style.visibility='visible';
        document.getElementById("info_boleta").style.visibility='visible';
        $("#titulo4Ultimos").show();
        $("#ultimos_digitos").show();
        $("#tituloTipoTarjeta").show();
        $("#tipo_tarjeta").show();
    
    }
    if(select == "D" )
    {
        document.getElementById("tituloTBK").style.visibility='visible';
        document.getElementById("info_boleta").style.visibility='';
        
        document.getElementById('total_efectivo').style.display = 'none';
        document.getElementById('total_tarjeta').style.display = '';
        
        $("#titulo4Ultimos").hide();
        $("#ultimos_digitos").hide();
        $("#tituloTipoTarjeta").hide();
        $("#tipo_tarjeta").hide();
        
    }
    
    if(select == "E" )
    {
        $("#titulo4Ultimos").hide();
        $("#ultimos_digitos").hide();
        $("#tituloTipoTarjeta").hide();
        $("#tipo_tarjeta").hide();
        $("#td1_confirmaFop").show();
        $("#td2_confirmaFop").show();
        $("#confirmaFop").show();
        
        document.getElementById('total_efectivo').style.display = '';
        document.getElementById('total_tarjeta').style.display = 'none';
        document.getElementById("tituloTBK").style.visibility='hidden';
        document.getElementById("info_boleta").style.visibility='hidden';
        
    }
    else
    {
        $("#td1_confirmaFop").hide();
        $("#td2_confirmaFop").hide();
        $("#confirmaFop").show();
    }

    if (tipo_reserva != 4) {
        redondea();
    }
    else
    {
        if(select == "E" ) //en el caso que es mayorista
        {
            $("#td1_confirmaFop").hide();
            $("#td2_confirmaFop").hide();
            $("#confirmaFop").hide();
        }
    }
}

function valida_tarjetas()
{
    var tbk = document.getElementById('info_boleta').value;
    var ultimos_digitos = $('#ultimos_digitos').val();
    var tipo_tarjeta = $('#tipo_tarjeta').val();
    
    if(tbk == '')
    {
        alert("Ingrese Voucher Transbank.");
        document.getElementById('info_boleta').focus();
        return false;
    }
    
    if (ultimos_digitos.length < 4)
    {
        alert("Complete los 4 ultimos digitos de la Tarjeta Bancaria");
        document.getElementById('ultimos_digitos').focus();
        return false; 
    }
    
    
    if (tipo_tarjeta == '')
    {
        alert("Seleccione Tipo de Tarjeta");
        document.getElementById('tipo_tarjeta').focus();
        return false;
    }
    
    return true;
}


function finalizar()
{

    var TipoReserva = $('#tipoReserva').val();
    var confirmaFop = $('#confirmaFop').val();
    var fop = $('#select_fop').val();
    var tipoUsuario = $('#tipoUsuario').val();
    var tbk = $('#info_boleta').val();
    var cmbo_coordinador = $('#dscto_cordinador').val();
    var cod_socio= $('#codSocio').val();
    var ultimos_digitos = $('#ultimos_digitos').val();
    var tipo_tarjeta = $('#tipo_tarjeta').val();
    var radio = $('#radio').val();
    var nombre_mayorista = $('#nombre_mayorista').val();
    
    

    if (TipoReserva == 3 && cod_socio == '')
    {
        alert("Ingreso de Codigo Socio es obligatorio");
        return false;
    }

    if (TipoReserva == 4 && nombre_mayorista == '0')
    {
        alert("Seleccione nombre de Mayorista");
        return false;
    }
    
    
    if (fop == "S")
    {
        alert("Seleccione Forma de Pago");
        $("#select_fop").focus();
        return false;
    }
    else
    {
        if (fop == "E" && confirmaFop != "E" && TipoReserva != 4) 
        {

        alert("Confirme Forma de Pago");
        $("#confirmaFop").focus();
        return false;
        }
    }


    
    if (radio == 3)
    {
 
        var cant = $("#cantidad").val();
        var telefono = $("#telefono").val();
        var rut = $("#rut").val();
        var flag_campos = false;
        var flag_rut = false;
 
        
        for (var i = 0; i < cant; i++)
        {
            var n = "check" + i; 

            if (document.getElementById(n).checked)
            {
                benef = $("#"+n).val();
                if (benef == 2 || benef == 6)
                {
                    if(telefono == "" || rut == "")
                    {
                        flag_campos = true;
                    }
                    else
                    {
                        flag_rut = true;
                    }
                }

            }

        }
        
        if (flag_rut == true)
        {
           var valido = $.ajax({
                        type: "POST",
                        url: "valida_rut.php",
                         data: "rut="+rut,
                        async: false,
                        success : function(data) {
                           
                        }
                    }).responseText;
                    if(valido=="invalido"){
                        alert( "Rut invalido");
                        return false;
                    };
            
        }
       
    }

     
    $.post("consulta_excursiones.php",function(data)  //consulta si existe una excursion codigo 6, para boletas
    { 

       var vaciar= confirm("Esta seguro de finalizar la compra");
         if ( vaciar ) 
         {
            
            if(flag_campos == true)
            {
                alert("Ingrese campos mandatorios (*)");
                return false;
            }


              if (data != '')
              {
                
                if (cmbo_coordinador != 0)
                {
                        if (data == 1 || data == 2)
                        {  
                            
                            $('#tipo_venta').val(1);

                                if (TipoReserva == 1)
                                {

                                   $('#tipo_venta').val(1);

                                }
                                

                                if (TipoReserva == 2) // Por Pagar no valida ingreso de boletas a menos que sea forma de pago TC
                                {

                                    if (fop == 'TC')
                                    {

                                      if (valida_tarjetas() == false)
                                      {
                                        return false;
                                      } 
                                      
                                      $('#tipo_venta').val(1);  

                                    }
                                    else  
                                    {
                                        
                                        if (fop == 'W')
                                        {
                                          
                                          $('#tipo_venta').val(1);  

                                        }
                                        else
                                        {
                                            $('#tipo_venta').val(3);                                           
                                        }
                                        

                                    }

                                }
                                if (TipoReserva == 3)  //RA, no valida el ingreso de boletas
                                {

                                    if (data != 2)
                                    {
                                        alert("Tipo de Reservas RA, valido para compras de una excursion");
                                        return false;
                                    }


                                    $('#tipo_venta').val(0);

                                }
                                if (TipoReserva == 4)  //Mayorista, no valida el ingreso de boletas
                                {
                                    /*
                                    if (document.getElementById('archivo').value == '')
                                    {
                                        alert("Es necesario cargar un archivo");
                                        document.getElementById('archivo').focus();
                                        return false;
                                        
                                    }
                                    */
                                    $('#tipo_venta').val(0);

                                }

                            

                        }

                       if (data == 3 ) //Si existe solo una excursion tipo 6 en carrito
                       {
                        
                            if (TipoReserva == 3)  //RA, no valida el ingreso de boletas
                            {
                                
                                alert("No es posible hacer reservas RA para esta excursion");
                                return false;
                            }

                            $('#tipo_venta').val(3);

                       } 

                       if (fop == 'C')
                       {
                            if (valida_tarjetas() == false)
                            {
                                 return false;
                            }
                       }
                        
                       if (fop == 'D')
                       {
                            if(tbk == '')
                            {
                                alert("Ingrese Voucher Transbank.");
                                document.getElementById('info_boleta').focus();
                                return false;
                            }
                       }
                        
                        

                        if(tipoUsuario == '7' )
                        {
                               if (document.getElementById("tipoReserva").value == document.getElementById("confirmaTipo").value)
                               {  
                                    $("#wpc").click(); 
                                    document.formCompra.submit();  
                                    //alert('comprando'); 
                               }
                               else
                               {
                                    alert("Confirme el Tipo de Reserva.");
                                    document.getElementById("confirmaTipo").focus();
                               }
                         }
                         else
                         {
                               $("#wpc").click(); 
                               document.formCompra.submit();
                               // alert('comprando');
                         }
                               
                         
                }
                else //end cmbo_coordinador 
                {
                    
                    //document.getElementById('tipo_venta').value = 3;
                    $('#tipo_venta').val(3);
                    document.formCompra.submit();  
                 }    
                      
                         
              }
              else// end  if (data != '')
              {
                 alert("No existen servicios para realizar una compra");
                 return false;
              }
                  
           
          }
    });
    
    
  
}

function vaciar_carrito(){


var vaciar= confirm("Esta seguro de vaciar el carrito?");

 if ( vaciar ) {

    $("#wpc").click();

    $.post("vaciar_carrito.php",{  },function(data){

        if(data == 1)
        {
            $(".modalCloseImg").click();
        }
        if(data == 2)
        {
            alert("Hubo un problema al eliminar los pasajeros, intente nuevamente")
            $(".modalCloseImg").click();
        }

        location.reload();

    });

 }
}


function busca_codigo(tab){
        miPopup = window.open("../PHP/Codigo_Vendedor.php?t="+tab,"miwin","top=200,left=350,width=470,height=460,scrollbars=yes,titlebar=no,location=no");
        miPopup.focus();
}




function recalcular_porcentaje(dscto_cordinador)
{

    var dscto_cordinador = dscto_cordinador.value;
    var select = document.getElementById("select_fop").value;
    
  
    if (dscto_cordinador != 0)
    {
        var tot_efectivo = ef;
        var tot_tarjeta  = ta;
        var tot_exento = ex;
        var tot_afecto = af;
    }
    else
    {
        var tot_efectivo = $('#tot_efectivo').val();
        var tot_tarjeta = $('#tot_tarjeta').val();
        var tot_exento = $('#tot_exento').val();
        var tot_afecto = $('#tot_afecto').val();
    }
    
    
    tot_efectivo = tot_efectivo.replace("$","").replace(',','');
    tot_tarjeta = tot_tarjeta.replace("$","").replace(',','');
    tot_exento = tot_exento.replace("$","").replace(',','');
    tot_afecto = tot_afecto.replace("$","").replace(',','');
    

    tot_e = (parseInt(dscto_cordinador)*parseInt(tot_efectivo))/100;
    tot_t = (parseInt(dscto_cordinador)*parseInt(tot_tarjeta))/100;
    tot_ex = parseInt(tot_e) - parseInt(tot_afecto);
    
    if (tot_ex < 0)
    {
        tot_ex = 0;
    }
    
    //tot_ex = (parseInt(dscto_cordinador)*parseInt(tot_exento))/100;
    //tot_af = (parseInt(dscto_cordinador)*parseInt(tot_afecto))/100;
    
    efectivo = formatNumber(tot_e, '');
    tarjeta = formatNumber(tot_t, '');
    exento = formatNumber(tot_ex, '');
    //afecto = formatNumber(tot_af, '');
    
    
    
    $('#tot_efectivo').val("$ "+efectivo)
    $('#tot_tarjeta').val("$ "+tarjeta);
    $('#tot_exento').val("$ "+exento);
    //$('#tot_afecto').val("$ "+afecto);
    
    if (dscto_cordinador == 0)
    {
        $('#boletaTTK').hide(); 
        $('#boletaTTS').hide(); 
    }
    else
    {
        $('#boletaTTK').show(); 
        $('#boletaTTS').show(); 
    }
    
}

function calcular_dscto_especial(dscto_especial)
{

    var dscto_especial = dscto_especial.value;
    
    var tot_efectivo = ef;
    var tot_tarjeta  = ta;
    var tot_exento = ex;
    var tot_afecto = af;

    if (dscto_especial == 0)
    {
        $("#div_dcto_especial").hide();
        $("#inp_dcto_especial").hide();
    }
    else
    {
        $("#div_dcto_especial").show();
        $("#inp_dcto_especial").show();

    }

    
    tot_efectivo = tot_efectivo.replace("$","").replace(',','');
    tot_tarjeta = tot_tarjeta.replace("$","").replace(',','');
    tot_exento = tot_exento.replace("$","").replace(',','');
    tot_afecto = tot_afecto.replace("$","").replace(',','');
    
    tot_e = (parseInt(dscto_especial)*parseInt(tot_efectivo))/100;
    tot_e = parseInt(tot_efectivo) - parseInt(tot_e);
    
    tot_t = (parseInt(dscto_especial)*parseInt(tot_tarjeta))/100;
    tot_t = parseInt(tot_tarjeta) - parseInt(tot_t);
    
    tot_ex = parseInt(tot_e) - parseInt(tot_afecto);
  
    
    if (tot_ex < 0)
    {
        tot_ex = 0;
    }
    
    efectivo = formatNumber(tot_e, '');
    tarjeta = formatNumber(tot_t, '');
    exento = formatNumber(tot_ex, '');

    $('#tot_efectivo').val("$ "+efectivo)
    $('#tot_tarjeta').val("$ "+tarjeta);
    $('#tot_exento').val("$ "+exento);
    
    dscto = (parseInt(dscto_especial)*parseInt(tot_efectivo))/100;
    valor_dscto = "$ "+formatNumber(dscto, '');
    
    $("#inp_dcto_especial").val(valor_dscto);
    
   redondea();

}

function Busca_Codigo()
{
    var nombre = $('#nombre_mayorista').val();
    if(nombre==0){document.getElementById("codSocio").value = '';document.getElementById("tipo_mayorista").value = 0;}
    //if(nombre==1){document.getElementById("codSocio").value = '';document.getElementById("tipo_mayorista").value = 1;}
    if(nombre==2){document.getElementById("codSocio").value = '10676';document.getElementById("tipo_mayorista").value = 2;} // VIATOR
    if(nombre==3){document.getElementById("codSocio").value = '10743';document.getElementById("tipo_mayorista").value = 3;}
    if(nombre==4){document.getElementById("codSocio").value = '11083';document.getElementById("tipo_mayorista").value = 4;}
    if(nombre==6){document.getElementById("codSocio").value = '11455';document.getElementById("tipo_mayorista").value = 6;}
    if(nombre==7){document.getElementById("codSocio").value = '11424';document.getElementById("tipo_mayorista").value = 7;}
    if(nombre==8){document.getElementById("codSocio").value = '12114';document.getElementById("tipo_mayorista").value = 8;}
    if(nombre==9){document.getElementById("codSocio").value = '12167';document.getElementById("tipo_mayorista").value = 9;} //DESPEGAR

}


</script>