<?php
session_start();
require ("../turistik/Clases/ClaseConexion.inc.php");
require ("../turistik/Clases/class.mail.php");

$correo_decifrado= base64_decode($_POST['c']);
$boleta_decifrada= base64_decode($_POST['b']);
$grupo_idioma = $_POST['g'];
$captcha = $_POST['captcha'];


/** Valida captcha */

if (!empty($captcha)) 
{
    if (empty($_SESSION['captcha']) || trim(strtolower($captcha)) != $_SESSION['captcha']) 
    {
        echo "no-captcha";
        exit();
    }


    $request_captcha = htmlspecialchars($captcha);
    unset($_SESSION['captcha']);
}




if ($correo_decifrado != '' && $boleta_decifrada != '')
{
    $pregunta_1 = $_POST['pregunta_1'];
    $pregunta_2 = $_POST['pregunta_2'];
    $pregunta_3 = $_POST['pregunta_3'];
    $pregunta_4 = $_POST['pregunta_4'];
    $option1 = $_POST['pregunta_5'];
    $comentario = $_POST['comentario'];
    $servicio = $_POST['servicio'];
    $agradecimiento = false;

    
    if ($pregunta_1 != "" || $pregunta_2 != "" || $pregunta_3 != "" || $pregunta_4 != "")
    {
        $responde = 1;
        $suma = $pregunta_1 + $pregunta_2 + $pregunta_3 + $pregunta_4;
        
        if ($suma == 20)
        {
            $calificacion = 3;// positiva
            $agradecimiento = true;
        }
        if ($suma >= 10 && $suma <= 15)
        {
            $calificacion = 2;// media
        }
        if ($suma < 10)
        {
            $calificacion = 1;// negativa
        }
        
        $miConexion = new ClaseConexion;
        $miConexion->conectar();
        $miConexion->EjecutaSP("Actualiza_respuesta_mailing"," ".$boleta_decifrada.", '".$calificacion."' , '".$option1."', '".$option2."', '".$option3."', '".$comentario."', '".$servicio."'  ");
        
        if ($suma > 0 && $suma < 20)
        {
            include_once('mail_agradecimiento.php');

            if ($resp == 1) {
                
                echo "ok";
            }
            else
            {
                echo $resp;
            }
        }
    
        
        exit();
        //$mensaje = "Gracias por su tiempo. Atte. Turistik S.A";
        //print "<script>alert('$mensaje')</script>";  
        //print "<script>window.location = 'index.php?c=".$_POST['c']."&b=".$_POST['b']."&g=".$_POST['g']."'</script>";  
    }


}
else
{
    echo "no";
}

?> 