<?php


include( "../Clases/excelclass.php" );	
require ("../Clases/ClaseConexion.inc.php");
require ("../Clases/ClaseUtil.inc.php");

$excel = new ExcelGen("Detalle_Reporte_Comisiones_Generales","prueba");
$_Util=new Util;
$miConexion = new ClaseConexion;

//echo $_GET['MODO'];

	
$miConexion->Conectar();
$queryDistribuidor=$miConexion->EjecutaSP("Consulta_Distribuidor_Codigo","'".$_GET['CODIGODISTRIBUIDORREAL']."'");
$rowDistribuidor= mysql_fetch_assoc($queryDistribuidor);

$miConexion->Conectar();
$queryVendedor=$miConexion->EjecutaSP("Consulta_Usuario_Codigo","".$_GET['CODIGOVENDEDORREAL']."");	
$rowVendedor= mysql_fetch_assoc($queryVendedor);

$miConexion->Conectar();
$queryPromotor=$miConexion->EjecutaSP("Consulta_Promotor_porCodigo","".$_GET['CODIGOPROMOTORREAL']."");
//$queryPromotor=$miConexion->EjecutaSP("Consulta_Usuario_Codigo","".$_GET['CODIGOPROMOTORREAL']."");	
$rowPromotor= mysql_fetch_assoc($queryPromotor);

$excel->NewLine();
$excel->WriteText("Reporte: Detalle Comisiones Generales");
$excel->NewLine();

$excel->NewLine();
$excel->WriteText("Distribuidor:");
$excel->WriteText("".$rowDistribuidor['NOMBRE_FANTASIA_DIST']."");

$excel->NewLine();
$excel->WriteText("Vendedor:");
$excel->WriteText("".$rowVendedor['NOMBRE_USUA']." ".$rowVendedor['APELLIDO_USUA']."");

$excel->NewLine();
$excel->WriteText("Promotor:");
$excel->WriteText("".$rowPromotor['NOMBRE_USUA']." ".$rowPromotor['APELLIDO_USUA']."");
$excel->NewLine();

$excel->NewLine();
$excel->WriteText("MODALIDAD");
$excel->WriteText("TIPO");
$excel->WriteText("FOLIO TARJETA");
$excel->WriteText("FOLIO TARJETA ANTERIOR");
$excel->WriteText("PRECIO");
$excel->WriteText("INGRESO TURISTIK");
$excel->WriteText("COMISION DISTRIBUIDOR");
$excel->WriteText("COMISION VENDEDOR");
$excel->WriteText("COMISION PROMOTOR");
$excel->WriteText("PRODUCTO");
$excel->WriteText("FECHA");

$excel->NewLine();
//////////////////////////////////////QUE TIPO DE USUARIO ES///////////////////////////////////////////////
$miConexion->Conectar();
$queryPerfil=$miConexion->EjecutaSP("Consulta_Perfil_Usuario","'".$_SESSION['usuario']."',@tipo");
$parametrosSalidaPerfil=$miConexion->ObtenerResultadoSP("@tipo");
$rowPerfil= mysql_fetch_assoc($parametrosSalidaPerfil);
///////////////////////////////////////////////////////////////////////////////////////////////////////////

$miConexion->Conectar();
$queryDetalleComisionGeneral=$miConexion->EjecutaSP("Consulta_Detalle_Comisiones_Generales","".$_GET['CODIGODISTRIBUIDORREAL'].",".$_GET['CODIGOVENDEDORREAL'].",".$_GET['CODIGOPROMOTORREAL'].",".$_GET['MODO']."");
while ($rowDetalleComisionGeneral = mysql_fetch_assoc($queryDetalleComisionGeneral))
	{		 
	  $excel->NewLine();
	  if ($_GET['MODO']==1)
		{
	    	$excel->WriteText("SIN TARJETA PREVIA");
			if ($rowDetalleComisionGeneral['VENTA_ANTERIOR']==2)
			{
				//$tpl->assign("VENTA","ACTUAL");
				$excel->WriteText("ACTUAL");
			}
		else
			{
				if ($rowDetalleComisionGeneral['VENTA_ANTERIOR']==3)
					{
						//$tpl->assign("VENTA","ANTERIOR");
						$excel->WriteText("ANTERIOR");
					}
				else
					{
						$excel->WriteText("");
					}
			}
			$excel->WriteText($rowDetalleComisionGeneral['FOLIO_TAR_COM_TEMP']);
			$excel->WriteText("");
			$PRECION=number_format($rowDetalleComisionGeneral['PRECIO_REAL_COM_TEMP'],2,",",".");
			$PRECION_PESO="$".$PRECION;
			$excel->WriteText($PRECION_PESO);
			if ($PERFIL=='3')
				{

					
			$excel->WriteText("");
					$excel->WriteText("");
				}
			else
				{
					$INGRESO=number_format($rowDetalleComisionGeneral['INGRESO'],2,",",".");
					$INGRESO_PESO="$".$INGRESO;
					$excel->WriteText($INGRESO_PESO);
					$COMISIONDISTRIBUIDOR=number_format($rowDetalleComisionGeneral['I_COMISION_D_COM_TEMP'],2,",",".");
					$COMISIONDISTRIBUIDOR_PESO="$".$COMISIONDISTRIBUIDOR;
					$excel->WriteText($COMISIONDISTRIBUIDOR_PESO);
				}
			$COMISIONVENDEDOR=number_format($rowDetalleComisionGeneral['I_COMISION_V_COM_TEMP'],2,",",".");
			$COMISIONVENDEDOR_PESO="$".$COMISIONVENDEDOR;
			$excel->WriteText($COMISIONVENDEDOR_PESO);
			if ($PERFIL=='3')
				{
					$excel->WriteText("");
				}
			else
				{
					$COMISIONPROMOTOR=number_format($rowDetalleComisionGeneral['I_COMISION_P_COM_TEMP'],2,",",".");
					$COMISIONPROMOTOR_PESO="$".$COMISIONPROMOTOR;
					$excel->WriteText($COMISIONPROMOTOR_PESO);
				}
			$miConexion->Conectar();
			$queryProducto=$miConexion->EjecutaSP("Consulta_Producto","'".$rowDetalleComisionGeneral['CODIGO_PROD_COM_TEMP']."'");
			$rowProducto= mysql_fetch_assoc($queryProducto);
			$excel->WriteText($rowProducto['NOMBRE_PROD']);
			$Fecha=$_Util->FormatearFecha($rowDetalleComisionGeneral['FECHA_COM_TEMP']);	
			$excel->WriteText($Fecha);
		}
	else
		{
			$excel->WriteText("CON TARJETA PREVIA");
			if ($rowDetalleComisionGeneral['VENTA_ANTERIOR']==2)
			{
				//$tpl->assign("VENTA","ACTUAL");
				$excel->WriteText("ACTUAL");
			}
		else
			{
				if ($rowDetalleComisionGeneral['VENTA_ANTERIOR']==3)
					{
						//$tpl->assign("VENTA","ANTERIOR");
						$excel->WriteText("ANTERIOR");
					}
				else
					{
						$excel->WriteText("");
					}
			}
			$excel->WriteText($rowDetalleComisionGeneral['FOLIO_TAR_COM_TEMP']);
			$excel->WriteText($rowDetalleComisionGeneral['FOLIO_TAR_ANTERIOR_COM_TEMP']);
			$PRECION=number_format($rowDetalleComisionGeneral['PRECIO_II_COM_TEMP'],2,",",".");
			$PRECION_PESO="$".$PRECION;
			$excel->WriteText($PRECION_PESO);
			if ($PERFIL=='3')
				{
				$excel->WriteText("");
				$excel->WriteText("");
				}
			else
			{
				$INGRESO=number_format($rowDetalleComisionGeneral['INGRESO'],2,",",".");
				$INGRESO_PESO="$".$INGRESO;
				$excel->WriteText($INGRESO_PESO);
				$COMISIONDISTRIBUIDOR=number_format($rowDetalleComisionGeneral['II_COMISION_D_COM_TEMP'],2,",",".");
				$COMISIONDISTRIBUIDOR_PESO="$".$COMISIONDISTRIBUIDOR;
				$excel->WriteText($COMISIONDISTRIBUIDOR_PESO);
			}
			$COMISIONVENDEDOR=number_format($rowDetalleComisionGeneral['II_COMISION_V_COM_TEMP'],2,",",".");
			$COMISIONVENDEDOR_PESO="$".$COMISIONVENDEDOR;
			$excel->WriteText($COMISIONVENDEDOR_PESO);
			
			if ($PERFIL=='3')
			{
			$excel->WriteText(""); 
			}
			else
			{
			$COMISIONPROMOTOR=number_format($rowDetalleComisionGeneral['II_COMISION_P_COM_TEMP'],2,",",".");
			$COMISIONPROMOTOR_PESO="$".$COMISIONPROMOTOR;
			$excel->WriteText($COMISIONPROMOTOR_PESO);
			}
			$miConexion->Conectar();
			$queryProducto=$miConexion->EjecutaSP("Consulta_Producto","'".$rowDetalleComisionGeneral['CODIGO_PROD_COM_TEMP']."'");
			$rowProducto= mysql_fetch_assoc($queryProducto);
			$excel->WriteText($rowProducto['NOMBRE_PROD']);
			$Fecha=$_Util->FormatearFecha($rowDetalleComisionGeneral['FECHA_COM_TEMP']);	
			$excel->WriteText($Fecha);
		}
	

	}
  $excel->SendFile();

?>


