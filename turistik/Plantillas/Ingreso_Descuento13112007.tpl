<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:16px;
	top:169px;
	width:816px;
	height:102px;
	z-index:13;
}
#Layer3 {
	position:absolute;
	left:18px;
	top:60px;
	width:372px;
	height:62px;
	z-index:0;
}
#Layer4 {
	position:absolute;
	left:17px;
	top:400px;
	width:296px;
	height:95px;
	{VISIBLE_FORM}
	
}
#Layer5 {
	position:absolute;
	left:17px;
	top:350px;
	width:296px;
	height:95px;
	z-index:14;	
}

#Layer2 {	position:absolute;
	left:831px;
	top:51px;
	width:126px;
	height:116px;
	z-index:12;
	}
-->
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Ingreso de Descuento</title>
<style type="text/css">
</style>
<script type="text/JavaScript">
function valida()
{
if((document.form1.porcentaje.value)>'100')
{
	alert('El porcentaje no puede superar el 100%');
	document.form1.porcentaje.focus();
	return;
}
}

function pres_num()
{
	if(event.keyCode!=44)
	{
		if((event.keyCode<48) || (event.keyCode>57))
		{
				event.returnValue = false;
			}		
	}
}	

function Popup()
{
 miPopup = window.open("../PHP/Ingreso_Ubicacion.php","miwin","top=200,left=350,width=400,height=230,scrollbars=yes,titlebar=no,location=no");
 miPopup.focus();	
}
function Asociar()
{
 miPopup = window.open("../PHP/Ingreso_Ubicacion.php","miwin","top=200,left=350,width=400,height=230,scrollbars=yes,titlebar=no,location=no");
 miPopup.focus();	
}

function Proveedor()
{
document.form1.submit();
 miPopup = window.open("../PHP/Ingreso_Proveedor.php?flag=1","miwin","top=200,left=350,width=560,height=400,scrollbars=no,titlebar=no,location=no");
 miPopup.focus();	

}

function Ver()
{
if(document.form1.descripcion.value=='')
{
	alert('Ingrese la descripción del descuento');
	document.form1.descripcion.focus();
	return;
}
if(document.form1.proveedor.value=='0x')
{
	alert('Selecciones el Proveedor');
	document.form1.proveedor.focus();
	return;
}
if(document.form1.descripcion.value=='')
{
	alert('Ingrese la descripción del descuento');
	document.form1.descripcion.focus();
	return;
}
/*
if(document.form1.precio.value=='')
{
	alert('Ingrese el precio del descuento');
	document.form1.precio.focus();
	return;
}
*/
/*
if(document.form1.cantidad.value=='')
{
	alert('Ingrese la cantidad de veces que se entrega el descuento');
	document.form1.cantidad.focus();
	return;
}
*/
/*
if((document.form1.precio.value=='')&&(document.form1.tope.value==''))
{
	alert('Ingrese el tope del valor del descuento');
	document.form1.tope.focus();
	return;
}
if((document.form1.precio.value=='')&&(document.form1.porcentaje.value==''))
{
	alert('Ingrese el procentaje de descuento');
	document.form1.porcentaje.focus();
	return;
}
*/
document.form1.guardar.value='1';
document.form1.submit();
}
</script>
</head>
<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>
<div id="Layer1">
  <form id="form1" name="form1" method="post" action="">
  <div id="layermenu" style="position:absolute; left:-9px; top:-176px; width:807px; height:42px; z-index:10; border:0pt black solid;">
  <script language="JavaScript"  src="../Menu/menu.js" type="text/javascript"></script>
  <script language="JavaScript" src="../Menu/menu_items.js" type="text/javascript"></script>
  <script language="JavaScript" src="../Menu/menu_tpl.js" type="text/javascript"></script>
  <script language="JavaScript" type="text/javascript">
	new menu (MENU_ITEMS, MENU_POS);
	</script>
	<script language='JavaScript'  src='../utils.js'></script>
</div>
    <table width="496" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="181" class="celdalazul">Descripci&oacute;n del Descuento </td>
        <td width="264" class="celdalCELEST"><input name="descripcion" type="text" class="seleccionTurist_250" id="descripcion" maxlength="15" /></td>
      </tr>
      <tr>
        <td class="celdalazul">Proveedor</td>
        <td class="celdalCELEST"><label>
          <select name="proveedor" class="seleccionTurist_250" id="proveedor">
            <option value="0x">-Seleccionar-</option>
			<!--START BLOCK : PROVEEDOR -->
            <option value="{CODIGO_PROV}" {SELECCIONAR}>{NOMBRE_PROV}</option>
			<!--END BLOCK : PROVEEDOR -->
          </select>
          <img src="../Imagenes/configurar19x19.png" alt="Ingreso Proveedor" width="19" height="19" style="cursor:hand" onclick="Proveedor()" /></label></td>
      </tr>
      <tr>
        <td class="celdalazul">Precio</td>
        <td class="celdalCELEST"><label>
          <input name="precio" type="text" class="seleccionTurist_180" id="precio" maxlength="11"  onKeyPress="pres_num()" />
        </label></td>
      </tr>
      <tr>
        <td class="celdalazul">Cantidad de Descuentos </td>
        <td class="celdalCELEST"><label>
          <input name="cantidad" type="text" class="seleccionTurist_80" id="cantidad" maxlength="11" onkeypress="pres_num()" />
        </label></td>
      </tr>
      <tr>
        <td class="celdalazul">Tope del Descuento </td>
        <td class="celdalCELEST"><input name="tope" type="text" class="seleccionTurist_80" id="tope" maxlength="11" onkeypress="pres_num()" /></td>
      </tr>
      <tr>
        <td class="celdalazul">Porcentaje del Descuento </td>
        <td class="celdalCELEST"><input name="porcentaje" type="text" class="seleccionTurist_80" id="porcentaje" maxlength="11" onkeypress="pres_num()" onchange="valida()"  />
          <label>
          %
          <input name="guardar" type="hidden" id="guardar" />
        </label></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      
      
      <tr>
        <td><input name="Submit2" type="button" class="BotonTurist_Celest" value="Aceptar" onclick="Ver()" />
        &nbsp;</td>
        <td><label></label></td>
      </tr>
    </table>
  </form>
</div>



<div class="TituloPlomo" id="Layer3">
  <table width="345" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="82" rowspan="4"><label><img src="../Imagenes/descuento.jpg" width="81" height="80" /></label></td>
      <td width="13">&nbsp;</td>
      <td width="250">&nbsp;</td>
    </tr>
    
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloPlomo"><strong>Ingreso Descuento </strong></td>
    </tr>
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">{fecha}</td>
    </tr>
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
    
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
  </table>
</div>
</body>
</html>
