<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:16px;
	top:159px;
	width:823px;
	height:231px;
	z-index:13;
}
#Layer3 {
	position:absolute;
	left:18px;
	top:60px;
	width:314px;
	height:62px;
	z-index:14;
}
-->
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Ingreso Usuario</title>
<style type="text/css">
<!--
#Layer2 {	position:absolute;
	left:831px;
	top:53px;
	width:132px;
	height:116px;
	z-index:1;
}
-->
</style>
<script type="text/JavaScript">

function SubmitFormulario()
{
document.form1.submit();
}

function pres_num()
	{
		if((event.keyCode<48) || (event.keyCode>57))
			{
				event.returnValue = false;
			}		
	}
	
function validar()
	{
    	// Primer paso: Obtener el rut que ingreso el usuario
   		 var rutCompleto = document.form1.rutusuario.value;
		// Eliminamos los caracteres raros, espacios, puntos, guiones.
    	// Pasamos a minusculas, y separamos el rut y el digito verificador
    	rutCompl = rutCompleto.toLowerCase();
   		var dv=document.form1.dvusuario.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
   		 valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   		 valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
        		alert("El Rut no es Valido.Ingreselo Nuevamente");
        		document.form1.rutusuario.value="";
				document.form1.dvusuario.value="";
				document.form1.rutusuario.focus();
   			 }
	}

function verificarDV(rut, dv)
	{
   		var multiplicador = 9;
    	var aux = rut;
    	var suma = 0;
    	while(aux > 0)
			{
        		var unidades = aux % 10 ;
        		aux = (aux - unidades) / 10;
        		suma += (multiplicador * unidades);
        		multiplicador--;
        		if (multiplicador < 4)
					{
            			multiplicador = 9;
       				 }
    		}
    	digito = suma % 11;
    	if (digito == 10)
			{
        		digito = "k";
    		}
    	if (dv == digito)
			{
        		return true;
    		}
    	else 
			{
       			 return false;
    		}
	} 

function isEmailAddress(email)
	{
		var s = email;
		var filter=/^[A-Za-z][A-Za-z0-9_]*@[A-Za-z0-9_]+\.[A-Za-z0-9_.]+[A-za-z]$/;
		if (s.length == 0 ) return true;
		if (filter.test(s))
		return true;
		else
		alert("Ingrese una direcci�n de correo v�lida");
		document.form1.emailusuario.focus();
		return false;
	}
function ModificarTipoUsuario()
	{
		miPopup = window.open("../PHP/ListaTipoUsuario.php?condicion=1","tipousuario","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
		
function ModificarDistribuidor()
	{
		miPopup = window.open("../PHP/Lista_Distribuidor.php","cue","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
function ModificarUsuarioSistema()
	{
		miPopup = window.open("../PHP/Lista_Usuario_Sistema.php","cue","top=200,left=400,width=350,height=200,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}	
function pres_espacio()
	{
		if(event.keyCode==32)
			{
				event.returnValue = false;
				return;
			}
		
		event.returnValue = true;
	}		
	
	
function BuscarUsuario()
	{
		//alert(document.form1.rutusuario.value);
		if (document.form1.rutusuario.value=='' && document.form1.dvusuario.value=='')
			{
				alert("Debe Ingresar el Rut y el digito verificador para realizar la busqueda");
				document.form1.rutusuario.focus();
				return;
			}
		// Primer paso: Obtener el rut que ingreso el usuario
   		 var rutCompleto = document.form1.rutusuario.value;
		// Eliminamos los caracteres raros, espacios, puntos, guiones.
    	// Pasamos a minusculas, y separamos el rut y el digito verificador
    	rutCompl = rutCompleto.toLowerCase();
   		var dv=document.form1.dvusuario.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
   		 valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   		 valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
        		alert("El Rut no es Valido.Ingreselo Nuevamente");
        		document.form1.rutusuario.value="";
				document.form1.dvusuario.value="";
				document.form1.rutusuario.focus();
   			 }
		document.form1.buscar.value=1;
		document.form1.submit();
	}
/*function ModificarUsuario()
	{
		if (document.form1.rutusuario.value=='')
			{
				alert("Debe Ingresar el Rut del Usuario que desea Modificar");
				document.form1.rutusuario.focus();
				return;
			}
		if (document.form1.dvusuario.value=='')
			{
				alert("Debe Ingresar el Digito Verificador del Rut del usuario que desea modificar");
				document.form1.dvusuario.focus();
				return;
			}
			
		if (document.form1.usuario.value=='')
			{
				alert("Debe Ingresar el Usuario que desea Modificar");
				document.form1.usuario.focus();
				return;
			}
		if (document.form1.password.value=='')
			{
				alert("Debe Ingresar la Contrase�a para el usuario que desea Modificar");
				document.form1.password.focus();
				return;
			}
		
		
		if (document.form1.tipo_usuario.value=='')
			{
				alert("Debe Ingresar el Tipo de Usuario que desea Modificar");
				document.form1.tipo_usuario.focus();
				return;
			}
		if (document.form1.nombreusuario.value=='')
			{
				alert("Debe Ingresar el Nombre");
				document.form1.nombreusuario.focus();
				return;
			}
		if (document.form1.apellido.value=='')
			{
				alert("Debe Ingresar el Apellido");
				document.form1.apellido.focus();
				return;
			}
		
		if (document.form1.direccion.value=='')
			{
				alert("Debe Ingresar la Direcci�n del usuario que desea modificar");
				document.form1.direccion.focus();
				return;
			}
		if (document.form1.comuna.value=='')
			{
				alert("Debe Ingresar la Comuna a la cual pertenece el usuario");
				document.form1.comuna.focus();
				return;
			}
		if (document.form1.ciudad.value=='')
			{
				alert("Debe Ingresar la Ciudad a la cual pertenece el usuario");
				document.form1.ciudad.focus();
				return;
			}
		if (document.form1.distribuidor.value=='')
			{
				alert("Debe Ingresar el Distribuidor");
				document.form1.distribuidor.focus();
				return;
			}
		if (document.form1.usuariosistema.value=='')
			{
				alert("Debe Ingresar si Usuario del Sistema");
				document.form1.usuariosistema.focus();
				return;
			}
		if (document.form1.cuentacorrinte.value=='')
			{
				alert("Debe Ingresar Cuenta Corriente para el usuario");
				document.form1.cuentacorrinte.focus();
				return;
			}
		if (document.form1.cuentacontable.value=='')
			{
				alert("Debe Ingresar Cuenta Contable para el usuario");
				document.form1.cuentacontable.focus();
				return;
			}
		if (document.form1.banco.value=='')
			{
				alert("Debe Ingresar el Banco");
				document.form1.banco.focus();
				return;
			}
		if (document.form1.estado.value=='')
			{
				alert("Debe Ingresar el Estado del Usuario que desea Modificar");
				document.form1.estado.focus();
				return;
			}
		
		if (document.form1.VALIDACION.value==1)
			{
				if (document.form1.codigomanager.value=='')
					{
						alert("Debe Ingresar el Codigo Manager para este tipo de Usuario");
						document.form1.codigomanager.focus();
						return;
					}
			}
    	var rutCompleto = document.form1.rutusuario.value;
		rutCompl = rutCompleto.toLowerCase();
    	var dv=document.form1.dvusuario.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
    	valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   	 	valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
       	 		alert("El Rut no es Valido.Ingreselo Nuevamente");
        		document.form1.rutusuario.value="";
				document.form1.dvusuario.value="";
    		}
		
		document.form1.guardar.value="1";
		document.form1.submit();
	}*/
function ModificarEstado()
	{
		miPopup = window.open("../PHP/Lista_Estados.php","cue","top=200,left=400,width=350,height=200,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
function CuentaContable()
	{
		miPopup = window.open("../PHP/Lista_CuentaContable.php","cuentacontable","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
function Limpiar()
	{
		document.form1.rutusuariovalue="";
		document.form1.dvusuario.value="";
		document.form1.tipousuario.value="0";
		document.form1.tipocuenta.value="0";
		document.form1.usuario.value="";
		document.form1.passwordusuario.value="";
		document.form1.nombreusuario.value="";
		document.form1.apellido.value="";
		document.form1.direccion.value="";
		document.form1.telefono.value="";
		document.form1.comuna.value="";
		document.form1.ciudad.value="";
		//document.form1.contactodistribuidor.value="";
		document.form1.distribuidor.value="0";
		document.form1.cuentacorriente.value="";
		document.form1.email.value="";
		document.form1.celular.value="";
	//	document.form1.usuariosistema.value="0";
	}
function ModificarBanco()
	{
		miPopup = window.open("../PHP/ListaBancos.php","bancos","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();

	}
function ModificarUsuario(CODIGO)
	{
		var nombreenviado=document.form1.VARIABLE.value;
		var apellidoenviado=document.form1.VARIABLE2.value;
		var rut=document.form1.VARIABLE3.value;
		var dv=document.form1.VARIABLE4.value;
		var tipousuario=document.form1.VARIABLE5.value;
		//alert(nombreenviado);
		miPopup = window.open("../PHP/Modificar_Usuario.php?CODIGO="+CODIGO+"&nombreenviado="+nombreenviado+"&apellidoenviado="+apellidoenviado+"&rut="+rut+"&dv="+dv+"&tipousuario="+tipousuario,"modificarusuarioo","top=200,left=400,width=900,height=500,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
	
function pres_comillas()
	{
		if(event.keyCode==39)
			{
				event.returnValue = false;
				return;
			}
		if(event.keyCode==34)
			{
				event.returnValue = false;
				return;
			}
		event.returnValue = true;
	}
	
function pres_num()
	{
		if((event.keyCode<48) || (event.keyCode>57))
			{
				event.returnValue = false;
			}		
	}
	
function validar()
	{
    	// Primer paso: Obtener el rut que ingreso el usuario
   		 var rutCompleto = document.form1.rut.value;
		// Eliminamos los caracteres raros, espacios, puntos, guiones.
    	// Pasamos a minusculas, y separamos el rut y el digito verificador
    	rutCompl = rutCompleto.toLowerCase();
   		var dv=document.form1.dv.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
   		 valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   		 valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
        		alert("El Rut no es Valido.Ingreselo Nuevamente");
        		document.form1.rut.value="";
				document.form1.dv.value="";
				document.form1.rut.focus();
   			 }
	}

function verificarDV(rut, dv)
	{
   		var multiplicador = 9;
    	var aux = rut;
    	var suma = 0;
    	while(aux > 0)
			{
        		var unidades = aux % 10 ;
        		aux = (aux - unidades) / 10;
        		suma += (multiplicador * unidades);
        		multiplicador--;
        		if (multiplicador < 4)
					{
            			multiplicador = 9;
       				 }
    		}
    	digito = suma % 11;
    	if (digito == 10)
			{
        		digito = "k";
    		}
    	if (dv == digito)
			{
        		return true;
    		}
    	else 
			{
       			 return false;
    		}
	} 
	
function Consultar()
	{
		document.form1.ver.value=1;
		document.form1.submit();
	}
function Paginador_Uno(dato,dato1)
	{	
//	alert(dato);
//	alert(dato1);
		document.form1.pag.value = dato;
		document.form1.ver.value = dato1;
		form1.submit();
	}

function PaginadorUno(dato,dato1,dato2)
	{	
		document.form1.pag.value = dato;
		document.form1.ver.value = dato1;
		document.form1.nombre.value = dato2;
		form1.submit();
	}
function PaginadorDos(dato,dato1,dato2,dato3)
	{	
		document.form1.pag.value = dato;
		document.form1.ver.value = dato1;
		document.form1.nombre.value = dato2;
		document.form1.apellido.value = dato3;
		form1.submit();
	}
function PaginadorTres(dato,dato1,dato2,dato3,dato4,dato5)
	{	
		document.form1.pag.value = dato;
		document.form1.ver.value = dato1;
		document.form1.nombre.value = dato2;
		document.form1.apellido.value = dato3;
		document.form1.rut.value = dato4;
		document.form1.dv.value = dato5;
		form1.submit();
	}
function PaginadorCuatro(dato,dato1,dato2,dato3,dato4,dato5,dato6)
	{	
		document.form1.pag.value = dato;
		document.form1.ver.value = dato1;
		document.form1.nombre.value = dato2;
		document.form1.apellido.value = dato3;
		document.form1.rut.value = dato4;
		document.form1.dv.value = dato5;
		document.form1.tipo_usuario.value = dato6;
		form1.submit();
	}
	
function PaginadorCinco(dato,dato1,dato2)
	{	
		document.form1.pag.value = dato;
		document.form1.ver.value = dato1;
		document.form1.apellido.value = dato2;
		form1.submit();
	}
function PaginadorSeis(dato,dato1,dato2,dato3,dato4)
	{	
		document.form1.pag.value = dato;
		document.form1.ver.value = dato1;
		document.form1.apellido.value = dato2;
		document.form1.rut.value = dato3;
		document.form1.dv.value = dato4;
		form1.submit();
	}
function PaginadorSiete(dato,dato1,dato2,dato3,dato4,dato5)
	{	
		document.form1.pag.value = dato;
		document.form1.ver.value = dato1;
		document.form1.apellido.value = dato2;
		document.form1.rut.value = dato3;
		document.form1.dv.value = dato4;
		document.form1.tipo_usuario.value = dato5;
		form1.submit();
	}
function PaginadorOcho(dato,dato1,dato2,dato3)
	{	
		document.form1.pag.value = dato;
		document.form1.ver.value = dato1;
		document.form1.rut.value = dato2;
		document.form1.dv.value = dato3;
		form1.submit();
	}
function PaginadorNueve(dato,dato1,dato2,dato3,dato4)
	{	
		document.form1.pag.value = dato;
		document.form1.ver.value = dato1;
		document.form1.rut.value = dato2;
		document.form1.dv.value = dato3;
		document.form1.tipo_usuario.value = dato4;
		form1.submit();
	}
function PaginadorDiez(dato,dato1,dato2)
	{	
		document.form1.pag.value = dato;
		document.form1.ver.value = dato1;
		document.form1.tipo_usuario.value = dato2;
		form1.submit();
	}
function PaginadorDiezUno(dato,dato1,dato2,dato3)
	{	
		document.form1.pag.value = dato;
		document.form1.ver.value = dato1;
		document.form1.nombre.value = dato2;
		document.form1.tipo_usuario.value = dato3;
		form1.submit();
	}
function PaginadorDiezDos(dato,dato1,dato2,dato3,dato4,dato5)
	{	
		document.form1.pag.value = dato;
		document.form1.ver.value = dato1;
		document.form1.nombre.value = dato2;
		document.form1.rut.value = dato3;
		document.form1.dv.value = dato4;
		document.form1.tipo_usuario.value = dato5;
		form1.submit();
	}
function PaginadorDiezTres(dato,dato1,dato2,dato3)
	{	
		document.form1.pag.value = dato;
		document.form1.ver.value = dato1;
		document.form1.apellido.value = dato2;
		document.form1.tipo_usuario.value = dato3;
		form1.submit();
	}

</script>
</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<form id="form1" name="form1" method="post" action="">
<div id="etiqutaprimera" style="position:absolute; left:19px; top:146px; width:944px; height:52px; z-index:3; border:0pt black solid;">
 
<table width="777" border="0" cellspacing="0">
  <tr>
    <td width="103" class="celdalazul">Nombre Pasajero </td>
    <td width="256" class="celdalCELEST"><label>
      <input name="nombre" type="text" class="seleccionTurist_250" id="nombre" onKeyPress="pres_comillas()" value="{nombre}" maxlength="50"/>
    </label></td>
    <td width="108" class="celdalazul"><span class="celdalazul">Apellido Pasajero</span></td>
    <td width="458" class="celdalCELEST"><input name="apellido" type="text" class="seleccionTurist_250" id="apellido" onKeyPress="pres_comillas()" value="{apellido}" maxlength="50" /></td>
  </tr>
  <tr>
    <td class="celdalazul">Voucher Pasajero  </td>
    <td class="celdalCELEST" colspan="4"><label>
      <input name="rut" type="text" class="seleccionTurist_180" id="rut" onkeypress="pres_num();" value="{rut}"/>
     </label></td>  
  </tr>
  <tr>
    <td colspan="4" class="celdalCELEST" > 
	<center>        
      <input name="Submit" type="button" class="BotonTurist_Celest" value="Consultar" onclick="Consultar()" /></td>
	</center>
    </tr>
</table>
</div>
<div id="etiquetasegunda" style="position:absolute; left:20px; top:258px; width:943px; height:268px; z-index:3; border:0pt black solid; overflow:auto">
  <table width="943" border="0" cellspacing="0">
    <tr>
      <td width="299" class="celdalazul">Usuario</td>
      <td width="183" class="celdalazul">Rut </td>
      <td width="133" class="celdalazul">Operador</td>
      <td width="155" class="celdalazul">Tipo</td>
      <td width="117" class="celdalazul">Cod.Vendedor</td>
      <td width="44" class="celdalazul">&nbsp;</td>
      </tr>
	    <!-- START BLOCK : usuarios -->
    <tr>
      <td {CLASES} width="299">&nbsp;{NOMBRE}&nbsp;{APELLIDO}</td>
      <td {CLASES} width="183">&nbsp;&nbsp;{RUT}-{DV}</td>
      <td {CLASES}>&nbsp;{OPERADOR}</td>
      <td {CLASES} width="155">&nbsp;{TIPO}</td>
      <td {CLASES} width="117">{VEND}&nbsp;</td>
      <td {CLASES}><img src="../Imagenes/modify.gif" width="16" height="16"  alt="Modificar Usuario" style="cursor:hand" onclick="ModificarUsuario({CODIGO})"/></td>
      </tr>
	   <!-- END BLOCK : usuarios -->
  </table>
 </div>

  
<div class="TituloPlomo" id="Layer3">
  <table width="267" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="83" rowspan="4"><img src="../Imagenes/Crystal_Clear_app_Login_Manager80x80_marco.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="171"><p>&nbsp;</p>      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong>Consulta Usuario  </strong><strong></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha">{fecha}</span></td>
    </tr>
    
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
  </table>
</div>
<div style="position:absolute; left:21px; top:514px; width:948px; height:38px; z-index:3; border:0pt black solid;">
  <table width="941" border="0" cellspacing="0">
    <tr>
      <!-- START BLOCK : tablageneral -->
      <td width="117" class="celdalazul">Total</td>
      <td width="820" class="celdalCELEST"><strong>{paginador}&nbsp; </strong></td>
      <!-- END BLOCK : tablageneral -->
    </tr>
  </table>
  <input name="pag" type="hidden" value="" />
  <input type="hidden" name="VARIABLE" value="{VARIABLENOMBRE}" />
  <input type="hidden" name="VARIABLE2" value="{VARIABLEAPELLIDO}" />
  <input type="hidden" name="VARIABLE3" value="{VARIABLERUT}" />
  <input type="hidden" name="VARIABLE4" value="{VARIABLEDV}" />
   <input type="hidden" name="VARIABLE5" value="{VARIABLETIPOUSUARIO}" />
</div>
</form>


</body>
</html>

