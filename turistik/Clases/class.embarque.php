<?
//ini_set('display_errors', 1);
Class Embarque
{
    var $tipo_re;
    var $embarque;
    var $observacion;
    var $respuesta;
    
    public function __construct($tipoReserva,$estado_embarque,$observacion)
    {
        $this->tipo_re = $tipoReserva;
        $this->embarque = $estado_embarque;
        $this->observacion = $observacion;
        

        
        if ($this->tipo_re == 2 || $this->tipo_re==4)
        {
            if ($this->embarque == 1)
            {
                //Mostrar estado embarque
                // Embarcado 1.1
                // No Show  1.2
                
                    $resp = 1;
                    $this->respuesta = $resp;
            }
            else
            {
                 //Mostrar estado embarque
                // Pendiente
                // 1.3 , 1.4          
                    $resp = 2;
                    $this->respuesta = $resp;

            }
        }
        else
        {
   
            if ($this->embarque == 1)
            {
                //Mostrar estado embarque
                // Embarcado 1.5
                // No show 1.51
                    $resp = 1;
                    $this->respuesta = $resp;

            }
            else
            {
                if ($this->embarque == 2)
                {
                    // Mostrar estado embarque
                    // Pendiente 1.6
          
                    $resp = 2;
                    $this->respuesta = $resp;
             
    
                }
                else
                {
                    
                    //Busca en Obs.Contable "cobro no show"
                    if ($this->observacion != "COBRO NO SHOW RA")
                    {
                        // Mostrar estado embarque
                        // Pendiente 1.7 
                        $resp = 2;
                        $this->respuesta = $resp;
                    }
                    else
                    {
                    
                        $resp = 3;
                        $this->respuesta = $resp;
                        
                    }
                    
                }
            }

 
        }

    }
    
    public function modificar_accion_embarque($muestra_accion,$estado_emb,$cod_cierre,$tipo_re,$boleta_re,$codigo,$comision,$observacion)
    {
        
        

        
        if ($muestra_accion == 1 && $estado_emb == 3)
        {
 
       
            if ($tipo_re == 2)
            {
               // echo " Modifica estado emb = 3
               // modifica cod_cierre = 4 ";
                Actualiza_Estado_Embarque($codigo,3,1,$observacion);
                Actualiza_Cod_Cierre($codigo,4);
                return 1;
                
            }
            else
            {

                if($cod_cierre == 2)
                {
                  //  echo "1.51 Modifica  estado emb 3";
                    Actualiza_Estado_Embarque($codigo,3,1,$observacion);
                    return 2;
                }
                else
                {
                    /*
                    echo "Modifica Tipo_re = 1
                     modifica comision = 0
                     comisionado = 50%
                     estado emb 3 "; 
                     */
                    $miConexion= new ClaseConexion;
                    $miConexion->Conectar();
                    $query3=$miConexion->EjecutaSP("Consulta_reserva_by_codigo"," ".$codigo." ");
                    while ($row3 = mysql_fetch_array($query3))
                    {
                        $precio_comision = $row3['COMISION_RE']/2;
                        $usuario = $row3['COD_USUA'];
                    }  
                    
         
        
                     
                    $Obj_Tkpass = new tkpass;
                    $linea = $Obj_Tkpass->Consulta_linea_credito_Admin($usuario);
                    $linea_tope = $linea[0];
                    

                    
                    if ($linea_tope != "")
                    {
                        $linea_actualizar = $linea[1]+$precio_comision;
                        $Obj_Tkpass->Actualiza_linea_credito_Admin($usuario, $linea_actualizar);     
                    }

                     
      
                     Actualiza_Estado_Embarque($codigo,3,1,$observacion);
                     
                     
                    // echo $comision;
                     Actualiza_Embarque_RA($codigo,3,$comision,$observacion);
                     
                     Actualiza_Cod_Cierre($codigo,2);
                     
                     return 3;
                 
                }
                

            }
        }
        
        if ($muestra_accion == 1 && $estado_emb == 2)
        {
       
            //echo " Modifica estado emba = 2";
            Actualiza_Estado_Embarque($codigo,2,1,'');
            return 4;

        }
        
        if ($muestra_accion == 2)
        {
            
            if ($cod_cierre == 4)
            {
                if ($boleta_re == '')
                {
                  //  echo " modifica estado emb = 1
                    // mod cod cierre = 1";
                    Actualiza_Estado_Embarque($codigo,1,1,$observacion);
                    Actualiza_Cod_Cierre($codigo,1);
                    return 5;
                }
                else
                {
                    // echo " modifica estado emb = 1
                    // mod cod cierre = 2";
                    Actualiza_Estado_Embarque($codigo,1,1,$observacion);
                    Actualiza_Cod_Cierre($codigo,2);
                    return 6;
                }
            }
            else
            {
               // echo " Modificar estado emb 1";
                Actualiza_Estado_Embarque($codigo,1,1,'');
                return 7;
            }
        }
        
        # se ejecuta el script cuando la reserva  RA esta no show y aqui es donde puede volver a estar pendiente
        # se elimina el registro de cartola y se actualiza los valores de comision en reserva
        if ($muestra_accion == 3)
        {
            
            //Actualiza_Estado_Embarque($codigo,1,1,$observacion);

            $miConexion= new ClaseConexion;
            $miConexion->Conectar();
            $query3=$miConexion->EjecutaSP("Consulta_reserva_by_codigo"," ".$codigo." ");
            while ($row3 = mysql_fetch_array($query3))
            {
                $precio = $row3['PRECIO_RE'];
                $precio_comisionado_re = $row3['PRECIO_COMISIONADO_RE'];
                $factura = $row3['FACTURA_RE'];
                $usuario = $row3['COD_USUA'];
            }
            
            if ($factura == '')
            {
                
                $Obj_Tkpass = new tkpass;
                $linea = $Obj_Tkpass->Consulta_linea_credito_Admin($usuario);
                $linea_tope = $linea[0];
                $linea_actualizar = $linea[1]-$precio_comisionado_re;
                
                
                $Obj_Tkpass->Actualiza_linea_credito_Admin($usuario, $linea_actualizar);

                
            
                $comision = $precio_comisionado_re * 2 ;
                $precio_comisionado_re = $precio - $comision;
                $tipo_re = 3;
                $observacion_contable = '';
                $estado=1;
                
                //Consulta_linea_credito_Admin
                
                
                $miConexion->Conectar();
                $query=$miConexion->EjecutaSP("Actualiza_Cobro_RA"," ".$tipo_re.", ".$precio_comisionado_re.",".$comision.", '".$observacion_contable."',".$estado.", '".$_SESSION['usuario']."', '".$observacion."',".$codigo." ");
            
            
                Actualiza_Cod_Cierre($codigo,1);
                Elimina_registro_cartola($codigo);
                
                return 8;
                
            }
            else
            {
                return 9;
            }
        }
        

    }
    

}

    
    function Actualiza_Estado_Embarque($codigo,$estado,$accion,$observacion)
    {
        
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaSP("Modificar_Estado_Embarque"," ".$codigo.",".$estado.",'".$_SESSION['usuario']."','".$observacion."',@cod, @msg ");
        mysql_free_result($query); 
        mysql_close(); 
        
    }
    
    function Actualiza_Cod_Cierre($codigo,$estado)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaSP("Actualiza_Cod_cierre_Original"," ".$codigo.", ".$estado." ");
        mysql_free_result($query); 
        mysql_close(); 
    }
    
    function Elimina_registro_cartola($codigo)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("DELETE FROM PREPAGOS WHERE COD_RE = $codigo ");
        mysql_free_result($query); 
        mysql_close();   
    }
    
    
    function Actualiza_Embarque_RA($codigo,$estado,$comision,$observacion)
    {
        $tipo_re = 3;
        $precio_comisionado_re = $comision/2;
        $observacion_contable = 'COBRO NO SHOW RA';
        $comision = 0;
        $fecha = date('Y-m-d H:i:s');
        
        //$boarding_pass = '131227011204564';
        //$codigo_usuario = '1845';
        
        $miConexion= new ClaseConexion;
        
        $miConexion->Conectar();
        $query3=$miConexion->EjecutaSP("Consulta_reserva_by_codigo"," ".$codigo." ");
         while ($row3 = mysql_fetch_array($query3))
        {
            $codigo_usuario = $row3['COD_USUA'];
            $boarding_pass = $row3['VOUCHER_RE'];
        }
        

        
        $miConexion->Conectar();
        $query=$miConexion->EjecutaSP("Actualiza_Cobro_RA"," ".$tipo_re.", ".$precio_comisionado_re.",".$comision.", '".$observacion_contable."',".$estado.", '".$_SESSION['usuario']."', '".$observacion."',".$codigo." ");
        
        

        $miConexion->Conectar();
        $query2=$miConexion->EjecutaSP("Inserta_Prepago"," ".$codigo_usuario.", ".$precio_comisionado_re.", '', '', '".$fecha."',7,'','','','','', '".$boarding_pass."',".$codigo." ");
        
        mysql_free_result($query);
        mysql_free_result($query2);
        mysql_free_result($query3);
         
        
        
        mysql_close();
    }
?>