<?php
$lista_documentos[] = 
"<table align='center'>
        <tr>
                <td class='celda_bordes'>
                       <table align='center' bgcolor='#FFFFFF'>
                		  <tr>
                			<td align='center'>$logo</td>
                		  </tr>
                		  <tr>
                			<td align='center' class='celda_titulo'><b>$idioma->titulo</b></td>
                		  </tr>
                		  <tr>
                			<td align='center' class='celda_titulo'>$idioma->subtitulo</td>
                		  </tr>
                		  <tr>
                			<td align='center' class='celda_titulo'>$voucher</td>
                		  </tr>
                		  <tr>
                			<td align='center' class='celda_titulo'>$CodeQR</td>
                		  </tr>
                            <tr>
                                <td><hr>
                                     <table class='celda_dos'>
                                		  <tr>
                                			<td class='celda'  >$idioma->const_nombre</td>
                                			<td class='celda'>$pasajero</td>
                                		  </tr>
                                		  <tr>
                                			<td class='celda'>$idioma->const_servicio</td>
                                			<td class='celda'>$n_excursion</td>
                                		  </tr>
                                		  <tr>
                                			<td class='celda'>$idioma->const_tipoPax:</td>
                                			<td class='celda'>$tipos_de_pasajeros</td>
                                		  </tr>
                                		  <tr>
                                			<td class='celda'>$idioma->const_fecha</td>
                                			<td class='celda'>$fecha</td>
                                		  </tr>
                                		  <tr>
                                			<td class='celda'>$idioma->cons_lugar</td>
                                			<td class='celda'>$lugares</td>
                                		  </tr>
                                		  <tr>
                                			<td class='celda'>$idioma->const_horario</td>
                                			<td class='celda'>$minutos_recogida</td>
                                		  </tr>
                                                              		  <tr>
                                			<td class='celda'>$idioma->const_total</td>
                                			<td class='celda'>".number_format($idioma->precio_total)." PESOS CHILENOS</td>
                                		  </tr>
                                		  <tr>
                                			<td class='celda'>$idioma->const_fop</td>
                                			<td class='celda'>$idioma->forma_pago</td>
                                		  </tr>
                                		  <tr>
                                			<td class='celda'>$idioma->pendiente</td>
                                			<td class='celda'>$idioma->precio_pendiente</td>
                                		  </tr>
                                		  <tr>
                                			<td class='celda'>$idioma->const_descuentos</td>
                                			<td class='celda'>$idioma->descuentos_aplicados</td>
                                		  </tr>
                                		  <tr>
                                			<td class='celda' colspan='2'>$titulo_promo</td>
                                		  </tr>

                                        </table>
                                        <table>
                                        	<tr>
                                                <hr>
                                            	<td align='justify' class='celda_peque'>$condiciones</td>
                                             
                                             </tr>
                                             <br><br><br>
                                             <tr>
                                            	<td align='left' class='celda_peque'>$usuario_imprime</td>
                                             </tr>
                               	        </table>
                                </td>
                            </tr>
                       </table>
                </td>
        </tr>
</table>";
?>