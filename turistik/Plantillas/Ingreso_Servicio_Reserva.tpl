<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Ingreso de Horario</title>
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<script language='JavaScript'  src='../utils.js'></script>
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:20px;
	top:11px;
	width:258px;
	height:181px;
	z-index:1;
}
-->
</style>
<script type="text/JavaScript">
function pres_num()
{
	if(event.keyCode!=44)
	{
		if((event.keyCode<48) || (event.keyCode>57))
		{
		if(event.keyCode!=39)
		{
		event.returnValue = true;
		}
		else
		{
				event.returnValue = false;
		}		
			}		
	}
}	


/*
function pres_puntos()
	{
	alert(event.keyCode);
		if(event.keyCode==39)
			{
				event.returnValue = false;
				return;
			}
		if(event.keyCode==34)
			{
				event.returnValue = false;
				return;
			}
		event.returnValue = true;
		*/



function Aceptar()
	{
		if(document.form1.horario.value=='')
			{
				alert('Ingrese el Horario en que se entrega el servicio');
				document.form1.horario.focus();
				return;
			}
		if(document.form1.reserva.value=='')
			{
				alert('Ingrese la cantidad de cupos de reservas');
				document.form1.reserva.focus();
				return;
			}
		document.form1.guardar.value='1';
		opener.document.form1.submit();
		document.form1.submit();
		opener.document.form1.submit();
	}

function Salir()
{
window.close();
opener.document.form1.submit();
}

function CheckTime(str,valor)
	{
		hora=str.value
		if (hora=='') 
			{
				return
			}
		if (hora.length>5) 
			{
				alert("Introdujo una cadena no valida, mayor a 5 caracteres");
				document.form1.horario.value='';
				str.focus();
				return;
				
			}
		if (hora.length!=5) 
			{
				alert("Introducir HH:MM");
				document.form1.horario.value='';
				document.form1.focus();
				str.focus();
				return;
			}
		a=hora.charAt(0) //<=2
		b=hora.charAt(1) //<4
		c=hora.charAt(2) //:
		d=hora.charAt(3) //<=5
		if ((a==2 && b>3) || (a>2)) 
			{
				alert("El valor que introdujo en la Hora no corresponde, introduzca un digito entre 00 y 23");
				document.form1.horario.value='';
				str.focus();
				return;
			}
		if (d>5) 
			{
				alert("El valor que introdujo en los minutos no corresponde, introduzca un digito entre 00 y 59");
				document.form1.horario.value='';
				str.focus();
				return;
				
			}
		if (c!=':') 
			{
				alert("Introduzca el caracter ':'");
				document.form1.horario.value='';
				str.focus();
				return;
				
			}
	}
	

</script>

</head>

<body>
<form id="form1" name="form1" method="post" action="">
<div id="Layer1">

  <table width="255" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" class="TituloPlomo"><strong> Horario del Servicio </strong></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td width="97" class="celdalazul">D&iacute;a</td>
      <td width="158" class="celdalCELEST"><label>{semana}
          <input name="cod_semana" type="hidden" id="cod_semana" value="{cod_semana}" />
          <input name="codigo" type="hidden" id="codigo" value="{codigo}" />
      </label></td>
    </tr>
    <tr>
      <td class="celdalazul">Horario</td>
      <td class="celdalCELEST"><label>
        <input name="horario" type="text" class="seleccionTurist_90" id="horario" value="{HORARIO}"  maxlength="5" onblur="CheckTime(this)" onkeypress="pres_num()" />
      HH:MM</label></td>
    </tr>
    <tr>
      <td class="celdalazul">N&ordm; Reservas </td>
      <td class="celdalCELEST"><label>
        <input name="reserva" type="text" class="seleccionTurist_90" id="reserva" onkeypress="pres_num()" value="{RESERVA}" maxlength="4" />
        <input name="guardar" type="hidden" id="guardar" value="{GUARDAR}" />
      </label></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><label>
        <input name="Submit" type="button" class="BotonTurist_Celest" value="Aceptar" onclick="Aceptar()"/>
      </label></td>
      <td><label>
        <input name="Submit2" type="button" class="BotonTurist_Celest" value="Cerrar" onclick="Salir()" />
      </label></td>
    </tr>
  </table>
</div>

</form>
</body>
</html>
