 <?php

     class CAF extends stdClass {
          public $id;
          public $Rut;
          public $Tipo;
          public $RangoDesde;
          public $RangoHasta;
          public $FechaCAF;
          public $FechaGEN;
          public $Modulo;
          public $Exponente;
          public $IDK;
          public $FRMA;
          public $RSASK;
          public $DocActual;
          public $consumidos;
          public $token;
          public $url;
     }

   class CAFResult extends stdClass
   {
        public $token;
        public $url;
        function __construct()
        {
            include("config.php");
            $this->url = URL;
            $this->token = TOKEN;
      
        }
        function resultJSON(){
          $client = new SoapClient($this->url, array('encoding'=>'ISO-8859-1'));
          $data = $client->ListarCAF()->ListarCAFResult->CAF; 
          for ($i=0; $i < count($data); $i++) { 
              $d = new CAF;
              $d = $data[$i];
              $result[] = $d; 
          }
          return json_encode($result);
        }
        function result(){
          $client = new SoapClient($this->url, array('encoding'=>'ISO-8859-1'));
          $data = $client->ListarCAF()->ListarCAFResult->CAF; 
          for ($i=0; $i < count($data); $i++) { 
              $d = new CAF;
              $d = $data[$i];
              $result[] = $d; 
          }
          return json_encode($result);
        }
   }
?>