// Funcion para imprimir en IE 4 o superior y NN
// --------------------------------------------
function imprimir ()
{
	if (navigator.appName.substring(0,8) == 'Netscape')
	{
		if (parseInt(navigator.appVersion.substring(0,1)) > 3)
			window.print();
		else
			alert("Lo siento, pero esta opcion solo esta disponible en IE 4.0, NN 4.0 o superior.\nPara imprimir, pulse en la opcion [IMPRIMIR] del menu Archivo.");
	}
	else
	{
		// IE 5.0 or later
		if ((navigator.appVersion.indexOf(5)) != -1)
			//window.print();		
			if (true)
					{
						var OLECMDID_PRINT = 6;
				        var OLECMDEXECOPT_DONTPROMPTUSER = -1;
				        var OLECMDEXECOPT_PROMPTUSER = 1;
				        var WebBrowser = "<OBJECT ID=\"WebBrowser1\" WIDTH=0 HEIGHT=0 CLASSID=\"CLSID:8856F961-340A-11D0-A96B-00C04FD705A2\"></OBJECT>";
				        document.body.insertAdjacentHTML("beforeEnd", WebBrowser);
					WebBrowser1.ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER);
				        WebBrowser1.outerHTML = "";
					}			
		else
		{
			// IE 4.0 versions
			if (parseInt(navigator.appVersion.substring(0,1)) > 3)
			{
				// a�adir un confirm
				// Si se utiliza OLECMDEXECOPT_DONTPROMPTUSER y al dialogo de la impresora se 
				// le da cancelar se va en error y ya no saca el dialogo....
				
					if (confirm('�Seguro que deseas imprimir?'))
					{
					var OLECMDID_PRINT = 6;
				        var OLECMDEXECOPT_DONTPROMPTUSER = 2;
				        var OLECMDEXECOPT_PROMPTUSER = 1;
				        var WebBrowser = "<OBJECT ID=\"WebBrowser1\" WIDTH=0 HEIGHT=0 CLASSID=\"CLSID:8856F961-340A-11D0-A96B-00C04FD705A2\"></OBJECT>";
				        document.body.insertAdjacentHTML("beforeEnd", WebBrowser);
						WebBrowser1.ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER);
				        WebBrowser1.outerHTML = "";
					}
			}
			else
				alert("Lo siento, pero esta opcion solo esta disponible en IE 4.0, NN 4.0 o superior.\nTPara imprimir, pulse en la opcion [IMPRIMIR] del menu Archivo.");
		}
	}
}

// Comprobar Navegador
// ----------------------------------------------
function navegador ()
{
	var b = navigator.appName;
	if (b=="Netscape") this.b = "ns";
	else if (b=="Microsoft Internet Explorer") this.b = "ie";
	else this.b = b;
	this.v = parseInt(navigator.appVersion);
	this.ns = (this.b=="ns" && this.v>=4 && this.v<6);
	this.ns4 = (this.b=="ns" && this.v==4);
	this.ns6 = (this.b=="ns" && this.v==5);
	this.ie = (this.b=="ie" && this.v>=4);
	this.ie4 = (navigator.userAgent.indexOf('MSIE 4')>0);
	this.ie5 = (navigator.userAgent.indexOf('MSIE 5')>0);
}
is = new navegador();