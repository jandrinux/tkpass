<? 
require ("../Clases/ClaseConexion.inc.php");   
// NuSoap API 
    require_once('lib/nusoap.php'); 
  
        // create the server instance 
        $webServer = new soap_server(); 
        $webServer->soap_defencoding = 'UTF-8';
        $namespace = 'http://www.tkpass.cl/turistik/Webservice/ws_precios_funicular.php?wsdl'; 
        $webServer->configureWSDL('ws'); 
        $webServer->wsdl->schemaTargetNamespace = $namespace; 
                
        $webServer->wsdl->addComplexType(
        'Precios',
        'complexType',
        'struct',
        'all',
        '',
        array(
        		'Tipo_Pax' => array('name' => 'Tipo_Pax','type' => 'xsd:string'),
        		'Tramo' => array('name' => 'Tramo','type' => 'xsd:string'),
                'Precio' => array('name' => 'Precio','type' => 'xsd:string')
        )
        );

        $webServer->wsdl->addComplexType(
        'ListaPrecio',
        'complexType',
        'array',
        '',
        'SOAP-ENC:Array',
        array(),
        array
        (
        array(
             'ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:Precios[]'
        )
        ),
        'tns:Precios'
        );


        
        	//Model Webservice :: Funcion TestPrint
        $methodName = 'ActualizarPrecios'; 
        
        
        $input = array(); 
        
        
        $output = array('return' => 'tns:ListaPrecio'); 
        
        $soapAction = false; 
        $style = 'rpc'; 
        $use = 'encoded'; 
        $description = 'Actualizar precios funicular'; 
        $webServer->register($methodName, $input, $output, $namespace, $soapAction, $style, $use, $description);
        
        
        function ActualizarPrecios()
        {

        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta(" SELECT * FROM FUNICULAR");
            	while ($con = mysql_fetch_row($query))
				 {
					$tipo_pax = $con['TIPO_PAX'];
                    $precio  = $con['TRAMO'];
                    $tramo = $con['PRECIO'];
						
							
				$elements[] = array(					
							"Tipo_Pax" => $tipo_pax,
							"Tramo" => $tramo ,
                            "Precio" => $precio
                            
                            );		
			
				 }  
        mysql_free_result($query); 
        mysql_close();          
         
				return $elements;
        }
        
        
        $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
        $webServer->service($HTTP_RAW_POST_DATA); 
		