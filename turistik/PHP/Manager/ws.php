<?PHP
ini_set("display_errors",0);
error_reporting(E_ALL);
session_start();
class WS_Manager
{
    public $URLWS ="http://wss.imanager.cl:8081/manager/ws/erptest/ventas.asmx?wsdl"  ;
//"http://wss.imanager.cl:8081/manager/ws/erptest/ventas.asmx?wsdl";//"http://wss.imanager.cl:8081/manager/ws/erptest/ventas.asmx?wsdl"; //"http://wss.imanager.cl/manager/ws/erp/ventas.asmx?wsdl";
    public $RUT_EXENTO = "76033430-8";
    public $RUT_AFECTO = "76708000-k";

    public $PERSONAL = "TKP";
    public $NUMERO_DOCUMENTO = "123";
    public $FECHA = "10/12/2013";
    public $TIPOVENTA = "1";
    public $RUTCLIENTE = "";
    public $NUMOT = "0";
    public $CODIGOTECNICO = "0";
    public $CODIGOMONEDA = "1";
    public $CODIGOVENDEDOR = "0";
    public $COMISION = "0";
    public $TIPODESCUENTO = "1"; // 0: DESCUENTO EN DINERO 1: DESCUENTO EN PORCENTAJE
    public $DESCUENTO = "0"; // 25% DE  DESCUENTO GLOBAL EN LA VENTA

    public $CODIGOSUCURSAL = "98"; 
    public $REBAJASTOCK = "0"; // 0: NO REBAJA  1: REBAJA
    public $NULA = "0"; // 0: NO NULA 1: NULA
    public $ELECTRONICA = "1"; // 0: NO ES ELECTROCIA 1: ES ELECTRONCA
    public $FORMAPAGO = "Efectivo";
    public $CODIGOCUENTACONTABLE = "3101101";
    public $CODIGOCENTRODECOSTOS = "51";

    public $GLOSACONTABLE = "''";
    public $NUMCAJA = "0";
    public $TURNO = "0";
    public $OBS = "TKPASS";

    //detalle
    public $CODIGOPRODUCTO = "P001"; // HOPON MANAGER
    public $CANTIDAD = "2";
    public $CODIGOBODEGA = "B01"; // BODGA PRINCIPAL . SUCURSAL MATRIZ ,
    public $NUMSERIE = "";
    public $NUMITEMREF = "1"; // LINEA DE DETALLE DE DOCUMENTO (ID DETALLE)
    public $AFECTA = false;

    public function Cabecera_Boleta($Numero, $Fecha, $Fop, $Afecta, $descuento)
    {
	if(isset($_SESSION["codigo_suc"])) {
	 $this->CODIGOSUCURSAL = $_SESSION["codigo_suc"];
	}
        $exe = false;
        $client = new SoapClient($this->URLWS, array('cache_wsdl' => 0));
        $this->AFECTA = $Afecta;
        if ($Afecta) {

            $this->CODIGOCUENTACONTABLE = "3101108";
            $rut = $this->RUT_AFECTO;
            $this->CODIGOMONEDA = "$";
            $exe = 1;
        } else {
           
                $this->CODIGOMONEDA = "1";
                $this->CODIGOCUENTACONTABLE = "3101101";
                $rut = $this->RUT_EXENTO;
           $exe = 0;
            
        }

        if($Fop == "E")
        {
         $this->FORMAPAGO = "Efectivo";
        }
        else
        {
             $this->FORMAPAGO = "Tarjeta Credito";
        }
        $this->NUMERO_DOCUMENTO = $Numero;
        $this->FECHA = $Fecha;
        $this->DESCUENTO = $descuento;
        $xxx = array(

            "rutEmpresa" => $rut,
            "codigoPersonal" => $this->PERSONAL,
            "numDocumento" => $this->NUMERO_DOCUMENTO,
            "fecha" => $this->FECHA,
            "tipoVenta" => $this->TIPOVENTA,
            "rutCliente" => $this->RUTCLIENTE,
            "numOt" => $this->NUMOT,
            "codigoTecnico" => $this->CODIGOTECNICO,
            "codigoMoneda" => $this->CODIGOMONEDA,
            "codigoVendedor" => $this->CODIGOVENDEDOR,
            "comision" => $this->COMISION,
            "atencionA" => "",
            "descuentoTipo" => $this->TIPODESCUENTO,
            "descuento" => $this->DESCUENTO,
            "codigoSucursal" => $this->CODIGOSUCURSAL,
            "rebajaStock" => $this->REBAJASTOCK,
            "nula" => 0,
            "esElectronica" => $this->ELECTRONICA,
            "formaPago" => $this->FORMAPAGO,
            "codigoCtaCble" => $this->CODIGOCUENTACONTABLE,
            "codigoCentroCosto" => $this->CODIGOCENTRODECOSTOS,
            "glosaContable" => $this->GLOSACONTABLE,
            "numCaja" => $this->NUMCAJA,
            "turno" => $this->TURNO,
            "observaciones" => $this->OBS,
            "Pmexento"=> '',
	    "IndServicio" =>'3');//$exe);

       // print_r($xxx); exit();
        $data = $client->boletaVentaCabeceraIngresar($xxx);


        $rtn = $data->IngresaCabeceraBoletaDeVentaResult->transaccionResult;
        echo 'paso cabezera xx';
	print_r($rtn);
        return $rtn->Error;
    }

    public function Detalle_Boleta($producto, $cantidad, $descuento)
    {
        if ($this->AFECTA)
            $rut = $this->RUT_AFECTO;
        else
            $rut = $this->RUT_EXENTO;
        $this->FORMAPAGO = "Efectivo";
        $client = new SoapClient($this->URLWS, array('cache_wsdl' => 0));
        $data = $client->boletaVentaDetalleIngresar(array(
            "rutEmpresa" => $rut,
            "numDocumento" => $this->NUMERO_DOCUMENTO,
            "fecha" => $this->FECHA,
            "codigoProducto" => $producto,
            "cantidad" => $cantidad,
            "descuento" => $descuento,
            "numLote" => "",
            "codigoBodega" => "B01",
            "codigoCtaCble" => $this->CODIGOCUENTACONTABLE,
            "codigoCentroCosto" => $this->CODIGOCENTRODECOSTOS,
            "numSerie" => "12",
            "numItemRef" => 0,
            "codigoPersonal" => $this->PERSONAL, "IndServicio" =>'3' ));

        return $data->IngresaDetalleBoletaDeVentaResult->transaccionResult->Error;
    }

    public function Eliminar_Boleta($numero, $fecha, $afecta)
    {
        if ($this->AFECTA)
            $rut = $this->RUT_AFECTO;
        else
            $rut = $this->RUT_EXENTO;
        $client = new SoapClient($this->URLWS, array('cache_wsdl' => 0));
        $data = $client->boletaVentaCabeceraEliminar(array(
            "rutEmpresa" => $rut,
            "numDocumento" => $numero,
            "fecha" => $fecha));

        $data->EliminaCabeceraDeNotaDeVentaResponse;
    }


    public function Anular_Cabecera_Boleta($Numero, $Fecha, $Fop, $Afecta, $descuento)
    {
        $client = new SoapClient($this->URLWS, array('cache_wsdl' => 0));
        $this->AFECTA = $Afecta;
        if ($Afecta) {
            $this->CODIGOCUENTACONTABLE = "3101101";
            $rut = $this->RUT_AFECTO;
        } else {
            $this->CODIGOCUENTACONTABLE = "3101108";
            $rut = $this->RUT_EXENTO;
        }
        $this->FORMAPAGO = "Efectivo";

        $this->NUMERO_DOCUMENTO = $Numero;
        $this->FECHA = $Fecha;
        $this->DESCUENTO = $descuento;
        $data = $client->boletaVentaCabeceraActualizar(array(

            "rutEmpresa" => $rut,
            "codigoPersonal" => $this->PERSONAL,
            "numDocumento" => $this->NUMERO_DOCUMENTO,
            "fecha" => $this->FECHA,
            "tipoVenta" => $this->TIPOVENTA,
            "rutCliente" => $this->RUTCLIENTE,
            "numOt" => $this->NUMOT,
            "codigoTecnico" => $this->CODIGOTECNICO,
            "codigoMoneda" => $this->CODIGOMONEDA,
            "codigoVendedor" => $this->CODIGOVENDEDOR,
            "comision" => $this->COMISION,
            "atencionA" => "",
            "descuentoTipo" => $this->TIPODESCUENTO,
            "descuento" => $this->DESCUENTO,
            "codigoSucursal" => $this->CODIGOSUCURSAL,
            "rebajaStock" => $this->REBAJASTOCK,
            "nula" => 1,
            "esElectronica" => $this->ELECTRONICA,
            "formaPago" => $this->FORMAPAGO,
            "codigoCtaCble" => $this->CODIGOCUENTACONTABLE,
            "codigoCentroCosto" => $this->CODIGOCENTRODECOSTOS,
            "glosaContable" => $this->GLOSACONTABLE,
            "numCaja" => $this->NUMCAJA,
            "turno" => $this->TURNO,
            "observaciones" => $this->OBS));

        return $data->ActualizaCabeceraBoletaDeVentaResult->transaccionResult->Error;
    }
}
/*
$data = new WS_Manager();

if($data->Cabecera_Boleta("999999","20/01/2014","Efectivo",false,5)==0)
{
//detalle
if($data->Detalle_Boleta("P002",2,0)==0)
echo "guardo";
}
else
{
echo "Error al Guardar";
}
*/
?>
