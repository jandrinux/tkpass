<?php 
session_start();
require ("../Clases/ClaseConexion.inc.php");
require ("../Clases/ClaseUtil.inc.php");
?>
<html>
<head>
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script language="javascript">

function Enviar()
{

   var nombre = $("#nombre").val()
   var hotel = $("#hotel").val()
   var phone = $("#phone").val()
   var email = $("#email").val()
   var rut_concat = $("#rut_concat").val()
   
   if (nombre == '' || hotel == '0' || phone == '')
   {
      alert('Complete campos obligatorios (*)');
   }
   else
   {
    message.style.display = 'none';

    $("#msgbox").removeClass().html('<img src="../Imagenes/enviando.gif" width="400px"/>').fadeIn(1000);

        $.post("enviarSolicitudDist.php",
              { nombre:nombre, hotel:hotel, phone: phone, email:email, rut_concat:rut_concat  }
              ,function(data)
        {
          if (data == "si") {
            alert("Correo enviado exitosamente");
          }else
          {
            alert("Hubo un problema al enviar el correo.");
          }
            msgbox.style.display = 'none';
            message.style.display = '';
            limpiar();
            return false;
        })
   }
}

function limpiar()
{
   $("#nombre").val("")
   $("#hotel").val(0)
   $("#phone").val("")
   $("#email").val("")
   $("#rut_concat").val()
}

function devolverValor() {
    
    var tab = document.getElementById('tab').value;
    var of = document.getElementById('oferta').value;
    
    if (tab == 6 && of == 0 )
    {
        alert("Distribuidor sin acceso a Ofertas");
        opener.location.href="ver_carrito.php?opt=4";
        window.close();
        return false;
    }

    
    opener.document.getElementById('codSocio').value = document.getElementById('campo').value;
    window.close();
}

function setFocus(){
     document.getElementById("rut").focus();
}

function devolverValorSheraton(cod) {

    opener.document.getElementById('codSocio').value = cod;
    window.close();

}


</script>

</head>
<body onload="setFocus()">

<div class="TituloPlomo" id="Layer3">
  <table width="482" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="80" rowspan="4"><img src="../Imagenes/IMG_6716.jpg" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong>Consulta C&oacute;digo Vendedor </strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><? ?></span></td>
    </tr>
    
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
  </table>
</div>


<br />
<? if ($_SESSION["codigo_suc"] != 8) {?>
<form name="formCodSocio" onSubmit="Buscar_Cod_Socio(); return false"  method="post" action="">
    <div id="Layer1">
        <table width="389" border="0" cellpadding="0" cellspacing="0">    
          <tr>
            <td width="144" class="celdalazul">Rut Vendedor </td>
            <td width="245" colspan="3" class="celdalCELEST">
              <input name="rut" type="text" class="seleccionTurist_320" id="rut" value=""/>
              -
              <input name="dv" type="text" id="dv" value="" size="2" maxlength="1" />
              
                <img src="../Imagenes/120px-Crystal_Clear_action_build19x19_derecha.png" width="19" height="19" style="cursor:hand" onclick="Buscar_Cod_Socio(); return false"  />

            </td>
          </tr>
        </table>
    </div>
    
    <input type="hidden" id="tab" value="<?=$_GET['t']?>"
    
</form>
        

<div id="resultado_listar"  style="margin:10px 0px 10px 0px; float:left;"><? include('CONSULTA_codSocio.php');?></div>
<? 
} 
else
{
    include('CONSULTA_codSocioSheraton.php');   
}
?>

<div id='msgbox' style='display:none'></div> 

</body>
</html>