<?php
include("menus.php");  

$tpl = new TemplatePower( "../Plantillas/Modificar_Evento.tpl" );
$_Util=new Util;
$tpl->prepare();
$tpl->assign("fecha", $_Util->Fecha());

$miConexion = new ClaseConexion;

$tpl->assign("descripcion", $_POST['descripcion']);
$tpl->assign("descripcionPos",$_POST['descripcionPos']);
$tpl->assign("fechaI", $_POST['fechaI']);
$tpl->assign("cantidad", $_POST['cantidad']);
$tpl->assign("precio", $_POST['precio']);
$tpl->assign("tipo",$_POST['tipo']);
$tpl->assign("proveedor", $_POST['proveedor']);
$tpl->assign("reserva", $_POST['reserva']);
$tpl->assign("fechaF", $_POST['fechaF']);
$tpl->assign("offline", $_POST['offline']);
$tpl->assign("escondido", $_POST['escondido']);
$tpl->assign("codigoTabla", $_POST['codigoTabla']);
$tpl->assign("codigoServ", $_POST['codigo']);
if($_POST['limpiar']=='1')
	{
		echo "<META HTTP-EQUIV='refresh' CONTENT='0; URL=$PHP_SELF'>";
	}
if($_POST['listo']=='1')
	{
		echo "<META HTTP-EQUIV='refresh' CONTENT='0; URL=$PHP_SELF'>";
	}
if($_POST['escondido']=='')
	{
		$tpl->assign("VISIBLE", "style='visibility:hidden'"); //INVISIBLE LA TABLA DE DIAS DE DESCUENTO
		$tpl->assign("VISIBLE2", "style='visibility:hidden'"); //INVISIBLE TABLA DE RESERVAS
		$tpl->assign("VISIBLE_DESC", "style='visibility:hidden'"); //INVISIBLE EL DESCUENTO
	}
else
	{
		$tpl->assign("VISIBLE_LAPIZ", "style='visibility:hidden'");      //INVISIBLE LAPIZ
		$tpl->assign("VISIBLE_CALENDARIO", "style='visibility:hidden'"); //INVISIBLE CALENDARIO
		$tpl->assign("LECTURA", "readonly=''");                          //LECTURA
		$tpl->assign("VISIBLE_boton", "style='visibility:hidden'");      //INVISIBLE BOTONES
	}

////////////////////////////////////////ELIMINAR DESCUENTOS/////////////////////////////////////////////////////////////////
if($_POST['eliminado_desc']<>'') 
	{
		$miConexion->conectar();
		$query_eliminar=$miConexion->EjecutaSP("Eliminar_Asociar_Descuentos",'"'.$_POST['eliminado_desc'].'",@pmessage');
		$parametrosSalida=$miConexion->ObtenerResultadoSP("@pmessage");
		$rowverificacion= mysql_fetch_assoc($parametrosSalida); 
		if($rowverificacion['@pmessage']<>'')
			{
				print("<script>alert('".$rowverificacion['@pmessage']."');</script>");
			}
		$tpl->assign("_ROOT.ELIMINADO", '');	
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////ELIMINAR RESERVA////////////////////////////////////////////////////////////
if($_POST['eliminado']<>'') 
	{
		$miConexion->conectar();
		$query_eliminar=$miConexion->EjecutaSP("Eliminar_Planificacion_Reserva",'1,"'.$_POST['eliminado'].'",@pmessage');
		$tpl->assign("ELIMINADO", ""); 	 

		$parametrosSalida=$miConexion->ObtenerResultadoSP("@pmessage");
		$rowverificacion= mysql_fetch_assoc($parametrosSalida); 
		if($rowverificacion['@pmessage']<>'')
			{
				print("<script>alert('".$rowverificacion['@pmessage']."');</script>");
				//echo "<META HTTP-EQUIV='refresh' CONTENT='0; URL=$PHP_SELF'>"; 
			}
		$tpl->assign("ELIMINADO", '');	
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

if($_POST['codigo_tipo']=='S')
	{
		$tpl->assign("VISIBLE_DESC", "style='visibility:hidden'"); //INVISIBLE EL DESCUENTO
	}
if($_POST['codigo_tipo']=='D')
	{
		$tpl->assign("VISIBLE", "style='visibility:hidden'"); //INVISIBLE TABLA HORAS
		$tpl->assign("VISIBLE2", "style='visibility:hidden'"); //INVISIBLE RESERVAS	
	}

if($_POST['codigoTabla']=='')
	{
		$tpl->assign("VISIBLE", "style='visibility:hidden'"); //INVISIBLE TABLA HORAS
		$tpl->assign("VISIBLE2", "style='visibility:hidden'"); //INVISIBLE RESERVAS	
		$tpl->assign("VISIBLE_DESC", "style='visibility:hidden'"); //INVISIBLE EL DESCUENTO
	}

///////////////////COMBOBOX SERVICIO//////////////////////////////////////////////

if($_POST['variableEstadoComision1']=='verdadero')
	{
		$_POST[variableEstadoComision1]='verdadero';
		$tpl->assign("_ROOT.variableEstadoComision1", "verdadero");
		$tpl->assign("_ROOT.x", "checked='checked'"); 
		$tpl->assign("_ROOT.xx", "checked='checked'"); 
		$miConexion->conectar();    
		$query_serv=$miConexion->EjecutaSP("Consulta_Servicio","0,2");
	}
else
	{
		$tpl->assign("_ROOT.xx", "'NO'"); 
		$miConexion->conectar();    
		$query_serv=$miConexion->EjecutaSP("Consulta_Servicio","0,0");
	}
			
//$miConexion->conectar();    
//$query_serv=$miConexion->EjecutaSP("Consulta_Servicio","0,0");
while ($row_serv = mysql_fetch_assoc($query_serv))
    {	
        $tpl->newBlock("SERVICIO");
        $tpl->assign("CODIGO", $row_serv['CODIGO']  );
		$tpl->assign("CODIGO_SERV", $row_serv['CODIGO_SERV']  );
        $tpl->assign("DESCRIPCION_POS_SERV", $row_serv['DESCRIPCION_SERV'] );
        if ($_POST['servicio'] == $row_serv['CODIGO_SERV'] ) 
            {
                $tpl->assign("SELECCIONAR_serv", 'selected' );
            }
		
   }
mysql_free_result($query_serv);
////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////DESPLIEGUE//////////////////////////////////////////////
if($_POST['codigoTabla']<>'' )
	{
		if($_POST['variableEstadoComision1']=='verdadero')
			{
				$miConexion->conectar();  
				$query=$miConexion->EjecutaSP("Consulta_Servicio","'".$_POST['codigoTabla']."',3");
			}
		else
			{
				$miConexion->conectar();  
				$query=$miConexion->EjecutaSP("Consulta_Servicio","'".$_POST['codigoTabla']."',0");
			}
		while ($row = mysql_fetch_assoc($query))
			{
				$codigoProveedor=$row['CODIGO_PROV'];
				$codigoOffile=$row['OPERAR_OFFLINE_SERV'];
				$codigoEstado=$row['ESTADO_SERV'];
				$codigoRelacion=$row['CODIGO'];
				$tpl->assign("_ROOT.codigo", $row['CODIGO_SERV']);
		 		$tpl->assign("_ROOT.descripcionPos", $row['DESCRIPCION_POS_SERV']);
		 		$tpl->assign("_ROOT.descripcion", $row['DESCRIPCION_SERV']);
		 		$tpl->assign("_ROOT.ESTADO_SERV", $row['ESTADO_SERV']);
				if($row['ESTADO_SERV']=='1')
		 			{
		  				$tpl->assign("_ROOT.ESTADO", 'ACTIVADO');
		 			}
		 		else
		 			{
		  				$tpl->assign("_ROOT.ESTADO", 'DESACTIVADO');
		 			}
		 		$tpl->assign("_ROOT.OPERAR_OFFLINE_SERV", $row['OPERAR_OFFLINE_SERV']); 
				$tpl->assign("_ROOT.CODIGO_PROV", $row['CODIGO_PROV']); 
		 		$tpl->assign("_ROOT.CODIGO_TIPO", $row['TIPO_SERV']); 
			 	if($row['OPERAR_OFFLINE_SERV']=='1')
		 			{
		 				$tpl->assign("_ROOT.offline", 'SI'); 
		 			}
		 		else
		 			{
		 				$tpl->assign("_ROOT.offline", 'NO'); 
		 			}
				$tpl->assign("_ROOT.DURACIONSERVICIO", $row['DURACION_SERVICIO_SERV']);	
				if ($row['FECHA_INICIO_SERV']=='')
					{
						$tpl->assign("_ROOT.FECHAINICIO","");	
					}
				else
					{
						$FechaInicial=$_Util->FormatearFecha($row['FECHA_INICIO_SERV']);	
						$tpl->assign("_ROOT.FECHAINICIO", $FechaInicial);	
					}
				if ($row['FECHA_FINAL_SERV']=='')
					{
						$tpl->assign("_ROOT.FECHAFINAL","");
					}
				else
					{
						$FechaFinal=$_Util->FormatearFecha($row['FECHA_FINAL_SERV']);
						$tpl->assign("_ROOT.FECHAFINAL", $FechaFinal);
					}
				
				$tpl->assign("_ROOT.HORAINICIO", $row['HORA_INICIO_SERV']); 	
				$tpl->assign("_ROOT.HORAFIN", $row['HORA_FINAL_SERV']); 
		 		$tpl->assign("_ROOT.CANTIDAD_SERV", $row['CANTIDAD_SERV']); 
		 		$tpl->assign("_ROOT.PRECIO_SERV", $row['PRECIO_SERV']); 
		 		$tipo=$row['TIPO_SERV'];
		 		if($row['TIPO_SERV']=='S')
		 			{
						$tpl->assign("_ROOT.tipo", 'SERVICIO'); 
						$tpl->assign("VISIBLE_DESC", "style='visibility:hidden'");
					}
				else
					{
						if($row['TIPO_SERV']=='D')
							{
								$tpl->assign("_ROOT.tipo", 'DESCUENTO'); 
								$tpl->assign("VISIBLE", "style='visibility:hidden'");
								$tpl->assign("VISIBLE2", "style='visibility:hidden'");
				 			}
					}
		 		$tpl->assign("_ROOT.proveedor", $row['NOMBRE_PROV']); 
		 		$tpl->assign("_ROOT.codigo_reserva", $row['RESERVA_SERV']); 
		 		$tpl->assign("_ROOT.cantidad", $row['CANTIDAD_SERV']); 
		 		$tpl->assign("_ROOT.precio", $row['PRECIO_SERV']); 
		 		$reserva=$row['RESERVA_SERV'];
		 		if($row['RESERVA_SERV']=='0')
					{
		  				$tpl->assign("_ROOT.reserva", 'NO'); 
		  			}
		  		else
		  			{
		  				$tpl->assign("_ROOT.reserva", 'SI'); 
		  			}
				
			}	 
	}

///////////////////////////////////////CONSULTAS DE PLANIFICACIONS RESERVAS////////////////////////////////
if(($_POST['escondido']<>'')and ($_POST['codigo_tipo']=='S'))
	{
		$miConexion->conectar();  
		$query_planificacion=$miConexion->EjecutaSP("Consulta_Planificacion_Reserva",'"'.$_POST['codigo'].'"');
		while ($row_planificacion = mysql_fetch_assoc($query_planificacion))
			{
		 		$tpl->newBlock("RESERVA");
		 		$tpl->assign("CODIGO_PLAN", $row_planificacion['CODIGO_PLAN']);
		 		$tpl->assign("DIA_SEMANA", $row_planificacion['DIA_SEMANA']);
		 		$tpl->assign("HORA_PLAN", $row_planificacion['HORA_PLAN']);
		 		$tpl->assign("NUM_RESERVA_ORIGEN_PLAN", $row_planificacion['NUM_RESERVA_ORIGEN_PLAN']); 
			}
	}

/////////////////SERVICIO SIN RESERVA/////////////////////////
if($reserva=='0')
	{
		$tpl->assign("VISIBLE", "style='visibility:hidden'");
		$tpl->assign("VISIBLE2", "style='visibility:hidden'");
	}

////////////////MODIFICAR SERVICIOS Y DESCUENTOS/////////////////////////////////////////////////////
if($_POST['escondido']=='1')
	{
		$miConexion->conectar();  
		if(($_POST['codigo']<>'' and $_POST['codigo']<>'descripcion') and ($_POST['modifica']=='1'))
			{
				//if ($_POST['fechainicio']!='')
				//{
				$fechaInicioDia=$_Util->hbuscar($_POST['fechainicio'],"/",1);
				$fechaInicioMes=$_Util->hbuscar($_POST['fechainicio'],"/",2);
       			$fechaInicioAno=$_Util->hbuscar($_POST['fechainicio'],"/",3);
       			$fechaFinalDia=$_Util->hbuscar($_POST['fechafinal'],"/",1);
       			$fechaFinalMes=$_Util->hbuscar($_POST['fechafinal'],"/",2);
       			$fechaFinalAno=$_Util->hbuscar($_POST['fechafinal'],"/",3);
	   			$fechaInicial=$fechaInicioAno.$fechaInicioMes.$fechaInicioDia;
	   			$fechaFinal=$fechaFinalAno.$fechaFinalMes.$fechaFinalDia;
				//}
				//else
				//{
				//	$fechaInicial='';
	   			//$fechaFinal='';
				//}
				$tpl->assign("_ROOT.precio", $_POST['precio']);
				$tpl->assign("_ROOT.descripcionPos", $_POST['descripcion']);
		 		$tpl->assign("_ROOT.descripcion", $_POST['descripcionPos']);
				$tpl->assign("_ROOT.proveedor",$_POST['proveedor']);
				$tpl->assign("_ROOT.cantidad", $_POST['cantidad']);
				$tpl->assign("_ROOT.offline",$_POST['offline']); 
				$tpl->assign("_ROOT.DURACIONSERVICIO",$_POST['duracion']);	
				$tpl->assign("_ROOT.FECHAINICIO",$_POST['fechainicio']);
				$tpl->assign("_ROOT.FECHAFINAL",$_POST['fechafinal']);	
				$tpl->assign("_ROOT.HORAINICIO",$_POST['horainicio']); 	
				$tpl->assign("_ROOT.HORAFIN",$_POST['horafin']); 
				$tpl->assign("_ROOT.ESTADO", $_POST['estado']);
				//$tpl->assign("_ROOT.reserva",$_POST['reserva']);
				//$tpl->assign("_ROOT.tipo", $_POST['tipo']); 
				if ($_POST['CODIGO_PROVEEDOR_OCULTO']=='codnulo')
					{
						$codigoProveedorReal=$codigoProveedor;
					}
				else
					{
						$codigoProveedorReal=$_POST['CODIGO_PROVEEDOR_OCULTO'];
					}
				if ($_POST['CODIGO_OPERAR_OFFLINE']=='codnulo')
					{
						$codigoOffileReal=$codigoOffile;
					}
				else
					{
						$codigoOffileReal=$_POST['CODIGO_OPERAR_OFFLINE'];
					}	
				if ($_POST['CODIGOESTADO']=='codnulo')
					{
						$codigoEstadoReal=$codigoEstado;
					}
				else
					{
						$codigoEstadoReal=$_POST['CODIGOESTADO'];
					}
					
				if ($fechaInicial=='')
					{
						$fechaI=0;
					}
				else
					{
						$fechaI=$fechaInicial;
					}
				if ($fechaFinal=='')
					{
						$fechaF=0;
					}
				else
					{
						$fechaF=$fechaFinal;
					}
				if ($_POST['horainicio']=='')
					{
						$horainicial=0;
					}
				else
					{
						$horainicial=$_POST['horainicio'];
					}
				if ($_POST['horafin']=='')
					{
						$horafinal=0;
					}
				else
					{
						$horafinal=$_POST['horafin'];
					}
						
				$variableprecio=str_replace(".","",$precio);
				
				if($_POST['variableEstadoComision1']=='verdadero')
					{
					$miConexion->conectar();  
						$query_planificacion=$miConexion->EjecutaSP1("Modificar_Servicio_Deshabilitadas",$codigoRelacion.",'".strtoupper($_POST['codigo'])."','".strtoupper($_POST['descripcion'])."',".$codigoProveedorReal.",'".$fechaI."','".$fechaF."',".$_POST['cantidad'].",".$variableprecio.",'".$codigoOffileReal."','".$_POST['codigo_tipo']."','".strtoupper($_POST['descripcionPos'])."',".$_POST['codigo_reserva'].",'".$horainicial."','".$horafinal."',".$_POST['duracion'].",".$codigoEstadoReal.",@codigo,@pmessage");
						//$miConexion->conectar(); 
						$parametrosSalida=$miConexion->ObtenerResultadoSP("@codigo,@pmessage");
						$rowverificacion= mysql_fetch_assoc($parametrosSalida);  
						if ($rowverificacion['@codigo']==1)
							{
								print("<script>alert('".$rowverificacion['@pmessage']."');</script>");
							}
						else
							{
								print("<script>alert('".$rowverificacion['@pmessage']."');</script>");
								//echo "<META HTTP-EQUIV='refresh' CONTENT='0; URL=$PHP_SELF'>";
							}
					}
				else
					{
					$miConexion->conectar();  
				$query_planificacion=$miConexion->EjecutaSP("Modificar_Servicio","'".strtoupper($_POST['codigo'])."','".strtoupper($_POST['descripcion'])."',".$codigoProveedorReal.",'".$fechaI."','".$fechaF."',".$_POST['cantidad'].",".$variableprecio.",'".$codigoOffileReal."','".$_POST['codigo_tipo']."','".strtoupper($_POST['descripcionPos'])."',".$_POST['codigo_reserva'].",'".$horainicial."','".$horafinal."',".$_POST['duracion'].",".$codigoEstadoReal.",@pmessage");
				//EXIT;
				
				/*if(($_POST['codigo_tipo']<>'S') and ($reserva=='0'))//SERVICIO , SIN RESERVA
					{
						//echo "<META HTTP-EQUIV='refresh' CONTENT='0; URL=$PHP_SELF'>"; 
					}*/
				if(($_POST['codigo_tipo']=='S' or $_POST['codigo_tipo']=='D' ) and ($codigoEstadoReal=='0'))//SERVICIO , SIN RESERVA
					{
						echo "<META HTTP-EQUIV='refresh' CONTENT='0; URL=$PHP_SELF'>";
					}
				 if($_POST['codigo_tipo']<>'D')//DESCUENTO
					{
						$parametrosSalida=$miConexion->ObtenerResultadoSP("@pmessage");
						$rowverificacion= mysql_fetch_assoc($parametrosSalida);  
						if($rowverificacion['@pmessage']<>'')
							{
								print("<script>alert('".$rowverificacion['@pmessage']."');</script>");
								if(($reserva=='0')and ($_POST['codigo_tipo']=='S'))//RESERVA= NO , TIPO=SERVICIO
									{
										echo "<META HTTP-EQUIV='refresh' CONTENT='0; URL=$PHP_SELF'>"; 
									}
							}		
					}
					}
				//if($_POST['codigo_reserva']=='0')
				//	{
				//		$miConexion->conectar();  
				//		$query_planificacion=$miConexion->EjecutaSP("Eliminar_Planificacion_Reserva","'0','".$_POST['codigo']."',@A");
				//	}
			}
	}

//PARA QUE ESTEN INVISIBLES LAS TABLAS ANTES HACER CLICK EN ACEPTAR
/*
if($_POST['escondido']=='')
{
$tpl->assign("VISIBLE", "style='visibility:hidden'");
$tpl->assign("VISIBLE2", "style='visibility:hidden'");
$tpl->assign("VISIBLE_DESC", "style='visibility:hidden'");
}
*/



//PARA BLOQUEAR LOS CAMPOS

if($_POST['escondido']=='1')
	{
		$tpl->assign("DESHABILIT","readonly");
		$tpl->assign("VISIBLE_boton", "style='visibility:hidden'");
	}



if(($_POST['codigo_tipo']=='D') or ($_POST['codigo_tipo']=='')) //TIPO= SERVICIO
{
$tpl->assign("VISIBLE", "style='visibility:hidden'");	   //INVISIBLE EL SEMANA
$tpl->assign("VISIBLE2", "style='visibility:hidden'"); 	   //INVISIBLE EL RESERVA
}


if(($_POST['codigo_tipo']=='S') or ($_POST['codigo_tipo']=='')) //TIPO= SERVICIO
{
	//if(($_POST['reserva']=='0')or($_POST['reserva']=='')) //NO TIENE RESERVA
	if(($reserva=='0')or($reserva=='')) //NO TIENE RESERVA
		{
			$tpl->assign("VISIBLE", "style='visibility:hidden'");	   //INVISIBLE EL SEMANA
			$tpl->assign("VISIBLE2", "style='visibility:hidden'"); 	   //INVISIBLE EL RESERVA
			$tpl->assign("VISIBLE_DESC", "style='visibility:hidden'"); //INVISIBLE EL DESCUENTO
		}
		
	//if(($_POST['reserva']=='1')or($_POST['reserva']=='')) //SI TIENE RESERVA 
	if(($reserva=='1')or($reserva=='')) //SI TIENE RESERVA 
		{
			$tpl->assign("VISIBLE_DESC", "style='visibility:hidden'"); //INVISIBLE EL DECUENTO
			/*
			//DESPLIEGA LA LISTA DE DESCUENTOS ASOCIADOS
			$miConexion->conectar();
			$query_consulta=$miConexion->EjecutaSP("Consulta_Servicio_Descuento",'"'.$_POST['codigo'].'"'); 	
			while($filas= mysql_fetch_assoc($query_consulta))
			{
			$tpl->newBlock("DESCUENTO");
			$tpl->assign("CODIGO_RSD", $filas['CODIGO_RSD']  );
			$tpl->assign("DESCRIPCION_SERV", $filas['DESCRIPCION_SERV']  );
			}
			*/	
		}	
}

//CONSULTA LOS DESCUENTOS ASOCIADOS
//if($_POST['codigo_tipo']=='D')
//{
//DESPLIEGA LA LISTA DE DESCUENTOS ASOCIADOS
//if(($_POST['escondido']<>'')and ($_POST['codigo_tipo']=='D'))
//{
	//echo 'andrea';
	$miConexion->conectar();
	$query_consulta=$miConexion->EjecutaSP("Consulta_Servicio_Descuento","'".$_POST['codigo']."',0");
	//echo $query_consulta;
	$numeroRegistros=$miConexion->NumRegistros();
	//echo $numeroRegistros;
	while($filas=mysql_fetch_assoc($query_consulta))
	{
		//echo "<br>filas".$filas['CODIGO_RSD']; 
	//}
		$tpl->newBlock("DESCUENTO");
		//echo $filas['CODIGO_RSD'] ;
		//$tpl->assign("CODIGO_RSD", $filas['CODIGO_RSD']  );
		//$tpl->assign("_ROOT.CODIGOS_RSD", $filas['CODIGO_RSD']  );
		$tpl->assign("CODIGOS_RSD",$filas['CODIGO_RSD'] );
		$tpl->assign("DESCRIPCION_DESC", strtoupper($filas['DESCRIPCION_DESC']));
		$tpl->assign("IMAGEN","<img src='../Imagenes/120px-Crystal_Clear_action_button_cancel19x19.png' width='19' height='19' style='cursor:hand' onClick='Eliminar_descuento(");
		
		$tpl->assign("IMAGEN2",")' />");
	/*}*/
	}
//}

$tpl->printToScreen();
?>
<div align="justify"></div>
