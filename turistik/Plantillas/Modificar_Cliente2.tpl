<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Modificar Información Distribuidor</title>
<style type="text/css">
<!--
#Layer3 {	position:absolute;
	left:18px;
	top:13px;
	width:432px;
	height:62px;
	z-index:14;
}
-->
</style>
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#Layer1 {	position:absolute;
	left:0px;
	top:92px;
	width:788px;
	height:212px;
	z-index:9;
}
-->
</style>
</head>
<script>
function TipoDistribuidor()
	{
		miPopup = window.open("../PHP/Lista_TipoDistribuidor.php","cue","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
function TipoDescripcion()
	{
		miPopup = window.open("../PHP/ListaNI.php","cue","top=200,left=400,width=350,height=250,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}	


function validar()
	{
    	// Primer paso: Obtener el rut que ingreso el usuario
   		 var rutCompleto = document.form1.rutdistribuidor.value;
		// Eliminamos los caracteres raros, espacios, puntos, guiones.
    	// Pasamos a minusculas, y separamos el rut y el digito verificador
    	rutCompl = rutCompleto.toLowerCase();
   		var dv=document.form1.dvdistribuidor.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
   		 valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   		 valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
        		alert("El Rut no es Valido.Ingreselo Nuevamente");
        		document.form1.rutdistribuidor.value="";
				document.form1.dvdistribuidor.value="";
				document.form1.rutdistribuidor.focus();
   			 }
	
	}

function verificarDV(rut, dv)
	{
   		var multiplicador = 9;
    	var aux = rut;
    	var suma = 0;
    	while(aux > 0)
			{
        		var unidades = aux % 10 ;
        		aux = (aux - unidades) / 10;
        		suma += (multiplicador * unidades);
        		multiplicador--;
        		if (multiplicador < 4)
					{
            			multiplicador = 9;
       				 }
    		}
    	digito = suma % 11;
    	if (digito == 10)
			{
        		digito = "k";
    		}
    	if (dv == digito)
			{
        		return true;
    		}
    	else 
			{
       			 return false;
    		}
	} 

function isEmailAddress(email)
	{
		var s = email;
		var filter=/^[A-Za-z][A-Za-z0-9_]*@[A-Za-z0-9_]+\.[A-Za-z0-9_.]+[A-za-z]$/;
		if (s.length == 0 ) return true;
		if (filter.test(s))
		return true;
		else
		alert("Ingrese una dirección de correo válida");
		document.form1.emaildistribuidor.value="";
		document.form1.emaildistribuidor.focus();
		return ;
}	
function pres_num()
	{
		if((event.keyCode<48) || (event.keyCode>57))
			{
				event.returnValue = false;
			}		
	}
function Categoria()
	{
		miPopup = window.open("../PHP/ListaCategoria.php","cue","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}		
function ModificarTipo()
	{
		miPopup = window.open("../PHP/ListaTipo.php","cue","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}

function ModificarDistribuidor()
	{
		if (document.form1.tipodistribuidor.value=='')
			{
				alert("Debe Ingresar el Tipo de Distribuidor que desea modificar");
				document.form1.tipodistribuidor.focus();
				return;
			}
		if (document.form1.nacional.value=='')
			{
				alert("Debe Ingresar la Descripción para el Distribuidor que desea modificar");
				document.form1.nacional.focus();
				return;
			}
		if (document.form1.nombrefantasia.value=='')
			{
				alert("Debe Ingresar el Nombre de Fantasía para el Distribuidor que desea modificar");
				document.form1.nombrefantasia.focus();
				return;
			}
		if (document.form1.razonsocial.value=='')
			{
				alert("Debe Ingresar la Razón Social para el Distribuidor");
				document.form1.razonsocial.focus();
				return;
			}
			
		if (document.form1.rutdistribuidor.value=='')
			{
				alert("Debe Ingresar el Rut para el Distribuidor");
				document.form1.rutdistribuidor.focus();
				return;
			}
			
		if (document.form1.dvdistribuidor.value=='')
			{
				alert("Debe Ingresar el digito verificador para el Rut del Distribuidor");
				document.form1.dvdistribuidor.focus();
				return;
			}
		
		if (document.form1.sucursal.value=='')
			{
				alert("Debe Ingresar la Sucursal correspondiente para ese Distribuidor");
				document.form1.sucursal.focus();
				return;
			}
			
		if (document.form1.direcciondistribuidor.value=='')
			{
				alert("Debe Ingresar la Dirección del Distribuidor que desea ingresar");
				document.form1.direcciondistribuidor.focus();
				return;
			}
		if (document.form1.comunadistribuidor.value=='')
			{
				alert("Debe Ingresar la Comuna a la cual pertenece el Distribuidor");
				document.form1.comunadistribuidor.focus();
				return;
			}
		
		if (document.form1.ciudaddistribuidor.value=='')
			{
				alert("Debe Ingresar la Ciudad a la cual pertenece el Distribuidor");
				document.form1.ciudaddistribuidor.focus();
				return;
			}
		
		if (document.form1.paisdistribuidor.value=='')
			{
				alert("Debe Ingresar el País al cual pertenece el Distribuidor");
				document.form1.paisdistribuidor.focus();
				return;
			}
			
		if (document.form1.categoria.value=='')
			{
				alert("Debe Seleccionar Categoría");
				document.form1.categoria.focus();
				return;
			}
		if (document.form1.giro.value=='')
			{
				alert("Debe Ingresar el giro del Distribuidor");
				document.form1.giro.focus();
				return;
			}
			/*	
		if (document.form1.tipo.value=='')
			{
				alert("Debe Ingresar el Tipo de Distribuidor");
				document.form1.giro.focus();
				return;
			}
			*/
		if (document.form1.estado.value=='')
			{
				alert("Debe Ingresar el Estado");
				document.form1.estado.focus();
				return;
			}
 		if (document.form1.lineaCredito.value=='')
			{
				alert("Debe Ingresar un saldo en linea de credito");
				document.form1.lineaCredito.focus();
				return;
			}
    	var rutCompleto = document.form1.rutdistribuidor.value;
		rutCompl = rutCompleto.toLowerCase();
    	var dv=document.form1.dvdistribuidor.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
    	valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   	 	valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
       	 		alert("El Rut no es Valido.Ingreselo Nuevamente");
				return;
        		document.form1.rutdistribuidor.value="";
				document.form1.dvdistribuidor.value="";
    		}
		/*if (document.form1.emaildistribuidor.value!='')
			{
				var s =document.form1.emaildistribuidor.value;
				var filter=/^[A-Za-z][A-Za-z0-9_]*@[A-Za-z0-9_]+\.[A-Za-z0-9_.]+[A-za-z]$/;
				if (s.length == 0 ) return true;
				if (filter.test(s))
					{}
				else
				{
					alert("Ingrese una dirección de correo válida");
					document.form1.emaildistribuidor.value="";
					document.form1.emaildistribuidor.focus();
					return ;
				}
			}*/
		document.form1.modificar.value="1";
		document.form1.submit();
}
	
function ModificarEstado()
	{
		miPopup = window.open("../PHP/ListaEstadosDistribuidor.php","cue","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}

function ModificarPago()
	{
		miPopup = window.open("../PHP/ListaPagoDistribuidor.php","cue","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
	
function ModificarPromotor()
	{
		miPopup = window.open("../PHP/ListaPromotor.php","cue","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}	
	
function Salir()
	{
		window.close();
	}
function validarP(e) 
{ // 1
    tecla = (document.all) ? e.keyCode : e.which; // 2
    if (tecla==8) 
        return true; // 3
    patron = /\d/; // Solo acepta n?meros
    te = String.fromCharCode(tecla); // 5
    return patron.test(te); // 6
}
	
</script>
<body>
<form id="form1" name="form1" method="post" action="">
<div class="TituloPlomo" id="Layer3">
  <table width="367" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="81" rowspan="4"><img src="../Imagenes/101px-Crystal_Clear_kuser280x80.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="259">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong>Modificar  Informaci&oacute;n Distribuidor</strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha">{fecha}</span>
        <div id="Layer1">
          <table width="787" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td class="celdalazul">Tipo Distribuidor </td>
              <td class="celdalCELEST">
                    <input name="tipodistribuidor" type="text" class="seleccionTurist_80" id="tipodistribuidor" {DESCRIPCIONGRUPO} readonly="" />
                <img src="../Imagenes/modify.gif" width="16" height="16" alt="Modificar Tipo Distribuidor" onclick="TipoDistribuidor()" style="cursor:hand"/>              </td>
              <td class="celdalazul">Descripci&oacute;n</td>
              <td class="celdalCELEST"><label>
                <input name="nacional" type="text" class="seleccionTurist_250" id="nacional" {DESCRIPCION} readonly=""/>
                <img src="../Imagenes/modify.gif" width="16" height="16"  alt="Modificar Descripción" onclick="TipoDescripcion()" style="cursor:hand"/></label></td>
            </tr>
            <tr>
              <td width="112" class="celdalazul">Nombre Fantas&iacute;a </td>
              <td width="287" class="celdalCELEST"><input name="nombrefantasia" type="text" class="seleccionTurist_250" id="nombrefantasia" {NOMBREFANTASIA} /></td>
              <td width="107" class="celdalazul">Raz&oacute;n Social </td>
              <td width="281" class="celdalCELEST"><label>
                <input name="razonsocial" type="text" class="seleccionTurist_250" id="razonsocial" {RAZONSOCIAL}>
              </label></td>
            </tr>
            <tr>
              <td class="celdalazul">Rut</td>
              <td class="celdalCELEST"><label>
                <input name="rutdistribuidor" type="text" class="seleccionTurist_180" onkeypress="pres_num();" maxlength="10" {RUT}/>
                -
                <input name="dvdistribuidor" type="text" class="seleccionT80_a" maxlength="1" onchange="validar()" {DV} />
              </label></td>
              <td class="celdalazul">Sucursal</td>
              <td class="celdalCELEST"><label>
                <input name="sucursal" type="text" class="seleccionTurist_80" id="sucursal" maxlength="3" {SUCURSAL} />
              </label></td>
            </tr>
            <tr>
              <td class="celdalazul">Direcci&oacute;n</td>
              <td class="celdalCELEST"><input name="direcciondistribuidor" type="text" class="seleccionTurist_250" {DIRECCION}/></td>
              <td class="celdalazul">Comuna</td>
              <td class="celdalCELEST"><input name="comunadistribuidor" type="text" class="seleccionTurist_180" {COMUNA}/></td>
            </tr>
            <tr>
              <td class="celdalazul">Ciudad</td>
              <td class="celdalCELEST"><label>
                <input name="ciudaddistribuidor" type="text" class="seleccionTurist_180" {CIUDAD}/>
              </label></td>
              <td class="celdalazul">Pa&iacute;s</td>
              <td class="celdalCELEST"><input name="paisdistribuidor" type="text" class="seleccionTurist_180" {PAIS} /></td>
            </tr>
            <tr>
              <td class="celdalazul">Tel&eacute;fono</td>
              <td class="celdalCELEST"><label>
                <input name="telefonodistribuidor" type="text" class="seleccionTurist_180" onkeypress="pres_num();" maxlength="10" {TELEFONO}/>
              </label></td>
              <td class="celdalazul">Email</td>
              <td class="celdalCELEST"><input name="emaildistribuidor" type="text" class="seleccionTurist_250"  maxlength="50" {EMAIL}/></td>
            </tr>
            <tr>
              <td class="celdalazul">Fax</td>
              <td class="celdalCELEST"><label>
                <input name="fax" type="text" class="seleccionTurist_180" id="fax" onkeypress="pres_num();" maxlength="10" {FAX} />
              </label></td>
              <td class="celdalazul">Giro</td>
              <td class="celdalCELEST"><label>
                <input name="giro" type="text" class="seleccionTurist_250" {GIRO} />
              </label></td>
            </tr>
            <tr>
              <td class="celdalazul">Categor&iacute;a<span class="celdalCELEST">
                <input name="tipo" type="hidden" class="seleccionTurist_80" id="tipo" {descripciontipo} readonly=""/>
              </span></td>
              <td class="celdalCELEST"><input name="categoria" type="text" class="seleccionTurist_250" id="categoria2" {DESCRIPCIONCATEGORIA} readonly="" />
                <img src="../Imagenes/modify.gif" width="16" height="16" alt="Modificar Categor&iacute;a" style="cursor:hand" onclick="Categoria()"/></td>
              <td class="celdalazul">Promotor</td>
              <td class="celdalCELEST"><input name="promotor" type="text" class="seleccionTurist_180" id="promotor" {PROMOTOR}/>
                <img src="../Imagenes/modify.gif" width="16" height="16"  alt="Modificar Estado Distribuidor" onclick="ModificarPromotor()" style="cursor:hand"/> 
                <input name="cod_promotor" type="hidden" id="cod_promotor2" {CODIGO_PROMOTOR} /></td>
            </tr>
            <tr>
              <td class="celdalazul">Pago</td>
              <td class="celdalCELEST"><input name="pago" type="text" class="seleccionTurist_180" id="estado" value="{PAGO}" readonly=""/>
                <img src="../Imagenes/modify.gif" width="16" height="16"  alt="Modificar Pago Distribuidor" onclick="ModificarPago()" style="cursor:hand"/></td>
              <td class="celdalazul">Estado</td>
              <td class="celdalCELEST"><input name="estado_dist" type="text" class="seleccionTurist_180" id="estado3" {ESTADO} readonly=""/>
                <img src="../Imagenes/modify.gif" width="16" height="16"  alt="Modificar Estado Distribuidor" onclick="ModificarEstado()" style="cursor:hand"/></td>
            </tr>
            <tr>
              <td class="celdalazul">L&iacute;nea de cr&eacute;dito</td>
              <td class="celdalCELEST"><input name="lineaCredito" type="text" onkeypress="return validarP(event);" class="seleccionTurist_250" {LINEA}/></td>
              
              <td class="celdalazul">Ofertas</td>
              <td class="celdalCELEST">
                <input name="OFERTA" type="checkbox" id="OFERTA" {OFERTA}/>
             </td>
            </tr>

            <tr>
              <td><input type="hidden" name="CODIGOGRUPO"   />
			  <input type="hidden" name="CODIGOPAGO" value="{CODIGOPAGO}"   />
			  <input type="hidden" name="CODIGONACIONAL"    />
	         <input type="hidden" name="CODIGOCATEGORIA" />
			 <input type="hidden" name="CODIGOTIPO"    />
			 <input type="hidden" name="CODIGOESTADO" />
            
             <input type="hidden" name="codigo_usua_dist" value="{COD_USUA_DIST}"  />
                   
				  
				
			    <div id="boton" style="position:absolute; left:337px; top:300px; width:200px; height:25px; z-index:3; border:0pt black solid;">
                  <input name="Submit2" type="button" class="BotonTurist_Celest" value="Modificar" onclick="ModificarDistribuidor()" />
				  <input type="hidden" name="modificar" value="0" />
			      <label>
			      <input name="Submit" type="submit" class="BotonTurist_Celest" value="Salir" onclick="Salir()" />
			      </label>
			    </div></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table>
      </div></td>
    </tr>
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
  </table>
</div>
</form>
</body>
</html>
