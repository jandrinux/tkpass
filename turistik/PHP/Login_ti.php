<?
require ("../Clases/ClaseConexion.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/style_login.css" />
<script src="../../js/jquery.js" type="text/javascript"></script>
<script src="../../js/ajax.js" type="text/javascript"></script>

<title>Turistik -Login</title>
<script language="javascript">

<!-- LOGIN -->

$(document).ready(function()
{
	$("#login_form").submit(function()
	{
	
		//remove all the class add the messagebox classes and start fading
		$("#msgbox").removeClass().addClass('messagebox').text('Validando....').fadeIn(1000);
		//check the username exists or not from ajax
       
		$.post("valida_login.php?a=admin",{ rut:$('#rut').val(),contrasena:$('#contrasena').val(),pass:$('#pass').val(),rand:Math.random(),suc:$("#Sucursal").val() } ,function(data)
        {
  
		  if(data=='yes') //if correct login detail
		  {
		  	$("#msgbox").fadeTo(200,0.1,function()  //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('Loging correcto, transfiriendo.....').addClass('messageboxok').fadeTo(900,1,
              function()
			  { 
			     //redirect to secure page
                 <?php 
                 if($_GET['url']!="")
                 {
                  echo  "window.location.href='".$_GET['url']."'"; 
                 }
                 else
                 {
                 ?>
				 window.location.href='Inicio.php';
                 <?
                 }?>
			  });
			  
			});
		  }
		  else 
		  {
		      
		  	$("#msgbox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
			  //add message and change the class of the box and start fading
			  $(this).html('Rut o password incorrecto...').addClass('messageboxerror').fadeTo(900,1);
			});		
          }
				
        });
 		return false; //not to post the  form physically
	});
	//now call the ajax also focus move from 
	$("#password").blur(function()
	{
		$("#login_form").trigger('submit');
	});
});


$(document).ready(function(){
 
      $(document).keypress(function(e) {
             
            //13 es el c�digo de la tecla
            if(e.which == 13) {
                  $("#login").submit();
            }
 
      });         
                   
});


</script>
<script type="text/javascript">
function setFocus()
{
     document.getElementById("rut").focus();
}
</script>

<!-- FONDO NUEVO -->

<style type="text/css">
html, body {margin:0; padding:0; width:100%; height:100%; overflow:hidden; }
body {font-family: 'Arial', serif;}
#background{position:absolute; z-index:1; width:100%; height:100%;}
#fixed {position:absolute; top:25px; left:10px; width:160px;  color:#333; padding:10px;}
#scroller {position:absolute; width:100%; height:100%; top:0; left:0; overflow:auto; z-index:2;} 
#content {padding:20px 20px 20px 0px;}
p {font-size:16px; text-align:justify; line-height:25px; text-shadow: 0 1px 2px rgba(0,0,0,0.5), 0px -1px 2px rgba(255,255,255,0.8);}
.Col {width:25%; float:left; padding:2%; min-width:300px;}
</style>
<body>
<div>
	<img id="background" src="img/bg.png" alt="" title=""></div>
<div id="scroller">
<div id="content">

<!-- FIN FONDO NUEVO -->



<!-- FONDO ANTIGUO -->
<!-- background-image: url(img/bg.jpg);  -->




<style type="text/css">
body {
    font-family: Arial;

}
.messagebox{
	
    top:415px ;
	width:10px;
    margin-left:0px;
    position:absolute;
    float:left ;
	padding:3px;
}
.messageboxok{

	width:auto;
    float:left ;
	padding:3px;
	font-weight:bold;
	color:#236E18;
	
}
.messageboxerror{

	width:auto;
    float:left ;
	padding:3px;
    font-weight:bold;
	color:#CC0000;
}
</style>
</head>

<body onload="setFocus()">

<div id="stylized" class="login">
<!--id="login_form"-->
  <form  method="post" id="login_form" action="" >
  <div class="logo"><img src="img/logo_turistik.png" width="50" height="60" /></div>
  
   <h1>Bienvenido a Turistik Pass 2.0</h1>

    <p></p>

	<label title="Ejemplo 16874256">Rut: *</label><p1>(sin digito verificador)</p1> 

    <input type="text" name="rut" id="rut" onkeypress="return isNumberKey(event)"  />  

    <label for="password">Password: *</label>
    <input type="password" name="pass" id="pass" />
    
    <label for="password">Seleccione Sucursal: *</label>
    <div style="padding-left:4cm; padding-bottom: 1cm; ">
    <select id="Sucursal"> 
    <option value="0" selected="">-- SELECCIONE --</option>
    <?php 

    
    $SQL = "select * from SUCURSAL WHERE ESTADO = 1 ORDER BY SUCURSAL";
    $COUNT = 0 ;
    $miConexion= new ClaseConexion;
    $miConexion->conectar();
    $queryGrupo=$miConexion->EjecutaConsulta($SQL);
    while ($row = mysql_fetch_assoc($queryGrupo))
    
    {
        echo "<option value='".md5($row['CODIGO_SUCURSAL'])."'>".$row['SUCURSAL']."</option>";
    }
    ?>
    </select>
    </div> 
    <span id="msgbox" style="display:none"></span>  
     <button name="login" type="submit">Login</button>
    <!--<input align="right" name="comprar" type="submit" class="BotonTurist_Celest" value="Login" />-->
    
    
    
   <div class="spacer"></div>

  </form>

 </div>

</body>

</html>