<?php
    session_start();
    include("menus.php");
    $_Util=new Util;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<link href="../../css/menu_small.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>


<script>

    function Reembolsar()
    {

        if (document.getElementById('precio').value == 0)
        {
            alert('Seleccione un servicio para reembolsar');
            return false;
        }
        if (document.getElementById('tipo').value == 'C' && document.getElementById('digitos_tarjeta').value == '')
        {
            alert('Ingrese ultimos 4 digitos de Tarjeta de Credito.');
            document.getElementById('digitos_tarjeta').focus();
            return false;
        }
        
        if (document.getElementById('mail').value == '')
        {
            alert('Ingrese E-mail de cliente.\n Campo Obligatorio (*)');
            document.getElementById('mail').focus();
            return false;
        }
        
        if (document.getElementById('mail').value != document.getElementById('mail2').value)
        {
               alert('Confirme E-mail de cliente.\n Campo Obligatorio (*)');
                document.getElementById('mail2').focus();
                return false;
        }
        
        if (document.getElementById('observacion').value == '')
        {
            alert('Ingrese observacion para reembolsar.\n Campo Obligatorio (*)');
            document.getElementById('observacion').focus();
            return false;
        }
        
             document.getElementById('porcentaje').value = document.getElementById('porc').value;
             document.getElementById('form2').submit();
        
    }

    function validate()
    {
    
    var porcentaje = document.getElementById('porc').value;
    var cantidad = document.getElementById('cantidad').value;
 

    var total = 0;

    for (var i = 1; i <= cantidad; i++)
    {
     var names = "precio_re"+i; 
     var n = "check"+i; 
    
        
       if (document.getElementById(n).checked)
       {
        
        var v = document.getElementById(names).value;
 
            total =parseInt(total) + parseInt(v); 
           
        }
        
    
    }
    sub = (porcentaje * total) / 100;

    total = formatNumber(sub,''); 
    document.getElementById('precio').value=total;
    
    return true;
    }
    

</script>
</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>
<div class="TituloPlomo" id="Layer3">
  <table width="482" border="0" cellpadding="0" cellspacing="0">  
  <tr>
      <td width="80" rowspan="4"><img src="../Imagenes/money_back.jpg" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Reembolso</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    

  </table>

</div>

<div id="Layer1">



<form  name="form" method="post" onSubmit="reembolso(); return false">
<table width="520" height="52" border="0" cellspacing="0" >

      <tr>
        <td width="80" class="celdalazul_peque">Boleta TTS :</td>
        <td width="100" class="celdalCELEST">     
    	   <input type="text" value="" id="boletaTTS" name="boletaTTS" onkeypress="return validarP3(event);" onclick="Ocultar();" class="seleccionTurist_180" maxlength="15"/>
        </td> 
        <td class="celdalCELEST"><input type="text" size="1" readonly="" style="background-color: #f4a846;" /> Embarcado</td>
      </tr>
      <tr>      
        <td width="80" class="celdalazul_peque">Boleta Turistik :</td>
        <td width="100" class="celdalCELEST">     
    	   <input type="text" value="" id="boletaTTK" name="boletaTTK" onkeypress="return validarP3(event);" class="seleccionTurist_180" maxlength="15"/>
        </td> 
        <td class="celdalCELEST"><input type="text" size="1" readonly="" style="background-color: #D70103;" /> Reembolsado</td>    
       </tr>

       <tr>
        <td width="80" class="celdalazul_peque">Porcentaje:</td>
        <td width="100" class="celdalCELEST">     
    	   <select name="porc" id="porc" class="seleccionTurist_180" onchange="validate()">
                <option value="10">10 %</option>
                <option value="20">20 %</option>
                <option value="30">30 %</option>
                <option value="40">40 %</option>
                <option value="50">50 %</option>
                <option value="60">60 %</option>
                <option value="70">70 %</option>
                <option value="80">80 %</option>
                <option value="90">90 %</option>
                <option value="100" selected="">100 %</option>
           </select>
           
        </td> 
        <td class="celdalCELEST"><input type="submit"  class="BotonAzul2"  value="Consultar" /></td>    
       
      </tr>
</table>
</form>

<form  name="form2" id="form2" method="post" action="reembolsar.php">
<?php
echo '<div id="resultado_listar" style=" PADDING-TOP: 10px; TEXT-ALIGN: left; PADDING-BOTTOM: 3px; FONT-FAMILY: Arial; FONT-SIZE: 8pt; left:20px; top:258px; width:1170px; height:330px;" >';
include('CONSULTA_reembolso.php');
echo '</div>';
?>
 <input type="hidden" id="porcentaje" name="porcentaje"/>

</form>
</div>
</body>
</html>