<?php 
include("menus.php");
//require ("../Clases/ClaseConexion.inc.php");
//include( "../Clases/class.TemplatePower.inc.php"); 
//require ("../Clases/ClaseUtil.inc.php");

$tpl = new TemplatePower( "../Plantillas/Creacion_Excursiones.tpl" );

$_Util=new Util;
$miConexion= new ClaseConexion;

$tpl->prepare();
$tpl->assign("fechas", $_Util->Fecha());
$tpl->assign("precio", $_POST['precio']);
$tpl->assign("nombre", $_POST['nombre']);
$tpl->assign("nombre_abr", $_POST['nombre_abr']);
$tpl->assign("descripcion", $_POST['descripcion']);
$tpl->assign("descripcion_corta", $_POST['descripcion_corta']);
$tpl->assign("descripcion_eng", $_POST['descripcion_eng']);
$tpl->assign("descripcion_por", $_POST['descripcion_por']);
$tpl->assign("cat1", $_POST['cat1']);
$tpl->assign("cat2", $_POST['cat2']);
$tpl->assign("cat3", $_POST['cat3']);
$tpl->assign("cat4", $_POST['cat4']);
$tpl->assign("cat5", $_POST['cat5']);
$tpl->assign("hora", $_POST['hora']);
$tpl->assign("mini", $_POST['mini']);
$tpl->assign("hora_correo", $_POST['hora_correo']);
$tpl->assign("min_correo", $_POST['min_correo']);
$tpl->assign("limite", $_POST['limite']);
$tpl->assign("limite_t", $_POST['limite_t']);
$tpl->assign("correo", $_POST['correo']);
$tpl->assign("descuento", $_POST['descuento']);
$tpl->assign("correo_auto", $_POST['correo_auto']);

$tpl->assign("nemotec", $_POST['nemotec']);
$tpl->assign("preaviso", $_POST['preaviso']);

$tpl->assign("desc_juniper", $_POST['desc_juniper']);
$tpl->assign("polit_juniper", $_POST['polit_juniper']);

$tpl->assign("guardar", $_POST['guardar']);

$vendedores=array('SI','NO');
$i=0;
foreach ($vendedores as $actual ) 
    {
        $i++;
        $tpl->newBlock("vendedores");
	    $tpl->assign("CODIGO", $i );
        $tpl->assign("DESCRIPCION", $actual );
        if ($_POST['vendedores'] == $i ) 
            {
                $tpl->assign("seleccionar_v", 'selected' );
            }
    }	

$vendedores_int=array('SI','NO');
$i=0;
foreach ($vendedores_int as $actual_int ) 
    {
        $i++;
        $tpl->newBlock("vendedores_Int");
	    $tpl->assign("CODIGO_I", $i );
        $tpl->assign("DESCRIPCION_I", $actual_int );
        if ($_POST['vendedores_int'] == $i ) 
            {
                $tpl->assign("seleccionar_int", 'selected' );
            }
    }	

if ($_POST['guardar']=='1')
{
    
    $status = "";
    for ($i=0; $i<=count($_FILES['archivo']['name']); $i++)
    {
        //$tamano = $_FILES["archivo"]['size'][$i];
       // $tipo = $_FILES["archivo"]['type'][$i];
        $archivo = $_FILES["archivo"]['name'][$i];
        $prefijo = substr(md5(uniqid(rand())),0,6);
        
        if ($archivo != "")
        {
            $destino = "../Imagenes/img-juniper/".$prefijo."_".$archivo;
            $file = $prefijo."_".$archivo;
                if (copy($_FILES['archivo']['tmp_name'][$i],$destino)) {
                    $file_concat = $file.",".$file_concat;
                    $status = "Archivo subido: <b>".$archivo."</b>";
                }
                else
                {
                    $status = "Error al subir el archivo ".$archivo;
                }
        }
        else
        {
            $status = "Error al subir archivo ".$archivo;
        }
    }
    
    #JUNIPER
    
    $nemotecnico = $_POST['nemotec'];
    
    $descripcion_juniper=str_replace("'","\'",$_POST['desc_juniper']);
    $politicas=str_replace("'","\'",$_POST['polit_juniper']);
    
    if ($_POST['preaviso'] == 'on'){
       $preaviso= 1;
    }
    else{
       $preaviso = 0;
    }
    
    
    $archivoJ = $_FILES['archivo']['name'][0];
    $prefijoJ = substr(md5(uniqid(rand())),0,6);
    if ($archivoJ != "")
    {
        $destinoJ = "../Imagenes/img-juniper/".$prefijo."_".$archivoJ;
        $fileJ = $prefijoJ."_".$archivoJ;
            if (copy($_FILES['archivoJ']['tmp_name'][0],$destinoJ)) {
                $file_concat = $fileJ.",".$file_concat;
                $status = "Archivo subido: <b>".$archivoJ."</b>";
            }
            else
            {
                $status = "Error al subir el archivo ".$archivo;
            }
    }

    
    ######END JUNIPER ########
    
    
    if ($_POST['pack'] == 'on'){
       $pack = 1;
    }
    else{
       $pack = 0;
    }
    
    if ($_POST['vendedorpp'] == "on"){
        $vendedorpp = 1;
    }else{
        $vendedorpp = 0;
    }
    
    if ($_POST['disp_chd'] == "on"){
        $disp_chd = 1;
    }else{
        $disp_chd = 0;
    }	
    
    if ($_POST['send'] == "1"){
        $send = 1;
    }else{
        $send = 0;
    }
    
    $hr_correo_preaviso = $_POST['hora_correo_preaviso'].":".$_POST['min_correo_preaviso'];
       
    
	$miConexion->Conectar();
	$lim=$_POST['limite']*10000;
	$lim_t=$_POST['limite_t']*10000;
    
    $descripciones_corta=str_replace("'","\'",$_POST['descripcion_corta']);
	$descripciones=str_replace("'","\'",$_POST['descripcion']);
    $descripciones_eng=str_replace("'","\'",$_POST['descripcion_eng']);
    $descripciones_por=str_replace("'","\'",$_POST['descripcion_por']);
    $hora_correo = $_POST['hora_correo'].":".$_POST['min_correo'];

    
	$procMasterPines=$miConexion->EjecutaSP('Ingresar_Excursion_Origen_',"'".$_POST['nombre']."', '".$_POST['nombre_abr']."', '".$descripciones_corta."', '".$descripciones."', '".$descripciones_eng."', '".$descripciones_por."','".$_POST['precio']."','".$_POST['cat1']."','".$_POST['cat2']."','".$_POST['cat3']."','".$_POST['cat4']."','".$_POST['cat5']."',
                                                                         '".$_POST['vendedores']."','".$_POST['vendedores_int']."','".$_POST['hora'].':'.$_POST['mini']."','".$lim_t."','".$lim."','".$_POST['correo']."','".$_POST['correo_alerta']."','".$_POST['descuento']."', ".$_POST['DBASICO'].",".$_POST['DPREMIUM'].",".$_POST['PORMAX'].",
                                                                         ".$_POST['VALOR_AFECTO'].",".$_POST['VALOR_CHILD'].", ".$pack.", '".$file_concat."', '".$hora_correo."', '".$_POST['correo_auto']."' ,
                                                                         '".$nemotecnico."', '".$descripcion_juniper."', '".$politicas."', '".$hr_correo_preaviso."', '".$fileJ."', $vendedorpp, $disp_chd,$send,  @respuesta,@mensaje");
    $parametrosSalida=$miConexion->ObtenerResultadoSP("@respuesta,@mensaje");
	$rowverificacion= mysql_fetch_assoc($parametrosSalida);
	if($rowverificacion['@mensaje']<>'')
	{
	print("<script>alert('".$rowverificacion['@mensaje']."');</script>");
	}
	echo "<META HTTP-EQUIV='refresh' CONTENT='0; URL=$PHP_SELF'>"; 	
	mysql_free_result($parametrosSalida); 
	mysql_close(); 

}
$tpl->printToScreen();
?>