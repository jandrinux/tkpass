<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:22px;
	top:156px;
	width:822px;
	height:232px;
	z-index:13;
}
#Layer3 {
	position:absolute;
	left:18px;
	top:56px;
	width:379px;
	height:62px;
	z-index:14;
}
-->
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Ingreso Usuario</title>
<style type="text/css">
<!--
#Layer2 {	position:absolute;
	left:878px;
	top:48px;
	width:132px;
	height:116px;
	z-index:1;
}
-->
</style>
<script type="text/JavaScript">

function pres_espacio()
	{
		if(event.keyCode==32)
			{
				event.returnValue = false;
				return;
			}
		
		event.returnValue = true;
	}		

function Popup()
	{
 		miPopup = window.open("../PHP/Ingreso_Ubicacion.php","miwin","top=200,left=350,width=550,height=200,scrollbars=yes,titlebar=no,location=no");
 		miPopup.focus();	
	}
	
function pres_num()
	{
		if((event.keyCode<48) || (event.keyCode>57))
			{
				event.returnValue = false;
			}		
	}
	
function validar()
	{
    	// Primer paso: Obtener el rut que ingreso el usuario
   		 var rutCompleto = document.form1.rutusuario.value;
		// Eliminamos los caracteres raros, espacios, puntos, guiones.
    	// Pasamos a minusculas, y separamos el rut y el digito verificador
    	rutCompl = rutCompleto.toLowerCase();
   		var dv=document.form1.dvusuario.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
   		 valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   		 valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
        		alert("El Rut no es Valido.Ingreselo Nuevamente");
        		document.form1.rutusuario.value="";
				document.form1.dvusuario.value="";
				document.form1.rutusuario.focus();
   			 }
	}

function verificarDV(rut, dv)
	{
   		var multiplicador = 9;
    	var aux = rut;
    	var suma = 0;
    	while(aux > 0)
			{
        		var unidades = aux % 10 ;
        		aux = (aux - unidades) / 10;
        		suma += (multiplicador * unidades);
        		multiplicador--;
        		if (multiplicador < 4)
					{
            			multiplicador = 9;
       				 }
    		}
    	digito = suma % 11;
    	if (digito == 10)
			{
        		digito = "k";
    		}
    	if (dv == digito)
			{
        		return true;
    		}
    	else 
			{
       			 return false;
    		}
	} 

function isEmailAddress(email)
	{
		var s = email;
		var filter=/^[A-Za-z][A-Za-z0-9_]*@[A-Za-z0-9_]+\.[A-Za-z0-9_.]+[A-za-z]$/;
		if (s.length == 0 ) return true;
		if (filter.test(s))
		return true;
		else
		alert("Ingrese una direcci�n de correo v�lida");
		document.form1.email.focus();
		return false;
}

function IngresarUsuario()
	{		
		if (document.form1.rutusuario.value=='')
			{
				alert("Debe Ingresar el Rut del Usuario que desea Ingresar");
				document.form1.rutusuario.focus();
				return;
			}
		if (document.form1.dvusuario.value=='')
			{
				alert("Debe Ingresar el Digito Verificador del Rut del usuario que desea agregar");
				document.form1.dvusuario.focus();
				return;
			}

		if (document.form1.login.value=='')
			{
				alert("Debe Ingresar el Login");
				document.form1.login.focus();
				return;
			}

		if (document.form1.password.value=='')
			{
				alert("Debe Ingresar la Contrase�a para el usuario que desea ingresar");
				document.form1.password.focus();
				return;
			}
		
		if (document.form1.nombre.value=='')
			{
				alert("Debe Ingresar el Nombre");
				document.form1.nombre.focus();
				return;
			}
		if (document.form1.apellido.value=='')
			{
				alert("Debe Ingresar el Apellido");
				document.form1.apellido.focus();
				return;
			}
		if (document.form1.direccion.value=='')
			{
				alert("Debe Ingresar la Direcci�n para el Usuario que desea Ingresar");
				document.form1.direccion.focus();
				return;
			}
		if (document.form1.comuna.value=='')
			{
				alert("Debe Ingresar la Comuna a la cual pertenece el Usuario");
				document.form1.comuna.focus();
				return;
			}
		if (document.form1.ciudad.value=='')
			{
				alert("Debe Ingresar la Ciudad a la cual pertenece el Usuario");
				document.form1.ciudad.focus();
				return;
			}
		if (document.form1.corriente.value=='')
			{
				alert("Debe seleccionar si tiene o no cuenta corriente");
				document.form1.corriente.focus();
				return;
			}
		if (document.form1.categoria.value=='')
			{
				alert("Debe seleccionar la categoria");
				document.form1.categoria.focus();
				return;
			}	
		if (document.form1.telefono.value=='')
			{
				alert("Debe ingresar Telefono");
				document.form1.telefono.focus();
				return;
			}	
		
    	var rutCompleto = document.form1.rutusuario.value;
		rutCompl = rutCompleto.toLowerCase();
    	var dv=document.form1.dvusuario.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
    	valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   	 	valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
       	 		alert("El Rut no es Valido.Ingreselo Nuevamente");
        		document.form1.rutusuario.value="";
				document.form1.dvusaurio.value="";
    		}
		
		document.form1.guardar.value="guardar";
		document.form1.submit();
	}

function NuevoTipoUsuario()
	{
		miPopup = window.open("../PHP/Mantenedor_TipoUsuario.php?flag=1&condicion=3","cue","top=200,left=400,width=550,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
		
function NuevoDistribuidor()
	{
		miPopup = window.open("../PHP/Ingreso_Cliente.php?flag=1","cue","top=200,left=400,width=950,height=500,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
function IngresarCuentaContable()
	{
		miPopup = window.open("../PHP/Ingresar_Cuenta_Contable.php?flag=1","cuentacontable","top=200,left=400,width=550,height=350,scrollbars=NO,titlebar=no,location=no");
		miPopup.focus();
	}
function tipo()
	{
		document.form1.tipo.value=1;
		document.form1.submit();
	}
function SubmitFormulario()
	{	
		document.form1.submit();
	}
function Limpiar()
	{
		document.form1.rutusuariovalue="";
		document.form1.dvusuario.value="";
		
		document.form1.usuario.value="";
		document.form1.passwordusuario.value="";
		document.form1.nombreusuario.value="";
		document.form1.apellido.value="";
		document.form1.direccion.value="";
		document.form1.telefono.value="";
		document.form1.comuna.value="";
		document.form1.ciudad.value="";
		//document.form1.contactodistribuidor.value="";
		document.form1.email.value="";
		document.form1.celular.value="";
	}
	

</script>
</head>

<body oncontextmenu="return false">
<form id="form1" name="form1" method="post" action="">
<div id="tablageneral" style="position:absolute; left:17px; top:156px; width:884px; height:152px; z-index:3; border:0pt black solid;">

    <table width="859" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="151" class="celdalazul"> Nombre </td>
        <td width="280" class="celdalCELEST"><input name="nombre" type="text" class="seleccionTurist_250" id="nombre" value="{nombre}" {valornombreusuario} />          <!--<img src="../Imagenes/configurar19x19.png" width="19" height="19" alt="Nuevo tipo Usuario" style="cursor:hand" onclick="NuevoTipoUsuario()"/>-->	    </td>
        <td width="126" class="celdalazul">Apellido</td>
        <td width="302" class="celdalCELEST"><input name="apellido" type="text" class="seleccionTurist_250" value="{apellido}" {valorapellido} /></td>
      </tr>
      <tr>
        <td class="celdalazul">Rut</td>
        <td class="celdalCELEST"><label>
        <input name="rutusuario" type="text" class="seleccionTurist_180" onkeypress="pres_num();" value="{rut}" maxlength="10" {valorrut}  />
- 
<input name="dvusuario" type="text" class="seleccionT80_a" onchange="validar()" value="{dv}" maxlength="1" {valordv}/>
</label></td>
        <td class="celdalazul">&nbsp;</td>
        <td class="celdalCELEST">&nbsp;</td>
      </tr>
      <tr>
        <td class="celdalazul">Login</td>
        <td class="celdalCELEST"><input name="login" type="text" class="seleccionTurist_180" id="login" value="{login}" {VALORCOMUNA} /></td>
        <td class="celdalazul">Contrase&ntilde;a</td>
        <td class="celdalCELEST"><input name="password" type="password" class="seleccionTurist_130" id="password2" onkeypress="pres_espacio()" value="{password}" maxlength="20" {valorpasswordusuario} /></td>
      </tr>
      
      <tr>
        <td class="celdalazul">Direcci&oacute;n</td>
        <td class="celdalCELEST"><input name="direccion" type="text" class="seleccionTurist_250" id="direccion3" value="{direccion}" {valordireccion} /></td>
        <td class="celdalazul">Comuna</td>
        <td class="celdalCELEST"><input name="comuna" type="text" class="seleccionTurist_180" id="comuna2" value="{comuna}" {VALORCOMUNA} /></td>
      </tr>
      <tr>
        <td class="celdalazul">Ciudad</td>
        <td class="celdalCELEST"><input name="ciudad" type="text" class="seleccionTurist_180" id="comuna3" value="{ciudad}" {VALORCOMUNA} /></td>
        <td class="celdalazul">Cuenta Corriente </td>
        <td class="celdalCELEST"><select name="corriente" class="seleccionTurist_180"  onchange="SubmitFormulario()" >
          <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
          <!-- START BLOCK : corriente -->
          <option value="{CODIGO}" class="seleccion2" {seleccionar_}>{DESCRIPCION}</option>
          <!-- END BLOCK : corriente -->
        </select></td>
      </tr>
      <tr>
        <td class="celdalazul">Categor&iacute;a Distribuidor</td>
        <td class="celdalCELEST"><label><select name="categoria" class="seleccionTurist_180"  onchange="SubmitFormulario()" >
          <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
          <!-- START BLOCK : categoria -->
          <option value="{CODIGO_C}" class="seleccion2" {seleccionar_c}>{DESCRIPCION_C}</option>
          <!-- END BLOCK : categoria -->
        </select>       </label></td>
        <td class="celdalazul">Telefono</td>
        <td class="celdalCELEST"><input name="telefono" type="text" class="seleccionTurist_180" onkeypress="pres_num();" value="{telefono}" {VALORTELEFONO} /></td>
      </tr>


      

      <tr>
        <td><input type="hidden" name="validacion" {VALIDACION} /></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </div>

<div class="TituloPlomo" id="Layer3">
  <table width="312" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="82" rowspan="4"><img src="../Imagenes/Crystal_Clear_app_Login_Manager80x80_marco.png" width="80" height="80" /></td>
      <td width="13" rowspan="4">&nbsp;</td>
      <td width="217"><p>&nbsp;</p>      </td>
    </tr>
    
    <tr>
      <td height="22" class="TituloPlomo"><strong>Ingreso Usuario </strong><strong></strong></td>
    </tr>
    <tr>
      <td height="22" class="TituloFecha">{fecha}</td>
    </tr>
    <tr>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
  </table>
</div>
<div id="boton" style="position:absolute; left:353px; top:344px; width:196px; height:25px; z-index:3; border:0pt black solid;">
<input name="Submit" type="button" class="BotonTurist_Celest" value="Ingresar" onclick="IngresarUsuario()" />
<input type="hidden" name="guardar" value="0" />
</div>

</form>
</body>
</html>
