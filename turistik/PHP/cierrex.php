<?php
    session_start();
    include("menus.php");
    $_Util=new Util;

   // $hora_actual = date("H");
    $hora_actual  = 19;
    $fecha_actual = date("Y-m-d");

    if ($hora_actual < 7) {
      $nuevafecha = strtotime ( '-1 day' , strtotime ( $fecha_actual ) ) ;
      $fecha_desde =date("Y-m-d",$nuevafecha). " 07:00:00";
      $fecha_hasta = $fecha_actual. " 02:59:59";
    }
    else
    {
      $fecha_desde = $fecha_actual. " 07:00:00";
      $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha_actual ) ) ;
      $fecha_hasta =date("Y-m-d",$nuevafecha). " 02:59:59";
    }


    $miConexion->Conectar();
    $query = $miConexion->EjecutaSP("Consulta_valores_cierre_x","  ".$_SESSION['codigo_suc'].", '".$fecha_desde."', '".$fecha_hasta."',  @monto_e, @monto_d, @monto_dep, @monto_e_ra, @monto_d_ra ");
    $Salida=$miConexion->ObtenerResultadoSP("@monto_e, @monto_d, @monto_dep, @monto_e_ra, @monto_d_ra");
    $r = mysql_fetch_assoc($Salida);
    $monto_e = $r['@monto_e']; 
    $monto_d = $r['@monto_d']; 
    $monto_dep = $r['@monto_dep']; 
    $monto_e_ra = $r['@monto_e_ra']; 
    $monto_d_ra = $r['@monto_d_ra']; 

    $monto_e = $monto_e - $monto_dep + $monto_e_ra;
    $monto_d = $monto_d + $monto_d_ra;
    

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />

<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">
  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Informe X</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    

  </table>
  
    

<div id="Layer1">
  <div id="dialog" title="Confirmaci&oacute;n de Cierre.">
</div>

<!-- Aqui va el cuerpo -->
<form name="form1" id="form1" method="post" action="save_cierrex.php" >

<table width="500"  border="0" cellspacing="0" style="margin-botom:30px";>
  <tr>
    <td width="200"  class="celdalazul_peque">Monto Efectivo:</td>
    <td width="200"  class="celdalCELEST">    
      <input name="monto" class="seleccionTurist_200" id="monto" readonly="" value="<?=number_format($monto_e)?>"></input>
    </td>  
  </tr>
  <tr>
    <td width="200"  class="celdalazul_peque">Confirmar Monto Efectivo: (*)</td>
    <td width="200"  class="celdalCELEST">    
      <input name="monto_efectivo" class="seleccionTurist_200" id="monto_efectivo"  value="<?=number_format($monto_e)?>" ></input>
    </td>  
  </tr>
  <tr>
    <td width="200" class="celdalazul_peque">Total Ventas Transbank:</td>
    <td width="200" class="celdalCELEST">
        <input name="monto_voucher" class="seleccionTurist_200" id="monto_voucher" readonly="" value="<?=number_format($monto_d)?>" ></input>
        <a onclick="abrir_popup('CONSULTA_detalle_tbk_modal.php', 500, 500);"><img src="../Imagenes/ico_alpha_Search2_16x16.png" width="16" height="16" style="cursor:hand"></a>
    </td>  
  </tr>
  <tr id="tb_obs" style="display:none";>
    <td width="200" class="celdalazul_peque">Observacion:</td>
    <td width="200" class="celdalCELEST">
      <textarea name="observacion" class="seleccionTurist_200" id="observacion"></textarea>
    </td>  
  </tr>
</table>
<br />

<table width="500"  border="0" cellspacing="0" >
<tr>
    <td width="200" class="celdalazul_peque">Monto Deposito:</td>
    <td width="200" class="celdalCELEST">
        <input name="monto_deposito" class="seleccionTurist_200" id="monto_deposito" onkeypress="return validarP3(event);" ></input>
    </td>  
  </tr>
  <tr>
    <td width="200" class="celdalazul_peque">Confirmaci&oacute;n Monto Deposito:</td>
    <td width="200" class="celdalCELEST">
        <input name="con_monto_deposito" class="seleccionTurist_200" id="con_monto_deposito" onkeypress="return validarP3(event);" ></input>
    </td>  
  </tr>
    <tr>
    <td width="200" class="celdalazul_peque">N&#176; Deposito:</td>
    <td width="200" class="celdalCELEST">
        <input name="num_deposito" class="seleccionTurist_200" id="num_deposito" onkeypress="return validarP3(event);" ></input>
    </td>  
  </tr>

  </tr>
    <tr>
    <td width="200" class="celdalazul_peque">Confirmaci&oacute;n N&#176; Deposito:</td>
    <td width="200" class="celdalCELEST">
        <input name="con_num_deposito" class="seleccionTurist_200" id="con_num_deposito" onkeypress="return validarP3(event);" ></input>
    </td>  
  </tr>
</table>


<input type="button" style="margin-left: 200px; margin-top:30px;" class="BotonTurist_Celest" name="save" id="save" value="Aceptar" />
</form>

<div id="resultado"><? include('CONSULTA_valida_cierrex.php'); ?></div>


</div>


<script type='text/javascript'>
/*
function callback(value) {
    if (value) {
        $("#form1").submit();
    } else {
        return false;
    }
}

function diferencia(monto_dep,num_dep)
{
    if (monto_dep == "" && num_dep == "") {
      return 1;
    }
    return 2;
}
*/
function validar()
{
  $("#form_valida").submit();
}

$(document).on("ready",function(){




// Solo acepta n�meros
function validar_numero(e) { 
    tecla = (document.all) ? e.keyCode : e.which; 
    if (tecla==8) 
        return true; 
    patron = /\d/; 
    te = String.fromCharCode(tecla); 
    return patron.test(te); 
}

$("#monto_efectivo").on("keypress",function(e){
  if(validar_numero(e)) {
   
  }else
  {
    return false;
  }

});
$("#monto_efectivo").on("keyup",function(e){
    var monto = $("#monto").val();
    var monto_efectivo = $("#monto_efectivo").val();

    if (monto != monto_efectivo) {
        $("#tb_obs").show();
    }
    else
    {
        $("#tb_obs").hide();
    }
});

$("#save").click(function() { 

    var monto = $("#monto").val();
    var monto_efectivo = $("#monto_efectivo").val();
    var monto_dep = $("#monto_deposito").val();
    var num_dep = $("#num_deposito").val();
    var con_monto_deposito = $("#con_monto_deposito").val();
    var con_num_deposito = $("#con_num_deposito").val();


    if (monto_efectivo == "" ) {
        alert("Ingrese campos obligatorios (*)");
        return false;
    }

    if (monto_dep != con_monto_deposito) {
      alert("Error de Confirmacion de Monto Deposito");
      $("#con_monto_deposito").focus();
      return false;
    }

    if (monto_dep != "" && num_dep == "") {
      alert("Ingrese Numero de Deposito");
      $("#num_deposito").focus();
      return false;
    };

    if (num_dep != con_num_deposito) {
      alert("Error de Confirmacion de Numero de Deposito");
      $("#con_num_deposito").focus();
      return false;
    }

     $("#form1").submit();

/*
    var titulo = "<p style='padding-bottom:20px';>Usted acepta que la informaci&oacute;n de caja es la siguiente.</p>";
    var texto_monto = "<p style='padding-bottom:10px';>Monto Efectivo: <b>"+monto_efectivo+"</b>.</p>";
    var texto_dep = "<p style='padding-bottom:10px';>Dep&oacute;sito: <b>"+monto_dep+"</b>.</p>";
    var texto_num_dep = "<p style='padding-bottom:10px';>N&uacute;mero Dep&oacute;sito: <b>"+num_dep+"</b>.</p>";


    if (diferencia(monto_dep,num_dep) == 2)
    {
      var texto_completo = titulo+texto_monto+texto_dep+texto_num_dep;
    }
    else
    {
      var texto_completo = titulo+texto_monto;
    }

     $("#dialog").html(texto_completo);

     $( "#dialog" ).dialog({
        resizable: false,
        height:340,
        width:400,
        modal: true,
        buttons: {
          "Aceptar": function() {
            $( this ).dialog( "close" );
            callback(true);
          },
          Cancelar: function() {
            $( this ).dialog( "close" );
            callback(false);
          }
        }
      });
*/
});
  


});


</script>
</body>
</html>