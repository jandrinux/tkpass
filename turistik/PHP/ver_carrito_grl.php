<?
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
session_start();
if ($_SESSION["autentificado"] != "SI"){
    header("location: Login.php");
}

require ("set_timeout.php");
include ("../Clases/ClaseConexion.inc.php");
include ("../functions/consulta_promociones.php");
require ("../Clases/ClaseUtil.inc.php");

$miConexion = new ClaseConexion();
$consulta_promociones = new Consulta_Promociones();
$opt_chkd = $consulta_promociones->Consulta_promocion($_GET['opt']);
$bol = $consulta_promociones->Mostar_Boleta($_SESSION['codigo_suc']);
$result = $consulta_promociones->Consulta_opcion_Checked($opt_chkd);

if ($opt_chkd[1]== 'checked')
{
    $id_combo= "id='primero'"; // es combo
}

if ($opt_chkd[3]== 'checked')
{
    $id_ninguno = "id='primero'";
}

?>

<html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
.Estilo1 {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
    font-size: 18px;
}

 </style>
<head>
<title>.: TURISTIK - Carrito de compras :.</title>
<!--<script language="JavaScript" type="text/javascript" src="../../js/jquery-1.3.2.js"></script>-->
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<link href="../../css/menu_small.css" rel="stylesheet" type="text/css">
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<? include ('js_ver_carrito_grl.php'); ?>


</head>

<body>

<div id="Layer2"><img src="../Imagenes/logo_greenline.png" width="130" height="118" /></div>

<div class="TituloPlomo" id="Layer3">
<img src= '../Imagenes/carrito_grl.png' width="60" style="margin-left: 30px;">
<span class="Estilo1">Carrito de Compras</span>
</div>

<div id="Layer1">

<div id="menuh">
        <ul>
           <li><a href="ver_carrito_grl.php?opt=4" name="radio" <?=$id_ninguno?> >NINGUNO</a></li>
           <li><a href="ver_carrito_grl.php?opt=2" name="radio" <?=$id_combo?> >COMBOS</a></li>
        </ul>
</div>

<form name="formCompra" style="float: left;"  action="finaliza_compra_grl.php"   method="post">
<div id="resultado_listar"  style="margin:20px 0px 20px 0px; float:left;">

<?
    if ($opt_chkd[1] == 'checked')    
    {   
        include('CONSULTA_carrito_combos_grl.php');        
    }

    if ($opt_chkd[3] == 'checked') // ninguno
    { 
        include('CONSULTA_carrito_ninguno_grl.php'); 
    }
?>


</div>
<div id="combo_dscto" style="margin-left: 798px; margin-bottom: 10px;">
<select name="dscto_cordinador" id="dscto_cordinador" class="seleccionTurist_180" onchange="recalcular_porcentaje(this);">
    <option value="100" >NINGUNO</option>
    <option value="90">10</option>
    <option value="80">20</option>
    <option value="70">30</option>
    <option value="60">40</option>
    <option value="50">50</option>
    <option value="40">60</option>
    <option value="30">70</option>
    <option value="20">80</option>
    <option value="10">90</option>
    <option value="0" >100</option>
    
</select>
</div>

<fieldset id="fieldset">
<legend class="celdalLeyend">Forma de Pago</legend>
 <table align="center" id="tabla" width="770" border="0" cellpadding="0" cellspacing="0">
 <tr>
    <td class="celdalazul_peque">Tipo Reserva: (*)</td>
    <td class="celdalCELEST">
        <select name="tipoReserva" required="" title="Seleccione tipo de reserva" id="tipoReserva" class="seleccionTurist_180" >
            <?

                $miConexion->Conectar();
                $queryTipoConsulta=$miConexion->EjecutaSP("Consulta_Tipo_Reserva_Sucursal"," '".$_SESSION["codigo_suc"]."', 4");
                while ($rowTipoConsulta = mysql_fetch_assoc($queryTipoConsulta)){
                     echo "<option value=".$rowTipoConsulta['CODIGO_TR']." >".htmlentities($rowTipoConsulta['TRESERVA'])."</option>" ;
                }
                mysql_free_result($queryTipoConsulta); 
                mysql_close();
            ?>
        
        </select>
    </td>
    <td class="celdalazul_peque">Forma de Pago: (*)</td>
    <td class="celdalCELEST">
        <select name="fop" required="" title="Seleccione forma de pago" id="select_fop" class="seleccionTurist_180" onchange="info_boleta_tr();">
            <option value="S" selected="">-- SELECCIONE --</option>
            <?
             
                $miConexion->Conectar();
                $queryTipoConsulta=$miConexion->EjecutaSP("Consulta_Tipo_Reserva_Fop_Sucursal"," '".$_SESSION["codigo_suc"]."', 1 ");
                while ($row = mysql_fetch_assoc($queryTipoConsulta)){
                     echo "<option value=".$row['ID_FOP']." >".$row['Fop']."</option>" ;
                }
                mysql_free_result($queryTipoConsulta); 
                mysql_close();

            ?>

        </select>
    </td>
    <td></td>
    <td></td>
 </tr>
  <tr >
    <td class="celdalazul_peque"><div id="tituloTBK" style="visibility: hidden;" >C&oacute;digo Autorizaci&oacute;n: (*)</div></td>
    <td class="celdalCELEST"><input name="voucherTBK" id="info_boleta" style="visibility: hidden;"  class="seleccionTurist_180" type="text"/></td>
    
    <td class="celdalazul_peque"><div id="bolTTS">Boleta TTS: (*)</div> </td>
    <td class="celdalCELEST"><input name="boletaTTS" id="boletaTTS"  onkeypress="return validarP3(event);" class="seleccionTurist_180" type="text" value="<?=$bol?>" /></td>
    <td></td>
    <td></td>

 </tr>


  <tr id="tr_tarjeta" style="display: none;">
    <td class="celdalazul_peque">4 Ultimos Digitos Tarjeta: (*)</td>
    <td class="celdalCELEST" >
    <input name="ultimos_digitos" id="ultimos_digitos"  onkeypress="return validarP3(event);"  class="seleccionTurist_180" type="text" maxlength="4"/>
    </td>
    <td class="celdalazul_peque">Tipo de Tarjeta: (*)</td>	
    <td class="celdalCELEST">
        <select name="tipo_tarjeta" required="" title="Seleccione Tarjeta" id="tipo_tarjeta" class="seleccionTurist_180" >
        <option value="" selected>-- SELECCIONE --</option>
            <?
            echo "<option value='MASTERCARD'>MASTERCARD</option>";
            echo "<option value='VISA' >VISA</option>";
            echo "<option value='AMERICAN EXPRESS'>AMERICAN EXPRESS</option>";
            echo "<option value='OTRO'>OTRO</option>";
            ?>
        </select>
    </td>     
    <td></td>
    <td></td>
 </tr>

 
</table>

</fieldset>


    <div style="margin-left: 400px; margin-top: 15px;">

    <input name="tipoUsuario" id="tipoUsuario" type="hidden" value="<? echo $_SESSION["codigo_suc"]; ?>" />
    <input  type="button"  class="BotonTurist_largo" value="Vaciar carrito" onclick="vaciar_carrito()"/>
    <input type="button" name="continuar" id="continuar" class="BotonTurist_largo" onclick="carrito();" value="Seguir comprando" />
    <input type="button" name="comprar" class="BotonTurist_largo" onclick="finalizar()" value="Finalizar compra"/>
    <input type="hidden" name="estado" id="estado"/>
    <input type="hidden" name="estado_ttk" id="estado_ttk"/>
    <input name="boletaTTSCorrelativa" id="boletaTTSCorrelativa"  type="hidden" value="<?=$bol?>" />
    <input type="hidden" id="tipo_venta" name="tipo_venta" value="4" /> 
    <input type="hidden" name="radio" id="radio" value="<? echo $result; ?>" />

    </div>

  
</form>

</div> <!--end Layer1-->

</body>
<script type="text/javascript">

$( document ).on("ready",function() {
  var cod_tusua = <?=$_SESSION['cod_tusua'];?>;
  if (cod_tusua != 9)
  {
    $("#combo_dscto").hide();
  }
});

var ef = $('#tot_efectivo').val();
var ta = $('#tot_tarjeta').val();
var ex = $('#tot_exento').val();
var af = $('#tot_afecto').val();
</script>
</html>