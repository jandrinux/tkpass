<?php
    session_start();
    include("menus.php");
    $_Util=new Util;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<link href="../../css/menu_small.css" rel="stylesheet" type="text/css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>

<script type="text/javascript">


  function filtro(f)
  {
      hidden();

      if (f==1) { $("#tb_codigo").show(); };
      if (f==2) { $("#tb_codigo_manager").show();  };
      if (f==3) { $("#tb_sucursal").show(); };
  }

  function hidden()
  {
    $("#tb_codigo").hide();
    $("#tb_codigo_manager").hide();
    $("#tb_sucursal").hide();
  }

  function modificar(cod_manager, nombre, descripcion, cantidad, precio, codigo, sucursal )
  {
     $("#formulario").hide();
     $("#asignar").hide();
     $("#modificar").show();

     $("#mod_cod_manager").val(cod_manager);
     $("#mod_nombre").val(nombre);
     $("#mod_descripcion").val(descripcion);
     $("#mod_cantidad").val(cantidad);
     $("#mod_precio").val(precio);
     $("#mod_cod").val(codigo);
     $("#mod_sucursal").val(sucursal);
     


  }

  function asignar(cod_manager, nombre, descripcion, cantidad, precio, codigo, sucursal, cod_sucursal)
  {
     $("#formulario").hide();
     $("#modificar").hide();
     $("#asignar").show();

     $("#as_cod_manager").val(cod_manager);
     $("#as_nombre").val(nombre);
     $("#as_descripcion").val(descripcion);
     $("#as_cantidad").val(cantidad);
     $("#as_precio").val(precio);
     $("#as_cod").val(codigo);
     $("#as_suc").val(cod_sucursal);
     $("#as_sucursal option:selected" ).text(sucursal);
  }

</script>

</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">
  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Productos Merchandisign</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    

  </table>
  
<div id="Layer1">

<div id="menuh">
    <ul>
       <li><a href="productos_merch.php" >Agregar</a></li>
       <li><a href="listar_productos_merch.php" id="primero" >Listar / Asignar / Modificar</a></li>
    </ul>
</div>

<div id="formulario">

  <table width="500" border="0" cellspacing="0" >
    <tr>
      <td width="100" class="celdalazul_peque">Buscar </td>
      <td width="264" class="celdalCELEST">        
        <select id="filtros" class="seleccionTurist_180" onchange="filtro(this.value)">
            <option value="0" selected >Seleccione</option>
            <option value="1">Codigo</option>
            <option value="2">Codigo Manager</option>
            <option value="3">Sucursal</option>
        </select>
        
      </td>
    </tr>
  </table>


<form  name="form" method="post" onSubmit="listar_productos_merch(); return false" action="">
  <table id="tb_codigo" style="display:none" width="500" border="0" cellspacing="0" >
    <tr>
      <td width="100" class="celdalazul_peque">Codigo </td>
      <td width="264" class="celdalCELEST">        
        <input  id="codigo" value="" class="seleccionTurist_180"  name="codigo" />
        <input align="right" name="aceptar" id="aceptar" type="submit" class="BotonTurist_Azul" value="Aceptar" /> 
      </td>
    </tr>
  </table>

  <table id="tb_codigo_manager" style="display:none" width="500" border="0" cellspacing="0" >
    <tr>
      <td width="100" class="celdalazul_peque">Codigo Manager</td>
      <td width="264" class="celdalCELEST">        
        <input  id="codigo_manager" value="" class="seleccionTurist_180"  name="codigo_manager" />
        <input align="right" name="aceptar" id="aceptar" type="submit" class="BotonTurist_Azul" value="Aceptar" /> 
      </td>
    </tr>
  </table>

  <table id="tb_sucursal" style="display:none" width="500" border="0" cellspacing="0" >
    <tr>
      <td width="100" class="celdalazul_peque">Sucursal</td>
      <td width="264" class="celdalCELEST" >  
      <select name="sucursal" id="sucursal" class="seleccionTurist_180">    
        <?
          $miConexion = new ClaseConexion;   
          $miConexion->Conectar();
          $querySucursal=$miConexion->EjecutaSP("Consulta_Listar_Sucursales","'"."0"."'");//sucursalES
                
           while($rowSucursal = mysql_fetch_array($querySucursal)){ 
              echo '<option  value="'.$rowSucursal["CODIGO_SUCURSAL"].'"'.$select.'>'.$rowSucursal['SUCURSAL'].'</option>';
          } 
        ?>
        </select>        
        <input align="right" name="aceptar" id="aceptar" type="submit" class="BotonTurist_Azul" value="Aceptar" /> 
      </td>
    </tr>
</table>

</form>

<div id="resultado_listar"></div>

</div> <!--formulario-->

<div id="modificar" style="display:none">

    <form  name="form" method="post" onSubmit="modifica_productos_merch(); return false" action="">

    <table width="600" border="0" cellspacing="0" >
        <tr>
          <td width="100" class="celdalazul_peque">Codigo Manager</td>
          <td width="264" class="celdalCELEST">        
            <input  id="mod_cod_manager" value="" class="seleccionTurist_180"  name="mod_cod_manager" readonly="" />
          </td>
          <td width="100" class="celdalazul_peque">Nombre</td>
          <td width="264" class="celdalCELEST">        
            <input  id="mod_nombre" value="" class="seleccionTurist_180"  name="mod_nombre" />
        
          </td>
        </tr>
         <tr>
          <td width="100" class="celdalazul_peque">Descripcion</td>
          <td width="264" class="celdalCELEST">        
            <input  id="mod_descripcion" value="" class="seleccionTurist_180"  name="mod_descripcion" />
        
          </td>
          <td width="100" class="celdalazul_peque">Precio</td>
          <td width="264" class="celdalCELEST">        
            <input  id="mod_precio" value="" class="seleccionTurist_180"  name="mod_precio" onkeypress="return validarP3(event);"/>
          </td>
        </tr>

        <tr>
          <td width="100" class="celdalazul_peque">Cantidad</td>
          <td width="264" class="celdalCELEST">        
            <input  id="mod_cantidad" value="" class="seleccionTurist_180"  name="mod_cantidad" readonly="" onkeypress="return validarP3(event);"/>
        
          </td>
          <td width="100" class="celdalazul_peque">Codigo</td>
          <td width="264" class="celdalCELEST">        
            <input  id="mod_cod" value="" class="seleccionTurist_180"  name="mod_cod" readonly="" />
          </td>
        </tr>

        <tr>
          <td width="100" class="celdalazul_peque"></td>
          <td width="264" class="celdalCELEST" >   
          </td>
          <td width="264" class="celdalCELEST" colspan="2">        
            <input align="right" name="aceptar" type="submit" class="BotonTurist_Azul" value="Modificar" /> 
          </td>
          
        </tr>

    </table>
    <input  id="mod_sucursal" value="" type="hidden" />
    </form>


<div id="resultado_modifica"></div>

</div>


<div id="asignar" style="display:none">

    <form  name="form" method="post" onSubmit="asignar_productos_merch(); return false" action="">

    <table width="600" border="0" cellspacing="0" >
        <tr>
          <td width="100" class="celdalazul_peque">Codigo Manager</td>
          <td width="264" class="celdalCELEST">        
            <input  id="as_cod_manager" value="" class="seleccionTurist_180"  name="as_cod_manager" readonly="" />
          </td>
          <td width="100" class="celdalazul_peque">Nombre</td>
          <td width="264" class="celdalCELEST">        
            <input  id="as_nombre" value="" class="seleccionTurist_180"  name="as_nombre" readonly="" />
        
          </td>
        </tr>
         <tr>
          <td width="100" class="celdalazul_peque">Descripcion</td>
          <td width="264" class="celdalCELEST">        
            <input  id="as_descripcion" value="" class="seleccionTurist_180"  name="as_descripcion" readonly="" />
        
          </td>
          <td width="100" class="celdalazul_peque">Precio</td>
          <td width="264" class="celdalCELEST">        
            <input  id="as_precio" value="" class="seleccionTurist_180"  name="as_precio" readonly="" onkeypress="return validarP3(event);"/>
          </td>
        </tr>

        <tr>
          <td width="100" class="celdalazul_peque">Cantidad</td>
          <td width="264" class="celdalCELEST">        
            <input  id="as_cantidad" value="" class="seleccionTurist_180"  name="as_cantidad" onkeypress="return validarP3(event);"/>
        
          </td>
          <td width="100" class="celdalazul_peque">Codigo</td>
          <td width="264" class="celdalCELEST">        
            <input  id="as_cod" value="" class="seleccionTurist_180"  name="as_cod" readonly="" />
          </td>
        </tr>

        <tr>
          <td width="100" class="celdalazul_peque">Sucursal</td>
          <td width="264" class="celdalCELEST" >   
            <select name="as_sucursal" id="as_sucursal" class="seleccionTurist_180">    
            <?
              $miConexion = new ClaseConexion;   
              $miConexion->Conectar();
              $querySucursal=$miConexion->EjecutaSP("Consulta_Listar_Sucursales","'"."0"."'");//sucursalES
                    
               while($rowSucursal = mysql_fetch_array($querySucursal)){ 
                  echo '<option  value="'.$rowSucursal["CODIGO_SUCURSAL"].'"'.$select.'>'.$rowSucursal['SUCURSAL'].'</option>';
              } 
            ?>
            </select> 
          </td>
          <td width="264" class="celdalCELEST" colspan="2">        
            <input align="right" name="aceptar" type="submit" class="BotonTurist_Azul" value="Asignar" /> 
          </td>
          
        </tr>

        <input  id="as_suc" value="" type="hidden" />

    </table>
    </form>


<div id="resultado_asignar"></div>

</div>

</div>
</body>
</html>