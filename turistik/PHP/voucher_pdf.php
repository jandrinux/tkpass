<?
session_start();

include('../../lib/MPDF/mpdf.php');

/*echo "Voucher: ".$_SESSION['voucher'].
	$_SESSION['comprador'].
	$_SESSION['cantidad'].
	$_SESSION['producto'].
	$_SESSION['fecha_exc'].
	$_SESSION['lugares'].
	$_SESSION['telefono'].
	$_SESSION['monto'].
	$_SESSION['forma_pago'].
	$_SESSION['resumen'];*/

function text_to_ascii($cadena){
	$cadena=stripslashes($cadena); 
	$ascii = "";	
	for($i = 0; $i < strlen($cadena); $i++){
		$a = "&#";
		$ascii .= $a.ord(substr($cadena,$i));
		
		//$ascii.=",";
	}		
$ascii=substr($ascii, 0, -1);	
return $ascii;
}	
	
$html='
<style type="text/css">
.celda_bordes {
	font-family: Arial;
	font-size: 2px;
	line-height: 2px;
	color: #000000;
	background-color: #FFFFFF;
	background-repeat: repeat-x;
	border: 1px solid #c3c3c3;
}
.celda_peque&ntilde;a {
	font-family: Arial;
	font-size: 10px;
	line-height: 16px;
	color: #000000;
	background-color: #FFFFFF;
	background-repeat: repeat-x;
}
</style>
<body>
<table align="center" bgcolor="#c3c3c3" width="98%"><tr><td class="celda_bordes">
<table align="center" bgcolor="#FFFFFF">
<tr><td align="center"><img src="../Imagenes/turistik-3pisos_.png" border="0"></td></tr>
<tr><td align="center" class="celda_peque&ntilde;a"><b>'.$_SESSION['voucher'].'</b></td></tr>
<tr><td align="center">
</td></tr>
<tr><td>
<table>
<tr>
<td width="20" class="celda_peque&ntilde;a"></td><td class="celda_peque&ntilde;a"><br><br><b>Estimado(a) '.$_SESSION['comprador'].'</b><br></td><td width="20"></td>
</tr>
<tr>
	<td width="20" class="celda_peque&ntilde;a"></td>
	<td class="celda_peque&ntilde;a">
	Agradecemos su compra de:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>'.$_SESSION['cantidad']. 
	' TICKET(s) '.$_SESSION['producto'].'</b><br>El servicio ser&aacute; utilizado:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>'.$_SESSION['fecha_exc'].'</b>. 
	<br />Lugar de Salida / Place of Departure:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>'.$_SESSION['lugares'].'</b><br>Tel&eacute;fono de contacto Cliente:&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b><u>'.$_SESSION['telefono'].'</u></b><br/>
	Monto Total de la Compra:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>$'.$_SESSION['monto'].' PESOS CHILENOS.</b>
	<br>Forma de Pago:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>'.$_SESSION['forma_pago'].'</b>	
	</td>
	<td width="20"></td>	
	</tr>
	<tr><td colspan="2"></td></tr>		
	</table>
<table>
	<tr><td width="20" align="center" ></td>
	<td align="justify" class="celda_peque&ntilde;a"><br><br>'.$_SESSION['resumen'].'<br>
	<br><strong><br>INFORMACION RELEVANTE: Para hacer uso del servicio deber&aacute;s llevar una copia de esta confirmaci&oacute;n / Esta compra no es reembolsable / Cambios son permitidos hasta 48 horas antes de la hora del servicio / Despu&eacute;s de realizar tu tour agradecer&iacute;amos tus comentarios a trav&eacute;s de www.turistik.cl/comments / Si tienes alguna duda o inquietud, por favor ll&aacute;manos al (56-2) 220-1000.</strong>
	<br><strong><br><br>IMPORTANT INFORMATION: To use our services you must carry a copy of this confirmation / This purchase is non refundable / Changes are allowed up to 48 hours prior to the time of the service / After taking the tour we would appreciate your comments through www.turistik.cl/comments / If you have any questions or doubts, please call us at (56-2) 220-1000.
</strong>
</td><td width="20"></td></tr>
	</table></td></tr>
<tr><td></td></tr>
</td></tr></table>
</td>
</tr>
</table>
</body>
';

$mpdf=new mPDF('c');
$mpdf->SetDisplayMode('fullpage');
$stylesheet = file_get_contents('mpdfstyleA4.css');
$mpdf->WriteHTML($stylesheet,1);
$stylesheet = file_get_contents('mpdfstylePaged.css');
$mpdf->WriteHTML($html);

$dir = "../../archivos_PDF";
$nombre_archivo= substr($_SESSION['voucher'],30);
//save the file
if (!file_exists($dir)){
mkdir($dir,0777,true);
}
$documento = ''.$nombre_archivo.'.pdf';
$mpdf->Output($dir.'/'.$documento,'F');
$mpdf->Output();
/*
$mpdf=new mPDF('c');
$mpdf->SetDisplayMode('fullpage');
$stylesheet = file_get_contents('mpdfstyleA4.css');
//$mpdf->WriteHTML($stylesheet,1);
$stylesheet = file_get_contents('mpdfstylePaged.css');
$mpdf->WriteHTML($html);

/****METODO 2 *********************************
$dir = "../../archivos_PDF";
$nombre_archivo= $_SESSION['voucher'];
//save the file
if (!file_exists($dir)){
mkdir($dir,0777,true);
}
$documento = ''.$nombre_archivo.'.pdf';
$mpdf->Output($dir.'/'.$documento,'F');
$mpdf->Output();
*
/****METODO 2 *********************************
$dir = "../../archivos_PDF";
$nombre_archivo= "aaaaa.pdf";
if (!file_exists($dir)){
    mkdir($dir,0777,true);
}
copy($dir."/".$nombre_archivo);
$mpdf->Output($dir.'/'.$nombre_archivo,'F');
$mpdf->Output();
/*******************************************/

?>
