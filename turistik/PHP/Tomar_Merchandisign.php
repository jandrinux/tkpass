<?php
    session_start();
    include("menus.php");
    $_Util=new Util;
    unset($_SESSION['cadena']); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>

<script type="text/javascript">

    addEventListener('keydown',function(event){

        if (event.keyCode == 13) {
          // $("#aceptar").click();
           consulta_codigo_merchandisign();
        }
        return false;
    });

    $( document ).ready(function() {
      $("#codigo").focus();
      $("#tituloTBK").hide();
      $("#info_boleta").hide();
      $("#tituloTipoTarjeta").hide();
      $("#tipo_tarjeta").hide();   
      $("#titulo4Ultimos").hide(); 
      $("#ultimos_digitos").hide();  
        
    });

    function consulta_codigo_merchandisign()
    {

      divResultado = document.getElementById('resultado');
      divResultado.innerHTML= '<div style="width:40px; text-align:left; margin-top:10px;"><img src="../Imagenes/load.gif"></div>';

        var id = $("#codigo").val();

        if (id != "")
        {

            var accion = "consulta_codigo_merchandisign";
            ajax=objetoAjax();
            ajax.open("POST", "PROCESO_ajax.php",true);
            ajax.onreadystatechange=function() {
                if (ajax.readyState==4) {
                  debugger;
                  divResultado.innerHTML = ajax.responseText
                  $("#codigo").val("");
                  $("#codigo").focus();
                }
            }

            ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
            ajax.send("id="+id+"&accion="+accion)
            $("#codigo").val("");
          
        }

    }


    function eliminar_cadena(id)
    {
        accion="eliminar_cadena";
        divResultado = document.getElementById('resultado');
        
        var eliminar = confirm("Esta seguro de eliminar?");
        if ( eliminar ) {
          ajax=objetoAjax();
          ajax.open("GET", "PROCESO_ajax.php?accion="+accion+"&id="+id);
          divResultado.innerHTML= '<div style="width:500px; text-align:left;float:left; margin:10px 0px 10px 50px;"><img src="../Imagenes/load.gif"></div>';
          ajax.onreadystatechange=function() {
            if (ajax.readyState==4) {
              //mostrar resultados en esta capa
              divResultado.innerHTML = ajax.responseText
            }
          }

          ajax.send(null)
        }
    }


    function fop(f)
    {
        if (f == "C")
        {
          $("#tituloTBK").show();
          $("#info_boleta").show();
          $("#tituloTipoTarjeta").show();
          $("#tipo_tarjeta").show();   
          $("#titulo4Ultimos").show();
          $("#ultimos_digitos").show();  
        }

        else
        {
          if (f == "D") {

            $("#tituloTBK").show();
            $("#info_boleta").show();
            $("#tituloTipoTarjeta").hide();
            $("#tipo_tarjeta").hide();   
            $("#titulo4Ultimos").hide();
            $("#ultimos_digitos").hide();           
          }
          else
          {
            $("#tituloTBK").hide();
            $("#info_boleta").hide();
            $("#tituloTipoTarjeta").hide();
            $("#tipo_tarjeta").hide();   
            $("#titulo4Ultimos").hide(); 
            $("#ultimos_digitos").hide();  
          }

        }
    }

function valida_tarjetas()
{
    var tbk = $("info_boleta").val();
    var ultimos_digitos = $('#ultimos_digitos').val();
    var tipo_tarjeta = $('#tipo_tarjeta').val();
    var cod_auto = $("#info_boleta").val();

    if(tbk == '')
    {
        alert("Ingrese Voucher Transbank.");
        document.getElementById('info_boleta').focus();
        return false;
    }
    
    if (ultimos_digitos.length < 4)
    {
        alert("Complete los 4 ultimos digitos de la Tarjeta Bancaria");
        document.getElementById('ultimos_digitos').focus();
        return false; 
    }
    
    if (tipo_tarjeta == '')
    {
        alert("Seleccione Tipo de Tarjeta");
        document.getElementById('tipo_tarjeta').focus();
        return false;
    }
    
    if (cod_auto == "") {

        alert("Ingrese Codigo de Autorizacion");
        $("#info_boleta").focus();
        return false;
    }

    
    return true;
}

    function finalizar()
    {
        var fop = $("#select_fop").val();
        var cod_auto = $("#info_boleta").val();
        var tipo_tarjeta = $("#tipo_tarjeta").val();  
        var ultimos_digitos =  $("#ultimos_digitos").val(); 

        if (fop == "S")
        {
           alert("Seleccione Forma de Pago");
           return false;
        }

        if (fop != "E") {

            if (valida_tarjetas() == false)
            {
              return false;
            } 
        };

        $.post("finaliza_merchandisign.php",{ fop:fop, cod_auto:cod_auto, tipo_tarjeta:tipo_tarjeta, ultimos_digitos:ultimos_digitos },function(data)
        {

            if (data == "no")
            {
              alert("Hubo un problema a intentar cerrar la venta, Inicie nuevamente su sesion");
              return false;
            }
            else
            {
              window.location.href = data; //Will take you to Google.
            }

        });
    }

</script>

</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">
  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Venta Merchandisign</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    

  </table>
  
    

<div id="Layer1">

  <form name="form1" method="post" onsubmit="consulta_codigo_merchandisign(); return false" >
  <table width="550" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="100" class="celdalazul_peque">Codigo Producto</td>
      <td width="264" class="celdalCELEST">        
        <input  id="codigo" value="" class="seleccionTurist_180"  name="codigo" />
        <input align="right" name="aceptar" id="aceptar" type="submit" class="BotonTurist_Azul" value="Consultar" /> 
      </td>
    </tr>


  </table>
</form>

<div id="resultado" ></div>

<!-- Aqui va el cuerpo -->
  <fieldset id="fieldset" style="margin-top:20px; padding: 10px 10px 10px 10px;">
     <table align="center" id="tabla" width="770" border="0" cellpadding="0" cellspacing="0">
       <tr>
          <td class="celdalazul_peque" width="192">Forma de Pago: (*)</td>
          <td class="celdalCELEST" width="192">
              <select name="fop"  title="Seleccione forma de pago" id="select_fop" class="seleccionTurist_180" onchange="fop(this.value)" >
                  <option value="S" selected="">-- SELECCIONE --</option>
                  <option value="E" >-- EFECTIVO --</option>
                  <option value="C" >-- CREDITO --</option>
                  <option value="D" >-- DEBITO --</option>
              </select>
          </td>
          <td class="celdalazul_peque" width="192"><div id="tituloTBK" >C&oacute;digo Autorizaci&oacute;n: (*)</div></td>
          <td class="celdalCELEST" width="192" >
              <input name="voucherTBK" id="info_boleta"  class="seleccionTurist_180" type="text"/>
          </td>
       </tr>
        <tr>
          <td class="celdalazul_peque"><div id="titulo4Ultimos" >4 Ultimos Digitos Tarjeta: (*)</div></td>
          <td class="celdalCELEST" >
          <input name="ultimos_digitos" id="ultimos_digitos"  onkeypress="return validarP3(event);"  class="seleccionTurist_180" type="text" maxlength="4"/>
          </td>
          <td class="celdalazul_peque"><div id="tituloTipoTarjeta">Tipo de Tarjeta: (*)</div></td>  
          <td class="celdalCELEST">
              <select name="tipo_tarjeta" required="" title="Seleccione Tarjeta" id="tipo_tarjeta" class="seleccionTurist_180" >
              <option value="" selected>-- SELECCIONE --</option>
                  <?
                  echo "<option value='MASTERCARD'>MASTERCARD</option>";
                  echo "<option value='VISA' >VISA</option>";
                  echo "<option value='AMERICAN EXPRESS'>AMERICAN EXPRESS</option>";
                  echo "<option value='OTRO'>OTRO</option>";
                  ?>
              </select>
          </td>     
       </tr>
    </table>
  </fieldset>

    <div style="float:right; margin-top:20px">
      <input type="button" name="comprar" class="BotonTurist_largo" onclick="finalizar()" value="Finalizar compra"/>
    </div>


</div>
</body>
</html>