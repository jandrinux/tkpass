<?php
Class Consulta_Carrito_Promociones
{
     /**
     * COMIENZO CARRITO DE PROMOCIONES
     */
    
    
    function Consulta_promociones()
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $SQL = " SELECT ID_PROMO, CANT_EXCURSIONES FROM PROMOCIONES
                 WHERE ESTADO =1 and COMBO=0";
        
        $query=$miConexion->EjecutaConsulta($SQL);
            while($row = mysql_fetch_array($query))
            {            
                $promos[] = $row['ID_PROMO'];
                $cant_ex[] = $row['CANT_EXCURSIONES'];

            }
        mysql_free_result($query); 
        mysql_close(); 
        
        
        return array ($promos, $cant_ex);
    }
    
    
    function Consulta_Cantidad_excursiones($sucursal, $cod_usua)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        /*
        $SQL = " SELECT PASAJERO, COUNT(COD_EXCURSION) AS EX FROM RESERVA_EXCURSION_TEMPORAL 
                    WHERE 
                    CODIGO_SUCURSAL = $sucursal and
                    COD_USUA = $cod_usua
                    GROUP BY PASAJERO
                    ORDER BY EX";
          */          
        $SQL = "SELECT RT.PASAJERO, COUNT(DISTINCT(RT.COD_EXCURSION)) AS EX, RE.LUGAR_RE FROM RESERVA_EXCURSION_TEMPORAL RT
                INNER JOIN RESERVA_EXCURSION RE ON RE.ID_TEMPORAL = RT.ID_TEMPORAL
                INNER JOIN EXCURSIONES_PROMOCIONES EP ON EP.ID_EXCURSION = RT.COD_EXCURSION
                WHERE 
                RT.CODIGO_SUCURSAL = $sucursal and
                RT.COD_USUA = $cod_usua and
                RT.ESTADO = 1";
        
        $query=$miConexion->EjecutaConsulta($SQL);
            while($row = mysql_fetch_array($query))
            {            
                $cant[] = $row['EX'];
                $lugar[] = $row['LUGAR_RE'];
            }
        mysql_free_result($query); 
        mysql_close(); 
        
        return array ($cant, $lugar);
    } 
    
    
    function Consulta_promocion_suc_nac($sucursal, $nacionalidad)
    {

        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $sql = "SELECT distinct(SP.PROMOCION)
        FROM SUCURSALES_PROMOCIONES SP
        WHERE SP.SUCURSAL = ".$sucursal." AND
        SP.ESTADO =  1 AND
        SP.PROMOCION IN (SELECT NP.ID_PROMO
                          FROM NACIONALIDAD_PROMOCIONES NP
                          WHERE 
                          NP.ID_PROMO = SP.PROMOCION and 
                          NP.ID_NACIONALIDAD = ".$nacionalidad.")";
                          
        $query=$miConexion->EjecutaConsulta($sql);
            while($row = mysql_fetch_array($query))
            {            
                $promo[] = $row['PROMOCION'];

            }
        mysql_free_result($query); 
        mysql_close(); 
        
        return $promo;
    }
    
    function Consulta_Nacionalidad($sucursal, $cod_usua)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $SQL = " SELECT NACIONALIDAD FROM RESERVA_EXCURSION_TEMPORAL 
                    WHERE 
                    CODIGO_SUCURSAL = $sucursal and
                    COD_USUA = $cod_usua and
                    ESTADO = 1
                    GROUP BY NACIONALIDAD";
        
        $query=$miConexion->EjecutaConsulta($SQL);
            while($row = mysql_fetch_array($query))
            {            
                $nacionalidad = $row['NACIONALIDAD'];

            }
        mysql_free_result($query); 
        mysql_close(); 
        
        return $nacionalidad;
    } 
    
    
    function Consulta_promocion_cliente($cliente)
    {
        
        
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        

            $sql = "SELECT ID_PROMO
            FROM CLIENTES_PROMOCIONES
            WHERE ID_CLIENTE = ".$cliente." ";
        

                          
        $query=$miConexion->EjecutaConsulta($sql);
            while($row = mysql_fetch_array($query))
            {   
                $promo[] = $row['ID_PROMO'];

            }
        mysql_free_result($query); 
        mysql_close(); 
        
        return $promo;
    }
    
        function Consulta_promocion_Lugar_Recogida($promo)
    {
        
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $sql = "SELECT ID_LUGAR_REC
                FROM LUGARES_REC_PROMOCIONES
                WHERE ID_PROMO = ".$promo." ";
                          
        $query=$miConexion->EjecutaConsulta($sql);
         while($row = mysql_fetch_array($query))
            {   
                $lugares[] = $row['ID_LUGAR_REC'];

            }
        
        mysql_free_result($query); 
        mysql_close(); 
        
        return $lugares;
    }
    
    
    function Consulta_promocion_by_Lugar_Recogida($promo, $lugar)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $sql = "SELECT ID_PROMO
                FROM LUGARES_REC_PROMOCIONES
                WHERE ID_PROMO = ".$promo." AND
                ID_LUGAR_REC = ".$lugar." ";
                          
        $query=$miConexion->EjecutaConsulta($sql);
         while($row = mysql_fetch_array($query))
            {   
                $promocion = $row['ID_PROMO'];

            }
        
        mysql_free_result($query); 
        mysql_close(); 
        
        return $promocion;
    }
    
    
    /**
     * FIN CARRITO PROMOCIONES
     */
}
?>