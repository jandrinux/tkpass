<?
class tkpass{ 
    
    var $cod_usuario;
    var $codigo_dist;
    var $tipoDistribuidor;
    var $fecha_format;
    var $tipo_excursion;
    var $usuario;
    var $total;
    var $cod_tusua;
    var $excursiones_stk;
   
    
    public function __construct() 
    {
        $this->cod_usuario = $_SESSION['codigo_usu'];
        $this->usuario = $_SESSION['usuario'];
        $this->codigo_dist = $_SESSION['codigo_dist'];
        $this->cod_tusua = $_SESSION['cod_tusua'];
        $this->excursiones_stk = array(6,54,55,63,64);
    }

    public function Actualiza_Reserva_Excursion($accion, $id_temporal, $tipoReserva, $fop, $voucherTBK, $voucher, $usuario, $codSocio, $cod_usuario, $cod_cierre, $suc_venta, $suc_reserva, $login_usua_cierre, $tipo_mayorista, $tipo_tarjeta, $ultimos_digitos)
    {
        $num_cotiza = "CO_".round(microtime(true) * 1000);
        $miConexion = new ClaseConexion; 
        $miConexion->conectar();   
        $miConexion->EjecutaSP("Actualiza_Reserva_Excursion"," ".$accion.", '".$id_temporal."', '".$tipoReserva."', '".$fop."', '".$voucherTBK."', '".$voucher."', '".$usuario."', '".$codSocio."', '".$cod_usuario."', '".$cod_cierre."', '".$suc_venta."', '".$suc_reserva."', '".$login_usua_cierre."', '".$tipo_mayorista."', '".$tipo_tarjeta."', '".$ultimos_digitos."', '".$num_cotiza."' ");
    }
    
    public function Consulta_Total_Precio_Comisionado($boarding_pass)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta(" SELECT SUM(PRECIO_COMISIONADO_RE) AS PRECIO FROM RESERVA_EXCURSION WHERE VOUCHER_RE = ".$boarding_pass." ");
        while ($row = mysql_fetch_array($query))
        {
            $precio = $row['PRECIO'];
        }
        mysql_free_result($query); 
        
        return $precio;
    }
    
    
    public function elimina_pasajero_temporal($accion, $id_temporal)
    {
        $objEliminaPasajero = new ClaseConexion; 
        $objEliminaPasajero->conectar();   
        $objEliminaPasajero->EjecutaSP("elimina_pasajero_temporal"," ".$accion.", '".$id_temporal."' ");                                                 

    }
    
    
    public function Consulta_descuentos_tipos_clientes($var, $tipo_usuario)
    {
        if ($tipo_usuario == 4)
        {
            $campo = 'ESTADO_EXTERNO';
        }
        else
        {
            $campo = 'ESTADO_TURISTIK';
        }
        
        
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta(" SELECT * FROM TIPOS_PASAJEROS WHERE ID_TIPO = ".$var." and $campo = 1 ");
            while($row = mysql_fetch_array($query))
            {                 
                $result=$row['DESCUENTO'];
            }
        mysql_free_result($query); 
        
        return $result;

    }
    
    public function Consulta_aplica_dscto_tipos_clientes($tipo_Excursion, $tipo_Cliente, $nacionalidad)
    {
        $result = 0;
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta(" SELECT ID_TIPO_CLIENTE FROM DSCTOS_TIPOS_CLIENTES WHERE ID_NACIONALIDAD = $nacionalidad and ID_EXCURSION = $tipo_Excursion ");
            while($row = mysql_fetch_array($query))
            {       
                if ($tipo_Cliente == $row['ID_TIPO_CLIENTE'] )
                {
                    $result = 1;
                }
            }
        mysql_free_result($query); 
        
        return $result;
    }
        
    
    public function transforma_fecha_sql($fecha){
    
        $fecha_sql = explode("-", $fecha);
        $anio=$fecha_sql[2]; // piece1
        $mes=$fecha_sql[1]; // piece2
        $dia=$fecha_sql[0]; // piece2
        $fecha = $anio."/".$mes."/".$dia;
        return $fecha;
    }
    
    
    public function Consulta_linea_credito()
    
    {
 
            $miConexion= new ClaseConexion;
            $miConexion->Conectar();
            $query=$miConexion->EjecutaConsulta(" SELECT LINEA_CREDITO, TRANSACCION FROM LINEAS_DE_CREDITOS WHERE COD_USUARIO = ".$this->cod_usuario." ");
                while($row = mysql_fetch_array($query))
                {
                    $Linea_credito = $row['LINEA_CREDITO'];
                    $Transaccion = $row['TRANSACCION'];
                }

            mysql_free_result($query); 
            
                        
            $array[0] = $Linea_credito;
            $array[1] = $Transaccion;
            
            return $array;
        
    }
    
    public function Consulta_linea_credito_Admin($cod_usua)
    
    {
 
            $miConexion= new ClaseConexion;
            $miConexion->Conectar();
            $query=$miConexion->EjecutaConsulta(" SELECT LINEA_CREDITO, TRANSACCION FROM LINEAS_DE_CREDITOS WHERE COD_USUARIO = ".$cod_usua." ");
                while($row = mysql_fetch_array($query))
                {
                    $Linea_credito = $row['LINEA_CREDITO'];
                    $Transaccion = $row['TRANSACCION'];
                }

            mysql_free_result($query);            
                        
            $array[0] = $Linea_credito;
            $array[1] = $Transaccion;
            
            return $array;
        
    }
    
    public function Actualiza_linea_credito($Linea)
    
    {
        
            $miConexion= new ClaseConexion;
            $miConexion->Conectar();
            $miConexion->EjecutaConsulta(" UPDATE LINEAS_DE_CREDITOS SET TRANSACCION = ".$Linea." WHERE COD_USUARIO = ".$this->cod_usuario." ");
  
    }
    
    public function Actualiza_linea_credito_Admin($cod_usua, $Linea)
    
    {
        
            $miConexion= new ClaseConexion;
            $miConexion->Conectar();
            $miConexion->EjecutaConsulta(" UPDATE LINEAS_DE_CREDITOS SET TRANSACCION = ".$Linea." WHERE COD_USUARIO = ".$cod_usua." ");

    }
    
    public function Consulta_tipo_distribuidor()
    {
       
        $objListaTipoReserva = new ClaseConexion;
        $objListaTipoReserva->Conectar();
        $queryTipoConsulta=$objListaTipoReserva->EjecutaSP("Consulta_Tipo_Reserva_Distribuidor"," '".$this->cod_usuario."' ");
        while ($rowTipoConsulta = mysql_fetch_assoc($queryTipoConsulta)){
            $tipoReserva = $rowTipoConsulta['CODIGO_TR'];
        }
        mysql_free_result($queryTipoConsulta); 
        
        return $tipoReserva;
    }
    
    public function Consulta_tipo_distribuidor_Admin($cod_usua)
    {
       
        $objListaTipoReserva = new ClaseConexion;
        $objListaTipoReserva->Conectar();
        $queryTipoConsulta=$objListaTipoReserva->EjecutaSP("Consulta_Tipo_Reserva_Distribuidor"," '".$cod_usua."' ");
        while ($rowTipoConsulta = mysql_fetch_assoc($queryTipoConsulta)){
            $tipoReserva = $rowTipoConsulta['CODIGO_TR'];
        }
        mysql_free_result($queryTipoConsulta); 
        
        return $tipoReserva;
    }
    
    public function Consulta_tipo_distribuidor_original($codigo_dist)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT CODIGO_TR_ORIGINAL FROM DISTRIBUIDOR WHERE CODIGO_DIST = ".$codigo_dist." ");
            while($row = mysql_fetch_array($query))
            {
                 $tipoReserva = $row['CODIGO_TR_ORIGINAL'];
            }

        mysql_free_result($query); 
        
        return $tipoReserva;
    }
    
    
    public function Consulta_Usuario_Cod_Distribuidor($distribuidor)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta(" SELECT COD_USUA FROM USUARIO WHERE CODIGO_DIST_USUA = ".$distribuidor." ");
            while($row = mysql_fetch_array($query))
            {
                $cod_usuario = $row['COD_USUA'];
            }

        mysql_free_result($query); 
        
        return $cod_usuario;
    }

    
    public function Actualiza_cambio_Tipodistribuidor($tipoDistribuidor)
    {
        
        
       if($tipoDistribuidor == 1)
       {
            $tipoDist_original = 3;
       }
       if($tipoDistribuidor == 3)
       {
            $tipoDist_original = 1;
       }
        
        
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $sql = "UPDATE DISTRIBUIDOR SET CODIGO_TR = '".$tipoDist_original."' WHERE CODIGO_DIST = '".$this->codigo_dist."'";
        $miConexion->EjecutaConsulta($sql);


    }
    
    public function Actualiza_cambio_Tipodistribuidor_Admin($codigo_dist)
    {
        
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $sql = "UPDATE DISTRIBUIDOR SET CODIGO_TR = 1 WHERE CODIGO_DIST = '".$codigo_dist."'";
        $miConexion->EjecutaConsulta($sql);

    }
    
    public function Ingresa_reserva_completa($cod_excursion, $recogida, $telefono, $pasajero, $tipo_re, $fecha_re, $fecha_realizacion,
                                            $estado_reserva, $cod_usua, $cod_cierre, $login_usua_cierre, $pasaporte, $otro_lugar_re, $cod_vendedor,
                                            $cobrada_re, $fecha_cobrada_re, $usuario_cobrada_re, $observacion, $boleta_re, $pagada_re,
                                            $cod_usua_pagada_re, $doc_pago_re, $fecha_boleta_re, $usuario_boleta_re, $tipo_bol_fact_re,
                                            $factura_re, $fecha_factura_re, $usuario_factura_re, $codigo_asoc, $voucher_asoc, $voucher_asoc_re,
                                            $fop, $sucursal, $hora_venta, $boleta_ttk, $voucher_tbk, $tipo_pax, $precio, $voucher, $hotel, $habitacion, $nacionalidad,
                                            $login_usua_reserva,$codigo_dist, $precio_comisionado)
                                            
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $sql = "INSERT INTO RESERVA_EXCURSION (CODIGO_EO, LUGAR_RE, TELEFONO_RE, PASAJERO_RE, TIPO_RE, FECHA_RE, FECHA_REALIZACION_RE, 
                ESTADO_RESERVA_ER, COD_USUA, COD_CIERRE, LOGIN_USUA_CIERRE, PASAPORTE_RE, OTRO_LUGAR_RE, CODIGO_VENDEDOR_RE, COBRADA_RE,
                FECHA_COBRADA_RE, USUARIO_COBRADA_RE, OBSERVACIONES_CONTABLES, BOLETA_RE, PAGADA_RE, COD_USUA_PAGADA_RE, DOCUMENTO_PAGO_RE,
                FECHA_BOLETA_RE, HOTEL_RE, HABITACION_RE, NACIONALIDAD_RE, LOGIN_USUA_RESERVA, USUARIO_BOLETA_RE, TIPO_BOL_FACT_RE,
                FACTURA_RE, FECHA_FACTURA_RE, USUARIO_FACTURA_RE,CODIGO_ASOC, VOUCHER_ASOC, VOUCHER_ASOC_RE,
                FOP,SUCURSAL_VENTA, HORA_VENTA, BOLETA_TTK, VOUCHER_TBK, TIPO_PASAJERO, PRECIO_RE, VOUCHER_RE, CODIGO_DIST, PRECIO_COMISIONADO_RE)
    VALUES (".$cod_excursion.", ".$recogida.", '".$telefono."','".$pasajero."','".$tipo_re."','".$fecha_re."','".$fecha_realizacion."',
            '".$estado_reserva."','".$cod_usua."','".$cod_cierre."',0,'".$pasaporte."', '".$otro_lugar_re."','".$cod_vendedor."', 1,
            '".$fecha_cobrada_re."','".$usuario_cobrada_re."','".$observacion."', 0, 0,'','".$doc_pago_re."',
            '".$fecha_boleta_re."','".$hotel."','".$habitacion."','".$nacionalidad."',0 ,'".$usuario_boleta_re."','".$tipo_bol_fact_re."',
            '".$factura_re."','".$fecha_factura_re."','".$usuario_factura_re."','".$codigo_asoc."','".$voucher_asoc."','".$voucher_asoc_re."',
            '".$fop."', '".$sucursal."', '".$hora_venta."',0, 0, '".$tipo_pax."','".$precio."', '".$voucher."', '".$codigo_dist."', '".$precio_comisionado."')";

        $miConexion->EjecutaConsulta($sql);

        
        
    }
    
    public function Ingresa_documento_prepago($mediodepago, $doc_pago, $banco, $fecha_trans, $monto, $cod_usua_prestamista ,$cod_usua_deudor, $estado)
    {
          /* 
          $ObjIns= new ClaseConexion;
          $ObjIns->conectar();
          $sql = "INSERT INTO DETALLES_PREPAGOS (COD_USUA_PRESTAMISTA, COD_USUA_DEUDOR, FECHA_PREPAGO, DOCUMENTO_PREPAGO, MEDIO_PAGO_PREPAGO, BANCO_ORIGEN_PREPAGO, MONTO_PREPAGO)
                                         VALUES ('".$cod_usua_prestamista."', ".$cod_usua_deudor.", '".$fecha_trans."', ".$doc_pago.", '".$mediodepago."', '".$banco."', ".$monto." )";
          $queryIns = $ObjIns->EjecutaConsulta($sql);
          mysql_free_result($queryIns); 
          mysql_close();
     */
     /*
           if ($mediodepago == 3)
           {
                $cargo = (5 * $monto)/100 ;
                $fecha_cargo = date('Y-m-d');
           }
           else
           {
            */
                $cargo = 0;
                $fecha_cargo = "";
      //     }
           
       
          $miConexion= new ClaseConexion;
          $miConexion->conectar();
          $sql = "INSERT INTO PREPAGOS (COD_USUA_DEUDOR, CARGO, PREPAGO, FECHA_CARGO, FECHA_PREPAGO, ESTADO, COD_USUA_PRESTAMISTA, DOCUMENTO_PREPAGO, MEDIO_PAGO_PREPAGO, BANCO_ORIGEN_PREPAGO, MONTO_PREPAGO )
                         VALUES (".$cod_usua_deudor.", ".$cargo.", ".$monto.", '".$fecha_cargo."', '".$fecha_trans."', ".$estado.", '".$cod_usua_prestamista."', ".$doc_pago.", '".$mediodepago."', '".$banco."', ".$monto." )";
          $miConexion->EjecutaConsulta($sql);

          
          
    }
    
    public function Consulta_linea_prepago($cod_usuario)
    {
        $miConexion = new ClaseConexion;
        $miConexion->Conectar();
        $queryPrepago=$miConexion->EjecutaSP("Consulta_linea_prepago_usuario"," '".$cod_usuario."' "); #consulta tabla prepagos
        $existe = mysql_num_rows($queryPrepago);
        
        if ($existe >= 1)
        {
          while ($row= mysql_fetch_assoc($queryPrepago)){
            $total = $row['linea_prepago'];
          }
        }
        else
        {
           $total = 0; 
        }

        mysql_free_result($queryPrepago); 
        
        
        return $total;
    }
    
    
    public function Actualiza_linea_prepago($linea_prepago)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT LINEA_PREPAGO FROM LINEAS_DE_PREPAGOS WHERE COD_USUA = ".$this->cod_usuario." ");
        $existe = mysql_num_rows($query);
        
        if ($existe >= 1)
        {
              $miConexion= new ClaseConexion;
              $miConexion->Conectar();
              $sql = "UPDATE LINEAS_DE_PREPAGOS SET  LINEA_PREPAGO = ".$linea_prepago." WHERE COD_USUA = ".$this->cod_usuario." ";
              $miConexion->EjecutaConsulta($sql);

        }
        else
        {
              $miConexion= new ClaseConexion;
              $miConexion->conectar();
              $sql = "INSERT INTO LINEAS_DE_PREPAGOS (COD_USUA, LINEA_PREPAGO) VALUES (".$this->cod_usuario.", ".$linea_prepago.")";
              $miConexion->EjecutaConsulta($sql);

        }
        mysql_free_result($query); 
        
    }
    
    public function Consulta_Saldo_linea_prepago()
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT LINEA_PREPAGO FROM LINEAS_DE_PREPAGOS WHERE COD_USUA = ".$this->cod_usuario." ");
        $existe = mysql_num_rows($query);
        if ($existe = 1)
        {
            while($row = mysql_fetch_array($query))
            {
                $total = $row['LINEA_PREPAGO'];
            }
        }
        else
        {
            $total = 0;        
        }
        mysql_free_result($query); 
        mysql_close();
        
        return $total;
    }
    
    public function Consulta_Saldo_linea_prepago_Admin($cod_usua)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT LINEA_PREPAGO FROM LINEAS_DE_PREPAGOS WHERE COD_USUA = ".$cod_usua." ");
        $existe = mysql_num_rows($query);
        if ($existe = 1)
        {
            while($row = mysql_fetch_array($query))
            {
                $total = $row['LINEA_PREPAGO'];
            }
        }
        else
        {
            $total = 0;        
        }
        mysql_free_result($query); 
        
        return $total;
    }
    
    
    public function Ingresa_cargo_prepago($cargo, $fecha_cargo, $boarding_pass, $estado)
    {
          $miConexion= new ClaseConexion;
          $miConexion->conectar();
          $sql = "INSERT INTO PREPAGOS (COD_USUA_DEUDOR, CARGO, FECHA_PREPAGO, BOARDING_PASS, ESTADO) VALUES (".$this->cod_usuario.", ".$cargo.", '".$fecha_cargo."', '".$boarding_pass."', ".$estado.")";
          $miConexion->EjecutaConsulta($sql);

    }
    
    public function Ingresa_observacion_de_pago($observacion_de_pago, $boarding_pass, $precio_abonado_re)
    {
          $miConexion= new ClaseConexion;
          $miConexion->Conectar();
          $sql = "UPDATE RESERVA_EXCURSION SET  OBSERVACION_DE_PAGO = '".$observacion_de_pago."', PRECIO_ABONADO_RE = '".$precio_abonado_re."' WHERE VOUCHER_RE = '".$boarding_pass."' ";
          $miConexion->EjecutaConsulta($sql);

    }
    
    public function Consulta_factura($factura)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT FACTURA_RE FROM RESERVA_EXCURSION WHERE FACTURA_RE = ".$factura." ");
        $existe = mysql_num_rows($query);
        
        mysql_free_result($query); 
        
        return $existe;
    }
    
    public function Consulta_boletas_registradas($boleta_re)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT BOLETA_RE FROM BOLETAS_ERP WHERE BOLETA_RE = ".$boleta_re." ");
        $existe = mysql_num_rows($query);
        
        mysql_free_result($query); 

        
        return $existe;
    }
    
    public function Consulta_boletas_registradas_grl($boleta_re)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT BOLETA_RE FROM BOLETAS_ERP_GRL WHERE BOLETA_RE = ".$boleta_re." ");
        $existe = mysql_num_rows($query);
        
        mysql_free_result($query); 
        
        return $existe;
    }
    
    public function Consulta_boletas_registradasTTK($boleta_ttk)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT BOLETA_TTK FROM BOLETAS_ERP WHERE BOLETA_TTK = ".$boleta_ttk." ");
        $existe = mysql_num_rows($query);
        
        mysql_free_result($query); 
        
        return $existe;
    }
    
     public function Consulta_factura_ab($factura)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT FACTURA_AB FROM RESERVA_EXCURSION WHERE FACTURA_AB = ".$factura." ");
        $existe = mysql_num_rows($query);
        
        mysql_free_result($query); 
        
        return $existe;
    }
    
    
    public function Consulta_tipo_cliente_tabla_temporal($cod_usuario, $cod_sucursal)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT ID_TEMPORAL, NACIONALIDAD, TIPO_CLIENTE FROM RESERVA_EXCURSION_TEMPORAL 
                                             WHERE CODIGO_SUCURSAL = ".$cod_sucursal." and
                                             COD_USUA = ".$cod_usuario." ");
        
          while($row = mysql_fetch_array($query))
          {
                $tipo_cliente = $row['TIPO_CLIENTE'];
                $nacionalidad = $row['NACIONALIDAD'];
                
          }
          
        //  $cant = mysql_num_rows($query);
          
        mysql_free_result($query); 

        $result = $tipo_cliente."*".$nacionalidad;
        return $result;
    }
    
    
    public function Ingresa_brazaletes_disponibles($folio, $numero, $cod_usua_promotor, $estado)
    {
          $miConexion= new ClaseConexion;
          $miConexion->conectar();
          $sql = "INSERT INTO BRAZALETES (FOLIO, NUMERO, COD_USUA_PROMOTOR, ESTADO) VALUES (".$folio.", ".$numero.", ".$cod_usua_promotor.", '".$estado."')";
          $miConexion->EjecutaConsulta($sql);

    }
    
    public function Consulta_precio_comisonado_excursion($codigo_eo, $codigo_dist)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT EO.PRECIO_EO, REL.VALOR_REO FROM EXCURSION_ORIGEN EO
                                            INNER JOIN REL_CATEGORIA_EXCURSION REL ON REL.CODIGO_EO = EO.CODIGO_EO
                                            INNER JOIN DISTRIBUIDOR D ON D.CODIGO_CATE = REL.CODIGO_CATE
                                             WHERE
                                             EO.CODIGO_EO = ".$codigo_eo." AND
                                             D.CODIGO_DIST = ".$codigo_dist." ");
        
          while($row = mysql_fetch_array($query))
          {
                $valor_ex = $row['PRECIO_EO'];
                $comision_ex = $row['VALOR_REO'];
                
          }
        mysql_free_result($query); 

        
          $precio = $valor_ex - $comision_ex;
          return $precio;
   
    }
    
    public function Actualiza_cierreReserva_WS($codigo_re, $accion)   
    {
        
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $miConexion->EjecutaConsulta(" UPDATE RESERVA_EXCURSION SET COD_CIERRE = ".$accion." WHERE CODIGO_RE = ".$codigo_re." ");


    }
    
    
    public function Consulta_excursiones_existentes()
    {
        
        $miConexion= new ClaseConexion;
        $miConexion->conectar();
        $Query = $miConexion->EjecutaConsulta("SELECT COD_EXCURSION FROM RESERVA_EXCURSION_TEMPORAL WHERE CODIGO_SUCURSAL = ".$_SESSION['codigo_suc']." and  COD_USUA = ".$_SESSION["codigo_usu"]." and ESTADO = 0 ");
        $cant_ex = mysql_num_rows($Query);

        return $cant_ex;
    }
    
    
    
    public function Consulta_tipos_excursiones_boleta()
    {
        $data = false;

        
        $ObjSelectGroup= new ClaseConexion;
        $ObjSelectGroup->conectar();
        $Query = $ObjSelectGroup->EjecutaConsulta("SELECT COD_EXCURSION FROM RESERVA_EXCURSION_TEMPORAL WHERE CODIGO_SUCURSAL = ".$_SESSION['codigo_suc']." and  COD_USUA = ".$_SESSION["codigo_usu"]." and ESTADO = 1 group by COD_EXCURSION");
        $cant_ex = mysql_num_rows($Query);
        while($row = mysql_fetch_array($Query))
        {
            if (in_array($row['COD_EXCURSION'], $this->excursiones_stk) ) 
            {
                $data = true;
            }
            
        }


        if ($cant_ex > 1 && $data == true)
        {
            return 1; // se piden 2 boletas obligatorias
        }
        if ($cant_ex == 1 && $data == false)
        {
            return 2; // se piden 2 boletas obligatorias
        }
        if ($cant_ex == 1 && $data == true)
        {
            return 3; // se pide solo una boleta
        }
        if ($cant_ex > 1 && $data == false)
        {
            return 1; // se piden 2 boletas obligatorias
        }
        if ($cant_ex == 0) {
            return 0;
        }
        
        
    }
    
    public function Consulta_total_disponibilidad_ex($fecha_format, $tipo_excursion)
    {
        $miConexion= new ClaseConexion;
       	$miConexion->conectar();

		$query4=$miConexion->EjecutaSP("Consulta_Fecha_Excursiones","'".$fecha_format."','".$tipo_excursion."','".$this->usuario."',@salida");
		$parametrosSalida=$miConexion->ObtenerResultadoSP("@salida");
		$rowverificacion= mysql_fetch_assoc($parametrosSalida);
		$total = $rowverificacion['@salida'];
        mysql_free_result($query4); 

        
        return $total;
    }
    
    public function Consulta_total_disponibilidad_ex_tomadas($fecha_format, $tipo_excursion)   
    {
        $miConexion= new ClaseConexion;//
       	$miConexion->conectar();//Consulta_Tomadas_Excursiones
        $query=$miConexion->EjecutaSP("Consulta_Tomadas_ExcursionesNoAnulada","'".$fecha_format."','".$tipo_excursion."',@salida");
		$parametrosSalida=$miConexion->ObtenerResultadoSP("@salida");
		$rowverificacion= mysql_fetch_assoc($parametrosSalida);
        $total = $rowverificacion['@salida'];
        mysql_free_result($query); 

        
        return $total;
    }
    
    public function Consulta_reserva_by_codigo($codigo_re)   
    {
        
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta(" SELECT COD_USUA, PRECIO_RE, VOUCHER_RE FROM RESERVA_EXCURSION WHERE CODIGO_RE = ".$codigo_re." ");
        while ($row = mysql_fetch_array($query))
        {
            $cod_usua = $row['COD_USUA'];
            $voucher = $row['VOUCHER_RE'];
        }
        mysql_free_result($query); 

    }
    
    public function Elimina_Imagen_Juniper($codigo_eo, $file, $accion)
    {
        
        $miConexion= new ClaseConexion;
        if ($accion == 1)
        {
            $miConexion->Conectar();
            $query=$miConexion->EjecutaConsulta(" SELECT IMAGENES FROM EXCURSION_ORIGEN WHERE CODIGO_EO = ".$codigo_eo." ");
            while ($row = mysql_fetch_array($query))
            {
                $imagenes = $row['IMAGENES'];
            }

            
            $newString = str_replace($file.",","",$imagenes);
    
            $miConexion->Conectar();
            $query1=$miConexion->EjecutaConsulta(" UPDATE EXCURSION_ORIGEN SET IMAGENES = '".$newString."' WHERE CODIGO_EO = ".$codigo_eo." ");

            mysql_close();        
        }
        else
        {
            $miConexion->Conectar();
            $query1=$miConexion->EjecutaConsulta(" UPDATE EXCURSION_ORIGEN SET IMAGEN_LISTADO = '' WHERE CODIGO_EO = ".$codigo_eo." ");

            mysql_close(); 
        }
        

        
    }
    
    
    public function Consulta_lugar_recogida($id)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT DESCRIPCION_HL FROM HOTEL_LUGAR WHERE CODIGO_HL = ".$id." ");
        while ($row = mysql_fetch_array($query))
        {
            $nombre_lugar = $row['DESCRIPCION_HL'];
        }
        
        mysql_free_result($query); 
        
        return $nombre_lugar;
    }
    

    
    public function Consulta_Usuario($cod_usua, $precio, $fecha, $voucher, $cantidad)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT NOMBRE_USUA, APELLIDO_USUA, RUT_USUA, LOGIN_USUA, CODIGO_VENDEDOR FROM USUARIO WHERE COD_USUA = $cod_usua ");
        while ($row = mysql_fetch_array($query))
        {
            $nombre = $row['NOMBRE_USUA'];
            $apellido = $row['APELLIDO_USUA'];
            $rut = $row['RUT_USUA'];
            $login = $row['LOGIN_USUA'];
            $codigo_vendedor = $row['CODIGO_VENDEDOR'];
        }
        $miConexion->Conectar();
        $miConexion->EjecutaSP("Ingresar_Reserva_Excursion_Hotelero"," 6,'".$fecha."', '1', '', '1', '1','228201000', '".$nombre."', '".$apellido."', '".$rut."','2','".$login."',".$cantidad.",'".$precio."','VENTA STK','','','".$codigo_vendedor."' ,'".date('H:m:s')."', 'ADT', 0, 100, 9, 'E',9, '".$voucher."', @respuesta,@pmessage"); 	
    
        mysql_free_result($query); 

    }
    
    public function Ingresa_Tabla_Reembolso($usuario, $fecha, $sucursal, $porcentaje, $total, $tipo, $boleta_tts, $boleta_ttk, $observacion, $digitos_tarjeta, $email)
    {
          $miConexion= new ClaseConexion;
          $miConexion->conectar();  #ESTADO 0 PENDIENTE A CONSUMIR EN SERVICIO WEB
          $sql = "INSERT INTO REEMBOLSO (USUARIO_REEMBOLSO, FECHA_REEMBOLSO, SUCURSAL_REEMBOLSO, PORCENTAJE_REEMBOLSO, TOTAL_REEMBOLSO, TIPO_REEMBOLSO, BOLETA_TTS, BOLETA_TTK, OBSERVACION, ULTIMOS_DIGITOS_TARJETA, EMAIL_CLIENTE) VALUES ('".$usuario."', '".$fecha."', '".$sucursal."', '".$porcentaje."', '".$total."', '".$tipo."', '".$boleta_tts."', '".$boleta_ttk."', '".$observacion."', '".$digitos_tarjeta."', '".$email."')";
          $miConexion->EjecutaConsulta($sql);

    }
    
    
    public function Consulta_id_Reembolso($boleta_tts, $boleta_ttk)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        
        if ($boleta_tts != '')
        {
            $query=$miConexion->EjecutaConsulta("SELECT ID_REEMBOLSO FROM REEMBOLSO WHERE BOLETA_TTS = '".$boleta_tts."'");

        }
        
        if ($boleta_ttk != '')
        {
            $query=$miConexion->EjecutaConsulta("SELECT ID_REEMBOLSO FROM REEMBOLSO WHERE BOLETA_TTK = '".$boleta_ttk."'");
        }
        
            while ($row = mysql_fetch_array($query))
            {
                $id = $row['ID_REEMBOLSO'];
            }
            
           mysql_free_result($query); 
           
           return $id;

    }
    
    public function Actualiza_id_reembolso($codigo_re, $id)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $miConexion->EjecutaConsulta(" UPDATE RESERVA_EXCURSION SET ID_REEMBOLSO = '".$id."' WHERE CODIGO_RE = ".$codigo_re." ");

    }
    
    public function Actualiza_Tabla_Reembolso($id, $ncr_tts, $ncr_ttk, $fecha, $usuario)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $sql = " UPDATE REEMBOLSO SET NCR_TTS = '".$ncr_tts."', NCR_TTK = '".$ncr_ttk."', FECHA_NCR= '".$fecha."', USUARIO_NCR = '".$usuario."', ESTADO = 1 WHERE ID_REEMBOLSO = ".$id." ";
        $miConexion->EjecutaConsulta($sql);
       
    }
    
    public function Consulta_Coordinador_Sucursal($cod_usua)
    {
        /*
         $miConexion= new ClaseConexion;
         $miConexion->Conectar();
         $query=$miConexion->EjecutaConsulta("SELECT FROM COORDINADORES_SUCURSALES WHERE ");
         
            while ($row = mysql_fetch_array($query))
            {
                $id = $row['ID_REEMBOLSO'];
            }
            
           mysql_free_result($query); 
           mysql_close(); 
           */
    }
    
    public function Consulta_Codigo_dist_Sucursal($codigo_suc)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT CODIGO_DIST FROM DISTRIBUIDOR WHERE CODIGO_ADICIONAL_DIST = ".$codigo_suc." ");
        while ($row = mysql_fetch_array($query))
        {
            $codigo = $row['CODIGO_DIST'];
        }
        
        mysql_free_result($query); 

        
        return $codigo;
    }
    

    
    public function consulta_stock_boletas_tts_grl($boleta_re)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT DB.FOLIO 
                                             FROM DETALLES_BOLETAS_GRL DB
                                             INNER JOIN STOCK_BOLETAS_GRL SB ON SB.ID_BOLETA=DB.ID_BOLETA
                                             WHERE
                                             DB.FOLIO = ".$boleta_re." AND
                                             DB.SUCURSAL_USO = '' AND
                                             SB.TIPO = 'E' AND
                                             SB.SUCURSAL = ".$_SESSION['codigo_suc']." AND
                                             SB.ESTADO=1");
        $existe = mysql_num_rows($query);
        
        mysql_free_result($query); 

        
        return $existe;
    }
    /*
    public function consulta_stock_boletas_ttk($boleta_ttk)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT DB.FOLIO 
                                             FROM DETALLES_BOLETAS DB
                                             INNER JOIN STOCK_BOLETAS SB ON SB.ID_BOLETA=DB.ID_BOLETA
                                             WHERE
                                             DB.FOLIO = ".$boleta_ttk." AND
                                             DB.SUCURSAL_USO = '' AND
                                             SB.TIPO = 'A' AND
                                             SB.SUCURSAL = ".$_SESSION['codigo_suc']." AND
                                             SB.ESTADO=1");
        $existe = mysql_num_rows($query);
        
        mysql_free_result($query); 
        mysql_close();
        
        return $existe;
    }
    
    public function consulta_stock_boletas_tts($boleta_re)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta("SELECT DB.FOLIO 
                                             FROM DETALLES_BOLETAS DB
                                             INNER JOIN STOCK_BOLETAS SB ON SB.ID_BOLETA=DB.ID_BOLETA
                                             WHERE
                                             DB.FOLIO = ".$boleta_re." AND
                                             DB.SUCURSAL_USO = '' AND
                                             SB.TIPO = 'E' AND
                                             SB.SUCURSAL = ".$_SESSION['codigo_suc']." AND
                                             SB.ESTADO=1");
        $existe = mysql_num_rows($query);
        
        mysql_free_result($query); 
        mysql_close();
        
        return $existe;
    }
    */
    public function consulta_stock_boletas($boleta, $tipo, $sucursal)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaSP("Consulta_stock_boletas", " ".$boleta.", '".$tipo."', ".$sucursal." ");
        
        $existe = mysql_num_rows($query);
        
        return $existe;
    }
    
    public function Consulta_stock_brazaletes($sucursal, $cantidad_pax)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $sql = "SELECT SERIAL
                 FROM DETALLE_STOCK 
                 WHERE
                 COD_USUA_ASIGNA != '' AND
                 TIPO = 2 AND
                 CODIGO_SUCURSAL = ".$sucursal." AND
                 ESTADO=0 AND
                 IMPRESO =0 
                 limit ".$cantidad_pax."";
            
        $query=$miConexion->EjecutaConsulta($sql);
        while ($row = mysql_fetch_array($query))
        {
            $codigo[] = $row['SERIAL'];
        }
        
        return $codigo;
    }
    
    public function Consulta_cantidad_pasajeros_ex_6($voucher)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $sql = "SELECT CODIGO_RE
                 FROM RESERVA_EXCURSION 
                 WHERE
                 COD_CIERRE = 2 AND
                 TIPO_RE = 1 AND
                 (TARJETA_TTS IS NULL OR TARJETA_TTS='') AND
                 CODIGO_EO in (6,54,55,63,64) AND
                 FECHA_RE >= '".date('Y-m-d')."' AND
                 VOUCHER_RE = '".$voucher."'";
        $query=$miConexion->EjecutaConsulta($sql);
        while ($row = mysql_fetch_array($query))
        {
            $codigo[] = $row['CODIGO_RE'];
        }
        
        return $codigo;
    }
    
    
    public function Consulta_cantidad_pasajeros_ex_6_retorno($voucher)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $sql = "SELECT CODIGO_RE
                 FROM RESERVA_EXCURSION 
                 WHERE
                 COD_CIERRE = 2 AND
                 FECHA_RE >= '".date('Y-m-d')."' AND
                 VOUCHER_RE = '".$voucher."'";
        $query=$miConexion->EjecutaConsulta($sql);
        while ($row = mysql_fetch_array($query))
        {
            $codigo[] = $row['CODIGO_RE'];
        }
        
        return $codigo;
    }
    
    public function Actualiza_brazalete_reserva_excursion($codigo_re, $serial)
    {
        $miConexion= new ClaseConexion;
        
        $miConexion->Conectar();
        $sql = "UPDATE RESERVA_EXCURSION SET TARJETA_TTS = ".$serial." WHERE CODIGO_RE=".$codigo_re." ";
        $miConexion->EjecutaConsulta($sql);
        
        $miConexion->Conectar();
        $sql2 = "UPDATE DETALLE_STOCK SET ESTADO = 1 WHERE SERIAL=".$serial." ";
        $miConexion->EjecutaConsulta($sql2);  
        
    }
}
?>