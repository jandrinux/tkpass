  <?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
session_start();
date_default_timezone_set("America/Santiago");
unset($_SESSION['accion']);
if ($_SESSION["autentificado"] != "SI"){
    header("location: Login.php");
}

require ("../Clases/ClaseConexion.inc.php");
require ("../Clases/ClaseUtil.inc.php");
require ("../functions/tkpass.php");
//require ("../Webservice/ws.php");
require ("../Clases/Tickets.php");


$_Util=new Util;

$codigo_suc = $_SESSION['codigo_suc'];
$usuario = base64_decode($_GET['u']);
$tipo_excursion = base64_decode($_GET['ex']);
$_SESSION['cod_excursion'] = $tipo_excursion;
$fecha_format = base64_decode($_GET['df']);
//$form_validado = '';

$Obj_Tkpass = new tkpass(); 
include_once('validaciones.php');

if(isset($_POST['comprar']))
{

   include_once('valida_form_reserva.php');
   include_once ('add_carrito.php');
 
   if($tipo_usuario != 4)
   {
        if($pasa_compra == true) // compra de usuarios con carrito
        {
            header('location: finaliza_compra_funicular.php');   
        }
   }
 

}
else
{

    if ($form_validado != 1 && $form_validado != 2 && $form_validado != 3 && $form_validado != 4)
    {
        include_once ('elimina_temporales.php');        
    }

} 


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US">

<html>
<head>
<title>Reserva Excursiones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>

<link rel="stylesheet" media="screen" href="../../css/demo.css" />
<link href="../../css/reconfirme.css" rel="stylesheet" type="text/css" />
<link href="../../css/autocomplete.css" rel="stylesheet" type="text/css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../../js/jquery.leanModal.min.js"></script>
<link href="../estilos1.css" rel="stylesheet" type="text/css"/>
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type='text/javascript' src="../../js/autocomplete.js"></script>

<? include('js.php');?>
</head>
   
<body onload="setFocus();">

<div id="Layer1" style="position:absolute; left:4px; top:120px; width:997px; height:311px; z-index:1">


<form name="form1" id="form1" method="post">
 <table width="990" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="95" class="celdalazul_peque">Nombre Excursion </td>
        <td width="208" class="celdalCELEST"><? echo $nombre ?></td>
        <td width="105" class="celdalazul_peque">Fecha de Excursi&oacute;n </td>
        <td class="celdalCELEST"><? echo base64_decode($_GET['f']) ?></td>
	    <td width="100" class="celdalazul_peque">Valor por persona</td>
	    <td class="celdalCELEST" >
	   <input name="monto_paso" type="hidden" id="monto_paso" value="<? echo $monto ?>"/>
       <input name="monto" type="hidden" id="monto2" value="">
        <label for="lbl_precio" id="lbl_precio"><? echo "$ ". number_format($monto)." ADT"; if ($TipoDistribuidor == 3 || $_SESSION['accion'] == "upt_chg") { echo "  /    $ ". number_format($abono)." ABONO";}  ?></label><br />
        <label for="lbl_precio" id="lbl_precio"><? echo "$ ". number_format($monto_child)." CHD"; if ($TipoDistribuidor == 3 || $_SESSION['accion'] == "upt_chg") { echo "  /    $ ". number_format($abono_chd)." ABONO";} ?></label>
        </td>
        <td width="96" class="celdalazul_peque"></td>
        <td width="96" class="celdalCELEST"></td>
    
      </tr>
      <tr>
			<td colspan="8" style="font-family: Arial; font-size: 12px; "><a href="#resumen" name="reconfirme" rel="resumen" id="go">Resumen Excursi&oacute;n</a></td>
      </tr>

</table>
</form>


<div id="resumen" style="display: none; text-align: justify;">
<div id="resumen-header">
<? echo nl2br($resumen)?>
</div>
</div>

<div id="msg_dispon"  style="margin:10px 0px 10px 0px; padding-bottom: 40px;"><? include('CONSULTA_disponibilidad.php'); ?></div>

<form name="agregaExcursion" onsubmit="agregar_reserva(); return false" method="post">
<fieldset id="fieldset">
<legend class="celdalLeyend">Agregar Pasajero</legend>
 <table id="tabla" width="970" border="0" cellpadding="0" cellspacing="0">
 <tr>
    <td class="celdalazul_peque">Nombre Pasajero: *</td>
    <td class="celdalCELEST"><input name="pasajero" type="text" required="" class="seleccionTurist_180" id="pasajero" title="Nombre pax es obligatorio" onkeypress="return validarP1(event);" maxlength="25"  x-moz-errormessage="Nombre pax es obligatorio"/></td>
    <td class="celdalazul_peque">Apellido Pasajero: *</td>
    <td class="celdalCELEST"><input name="apasajero" class="seleccionTurist_180" required=""  x-moz-errormessage="Apellido pax es obligatorio"   size="18" maxlength="20" id="apasajero" type="text" /></td>

 </tr>
 <tr>

    <td class="celdalazul_peque">Tipo pasajero: </td>
    <td class="celdalCELEST">

        <select name="tipoPasajero" id="tipoPasajero" class="seleccionTurist_180" >
            <option selected value="ADT">ADULTO</option>
            <option value="CHD">NI&ntilde;O</option>
        </select>

    </td>


    <td class="celdalazul_peque">Nacionalidad: *</td>
    <td class="celdalCELEST">
        <select name="nacionalidad" class="seleccionTurist_180" id="nacionalidad" required=""  x-moz-errormessage="Nombre pax es obligatorio" title="Seleccione Nacionalidad" />
            
            <?
            if ($nacion == '')
            {
                echo "<option selected value='0' >Seleccione</option>";
            }
            $objListaNacionalidad = new ClaseConexion;
            $objListaNacionalidad->Conectar();
            $queryTipoNaC=$objListaNacionalidad->EjecutaSP("Consulta_Nacionalidad","'0'");
            while ($rowTipoNac = mysql_fetch_assoc($queryTipoNaC)){

                if ($nacion == $rowTipoNac['CODIGO_NAC'])
                {
                    echo "<option value=".$rowTipoNac['CODIGO_NAC']." >".$rowTipoNac['PAIS_NAC']."</option>" ;   
                }
                if ($nacion == '')
                {
                    echo "<option value=".$rowTipoNac['CODIGO_NAC']." >".$rowTipoNac['PAIS_NAC']."</option>" ;   
                }

           }
        
            mysql_free_result($queryTipoNaC); 
            mysql_close();
            ?>
        </select>
    </td>

 </tr>
 <tr>

 <? if ($_SESSION['cod_tusua'] != 4) {?>
    <td class="celdalazul_peque">Observaci&oacute;n por pax: </td>
    <td class="celdalCELEST"><textarea name="observacion" id="observacion" rows="1" class="seleccionTurist_180" ><? echo $_POST['observacion'] ?></textarea></td>
 <? }else{ ?>
    <td class="celdalazul_peque"></td>
    <td class="celdalCELEST"><input type="hidden" name="observacion" id="observacion" class="seleccionTurist_180" ></td>
 <? } ?>


    <td class="celdalazul_peque">Rut o Pasaporte: </td>
    <td class="celdalCELEST"><input name="pasaporte" class="seleccionTurist_180"  size="18" maxlength="17" id="pasaporte" type="text" /></td>


    <td></td>
 </tr>
  <tr>
 
    <td class="celdalazul_peque"></td>
    <td class="celdalCELEST" ></td>
    
    <td class="celdalazul_peque">Edad: </td>
    <td class="celdalCELEST">
        <select id="edad" name="edad" class="seleccionTurist_180">
            <option value="0" selected="">-- SELECCIONE --</option>
            <option value="13 - 17">13 - 17</option>
            <option value="18 - 25">18 - 25</option>
            <option value="26 - 35">26 - 35</option>
            <option value="36 - 45">36 - 45</option>
            <option value="46 - 55">46 - 55</option>
            <option value="56 y mas">56 y mas</option>
        </select>
    </td>

 </tr>
 
</table>

<input name="email" id="email" type="hidden" value="" />
<input name="email2" id="email2" type="hidden" value="" />
<input type="hidden" name="descuento" readonly="" class="seleccionTurist_180" id="descuento" value="<? echo $monto ?>" />  
<input name="tipoReserva" id="tipoReserva" type="hidden" value="<? echo $TipoDistribuidor ?>" />
<input name="abono" id="abono" type="hidden" value="<? echo $abono ?>" />
<input name="cod_usuario" id="cod_usuario" type="hidden" value="<? echo $_SESSION['codigo_usu'] ?>" />
<input name="tipo_excursion" id="tipo_excursion" type="hidden" value="<? echo $tipo_excursion ?>" />
<input name="codigo_suc" id="codigo_suc" type="hidden" value="<? echo $codigo_suc ?>" />
<input name="fecha_format" id="fecha_format" type="hidden" value="<? echo $fecha_format ?>" />
      

<table align="right">
    <tr>
        <td><input name="Submit" type="submit" class="BotonTurist_Celest" value="Agregar" /></td>
    </tr>
</table>   

</fieldset>


<?
echo "<div class='msg_dispon'>$msg_reserva</div>";
?>


<div id="resultado_listar"><? include('CONSULTA_reserva.php'); ?></div>
</form>


<form name="CompraExcursion" id="CompraExcursion" style="margin:10px 0px 10px 0px; float:left;" action="Tomar_Reserva_Funicular.php?u=<? echo base64_encode($usuario) ?>&df=<? echo base64_encode($fecha_format)?>&f=<? echo base64_encode($fecha) ?>&ex=<? echo base64_encode($tipo_excursion)?>" method="post">
<fieldset id="fieldset">
<legend class="celdalLeyend"><? if ($tipo_excursion == 6) echo "Hotel"; else echo "Agregar Recogida"; ?> </legend>

 <table id="tabla" width="970" border="0" cellpadding="0" cellspacing="0">
 <tr>
    <? if ($tipo_excursion != 6){?>    
    <td class="celdalazul_peque">Lugar Recogida: (*)</td>
    <td class="celdalCELEST">
        <select name="lRecogida"  id="lRecogida"  title="Selecciona un lugar de recogida"  >
            <option selected value="0"></option>

                <?php
 
                    $objListaRecogida = new ClaseConexion;
                    $objListaRecogida->Conectar();
                    $queryTipoLugar=$objListaRecogida->EjecutaSP("Consulta_lugar_recogida"," 1,  $tipo_excursion");//LUGAR
                         while ($rowTipoLugar = mysql_fetch_assoc($queryTipoLugar)){
                            if ($recogida_hl == $rowTipoLugar['CODIGO_HL'] || $lugar_reserva == $rowTipoLugar['CODIGO_HL'] || $_POST['lRecogida'] == $rowTipoLugar['CODIGO_HL']){
                                echo "<option value=".$rowTipoLugar['CODIGO_HL']." selected>".$rowTipoLugar['DESCRIPCION_HL']."</option>" ;
                            }else{
                                echo "<option value=".$rowTipoLugar['CODIGO_HL'].">".$rowTipoLugar['DESCRIPCION_HL']."</option>" ;
                            }
                        } 
                        
                        
                    mysql_free_result($queryTipoHotel); 
                    mysql_close(); 
              
                ?>

        </select> 
    </td>

    <td class="celdalazul_peque">Tel&eacute;fono: <? if ($tipo_excursion != 6){ echo "(*)"; } ?> </td>
    <td class="celdalCELEST">
    <div id="telefono"><input name="telefono" title="N&uacute;mero de tel&eacute;fono" class="seleccionTurist_180" value="<? echo $telefono ?>"  maxlength="12" id="telefono" type="text" onkeypress="return validarP3(event);"/></div>
    </td>
    <? } ?>
 </tr>
 <tr>
     <td  class="celdalazul_peque" width="150">Hotel: (*)</td>
    <td class="celdalCELEST" width="300" >
            <select name="hotel" title="Selecciona un hotel" id="select1"  class="seleccionTurist_200" >
                <option selected value="">Seleccione Hotel</option>
                  <?php
                  $objListaHotel = new ClaseConexion;
                  $objListaHotel->Conectar();
                    $queryTipoHotel=$objListaHotel->EjecutaSP("Consulta_Hotel_Lugar"," 4 ");//hotel
                        while($rowTipoHotel = mysql_fetch_array($queryTipoHotel)){
                            if ($hotel == $rowTipoHotel['CODIGO_HL'] || $recogida_hl == $rowTipoHotel['CODIGO_HL']){
                                echo "<option value=".$rowTipoHotel['CODIGO_HL']." selected>".$rowTipoHotel['DESCRIPCION_HL']."</option>" ;
                            }else{
                                echo "<option value=".$rowTipoHotel['CODIGO_HL'].">".$rowTipoHotel['DESCRIPCION_HL']."</option>" ;
                            }
                        } 
                        
                        
                    mysql_free_result($queryTipoHotel); 
                    mysql_close(); 
                  ?>
            </select>
    </td>
    <? if ($tipo_excursion != 6){?>   
    <td class="celdalazul_peque">Habitaci&oacute;n: (*)</td>
    <td class="celdalCELEST"><input name="habitacion" id="habitacion"  title="N&uacute;mero de habitaci&oacute;n" class="seleccionTurist_180"  maxlength="8" type="text" onkeypress="return validarP3(event);" value="<? echo $habitacion ?>" /></td>
    <? }else{ ?>
    <td ></td>
    <td class="celdalCELEST"></td>
    <? } ?>
 </tr>

</table>
</fieldset>

     <input name="codigo_suc" type="hidden" value="<? echo $codigo_suc ?>"  />
     <input name="tipo_excursion" type="hidden" value="<? echo $tipo_excursion ?>" />
     <input name="usuario" type="hidden" value="<? echo $usuario ?>" />
     <input name="fecha_format" type="hidden" value="<? echo $fecha_format ?>" />
     <input name="total_pasajeros" id="total_pasajeros" type="hidden" value="" />
     <input type="hidden" name="radio" id="radio"  /> <!-- valor pasado desde ventana hija, diferencia el tipo de venta descuento/o ninguno , externos-->
     
<table align="right">
    <tr>

        <td><input align="right" name="reservar" id="reservar" type="button" class="BotonTurist_Celest" value="Comprar" onclick="Reservar(this)"/></td>
        <input align="right" name="comprar" type="submit" style="display:none;" class="BotonTurist_Celest" value="Comprar" />
        
    </tr>
</table>  
 
</form>

</div>


<? if($tipo_usuario != 4){?>
<div id="ReloadThis" style="position:absolute; font-family: Arial;  margin: 15px 0px 0px 840px"><? include ('estado_carrito.php') ?></div>
<? } ?>




<div id="Layer2" style="position:absolute; left:5px; top:0px; width:439px; height:89px; z-index:2">

<table>
    <tr>
      <td width="13"><img src="../Imagenes/IMG_6902.jpg"  width="80" height="80"  /></td>
      <td width="400">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="Titulo">Toma de Reservas de Excursiones</span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
</table>
  

</div>
<script>
 $( "#lRecogida" ).combobox();
  $( "#toggle" ).click(function() {
      $( "#lRecogida" ).toggle(); 
     
    });
	
	
	 $( "#select1" ).combobox();
        $( "#toggle" ).click(function() {
      $( "#select1" ).toggle(); 
    });
</script>

</body>

</html>
