var MENU_ITEMS = [
	['Paquetes',null,null,
	['Servicios', null,null,
			['Ingreso Servicios','Ingreso_Evento.php?bandera=0'],
			['Modificar Servicios','Modificar_Evento.php']
	],
	['Productos',null,null,
			['Ingreso de Productos','Ingreso_Producto.php'],
			['Modificar Productos','Modificar_Producto.php'],
			],
	['Descuento',null,null,
			['Ingreso de Descuento','Ingreso_Descuento.php'],
			['Modificar Descuento','Modificar_Descuento_Detalle.php'],		
			],
	['Comisiones',null,null,
			['Ingreso de Comisiones','Ingreso_Comision.php'],
			['Modificar Comisiones','Modificar_Comision.php'],	
	]],
	
	['POS', null, null,
		['Perfil', null,null,
				 ['Creacion Perfil de POS','Asignacion_Evento_Perfil.php?bandera=0'],
				 ['Modificar Perfil de POS','Modificar_Evento_Perfil.php']
		],
		['Serie',null,null,
				['Asignar Perfil','Asignacion_Evento_Serie.php'],
				['Modificar Asignacion Perfil','-'],
		]],
	
	
	
	
    ['Tarjeta', null, null,
			['Generar Tarjeta','Ingreso_Tarjeta.php'],
			['Eliminar Tarjeta','Modificar_Tarjeta.php'],
			['Recepcionar','Recepcion_Tarjeta.php'],
			['Activar Tarjeta','Activacion_Tarjeta.php'],
			['Activar Tarjeta Turistik','Activacion_Tarjeta_Turistik.php'],
			['Consultar Tarjeta','Consulta_Tarjeta.php'],
			['Validar Tarjeta','Validar_Tarjeta.php']
			],

	 ['Reservar',null,null,
		['Incluir Reserva','Incluir_Reserva.php'],
		['Eliminar Reserva','Eliminar_Reserva.php']],
	
	
	['Mantenedor',null, null,
	
	 ['Distribuidor',null,null,
		['Ingreso','Ingreso_Cliente.php'],
		['Modificación','Modificar_Cliente.php']],
	 ['Promotor',null,null,
		['Ingreso','Ingreso_Cliente.php'],
		['Modificación','Modificar_Cliente.php']],
	 ['Usuario', null,null,
	    ['Ingreso Usuario','Ingreso_Usuario.php'],
		['Modificación Usuario','Modificar_Usuario.php']],
	 ['Color de Tarjeta', null,null,
	    ['Ingreso Color','Ingreso_Color.php'],
		['Modificación Color','Modificar_Usuario.php']],
	 ['Proveedor', null,null,
	    ['Ingreso Proveedor','Ingreso_Proveedor.php'],
		['Modificación Proveedor','Modificar_Usuario.php']],
	 ['Menu', null,null,
	    ['Configuracion Menu','Configurar_Menu.php'],
		['Asociar Menu','Modificar_Usuario.php']],
	 ['Master de Pines', null,null,
	    ['Ingreso','Master_Pines.php']],
	 ['Cuenta Contables', null,null,
	    ['Ingreso','Master_Pines.php']],
	 ['Serie de POS', null,null,
	    ['Ingreso','Master_Pines.php'],
	 	['Modificar','Master_Pines.php']],
	 ],
	
	['Reportes', null,null,
	 ['Inventario Stock','Inventario_Stock.php'],
	 ['Reporte de Venta','Reporte_Ventas.php'],
	 ['Reporte de Comisiones',null,null,
	 	['Comisiones al Detalle','Reporte_Comisiones.php'],
		['Comisiones Generales','Reporte_Comisiones_General.php']],
	 ['Reporte de Reservas','Reporte_Reservas.php']	, 
	],
	 		['Salir', '-', null,]

];
