<?php
	class  ExcelGen {
	      var  $excel_data;      	// a buffer for store excel stream data
	      var  $excel_filename;     // excel filename
		  var  $crow     = 0;       // Numero de fila actual
          var  $ccol     = 0;       // Numero de columno actual

	     // Default constructor
	     function  ExcelGen($excel_filename='excelgen',$excel_wksheetname='david')
	     {
		$this->excel_data="";		             // a buffer for store excel stream data
		$this->excel_filename=$excel_filename;	     // excel filename
	
		$this->ExcelStart();
	     }
     
	     // start of the excel file
	     function ExcelStart()
	     {
	     	// start of excel file header
	     	$this->excel_data = pack( "vvvvvv", 0x809, 0x08, 0x00,0x10, 0x0, 0x0 );
     	     }
     
	     // end of the excel file
	     function ExcelEnd()
	     {
	       $this->excel_data .= pack( "vv", 0x0A, 0x00 );
	     }
     
	     function NewLine()
         {
            $this->ccol = 0;
            $this->crow++;
            return;
         }
		 
		 function NewCol()
         {
            $this->ccol++;
            return;
         }

	     function WriteNumber($value )
	     {
		 	
	        $this->excel_data .= pack( "vvvvv", 0x0203, 14, $this->crow, $this->ccol, 0x00 );
	        $this->excel_data .= pack( "d", $value );
			$this->NewCol();
	     }

		 function WriteText($value )
	     {
	        $len = strlen( $value );
	        $this->excel_data .= pack( "v*", 0x0204, 8 + $len, $this->crow, $this->ccol, 0x00, $len );
	        $this->excel_data .= $value;
			$this->NewCol();
	     }
	 
	     // send generated xls as stream file
	     function SendFile()
	     {
	     	//Close Excel File         
	        $this->ExcelEnd();
	        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT");
	        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
	        header ( "Pragma: no-cache" );
	        header ( "Content-type: application/octet-stream; name=$this->excel_filename.xls" );
	        header ( "Content-Disposition: attachment; filename=$this->excel_filename.xls"); 
	        header ( "Content-Description: PHP ExcelGen Class" );
	        print $this->excel_data;
	     }

	} // end of the class ExcelGen() class
?>
