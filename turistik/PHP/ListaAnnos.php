<?php	
require ("../Clases/ClaseConexion.inc.php");
include( "../Clases/class.TemplatePower.inc.php"); 
require ("../Clases/ClaseUtil.inc.php");
//make a new TemplatePower object
$tpl = new TemplatePower( "../Plantillas/ListaAnnos.tpl" );
$miConexion= new ClaseConexion;
$_Util=new Util;
$tpl->prepare();
$tpl->assign("fecha", $_Util->Fecha());

$anos=array('2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020');
$i=0;
foreach ($anos as $actual ) 
    {
        $i++;
        $tpl->newBlock("bloqueano");
        $tpl->assign("CODIGOANO", $actual );
        $tpl->assign("DESCRIPCIONANO", $actual );
		if ($_POST['ano'] == $actual ) 
           {
               $tpl->assign("SELECCIONAR", 'selected' );
           }
       
    }	

$tpl->printToScreen();
?>
