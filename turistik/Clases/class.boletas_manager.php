<?php
	
    Class Boletas_Manager
    {
        
        
        var $descuento;
        var $precio_total;
        var $excursion;
        var $result;

        
        function __construct($precio_total,$excursion)
        {
            if (count($excursion) > 0)
            {
                $this-> precio_total = $precio_total;
                $this-> excursion = $excursion;

                $this->calcular_descuento();
            }

        }


        function calcular_descuento()
        {
            
            $miConexion = new ClaseConexion;
            $miConexion->Conectar();
            $query=$miConexion->EjecutaSP("Consulta_precio_final_total","  ".$_SESSION['codigo_suc'].", ".$_SESSION['codigo_usu'].", @total ");
            $Salida=$miConexion->ObtenerResultadoSP("@total");
            $r = mysql_fetch_assoc($Salida);
            $total_excursion = $r['@total'];
            

            $porcentaje = ($total_excursion * 100) / $this-> precio_total;
            $descuento = 100-$porcentaje;
            $descuento = round($descuento, 2);
            $this->descuento = $descuento;   
            
        }
        
        
        function Ingresar_Cabecera($boleta,$descuento,$fop,$tipo,$excursion,$cantidad, $pasajeros_cant, $tipo)
        {

            $miConexion = new ClaseConexion;
            $miConexion->Conectar();
            $query=$miConexion->EjecutaSP("Ingresar_Cabecera_Boletas_Manager","  ".$boleta.",'".date('Y-m-d')."', ".$descuento.", ".$_SESSION['codigo_suc'].", '".$fop."', '".$tipo."' ");
            
            for ( $e=0; $e<count($excursion); $e++) 
            {

                $adultos = $pasajeros_cant[$excursion[$e]]['adt'];
                $ninos = $pasajeros_cant[$excursion[$e]]['chd'];
                     
                if ($adultos>0)
                {
                    //item excursion solo adulto
                    $miConexion->Conectar();
                    $query=$miConexion->EjecutaSP("Ingresar_Detalle_Boletas_Manager"," 1, ".$boleta.",'".$excursion[$e]."', ".$cantidad[$e].", $adultos, 0, '".$tipo."' ");
                }
                if ($ninos>0 && $tipo == "E")
                {
                    //item excursion solo chd

                    $miConexion->Conectar();
                    $query=$miConexion->EjecutaSP("Ingresar_Detalle_Boletas_Manager"," 2, ".$boleta.",'".$excursion[$e]."', ".$cantidad[$e].", 0, $ninos, '".$tipo."' ");  
                }

                $adultos = 0;
                $ninos = 0;
                
  
            }
         
        }
        
    }
?>