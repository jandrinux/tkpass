<?php 
session_start(); 
require ("../Clases/ClaseConexion.inc.php");
include( "../Clases/class.TemplatePower.inc.php"); 
require ("../Clases/ClaseUtil.inc.php");

//session_name("loginUsuario"); 

//cambiamos la duraci�n a la cookie de la sesi�n
session_set_cookie_params(0, "/", $HTTP_SERVER_VARS["HTTP_HOST"], 0);
if ($_SESSION["autentificado"] != "SI") 
    { 
       header("Location: Login.php"); 
    } 
else 
    { 
        //calculamos el tiempo transcurrido 
        $fechaGuardada = $_SESSION["ultimoAcceso"]; 
        $ahora = date("Y-n-j H:i:s"); 
        $tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada)); 
        //comparamos el tiempo transcurrido 
        if($tiempo_transcurrido >= 2000) 
            { 
                //si pasaron 10 minutos o m�s 
                session_destroy(); // destruyo la sesi�n 
                header("Location: Login.php"); //env�o al usuario a la pag. de autenticaci�n 
                //sino, actualizo la fecha de la sesi�n 
            }
        else 
           { 
               $_SESSION["ultimoAcceso"] = $ahora; 
           }
	}
	
$tpl = new TemplatePower( "../Plantillas/DetalleComisionesGenerales.tpl" );

$_Util=new Util;
$miConexion= new ClaseConexion;
$tpl->prepare();
$tpl->assign("fecha", $_Util->Fecha());

$miConexion->Conectar();
$queryDistribuidor=$miConexion->EjecutaSP("Consulta_Distribuidor_Codigo","'".$_GET['CODIGODISTRIBUIDOR']."'");
$rowDistribuidor= mysql_fetch_assoc($queryDistribuidor);
$tpl->assign("_ROOT.CODIGODIST",$rowDistribuidor['NOMBRE_FANTASIA_DIST']);
$tpl->assign("_ROOT.CODIGODISTRIBUIDORREAL",$_GET['CODIGODISTRIBUIDOR']);

$miConexion->Conectar();
$queryVendedor=$miConexion->EjecutaSP("Consulta_Usuario_Codigo","".$_GET['CODIGOVENDEDOR']."");	
$rowVendedor= mysql_fetch_assoc($queryVendedor);
$tpl->assign("_ROOT.CODIGOVEND",$rowVendedor['NOMBRE_USUA']." ".$rowVendedor['APELLIDO_USUA']);
$tpl->assign("_ROOT.CODIGOVENDEDORREAL",$_GET['CODIGOVENDEDOR']);

$miConexion->Conectar();
$queryPromotor=$miConexion->EjecutaSP("Consulta_Promotor_porCodigo","".$_GET['CODIGOPROMOTOR']."");	
$rowPromotor= mysql_fetch_assoc($queryPromotor);
$tpl->assign("_ROOT.CODIGOPROM",$rowPromotor['NOMBRE_USUA']." ".$rowPromotor['APELLIDO_USUA']);
$tpl->assign("_ROOT.CODIGOPROMOTORREAL",$_GET['CODIGOPROMOTOR']);

//echo $_GET['MODALIDAD'];
//////////////////////////////////////QUE TIPO DE USUARIO ES///////////////////////////////////////////////
$miConexion->Conectar();
$queryPerfil=$miConexion->EjecutaSP("Consulta_Perfil_Usuario","'".$_SESSION['usuario']."',@tipo");
$parametrosSalidaPerfil=$miConexion->ObtenerResultadoSP("@tipo");
$rowPerfil= mysql_fetch_assoc($parametrosSalidaPerfil);
///////////////////////////////////////////////////////////////////////////////////////////////////////////
$miConexion->Conectar();
$queryDetalleComisionGeneral=$miConexion->EjecutaSP("Consulta_Detalle_Comisiones_Generales","".$_GET['CODIGODISTRIBUIDOR'].",".$_GET['CODIGOVENDEDOR'].",".$_GET['CODIGOPROMOTOR'].",".$_GET['MODALIDAD']."");
while ($rowDetalleComisionGeneral = mysql_fetch_assoc($queryDetalleComisionGeneral))
	{
		$tpl->newBlock("detallecomision");
		$tpl->assign("FOLIO",$rowDetalleComisionGeneral['FOLIO_TAR_COM_TEMP']);
		$perfil=$rowPerfil['@tipo'];
		if ($_GET['MODALIDAD']==1)
			{
				$tpl->assign("MODALIDAD","SIN TARJETA PREVIA");
				$tpl->assign("FOLIOANTERIOR","");
				$PRECION=number_format($rowDetalleComisionGeneral['PRECIO_REAL_COM_TEMP'],2,",",".");
				$PRECION_PESO="$".$PRECION;
				$tpl->assign("PRECIO",$PRECION_PESO);
				//echo "tipo".$rowPerfil['@tipo']; 
				
				if ($rowPerfil['@tipo']=='3')
					{
						
						$tpl->assign("COMISIONDISTRIBUIDOR","");
						$tpl->assign("COMISIONPROMOTOR","");
				
					}
				else
					{
						$COMISIONDISTRIBUIDOR=number_format($rowDetalleComisionGeneral['I_COMISION_D_COM_TEMP'],2,",",".");
						$COMISIONDISTRIBUIDOR_PESO="$".$COMISIONDISTRIBUIDOR;
						$tpl->assign("COMISIONDISTRIBUIDOR",$COMISIONDISTRIBUIDOR_PESO);
						$COMISIONPROMOTOR=number_format($rowDetalleComisionGeneral['I_COMISION_P_COM_TEMP'],2,",",".");
						$COMISIONPROMOTOR_PESO="$".$COMISIONPROMOTOR;
						$tpl->assign("COMISIONPROMOTOR",$COMISIONPROMOTOR_PESO);
				
					}
				
				$COMISIONVENDEDOR=number_format($rowDetalleComisionGeneral['I_COMISION_V_COM_TEMP'],2,",",".");
				$COMISIONVENDEDOR_PESO="$".$COMISIONVENDEDOR;
				$tpl->assign("COMISIONVENDEDOR",$COMISIONVENDEDOR_PESO);
				
				$tpl->assign("_ROOT.MODO",1);
				$tpl->assign("_ROOT.PERFIL",$perfil);
			}
		else
			{
				$tpl->assign("MODALIDAD","CON TARJETA PREVIA");
				$tpl->assign("FOLIOANTERIOR",$rowDetalleComisionGeneral['FOLIO_TAR_ANTERIOR_COM_TEMP']);
				$PRECION=number_format($rowDetalleComisionGeneral['PRECIO_II_COM_TEMP'],2,",",".");
				$PRECION_PESO="$".$PRECION;
				$tpl->assign("PRECIO",$PRECION_PESO);
				if ($rowPerfil['@tipo']=='3')
					{
						$tpl->assign("COMISIONDISTRIBUIDOR","");
					}
				else
					{
						$COMISIONDISTRIBUIDOR=number_format($rowDetalleComisionGeneral['II_COMISION_D_COM_TEMP'],2,",",".");
						$COMISIONDISTRIBUIDOR_PESO="$".$COMISIONDISTRIBUIDOR;
						$tpl->assign("COMISIONDISTRIBUIDOR",$COMISIONDISTRIBUIDOR_PESO);
					}
				
				$COMISIONVENDEDOR=number_format($rowDetalleComisionGeneral['II_COMISION_V_COM_TEMP'],2,",",".");
				$COMISIONVENDEDOR_PESO="$".$COMISIONVENDEDOR;
				$tpl->assign("COMISIONVENDEDOR",$COMISIONVENDEDOR_PESO);
				if ($rowPerfil['@tipo']=='3')
					{
						$tpl->assign("COMISIONPROMOTOR","");
					}
				else
					{
					
							$COMISIONPROMOTOR=number_format($rowDetalleComisionGeneral['II_COMISION_P_COM_TEMP'],2,",",".");
							$COMISIONPROMOTOR_PESO="$".$COMISIONPROMOTOR;
							$tpl->assign("COMISIONPROMOTOR",$COMISIONPROMOTOR_PESO);
					}
			
				$tpl->assign("_ROOT.MODO",2);
				$tpl->assign("_ROOT.PERFIL",$perfil);
			}
		if ($rowPerfil['@tipo']=='3')
			{
				$tpl->assign("INGRESO","");
			}
		else
			{
				$INGRESO=number_format($rowDetalleComisionGeneral['INGRESO'],2,",",".");
				$INGRESO_PESO="$".$INGRESO;
				$tpl->assign("INGRESO",$INGRESO_PESO);
			}
		
		
		$miConexion->Conectar();
		$queryProducto=$miConexion->EjecutaSP("Consulta_Producto","'".$rowDetalleComisionGeneral['CODIGO_PROD_COM_TEMP']."'");
		$rowProducto= mysql_fetch_assoc($queryProducto);
		$tpl->assign("PRODUCTO",$rowProducto['NOMBRE_PROD']);
		$Fecha=$_Util->FormatearFecha($rowDetalleComisionGeneral['FECHA_COM_TEMP']);	
		$tpl->assign("FECHA",$Fecha);
		if ($rowDetalleComisionGeneral['VENTA_ANTERIOR']==2)
			{
				$tpl->assign("VENTA","ACTUAL");
			}
		else
			{
				if ($rowDetalleComisionGeneral['VENTA_ANTERIOR']==3)
					{
						$tpl->assign("VENTA","ANTERIOR");
					}
			}
	
	
	}


$tpl->printToScreen();
?>
