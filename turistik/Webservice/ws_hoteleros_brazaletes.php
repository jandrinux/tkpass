<?PHP
//clase soap(;)
	require ("../Clases/ClaseConexion.inc.php");   
	require_once('lib/nusoap.php'); 
	
	$webServer = new soap_server(); 
	$webServer->soap_defencoding = 'UTF-8';
	$namespace = 'http://www.tkpass.cl/turistik/Webservice/ws_hoteleros_brazaletes.php?wsdl'; 
	$webServer->configureWSDL('WS'); 
	$webServer->wsdl->schemaTargetNamespace = $namespace; 
	
	$webServer->wsdl->addComplexType(
		'Brazalete',
		'complexType',
		'struct',
		'all',
		'',
		array(
			'serial' => array('name' => 'serial','type' => 'xsd:string'),
			'box' => array('name' => 'box','type' => 'xsd:string'),
			'fecha' => array('name' => 'fecha','type' => 'xsd:string')
		)
	);
	
	$webServer->wsdl->addComplexType(
		'Brazaletes',
		'complexType',
		'array',
		'',
		'SOAP-ENC:Array',
		array(),
		array
		(
		array(
		 'ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:Brazalete[]'
		)
		),
		'tns:Brazalete'
	);
	
	$methodName = 'ListarBrazaletes'; 
	$input = array(); 
	$output = array('return' => 'tns:Brazaletes'); 
	$soapAction = false; 
	$style = 'rpc'; 
	$use = 'encoded'; 
	$description = 'Listado de brazaletes hoteleros por box'; 
	$webServer->register($methodName, $input, $output, $namespace, $soapAction, $style, $use, $description);
	function ListarBrazaletes()
	{
		$miConexion= new ClaseConexion;
		$miConexion->Conectar();
		
		$query=$miConexion->EjecutaConsulta(" SELECT * FROM DETALLE_STOCK WHERE IMPRESO is null AND ESTADO = 0" );
		while ($con = mysql_fetch_row($query))
		{
			$SERIAL = $con['SERIAL'];
			$BOX  = $con['NOMBREBOX'];
			$FECHA = $con['FECHA_CREACION'];
			
			$elements[] = array(					
					"serial" => $SERIAL,
					"box" => $BOX ,
					"fecha" => $FECHA
			);	
		
		}  
		mysql_free_result($query); 
		mysql_close();          
		
		return $elements;
	}
	
			
	
	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$webServer->service($HTTP_RAW_POST_DATA); 

?>