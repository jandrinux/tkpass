<?php	
require ("../Clases/ClaseConexion.inc.php");
include( "../Clases/class.TemplatePower.inc.php"); 
require ("../Clases/ClaseUtil.inc.php");
//make a new TemplatePower object
$tpl = new TemplatePower( "../Plantillas/ListaImprimir.tpl" );
$miConexion= new ClaseConexion;
$_Util=new Util;
$tpl->prepare();
$tpl->assign("fecha", $_Util->Fecha());

$imprimir=array('NO','SI');
$i=-1;
foreach ($imprimir as $actual ) 
    {
        $i++;
        $tpl->newBlock("bloqueimprimir");
        $tpl->assign("CODIGOIMPRIMIR", $i );
        $tpl->assign("DESCRIPCIONIMPRIMIR", $actual );
		if ($_POST['imprimir'] == $i  ) 
            {
                $tpl->assign("SELECCIONAR", 'selected' );
            }
       
    }	
$tpl->printToScreen();
?>
