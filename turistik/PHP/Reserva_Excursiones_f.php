<?
include("menus.php");
require ("../functions/tkpass.php");

$miConexion = new ClaseConexion;
$_Util=new Util;
$usuario=$_SESSION['usuario'];
$g = 20;
$cuenta = 0;

if(isset($_POST['fecha']))
{
    $fecha = $_POST['fecha'];
}
else
{ 
    $fecha = date('d/m/Y');
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<title> </title>

 <meta charset="utf-8" />
   <link rel="stylesheet" href="../../css/plantilla_matriz.css" type="text/css" media="all">
    <link href="../estilos1.css" rel="stylesheet" type="text/css">
    <link href="../../css/noticias.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
    <script type="text/javascript" src="../../js/ui.datepicker.js"></script>
    
    <script type="text/javascript">
  	$(function() {
		$("#fecha").datepicker();
	});

    var auto_refresh = setInterval(
    function ()
    {
    $('#ReloadThis').load('estado_carrito.php');
    }, 3000); // refresh every 10000 milliseconds
    
    var auto_refresh = setInterval(
    function ()
    {
    $('#principal').load('matriz_excursiones_f.php');
    }, 30000); // refresh every 10000 milliseconds
    
    </script>
    
</head>
<body>
 <div id="global">
 
 <div id="ReloadThis" style="margin: 20 0 0 520px; position: absolute;"><? include ('estado_carrito.php') ?></div>
 
 
 <div id="cabecera">

      <table width="482" border="0" cellpadding="0" cellspacing="0">  
    	<tr>
          <td width="80" rowspan="4"><img src="../Imagenes/IMG_6902.jpg" width="80" height="80" /></td>
          <td width="13">&nbsp;</td>
          <td width="389">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><strong id="TituloPlomo"><b>Consultar Cantidades de Excursiones </b></strong></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
        </tr>    
    
      </table>

 </div>
  
 <div id="navegacion">
     <table>
        <tr>
            <td>    
                <form name="form1" method="post" action="Reserva_Excursiones_c.php?g=<?=$g?>">
                    <table width="350" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="100" class="celdalazul_peque">
                         Fecha
                        </td>
                        <td width="100" class="celdalCELEST">
                         <input type="text" name="fecha" id="fecha" value="<?=$fecha?>" readonly="">
                        </td>
                        <td width="100" class="celdalCELEST">
                         <input name="consulta" type="submit" class="BotonTurist_Azul"  value="Consultar" />
                        </td>
                      </tr>
                    
                    </table>
                </form>
            </td>
             <td>
                 <form name="cant_pax" action="cambio_cant_pax.php?tipo=2" method="post">
                    <table align="left" width="310px" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="celdalazul_peque" width="100px" >Cantidad Pax: </td>
                            <td class="celdalCELEST" width="60px">
                            <select name="cant_pax" class="seleccionTurist_90">
                            <?
                            $z=1;
                            for($z=1; $z<=30; $z++)
                            {
                                if($_COOKIE['cantidad'] == $z)
                                {
                                    echo "<option value='".$z."' selected>".$z."</option>";
                                }
                                else
                                {
                                    echo "<option value='".$z."'>".$z."</option>";
                                }
                            
                            }
                            ?> 
                            </select>
                            </td>
                            <td class="celdalCELEST">
                            <input align="right" name="pax" type="submit" class="BotonTurist_Azul" value="Aceptar" />
                            </td>
                        </tr>
                    </table>
                </form> 
             </td>
        </tr>
        <tr>
            <td colspan="2">
                <table align="left" width="340px" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="42" class="celdalazul_peque">Excursiones </td>
                        <td width="120" class="celdalCELEST">
                            <select class="seleccionTurist_180" name="tipos_excursiones" onchange="location = this.value">
                            <option value="Reserva_Excursiones_c.php">--SELECCIONE--</option>
                            <option value="Reserva_Excursiones_c.php?g=0">--TODOS--</option>
                            <?
                            $miConexion->Conectar();
                            $queryGrupos=$miConexion->EjecutaSP("Consulta_Grupos_Escursiones","");
                            while ($rowgrupos = mysql_fetch_assoc($queryGrupos))
                            {
                                if ($_GET['g'] == $rowgrupos['ID_GRUPO'])
                                {
                                    echo "<option selected value=Reserva_Excursiones_c.php?g=".$_GET['g'].">".htmlentities($rowgrupos['NOMBRE_GRUPO'])."</option>" ;
                                }
                                else
                                {
                                    if($rowgrupos['ID_GRUPO']==20)
                                    {
                                        echo "<option value=Reserva_Excursiones_f.php?g=".$rowgrupos['ID_GRUPO'].">".htmlentities($rowgrupos['NOMBRE_GRUPO'])."</option>" ;
                                    }   
                                    else
                                    {
                                        echo "<option value=Reserva_Excursiones_c.php?g=".$rowgrupos['ID_GRUPO'].">".htmlentities($rowgrupos['NOMBRE_GRUPO'])."</option>" ;
                                    }
                                }
                            }
                            mysql_free_result($queryGrupos); 
                            mysql_close();
                            ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
     </table>
 

 </div><!--cierre navegacion -->
 
 
 
 <div id="Layer4" style="position:absolute; left:835px; top:52px; width:128px; height:80px; z-index:4">
    <img src="../Imagenes/turistik107_118.jpg" width="107" height="118" />
 </div>
 

 <div id="principal">
    <? include ('matriz_excursiones_f.php') ?>
 </div>
 
 <div class="anuncios1">

    <div id="titulo_noticia" style="height:30px; background: #D70000;"><div style="float: left; color: #FFF; padding: 5px 0 0 5px;"><b>Turistik Informa </b></div></div>
    
    <div>
        <div id="cont_6d8b98575023d062d911ccdca935eef3">
            <span id="h_6d8b98575023d062d911ccdca935eef3">
            <script type="text/javascript" src="http://www.meteored.cl/wid_loader/6d8b98575023d062d911ccdca935eef3"></script>
        </div>
    </div>

    <ul id="widget">
    
    <?
    $objStock = new ClaseConexion;
    $objStock->Conectar();
    $queryStock=$objStock->EjecutaConsulta("select * from noticias where activa=1 and Fecha > '".date("Y/m/d")."'  order by idnoticias desc");        
    while($row_listar_stock=mysql_fetch_array($queryStock))
    {
     if($row_listar_stock['Externos']==1)
     {
            if($_SESSION['cod_tusua']==4)
            {
                 ECHO  	"<li>";
            	 echo "<div class='titulo'>".$row_listar_stock['titulo']. "</div>";
               
            	 echo '	<h4><strong style="color:#D70000">'.substr($row_listar_stock["fecha"],11)." |</strong>  ". $row_listar_stock["contenido"].'</h4>';
            			
            	 echo '	</li>';
            } 
     }
     else
     {
        if($row_listar_stock['Turistik']==1)
        {
                 ECHO  	"<li>";
                 echo "<div class='titulo'>".$row_listar_stock['titulo']. "</div>";
             	 echo '	<h4><strong style="color:#D70000">'.substr($row_listar_stock["fecha"],11)." |</strong>  ". $row_listar_stock["contenido"].'</h4>';
            		
            	 echo '	</li>';
        }
        
     }
     
        if($row_listar_stock['Turistik']==1 && $row_listar_stock['Externos']==1)
        {
                 ECHO  	"<li>";
                 echo "<div class='titulo'>".$row_listar_stock['titulo']. "</div>";
             	 echo '	<h4><strong style="color:#D70000">'.substr($row_listar_stock["fecha"],11)." |</strong>  ". $row_listar_stock["contenido"].'</h4>';
            		
            	 echo '	</li>';
        }
    
    
    }
    mysql_free_result($queryStock); 
    mysql_close();
    ?>
    
    </ul>

 </div> <!--cierre anuncios -->
 
 
 
 
 <div id="pie">
    <!--Pie-->
 </div>
 
 </div>
</body>
</html>