<?php
//ini_set('display_errors', 1);
session_start();
date_default_timezone_set("America/Santiago");
if ($_SESSION["autentificado"] != "SI" )
{
    header("location: Login.php");
}
//error_reporting(E_ALL);

require ("set_timeout.php");
require ("../Clases/ClaseConexion.inc.php");
require ("../Clases/ClaseUtil.inc.php");
include ("../functions/tkpass.php");
include ("../functions/check_descuentos.php");
include ("../functions/consulta_descuento.php");
include ("../functions/consulta_promociones.php");
include ("../functions/check_promociones.php"); // BENEFICIOS Y COMBOS
include ("../Clases/ClaseFinCompra.php");
require ("../Clases/class.boletas_manager.php");
require ("../Clases/class.mail.php");
require ("SII/class.boletaGen.php");
//require ("../Webservice/ws.php");
//
$Obj_Tkpass = new tkpass(); 

//$tipo_opcion = //combo = 2 // descuento = 1 // beneficio = 3 // ninguno = 4 // promocion 5 // ofertas 6
$tipo_opcion = $_POST['radio']; 
$tipoReserva = $_POST['tipoReserva'];
$fop = $_POST['fop'];
$fop_manager = $_POST['fop'];
$voucherTBK = $_POST['voucherTBK'];
$codSocio = $_POST['codSocio']; 
$tipo_tarjeta = $_POST['tipo_tarjeta'];
$ultimos_digitos = $_POST['ultimos_digitos'];
$promocion_act = $_POST['id_promo']; // id de la promocion o combo
$combo_promocion = $_POST['id_combo']; // valor 1 es combo valor 0 es promocion
$beneficio_act = $_POST['beneficio_activado'];
$total_tarjeta = $_POST['total_t'];
$total_efectivo = $_POST['total_e'];
$redondea = $_POST['valor_redondeo'];
$estado_boleta = $_POST['estado'];
$tipo_venta = $_POST['tipo_venta']; // if 2 solo es excursion 6
$tipo_mayorista = $_POST['tipo_mayorista'];
$dscto_coordinador = $_POST['dscto_cordinador'];
$dscto_especial = $_POST['descuento_especial'];
$tot_exento =  str_replace("$", "", $_POST['tot_exento']);
$tot_exento =  str_replace(",", "", $tot_exento);
$flag_correo_descuento = false;
$genera_boleta = true;
$suma_excursiones = 0;
$boleta_exenta = "";
$boleta_afecta = "";
$promo_adt = "";
$promo_chd = "";



if ($tipoReserva == 3 && $codSocio != '')
{
    $tipo_opcion = 0;
}

if ($combo_promocion == 1)
{
    $promo_adt = unserialize($_POST['promo_adt']);
    $promo_chd = unserialize($_POST['promo_chd']);
    
    $total_combo_adt = $_POST['total_combo_adt'];
    $total_combo_chd = $_POST['total_combo_chd'];
    
    $arreglo_promocion = unserialize($_POST['lista_promo']); // arreglo de promocion o combo  
    $excursiones_dscto = $arreglo_promocion[$promocion_act]; // excursiones a aplicar combo y descto
    $num_excursiones_arreglo = count($arreglo_promocion[$promocion_act]); // numerpo de excursiones
    
    $cantidad_promociones = unserialize($_POST['cant_promo']); 
    $cant_promo_adt = $cantidad_promociones[$promocion_act]['adt']; // cantidad de promociones para adt
    $cant_promo_chd = $cantidad_promociones[$promocion_act]['chd']; // cantidad de promociones para chd
    
    $precios_totales = unserialize($_POST['precios_totales']); 
    $precio_total_adt = $precios_totales[$promocion_act]['suma_adt'];
    $precio_total_chd = $precios_totales[$promocion_act]['suma_chd'];

    $comision_promocion = $_POST['comision_promocion'];
    
    $dscto_unitario_combo_adt =  round(($precio_total_adt - $total_combo_adt)/$num_excursiones_arreglo);
    $dscto_unitario_combo_chd =  round(($precio_total_chd - $total_combo_chd)/$num_excursiones_arreglo);
    
}

if ($fop == "W")
{

    $FinCompra = new FinCompra($suc=18, $tipoReserva, $codSocio, $fop);
    $fop = $FinCompra->fop;
    $tipoReserva = $FinCompra->tipoReserva;
    $codigo_distribuidor = $FinCompra->codigo_distribuidor;
    $cod_vendedor = $FinCompra->codigo_vendedor;
    
    
    $url ="cod_vendedor=".$cod_vendedor."&codigo_dist=".$codigo_distribuidor."&fop=".$fop."&tipo_re=".$tipoReserva."&tts=".$boletaTTS."&ttk=".$boletaTTK;
    header("location:finaliza_webpay.php?$url");
    exit();

}else{
    

    if ($tipoReserva == '4') 
    {
        $fop='E';
    }
    

    $FinCompra = new FinCompra($_SESSION['codigo_suc'], $tipoReserva, $codSocio, $fop);
    
    $cod_usuario = $FinCompra->cod_usuario;
    $suc_reserva = $FinCompra->suc_reserva;
    $suc_venta = $FinCompra->suc_venta;
    $fop = $FinCompra->fop;
    $cod_cierre = $FinCompra->cod_cierre;
    $login_usua_cierre = $FinCompra->login_cierre;
    $tipoReserva = $FinCompra->tipoReserva;
    $codigo_distribuidor = $FinCompra->codigo_distribuidor;


    include('aplica_descuentos.php');

    $lista_v = explode("/",$voucher_generado);
    $lista_v = array_filter($lista_v);


    if ($tipoReserva == '4') // si es una venta de tipo reserva mayorista
    {
        // guardar archivo adjunto
    }
   
    if ($redondea != 0)
    {

        $miConexion->Conectar();
        $miConexion->EjecutaSP("Actualiza_Precio_Re"," 2, '".$redondea_codigo_re."', '".$redondea."' ");
    }

    if ($tipoReserva == 4 || $tipoReserva == 3 || $tipoReserva == 2) {
        $genera_boleta = false;
    }
    if ($tipoReserva == 2 && $fop == "TC" ) {
        $genera_boleta = true;
    }


    ## SII GENERACION DE BOLETAS
    #

    if ($genera_boleta == true) {
        

        $Manager = new Boletas_Manager($precio_real, $excursion_array);
        $des_mng = $Manager->descuento;

        for ( $e=0; $e<count($excursion_array); $e++) 
        {
            $miConexion->Conectar();
            $query = $miConexion->EjecutaSP("Consulta_descuento_excursion","  '".$excursion_array[$e]."', ".$suc_reserva.", ".$_SESSION['codigo_usu'].", @total ");
            $Salida=$miConexion->ObtenerResultadoSP("@total");
            $r = mysql_fetch_assoc($Salida);
            $total_excursion[$e] = $r['@total'];  
            $suma_excursiones = $total_excursion[$e] + $suma_excursiones;     
        }


        if($suma_excursiones > 0)
        {
            include_once("sii.php");
        }

        
    }                  

    include_once("respuesta_compra.php");  ## webservice de brazaletes


    if ($genera_boleta == true && $suma_excursiones > 0) {
    
        //// INGRESAR BOLETAS MANAGER
        try {   
            
            //Boleta afecta TTK
            $tipo = 'E';
            $Manager->Ingresar_Cabecera($boleta_exenta,$des_mng,$fop_manager,$tipo,$excursion_array,$cantidadPax, $pasajeros_cant, $tipo );
            
            // Solo agregar boleta TTS
            
            for ($i=0; $i<count($excursion_array); $i++)
            {
                if ($excursion_array[$i] == 6)
                {
                    unset($excursion_array[$i]);
                    unset($cantidadPax[$i]);
        
                }
            }
            
            sort($excursion_array);
            sort($cantidadPax);

            
            //Boleta afecta TTK y TTS
            $tipo = 'A';
            if ($tipo_venta == 1 || $tipo_venta == 2)
            {
                $Manager->Ingresar_Cabecera($boleta_afecta,0,$fop_manager,$tipo,$excursion_array,$cantidadPax, $pasajeros_cant, $tipo );
                
            }
            
        
       }catch (Exception $e) {
            echo 'Excepci�n capturada: ',  $e->getMessage(), "\n";
            
       }

        $url_exento = base64_encode($url_exento);
        $url_afecto = base64_encode($url_afecto);
        $link = "boarding_pass.php?v=$voucher_generado&be=$url_exento&ba=$url_afecto";
    }
    else
    {
        $link = "boarding_pass.php?v=$voucher_generado";
    }


    foreach ($array_temporal as $id_temporal) {
       $miConexion->conectar();   
       $miConexion->EjecutaSP("elimina_pasajero_temporal", " 1, $id_temporal ");
    }

mysql_close();
header("Location:".$link);
}    
?>