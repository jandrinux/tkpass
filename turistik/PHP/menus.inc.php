<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t&iacute;tulo</title>
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
</head>

<script language="JavaScript" >
var A_MENUS = [];
function menu (a_items, a_tpl) 
	{
		if (!document.body || !document.body.style)
		return;
		this.a_config = a_items;
		this.a_tpl = a_tpl;
		this.n_id = A_MENUS.length;
		this.a_index = [];
		this.a_children = [];
		this.expand      = menu_expand;
		this.collapse    = menu_collapse;

		this.onclick     = menu_onclick;
		this.onmouseout  = menu_onmouseout;
		this.onmouseover = menu_onmouseover;
		this.onmousedown = menu_onmousedown;

		this.a_tpl_def = {
		'block_top'  : 16,
		'block_left' : 16,
		'top'        : 20,
		'left'       : 4,
		'width'      : 120,
		'height'     : 22,
		'hide_delay' : 0,
		'expd_delay' : 0,
		'css'        : {
			'inner' : '',
			'outer' : ''
		}
	};
	
	// assign methods and properties required to imulate parent item
	this.getprop = function (s_key) {
		return this.a_tpl_def[s_key];
	};

	this.o_root = this;
	this.n_depth = -1;
	this.n_x = 0;
	this.n_y = 0;

	// 	init items recursively
	for (n_order = 0; n_order < a_items.length; n_order++)
		new menu_item(this, n_order);

	// register self in global collection
	A_MENUS[this.n_id] = this;

	// make root level visible
	for (var n_order = 0; n_order < this.a_children.length; n_order++)
		this.a_children[n_order].e_oelement.style.visibility = 'visible';
		
}

// --------------------------------------------------------------------------------
function menu_collapse (n_id) {
	// cancel item open delay
	clearTimeout(this.o_showtimer);

	// by default collapse to root level
	var n_tolevel = (n_id ? this.a_index[n_id].n_depth : 0);
	
	// hide all items over the level specified
	for (n_id = 0; n_id < this.a_index.length; n_id++) {
		var o_curritem = this.a_index[n_id];
		if (o_curritem.n_depth > n_tolevel && o_curritem.b_visible) {
			o_curritem.e_oelement.style.visibility = 'hidden';
			o_curritem.b_visible = false;
			combos('visible');
		}
	}

	// reset current item if mouse has gone out of items
	if (!n_id){
		this.o_current = null;
		combos('visible');
	}
		//combos('visible');
			
		
}

// --------------------------------------------------------------------------------
function menu_expand (n_id) {

    
	// expand only when mouse is over some menu item
	if (this.o_hidetimer){
		combos('visible');
		return;
	}
	// lookup current item
	var o_item = this.a_index[n_id];

	// close previously opened items
	if (this.o_current && this.o_current.n_depth >= o_item.n_depth){
		this.collapse(o_item.n_id);
		combos('hidden');
	}
	this.o_current = o_item;

	// exit if there are no children to open
	if (!o_item.a_children){
		return;
	}
	// show direct child items
	for (var n_order = 0; n_order < o_item.a_children.length; n_order++) {
		var o_curritem = o_item.a_children[n_order];
		o_curritem.e_oelement.style.visibility = 'visible';
		o_curritem.b_visible = true;
	}

}

// --------------------------------------------------------------------------------
//
// --------------------------------------------------------------------------------
function menu_onclick (n_id) {
	// don't go anywhere if item has no link defined
	return Boolean(this.a_index[n_id].a_config[1]);
}

// --------------------------------------------------------------------------------
function menu_onmouseout (n_id) {

	// lookup new item's object	
	var o_item = this.a_index[n_id];

	// apply rollout
	o_item.e_oelement.className = o_item.getstyle(0, 0);
	o_item.e_ielement.className = o_item.getstyle(1, 0);
	
	// update status line	
	o_item.upstatus(7);

	// run mouseover timer
	this.o_hidetimer = setTimeout('A_MENUS['+ this.n_id +'].collapse();',
		o_item.getprop('hide_delay'));
	
}

// --------------------------------------------------------------------------------
function menu_onmouseover (n_id) {

	// cancel mouseoute menu close and item open delay
	clearTimeout(this.o_hidetimer);
	this.o_hidetimer = null;
	clearTimeout(this.o_showtimer);

	// lookup new item's object	
	var o_item = this.a_index[n_id];

	// update status line	
	o_item.upstatus();

	// apply rollover
	o_item.e_oelement.className = o_item.getstyle(0, 1);
	o_item.e_ielement.className = o_item.getstyle(1, 1);
	
	// if onclick open is set then no more actions required
	if (o_item.getprop('expd_delay') < 0)
		return;

	// run expand timer
	this.o_showtimer = setTimeout('A_MENUS['+ this.n_id +'].expand(' + n_id + ');',
		o_item.getprop('expd_delay'));
	
	

}

// --------------------------------------------------------------------------------
// called when mouse button is pressed on menu item
// --------------------------------------------------------------------------------
function menu_onmousedown (n_id) {
	
	// lookup new item's object	
	var o_item = this.a_index[n_id];

	// apply mouse down style
	o_item.e_oelement.className = o_item.getstyle(0, 2);
	o_item.e_ielement.className = o_item.getstyle(1, 2);

	this.expand(n_id);
//	this.items[id].switch_style('onmousedown');


}


// --------------------------------------------------------------------------------
// menu item Class
function menu_item (o_parent, n_order) {

	// store parameters passed to the constructor
	this.n_depth  = o_parent.n_depth + 1;
	this.a_config = o_parent.a_config[n_order + (this.n_depth ? 3 : 0)];

	// return if required parameters are missing
	if (!this.a_config) return;

	// store info from parent item
	this.o_root    = o_parent.o_root;
	this.o_parent  = o_parent;
	this.n_order   = n_order;

	// register in global and parent's collections
	this.n_id = this.o_root.a_index.length;
	this.o_root.a_index[this.n_id] = this;
	o_parent.a_children[n_order] = this;

	// calculate item's coordinates
	var o_root = this.o_root,
		a_tpl  = this.o_root.a_tpl;

	// assign methods
	this.getprop  = mitem_getprop;
	this.getstyle = mitem_getstyle;
	this.upstatus = mitem_upstatus;

	this.n_x = n_order
		? o_parent.a_children[n_order - 1].n_x + this.getprop('left')
		: o_parent.n_x + this.getprop('block_left');

	this.n_y = n_order
		? o_parent.a_children[n_order - 1].n_y + this.getprop('top')
		: o_parent.n_y + this.getprop('block_top');

	// generate item's HMTL
	document.write (
		'<a id="e' + o_root.n_id + '_'
			+ this.n_id +'o" class="' + this.getstyle(0, 0) + '" href="' + this.a_config[1] + '"'
			+ (this.a_config[2] && this.a_config[2]['tw'] ? ' target="'
			+ this.a_config[2]['tw'] + '"' : '')
			+ (this.a_config[2] && this.a_config[2]['tt'] ? ' title="'
			+ this.a_config[2]['tt'] + '"' : '') + ' style="position: absolute; top: '
			+ this.n_y + 'px; left: ' + this.n_x + 'px; width: '
			+ this.getprop('width') + 'px; height: '
			+ this.getprop('height') + 'px; visibility: hidden;'
			//+' -: -1;" '
			+' z-index: ' + this.n_depth + ';" '
			+ 'onclick="return A_MENUS[' + o_root.n_id + '].onclick('
			+ this.n_id + ');" onmouseout="A_MENUS[' + o_root.n_id + '].onmouseout('
			+ this.n_id + ');" onmouseover="A_MENUS[' + o_root.n_id + '].onmouseover('
			+ this.n_id + ');" onmousedown="A_MENUS[' + o_root.n_id + '].onmousedown('
			+ this.n_id + ');"><div  id="e' + o_root.n_id + '_'
			+ this.n_id +'i" class="' + this.getstyle(1, 0) + '">'
			+ this.a_config[0] + "</div></a>\n"
		);
	this.e_ielement = document.getElementById('e' + o_root.n_id + '_' + this.n_id + 'i');
	this.e_oelement = document.getElementById('e' + o_root.n_id + '_' + this.n_id + 'o');

	this.b_visible = !this.n_depth;

	// no more initialization if leaf
	if (this.a_config.length < 4)
		return;

	// node specific methods and properties
	this.a_children = [];

	// init downline recursively
	for (var n_order = 0; n_order < this.a_config.length - 3; n_order++)
		new menu_item(this, n_order);
		
}

// --------------------------------------------------------------------------------
// reads property from template file, inherits from parent level if not found
// ------------------------------------------------------------------------------------------
function mitem_getprop (s_key) {

	// check if value is defined for current level

	var s_value = null,
		a_level = this.o_root.a_tpl[this.n_depth];

	// return value if explicitly defined
	if (a_level)
		s_value = a_level[s_key];

	// request recursively from parent levels if not defined
	return (s_value == null ? this.o_parent.getprop(s_key) : s_value);
}
// --------------------------------------------------------------------------------
// reads property from template file, inherits from parent level if not found
// ------------------------------------------------------------------------------------------
function mitem_getstyle (n_pos, n_state) {

	var a_css = this.getprop('css');
	var a_oclass = a_css[n_pos ? 'inner' : 'outer'];

	// same class for all states	
	if (typeof(a_oclass) == 'string')
		return a_oclass;

	// inherit class from previous state if not explicitly defined
	for (var n_currst = n_state; n_currst >= 0; n_currst--)
		if (a_oclass[n_currst])
			return a_oclass[n_currst];
}

// ------------------------------------------------------------------------------------------
// updates status bar message of the browser
// ------------------------------------------------------------------------------------------
function mitem_upstatus (b_clear) {
	window.setTimeout("window.status=unescape('" + (b_clear
		? ''
		: (this.a_config[2] && this.a_config[2]['sb']
			? escape(this.a_config[2]['sb'])
			: escape(this.a_config[0]) + (this.a_config[1]
				? ' ('+ escape(this.a_config[1]) + ')'
				: ''))) + "')", 10);
}

function combos(visu) 
{ 
   selects=document.body.getElementsByTagName("SELECT"); 
   counter=selects.length; 
   for(var i=0;i<counter;i++) {selects[i].style.visibility=visu;}    
} 
</script>
<?php 
require ("../Clases/ClaseConexion.inc.php");
include( "../Clases/class.TemplatePower.inc.php"); 
require ("../Clases/ClaseUtil.inc.php");
$tipoUsuario='ADMINISTRADOR';//ADMINISTRADOR andreaPAZ

$miConexion= new ClaseConexion;
$miConexion->Conectar();
$queryUsuarioTipo=$miConexion->EjecutaSP("Consulta_Usuario_tipo","'".$tipoUsuario."'");
$rowUsuarioTipo = mysql_fetch_assoc($queryUsuarioTipo);

$miConexion->Conectar();
$queryMenu=$miConexion->EjecutaSP("Consulta_Menu","".$rowUsuarioTipo['COD_TUSUA']."");
while ($rowMenu = mysql_fetch_assoc($queryMenu))
	{
		if ($rowMenu['POSICION_MENU']==0 and $rowMenu['RELACION_MENU']=='00')//PRIMER MENU
			{
				$a=$a+1;
				$arreglo_uno[$a][0]=$rowMenu['TITULO_MENU'];
				$cont1=$cont1+1;
			}
		if ($rowMenu['POSICION_MENU']==1 and $rowMenu['RELACION_MENU']=='01')
			{
				$b=$b+1;
				$arreglo_dos[$b][1]=$rowMenu['TITULO_MENU'];
				$cont2=$cont2+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==2 and $rowMenu['RELACION_MENU']=='01' )
			{
				$c=$c+1;
				$arreglo_dos[$c][2]=$rowMenu['TITULO_MENU'];
				$arreglo_dos[$c][3]=$rowMenu['URL_MENU'];
				$cont3=$cont3+1;
			}
			
		if ($rowMenu['POSICION_MENU']==1 and $rowMenu['RELACION_MENU']=='02')
			{
				$d=$d+1;
				$arreglo_tres[$d][1]=$rowMenu['TITULO_MENU'];
				$cont4=$cont4+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==2 and $rowMenu['RELACION_MENU']=='02' )
			{
				$e=$e+1;
				$arreglo_tres[$e][2]=$rowMenu['TITULO_MENU'];
				$arreglo_tres[$e][3]=$rowMenu['URL_MENU'];
				$cont5=$cont5+1;
			}	
		if ($rowMenu['POSICION_MENU']==1 and $rowMenu['RELACION_MENU']=='03')
			{
				$f=$f+1;
				$arreglo_cuatro[$f][1]=$rowMenu['TITULO_MENU'];
				$cont6=$cont6+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==2 and $rowMenu['RELACION_MENU']=='03' )
			{
				$g=$g+1;
				$arreglo_cuatro[$g][2]=$rowMenu['TITULO_MENU'];
				$arreglo_cuatro[$g][3]=$rowMenu['URL_MENU'];
				$cont7=$cont7+1;
			}	
		if ($rowMenu['POSICION_MENU']==1 and $rowMenu['RELACION_MENU']=='04')
			{
				$h=$h+1;
				$arreglo_cinco[$h][1]=$rowMenu['TITULO_MENU'];
				$cont8=$cont8+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==2 and $rowMenu['RELACION_MENU']=='04' )
			{
				$i=$i+1;
				$arreglo_cinco[$i][2]=$rowMenu['TITULO_MENU'];
				$arreglo_cinco[$i][3]=$rowMenu['URL_MENU'];
				$cont9=$cont9+1;
			}
			
		if ($rowMenu['POSICION_MENU']==0 and $rowMenu['RELACION_MENU']=='10')//SEGUNDO MENU
			{
				$j=$j+1;
				$arreglo_seis[$j][0]=$rowMenu['TITULO_MENU'];
				$arreglo_seis[$j][1]=$rowMenu['RELACION_MENU'];
				$cont10=$cont10+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==1 and $rowMenu['RELACION_MENU']=='11')
			{
				$k=$k+1;
				$arreglo_siete[$k][1]=$rowMenu['TITULO_MENU'];
				$cont11=$cont11+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==2 and $rowMenu['RELACION_MENU']=='11' )
			{
				$l=$l+1;
				$arreglo_siete[$l][2]=$rowMenu['TITULO_MENU'];
				$arreglo_siete[$l][3]=$rowMenu['URL_MENU'];
				$cont12=$cont12+1;
			}
			
		if ($rowMenu['POSICION_MENU']==1 and $rowMenu['RELACION_MENU']=='12')
			{
				$m=$m+1;
				$arreglo_ocho[$m][1]=$rowMenu['TITULO_MENU'];
				$cont13=$cont13+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==2 and $rowMenu['RELACION_MENU']=='12' )
			{
				$a1=$a1+1;
				$arreglo_ocho[$a1][2]=$rowMenu['TITULO_MENU'];
				$arreglo_ocho[$a1][3]=$rowMenu['URL_MENU'];
				$cont14=$cont14+1;
			}
		if ($rowMenu['POSICION_MENU']==0 and $rowMenu['RELACION_MENU']=='20')//TERCER MENU
			{
				$b1=$b1+1;
				$arreglo_nueve[$b1][0]=$rowMenu['TITULO_MENU'];
				$cont15=$cont15+1;
			}
			
		if ($rowMenu['POSICION_MENU']==1 and ($rowMenu['RELACION_MENU']=='21' or $rowMenu['RELACION_MENU']=='22' or $rowMenu['RELACION_MENU']=='23' or $rowMenu['RELACION_MENU']=='24' or $rowMenu['RELACION_MENU']=='25' or $rowMenu['RELACION_MENU']=='26' or $rowMenu['RELACION_MENU']=='27'))
			{
				$c1=$c1+1;
				$arreglo_nueve[$c1][1]=$rowMenu['TITULO_MENU'];
				$arreglo_nueve[$c1][2]=$rowMenu['URL_MENU'];
				$cont16=$cont16+1;
			}	
		if ($rowMenu['POSICION_MENU']==0 and $rowMenu['RELACION_MENU']=='30')//CUARTO MENU
			{
				$d1=$d1+1;
				$arreglo_diez[$d1][0]=$rowMenu['TITULO_MENU'];
				$cont17=$cont17+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==1 and ($rowMenu['RELACION_MENU']=='31' or $rowMenu['RELACION_MENU']=='32' or $rowMenu['RELACION_MENU']=='33'))
			{
				$e1=$e1+1;
				$arreglo_diez[$e1][1]=$rowMenu['TITULO_MENU'];
				$arreglo_diez[$e1][2]=$rowMenu['URL_MENU'];
				$cont18=$cont18+1;
			}	
		if ($rowMenu['POSICION_MENU']==0 and $rowMenu['RELACION_MENU']=='40')//QUINTO MENU
			{
				$f1=$f1+1;
				$arreglo_once[$d1][0]=$rowMenu['TITULO_MENU'];
				$cont19=$cont19+1;
			}	
		if ($rowMenu['POSICION_MENU']==1 and $rowMenu['RELACION_MENU']=='41')
			{
				$g1=$g1+1;
				$arreglo_doce[$g1][1]=$rowMenu['TITULO_MENU'];
				$cont20=$cont20+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==2 and $rowMenu['RELACION_MENU']=='41' )
			{
				$h1=$h1+1;
				$arreglo_doce[$h1][2]=$rowMenu['TITULO_MENU'];
				$arreglo_doce[$h1][3]=$rowMenu['URL_MENU'];
				$cont21=$cont21+1;
			}	
		if ($rowMenu['POSICION_MENU']==1 and $rowMenu['RELACION_MENU']=='42')
			{
				$g2=$g2+1;
				$arreglo_trece[$g2][1]=$rowMenu['TITULO_MENU'];
				$cont22=$cont22+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==2 and $rowMenu['RELACION_MENU']=='42' )
			{
				$h2=$h2+1;
				$arreglo_trece[$h2][2]=$rowMenu['TITULO_MENU'];
				$arreglo_trece[$h2][3]=$rowMenu['URL_MENU'];
				$cont23=$cont23+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==1 and $rowMenu['RELACION_MENU']=='43')
			{
				$g3=$g3+1;
				$arreglo_catorce[$g3][1]=$rowMenu['TITULO_MENU'];
				$cont24=$cont24+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==2 and $rowMenu['RELACION_MENU']=='43' )
			{
				$h3=$h3+1;
				$arreglo_catorce[$h3][2]=$rowMenu['TITULO_MENU'];
				$arreglo_catorce[$h3][3]=$rowMenu['URL_MENU'];
				$cont25=$cont25+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==1 and $rowMenu['RELACION_MENU']=='44')
			{
				$g4=$g4+1;
				$arreglo_quince[$g4][1]=$rowMenu['TITULO_MENU'];
				$cont26=$cont26+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==2 and $rowMenu['RELACION_MENU']=='44' )
			{
				$h4=$h4+1;
				$arreglo_quince[$h4][2]=$rowMenu['TITULO_MENU'];
				$arreglo_quince[$h4][3]=$rowMenu['URL_MENU'];
				$cont27=$cont27+1;
			}	
			
			if ($rowMenu['POSICION_MENU']==1 and $rowMenu['RELACION_MENU']=='45')
			{
				$g5=$g5+1;
				$arreglo_diezseis[$g5][1]=$rowMenu['TITULO_MENU'];
				$cont28=$cont28+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==2 and $rowMenu['RELACION_MENU']=='45' )
			{
				$h5=$h5+1;
				$arreglo_diezseis[$h5][2]=$rowMenu['TITULO_MENU'];
				$arreglo_diezseis[$h5][3]=$rowMenu['URL_MENU'];
				$cont29=$cont29+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==1 and $rowMenu['RELACION_MENU']=='46')
			{
				$g6=$g6+1;
				$arreglo_diezsiete[$g6][1]=$rowMenu['TITULO_MENU'];
				$cont30=$cont30+1;
			}	
			
		if ($rowMenu['POSICION_MENU']==2 and $rowMenu['RELACION_MENU']=='46' )
			{
				$h6=$h6+1;
				$arreglo_diezsiete[$h6][2]=$rowMenu['TITULO_MENU'];
				$arreglo_diezsiete[$h6][3]=$rowMenu['URL_MENU'];
				$cont31=$cont31+1;
			}	
		if ($rowMenu['POSICION_MENU']==0 and $rowMenu['RELACION_MENU']=='50')//SEXTO MENU
			{
				$d2=$d2+1;
				$arreglo_diezocho[$d2][0]=$rowMenu['TITULO_MENU'];
				$cont32=$cont32+1;
			}		
		if ($rowMenu['POSICION_MENU']==1 and ($rowMenu['RELACION_MENU']=='51' or $rowMenu['RELACION_MENU']=='52' or $rowMenu['RELACION_MENU']=='53' or $rowMenu['RELACION_MENU']=='54'))
			{
				$d3=$d3+1;
				$arreglo_diezocho[$d3][1]=$rowMenu['TITULO_MENU'];
				$arreglo_diezocho[$d3][2]=$rowMenu['URL_MENU'];
				$cont33=$cont33+1;
			}
			
		if ($rowMenu['POSICION_MENU']==0 and $rowMenu['RELACION_MENU']=='60')//SEPTIMO MENU
			{
				$d3=$d3+1;
				$arreglo_dieznueve[6][0]=$rowMenu['TITULO_MENU'];
				$cont34=$cont34+1;
				echo $arreglo_dieznueve[$d3][0];
			}		
		
		}


?>
<script>
var MENU_ITEMS = [
					<?PHP 
					for ($z=1;$z<=$cont1;$z++)
						{	
						?>
							['<?php echo $arreglo_uno[$z][0] ?>',null,null,
							<?php	
								for ($x=1;$x<=$cont2;$x++)
									{?>
										['<?php echo $arreglo_dos[$x][1] ?>', null,null,
										<?php	
										for ($Y=1;$Y<=$cont3;$Y++)
											{?>
												['<?php echo $arreglo_dos[$Y][2] ?>','<?php echo $arreglo_dos[$Y][3] ?>'],
							  		 		<?php  } ?>
												],
					 		<?php  } 
					 			for ($w=1;$w<=$cont4;$w++)
									{?>
										['<?php echo $arreglo_tres[$w][1] ?>', null,null,
											<?php	
											for ($v=1;$v<=$cont5;$v++)
												{?>
													['<?php echo $arreglo_tres[$v][2] ?>','<?php echo $arreglo_tres[$v][3] ?>'],
							  			 <?php  } ?>
										],
					 		<?php  } 
							 for ($t=1;$t<=$cont6;$t++)
								{?>
									['<?php echo $arreglo_cuatro[$t][1] ?>', null,null,
									<?php	
										for ($s=1;$s<=$cont7;$s++)
											{?>
												['<?php echo $arreglo_cuatro[$s][2] ?>','<?php echo $arreglo_cuatro[$s][3] ?>'],
							  		 <?php  } ?>
									],
					 	 <?php  } 
					  	 for ($r=1;$r<=$cont8;$r++)
								{?>
									['<?php echo $arreglo_cinco[$r][1] ?>', null,null,
										<?php	
											for ($q=1;$q<=$cont9;$q++)
												{?>
													['<?php echo $arreglo_cinco[$q][2] ?>','<?php echo $arreglo_cinco[$q][3] ?>'],
							  			 <?php  } ?>
									],
					 	<?php  } 
					 ?>
					],
				<?php  } 
				for ($p=1;$p<=$cont10;$p++)
					{?>
						['<?php echo $arreglo_seis[$p][0] ?>',null,null,
						<?php 
						for ($o=1;$o<=$cont11;$o++)
							{?>
								['<?php echo $arreglo_siete[$o][1] ?>', null,null,
								<?php	
								for ($n=1;$n<=$cont12;$n++)
									{?>
										['<?php echo $arreglo_siete[$n][2] ?>','<?php echo $arreglo_siete[$n][3] ?>'],
							  <?php  } ?>
								],
					 <?php  } 
					 for ($z1=1;$z1<=$cont13;$z1++)
							{?>
								['<?php echo $arreglo_ocho[$z1][1] ?>', null,null,
								<?php	
								for ($y1=1;$y1<=$cont14;$y1++)
									{?>
										['<?php echo $arreglo_ocho[$y1][2] ?>','<?php echo $arreglo_ocho[$y1][3] ?>'],
							  <?php  } ?>
								],
					 <?php  } 		
						
						
				?>
				],
				<?php  
				}
				for ($w1=1;$w1<=$cont15;$w1++)
					{?>
						['<?php echo $arreglo_nueve[$w1][0] ?>',null,null,
						<?php
						for ($v1=1;$v1<=$cont16;$v1++)
									{?>
										['<?php echo $arreglo_nueve[$v1][1] ?>','<?php echo $arreglo_nueve[$v1][2] ?>'],
										<?php }?>
						],
					<?php  }
					for ($r1=1;$r1<=$cont17;$r1++)
					{?>
						['<?php echo $arreglo_diez[$r1][0] ?>',null,null,
						<?php
						for ($s1=1;$s1<=$cont18;$s1++)
									{?>
										['<?php echo $arreglo_diez[$s1][1] ?>','<?php echo $arreglo_diez[$s1][2] ?>'],
										<?php }?>
						],
					<?php  }
					for ($p1=1;$p1<=$cont19;$p1++)
					{?>
						['<?php echo $arreglo_once[$p1][0] ?>',null,null,
						<?php for ($p2=1;$p2<=$cont20;$p2++)
							{?>
								 ['<?php echo $arreglo_doce[$p2][1] ?>',null,null,
								 <?php for ($p3=1;$p3<=$cont21;$p3++)
									{?>
										['<?php echo $arreglo_doce[$p3][2] ?>','<?php echo $arreglo_doce[$p3][3] ?>'],
									<?php
									}?>
								 ],
							<?php
							}
							for ($p4=1;$p4<=$cont22;$p4++)
							{?>
								 ['<?php echo $arreglo_trece[$p4][1] ?>',null,null,
								 <?php for ($p5=1;$p5<=$cont23;$p5++)
									{?>
										['<?php echo $arreglo_trece[$p5][2] ?>','<?php echo $arreglo_trece[$p5][3] ?>'],
									<?php
									}?>
								 ],
							<?php
							}
							for ($p6=1;$p6<=$cont24;$p6++)
							{?>
								 ['<?php echo $arreglo_catorce[$p6][1] ?>',null,null,
								 <?php for ($p7=1;$p7<=$cont25;$p7++)
									{?>
										['<?php echo $arreglo_catorce[$p7][2] ?>','<?php echo $arreglo_catorce[$p7][3] ?>'],
									<?php
									}?>
								 ],
							<?php
							}
							for ($p8=1;$p8<=$cont26;$p8++)
							{?>
								 ['<?php echo $arreglo_quince[$p8][1] ?>',null,null,
								 <?php for ($p9=1;$p9<=$cont27;$p9++)
									{?>
										['<?php echo $arreglo_quince[$p9][2] ?>','<?php echo $arreglo_quince[$p9][3] ?>'],
									<?php
									}?>
								 ],
							<?php
							}
							for ($p10=1;$p10<=$cont28;$p10++)
							{?>
								 ['<?php echo $arreglo_diezseis[$p10][1] ?>',null,null,
								 <?php for ($p11=1;$p11<=$cont29;$p11++)
									{?>
										['<?php echo $arreglo_diezseis[$p11][2] ?>','<?php echo $arreglo_diezseis[$p11][3] ?>'],
									<?php
									}?>
								 ],
							<?php
							}
							for ($p12=1;$p12<=$cont30;$p12++)
							{?>
								 ['<?php echo $arreglo_diezsiete[$p12][1] ?>',null,null,
								 <?php for ($p13=1;$p13<=$cont31;$p13++)
									{?>
										['<?php echo $arreglo_diezsiete[$p13][2] ?>','<?php echo $arreglo_diezsiete[$p13][3] ?>'],
									<?php
									}?>
								 ],
							<?php
							}
							?>
						],
					<?php
					}
					for ($i1=1;$i1<=$cont32;$i1++)
					{?>
					['<?php echo $arreglo_diezocho[$i1][0] ?>',null,null,
					<?php 
					for ($i2=1;$i2<=$cont33;$i2++)
					{?>
					 ['<?php echo $arreglo_diezocho[$i2][1] ?>','<?php echo $arreglo_diezocho[$i2][2] ?>'],
					<?php
					} 
					?>
					],
					<?php }?>
					
					['<?php echo $arreglo_dieznueve[6][0] ?>',null,null,],
					
				
		];	
		</script>					
	<?php 
	
?>
<script>
var MENU_POS = [{

	'height': 24,
	'width': 130,
	'block_top': 20,
	'block_left': 10,
	
	'top': 0,
	'left': 131,

	'hide_delay': 200,
	'expd_delay': 200,
	'css' : {
		'outer' : ['m0l0oout', 'm0l0oover'],
		'inner' : ['m0l0iout', 'm0l0iover']
	}
},{
	'height': 24,
	'width': 170,
	'block_top': 25,
	'block_left': 0,
	'top': 23,
	'left': 0,
	'css' : {
		'outer' : ['m0l1oout', 'm0l1oover'],
		'inner' : ['m0l1iout', 'm0l1iover']
	}
},{
	'block_top': 5,
	'block_left': 160
}
]

// --------------------------------------------------------------------------------
// that's all folks
</script>
<body>
<div id="layermenu" style="position:absolute; left:3px; top:-8px; width:807px; height:42px; z-index:18; border:0pt black solid;">
  <script language="JavaScript" type="text/javascript">
new menu (MENU_ITEMS, MENU_POS);
</script>
<?php
//echo 'Andrea';
//$algunacosa=1;
?>
</div>
</body>
</html>
