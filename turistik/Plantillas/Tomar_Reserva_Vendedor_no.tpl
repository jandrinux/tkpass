<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Reserva Excursiones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" type="text/JavaScript">
<!--
window.moveTo(0,0); 
window.resizeTo(screen.availWidth,screen.availHeight); 


function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function Aceptar()
{
if(document.form1.hotel.value=='0')
{
alert('Seleccione el Hotel');
document.form1.hotel.focus();
return;
}
if(document.form1.habitacion.value=='')
{
alert('Ingrese el Numero de la Habitacion');
document.form1.habitacion.focus();
return;
}
if(document.form1.lugar.value=='0' && document.form1.otro_lugar.value=='')
{
alert('Seleccione un lugar de recogida o ingrese el lugar de recogida');
document.form1.lugar.focus();
return;
}
if(document.form1.lugar.value!='0' && document.form1.otro_lugar.value!='')
{
alert('Solo seleccione un lugar de recogida o ingrese el lugar de recogida, no ambas');
document.form1.lugar.focus();
return;
}


if(document.form1.telefono.value=='')
{
alert('Ingrese el Telefono de contacto');
document.form1.telefono.focus();
return;
}

if(document.form1.pasajero1.value=='')
{
alert('Ingrese el nombre del pasajero');
document.form1.pasajero1.focus();
return;
}

if(document.form1.tipo.value=='0')
{
alert('Ingrese el tipo de reserva');
document.form1.tipo.focus();
return;
}
document.form1.guardar.value='1';
document.form1.submit();
}

function pres_num()
{
	if(event.keyCode!=44)
	{
		if((event.keyCode<48) || (event.keyCode>57))
		{
				event.returnValue = false;
			}		
	}
}

function SubmitFormulario()
{
document.form1.submit();
}

function Cancelar()
{
window.parent.close();
}

function busca()
	{
 		miPopup = window.open("../PHP/Hotel_Lugar.php?esconde=1","miwin","top=200,left=350,width=670,height=460,scrollbars=yes,titlebar=no,location=no");
 		miPopup.focus();	
	}

function nacionalidad()
	{
 		miPopup = window.open("../PHP/Ingresar_Nacionalidad.php?esconde=1","miwin","top=200,left=350,width=670,height=460,scrollbars=yes,titlebar=no,location=no");
 		miPopup.focus();	
	}

//-->
</script>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.Estilo2 {font-size: 9pt}
-->
</style>
</head>

<body>

<div id="Layer1" style="position:absolute; left:4px; top:102px; width:997px; height:311px; z-index:1">
  <form name="form1" method="post" action="">
    <table width="990" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="89" class="celdalazul_peque">Nombre Excursion </td>
        <td width="208" class="celdalCELEST">{nombre}&nbsp;</td>
        <td width="79" class="celdalazul_peque">Fecha de Excursi&oacute;n </td>
        <td class="celdalCELEST">{fecha}&nbsp;</td>
		<td class="celdalazul_peque">Precio</td>
		<td class="celdalCELEST">{precio}&nbsp;
          <input name="monto" type="hidden" id="monto2" value="{monto}">		
        <td class="celdalazul_peque">C&oacute;d.	Vendedor	
		<td class="celdalCELEST"><input name="codigo_v" type="text" class="seleccionTurist_90" id="codigo_v2" value="{codigo_v}" maxlength="5"></td>
      </tr>
      <tr>
        <td class="celdalazul_peque">Resumen Excursi&oacute;n </td>
        <td colspan="7" class="celdalCELEST"><div align="justify">{resumen}</div></td>
      </tr>
      <tr>
        <td class="celdalazul_peque">Lugar de Recogida</td>
        <td colspan="3" class="celdalCELEST"><select name="lugar" id="select" class="seleccionT3" >
          <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
          <!-- START BLOCK : lugar -->
          <option value="{CODIGO_L}" class="seleccion2" {seleccionar_l}>{DESCRIPCION_L}</option>
          <!-- END BLOCK : lugar -->
        </select></td>
        <td class="celdalazul_peque">Otro lugar </td>
        <td colspan="3" class="celdalCELEST"><input name="otro_lugar" type="text" class="seleccionTurist_200" id="otro_lugar3" value="{otro_lugar}"></td>
      </tr>
      <tr>
        <td class="celdalazul_peque">Hotel </td>
        <td class="celdalCELEST"><select name="hotel" id="hotel" class="seleccionTurist_200" >
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : hotel -->
            <option value="{CODIGO_HL}" class="seleccion2" {SELECCIONAR_HL}>{DESCRIPCION_HL}</option>
            <!-- END BLOCK : hotel -->
          </select>          </td>
        <td class="celdalazul_peque">Habitaci&oacute;n </td>
        <td class="celdalCELEST"><input name="habitacion" type="text" class="seleccionTurist_130" id="habitacion2" value="{habitacion}" onKeyPress="pres_num()"></td>
        <td class="celdalazul_peque">Tel&eacute;fono </td>
        <td colspan="3" class="celdalCELEST"><input name="telefono" type="text" class="seleccionTurist_130" id="telefono2" value="{telefono}" onKeyPress="pres_num()"></td>
      </tr>
      <tr>
        <td class="celdalazul_peque">Pasajero 1 </td>
        <td class="celdalCELEST"><input name="pasajero1" type="text" class="seleccionTurist_200" id="pasajero23" value="{pasajero1}"></td>
        <td class="celdalazul_peque">Pasaporte o RUT </td>
        <td width="128" class="celdalCELEST"><input name="pas1" type="text" class="seleccionTurist_180" id="pas1" value="{pas1}"></td>
        <td width="71" class="celdalazul_peque">Nacionalidad</td>
        <td width="138" class="celdalCELEST"><!--<input name="nac1" type="text" class="seleccionTurist_180" id="nas1" value="{nac1}">-->
		<select name="nac1" id="nac1" class="seleccionTurist_180" >
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : nacionalidad -->
            <option value="{CODIGO_NAC}" class="seleccion2" {SELECCIONAR_NAC}>{PAIS_NAC}</option>
            <!-- END BLOCK : nacionalidad -->
          </select>	    </td>
        <td width="69" class="celdalazul_peque"><span class="celdalazul_peque">Observaci&oacute;n</span></td>
        <td width="208" class="celdalCELEST"><input name="obs1" type="text" class="seleccionTurist_200" id="obs12" value="{obs1}"></td>
      </tr>
      <tr>
        <td class="celdalazul_peque">Pasajero 2 </td>
        <td class="celdalCELEST"><input name="pasajero2" type="text" class="seleccionTurist_200" id="pasajero32" value="{pasajero2}"></td>
        <td class="celdalazul_peque">Pasaporte o RUT</td>
        <td class="celdalCELEST"><input name="pas2" type="text" class="seleccionTurist_180" id="pas2" value="{pas2}"></td>
        <td class="celdalazul_peque">Nacionalidad</td>
        <td class="celdalCELEST"><!--<input name="nac2" type="text" class="seleccionTurist_180" id="nac2" value="{nac2}"></td>-->
		<select name="nac2" id="nac2" class="seleccionTurist_180"  >
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : nacionalidad2 -->
            <option value="{CODIGO_NAC}" class="seleccion2" {SELECCIONAR_NAC2}>{PAIS_NAC}</option>
            <!-- END BLOCK : nacionalidad2 -->
          </select>                
        <td class="celdalazul_peque"><span class="celdalazul_peque">Observaci&oacute;n</span>        
        
        <td class="celdalCELEST"><input name="obs2" type="text" class="seleccionTurist_200" id="obs2" value="{obs2}">        </tr>
      <tr>
        <td class="celdalazul_peque">Pasajero 3 </td>
        <td class="celdalCELEST"><input name="pasajero3" type="text" class="seleccionTurist_200" id="pasajero43" value="{pasajero3}"></td>
        <td class="celdalazul_peque">Pasaporte o RUT</td>
        <td class="celdalCELEST"><input name="pas3" type="text" class="seleccionTurist_180" id="pas3" value="{pas3}"></td>
        <td class="celdalazul_peque">Nacionalidad</td>
        <td class="celdalCELEST"><!--<input name="nac3" type="text" class="seleccionTurist_180" id="nac3" value="{nac3}"></td>-->
          <select name="nac3" id="nac3" class="seleccionTurist_180"  >
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : nacionalidad3 -->
            <option value="{CODIGO_NAC}" class="seleccion2" {SELECCIONAR_NAC3}>{PAIS_NAC}</option>
            <!-- END BLOCK : nacionalidad3 -->
          </select>                
        <td class="celdalazul_peque"><span class="celdalazul_peque">Observaci&oacute;n</span>        
        
        <td class="celdalCELEST"><input name="obs3" type="text" class="seleccionTurist_200" id="obs3" value="{obs3}">        </tr>
      <tr>
        <td class="celdalazul_peque">Pasajero 4</td>
        <td class="celdalCELEST"><input name="pasajero4" type="text" class="seleccionTurist_200" id="pasajero4" value="{pasajero4}"></td>
        <td class="celdalazul_peque">Pasaporte o RUT</td>
        <td class="celdalCELEST"><input name="pas4" type="text" class="seleccionTurist_180" id="pas4" value="{pas4}"></td>
        <td class="celdalazul_peque">Nacionalidad</td>
        <td class="celdalCELEST"><!--<input name="nac4" type="text" class="seleccionTurist_180" id="nac4" value="{nac4}"></td>-->
		<select name="nac4" id="nac4" class="seleccionTurist_180"  >
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : nacionalidad4 -->
            <option value="{CODIGO_NAC}" class="seleccion2" {SELECCIONAR_NAC4}>{PAIS_NAC}</option>
            <!-- END BLOCK : nacionalidad4 -->
          </select>                
        <td class="celdalazul_peque"><span class="celdalazul_peque">Observaci&oacute;n</span>        
        
        <td class="celdalCELEST"><input name="obs4" type="text" class="seleccionTurist_200" id="obs4" value="{obs4}">        </tr>
      <tr>
        <td class="celdalazul_peque">Pasajero 5 </td>
        <td class="celdalCELEST"><input name="pasajero5" type="text" class="seleccionTurist_200" id="pasajero62" value="{pasajero5}"></td>
        <td class="celdalazul_peque">Pasaporte o RUT</td>
        <td class="celdalCELEST"><input name="pas5" type="text" class="seleccionTurist_180" id="pas5" value="{pas5}"></td>
        <td class="celdalazul_peque">Nacionalidad</td>
        <td class="celdalCELEST"><!--<input name="nac5" type="text" class="seleccionTurist_180" id="nac5" value="{nac5}"></td>-->
		<select name="nac5" id="nac5" class="seleccionTurist_180"  >
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : nacionalidad5 -->
            <option value="{CODIGO_NAC}" class="seleccion2" {SELECCIONAR_NAC5}>{PAIS_NAC}</option>
            <!-- END BLOCK : nacionalidad5 -->
          </select>                
        <td class="celdalazul_peque"><span class="celdalazul_peque">Observaci&oacute;n</span>        
        
        <td class="celdalCELEST"><input name="obs5" type="text" class="seleccionTurist_200" id="obs5" value="{obs5}">        </tr>
      <tr>
        <td class="celdalazul_peque">Pasajero 6</td>                                
        <td class="celdalCELEST"><input name="pasajero6" type="text" class="seleccionTurist_200" id="pasajero6" value="{pasajero6}"></td>
        <td class="celdalazul_peque">Pasaporte o RUT</td>
        <td class="celdalCELEST"><input name="pas6" type="text" class="seleccionTurist_180" id="pas6" value="{pas6}"></td>
        <td class="celdalazul_peque">Nacionalidad</td>
        <td class="celdalCELEST"><!--<input name="nac6" type="text" class="seleccionTurist_180" id="nac6" value="{nac6}"></td>-->
		<select name="nac6" id="nac6" class="seleccionTurist_180" >
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : nacionalidad6 -->
            <option value="{CODIGO_NAC}" class="seleccion2" {SELECCIONAR_NAC6}>{PAIS_NAC}</option>
            <!-- END BLOCK : nacionalidad6 -->
          </select>                
        <td class="celdalazul_peque"><span class="celdalazul_peque">Observaci&oacute;n</span>        
        
        <td class="celdalCELEST"><input name="obs6" type="text" class="seleccionTurist_200" id="obs6" value="{obs6}">        </tr>
      <tr>
        <td class="celdalazul_peque">Pasajero 7 </td>
        <td class="celdalCELEST"><input name="pasajero7" type="text" class="seleccionTurist_200" id="pasajero7" value="{pasajero7}"></td>
        <td class="celdalazul_peque">Pasaporte o RUT</td>
        <td class="celdalCELEST"><input name="pas7" type="text" class="seleccionTurist_180" id="pas7" value="{pas7}"></td>
        <td class="celdalazul_peque">Nacionalidad</td>
        <td class="celdalCELEST"><!--<input name="nac7" type="text" class="seleccionTurist_180" id="nac7" value="{nac7}"></td>-->
		<select name="nac7" id="nac7" class="seleccionTurist_180"  >
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : nacionalidad7 -->
            <option value="{CODIGO_NAC}" class="seleccion2" {SELECCIONAR_NAC7}>{PAIS_NAC}</option>
            <!-- END BLOCK : nacionalidad7 -->
          </select>                
        <td class="celdalazul_peque"><span class="celdalazul_peque">Observaci&oacute;n</span>        
        
        <td class="celdalCELEST"><input name="obs62" type="text" class="seleccionTurist_200" id="obs62" value="{obs7}">        </tr>
      <tr>
        <td class="celdalazul_peque">Pasajero 8 </td>
        <td class="celdalCELEST"><input name="pasajero8" type="text" class="seleccionTurist_200" id="pasajero8" value="{pasajero8}"></td>
        <td class="celdalazul_peque">Pasaporte o RUT</td>
        <td class="celdalCELEST"><input name="pas8" type="text" class="seleccionTurist_180" id="pas8" value="{pas8}"></td>
        <td class="celdalazul_peque">Nacionalidad</td>
        <td class="celdalCELEST"><!--<input name="nac8" type="text" class="seleccionTurist_180" id="nac8" value="{nac8}"></td>-->
		<select name="nac8" id="nac8" class="seleccionTurist_180"  >
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : nacionalidad8 -->
            <option value="{CODIGO_NAC}" class="seleccion2" {SELECCIONAR_NAC8}>{PAIS_NAC}</option>
            <!-- END BLOCK : nacionalidad8 -->
          </select>                
        <td class="celdalazul_peque"><span class="celdalazul_peque">Observaci&oacute;n</span>        
        
        <td class="celdalCELEST"><input name="obs8" type="text" class="seleccionTurist_200" id="obs8" value="{obs8}">        </tr>
      <tr>
        <td class="celdalazul_peque">Pasajero 9 </td>
        <td class="celdalCELEST"><input name="pasajero9" type="text" class="seleccionTurist_200" id="pasajero102" value="{pasajero9}"></td>
        <td class="celdalazul_peque">Pasaporte o RUT</td>
        <td class="celdalCELEST"><input name="pas9" type="text" class="seleccionTurist_180" id="pas9" value="{pas9}"></td>
        <td class="celdalazul_peque">Nacionalidad</td>
        <td class="celdalCELEST"><!--<input name="nac9" type="text" class="seleccionTurist_180" id="nac9" value="{nac9}"></td>-->
		<select name="nac9" id="nac9" class="seleccionTurist_180"  >
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : nacionalidad9-->
            <option value="{CODIGO_NAC}" class="seleccion2" {SELECCIONAR_NAC9}>{PAIS_NAC}</option>
            <!-- END BLOCK : nacionalidad9 -->
          </select>                
        <td class="celdalazul_peque"><span class="celdalazul_peque">Observaci&oacute;n</span>        
        
        <td class="celdalCELEST"><input name="obs9" type="text" class="seleccionTurist_200" id="obs9" value="{obs9}">        </tr>
      <tr>
        <td class="celdalazul_peque">Pasajero 10 </td>
        <td class="celdalCELEST"><input name="pasajero10" type="text" class="seleccionTurist_200" id="pasajero10" value="{pasajero9}"></td>
        <td class="celdalazul_peque">Pasaporte o RUT</td>
        <td class="celdalCELEST"><input name="pas10" type="text" class="seleccionTurist_180" id="pas10" value="{pas10}"></td>
        <td class="celdalazul_peque">Nacionalidad</td>
        <td class="celdalCELEST"><!--<input name="nac10" type="text" class="seleccionTurist_180" id="nac10" value="{nac10}"></td>-->
		<select name="nac10" id="nac10" class="seleccionTurist_180" >
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : nacionalidad10 -->
            <option value="{CODIGO_NAC}" class="seleccion2" {SELECCIONAR_NAC10}>{PAIS_NAC}</option>
            <!-- END BLOCK : nacionalidad10 -->
          </select>                
        <td class="celdalazul_peque"><span class="celdalazul_peque">Observaci&oacute;n</span>        
        
        <td class="celdalCELEST"><input name="obs10" type="text" class="seleccionTurist_200" id="obs10" value="{obs10}">        </tr>
      <tr>
        <td class="celdalazul_peque">Tipo de Reserva </td>
        <td colspan="7" class="celdalCELEST">
		<select name="tipo" class="seleccionTurist_180" >
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : tipo -->
            <option value="{CODIGO_TR}" class="seleccion2" {SELECCIONAR_C}>{GLOSA_TR}</option>
            <!-- END BLOCK : tipo -->
          </select>       
		  <input name="guardar" type="hidden" id="guardar2">          
		  <input name="user" type="hidden" id="user"></td>
      </tr>
    </table>
    <div align="center"><br>
      <input name="Submit" type="button" class="BotonTurist_Celest" value="Aceptar" onClick="Aceptar();">
      &nbsp;
      <input name="Submit2" type="submit" class="BotonTurist_Celest" value="Cancelar" onClick="Cancelar()">
    </div>
  </form>
</div>
<div id="Layer2" style="position:absolute; left:5px; top:11px; width:500px; height:89px; z-index:2">
  <table width="493" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="80" rowspan="4"><img src="../Imagenes/IMG_6902.jpg" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="400">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="Estilo1">Toma de Reservas de Excursiones</span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
  </table>
</div>
</body>
</html>
