<?
session_start();
include("menus.php");
$_Util=new Util; 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reservas Reagendadas</title>
<link rel="stylesheet" href="../../css/style_reagenda.css" />
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../../js/ui.core.js"></script>
<script type="text/javascript" src="../../js/ui.datepicker.js"></script>
<link type="text/css" href="../../css/demos.css" rel="stylesheet" />
<script type="text/javascript">
$(function() {
	$("#datepicker").datepicker();
	$("#datepicker1").datepicker();
});
</script>

<style type="text/css">

#Layer2 {	
    position:absolute;
	left:831px;
	top:38px;
	width:132px;
	height:116px;
	z-index:1;
}

</style>
</head>
<body>

<div id="Layer2"><img src="../Imagenes/Logo_Turistik.png" width="107" height="118" /></div>
  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr height="50"><td><br></td></tr>
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Tipo_Grupo_80x80.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td style="font-family:Arial, Helvetica, Sans-Serif; font-size: 16px; font-weight: bold;">Lista de reservas reagendadas</style></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>  

  </table>
  
	<div id="tablewrapper">
		<div id="tableheader">
        	<table >    
                <tr>    
                    <td class="celdalCELEST"><select id="columns" onchange="sorter.search('query')"></select></td>
                    <td class="celdalCELEST"><input class="seleccionTurist_180" type="text" id="query" onkeyup="sorter.search('query')" /></td>
                </tr>
                <form name="form" method="get" action="Reservas_reagendadas.php">
                <tr>
                    <td class="celdalCELEST">Fecha Desde:</td>
                    <td class="celdalCELEST"><input class="seleccionTurist_180" type="text" readonly="" name="fecha_desde" id="datepicker" value="<?=date('d/m/Y')?>" /></td>
                </tr>
                <tr>
                    <td class="celdalCELEST" >Fecha Hasta:</td>
                    <td class="celdalCELEST"><input class="seleccionTurist_180" type="text" readonly="" name="fecha_hasta" id="datepicker1" value="<?=date('d/m/Y')?>" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                    <input type="submit" value="Consultar" class="BotonTurist_Celest" />
                    </td>
                </tr>
                </form>
            </table>
                
                
                
            
            
            <span class="details">
				<div>Registros <span id="startrecord"></span>-<span id="endrecord"></span> de <span id="totalrecords"></span></div>
        		<div><a href="javascript:sorter.reset()">orden por defecto</a></div>
        	</span>
        </div>
        <table style="margin-top:50px;" cellpadding="0" cellspacing="0" border="0" id="table" class="tinytable">
            <thead>
                <tr>
                    <th class="nosort"><h3>Nombre</h3></th>
                    <th><h3>Tipo pax</h3></th>
                    <th><h3>Reserva</h3></th>
                    <th><h3>Lugar salida</h3></th>
                    <th><h3>Fecha Reserva</h3></th>
                    <th><h3>Excursion</h3></th>
                    <th><h3>Agente Reserva</h3></th>
                    <th><h3>Agente Reag.</h3></th>
                    <th><h3>Sucursal Venta</h3></th>
                    <th><h3>Fecha reagendado</h3></th>
                    <th><h3>Estado</h3></th>
                </tr>
            </thead>
            <tbody>
            
            <?
            
            if ($_GET['fecha_desde'] != '' && $_GET['fecha_hasta'] != '')
            {
                $fecha_desde = $_Util->DesformatearFecha($_GET['fecha_desde']);
                $fecha_hasta = $_Util->DesformatearFecha($_GET['fecha_hasta']);
                    
               
                $objListaReagendados = new ClaseConexion;
                $objListaReagendados->Conectar();
                $queryLista=$objListaReagendados->EjecutaSP("Consulta_Lista_Reservas_Reagendadas"," '".$fecha_desde."', '".$fecha_hasta."' ");
                while ($rowLista = mysql_fetch_assoc($queryLista)){
                    
                    if ($rowLista['FOP'] == 'p'){
                        $fop = "PP";
                    }
                    if ($rowLista['FOP'] == 'C'){
                        $fop = "CC";
                    }
                    
                     echo "<tr>";
                     
                        echo "<td> ".$rowLista['PASAJERO_RE']."</td>";
                        echo "<td> ".$rowLista['TIPO_PASAJERO']."</td>";
                        echo "<td> ".$rowLista['VOUCHER_RE']."</td>";
                        echo "<td> ".$rowLista['DESCRIPCION_HL']."</td>";
                        echo "<td> ".$rowLista['FECHA_REALIZACION_RE']." ".$rowLista['HORA_VENTA']."</td>";
                        echo "<td> ".$rowLista['NOMBRE_EO']."</td>";
                        echo "<td> ".$rowLista['LOGIN_USUA']."</td>";
                        echo "<td> ".$rowLista['LOGIN_USUA_CIERRE']."</td>";
                        echo "<td> ".$rowLista['SUCURSAL']."</td>";
                        echo "<td> ".$rowLista['FECHA']."</td>";
                        echo "<td> ".$fop."</td>";
                     echo "</tr>";
                    
                     
                }
                mysql_free_result($queryLista); 
                mysql_close();
            
            }
            ?>
  
            </tbody>
        </table>
        <div id="tablefooter">
          <div id="tablenav">
            	<div>
                    <img src="../Imagenes/images/first.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                    <img src="../Imagenes/images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="../Imagenes/images/next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="../Imagenes/images/last.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
                </div>
                <div>
                	<select id="pagedropdown"></select>
				</div>
                <div>
                	<a href="javascript:sorter.showall()">ver todo</a>
                </div>
            </div>
			<div id="tablelocation">
            	<div>
                    <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                        <option value="10" >10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Registros por p&aacute;gina</span>
                </div>
                <div class="page">P&aacute;gina <span id="currentpage"></span> de <span id="totalpages"></span></div>
            </div>
        </div>
    </div>
	<script type="text/javascript" src="../../js/script.js"></script>
	<script type="text/javascript">
	var sorter = new TINY.table.sorter('sorter','table',{
		headclass:'head',
		ascclass:'asc',
		descclass:'desc',
		evenclass:'evenrow',
		oddclass:'oddrow',
		evenselclass:'evenselected',
		oddselclass:'oddselected',
		paginate:true,
		size:10,
		colddid:'columns',
		currentid:'currentpage',
		totalid:'totalpages',
		startingrecid:'startrecord',
		endingrecid:'endrecord',
		totalrecid:'totalrecords',
		hoverid:'selectedrow',
		pageddid:'pagedropdown',
		navid:'tablenav',
		sortcolumn:1,
		sortdir:1,
		sum:[8],
		avg:[6,7,8,9],
		columns:[{index:7, format:'%', decimals:1},{index:8, format:'$', decimals:0}],
		init:true
	});
  </script>
</body>
</html>