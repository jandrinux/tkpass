<?
/**
 * class DBNucleo
 */
class DBNucleo extends Conexion
{
	// funciones comodines...
        //*****************************************************//
        //funciones extras
        //*****************************************************//
	function DBNucleo() { $this->conecta();}

        function TraerLosDatos($tabla,$campo1,$campo2, $sqlextra="",$lenguaje="")
        {
		$dic = new DBDiccionario();
                $salida = array();
                $sql = "SELECT $campo1,$campo2 from $tabla";
		if($sqlextra)
			$sql.= $sqlextra;

		$aCampo = split(" ",$campo2);
		if (count($aCampo)>1) $campo2 = $aCampo[1];
		echo "<!--TraerLosDatos: $sql -->";
                $res = $this->query($sql);
                if($res)
                {
                        while($row = $this->getRows($res))
                        {
                                $tmp["id"]     = $row["$campo1"];
                                $tmp["valor"]  = $row["$campo1"];
                                $tmp["nombre"] = ($lenguaje)?$dic->traducir($lenguaje,$row["$campo2"]):$row["$campo2"];
                                $tmp["txt"]    = ($lenguaje)?$dic->traducir($lenguaje,$row["$campo2"]):$row["$campo2"];
                                $salida[] = $tmp;
                        }
                }
                return $salida;
        }
        function TraerLosDatos2($tabla,$campo1,$campo2,$campo3, $sqlextra="",$lenguaje="")
        {
		$dic = new DBDiccionario();
                $salida = array();
                $sql = "SELECT $campo1,$campo2,$campo3 from $tabla";
		if($sqlextra)
			$sql.= $sqlextra;

		$aCampo = split(" ",$campo2);
		if (count($aCampo)>1) $campo2 = $aCampo[1];
		echo "<!--TraerLosDatos: $sql -->";
                $res = $this->query($sql);
                if($res)
                {
                        while($row = $this->getRows($res))
                        {
                                $tmp["id"]     = $row["$campo1"];
                                $tmp["valor"]  = $row["$campo1"];
				$precio = $row["$campo3"];
                                $tmp["nombre"] = $row["$campo2"]."  ( $".number_format($precio,0,",",".").".-)";
                                $tmp["txt"]    = $row["$campo2"]."  ( $".number_format($precio,0,",",".").".-)";
                                $salida[] = $tmp;
                        }
                }
                return $salida;
        }
	function TraerLosDatosDistinct($tabla,$campo1,$campo2, $sqlextra="",$lenguaje="")
        {
		$dic = new DBDiccionario();
                $salida = array();
                $sql = "SELECT distinct($campo1),$campo2 from $tabla";
		if($sqlextra)
			$sql.= $sqlextra;

		$aCampo = split(" ",$campo2);
		if (count($aCampo)>1) $campo2 = $aCampo[1];
		//echo "<!--TraerLosDatos: $sql -->";
                $res = $this->query($sql);
                if($res)
                {
                        while($row = $this->getRows($res))
                        {
                           $tmp["id"]     = $row["$campo1"];
							
                                $tmp["valor"]  = $row["$campo1"];
                                $tmp["nombre"] = ($lenguaje)?$dic->traducir($lenguaje,$row["$campo2"]):$row["$campo2"];
                                $tmp["txt"]    = ($lenguaje)?$dic->traducir($lenguaje,$row["$campo2"]):$row["$campo2"];
                                $salida[] = $tmp;
                        }
                }
                return $salida;
        }
	function ElDato($id,$tabla,$campo1,$campo2,$sqlext=""){
		$tmp = array();
		if ($id)
			if (is_string($id))
				$sql = "SELECT $campo1,$campo2 from $tabla WHERE $campo1='$id'";
              			else
				$sql = "SELECT $campo1,$campo2 from $tabla WHERE $campo1=$id";
              		else if ($sqlext)
			$sql = "SELECT $campo1,$campo2 from $tabla WHERE id>1 ";
              		if ($sqlext) $sql.=" and $sqlext";
		$sql.= " limit 1";
//echo $sql;
		//echo "<!--ElDato:$sql-->";
                $res = $this->query($sql);
                if($res)
                {
                        if($row = mysql_fetch_array($res))
                        {
                                $tmp["id"] = $row["$campo1"];
                                $tmp["valor"] = $row["$campo1"];
                                $tmp["nombre"] = $row[1];
                                $tmp["txt"] = $row[1];
                        }
                }

                return $tmp["txt"];
	}
		function ElDatoSolo($id,$tabla,$campo1,$campo2,$sqlext=""){
		$tmp = array();
		if (is_string($id))
			$sql = "SELECT $campo1 from $tabla WHERE $campo2='$id'";
		
		if ($sqlext) $sql.=" and $sqlext";
		echo "<!--ElDatoSolo:$sql-->";
                $res = $this->query($sql);
                if($res)
                {
                        if($row = mysql_fetch_array($res))
                        {
                                $tmp["id"] = $row["$campo1"];
                                $tmp["valor"] = $row["$campo1"];
                                $tmp["nombre"] = $row[1];
                                $tmp["txt"] = $row[1];
                        }
                }

                return $tmp["txt"];
	}

	function ElDatoInner($id,$tabla,$campo1,$campo2,$sqlext=""){
		$tmp = array();
		if (is_string($id))
			$sql = "SELECT t2.descripcion,t2.descripcion from $tabla t inner join MotivoTicket t2 on  t.valor=t2.id WHERE id_ticket='$id'";
                else
			$sql = "SELECT t2.descripcion,t2.descripcion from $tabla t inner join MotivoTicket t2 on  t.valor=t2.id WHERE id_ticket='$id'";
		
		if ($sqlext) $sql.=" and $sqlext";
		echo "<!--ElDatoinner:$sql-->";
                $res = $this->query($sql);
                if($res)
                {
                        if($row = mysql_fetch_array($res))
                        {
                                $tmp["id"] = $row["$campo1"];
                                $tmp["valor"] = $row["$campo1"];
                                $tmp["nombre"] = $row[1];
                                $tmp["txt"] = $row[1];
                        }
                }

                return $tmp["txt"];
	}

	function CallFuncSQL($nombre_sp,$parametros){
		$tmp = array();
                $sql = "select $nombre_sp($parametros)";
		//echo "<p>SQL:$sql</p>";
                $res = $this->query($sql);
                if($res)
                {
                        if($row = mysql_fetch_array($res))
                        {
                                $valor = $row[0];
                        }
                }

                return $valor;
	}
       
        // funcion que envia mail
        // la variable $html indica si el mensaje es html o no... (obvio!!)
        function XSendMail($to,$nombrefrom,$from,$subject,$message,$html="")
        {
                $headers= "MIME-Version: 1.0\r\n";
                if($html=="1")
                {
                        $headers.= "Content-type:text/html;charset=iso-8859-1 \r\n";
                }
                $headers.= "From: $nombrefrom<$from>\r\n";
                $headers.= "Reply-To: $from \r\n";
                $headers.= "X-Priority: 1\r\n";
                $headers.= "X-MSMail-Priority: High\r\n";
                $headers.= "X-Mailer: localhost\r\n";

                if(!mail($to, $subject, $message, $headers))
                {
                       $msg = "El Mail No se pudo enviar";
                       return -1;
                }
                $msg = "Enviando Mail a $to\n";
                return 1;
       }
	function SelectArray($lenguaje="",$tmp, $nombre, $msg="", $default="", $valormsg="", $js="",$disabled="")
	{
		$dic = new DBDiccionario();
		if($js)
			echo "<select id='$nombre' name='$nombre' onchange=\"$js\" class='contenidonegro' $disabled>\n";
		else
			echo "<select id='$nombre' name='$nombre' class='contenidonegro' $disabled>\n";

		if($msg)
		{
			if($valormsg=="")
				echo "<option value='0'>$msg</option>\n";
			else
				echo "<option value='$valormsg'>$msg</option>\n";
		}
		for($i=0;$i<count($tmp);$i++)
		{
			$datos = $tmp[$i];
			$valor = $datos["valor"];
			$txt = $dic->traducir($lenguaje,$datos["txt"]);
			$txt = substr($txt,0,50);
			if($valor == $default)
				echo "<option value='$valor' selected>$txt</option>\n";
			else
				echo "<option value='$valor'>$txt</option>\n";
		}
		echo "</select>\n";
	}
	function strSelectArray($lenguaje="",$tmp, $nombre, $msg="", $default="", $valormsg="", $js="",$disabled="")
	{
		$dic = new DBDiccionario();
		//echo "<!--Nombre:$nombre,MSG:$msg, Default:$default ValorMsg:$valormsg JS:$js -->";
		if($js)
			$select = "<select id='$nombre' name='$nombre' onchange=\"$js\" class='contenidonegro' $disabled>\n";
		else
			$select = "<select id='$nombre' name='$nombre' class='contenidonegro' $disabled>\n";

		if($msg)
		{
			if($valormsg=="")
				$select.= "<option value='0'>$msg</option>\n";
			else
				$select.= "<option value='$valormsg'>$msg</option>\n";
		}
		for($i=0;$i<count($tmp);$i++)
		{
			$datos = $tmp[$i];
			$valor = $datos["valor"];
			$txt = $dic->traducir($lenguaje,$datos["txt"]);
			if($valor == $default)
				$select.= "<option value='$valor' selected>$txt</option>\n";
			else
				$select.= "<option value='$valor'>$txt</option>\n";
		}
		$select.= "</select>\n";
		return $select;
	}

	//funcion que retorna un numero random
	//basado en la funcion microtime
	//tiene un largo de 17 caracteres
	function ticks()
	{
        	$tiempo = microtime();
	        $tiempo = str_replace("0.","",$tiempo);
		$aux = split(" ",$tiempo);
		$stick = $aux[1].$aux[0];
		return $stick;
	}

	// Busca un id de un record por el campo insert_ticks
	// Si no encuentra id retorna -1
	function buscaIdPorTicks($ticks,$tabla)
	{
            $sql = "SELECT id FROM $tabla WHERE insertticks='$ticks'";
            $result=mysql_query($sql);
            if ($view_campo = mysql_fetch_array($result))
            {
                    return $view_campo[id];
            }
            else
            {
                    return -1;
            }
	}
	
	// Busca un campo de un record por el campo Estampilla
	// Si no encuentra  retorna -1
	function buscaPorEstampilla($ticks,$campo,$tabla)
	{
            $sql = "SELECT $campo FROM $tabla WHERE Estampilla='$ticks'";
	    echo "<!-- SQL: $sql -->";
	    $result=$this->query($sql);
            if ($view_campo = $this->getRows($result))
            {
		    $salida = $view_campo[0];
                    return $salida;
            }
            else
            {
                    return -1;
            }
	}

        function flipcolor($c){
                $color  = ($c == "#f8f8f8")?"#f0f0f0":"#f8f8f8";
                return $color;
        }
	function rowEffect($i,$color,$click="",$pointer=""){
		$over  = " onmouseover=\"setPointer(this, 0, 'over', '$color', '#CCFFCC', '#FFCC99');\"";
		$out   = " onmouseout=\"setPointer(this, 0, 'out', '$color', '#CCFFCC', '#FFCC99');\"";
		$row = "$over $out";
		$extra ="";
		if ($click)   $extra.= "$click;";
		if ($pointer) $extra.= "setPointer(this, $i, 'click', '$color', '#CCFFCC', '#FFCC99');";
		
		if ($click || $pointer)
			$row.= " onClick=\"$extra\"";
		return $row;
	}
	function deleteEffect($id_del,$id,$url="",$js=""){
		if (!$js) $js="eliminar";
		
		$html = "<a href=\"javascript: $js('$id_del','$url')\" onmouseover=\"x$id.src='/images/cross_on.gif'\"      onmouseout=\"x$id.src='/images/cross_off.gif'\"><img src=\"/images/cross_off.gif\" name=\"x$id\" border=\"0\"></a>";
		return $html;

		
	}
	function deleteEffect2($id_del,$id,$url="",$section="",$js="",$lenguaje=""){
		$dic = new DBDiccionario();
		if (!$js) $js="eliminar";
		$mensaje = $dic->traducir($lenguaje,"Desea Eliminar esta Orden de Trabajo");
		$html = "<a href=\"javascript: 
		if(confirm('$mensaje?'))
		{
			makerequest('/includes/lasordenes.php?id_del=$id_del&$url','$section'); 
		}\" onmouseover=\"x$id.src='/images/cross_on.gif'\" onmouseout=\"x$id.src='/images/cross_off.gif'\"><img src=\"/images/cross_off.gif\" name=\"x$id\" border=\"0\"></a>";

		return $html;

		
	}
	function Paginador($url,$seccion,$lapag,$nropag,$primero,$limite,$inicio,$pag_show="",$prefix=""){
		if (!$inicio) $inicio=0;
		$primero=($primero==0)?1:$primero;
		if ($nropag > $pag_show) $xnext = $pag_show; else $xnext = $nropag;

		if ($primero-$limite>=0){
			$l_lapag = $lapag-1;
			$lprimero =$primero-$limite;
			$linicio = (($lapag-1) == $inicio)?($inicio-$pag_show):$inicio;
		} else {
			$lprimero = $limite*($nropag-1);
			$linicio = $nropag -$xnext;
			$l_lapag = $nropag;
		}
		$url_prev ="$url&primero$prefix=$lprimero&lapag$prefix=$l_lapag&limite$prefix=$limite&inicio$prefix=$linicio&pag_show$prefix=$pag_show";
		$url_primero="$url&primero$prefix=0&lapag$prefix=1&limite$prefix=$limite&inicio$prefix=0&pag_show$prefix=$pag_show";
		if($nropag>1){
?>
<a href="javascript:makerequest('<?=$url_primero?>','<?=$seccion?>');" class="titulonegro"><img src="/images/ar_prev.gif" border=0 ><img src="/images/ar_prev.gif" border=0></a>
<a href="javascript:makerequest('<?=$url_prev?>','<?=$seccion?>');" class="titulonegro"><img src="/images/leftarrow.gif" border=0 ></a>
<?
		for($i=$inicio; $i < ($inicio+$xnext);$i++){
			$p = $i+1;
			$pag = $limite * $i;
			if ($p==$lapag) 
				$pag_mark="<font color='red'>[ $p ]</font>"; 
			else 
				$pag_mark="&nbsp;$p&nbsp;";/*
?>
 <a href="<?="$url&primero$prefix=$pag&limite$prefix=$limite&lapag$prefix=$p&inicio$prefix=$inicio"?>" class='contenidonegro'><?="$pag_mark"?></a>
<?*/
?>
 <a href="javascript:makerequest('<?="$url&primero$prefix=$pag&limite$prefix=$limite&lapag$prefix=$p&inicio$prefix=$inicio"?>','<?=$seccion?>');" class='contenidonegro'><?="$pag_mark"?></a>
<?
		} //Fin for para escribir paginas
		$sig = ($limite*$i)+1;
		$pag_sig = $i+1;
		if ($pag_sig > $nropag ){ $pag_sig = 1; $sig=0; }
		$rinicio=$inicio;
		$inicio=$pag_sig-1;
		$xprimero = $limite*($nropag-1);
		$xinicio = $nropag -$xnext;
		$rprimero = $primero+$limite-1;
		$psig=$lapag+1;
                if ($psig > $nropag || $lapag>$nropag){ $pag_sig=1;$psig=1;$primero=0;$inicio=0;$rprimero=0;$rinicio=0;}
                if (($lapag % $pag_show)==0)
                        $url_next = "$url&primero$prefix=$sig&lapag$prefix=$pag_sig&limite$prefix=$limite&inicio$prefix=$inicio&pag_show$prefix=$pag_show";
                else
                        $url_next = "$url&primero$prefix=$rprimero&lapag$prefix=$psig&limite$prefix=$limite&inicio$prefix=$rinicio&pag_show$prefix=$pag_show";

                $url_siguiente="$url&primero$prefix=$sig&lapag$prefix=$pag_sig&limite$prefix=$limite&inicio$prefix=$inicio&pag_show$prefix=$pag_show";
                $url_ultimo="$url&primero$prefix=$xprimero&lapag$prefix=$nropag&limite$prefix=$limite&inicio$prefix=$xinicio&pag_show$prefix=$pag_show";

		if ($nropag > 10) $inicio = $pag_sig;
?>
<a href="javascript:makerequest('<?=$url_next?>','<?=$seccion?>');" class="titulonegro"><img src="/images/rightarrow.gif" border=0></a>&nbsp;
<a href="javascript:makerequest('<?=$url_ultimo?>','<?=$seccion?>');" class="titulonegro"><img src="/images/ar_next.gif" border=0><img src="/images/ar_next.gif" border=0></a>&nbsp;
<?
		}


   	}

	function round_up($value){
		list($val,$dummy) = explode(".",$value);
		return $dummy?$val+1:$val;
	}

	function valor($valor){
		return ($valor)?$valor:0;
	}

	function lista2str($array_obj){
		$str_lista="";$i=0;
		for($i=0;$i<count($array_obj)-1;$i++){
			$tmp = $array_obj[$i];
			$descripcion = $tmp['txt'];
			$str_lista.= "$descripcion,";
			echo "<!--$descripcion-->";
		}
		$tmp = $array_obj[$i];
		$descripcion = $tmp['txt'];
		$str_lista.= $descripcion;
		echo "<!--$descripcion-->";
		return $str_lista;
	}

	function showCampo($tmp,$valor="",$jsCode="",$noeditar="",$lenguaje=""){
		$Campo = new DBCampo();

		$id_campo	= $tmp->getId();
		$tipo_campo	= $tmp->getIdTipoCampo();
		$id_entidad	= $tmp->getIdEntidad();
		$desc_campo	= $tmp->getDescripcion();
		$opciones	= $tmp->getOpciones();
		$tipo_dato	= $tmp->getIdTipoDato();
		$valor		= ($valor)?$valor:$tmp->getValor();
		$largo		= $tmp->getLargoCampo();
		$valor_defecto = $tmp->getValorDefecto();
		$nombre_campo  = $tmp->getNombreCampo();

		$xtipo		= $Campo->getTipos($tipo_campo);
		$largo		= $xtipo[0]->getLargoCampo();	
		$opciones 	= $xtipo[0]->getOpciones();


		$jsCodigo = ($jsCode)?"onBlur='$jsCode'":"";
		$nombre_campo = "campo_entidad$id_campo";

		switch($tipo_dato){
		case 1://Numerico
			$campo = "<input type='text' name='$nombre_campo' id='$nombre_campo' size='$largo' value='$valor' $jsCodigo>";
			break;
		case 2://Texto
			if ($largo>60) {
				$filas = round($largo / 60)+1;
				$campo = "<textarea name=\"$nombre_campo\" id=\"$nombre_campo\"   cols=60 rows=$filas>$valor</textarea>";
			} else {
				$campo = "<input type=\"text\" name=\"$nombre_campo\" id=\"$nombre_campo\"   size='$largo' value=\"$valor\" >";//  onChange=\"$jsCode;\">";
			}
 			break;
		case 3://Fecha
			$campo = "<input type='text' name='$nombre_campo' id='$nombre_campo' value=\"$valor\" $lectura class=\"contenidonegro\">&nbsp;
		<a href=\"javascript:cal$id_campo.popup();\"  class=\"contenidonegro\"><img src=\"/images/cal.gif\" width=\"18\" height=\"16\" border=\"0\" alt=\"Haga  click para seleccionar una fecha\"></a><script language=\"JavaScript\">var cal$id_campo = new calendar1(document.forms[0].elements['$nombre_campo']);cal$id_campo.year_scroll = false;cal$id_campo.time_comp = false;</script>";
			break;
		case 4://Seleccion
			$xopciones = explode(",",$opciones);
			$lasopciones = array();
			for($j=0;$j<count($xopciones);$j++){
				$tmp = $xopciones[$j];$tmp2=array();
				$tmp2[valor] = $xopciones[$j];
				$tmp2[txt]   = $xopciones[$j];
				$lasopciones[] = $tmp2;
			}
			$jsCodigo = ($jdCode)?"MiSubmit('$jsCode')":""; echo "<!-- jsCodigo: $jsCodigo -->";
			$campo = $this->strSelectArray($lenguaje,$lasopciones,"valor_campo","----",$valor,"","$jsCodigo");
			break;
		case 5:// From Table
			// 1.- Campo Indice
			// 2.- Campo descripcion
			// 3.- Where 

			$param = explode(";",$opciones);// Campo Indice; Campo Descripcion; Where
			$jsCodigo = ($jsCode)?"$jsCode":""; ;
			$sql_extra = ($param[2])?$param[2]:"";
                        if ($param[2]){ //Si existe where descomponer
                                if(strpos($param[2],"[")>0){
                                        $pi = strpos($param[2],"[");$pf = strpos($param[2],"]");
                                        $tipo = substr($param[2],$pi+1,$pf-$pi-1);//Extrae el Tipo de Parametro que se quiere comparar
					$elwhere = $param[2];
					if ($tipo=="EMISOR"){
						$elwhere = str_replace("[$tipo]","( $clave )",$elwhere);
					} else if ($tipo=="IDORDENPADRE"){
						$elwhere = str_replace("[$tipo]","$idpadre",$elwhere);
					} else {
						$a = $Campo->get("",$idorden,$tipo);//Busca el primer parametro con la orden indicada con ese tipo
						if ($a[0]){
							$valor_token = $a[0]->getValor();
							$elwhere= str_replace("[$tipo]",$valor_token,$elwhere);
						} else $elwhere="";
					}
					$sql_extra=" ".$elwhere;
                                } else {
                                $sql_extra = $param[2];}
                        }
			//echo "<p>TABLA:$valor_defecto SQL EXTRA: $sql_extra Valor:$valor</p>";
			$xopciones = $this->TraerLosDatos($valor_defecto,$param[0],$param[1],$sql_extra);
			$campo     = $this->strSelectArray($lenguaje,$xopciones,"$nombre_campo","----",$valor);//,"","$jsCodigo");
			$valor	   = $this->ElDato($valor,$valor_defecto,$param[0],$param[1]);
			//echo "<p>Valor : $valor Tabla:$valor_defecto, P1:$param[0] P2:$param[1]</p>";
			break;
		case 6://Rut Chile
			$campo = "<input type='text' id='$nombre_campo' name='$nombre_campo' size='$largo' value='$valor' onChange=\"if (checkRutField(document.forms[0].$nombre_campo.value,document.forms[0].$nombre_campo)) $jsCode; else this.focus();\">";
			break;
		}
		$tmp=array();
		$tmp['campo'] = $campo;
		$tmp['valor'] = strtoupper($valor);
		return $tmp;
	}

	function showCampoActivo($tipoCar="",$elactivo="",$elvalor="",$jsCode="",$lenguaje="",$noeditar=""){
		if ($elactivo){
			$valor = $elactivo->getValor();
			$idcampo = $elactivo->getId();
		}
		$valor_car = $valor;
		$jsCodigo = ($jsCode)?"onBlur=\"$jsCode\"":"";
		$tipo_dato = $tipoCar->getTipoDato();
		$opciones = $tipoCar->getOpciones();
		$valor_defecto = $tipoCar->getValorDefecto();
		$largo = $tipoCar->getLargo();
		$nombre_campo = "campo_activo$idcampo"; //$tipoCar->getNombreCampo();
		$valor_parametro = $valor;
		switch($tipo_dato){
		case 1://Numerico
			$largo2=$largo + 2;
			$campo = "<input type=\"text\" name=\"$nombre_campo\" id='$nombre_campo' value='$valor_parametro' size='$largo2' $jsCodigo>";
			break;
		case 2://Texto
			if ($largo>60) {
				$filas = round($largo / 60)+1;
				$campo = "<textarea name=\"$nombre_campo\" id=\"$nombre_campo\" cols=60 rows=$filas $jsCodigo>$valor_parametro</textarea>";
			} else {
				$campo = "<input type=\"text\" name=\"$nombre_campo\" id=\"$nombre_campo\" size='$largo' value=\"$valor_parametro\"  $jsCodigo>";
			}
 			break;
		case 3://Fecha
			//$losmeses = $this->TraerLosDatos("Meses","id","descripcion");
			list($aano,$ames,$adia)=split("-",$valor_parametro);
			$valor1 = $valor_parametro;
			setlocale(LC_ALL,'es_ES.UTF-8@iso-8859-1');
			//setlocale(LC_ALL,"iso-8859-1");
			$f = strftime("%A %d, %B %Y",strtotime($valor)); //%A %d, %B %Y
			$eldia = strftime("%u",strtotime($valor)); //%A %d, %B %Y
			$elmes = strftime("%m",strtotime($valor)); //%A %d, %B %Y
			$f = $this->getDia($eldia)." $adia ,  ".$this->getMes($elmes)." $aano";
			$valor_car = $f;
			
			$campo = "(YYYY-MM-DD):<input type='text' name=\"$nombre_campo\" id=\"$nombre_campo\" value=\"$valor1\" onchange=\"alert('Me estan asingado un valor');\" size='9' readonly>";
			$campo.= "<a href=\"javascript:cal$id_param.popup();\"  class=\"contenidonegro\"><img src=\"/images/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Haga  click para seleccionar una fecha\"></a>
<script language=\"JavaScript\">
	var cal$id_param = new calendar1(document.forms[0].elements['$nombre_campo']);
	cal$id_param.year_scroll = true;
	cal$id_param.time_comp = false;
</script>";
			break;
		case 4://Seleccion
			echo "<!--Opciones:$opciones-->";
			$xopciones = explode(",",$opciones);
			$lasopciones = array();
			for($j=0;$j<count($xopciones);$j++){
				$tmp = $xopciones[$j];$tmp2=array();
				$tmp2[valor] = $xopciones[$j];
				$tmp2[txt]   = $xopciones[$j];
				$lasopciones[] = $tmp2;
			}
			$jsCodigo = ($jsCode)?$jsCode:""; echo "<!-- jsCodigo: $jsCodigo -->";
			
			$campo = $this->strSelectArray($lenguaje,$lasopciones,"$nombre_campo","----",$valor_parametro,"","$jsCodigo");
			$valor_paramtro = $valor;
			break;
		case 5:// From Table
			$campo = new DBParametro();

			$param = explode(";",$opciones);// Campo Indice; Campo Descripcion; Where
			//echo "<p>P1:".$param[0]."P2:".$param[1]."P3:".$param[2]."</p>";
			$jsCodigo = ($jsCode)?"$jsCode":""; ;
			$sql_extra = ($param[2])?$param[2]:"";
			echo "<!--Clave: $clave -->";
                        if ($param[2]){ //Si existe where descomponer
                                if(strpos($param[2],"[")>0){
                                        $pi = strpos($param[2],"[");$pf = strpos($param[2],"]");
                                        $tipo = substr($param[2],$pi+1,$pf-$pi-1);//Extrae el Tipo de Parametro que se quiere comparar
					$elwhere = $param[2];
					if ($tipo=="EMISOR"){
						$elwhere = str_replace("[$tipo]","( $clave )",$elwhere);
					} else {
						$a = $campo->get("",$idorden,$tipo);//Busca el primer parametro con la orden indicada con ese tipo
						if ($a[0]){
							$valor_token = $a[0]->getValor();
							$elwhere= str_replace("[$tipo]",$valor_token,$elwhere);
						} else $elwhere="";
					}
					$sql_extra=$elwhere;
                                } else {
                                $sql_extra = $param[2];}
                        }
			echo "<!--TABLA:".$valor_defecto." (".$param[0].") (".$param[1].") SQL EXTRA: $sql_extra -->";
			$xopciones = $this->TraerLosDatos($valor_defecto,$param[0],$param[1],$sql_extra);
			$campo     = $this->strSelectArray($lenguaje,$xopciones,"$nombre_campo","----",$valor,"","$jsCodigo");
			$valor_parametro = $this->ElDato($valor,$valor_defecto,$param[0],$param[1]);
			break;
		case 6://Rut Chile
			$campo = "<input type='text' id='$nombre_campo' name='$nombre_campo' size='$largo' value='$valor_parametro' onChange=\"ret=checkRutField(this.value,this);if(!ret) this.focus();\">";
			break;
		case 7://Memo
			$campo="<textarea name='$nombre_campo' cols=60 rows=5>$valor_parametro</textarea>";
			break;
		case 8://Cod. Comercio
			$campo = new DBParametro();

			$param = explode(";",$opciones);// Campo Indice; Campo Descripcion; Where
			$jsCodigo = ($jsCode)?"$jsCode":""; ;
			$sql_extra = ($param[2])?$param[2]:"";
                        if ($param[2]){
                                if(strpos($param[2],"[")>0){
                                        $pi = strpos($param[2],"[");$pf = strpos($param[2],"]");
                                        $tipo = substr($param[2],$pi+1,$pf-$pi-1);
					if ($tipo=="EMISOR"){
						$param[2] = str_replace("[$tipo]","($clave)",$param[2]);
					} else {
						$a = $campo->get("",$idorden,$tipo);
						if ($a[0]){
							$valor_token = $a[0]->getValor();
							$param[2] = str_replace("[$tipo]",$valor_token,$param[2]);
						} else $param[2]="";
					}
                                }
                                $sql_extra = $param[2];
                        } else $sql_extra="";
			
			$valor_emisor = $this->ElDato($valor_parametro,$valor_defecto,$param[0],$param[1],$sql_extra);
			$a = $this->getFormatoEmisor("",$valor_emisor);
			$tmp = $a[0];
			$largo_comercio = $tmp["comercio"];
			$jsValida ="checkLargo(this,$largo_comercio)";
			$campo = "<input type='text' id='$nombre_campo' name='$nombre_campo' size='$largo'  onChange=\" ret=checkLargo(this,$largo_comercio); document.forms[0].$nombre_campo.focus();\"  value='$valor_parametro' >";
			break;
		case 9://Cod. Sucursal
			$campo = new DBParametro();

			$param = explode(";",$opciones);// Campo Indice; Campo Descripcion; Where
			$jsCodigo = ($jsCode)?"$jsCode":""; ;
			$sql_extra = ($param[2])?$param[2]:"";
                        if ($param[2]){
                                if(strpos($param[2],"[")>0){
                                        $pi = strpos($param[2],"[");$pf = strpos($param[2],"]");
                                        $tipo = substr($param[2],$pi+1,$pf-$pi-1);
					if ($tipo=="EMISOR"){
						$param[2] = str_replace("[$tipo]","($clave)",$param[2]);
					} else {
						$a = $campo->get("",$idorden,$tipo);
						if ($a[0]){
							$valor_token = $a[0]->getValor();
							$param[2] = str_replace("[$tipo]",$valor_token,$param[2]);
						} else $param[2]="";
					}
                                }
                                $sql_extra = $param[2];
                        } else $sql_extra="";
			echo "<!--TABLA:".$valor_defecto."  SQL EXTRA: $sql_extra -->";
			
			$valor_emisor = $this->ElDato($valor_parametro,$valor_defecto,$param[0],$param[1],$sql_extra);
			$a = $this->getFormatoEmisor("",$valor_emisor);

			$tmp = $a[0];
			$largo_suc = $tmp["sucursal"];
			$jsValida ="checkLargo(this,$largo_comercio)";
			$campo = "<input type='text' id='$nombre_campo' name='$nombre_campo' size='$largo'  onChange=\" ret=checkLargo(this,$largo_suc); document.forms[0].$nombre_campo.focus();\"  value='$valor_parametro' >";
			break;
		case 10://From Store procedure
			$campo = new DBParametro();

			$param = explode(";",$opciones);// Campo Indice; Campo Descripcion; Where
			$jsCodigo = ($jsCode)?"$jsCode":""; ;
			$sql_extra = ($param[2])?$param[2]:"";
			//echo "<p>Clave: $clave </p>";
                        if ($param[1]){
                                if(strpos($param[1],"]")>0){
                                        $pi = strpos($param[1],"[");$pf = strpos($param[1],"]");
                                        $tipo = substr($param[1],$pi+1,$pf-$pi-1);
					if ($tipo=="EMISOR"){
						$param[1] = str_replace("[$tipo]","($clave)",$param[1]);
					} else {
						$a = $campo->get("",$idorden,$tipo);
						if ($a[0]){
							$param[1] = $a[0]->getValor();
						} else $param[1]="";
					}
                                }
			} 
			$parametros = " ".$param[0].",".$param[1];
			$valor_parametro = $this->CallFuncSQL($valor_defecto,$parametros);
			$campo     = "<input type='hidden' name=$nombre_campo value='$valor_parametro' size='$largo' readonly onchange='$jsCode'>";
			break;
		case 11://Find Sucursal
			$dbEntidad = new DBEntidad();
			echo "<!--SUCURSAL:$valor-->";
			if (is_numeric($valor))	{
				$valor_parametro = $dbEntidad->ElDato($valor,"Entidad","id","getCampoEntidad(15,id)");
				$campo = "<input type='text' id='$nombre_campo' size=60 name='$nombre_campo' value='$valor_parametro' readonly>";
			} else {
				$ax = $dbEntidad->get("",3,"","",3,$valor,"","",2);//Busca Direccion
				$entidad = $ax['salida'];
				if ($entidad[0]){//Existe Sucursal
					$id_entidad = $entidad[0]->getId();
					$valor = strtoupper($valor);
					$valor_parametro  = "$valor <!--[$id_entidad]-->";
					$campo = "$valor &nbsp;<input type='hidden' id='$nombre_campo' size=4 name='$nombre_campo' value='$id_entidad' readonly>";
				} else {
					$valor = str_replace("'","&#39;",$valor);
					$campo = "<input type=\"text\" id=\"$nombre_campo\" name=\"$nombre_campo\" value=\"$valor\" onblur=\"$jsCode\" size='$largo'>&nbsp;<a href='javascript:openWindow(\"/index.php?w=lasentidades&tipo_buscar=3&buscarpor=$valor&entidad=3&ver_filtro=1&campo_target=$nombre_campo\",\"Sucursales\",780,400)'><img src='/images/lupa.gif' border=0 title='Buscar Sucursal'></a>&nbsp;<input type='button' onclick='openWindow(\"/index.php?w=nueva_entidad&nsol=1&id_orden=".$this->OrdenPadre($idorden)."&entidad=3&campo_focus=descripcion&entidad=3&editar=1&descripcion=$valor\",\"NuevaSucursal\",780,400)' value='Crear Sucursal'>";
					$valor_parametro ="<div class=titulorojo>$valor_parametro --NO Existe--</div>";	
				}				
			}
			break;
		case 12://Find Comercio
			$dbEntidad = new DBEntidad();
			echo "<!--COMERCIO:$valor-->";
			if ($valor!="")
			if (is_numeric($valor)&&false) {
				$valor_parametro = $dbEntidad->ElDato($valor,"Entidad","id","getCampoEntidad(18,id)");
				$campo = "<input type='hidden' id='$nombre_campo' size=60 name='$nombre_campo' value='$valor_parametro' readonly>";
			} else {
				$ax = $dbEntidad->get("",2,"","",4,$valor,"","",2);$entidad = $ax['salida'];
				if ($entidad[0]){//Existe el comercio
					echo "<!--Encontre una Entidad-->";
					$id_entidad = $entidad[0]->getId();	
					$nomfan     = $this->CallFuncSQL("getCampoEntidad","4,$id_entidad");
					$valor_parametro = "[$valor] $nomfan <!--[$id_entidad]-->";
					$campo = "($valor) $nomfan&nbsp;<input type='hidden' id='$nombre_campo' name='$nombre_campo' value='$id_entidad' readonly>";
				} else {
					echo "<!--NO Encontre una Entidad-->";
					$campo = "($valor) $nomfan&nbsp;<input type='text' id='$nombre_campo' name='$nombre_campo' value='$valor' onblur='$jsCode' size='$largo'><input type='button' onclick=\"openWindow('/index.php?w=nueva_entidad&nsol=1&&entidad=1&campo_focus=descripcion&entidad=2&editar=1&campo_target=$nombre_campo&id_orden=".$this->OrdenPadre($idorden)."','nuevoComercio',760,400)\" name=bbfc value='Crear Comercio'>";
					$valor="<div class=titulorojo>--Crear Comercio--</div>";	
				}
			}
			break;
		case 14: //Titulo
			$campo = "<input type=\"hidden\" name=\"$nombre_campo\" id='$nombre_campo' value='$valor_parametro' size='$largo2'>";
			break;
		case 15://Codigo
			$largo2 = $largo+2;
			$campo = "<input type=\"text\" name=\"$nombre_campo\" id=\"$nombre_campo\" value='$elvalor' onBlur=\"checkLargo(this,$largo);$jsCodigo\" size='$largo2'>";
			break;
		case 16://Hidden
			$largo2 = $largo+2;
			$campo = "<input type=\"hidden\" name=\"$nombre_campo\" id=\"$nombre_campo\" value='$valor_parametro'>";
			break;
		}
		$tmp=array();
		$tmp['campo'] = $campo;
		$tmp['valor'] = strtoupper($valor_parametro);
		return $tmp;
	}

	function xShowCampo($tmp,$url="",$var_campo="",$valor="",$jsCode="",$lenguaje=""){
		$id_campo      = $tmp->getId();
		$desc_campo    = $tmp->getDescripcion();
		$opciones      = $tmp->getOpciones();
		$tipo_dato     = $tmp->getIdTipoDato();
		$valor_defecto = $tmp->getValorDefecto();
		$nombre_campo  = $tmp->getNombreCampo();

		$jsCodigo = ($jsCode)?" onBlur='$jscode'":"";
		if (!$var_campo) $var_campo="valor_campo";
		switch($tipo_dato){
		case 1://Numerico
			$campo = "<input type=\"text\" name=\"valor_campo\" id='valor_campo' value='$valor' onChange=\"$jsCode;\">";
			break;
		case 2://Texto
			$campo = "<input type=\"text\" name=\"valor_campo\" id=\"valor_campo\" size=50 value=\"$valor\"  onChange=\"$jsCode;\">";
 			break;
		case 3://Fecha
			$campo = "<input type=\"text\" name=\"$var_campo\" value=\"$valor\" $lectura class=\"contenidonegro\" $jsCodigo>&nbsp;
		<a href=\"javascript:cal7.popup();\"  class=\"contenidonegro\"><img src=\"/images/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Haga  click para seleccionar una fecha\"></a><script language=\"JavaScript\">var cal7 = new calendar1(document.forms[0].elements['$var_campo']);cal7.year_scroll = false;cal7.time_comp = false;</script>";
			break;
		case 4://Seleccion
			$xopciones = explode(",",$opciones);
			$lasopciones = array();
			for($j=0;$j<count($xopciones);$j++){
				$tmp = $xopciones[$j];$tmp2=array();
				$tmp2[valor] = $xopciones[$j];
				$tmp2[txt]   = $xopciones[$j];
				$lasopciones[] = $tmp2;
			}
			$campo = $Aux->strSelectArray($lenguaje,$lasopciones,"$var_campo","----",$valor,"","MiSubmit('$jsCode')");
			break;
		case 5:// From Table
			$campo = new DBCampo();
			//echo "<!--ValorDefecto:$valor_defecto, Campo:$nombre_campo-->";
			if ($opciones){
				while(strpos($opciones,"{")>0){
					$pi = strpos($opciones,"{");
					$pf = strpos($opciones,"}");
					$tipo = substr($opciones,$pi+1,$pf-$pi-1);

					$a = $campo->get("",$id_entidad,"",$tipo);
					if ($a[0]){
					$valor_token = $a[0]->getValor();
					switch($tipo){
						case 21: $opciones = str_replace("{21}",$valor_token,$opciones);break;
						case 16: $opciones = str_replace("{16}",$valor_token,$opciones);break;
						case 11: $opciones = str_replace("{11}",$valor_token,$opciones);break;
						case 24: $opciones = str_replace("{24}",$valor_token,$opciones);break;
					}
					}
				}
				$sql_extra = " where $opciones order by descripcion";
				//echo "<!--Valor:$sql_extra-->";
			} else $sql_extra="";
			$xopciones = $this->TraerLosDatos($valor_defecto,"id",$nombre_campo,$sql_extra);
			$campo     = $this->strSelectArray($lenguaje,$xopciones,"$var_campo","----",$valor,"","MiSubmit('/index.php?p=$url')");
			$valor     = $this->ElDato($valor,$valor_defecto,"id",$nombre_campo);
			break;
		case 6://Rut Chile
			$campo = "<input type=text name=$var_campo size=10 value='$valor' onChange=\"checkRutField(document.forms[0].valor_campo.value);\" $jsCodigo>";
			break;
		}
		$_tmp=array();
		$_tmp['campo'] = $campo;
		$_tmp['valor'] = strtoupper($valor);
		return $tmp;
	}

	function CamposEntidad($id_entidad,$entidad){
		if ($id_entidad==0) return;
		?><table width="100%"><?
		$Campo = new DBCampo();
		$j=0;
		$loscampos = $Campo->get("",$id_entidad,$entidad);//Campos Entidad Emisor
		//$loscampos = $Campo->getCampos($id);//Campos Entidad Emisor
		while($tmp=$loscampos[$j])
		{
			$id_campo      = $tmp->getId();
			$tipo_campo    = $tmp->getIdTipoCampo();
			//$xtipo       = $Campo->getTipos($tipo_campo);
			$desc_campo    = $tmp->getDescripcion();
			$opciones      = $tmp->getOpciones();
			$tipo_dato     = $tmp->getIdTipoDato();
			
			$valor         = $tmp->getValor();
			$valor_defecto = $tmp->getValorDefecto();
			$nombre_campo  = $tmp->getNombreCampo();
			$campo         = $this->showCampo($tmp);
			if ($id_campo == $idcampo) {
				$fi = "<b>";$ff="</b>";
				$actualiza = "<input type=\"button\" value=\"Modificar\" name=aa onclick=\"poner(4)\">";
			} else {
				$fi = "";
				$ff="";
				$actualiza="";
			}
			$elclick="poner2($id_campo)";
			$color      = $this->flipcolor($color);
		?>
<tr <?=($id_campo!=$idcampo)?$this->rowEffect($j,$color,$elclick):""?>>
<TD class='hnavbg2' width="200"><?="$desc_campo"?></TD>
<td bgcolor="<?=$color?>"><?=($id_campo==$idcampo && $comando==7)?$campo['campo']:$campo['valor']?> &nbsp;<?=($comando==7)?$actualiza:""?></td>
</tr>
	<?	
			$j++;
		}
	?>
</table>
<?
	}
	function estaRepetido($tabla,$campo,$valor){
		$sql = "select $campo from $tabla where $campo='$valor'";
		$rs = $this->query($sql);
		$elvalor="";
		if ($rs){
			$row = $this->getRows($rs);
			$elvalor = $row[0];
		} 
		return $elvalor;
	}
	function Ocultar($seccion,$campo_foco="",$img_on="",$img_off="",$texto="",$titulo=""){
		if (!$img_on) { $img_on = "arrow_down.gif";$img_off="arrow.gif"; }
		$tag = "<a href=\"javascript: toggleLayer('$seccion');";
		if ($campo_foco) $tag.=" document.formulario.$campo_foco.focus();";
		$tag.=" \" onmouseover=\"flecha_$seccion.src='/images/$img_on'\" onmouseout=\"flecha_$seccion.src='/images/$img_off'\" class='contenidonegro'>";
		if ($texto) $tag.=$texto;
		$st = ($titulo)?"title='$titulo'":"";
		$tag.="&nbsp;<img src=\"/images/$img_off\" name=flecha_$seccion border=\"0\" $st></a>";
		return $tag;
	}
	function delArray($a,$arreglo){
		$tmp=array();
		for($i=0;$i<count($arreglo);$i++){
			if ($a!=$arreglo[$i]) $tmp[]=$arreglo[$i];
		}
		return $tmp;
	}
	function lista2BR($separador,$str_lista,$tabla="",$campo1="",$campo2="",$valor=""){
		if (!$campo1) $campo1="id";
		if (!$campo2) $campo2="descripcion";
		if ($str_lista=="") return "";
		$ax = explode($separador,$str_lista);
		$tmp = array();
		for($i=0;$i<count($ax);$i++){
			if ($tabla) $valor =$this->ElDato($ax[$i],$tabla,$campo1,$campo2);
			$token = ($tabla)? $valor:$ax[$i];
			if ($valor==$ax[$i] && $token) $tmp[] = "<li><b>$token</b></li>";
			else if ($token) $tmp[] = "<li>$token</li>";
		}
		return (count($ax)>0)?"<ul type='square'>".implode("",$tmp)."</ul>":"";
	}
	function lista2Select($nombre,$separador,$str_lista,$tabla="",$campo1="",$campo2=""){
		if (!$campo1) $campo1="id";
		if (!$campo2) $campo2="descripcion";
		if ($str_lista=="") return "";
		$ax = explode($separador,$str_lista);
		$tmp = array();
		for($i=0;$i<count($ax);$i++){
			if ($tabla) $valor =$this->ElDato($ax[$i],$tabla,$campo1,$campo2);
			$token = ($tabla)? $valor:$ax[$i];
			$tmp[] = "<option>$token</option>";
		}
		return (count($ax)>0)?"<select name=$nombre class='contenidonegro' style='background-color:#d3d3d3'>".implode("",$tmp)."</select>":"";
	}
	function getFormatoEmisor($id="",$id_emisor=""){
		$sql="select * from formato_cod_com where id>0";
		if ($id) $sql.=" and id=$id";
		if ($id_emisor) $sql.=" and id_emisor=$id_emisor";
		echo "<!-- SQL: $sql -->";
		$rs=$this->query($sql);
		if($rs){
			$salida=array();
			while($row=$this->getRows($rs)){
				$tmp=array();
				$tmp["emisor"]=$row["emisor"];
				$tmp["comercio"]=$row["comercio"];
				$tmp["sucursal"]=$row["sucursal"];
				$salida[]=$tmp;
			}
		}
		return $salida;
	}
	function valorBool($x){
		return ($x)?$x:2;
	}
	function getMes($i){
		switch($i){
			case 1:$mes="Enero";break;
			case 2:$mes="Febrero";break;
			case 3:$mes="Marzo";break;
			case 4:$mes="Abril";break;
			case 5:$mes="Mayo";break;
			case 6:$mes="Junio";break;
			case 7:$mes="Julio";break;
			case 8:$mes="Agosto";break;
			case 9:$mes="Septiembre";break;
			case 10:$mes="Octubre";break;
			case 11:$mes="Noviembre";break;
			case 12:$mes="Diciembre";break;
		}
		return $mes;
	}

	function getDia($i){
		switch($i){
			case 1:$dia="Lunes";break;
			case 2:$dia="Martes";break;
			case 3:$dia="Mi&eacute;rcoles";break;
			case 4:$dia="Jueves";break;
			case 5:$dia="Viernes";break;
			case 6:$dia="S&aacute;bado";break;
			case 7:$dia="Domingo";break;
		}
		return $dia;
	}

	function getUltimoDiaMes($i){
		$mes[1] = 31;
		$mes[2] = (strftime("%Y",strtotime(date("Y-m-d")))%4==0)?29:28;
		$mes[3] = 31;
		$mes[4] = 30;
		$mes[5] = 31;
		$mes[6] = 30;
		$mes[7] = 31;
		$mes[8] = 31;
		$mes[9] = 30;
		$mes[10] = 31;
		$mes[11] = 30;
		$mes[12] = 31;
		return $mes[$i];
	}

	function CortarStr($str,$n){ return substr($str,0,$n)."...";}

	function OrdenPadre($id){
		$this->conecta();
		$sql = "select id_orden_padre from Orden where id=$id";
		$rs = $this->query($sql);
		if ($rs){
			$row = $this->getRows($rs);
			return $row[0];
		}
	}
	function uptField($id,$table,$key,$campo,$valor){
		$sql = "update $table set $campo='$valor' where $key=$id";
		$rs = $this->query($sql);
		if($rs) return true; else return false;
	}
	function FechaFmt($fecha,$lenguaje=""){
		echo "<!--Fecha:$fecha-->";
		if ($fecha){
			$xfecha = explode("-",$fecha);
			$dia = strftime("%u",strtotime($fecha));
			$dd= strftime("%d",strtotime($fecha));
			$anno= substr($xfecha[0],-4);
			$hora = strftime("(%H:%M:%S)",strtotime($fecha));
			switch($xfecha[1]){
				case 1:  $mes[1]="Enero";$mes[2]="Jan";break;
				case 2:  $mes[1]="Febrero";$mes[2]="Feb";break;
				case 3:  $mes[1]="Marzo";$mes[2]="Mar";break;
				case 4:  $mes[1]="Abril";$mes[2]="Apr";break;
				case 5:  $mes[1]="Mayo";$mes[2]="May";break;
				case 6:  $mes[1]="Junio";$mes[2]="Jun";break;
				case 7:  $mes[1]="Julio";$mes[2]="Jul";break;
				case 8:  $mes[1]="Agosto";$mes[2]="Aug";break;
				case 9:  $mes[1]="Septiembre";$mes[2]="Sep";break;
				case 10: $mes[1]="Octubre";$mes[2]="Oct";break;
				case 11: $mes[1]="Noviembre";$mes[2]="Nov";break;
				case 12: $mes[1]="Diciembre";$mes[2]="Dec";break;
			}
			switch($dia){
				case 1: $eldia[1]="Lun";$eldia[2]="Mon";break;
				case 2: $eldia[1]="Mar";$eldia[2]="Tue";break;
				case 3: $eldia[1]="Mi&eacute;";$eldia[2]="Wed";break;
				case 4: $eldia[1]="Jue";$eldia[2]="Thu";break;
				case 5: $eldia[1]="Vie";$eldia[2]="Fri";break;
				case 6: $eldia[1]="S&aacute;b";$eldia[2]="Sat";break;
				case 7: $eldia[1]="Dom";$eldia[2]="Sun";break;
			}
			if ($hora=="(00:00:00)") $hora="";
			return "$eldia[$lenguaje] $dd, $mes[$lenguaje] $anno $hora";
		} else return "----";
	}
	function listaPadres($id){
		$sql = "select id_orden_padre from Orden where id=$id";
		$rs = $this->query($sql);
		if ($rs){ 
			$row = $this->getRows($rs);
			$id2 = $row[id_orden_padre];
			$v = $this->listaPadres($id2);
			if ($v)
				return "$v,$id";
			else
				return "$id";
		} else {
			return "";	
		}
	}
	function listaHijos($id){
		$sql = "select id from Orden where id_orden_padre =$id";
		$rs = $this->query($sql);
		if ($rs){ 
			$row = $this->getRows($rs);
			$id2 = $row[id];
			$v = $this->listaHijos($id2);
			if ($v)
				return "$v,$id";
			else
				return "$id";
		} else {
			return "";	
		}
	}
	function sendHTMLemail($HTML,$from,$to,$subject)
	{
		$headers = "From: $from \r\n"; 
		$headers .= "MIME-Version: 1.0\r\n"; 
		$headers .= "Content-Type: text/html;\r\n\r\n";
		// And then send the email ....
		mail($to,$subject,$HTML,$headers);
	}
	function sendMail($mensaje,$from,$to,$subject){
		$ticks = $this->ticks();
		$dir = "/home/eroman/desa/sayot";
		$dir = "/home/sayot";
		$filename = "$dir/lib_email/email$ticks.txt";
		$fp=fopen($filename,"w");
		
		fwrite($fp,$mensaje);
		fclose($fp);
	
		$fp=fopen($filename,"r");
		$mensaje=fread($fp,filesize($filename));
		fclose($fp);
		
		$para = $to;
		$from ="sistema@sayot.pointpay.cl";
		$asunto = $subject;
		$mail = new htmlMimeMail5();
		$mail->setFrom($from);
		$mail->setSubject($asunto);
		$mail->setPriority('high');
		//$mail->setText('Sample text');
		$mail->setHTML($mensaje);
		//$mail->addEmbeddedImage(new fileEmbeddedImage("/lib_archivos/$archivo"));
		//$mail->addAttachment(new fileAttachment('example.zip'));
		$mail->send(array($to));
	}
	function getIpAddress() {
	return (empty($_SERVER['HTTP_CLIENT_IP'])?(empty($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['REMOTE_ADDR']:$_SERVER['HTTP_X_FORWARDED_FOR']):$_SERVER['HTTP_CLIENT_IP']);
	}
	function stValor($x){ $valor= ($x)?$x:"---"; return $valor;}

	function datediff($interval, $datefrom, $dateto, $using_timestamps = false) {
	/*
	$interval can be:
	yyyy - Number of full years
	q - Number of full quarters
	m - Number of full months
	y - Difference between day numbers
	(eg 1st Jan 2004 is "1", the first day. 2nd Feb 2003 is "33". The datediff is "-32".)
	d - Number of full days
	w - Number of full weekdays
	ww - Number of full weeks
	h - Number of full hours
	n - Number of full minutes
	s - Number of full seconds (default)
	*/
	
		if (!$using_timestamps) {
		$datefrom = strtotime($datefrom, 0);
		$dateto = strtotime($dateto, 0);
		}
		$difference = $dateto - $datefrom; // Difference in seconds
		
		switch($interval) {
		
		case 'yyyy': // Number of full years
		
		$years_difference = floor($difference / 31536000);
		if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", datefrom)+$years_difference) > $dateto) {
		$years_difference--;
		}
		if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto)-($years_difference+1)) > $datefrom) {
		$years_difference++;
		}
		$datediff = $years_difference;
		break;
		
		case "q": // Number of full quarters
		
		$quarters_difference = floor($difference / 8035200);
		while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($quarters_difference*3), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
		$months_difference++;
		}
		$quarters_difference--;
		$datediff = $quarters_difference;
		break;
		
		case "m": // Number of full months
		
		$months_difference = floor($difference / 2678400);
		while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
		$months_difference++;
		}
		$months_difference--;
		$datediff = $months_difference;
		break;
		
		case 'y': // Difference between day numbers
		
		$datediff = date("z", $dateto) - date("z", $datefrom);
		break;
		
		case "d": // Number of full days
		
		$datediff = floor($difference / 86400);
		break;
		
		case "w": // Number of full weekdays
		
		$days_difference = floor($difference / 86400);
		$weeks_difference = floor($days_difference / 7); // Complete weeks
		$first_day = date("w", $datefrom);
		$days_remainder = floor($days_difference % 7);
		$odd_days = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?
		if ($odd_days > 7) { // Sunday
		$days_remainder--;
		}
		if ($odd_days > 6) { // Saturday
		$days_remainder--;
		}
		$datediff = ($weeks_difference * 5) + $days_remainder;
		break;
		
		case "ww": // Number of full weeks
		
		$datediff = floor($difference / 604800);
		break;
		
		case "h": // Number of full hours
		
		$datediff = floor($difference / 3600);
		break;
		
		case "n": // Number of full minutes
		
		$datediff = floor($difference / 60);
		break;
		
		default: // Number of full seconds (default)
		
		$datediff = $difference;
		break;
		}
		
		return $datediff;
	
	}
	function getSubFecha($fecha,$dias){
		$sql = "select date_sub('$fecha',interval $dias day)";
		//echo "<p>SQL: $sql </p>";
		$rs=$this->query($sql);
		if ($rs){
			$row = $this->getRows($rs);
			return $row[0];
		}		
	}
}
?>