<?

Class Idiomas_Boarding
{
    var $tipoUsuario;
    var $titulo;
    var $subtitulo;
    var $forma_pago;
    var $tipoReserva;
    var $pendiente;
    
    var $dcto_pax;
    var $dcto_lugar;
    var $dcto_lugar_reserva;
    var $dcto_excursiones;

    var $descuentos_aplicados;
    var $precio_pendiente;
    
    var $const_nombre;
    var $const_servicio;
    var $const_tipoPax;
    var $const_fecha;
    var $cons_lugar;
    var $const_horario;
    var $const_telefono;
    var $const_total;
    var $const_fop;
    var $const_descuentos;
    var $estado_reagendado;
    
    var $total_comision;
    var $precio_total;
    var $cod_cierre;

    function Idioma($cod_tusua, $tipoReserva, $fop, $idioma, $dcto_pax, $dcto_lugar, $dcto_lugar_reserva, $dcto_excursiones, $total_comision, $precio_total, $estado_reagendado,$cod_cierre)
    {
        
        $this->dcto_pax = $dcto_pax;
        $this->dcto_lugar = $dcto_lugar;
        $this->dcto_lugar_reserva = $dcto_lugar_reserva;
        $this->dcto_excursiones = $dcto_excursiones;
        $this->total_comision =  $total_comision;
        $this->precio_total = $precio_total;
        $this->cod_cierre = $cod_cierre;
        
        $this->tipoUsuario = $cod_tusua;
        $this->tipoReserva = $tipoReserva;
        $this->estado_reagendado = $estado_reagendado;
        
        
        if ($idioma == 0 || $idioma == 1)
        {        
            $this->Spanish();
            $this->FopSpanish($fop);
            
        }
        if ($idioma == 2)
        {
            $this->English();
            $this->FopEnglish($fop);
            
        }
        if ($idioma == 3)
        {
            $this->Portugues();
            $this->FopPortugues($fop);
        }

        
    }
    
    function Spanish()
    {
         
        if ($this->dcto_pax != 0)
        {
            $descuento_p = "CANTIDAD PASAJEROS, ";
        }
        if ($this->dcto_lugar != 0)
        {
            $descuento_l = "LUGAR RECOGIDA, ";
        }
        if ($this->dcto_lugar_reserva != 0)
        {
            $descuento_lr = "LUGAR RESERVA, ";
        }
        if ($this->dcto_excursiones != 0)
        {
            $descuento_ex = "CANTIDAD DE SERVICIOS";              
        }
        
        if ($this->dcto_pax != 0 || $this->dcto_lugar != 0 || $this->dcto_lugar_reserva != 0 || $this->dcto_excursiones != 0 )
        {
            $this->const_descuentos = "DESCUENTOS APLICADOS :";
        }
        
        
         $this->descuentos_aplicados = $descuento_c.$descuento_p.$descuento_l.$descuento_lr.$descuento_ex;
         
       

         if ($this->tipoReserva == 1 || $this->tipoReserva == 4)
         {
            $this->subtitulo='RESERVA PAGADA';
         }
         
         if ($this->cod_cierre == 2)
         {
            $this->subtitulo='RESERVA PAGADA';
         }
         
         if ($this->estado_reagendado == 1)
         {
            $this->subtitulo= 'RESERVA REAGENDADA - PAGADA';
         }
         
         
         $this->const_nombre = 'NOMBRE';
         $this->const_servicio = 'SERVICIO';
         $this->cons_lugar = 'LUGAR DE RECOGIDA Y REGRESO';
         $this->const_fecha = 'FECHA DE SERVICIO';
         $this->const_fop = 'FORMA DE PAGO';
         $this->const_horario = 'HORARIO RECOGIDA';
         $this->const_telefono = 'TELEFONO CONTACTO CLIENTE';
         $this->const_tipoPax = 'PASAJEROS';
         $this->const_total = 'PRECIO TOTAL';
         
         

    }
    
    function English()
    {
        
        if ($this->dcto_pax != 0)
        {
            $descuento_p = "NUMBER OF PASSENGERS, ";
        }
        if ($this->dcto_lugar != 0)
        {
            $descuento_l = "PLACE OF PICK UP, ";
        }
        if ($this->dcto_lugar_reserva != 0)
        {
            $descuento_lr = "PLACE OF RESERVATION, ";
        }
        if ($this->dcto_excursiones != 0)
        {
            $descuento_ex = "NUMBER OF SERVICES";              
        }
        
        if ($this->dcto_pax != 0 || $this->dcto_lugar != 0 || $this->dcto_lugar_reserva != 0 || $this->dcto_excursiones != 0 )
        {
            $this->const_descuentos = "DISCOUNTS INCLUDED :";
        }
        
        
         $this->descuentos_aplicados = $descuento_c.$descuento_p.$descuento_l.$descuento_lr.$descuento_ex;
         
         if ($this->tipoReserva == 1 || $this->tipoReserva == 4)
         {
            $this->subtitulo='FULLY PAID RESERVATION';
         }

         if ($this->cod_cierre == 2)
         {
            $this->subtitulo='FULLY PAID RESERVATION';
         }
         if ($estado_reagendado == 1)
         {
            $this->subtitulo= 'RESCHEDULED RESERVATION - PAID';
         }
         
         $this->const_nombre = 'NAME';
         $this->const_servicio = 'SERVICE';
         $this->const_tipoPax = 'PASSENGERS';
         $this->const_fecha = 'DATE OF SERVICE';
         $this->cons_lugar = 'PLACE OF PICK-UP & RETURN';
         $this->const_horario = 'PICK-UP TIME';
         $this->const_telefono = 'CLIENT CONTACT TELEPHONE';
         $this->const_total = 'TOTAL SERVICE';
         $this->const_fop = 'PAYMENT';

    }
    
    function Portugues()
    {
        
        if ($this->dcto_pax != 0)
        {
            $descuento_p = "QUANTIDADE DE PASSAGEIROS, ";
        }
        if ($this->dcto_lugar != 0)
        {
            $descuento_l = "LUGAR DE SAIDA, ";
        }
        if ($this->dcto_lugar_reserva != 0)
        {
            $descuento_lr = "LUGAR DE RESERVA, ";
        }
        if ($this->dcto_excursiones != 0)
        {
            $descuento_ex = "QUANTIDADE DE SERVIÇOS";              
        }
        
        if ($this->dcto_pax != 0 || $this->dcto_lugar != 0 || $this->dcto_lugar_reserva != 0 || $this->dcto_excursiones != 0 )
        {
            $this->const_descuentos = "DESCONTOS INCLUÍDOS :";
        }
        
        
         $this->descuentos_aplicados = $descuento_c.$descuento_p.$descuento_l.$descuento_lr.$descuento_ex;

         if ($this->tipoReserva == 1 || $this->tipoReserva == 4)
         {
            $this->subtitulo='RESERVA PAGA';
         }

         if ($this->cod_cierre == 2)
         {
            $this->subtitulo='RESERVA PAGA';
         }
         if ($estado_reagendado == 1)
         {
            $this->subtitulo= 'RESERVA REPROGRAMADA - PAGA';
         }
         $this->const_nombre = 'NOME';
         $this->const_servicio = 'SERVIÇO';
         $this->const_tipoPax = 'PASSAGEIROS';
         $this->const_fecha = 'DATA DO SERVIÇO';
         $this->cons_lugar = 'LUGAR DE SAIDA E DE REGRESSO';
         $this->const_horario = 'HORARIO DE SAIDA';
         $this->const_telefono = 'TELEFONE DE CONTATO DO CLIENTE';
         $this->const_total = 'PREÇO TOTAL';
         $this->const_fop = 'PAGAMENTO';

    }
    
    
    function FopSpanish($fop)
    {
        switch($fop)
        {
    	case "C":
        
            if ($this->tipoUsuario == 4)
            {
                $this->forma_pago = "CC";  
            }
            else
            {
                $this->forma_pago = "TARJETA DE CREDITO";
            }
            
            $this->titulo = 'BOARDING PASS';
            //$this->subtitulo='RESERVA PAGADA';
            
    	break;  
        
    	case "TC":
        
            if ($this->tipoUsuario == 4)
            {
                $this->forma_pago = "CC";  
            }
            else
            {
                $this->forma_pago = "TARJETA DE CREDITO";
            }
            
            $this->titulo = 'BOARDING PASS';
            //$this->subtitulo='RESERVA PAGADA';
            
    	break;  
                 	
    	case "D":
        
            $this->forma_pago = "TARJETA DE DEBITO";
            $this->titulo = 'BOARDING PASS';
            //$this->subtitulo='RESERVA PAGADA';
            
    	break;
                    	
    	case "E":
        
            $this->forma_pago = "EFECTIVO";
            $this->titulo = 'BOARDING PASS';
            //$this->subtitulo='RESERVA PAGADA';
            //$ObjTotalRedondeado = new BuscaDescuentos();
            //$this->precio_total = $ObjTotalRedondeado->RedondearValor($this->precio_total);

            
    	break;
        
        case "pp":
        
            if ($cod_cierre == 2)
            {
                $this->titulo = 'BOARDING PASS';
                $this->forma_pago = 'PAGADO';
            }
            else
            {
                $this->forma_pago = "POR PAGAR";
                $this->titulo = 'RESERVA PENDIENTE DE PAGO';
                $this->subtitulo='';
            }

    	break;
        case "RA":
        
            if ($cod_cierre == 2)
            {
                $this->titulo = 'BOARDING PASS';
                $this->forma_pago = 'PAGADO';
            }
            else
            {
                //$T_fop = "ABONO :";
                $this->forma_pago = "ABONO : $".number_format($this->total_comision);
                $this->pendiente = "PENDIENTE DE PAGO:";
                $this->precio_pendiente = "$".number_format($this->precio_total-$this->total_comision);
                $this->titulo = 'RESERVA DEPOSITO PARCIAL';
                $this->subtitulo='';
            }
            
    	break;
        }
    }
    
        
    function FopEnglish($fop)
    {
        switch($fop)
        {
    	case "C":
        
            if ($this->tipoUsuario == 4)
            {
                $this->forma_pago = "CC";  
            }
            else
            {
                $this->forma_pago = "CREDIT CARD";
            }
            
            $this->titulo = 'BOARDING PASS';
           // $this->subtitulo='FULLY PAID RESERVATION';
            
    	break;  
    	case "TC":
        
            if ($this->tipoUsuario == 4)
            {
                $this->forma_pago = "CC";  
            }
            else
            {
                $this->forma_pago = "CREDIT CARD";
            }
            
            $this->titulo = 'BOARDING PASS';
           // $this->subtitulo='FULLY PAID RESERVATION';
            
    	break;   
                 	
    	case "D":
        
            $this->forma_pago = "DEBIT CARD";
            $this->titulo = 'BOARDING PASS';
            //$this->subtitulo='FULLY PAID RESERVATION';
            
    	break;
                    	
    	case "E":
        
            $this->forma_pago = "CASH";
            $this->titulo = 'BOARDING PASS';
            //$this->subtitulo='FULLY PAID RESERVATION';
            //$ObjTotalRedondeado = new BuscaDescuentos();
            //$this->precio_total = $ObjTotalRedondeado->RedondearValor($this->precio_total);
            
    	break;
        
        case "pp":
        
            if ($cod_cierre == 2)
            {
                $this->titulo = 'BOARDING PASS';
                $this->forma_pago = 'PAID';
            }
            else
            {
                $this->forma_pago = "PENDING PAYMENT";
                $this->titulo = 'PENDING PAYMENT RESERVATION';
                $this->subtitulo='';
            }

    	break;
        case "RA":
        
            if ($cod_cierre == 2)
            {
                $this->titulo = 'BOARDING PASS';
                $this->forma_pago = 'PAID';
            }
            else
            {
                //$T_fop = "ABONO :";
                $this->forma_pago = "DEPOSIT : $".number_format($this->total_comision);
                $this->pendiente = "PENDING PAYMENT:";
                $this->precio_pendiente = "$".number_format($this->precio_total-$this->total_comision);
                $this->titulo = 'PARTIAL DEPOSIT RESERVATION';
                $this->subtitulo='';
            }
            
    	break;
        }
    }
    
    function FopPortugues($fop)
    {
        switch($fop)
        {
    	case "C":
        
            if ($this->tipoUsuario == 4)
            {
                $this->forma_pago = "CC";  
            }
            else
            {
                $this->forma_pago = "CARTÃO DE CREDITO";
            }
            
            $this->titulo = 'BOARDING PASS';
           // $this->subtitulo='RESERVA PAGA';
            
    	break; 
          
    	case "TC":
        
            if ($this->tipoUsuario == 4)
            {
                $this->forma_pago = "CC";  
            }
            else
            {
                $this->forma_pago = "CARTÃO DE CREDITO";
            }
            
            $this->titulo = 'BOARDING PASS';
           // $this->subtitulo='RESERVA PAGA';
            
    	break; 
                 	
    	case "D":
        
            $this->forma_pago = "CARTÃO DE DEBITO";
            $this->titulo = 'BOARDING PASS';
            //$this->subtitulo='RESERVA PAGA';
            
    	break;
                    	
    	case "E":
        
            $this->forma_pago = "DINHEIRO";
            $this->titulo = 'BOARDING PASS';
            //$this->subtitulo='RESERVA PAGA';
           // $ObjTotalRedondeado = new BuscaDescuentos();
            //$this->precio_total = $ObjTotalRedondeado->RedondearValor($this->precio_total);
            
    	break;
        
        case "pp":
        
            if ($cod_cierre == 2)
            {
                $this->titulo = 'BOARDING PASS';
                $this->forma_pago = 'PAGA';
            }
            else
            {
                $this->forma_pago = "PAGAMENTO PENDENTE";
                $this->titulo = 'RESERVA PENDENTE PARA PAGAMENTO';
                $this->subtitulo='';
            }

    	break;
        case "RA":
        
            if ($cod_cierre == 2)
            {
                $this->titulo = 'BOARDING PASS';
                $this->forma_pago = 'PAGA';
            }
            else
            {
                //$T_fop = "ABONO :";
                $this->forma_pago = "ABONO : $".number_format($this->total_comision);
                $this->pendiente = "PAGAMENTO PENDENTE:";
                $this->precio_pendiente = "$".number_format($this->precio_total-$this->total_comision);
                $this->titulo = 'PAGAMENTO PARCIAL DE RESERVA';
                $this->subtitulo='';
            }
            
    	break;
        }
    }
}
?>