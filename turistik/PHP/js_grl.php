<script type="text/javascript">

function Ajax(){
var xmlHttp;
	try{	
		xmlHttp=new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
	}
	catch (e){
		try{
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
		}
		catch (e){
		    try{
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e){
				alert("No AJAX!?");
				return false;
			}
		}
	}

xmlHttp.onreadystatechange=function(){
	if(xmlHttp.readyState==4){
		document.getElementById('ReloadThis').innerHTML=xmlHttp.responseText;
		setTimeout('Ajax()',3000);
	}
}
xmlHttp.open("GET","estado_carrito_grl.php",true);
xmlHttp.send(null);
}

window.onload=function(){
	setTimeout('Ajax()',3000);
}


$(document).ready(function() {
    $("a[rel*=resumen]").leanModal(
    { 
    	top : 200, overlay : 0.4, closeButton: ".modal_close" 
     });

    
});


function Reservar(a)
{
    var boton = ($(a).attr("name"));
    
    var tipo_excursion = <?=$tipo_excursion?>;
    var reserva = false;
    var lrecogida = document.getElementById("lRecogida").value;
    var hotel = document.getElementById("select1").value;
    var telefono = document.CompraExcursion.telefono.value;
    var habit = document.getElementById("habitacion").value;
    
    
    if (lrecogida != 0 && hotel != '' && telefono != '' && habit != '') 
    {
        var reserva = true;
    }
        


    var cookie = <?=$_COOKIE['cantidad']?>;
    var tipoUsua = <?=$_SESSION['cod_tusua']?>;
    
    

    if (reserva == true) 
    {  
        
        $.post("consulta_excursiones_existentes.php",function(data)
        {
            document.getElementById("total_pasajeros").value = data;
            
            if (data == 0)
            {
                alert('No existen pasajeros para realizar una reserva');
            }
            if (cookie >= data && data > 0)
            {

                // agregar condicion si va para el carrito o el form de reservas
                if (boton == 'reservar')
                {
                   finalizar_desde_resumen();
                }
                else
                {
                    document.CompraExcursion.carrito.click(); 
                }
                    
            }

            if (cookie < data)
            {
              $("#cant_pax").val(data);
              $("#btn").click();

            }

        });
        
    }
    else
    {
        alert('Complete los campos requeridos marcados con (*)');

    }
    

}

function finalizar_desde_resumen()
{
    document.CompraExcursion.comprar.click();  
}

function ocultarReconfirme()
{
    var tipoUsua = <?=$_SESSION['cod_tusua']?>;
    var tipo_excursion = <?=$tipo_excursion?>;
    var precio_comisionado = <?=$precio_com?>;
   // var reserva = false;
    var lrecogida = document.getElementById("lRecogida").value;
    var hotel = document.getElementById("select1").value;
    var telefono = document.CompraExcursion.telefono.value;
    var habit = document.getElementById("habitacion").value;
    
    $.post("consulta_cantidad_pax.php",{ cant:$('#cant_pax').val(), tipo_ex:$('#tipo_excursion').val(), fecha:$('#fecha_format').val(), total_pax:$('#total_pasajeros').val(), precio_com:precio_comisionado },function(data)
    { 
        
       // alert(data)
        
        if (data == 'ok')
        {

            $("#lean_overlay").fadeOut(200);
            $("#reconfirme").css({ 'display' : 'none' });
            
            finalizar_desde_resumen();
                 
        }
        
        if (data == 'x')
        {
            alert('El numero ingresado no corresponde a la cantidad de pasajeros');
            $("#lean_overlay").fadeOut(200);
            $("#reconfirme").css({ 'display' : 'none' });
        }
        
        if (data == 'no')
        {
            alert('No existen cupos disponibles para esta cantidad');
            window.close();
            
        }
    });

}

$(document).ready(function(){
	$("#select1").change(function(){ 
		$.post("CARGA_dinamica.php?a=1",{ id:$(this).val() },function(data){$("#telefono").html(data);})
	});
})

$(document).ready(function(){
	$("#tipoPasajero").change(function(){ 
		$.post("CARGA_dinamica.php?a=6",{ id:$(this).val(), id_nac:$(document.getElementById("nacionalidad")).val(), cod_eo:$(document.getElementById("tipo_excursion")).val() },function(data){$("#tipoCliente").html(data);})
	});
})


function setFocus()
{
     document.getElementById("pasajero").focus();
}
</script>