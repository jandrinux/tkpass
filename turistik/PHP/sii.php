<?php
$miConexion = new ClaseConexion;
$items =  array();
$excursiones_stk = array(6,54,55,63,64);
    
$line = 1;
$tipo_boleta = false; 


    ## GENERACION DE BOLETA EXENTA

    for ( $e=0; $e<count($excursion_array); $e++) 
    {

        $adultos = $pasajeros_cant[$excursion_array[$e]]['adt'];
        $ninos = $pasajeros_cant[$excursion_array[$e]]['chd'];

        $miConexion->Conectar();
        $SQL = "SELECT NOMBRE_EO, PRECIO_EO, VALOR_EXENTO, VALOR_CHILD, MANAGER_TTS, MANAGER_CHD FROM EXCURSION_ORIGEN WHERE CODIGO_EO = ".$excursion_array[$e];
        $query2=$miConexion->EjecutaConsulta($SQL);
        while($row = mysql_fetch_array($query2))
        {

             $codigoManagerADT = $row['MANAGER_TTS'];
             $codigoManagerCHD = $row['MANAGER_CHD'];
             $nombreEx = $row['NOMBRE_EO'];
			 $precio_original_adt = $row['PRECIO_EO'];
			 $valor_excursion = $total_excursion[$e] / $adultos;
			 $valor_afecto = $row['VALOR_EXENTO'];
			 $adt_exento = $precio_original_adt - $valor_afecto;
			 $adt_afecto = $row['VALOR_EXENTO'];


		 	 if ($precio_original_adt  == $valor_excursion) # no hay descuento
			 {
			 	$precioADT = $adt_exento;
			 }
			 else
			 {
				$desc = ($precio_original_adt * $adultos) - $total_excursion[$e];
				$precioADT = ($total_excursion[$e] / $adultos) - $adt_afecto;
			 }

             $precioCHD = $row['VALOR_CHILD'];

             if ($adultos>0 && $precioADT > 0)
             {

                //item excursion solo adulto
                $i = new Item;
                $i->Linea=$line;
                $i->codigoMannager=$codigoManagerADT;
                $i->Descripcion=$nombreEx. " ADT";
                $i->Cantidad=$adultos;
                $i->PrecioUnitario=(int)$precioADT;
                $i->Descuento=(int)$desc;
                $items[] = $i ;
                $line++;
                
             }
             if ($ninos>0 && $precioCHD > 0)
             {
                //item excursion solo chd
                $i = new Item;
                $i->Linea=$line;
                $i->codigoMannager= $codigoManagerCHD;
                $i->Descripcion=$nombreEx." CHD";
                $i->Cantidad=$ninos;
                $i->PrecioUnitario=(int)$precioCHD;
                $i->Descuento=0;
                $items[] = $i ;
                $line++;           

             }  
		  /*
             echo "exento adt: ".$precioADT."<br>";
             echo "exento desc adt: ".$desc."<br>";
             echo "exento chd: ".$precioCHD."<br>";
          */              
           $descuento = $descuento + $desc;
        }

        $adultos = 0;
        $ninos = 0;
        

    }


    $g = new BoletaGen;
	$retorno =	$g->generar(false,$items, $descuento);
	$url_exento = $retorno->Message;
	$boleta_exenta = $retorno->numero;

	
    unset($items);
	## GENERACION DE BOLETA AFECTA
	if ($tipo_venta == 1 || $tipo_venta == 2) {

		$items =  array();
	    $line = 1;
        $tipo_boleta = true;

	    for ( $e=0; $e<count($excursion_array); $e++) 
	    {

            if (!in_array($excursion_array[$e], $excursiones_stk))
        	{

        		$adultos = $pasajeros_cant[$excursion_array[$e]]['adt'];

		        $miConexion->Conectar();
		        $SQL = "SELECT NOMBRE_EO, PRECIO_EO, VALOR_CHILD,VALOR_EXENTO, MANAGER_TTK, MANAGER_CHD FROM EXCURSION_ORIGEN WHERE CODIGO_EO = ".$excursion_array[$e];
		        $query2=$miConexion->EjecutaConsulta($SQL);
		        while($row = mysql_fetch_array($query2))
		        {
	  
		             $codigoManagerADT = $row['MANAGER_TTK'];
		             $codigoManagerCHD = $row['MANAGER_CHD'];
		             $nombreEx = $row['NOMBRE_EO'];
					 $adt_afecto = $row['VALOR_EXENTO'];
					 $precioADT = $adt_afecto;
	 
		             if ($adultos>0 && $precioADT > 0)
		             {
		                //item excursion solo adulto
		                $i = new Item;
		                $i->Linea=$line;
		                $i->codigoMannager=$codigoManagerADT;
		                $i->Descripcion=$nombreEx. " ADT";
		                $i->Cantidad=$adultos;
		                $i->PrecioUnitario=(int)$precioADT;
		                $i->Descuento=0;
		                $items[] = $i ;
		                $line++;
		                
		             }
			         $adultos = 0;

                     //echo "afecto adt: ".$precioADT."<br>";


		        	
		        }

	        }

	    }
	    $g = new BoletaGen;
		$retorno =	$g->generar(true,$items,0);
		$url_afecto = $retorno->Message;
		$boleta_afecta = $retorno->numero;
	}

?>
