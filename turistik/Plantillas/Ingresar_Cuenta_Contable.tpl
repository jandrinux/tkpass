<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cuentas Contables</title>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#Layer3 {position:absolute;
	left:14px;
	top:50px;
	width:316px;
	height:46px;
	z-index:14;
}
#Layer2 {position:absolute;
	left:831px;
	top:53px;
	width:132px;
	height:116px;
	z-index:1;
}
-->
</style>
</head>
<script>
function salir()
	{
		window.close();
	}

			
function IngresarCuenta()
	{
	 	if (document.form1.tipocuenta.value=='0')
			{
				alert("Debe Seleccionar el Tipo de Cuenta que desea Ingresar");
				document.form1.tipocuenta.focus();
				return;
			}
		
		if (document.form1.cuenta.value=='')
			{
				alert("Debe Ingresar Cuenta");
				document.form1.cuenta.focus();
				return;
			}
		
		if (document.form1.descripcion.value=='')
			{
				alert("Debe Ingresar Descripción para la cuenta que desea ingresar");
				document.form1.descripcion.focus();
				return;
			}		
		document.form1.guardar.value=1;
		document.form1.submit;
}
function TipoCuenta()
	{
		miPopup = window.open("../PHP/Ingresar_Tipo_Cuenta.php?flag=1","cue","top=200,left=400,width=550,height=250,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
</script>
<body>
<form id="form1" name="form1" method="post" action="">
  <div class="TituloPlomo" id="Layer3">
    <table width="312" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="82" rowspan="4"><img src="../Imagenes/Crystal_Clear_app_korganizer80x80.png" width="80" height="80" /></td>
        <td width="13" rowspan="4">&nbsp;</td>
        <td width="217"><p>&nbsp;</p></td>
      </tr>
      <tr>
        <td height="22" class="TituloPlomo"><strong>Cuentas Contables </strong><strong></strong></td>
      </tr>
      <tr>
        <td height="22" class="TituloFecha">{fecha}</td>
      </tr>
      <tr>
        <td height="22" class="TituloFecha">&nbsp;</td>
      </tr>
    </table>
  </div>
  <p>&nbsp;</p>
 <p>&nbsp;</p>
 <p>&nbsp;</p>
<div id="segundodiv" style="position:absolute; left:13px; top:153px; width:503px; height:47px; z-index:3; border:0pt black solid;">
  <table width="493" border="0" cellspacing="0">
    
    <tr>
      <td width="121" class="celdalazul">Tipo Cuenta</td>
      <td width="368" class="celdalCELEST"><select name="tipocuenta" class="seleccionTurist_180">
          <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
	  			<!-- START BLOCK : bloquetipocuenta -->
       			<option value="{CODIGOTIPOCUENTA}" class="seleccion2" {SELECCIONAR}>{DESCRIPCIONTIPOCUENTA}</option>
	   			<!-- END BLOCK : bloquetipocuenta-->
        </select>
        <img src="../Imagenes/nuevo.png" width="34" height="11" alt="Nuevo Tipo de Cuenta" onclick="TipoCuenta()" style="cursor:hand" /></td>
    </tr>
    <tr>
      <td class="celdalazul">Cuenta</td>
      <td class="celdalCELEST"><label>
        <input name="cuenta" type="text" class="seleccionTurist_180" id="cuenta" maxlength="12" />
      </label></td>
    </tr>
    <tr>
      <td class="celdalazul">Descripci&oacute;n</td>
      <td class="celdalCELEST"><label>
        <input name="descripcion" type="text" class="seleccionTurist_250" id="descripcion" />
      </label></td>
    </tr>
  </table>
  <div id="tercerdiv" style="position:absolute; left:126px; top:101px; width:219px; height:23px; z-index:3; border:0pt black solid;">
   <input name="guardar" type="hidden" value="0" />
    <input name="button" type="submit" class="BotonTurist_Celest" value="Ingresar"  onclick="IngresarCuenta()"/>
    <input name="Submit" type="button" class="BotonTurist_Celest" value="Cancelar" onclick="salir()" {VISIBILIDADBOTON} />
   
  </div>
  <p>&nbsp;</p>
</div>
</form>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>
</body>
</html>
