var aWindow=null;

  var xmlhttp = false;
  //Check if we are using IE.
  try {
    //If the Javascript version is greater than 5.
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    //alert ("You are using Microsoft Internet Explorer.");
  } catch (e) {
    //If not, then use the older active x object.
    try {
      //If we are using Internet Explorer.
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      //alert ("You are using Microsoft Internet Explorer");
    } catch (E) {
      //Else we must be using a non-IE browser.
      xmlhttp = false;
    }
  }
  //If we are using a non-IE browser, create a javascript instance of the object.
  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    xmlhttp = new XMLHttpRequest();
    //alert ("You are not using Microsoft Internet Explorer");
  }
  function makerequest(serverPage, objID) {
	//startTimer();
    var obj = document.getElementById(objID);
    xmlhttp.open("GET", serverPage);
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        obj.innerHTML = xmlhttp.responseText;
      }
    }
    xmlhttp.send(null);
  }

function openWin(URL,width,height)
{
	w = screen.width/2;
	h = screen.height/2;
	t = 10;//h -(height/2);
	l = w -(width/2);
	if (aWindow == null)
	{
		aWindow=window.open(URL,"windows","width="+width+",height="+height+",resizable=yes,border=yes,scrollbars=auto, title=yes,top="+t+",left="+l+"");
	}
	else
	{
		if (!document.all && aWindow.closed)
			aWindow=window.open(URL,"windows","width="+width+",height="+height+",resizable=yes,border=yes,title=yes,scrollbars=1,top="+t+",left="+l+"");
		else
		{
			if (!document.all)
			{
				aWindow.focus();
				aWindow.location = URL
			}
			else
			{
				aWindow=window.open(URL,"windows","width="+width+",height="+height+",resizable=yes,border=yes,title=yes,scrollbars=auto,top="+t+",left="+l+"");
			}
		}
	}
}

function openWindow(URL,ventana,width,height)
{
	w = screen.width/2;
	h = screen.height/2;
	t = h -(height/2);
	l = w -(width/2);

	if (aWindow == null)
	{
		aWindow=window.open(URL,ventana,"width="+width+",height="+height+",resizable=yes,scrollbars=1, border=yes, title=yes,top="+t+",left="+l+" ");
	}
	else
	{
		if (!document.all && aWindow.closed)
			aWindow=window.open(URL,ventana,"width="+width+",height="+height+",resizable=yes,title=yes,border=yes,scrollbars=1,top="+t+",left="+l+"");
		else
		{
			if (!document.all)
			{
				aWindow.focus();
				aWindow.location = URL
			}
			else
			{
				aWindow=window.open(URL,ventana,"width="+width+",height="+height+",resizable=yes,title=yes,border=yes,scrollbars=1,top="+t+",left="+l+"");
			}
		}
	}
}

function blink (elId) {
        var html = '';
        if (document.all)
                html += 'var el = document.all.' + elId + ';';
        else if (document.getElementById)
                html += 'var el = document.getElementById("' + elId + '");';
        html += 'el.style.visibility = ' + 'el.style.visibility == "hidden" ? "visible" : "hidden"';
        if (document.all || document.getElementById)
                setInterval(html,500);
}

function MiSubmit(ruta)
{
    if(ruta)
    {
           document.forms[0].action = ruta;
           document.forms[0].submit();
    }
}

function checkRutField(rut,valor_campo)
{
	var tmpstr = "";
	for ( i=0; i < rut.length ; i++ )
		if ( rut.charAt(i) != ' ' && rut.charAt(i) != '.' && rut.charAt(i) != '-' )
			tmpstr = tmpstr + rut.charAt(i);
	rut = tmpstr;
	largo = rut.length;

	elrut = valor_campo.value;
	document.forms[0].rut.value = elrut.substring(0, largo - 1);
	document.forms[0].dig.value = elrut.charAt(largo-1);
// [VARM+]
	tmpstr = "";
	for ( i=0; rut.charAt(i) == '0' ; i++ );
		for (; i < rut.length ; i++ )
			tmpstr = tmpstr + rut.charAt(i);
	rut = tmpstr;
	largo = rut.length;
// [VARM-]
	if ( largo < 2 )
	{
		alert("Debe ingresar el rut completo.");
		valor_campo.focus();
		valor_campo.select();
		return false;
	}
	for (i=0; i < largo ; i++ )
	{
		if ( rut.charAt(i) != "0" && rut.charAt(i) != "1" && rut.charAt(i) !="2" && rut.charAt(i) != "3" && rut.charAt(i) != "4" && rut.charAt(i) !="5" && rut.charAt(i) != "6" && rut.charAt(i) != "7" && rut.charAt(i) !="8" && rut.charAt(i) != "9" && rut.charAt(i) !="k" && rut.charAt(i) != "K" )
		{
			alert("El valor ingresado no corresponde a un R.U.T valido.");
			
			valor_campo.select();
			valor_campo.focus();
			return false;
		}
	}
	var invertido = "";
	for ( i=(largo-1),j=0; i>=0; i--,j++ )
		invertido = invertido + rut.charAt(i);
	var drut = "";
	drut = drut + invertido.charAt(0);
	drut = drut + '-';
	cnt = 0;
	for ( i=1,j=2; i<largo; i++,j++ )
	{
		if ( cnt == 3 )
		{
			drut = drut + '.';
			j++;
			drut = drut + invertido.charAt(i);
			cnt = 1;
		}
		else
		{
			drut = drut + invertido.charAt(i);
			cnt++;
		}
	}
	invertido = "";
	for ( i=(drut.length-1),j=0; i>=0; i--,j++ )
		invertido = invertido + drut.charAt(i);
	valor_campo.value = invertido;
	if ( checkDV(rut,valor_campo) )
		return true;
	this.focus();
	return false;
}
function checkDV( crut ,valor_campo)
{
	largo = crut.length;
	if ( largo < 2 )
	{
		alert("Debe ingresar el rut completo.");
		valor_campo.focus();
		//valor_campo.select();
		return false;
	}
	if ( largo > 2 )
		rut = crut.substring(0, largo - 1);
	else
		rut = crut.charAt(0);
	dv = crut.charAt(largo-1);
	checkCDV( dv,valor_campo );
	if ( rut == null || dv == null )
		return 0;
	var dvr = '0';
	suma = 0;
	mul = 2;
	for (i= rut.length -1 ; i >= 0; i--)
	{
		suma = suma + rut.charAt(i) * mul;
		if (mul == 7)
			mul = 2;
		else
			mul++;
	}
	res = suma % 11;
	if (res==1)
		dvr = 'k';
	else if (res==0)
		dvr = '0';
	else
	{
		dvi = 11-res;
		dvr = dvi + "";
	}
	if ( dvr != dv.toLowerCase() )
	{
		alert("EL rut es incorrecto.");
		valor_campo.value = "";

		return false;
	}
	return true;
}
function checkCDV( dvr,valor_campo )
{
	dv = dvr + "";
	if ( dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k'  && dv != 'K')
	{
		alert("Debe ingresar un digito verificador valido.");
		valor_campo.focus();
		valor_campo.select();
		return false;
	}
	return true;
}
function esDigito(sChr){
	var sCod = sChr.charCodeAt(0);
	return ((sCod > 47) && (sCod < 58));
}
function valSep(oTxt){
	var bOk = false;
	bOk = bOk || ((oTxt.value.charAt(2) == "-") && (oTxt.value.charAt(5) == "-"));
	bOk = bOk || ((oTxt.value.charAt(2) == "/") && (oTxt.value.charAt(5) == "/"));
	return bOk;
}
function finMes(oTxt){
	var nMes = parseInt(oTxt.value.substr(3, 2), 10);
	var nRes = 0;
	switch (nMes){
		case 1: nRes = 31; break;
		case 2: nRes = 29; break;
		case 3: nRes = 31; break;
		case 4: nRes = 30; break;
		case 5: nRes = 31; break;
		case 6: nRes = 30; break;
		case 7: nRes = 31; break;
		case 8: nRes = 31; break;
		case 9: nRes = 30; break;
		case 10: nRes = 31; break;
		case 11: nRes = 30; break;
		case 12: nRes = 31; break;
	}
	return nRes;
}
function valDia(oTxt){
	var bOk = false;
	var nDia = parseInt(oTxt.value.substr(0, 2), 10);
	bOk = bOk || ((nDia >= 1) && (nDia <= finMes(oTxt)));
	return bOk;
}
function valMes(oTxt){
	var bOk = false;
	var nMes = parseInt(oTxt.value.substr(3, 2), 10);
	bOk = bOk || ((nMes >= 1) && (nMes <= 12));
	return bOk;
}
function valAno(oTxt){
	var bOk = true;
	var nAno = oTxt.value.substr(6);
	bOk = bOk && ((nAno.length == 2) || (nAno.length == 4));
	if (bOk){
		for (var i = 0; i < nAno.length; i++){
			bOk = bOk && esDigito(nAno.charAt(i));
		}
	}
	return bOk;
}
function valFecha(oTxt){
	var bOk = true;
	if (oTxt.value != ""){
		bOk = bOk && (valAno(oTxt));
		bOk = bOk && (valMes(oTxt));
		bOk = bOk && (valDia(oTxt));
		bOk = bOk && (valSep(oTxt));
		if (!bOk){
			alert("Fecha inválida");
			oTxt.value = "";
			oTxt.focus();
		}
	}
}
function ValidarFecha(Cadena){
	var Fecha= new String(Cadena.value)	// Crea un string
	var RealFecha= new Date()	// Para sacar la fecha de hoy
	// Cadena Año
	var Ano= new String(Fecha.substring(0,Fecha.indexOf("-")))
	// Cadena Mes
	var Mes= new String(Fecha.substring(Fecha.indexOf("-")+1,Fecha.lastIndexOf("-")))
	// Cadena Día
	var Dia= new String(Fecha.substring(Fecha.lastIndexOf("-")+1,Fecha.length))

	// Valido el año
	if (isNaN(parseInt(Ano)) || Ano.length<4 || parseFloat(Ano)<1900){
        	alert('Invalid Year'+Ano);Cadena.focus();
		return false
	}
	// Valido el Mes
	if (isNaN(parseInt(Mes)) || parseFloat(Mes)<1 || parseFloat(Mes)>12){
		alert('Invalid Month');Cadena.focus();
		return false
	}
	// Valido el Dia
	if (isNaN(parseInt(Dia)) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){
		alert('Invalid Day');Cadena.focus();
		return false
	}
	if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {
		if (Mes==2 && Dia > 28 || Dia>30) {
			alert('Día inválido')
			return false
		}
	}
	
  //para que envie los datos, quitar las  2 lineas siguientes
  //alert("Fecha correcta.")
  return true;
}
function creaFecha(obj_aa,obj_mm,obj_dd,campo){
	var nRes=0;

	aa = obj_aa.value;
	mm = obj_mm.value;
	dd = obj_dd.value;
	lafecha = aa+"-"+mm+"-"+dd;

	switch(mm){
		case "1":  nRes = 31; break;
		case "2":  nRes = 29; break;
		case "3":  nRes = 31; break;
		case "4":  nRes = 30; break;
		case "5":  nRes = 31; break;
		case "6":  nRes = 30; break;
		case "7":  nRes = 31; break;
		case "8":  nRes = 31; break;
		case "9":  nRes = 30; break;
		case "10": nRes = 31; break;
		case "11": nRes = 30; break;
		case "12": nRes = 31; break;
	}

	if ( !EsNumerico(aa) ) { alert("Los valores deben ser numericos AA");obj_aa.focus();return false; }
	if ( !EsNumerico(mm) ) { alert("Los valores deben ser numericos MM");obj_mm.focus();return false; }
	if ( !EsNumerico(dd) ) { alert("Los valores deben ser numericos DD");obj_dd.focus();return false; }
	if ( aa<1900 )        { alert("El ano debe ser mayor que 1900 ");             obj_aa.focus(); return false; }
	if ( mm<0 || mm>12)   { alert("El valor del mes debe estar entre 1 y 12");    obj_mm.focus(); return false;}
	if ( dd<0 || dd>nRes) { alert("El valor del dia debe estar entre 1 y "+nRes); obj_dd.focus(); return false;}

	str_mm = (mm<10)?"0"+mm:mm;
	str_dd = (dd<10)?"0"+dd:dd;
	lafecha = aa+"-"+str_mm+"-"+str_dd;

	//alert("La Fecha "+lafecha);
	campo.value=lafecha;
	return true;
}
function EsNumerico(Dato){
	var CadenaNumeros;
	var EsteCaracter;
	var Contador;
	Contador=0;
	Dato = Dato.toString();
	CadenaNumeros="0123456789";
	for(i=0; i < Dato.length; i++){
		EsteCaracter = Dato.substring(i,i+1);file:///home/eroman/desa/sysgset/js/libreria.js
		if (CadenaNumeros.indexOf(EsteCaracter) !=-1)
			Contador++;
	}
	if (Contador == Dato.length)
		return true;
	else
		return false;
}
function SelectAll(CheckBoxControl)
{
	if (CheckBoxControl.checked == true)
	{
		var i;
		for (i=0; i < document.forms[0].elements.length; i++)
		{
			if ((document.forms[0].elements[i].type == 'checkbox') &&
			(document.forms[0].elements[i].name.indexOf('ids') > -1))
			{
				document.forms[0].elements[i].checked = true;
			}
		}
	}
	else
	{
		var i;
		for (i=0; i < document.forms[0].elements.length; i++)
		{
			if ((document.forms[0].elements[i].type == 'checkbox') &&
			(document.forms[0].elements[i].name.indexOf('ids') > -1))
			{
				document.forms[0].elements[i].checked = false;
			}
		}
	}
}

function toggleLayer( whichLayer )
{
  var elem, vis;
  if( document.getElementById ) // this is the way the standards work
    elem = document.getElementById( whichLayer );
  else if( document.all ) // this is the way old msie versions work
      elem = document.all[whichLayer];
  else if( document.layers ) // this is the way nn4 works
    elem = document.layers[whichLayer];
  vis = elem.style;
  // if the style.display value is blank we try to figure it out here
  if(vis.display==''&&elem.offsetWidth!=undefined&&elem.offsetHeight!=undefined)
    vis.display = (elem.offsetWidth!=0&&elem.offsetHeight!=0)?'block':'none';
  vis.display = (vis.display==''||vis.display=='block')?'none':'block';
}

function toggleLayer2( whichLayer , imgSel,img1,img2)
{
	var elem, vis ,img_show;

	if( document.getElementById ) // this is the way the standards work
		elem = document.getElementById( whichLayer );
	else if( document.all ) // this is the way old msie versions work
		elem = document.all[whichLayer];
	else if( document.layers ) // this is the way nn4 works
		elem = document.layers[whichLayer];
	vis = elem.style;
	// if the style.display value is blank we try to figure it out here
	if(vis.display=='' && elem.offsetWidth!=undefined && elem.offsetHeight!=undefined)
		vis.display = (elem.offsetWidth!=0 && elem.offsetHeight!=0)?'block':'none';
	vis.display = (vis.display=='' || vis.display=='block')?'none':'block';

	img_show = document.getElementById( imgSel );
	img_show.src = (vis.display=='' || vis.display=='block')?img2:img1;
}

function checkLargo(objCampo,largo){

	if (objCampo.value.length > largo) { 
		alert('El largo del codigo excede al establecido, que correspode a '+largo+' caracteres.');
		objCampo.value="";
		setTimeout(function(){objCampo.focus();},10);
		//objCampo.focus();
		return true;
	} else {
		return false;
	}
}
function insertFields(section,valor,id_orden,campos){
	if (valor=="SI"){
		alert(valor+" ID ORDEN:"+id_orden+" Parametros:"+campos);
		makerequest("/includes/insertaParametros.php?id_orden="+id_orden+"&loscampos="+campos,section);
	}
}
function buscar_rut(elrut){
	makerequest("/includes/lasentidades.php?entidad=2&tipo_buscar=4&buscar_por="+elrut,"resultado_rut");
}
function buscar_sucursal(id_entidad){
	alert("Voy a buscar:"+id_entidad);
	makerequest("/includes/datos_entidad.php?entidad=3&id_entidad="+id_entidad,"suc_datos");
	return true;
}
