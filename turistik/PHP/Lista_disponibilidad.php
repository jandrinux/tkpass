<?php
    session_start();
    include("menus.php");
    $_Util=new Util;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>


<link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../../js/ui.core.js"></script>
<script type="text/javascript" src="../../js/ui.datepicker.js"></script>
<link type="text/css" href="../../css/demos.css" rel="stylesheet" />


<script type="text/javascript">
$(document).ready(function()
{   
    $("#datepicker1").datepicker({
        dateFormat: 'mm/yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,

        onClose: function(dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month, 1)));
        }
    });

    $("#monthPicker").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
    });
});
</script>

<style>
.ui-datepicker-calendar {
	display: none;
}
</style>

</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">
  <table width="482" border="0" cellpadding="0" cellspacing="0">  

	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Disponibilidad Excursiones</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    

  </table>
  
    

<div id="Layer1">

<!-- Aqui va el cuerpo -->

<form  name="form" method="post" onSubmit="lista_disponibilidad(); return false" action="">
<!-- 
<table width="700" border="0" cellspacing="0" >
  <tr>
    <td width="100" class="celdalazul_peque">Fecha Desde </td>
    <td width="100" class="celdalCELEST">     
	   <input type="text" value="<? echo date('d/m/Y');?>" id="datepicker" name="fecha_rango_1" class="seleccionTurist_180" maxlength="15"/>
    </td> 
    <td width="100" class="celdalazul_peque">Fecha Hasta</td>    
    <td width="250" class="celdalCELEST">	
		<input type="text" value="<? echo date('d/m/Y');?>" id="datepicker1" name="fecha_rango_2" class="seleccionTurist_180" maxlength="15" />
        <input align="right" name="aceptar" type="submit" class="BotonTurist_Azul" value="Aceptar" />   
    </td>   
  </tr>
</table>
-->

<table width="500" border="0" cellspacing="0" >
    <tr>
      <td width="100" class="celdalazul_peque">Mes</td>
      <td width="264" class="celdalCELEST">        
        <input  id="datepicker1" value="<?=date('m/Y')?>" class="seleccionTurist_180" readonly=""  name="fecha" />
      </td>
    </tr>
    <tr>
      <td class="celdalazul_peque" width="100">Excursiones</td>
      <td class="celdalCELEST" >     
      <select name="excursion" id="excursion" class="seleccionTurist_200">
          <option value="0" selected="">-- TODOS --</option>
    
            <?
                $miConexion = new ClaseConexion;      
                $miConexion->Conectar();
                $query=$miConexion->EjecutaConsulta(" SELECT CODIGO_EO, NOMBRE_EO FROM EXCURSION_ORIGEN WHERE
                                                      ESTADO_EO = 1 and EMPRESA = 'TURISTIK' ORDER BY NOMBRE_EO");
                while ($row = mysql_fetch_assoc($query))
                {
                    echo "<option value=".$row['CODIGO_EO']." >".$row['NOMBRE_EO']."</option>" ; 
                }
            
                mysql_free_result($query); 
                mysql_close();
            ?>
        </select> 
        <input align="right" name="aceptar" type="submit" class="BotonTurist_Azul" value="Aceptar" />  
    </tr>
</table>
</form>

<div id="resultado_listar"></div>

</div>
</body>
</html>