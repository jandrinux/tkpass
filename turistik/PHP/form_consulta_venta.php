<?php

include("menus.php"); 
$_Util=new Util;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Estadisticas de Ventas</title>
<head>
    <link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
	<link href="../../css/menu_small.css" rel="stylesheet" type="text/css">
	<link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
	<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
	<script type="text/javascript" src="../../js/ui.core.js"></script>
	<script type="text/javascript" src="../../js/ui.datepicker.js"></script>
	<link type="text/css" href="../../css/demos.css" rel="stylesheet" />
	<script type="text/javascript">
	$(function() {
		$("#datepicker").datepicker();
		$("#datepicker1").datepicker();
	});
    
        $(document).ready(function() {
	$("#botonExcel").click(function(event) {
	   alert("Exportando");
		$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
		$("#FormularioExportacion").submit();
});
});
	</script>
    

</head>

<body>


<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>
<div class="TituloPlomo" id="Layer3">

  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Estadisticas de Ventas</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    
  </table>
 
<div id="Layer1">
<div id="menuh">
        <ul>
           <li><a href="form_consulta_venta.php" id="primero">Detalle</a></li>
           <li><a href="form_estadistica_venta.php" >Resumen</a></li>
        </ul>
</div>


<form  name="formEstadistica" method="post" onSubmit="consulta_estadisticas(); return false" action="">
<table width="700" border="0" cellspacing="0" >
  <tr>
    <td width="100" class="celdalazul_peque">Fecha Venta Desde </td>
    <td width="100" class="celdalCELEST">     
	   <input type="text" value="<? echo date('d/m/Y');?>" id="datepicker" name="fecha_rango_1" class="seleccionTurist_180" maxlength="15"/>
    </td> 
    <td width="100" class="celdalazul_peque">Fecha Venta Hasta</td>    
    <td width="250" class="celdalCELEST">	
		<input type="text" value="<? echo date('d/m/Y');?>" id="datepicker1" name="fecha_rango_2" class="seleccionTurist_180" maxlength="15" />
    </td>   
  </tr>
  <tr>
    <td width="100" class="celdalazul_peque">Sucursal de Venta</td>
    <td width="100" class="celdalCELEST">
			<select name="sucursal" id="sucursal" class="seleccionTurist_180">
				<option value="0">TODAS</option>
				<?
				$miConexion = new ClaseConexion;
				
				$miConexion->Conectar();
                if ($_SESSION['cod_tusua'] != 9)
                {
                    $querySucursal=$miConexion->EjecutaSP("Consulta_Listar_Sucursales","'"."0"."'");//sucursalES
                }
                else
                {
                    $querySucursal=$miConexion->EjecutaSP("Consulta_Listar_Sucursales_Coordinadores", " '".$_SESSION['codigo_usu']."' ");//sucursalES
                }
							
					 while($rowSucursal = mysql_fetch_array($querySucursal)){ 
							echo '<option value="'.$rowSucursal["CODIGO_SUCURSAL"].'"'.$select.'>'.$rowSucursal['SUCURSAL'].'</option>';
					
					}  
				mysql_free_result($querySucursal); 
				mysql_close();
				?>
			</select>
   </td>
    

   <td class="celdalazul_peque">Sucursal Reserva</td> 
       <td width="150" class="celdalCELEST">
			<select name="sucursal_res" id="sucursal_res" class="seleccionTurist_180">
				<option value="0">TODAS</option>
				<?
				$miConexion = new ClaseConexion;
				
				$miConexion->Conectar();
                if ($_SESSION['cod_tusua'] != 9)
                {
                    $querySucursal=$miConexion->EjecutaSP("Consulta_Listar_Sucursales","'"."0"."'");//sucursalES
                }
                else
                {
                    $querySucursal=$miConexion->EjecutaSP("Consulta_Listar_Sucursales_Coordinadores", " '".$_SESSION['codigo_usu']."' ");//sucursalES
                }			
					 while($rowSucursal = mysql_fetch_array($querySucursal)){ 
							echo '<option value="'.$rowSucursal["CODIGO_SUCURSAL"].'"'.$select.'>'.$rowSucursal['SUCURSAL'].'</option>';
					
					}  
				mysql_free_result($querySucursal); 
				mysql_close();
				?>
			</select>
      </td>
      

   </tr>

     <tr>
    <td width="100" class="celdalazul_peque"></td>
    <td width="100" class="celdalCELEST">
   </td>
    

   <td class="celdalazul_peque">Forma Pago</td> 
       <td width="150" class="celdalCELEST">
			<select name="fop" id="fop"  class="seleccionTurist_180">
				<option value="0">TODAS</option>
                <option value="C">CREDITO</option>
                <option value="D">DEBITO</option>
                <option value="E">EFECTIVO</option>
                <option value="TC">TC SIN PRESENCIA</option>

			</select>
            <input type="submit"  class="BotonAzul2"  value="Consultar" />
      </td>
      

   </tr>
</table>
</form>
<div style="cursor:hand;">
    <form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
    <p id="botonExcel"  class="BotonAzul2">Generar Reporte <img src="export_to_excel.gif" class="botonExcel" />s</p>
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
    </form>
</div>

</body>
</html>
<?php

echo '<div id="resultado_listar" style=" PADDING-TOP: 10px; TEXT-ALIGN: left; PADDING-BOTTOM: 3px; FONT-FAMILY: Arial; FONT-SIZE: 8pt; left:20px; top:258px; width:1170px; height:330px;" >';
include('CONSULTA_venta.php');
echo '</div>';


?>
</div>