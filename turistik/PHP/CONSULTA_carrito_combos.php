<?
/**
 * ARCHIVO PERTENECE A COMBOS 
*/
include ("../functions/check_promociones.php");

$promociones = new Promociones(); 
$promociones_activas = $promociones->Consulta_promociones();  /// promociones disponibles

$query=$promociones->Consulta_excursiones($_SESSION['codigo_suc'], $_SESSION['codigo_usu']);
while($row = mysql_fetch_array($query))
{   
    $excursiones[] = $row['COD_EXCURSION']; 
    $cantidad_de_excursiones[$row['COD_EXCURSION']] = $row['CANTIDAD']; 
}


if (count($excursiones)>0)
{

//filtro de excursiones mandatorias

for($i=0; $i<count($promociones_activas); $i++)
{
    $id = $promociones_activas[$i]['id_promocion'];
    $cant = $promociones_activas[$i]['cant_excursiones'];
    

    if (count($excursiones) >= $cant)
    {
        
        $output = array_slice($excursiones, 0, $cant); // selecciona los 3 o 2 primeras excursiones de un arreglo

        $excursiones_check[$id] = $output;
  
        
        $excursiones_mandatorias = $promociones->Consulta_excursiones_mandatorias($id);
        $ex_mandatorias[$id] = $excursiones_mandatorias;  // ex mandatorias
        $total_ex_mandatorias = count($ex_mandatorias[$id]);
        $lista_promociones[$id] = array_intersect($ex_mandatorias[$id], $excursiones_check[$id]);
        

        
    
        if (count($lista_promociones[$id]) >= $total_ex_mandatorias)
        {
            $excursiones_promos = $promociones->Consulta_promocion_excursiones($id);

            //Hay que elimnar las excursiones mandatorias y comparar con el arreglo principal
            
           // $diff[$id] = array_diff($excursiones_promos, $excursiones_check[$id]);
            #ver que excursiones tengo en carrito y comparar

            $lista_promos[$id] = array_intersect($excursiones_promos, $excursiones_check[$id]); // excursiones que hacen combo
            
             if(count($lista_promos[$id])>= $cant)
             {
                $promociones_activas_2[] = $id;
             }
        }
    }   
}    



$lista = array_filter($lista_promos);

// ordena indices de arreglo
for($i=0; $i<count($promociones_activas_2); $i++)
{
     $id = $promociones_activas_2[$i];
     $lista_excursiones[$id]= array_merge($lista[$id]); 
}


//elimina arreglos vacios 
//lista_promociones arreglo que contiene como indice la promocion y las excursiones filtradas en el carrito
$lista_excursiones = array_filter($lista_excursiones);


## consulta nacionalidad unica
$nacionalidad = $promociones->Consulta_Nacionalidad($_SESSION['codigo_suc'], $_SESSION['codigo_usu']);

## consulta que promocion continua segun nacionalidad y sucursal
$lista_promociones_nacionalidad = $promociones->Consulta_promocion_suc_nac($_SESSION['codigo_suc'], $nacionalidad);

##filtro promociones seleccionadas por las seleccionadas por nacionalidad y sucursal
$promociones_activas_2 = array_intersect($lista_promociones_nacionalidad, $promociones_activas_2);

// ordena indices de arreglo
foreach($promociones_activas_2 as $pr)
{
     $promociones_activas_3[]= $pr; 
}


//comparar lugar de excursion
for($i=0; $i<count($promociones_activas_3); $i++)
{
     $id = $promociones_activas_3[$i];
     $cant = $promociones->Consulta_Cantidad_excursiones($_SESSION['codigo_suc'], $_SESSION['codigo_usu'], $id);
     $cant_excursiones[$id] = $cant;
}


//print_r($cant_excursiones);
// comparar la excursion filtrada con la cantidad de excursiones por el lugar de recogida ##########################
$j = 0;
for($i=0; $i<count($promociones_activas); $i++)
{
    $id = $promociones_activas[$i]['id_promocion'];
    $cant = $promociones_activas[$i]['cant_excursiones'];

    if($id == $promociones_activas_3[$j])
    {

        if ($cant <=  $cant_excursiones[$id])
        {
           $promociones_activas_4[] = $id;
        }
        $j++;
    }

}

$promociones_finales = array_unique($promociones_activas_4);


for($i=0; $i<count($promociones_finales); $i++)
{
    $id = $promociones_finales[$i];
    $suma_adt = 0;
    $suma_chd = 0;

    for($j=0; $j<count($excursiones_check[$id]); $j++)
    {
        $codigo_eo = $excursiones_check[$id][$j];

       
        $queryTP = $promociones->Consulta_cantidad_pasajeros($_SESSION['codigo_suc'], $_SESSION['codigo_usu'], $codigo_eo );
        while($rowTP = mysql_fetch_array($queryTP))
        {  
            $cantidad_pax = $rowTP['CANTIDAD'];
            $tipo_pasajero = $rowTP['TIPO_PASAJERO'];
            
            if ($tipo_pasajero == "ADT")
            {
                $suma_adt = $cantidad_pax + $suma_adt;
            }
            else
            {
                $suma_chd = $cantidad_pax + $suma_chd;
            }
        }

        $cantidad_excursiones[$id] = array("adt"=>$suma_adt,
                                           "chd"=>$suma_chd);

    }

}



for($i=0; $i<count($promociones_activas); $i++)
{
    $id = $promociones_activas[$i]['id_promocion'];
    $cant = $promociones_activas[$i]['cant_excursiones'];
    

    foreach($promociones_finales as $pf)
    {
        if ($id == $pf)
        {

    
            $cant_adulto = $cantidad_excursiones[$id]['adt'];
            $cant_nino = $cantidad_excursiones[$id]['chd'];
            
            $numero_promo_adt = $cant_adulto/$cant;
            $numero_promo_chd = $cant_nino/$cant;
            $cant_promo_adt = floor($cant_adulto/$cant);
            $cant_promo_chd = floor($cant_nino/$cant);
            
            $numero_de_promociones[$id] = array("adt"=>$numero_promo_adt,
                                                "chd"=>$numero_promo_chd);
                                                
    
            $cant_promociones[$id] = array("adt"=>$cant_promo_adt,
                                           "chd"=>$cant_promo_chd);
                                         
            
        }
    }
    


    
}




for($i=0; $i<count($promociones_finales); $i++)
{
   $id = $promociones_finales[$i];
   $excursiones_a_cobrar[$id] = array_diff($excursiones,$excursiones_check[$id]); 
}



for($i=0; $i<count($promociones_finales); $i++)
{
     $id = $promociones_finales[$i];
     $excursiones_a_cobrar[$id]= array_merge($excursiones_a_cobrar[$id]); 
}

$excursiones_a_cobrar = array_filter($excursiones_a_cobrar);



for($i=0; $i<count($promociones_finales); $i++)
{
    $id = $promociones_finales[$i];
    for($j=0; $j<count($excursiones_a_cobrar[$id]); $j++)
    {
       
       $codigo_eo = $excursiones_a_cobrar[$id][$j];
        
        $query = $promociones->Consulta_afecto_exento($_SESSION['codigo_suc'], $_SESSION['codigo_usu'],$codigo_eo );
        while($row = mysql_fetch_array($query))
        {   

            if ($row['TIPO_PASAJERO'] == "ADT")
            {
                $af = $row['VALOR_EXENTO']; //valor_afecto
                $ex = $row['PRECIO_EO'] - $row['VALOR_EXENTO'] ;//valor_exento
                $suma_afecto_af = $af + $suma_afecto_af;
                $suma_exento_ex = $ex + $suma_exento_ex;
                $precio_eoo = $row['PRECIO_EO'];
                $suma_precio_ex = $precio_eoo + $suma_precio_ex;
 
            }         
            else
            {
                $af = 0; //valor_afecto
                $ex = $row['VALOR_CHILD'];//valor_exento
                $suma_afecto_af = $af + $suma_afecto_af;
                $suma_exento_ex = $ex + $suma_exento_ex;
                $precio_eoo = $row['VALOR_CHILD'];
                $suma_precio_ex = $precio_eoo + $suma_precio_ex;

            }
            
                $total_precio2[$id] = array ('total'=>$suma_precio_ex,
                                            'afecto'=>$suma_afecto_af,
                                            'exento'=>$suma_exento_ex);
                                            
               // print_r($total_precio2);
        }//end while
    }  
}
//print_r($total_precio2);
// calcular los montos de las excursiones adt y chd

for($i=0; $i<count($promociones_finales); $i++)
{

        $id = $promociones_finales[$i];
        $c_adt = $cant_promociones[$id]['adt'];
        $c_chd = $cant_promociones[$id]['chd'];
        $suma_precio_adt = 0;
        $suma_precio_chd = 0;
        
        for($j=0; $j<count($excursiones_check[$id]); $j++)
        {
            $flag = false;
            $codigo_eo = $excursiones_check[$id][$j];

                $query = $promociones->Consulta_afecto_exento($_SESSION['codigo_suc'], $_SESSION['codigo_usu'],$codigo_eo );
                while($row = mysql_fetch_array($query))
                {        
                   
                    if ($row['TIPO_PASAJERO'] == "ADT" and $flag == false)
                    {
                            $precio = $row['PRECIO_EO'];
                            $suma_precio_adt = $precio + $suma_precio_adt;
                            $flag = true;
         
                    }         
                    else
                    {
                        if ($row['TIPO_PASAJERO'] != "ADT")
                        {
                            $precio = $row['VALOR_CHILD'];
                            $suma_precio_chd = $precio + $suma_precio_chd;

                        }

                    }
                    

                }//end while
                $precios_totales[$id] = array ('suma_adt'=>$suma_precio_adt,
                                               'suma_chd'=>$suma_precio_chd);
                                               
                //print_r($precios_totales);

        }     

}

$adt_entero = false;
$chd_entero = false;

for($i=0; $i<count($promociones_finales); $i++)
{

        $id = $promociones_finales[$i];
       // $c_adt = $numero_de_promociones[$id]['adt'];
       // $c_chd = $numero_de_promociones[$id]['chd'];
        $c_adt = $cant_promociones[$id]['adt'];
        $c_chd = $cant_promociones[$id]['chd'];

        
        $suma_precio_eo = 0;
        $suma_afecto = 0;
        $suma_exento = 0;
        
        if( is_int($c_adt ) || $c_adt == 0)
        {
            $adt_entero = true;
        }
        if( is_int($c_chd ) || $c_chd == 0 )
        {
            $chd_entero = true;
        }
        
        for($j=0; $j<count($excursiones_check[$id]); $j++)
        {
            $cont_adt = 1;
            $cont_chd = 1;
            //$flag = false;
            $codigo_eo = $excursiones_check[$id][$j];

            if ( ($adt_entero == false or $chd_entero == false ) or ($adt_entero == true and $chd_entero == false) or ($adt_entero == false and $chd_entero == true) )
            {
                $query = $promociones->Consulta_afecto_exento($_SESSION['codigo_suc'], $_SESSION['codigo_usu'],$codigo_eo );
                while($row = mysql_fetch_array($query))
                {   
                    
                        if ($row['TIPO_PASAJERO'] == "ADT")
                        {
                            
                               if ($cont_adt>$c_adt)
                               {
                               // echo $codigo_eo."- ".$cont_adt.">".$c_adt."<br>";
                                    $af = $row['VALOR_EXENTO']; //valor_afecto
                                    $ex = $row['PRECIO_EO'] - $row['VALOR_EXENTO'] ;//valor_exento
                                    $suma_afecto = $af + $suma_afecto;
                                    $suma_exento = $ex + $suma_exento;
                                    $precio_eo = $row['PRECIO_EO'];
                                    $suma_precio_eo = $precio_eo + $suma_precio_eo; 
                                    
                               } 
                               else
                               {
                                    $cont_adt++;
                               }

                        }
                    
                    
                        if ($row['TIPO_PASAJERO'] == "CHD")
                        {
                           if ($cont_chd>$c_chd)
                           {
                                $af = 0; //valor_afecto
                                $ex = $row['VALOR_CHILD'];//valor_exento
                                $suma_afecto = $af + $suma_afecto;
                                $suma_exento = $ex + $suma_exento;
                                $precio_eo = $row['VALOR_CHILD'];
                                $suma_precio_eo = $precio_eo + $suma_precio_eo;
                           } 
                           else
                           {
                                 $cont_chd++;
                           }

                        }


                    
                    
    
    /*
                    if ($row['TIPO_PASAJERO'] == "ADT" and $flag == true)
                    {
                        $af = $row['VALOR_EXENTO']; //valor_afecto
                        $ex = $row['PRECIO_EO'] - $row['VALOR_EXENTO'] ;//valor_exento
                        $suma_afecto = $af + $suma_afecto;
                        $suma_exento = $ex + $suma_exento;
                        $precio_eo = $row['PRECIO_EO'];
                        $suma_precio_eo = $precio_eo + $suma_precio_eo;
         
                    }         
                    else
                    {
                        if ($row['TIPO_PASAJERO'] != "ADT")
                        {
                            $af = 0; //valor_afecto
                            $ex = $row['VALOR_CHILD'];//valor_exento
                            $suma_afecto = $af + $suma_afecto;
                            $suma_exento = $ex + $suma_exento;
                            $precio_eo = $row['VALOR_CHILD'];
                            $suma_precio_eo = $precio_eo + $suma_precio_eo;
               
                        }
                        else
                        {
                            $flag = true;
                        }
                    }
     */               
    
                }//end while
                $total_precio[$id] = array ('total'=>$suma_precio_eo,
                                            'afecto'=>$suma_afecto,
                                            'exento'=>$suma_exento);
                                            
                                            
            } 
            else
            {
                $total_precio[$id] = array ('total'=>0,
                                            'afecto'=>0,
                                            'exento'=>0);
            }
            
            //print_r($total_precio);

        }     

}

//print_r($total_precio);

for($i=0; $i<count($promociones_finales); $i++)
{
     $id = $promociones_finales[$i];
     $total_suma = $total_precio[$id]['total'] + $total_precio2[$id]['total'];
     $total_suma_afecto = $total_precio[$id]['afecto'] + $total_precio2[$id]['afecto'];
     $total_suma_exento = $total_precio[$id]['exento'] + $total_precio2[$id]['exento'];
     
     
     $total_promocion[$id] = array ('total'=>$total_suma,
                                    'afecto'=>$total_suma_afecto,
                                    'exento'=>$total_suma_exento);
}



//print_r($total_promocion);
if (count($promociones_finales) > 0)
{

        $i=1;
        $chk[$i] = 'checked';
        echo "<table align='left' cellspacing='0' cellpadding='3' border='0'   style='border:1px solid #FFF; color:#000; width:950px;  background:#F1F2FF; '>";
        echo "<tr style='background:#CCC;' font-size=11px;'>"; 
        echo "<td style='font-weight: bold;' class='celdalazul_peque' width='120px' align='center'>LISTA DE COMBOS</td>";
        echo "<td style='font-weight: bold;' class='celdalazul_peque' align='center'>DESCRIPCION</td>";
        echo "</tr>";        
        foreach ($promociones_finales as $promo)
        {
                $afecto_total_combo = 0;
                $exento_total_combo = 0;

                $miConexion= new ClaseConexion;
                $miConexion->Conectar();
                $sql = "SELECT ID_PROMO, NOMBRE,
                        DESCRIPCION, COMBO, 
                        AFECTO_COMBO_ADT, EXENTO_COMBO_ADT,
                        AFECTO_COMBO_CHD, EXENTO_COMBO_CHD, COMISION
                        FROM  PROMOCIONES
                        WHERE ID_PROMO = ".$promo." ";
                                  
                $query=$miConexion->EjecutaConsulta($sql);
                    while($row = mysql_fetch_array($query))
                    {   
                        $id_promo = $row['ID_PROMO'];
                       

                        $afecto_combo_adt = $row['AFECTO_COMBO_ADT']*$cant_promociones[$promo]['adt'];
                        $exento_combo_adt = $row['EXENTO_COMBO_ADT']*$cant_promociones[$promo]['adt'];
                        //echo $cant_promociones[$promo]['adt'];
                        
                        $afecto_combo_chd = $row['AFECTO_COMBO_CHD']*$cant_promociones[$promo]['chd'];
                        $exento_combo_chd = $row['EXENTO_COMBO_CHD']*$cant_promociones[$promo]['chd'];
                        $comision = $row['COMISION'];
                        
                        $total_combo_adt = $row['AFECTO_COMBO_ADT'] + $row['EXENTO_COMBO_ADT'];
                        $total_combo_chd = $row['AFECTO_COMBO_CHD'] + $row['EXENTO_COMBO_CHD'];
                        
                        $afecto_total_combo = $afecto_combo_adt + $afecto_combo_chd;
                        $exento_total_combo = $exento_combo_adt + $exento_combo_chd;
                        
                        $promocion =   array("id_promo"=>$id_promo,
                                             "monto_afecto"=>$afecto_total_combo,
                                             "monto_exento"=>$exento_total_combo);
                                   
                    
                     echo "<tr  font-size=11px;'>"; 
                        echo "<td style='font-weight: bold;' class='celdalPasajeros' width='120px' >";
                        echo "<input type='radio' name='promocion_activada' value='' id='promocion_activada'  onclick='combos(".$total_promocion[$id_promo]['afecto'].", ".$total_promocion[$id_promo]['exento'].", ".$row['COMBO'].", ".$afecto_total_combo.", ".$exento_total_combo.", ".$id_promo.", ".$total_promocion[$id_promo]['total'].", ".$comision.", ".$total_combo_adt.", ".$total_combo_chd.")' >";
                        echo $row['NOMBRE'];
                       // echo $total_promocion[$id_promo]['afecto'].", ".$total_promocion[$id_promo]['exento'].", ".$row['COMBO'].", ".$afecto_total_combo.", ".$exento_total_combo.", ".$id_promo.", ".$total_promocion[$id_promo]['total'].", ".$comision.", ".$total_combo_adt.", ".$total_combo_chd;
                        echo "</td>";
                     echo "<td style='font-weight: bold;' class='celdalPasajeros' >".$row['DESCRIPCION']."</td>";
                     echo "</tr>";
                     
                     $i++;
                    }
                mysql_free_result($query); 
                mysql_close(); 
        }
         echo "</table>";
}

} //end (count($excursiones)
//print_r($promocion);

include ("../functions/tkpass.php");
include ("../functions/check_descuentos.php");
include("../functions/consulta_descuento.php");
$miConexion = new ClaseConexion;
$ObjTotalRedondeado = new BuscaDescuentos();

        
$j = 1;
$cant_pasajeros = 0;


echo "<table align='left' cellspacing='0' cellpadding='3' border='0'   style='border:1px solid #FFF; margin-top:20px;  color:#000; width:950px;  background:#F1F2FF; '>";
    echo "<tr style='background:#CCC;' font-size=11px;'>"; 
    echo "<td style='font-weight: bold;' class='celdalazul_peque' align='center'>N&#176;</td>";
    echo "<td style='font-weight: bold;' class='celdalazul_peque' align='center'>Pasajero</td>";
    echo "<td style='font-weight: bold;' class='celdalazul_peque' align='center'>Servicio</td>";
    echo "<td style='font-weight: bold;' class='celdalazul_peque' align='center'>Lugar de Recogida</td>";
    echo "<td style='font-weight: bold;' class='celdalazul_peque' align='center'>Fecha Servicio</td>";
    echo "<td style='font-weight: bold;' class='celdalazul_peque' align='center'>Tipo Pax</td>";
    echo "<td style='font-weight: bold;' class='celdalazul_peque' align='center'>Valor</td>";
    echo "<td style='font-weight: bold;' class='celdalazul_peque' align='center'>Eliminar</td>";
    echo "<tr>";  
   
    
    $miConexion->conectar();
    $Query = $miConexion->EjecutaSP("Consulta_Pax_Carrito_by_Excursion", " ".$_SESSION['codigo_suc'].", ".$_SESSION["codigo_usu"].", 1 ");
    $num_excursiones = mysql_num_rows($Query);
    
    while($row=mysql_fetch_array($Query))
    {

            
        $miConexion->conectar();
        $QuerySelect = $miConexion->EjecutaSP("Consulta_Temporal_Carrito", " ".$row['COD_EXCURSION'].", ".$_SESSION['codigo_suc'].", ".$_SESSION["codigo_usu"].", '".$row['FECHA']."' ");
        $total_pax = mysql_num_rows($QuerySelect);
            
            while($row_temporal_reserva=mysql_fetch_array($QuerySelect))
            {
                
           
                if($row_temporal_reserva['PRECIO_RE'] != 0)
                {
                    
                   $valor_pax = $row_temporal_reserva['PRECIO_RE'];
                   if($row_temporal_reserva['TIPO_PASAJERO'] == "ADT" )
                    {
                        $cant_pasajeros++;
                        $monto_total_af= $row_temporal_reserva['VALOR_EXENTO'] + $monto_total_af;
                         //$total_afecto = $row_temporal_reserva['VALOR_EXENTO'] + $total_afecto;
                    }

      
                   	if($orden%2==0)
                	   $color='#D3D5FF';
                	else
                	   $color='#F1F2FF';
                    		
                	echo "<tr style='background-color=".$color."; font-size=11px;'>"; 
                    echo "<td class='celdalPasajeros' align='center'>".$j."</td>";
                    echo "<td class='celdalPasajeros'>".$row_temporal_reserva['PASAJERO']." ".$row_temporal_reserva['APASAJERO']."</td>";
                	echo "<td class='celdalPasajeros' >".htmlentities($row_temporal_reserva['NOMBRE_EO'])."</td>";
                    echo "<td class='celdalPasajeros' >".htmlentities($row_temporal_reserva['DESCRIPCION_HL'])."</td>";
                    echo "<td class='celdalPasajeros' align='center'>".$row_temporal_reserva['FECHA']."</td>";
                	echo "<td class='celdalPasajeros' align='center'>".$row_temporal_reserva['TIPO_PASAJERO']."</td>";
                    echo "<td class='celdalPasajeros' align='left'>$ ".number_format($row_temporal_reserva['PRECIO_RE'])."</td>";
                    echo "<td class='celdalPasajeros' align='center'><a style=\"text-decoration:underline;cursor:pointer;\" onclick=\"return eliminar_servicio('".$row_temporal_reserva['ID_TEMPORAL']."',2, '".$row_temporal_reserva['SERIAL_TTS']."')\"><img src='../Imagenes/cancel.png' border='0' /></a></td>";
                    echo "<tr>";

                    $sub = $row_temporal_reserva['PRECIO_RE'] + $sub;
                    $precio_ex = $valor_pax + $precio_ex;
                    
                    $j++;

                }

            }
            mysql_free_result($QuerySelect); 
            mysql_close();
            

                echo "<tr>";
                    echo "<tr style='background-color=".$color."; font-size=11px;'>"; 
                    echo "<td class='celdalazul_peque' align='center'></td>";

                	echo "<td class='celdalazul_peque' ></td>";
                    echo "<td class='celdalazul_peque' ></td>";
                    echo "<td class='celdalazul_peque' ></td>";
                    echo "<td class='celdalazul_peque'></td>";
                    echo "<td class='celdalazul_peque' >Total : </td>";
                    echo "<td class='celdalazul_peque'>$ ".number_format($precio_ex)." </td>";
                    echo "<td class='celdalazul_peque' align='center'></td>";
                echo "</tr>";

               $precio_ex = 0;

    }
    mysql_free_result($Query); 
    mysql_close();
    echo "</tr>";
    $total = $sub + $total;
    $total_exento = $total - $monto_total_af;
echo "</table>";


?>

<div id="resumen_promocion">
<table align='left'  cellspacing='0' cellpadding='3' style='border:1px solid #FFF; width: 250px; margin-left:700px; margin-top: 0px;  background:#F1F2FF; '>

  <tr >
    <td style="font-weight: bold;" width="150px" class='celdalPasajeros' >Sub Total: </td>
    <td style="font-weight: bold;" width="100px" class='celdalPasajeros'><? echo "$ ". number_format($sub) ?></td>
    <input type="hidden" value="<?=$sub?>" id="subtotal" />
  </tr>
  <tr >
    <td style="font-weight: bold;" class='celdalPasajeros' >Total Afecto Turistik: </td>
    <td style="font-weight: bold;" class='celdalPasajeros'><? echo "$ ". number_format($monto_total_af) ?></td>
     <input type="hidden" id="valor_afecto_normal" value="<? echo $monto_total_af ?>" />
  </tr>
  <tr >
    <td style="font-weight: bold;" class='celdalPasajeros' >Total Exento: </td>
    <td style="font-weight: bold;" class='celdalPasajeros'><? echo "$ ". number_format($total_exento) ?></td>
    <input type="hidden" id="valor_exento_normal" value="<? echo $total_exento ?>" />
  </tr>

   <tr id="total_tarjeta" >
    <td style="font-weight: bold;" class='celdalazul_peque' >Total Tarjeta :</td>
    <td style="font-weight: bold;" class='celdalazul_peque'>
         <input type="text" style='background-color: #010356; border-width:0; COLOR: #FFF; FONT-FAMILY: Arial; FONT-SIZE: 8pt; font-weight: bold;'  readonly="" id="total_tarjeta" value="<? echo $total ?>"/>
    </td>
  </tr>
  <div id="total" >
  <tr id="total_efectivo">
    <td style="font-weight: bold;" width="180px" class='celdalazul_peque' >Total Efectivo :</td>
    <td style="font-weight: bold;" class='celdalazul_peque'>
    <?
      $total_redondeado = $ObjTotalRedondeado->RedondearValor($total);
    ?>
    <input type="text" readonly="" id="tot_efectivo" value="<?="$ ". number_format($total_redondeado)?>" style="background-color: #010356; border-width:0; color:#FFF; FONT-SIZE: 8pt; font-weight: bold;  FONT-FAMILY: Arial; align:center;"/>
    </td>
  </tr>
  </div>
</table>
</div>

<!-- Si es un combo la opcion checkeada se muestran el div -->
<div id="resumen_combo" style="display:none">
<table align='left'  cellspacing='0' cellpadding='3' style='border:1px solid #FFF; width: 250px; margin-left:700px; margin-top: 0px;  background:#F1F2FF; '>

  <tr >
    <td style="font-weight: bold;" width="150px" class='celdalPasajeros' >Sub Total: </td>
    <td style="font-weight: bold;" width="100px" class='celdalPasajeros'><? echo "$ ". number_format($sub) ?></td>
    <input type="hidden" value="<?=$sub?>" id="subtotal" />
  </tr>
  <tr >
    <td style="font-weight: bold;" width="150px" class='celdalPasajeros' >Dscto Combo: </td>
    <td style="font-weight: bold;" width="100px" class='celdalPasajeros'>
    <input type="text" style='background-color: #F1F2FF; border-width:0; COLOR: #000; FONT-FAMILY: Arial; FONT-SIZE: 8pt; font-weight: bold;' name="dscto_combo"  readonly="" id="dscto_combo" value=""/>
    
    </td>
  </tr>
  <tr >
    <td style="font-weight: bold;" class='celdalPasajeros' >Total Afecto Turistik: </td>
    <td style="font-weight: bold;" class='celdalPasajeros'>
    <input type="text" style='background-color: #F1F2FF; border-width:0; COLOR: #000; FONT-FAMILY: Arial; FONT-SIZE: 8pt; font-weight: bold;'  readonly="" id="total_afecto" value="<? echo $total_afecto ?>"/>
    <input type="hidden" id="valor_afecto" value="<? echo $monto_total_af ?>" />
    </td>
  </tr>
  <tr >
    <td style="font-weight: bold;" class='celdalPasajeros' >Total Exento: </td>
    <td style="font-weight: bold;" class='celdalPasajeros'>
    <input type="text" style='background-color: #F1F2FF; border-width:0; COLOR: #000; FONT-FAMILY: Arial; FONT-SIZE: 8pt; font-weight: bold;'  readonly="" id="total_exento" value="<? echo $total_exento ?>"/>
    <input type="hidden" id="valor_exento" value="<? echo $total_exento ?>" />
    </td>
  </tr>

   <tr id="total_tarjeta" >
    <td style="font-weight: bold;" class='celdalazul_peque' >Total Efectivo :</td>
    <td style="font-weight: bold;" class='celdalazul_peque'>
         <input type="text" style='background-color: #010356; border-width:0; COLOR: #FFF; FONT-FAMILY: Arial; FONT-SIZE: 8pt; font-weight: bold;'  readonly="" id="tot_tarjeta" value=""/>
    </td>
  </tr>
  <input type="hidden" name="id_combo" id="id_combo" />
  <input type="hidden" name="id_promo" id="id_promo" />
  <input type="hidden" name="lista_promo" id="lista_promo" value="<? echo htmlspecialchars(serialize($excursiones_check))?>" />
  <input type="hidden" name="cant_promo" id="cant_promo" value="<? echo htmlspecialchars(serialize($cant_promociones))?>" />
  <input type="hidden" name="comision_promocion" id="comision_promocion" />
  <input type="hidden" name="total_combo_adt" id="total_combo_adt" />
  <input type="hidden" name="total_combo_chd" id="total_combo_chd" />
  <input type="hidden" name="precios_totales" id="precios_totales" value="<? echo htmlspecialchars(serialize($precios_totales))?>" />
 
  

    
  
</table>
</div>
