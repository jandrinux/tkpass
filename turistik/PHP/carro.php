<?
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
session_start();
if ($_SESSION["autentificado"] != "SI"){
    header("location: Login.php");
}

require ("set_timeout.php");
include ("../Clases/ClaseConexion.inc.php");
include ("../functions/consulta_promociones.php");
require ("../Clases/ClaseUtil.inc.php");
//include("../Clases/class.descuento_ninguno.php");


$miConexion = new ClaseConexion();
$consulta_promociones = new Consulta_Promociones();
$opt_chkd = $consulta_promociones->Consulta_promocion($_GET['opt']);
$bol = $consulta_promociones->Mostar_Boleta($_SESSION['codigo_suc']);
$bolTTK = $consulta_promociones->Mostar_BoletaTTK($_SESSION['codigo_suc']);
$result = $consulta_promociones->Consulta_opcion_Checked($opt_chkd);


if ($opt_chkd[3]== 'checked')
{
    $id_ninguno = "id='primero'";
}
if ($opt_chkd[1]== 'checked')
{
    $id_combo= "id='primero'"; // es combo
}
if ($opt_chkd[0]== 'checked')
{
    $id_descuento = "id='primero'";
}
if ($opt_chkd[2]== 'checked')
{
    $id_beneficio = "id='primero'";
}
if ($opt_chkd[4]== 'checked')
{
    $id_promocion = "id='primero'";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<title> </title>

 <meta charset="utf-8" />
   <link rel="stylesheet" href="../../css/plantilla_carrito.css" type="text/css" media="all">
    <link href="../estilos1.css" rel="stylesheet" type="text/css">
<!--
    <link href="../../css/menu_small.css" rel="stylesheet" type="text/css">
-->
    <script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <? include ('js_ver_carrito.php'); ?>
    
</head>

<body onload="calcular_descuento();">

 <div id="global">
 

     <div id="cabecera">
    
          <table width="482" border="0" cellpadding="0" cellspacing="0">  
        	<tr>
              <td width="80" rowspan="4"><img src="../Imagenes/carrito.png" width="80" height="80" /></td>
              <td width="13">&nbsp;</td>
              <td width="389">&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><strong id="TituloPlomo"><b>Carrito de Compras</b></strong></td>
            </tr>
 
        
          </table>
    
     </div>
      
     <div id="navegacion">
        <ul>
           <li><a href="ver_carrito.php?opt=4" name="radio" <?=$id_ninguno?> >NINGUNO</a></li>
           <li><a href="ver_carrito.php?opt=2" name="radio" <?=$id_combo?> >COMBOS</a></li>
           <li><a href="ver_carrito.php?opt=1" name="radio" <?=$id_descuento?> >DESCUENTOS</a></li>
           <li><a href="ver_carrito.php?opt=3" name="radio" <?=$id_beneficio?> >BENEFICIOS</a></li>
           <li><a href="ver_carrito.php?opt=5" name="radio" <?=$id_promocion?> >PROMOCIONES</a></li>
        </ul>
     </div><!--cierre navegacion -->
     
     

     
    <form name="formCompra"  action="finaliza_compra.php"   method="post" enctype="multipart/form-data">
    
     <div id="principal">
    
        <?
            if ($opt_chkd[1] == 'checked')    
            {     
                include('CONSULTA_carrito_combos.php');        
            }
            if ($opt_chkd[0] == 'checked')  
            {
                include('CONSULTA_carrito.php');            // Descuentos 1
            }
            if ($opt_chkd[2] == 'checked')
            { 
                include('CONSULTA_carrito_beneficio.php');  // Beneficio cliente 3
            }
            if ($opt_chkd[3] == 'checked') // ninguno
            { 
                include('CONSULTA_carrito_ninguno.php');  
            }
            if ($opt_chkd[4] == 'checked') // ninguno
            { 
                include('CONSULTA_carrito_promociones.php');  
            }
         
        ?>
        
        <div id="combo_dscto" style="margin-left: 798px; margin-bottom: 10px;">
            <select name="dscto_cordinador" id="dscto_cordinador" class="seleccionTurist_180" onchange="recalcular_porcentaje(this);">
                <option value="100" >NINGUNO</option>
                <option value="90">10</option>
                <option value="80">20</option>
                <option value="70">30</option>
                <option value="60">40</option>
                <option value="50">50</option>
                <option value="40">60</option>
                <option value="30">70</option>
                <option value="20">80</option>
                <option value="10">90</option>
                <option value="0" >100</option>
            </select>
        </div>
        

        <fieldset id="fieldset_beneficio" style="display: none;">
        <legend class="celdalLeyend">Beneficio</legend>
             <table align="center" id="tabla" width="770" border="0" cellpadding="0" cellspacing="0">
             <tr>
                <td class="celdalazul_peque">Numero de Telefono:(*)</td>
                <td class="celdalCELEST">
                <input name="telefono" id="telefono"  class="seleccionTurist_180" type="text" onkeypress="return validarP3(event);"/>
                </td>
                <td class="celdalazul_peque" width="130">Rut:(*)</td>
                <td class="celdalCELEST">
                <input name="rut" id="rut"  class="seleccionTurist_180" type="text" onkeypress="return isNumberKey(event);" placeholder="Ingrese rut" />
                </td>
                <td></td>
                <td></td>
             </tr>
             </table>
        </fieldset>

        <fieldset id="fieldset">
        <legend class="celdalLeyend">Forma de Pago</legend>
         <table align="center" id="tabla" width="770" border="0" cellpadding="0" cellspacing="0">
         <tr>
            <td class="celdalazul_peque">Tipo Reserva: (*)</td>
            <td class="celdalCELEST">
                <select name="tipoReserva" required="" title="Seleccione tipo de reserva" id="tipoReserva" class="seleccionTurist_180" >
                    <?
        
                        $miConexion->Conectar();
                        $queryTipoConsulta=$miConexion->EjecutaSP("Consulta_Tipo_Reserva_Sucursal"," '".$_SESSION["codigo_suc"]."', $result");
                        while ($rowTipoConsulta = mysql_fetch_assoc($queryTipoConsulta)){
                             echo "<option value=".$rowTipoConsulta['CODIGO_TR']." >".htmlentities($rowTipoConsulta['TRESERVA'])."</option>" ;
                        }
                        mysql_free_result($queryTipoConsulta); 
                        mysql_close();
        
                    ?>
                
                </select>
            </td>
            <td class="celdalazul_peque">Forma de Pago: (*)</td>
            <td class="celdalCELEST">
                <select name="fop" required="" title="Seleccione forma de pago" id="select_fop" class="seleccionTurist_180" onchange="info_boleta_tr();">
                    <option value="S" selected="">-- SELECCIONE --</option>
                    <?
                     
                        $miConexion->Conectar();
                        $queryTipoConsulta=$miConexion->EjecutaSP("Consulta_Tipo_Reserva_Fop_Sucursal"," '".$_SESSION["codigo_suc"]."', 1 ");
                        while ($row = mysql_fetch_assoc($queryTipoConsulta)){
                             echo "<option value=".$row['ID_FOP']." >".$row['Fop']."</option>" ;
                        }
                        mysql_free_result($queryTipoConsulta); 
                        mysql_close();
                      
        
        
                    ?>
        
                </select>
            </td>
            <td></td>
            <td></td>
         </tr>
          <tr >
            <td class="celdalazul_peque"> <? if ($_SESSION["codigo_suc"] == 7) { echo "Confirme Tipo Res.(*)"; } ?>  </td>
            <td class="celdalCELEST">
            <? if ($_SESSION["codigo_suc"] == 7) { ?>
                <select name="confirmaTipo" required="" title="Seleccione tipo de reserva" id="confirmaTipo" class="seleccionTurist_180"  >
                   <option value="0" selected="">Seleccione</option>
                    <?
        
                        $miConexion->Conectar();
                        $queryTipoConsulta=$miConexion->EjecutaSP("Consulta_Tipo_Reserva_Sucursal"," '".$_SESSION["codigo_suc"]."' , $result");
                        while ($rowTipoConsulta = mysql_fetch_assoc($queryTipoConsulta)){
                             echo "<option value=".$rowTipoConsulta['CODIGO_TR']." >".htmlentities($rowTipoConsulta['TRESERVA'])."</option>" ;
                        }
                        mysql_free_result($queryTipoConsulta); 
                        mysql_close();
                        
        
        
                    ?>
                
                </select>
             <? } ?>
            </td>
            
            <td class="celdalazul_peque"><div id="bolTTS">Boleta TTS: (*)</div> </td>
            <td class="celdalCELEST"><input name="boletaTTS" id="boletaTTS"  onkeypress="return validarP3(event);" class="seleccionTurist_180" type="text" value="<?=$bol?>" /></td>
            <td></td>
            <td></td>
        
         </tr>
         <tr >
            <td class="celdalazul_peque"><div id="titulo_mayorista" style="visibility: hidden;" >Mayorista (*):</div></td>
            <td class="celdalCELEST" >
            <select name="nombre_mayorista" id="nombre_mayorista" class="seleccionTurist_180" style="visibility: hidden;" onchange="Busca_Codigo();">
                <option value="0" selected="">Seleccione</option>
                    <?
                        $miConexion->Conectar();
                        $query=$miConexion->EjecutaSP("Consulta_Canal_Venta");
                        while ($rowTipoConsulta = mysql_fetch_assoc($query)){
                             echo "<option value=".$rowTipoConsulta['CODID']." >".htmlentities($rowTipoConsulta['CANAL'])."</option>" ;
                        }
                        mysql_free_result($query); 
                        mysql_close();
                    ?>
                </select>
            </td>
        
         
            <td class="celdalazul_peque"><div id="bolTD">Boleta Turistik: (*)</div> </td>
            <td  class="celdalCELEST"><input name="boletaTTK" id="boletaTTK" required="" onkeypress="return validarP3(event);" class="seleccionTurist_180" type="text" value="<?=$bolTTK?>"/></td>
            <td></td>
            <td></td>
        
         </tr>
         <tr>
            <td class="celdalazul_peque"><div id="tituloTBK" style="visibility: hidden;" >C&oacute;digo Autorizaci&oacute;n: (*)</div></td>
            <td class="celdalCELEST" >
                <input name="voucherTBK" id="info_boleta" style="visibility: hidden;"  class="seleccionTurist_180" type="text"/>
            </td>
            <td class="celdalazul_peque"><div id="tituloCodSocio" style="visibility: hidden;" >Buscar C&oacute;d. Socio</div>
            </td>	
            <td class="celdalCELEST">
        
                <img src="../Imagenes/edit_group.png" width="22" height="22" id="img_socio" onclick="busca_codigo()" alt="Consulta Codigo Vendedor" />
                <input name="codSocio" readonly=""  type="text"  id="codSocio" size="4" />
        
            </td>     
            <td></td>
            <td></td>
         </tr>
          <tr>
            <td class="celdalazul_peque"><div id="titulo4Ultimos">4 Ultimos Digitos Tarjeta: (*)</div></td>
            <td class="celdalCELEST" >
            <input name="ultimos_digitos" id="ultimos_digitos"  onkeypress="return validarP3(event);"  class="seleccionTurist_180" type="text" maxlength="4"/>
            </td>
            <td class="celdalazul_peque"><div id="tituloTipoTarjeta">Tipo de Tarjeta: (*)</div></td>	
            <td class="celdalCELEST">
                <select name="tipo_tarjeta" required="" title="Seleccione Tarjeta" id="tipo_tarjeta" class="seleccionTurist_180" >
                <option value="" selected>-- SELECCIONE --</option>
                    <?
                    echo "<option value='MASTERCARD'>MASTERCARD</option>";
                    echo "<option value='VISA' >VISA</option>";
                    echo "<option value='AMERICAN EXPRESS'>AMERICAN EXPRESS</option>";
                    echo "<option value='OTRO'>OTRO</option>";
                    ?>
                </select>
            </td>     
            <td></td>
            <td></td>
         </tr>
        
         
         <? if ($_SESSION["codigo_suc"] == 7 || $_SESSION["codigo_suc"] == 15 ) { ?>
          <tr>
            <td class="celdalazul_peque">Respaldo Reserva (*):</td>
            <td class="celdalCELEST" colspan="5"><input type="file" id="archivo" name="archivo" /></td>
         </tr>
         <? } ?>
        </table>
        
        </fieldset>
        
     </div>
     
     <div class="anuncios1">
    
    
     </div> <!--cierre anuncios -->
     
     
     
     
     <div id="pie">


        <input name="tipoUsuario" id="tipoUsuario" type="hidden" value="<? echo $_SESSION["codigo_suc"]; ?>" />
        <input  type="button"  class="BotonTurist_largo" value="Vaciar carrito" onclick="vaciar_carrito()"/>
        <input type="button" name="continuar" id="continuar" class="BotonTurist_largo" onclick="carrito();" value="Seguir comprando" />
        <input type="button" name="comprar" class="BotonTurist_largo" onclick="finalizar()" value="Finalizar compra"/>
        <input type="hidden" name="radio" id="radio" value="<? echo $result; ?>" />
        <input type="hidden" name="estado" id="estado"/>
        <input type="hidden" name="estado_ttk" id="estado_ttk"/>
        <input name="boletaTTSCorrelativa" id="boletaTTSCorrelativa"  type="hidden" value="<?=$bol?>" />
        <input name="boletaTTKCorrelativa" id="boletaTTKCorrelativa"  type="hidden" value="<?=$bolTTK?>" />
        <input type="hidden" id="tipo_venta" name="tipo_venta" /> 
        <input type="hidden" id="tipo_mayorista" name="tipo_mayorista" value="0" /> 
        <input type="hidden" id="dscto_especial" name="dscto_especial" value="<?=$descuento_especial?>" /> 


     </div>
     
  </form>
 
 </div> <!-- cierre global-->
</body>
</html>