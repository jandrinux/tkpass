<?php
//ob_end_clean();
require('../Clases/fpdf153/fpdf.php');
require ("../Clases/ClaseConexion.inc.php");
require ("../Clases/class.pdf.php");
require ("../Clases/ClaseUtil.inc.php");

ob_end_clean();


$miConexion= new ClaseConexion;
$_Util=new Util;
$miConexion->Conectar();

//variables para generar las columnas 
$column_name = "";
$column_numerotxt = "";
$column_monto = "";
$column_comision = "";
$column_cobrototal = "";
$column_cobrototalfinal = "";
$column_emisor= "";
$column_rut= "";
$column_direccion= "";
$column_ciudad= "";
$column_region= "";
$column_comuna= "";
$column_dv= "";
$column_descripeventos= "";
$column_fechaingresoeventos= "";
$column_fechacierreeventos= "";
$column_modocobroeventos= "";
$column_cantidadeventos= "";
$column_valorfijoeventos= "";
$column_monedaeventos= "";
$column_valortotaleventos= "";
$column_nombrecomercios= "";
$column_comerciosdescripcion="";
$total = 0;

$pdf=new FPDF();

$pdf->Open();
$pdf->AddPage();


$pdf->SetFont('Arial','B',16);
$pdf->SetTextColor(0);
$pdf->SetY(20);
$pdf->Cell(0,-20,'Confirmacion de Activacion');
//$pdf->SetX(1000);
$pdf->Image("../Imagenes/turistik107_118.jpg",150,10,30,0,0);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0);
$pdf->SetY(55);
$pdf->Cell(0,-20,$fecha);

$Y_Fields_Name_positionE = 20;//30
$Y_Table_PositionE = 50;//10

$pdf->SetY($Y_Fields_Name_positionE);

$pdf->SetFillColor(211,213,254);
$pdf->Cell(35,8,'Distribuidor',1,0,'L',1); //ANCHO CELDA
$pdf->SetFillColor(255,255,255);
$pdf->SetFont('Arial','',10);
$pdf->Cell(85,8,'aaa',1,0,'L',1);

$pdf->Ln();

$pdf->SetFillColor(211,213,254);
$pdf->SetFont('Arial','',10);
$pdf->Cell(35,8,'ID Tarjeta',1,0,'L',1); //ANCHO CELDA
$pdf->SetFillColor(255,255,255);
$pdf->SetFont('Arial','',10);
$pdf->Cell(85,8,$column_rut.'-'.$column_dv,1,0,'L',1);
$pdf->SetFillColor(102,153,204);
$pdf->SetFont('Arial','',10);


$pdf->Ln();


$pdf->SetFillColor(211,213,254);
$pdf->SetFont('Arial','',10);
$pdf->Cell(35,16,'Pasajero',1,0,'L',1); 

$pdf->SetFillColor(255,255,255);
$pdf->SetFont('Arial','',10);
$pdf->Cell(85,16,$_GET['tarjetas'],1,0,'L',1);
$pdf->SetFillColor(102,153,204);
$pdf->SetFont('Arial','',10);


$pdf->Ln();


$pdf->SetFillColor(211,213,254);
$pdf->SetFont('Arial','',10);
$pdf->Cell(35,8,'Precio',1,0,'L',1); //ANCHO CELDA
$pdf->SetFillColor(255,255,255);
$pdf->SetFont('Arial','',10);
$pdf->Cell(85,8,$column_ciudad,1,0,'L',1);
$pdf->Ln();


$pdf->SetFillColor(211,213,254);
$pdf->SetFont('Arial','',10);
$pdf->Cell(35,8,'Estado',1,0,'L',1); //ANCHO CELDA
$pdf->SetFillColor(255,255,255);
$pdf->SetFont('Arial','',10);
$pdf->Cell(85,8,$UF,1,0,'L',1);

$pdf->Ln();

$pdf->SetFillColor(211,213,254);
$pdf->SetFont('Arial','',10);
$pdf->Cell(35,24,'Firma Cajero',1,0,'L',1); //ANCHO CELDA
$pdf->SetFillColor(255,255,255);
$pdf->SetFont('Arial','',10);
$pdf->Cell(85,24,'',1,0,'L',1);

$pdf->Ln();

$pdf->SetFillColor(211,213,254);
$pdf->SetFont('Arial','',10);
$pdf->Cell(35,24,'Firma Pasajero',1,0,'L',1); //ANCHO CELDA
$pdf->SetFillColor(255,255,255);
$pdf->SetFont('Arial','',10);
$pdf->Cell(85,24,'',1,0,'L',1);

$pdf->Ln();


$pdf->Output();

?> 