<?
include("menus.php");
$miConexion = new ClaseConexion;
$_Util=new Util;
$usuario=$_SESSION['usuario'];



?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
<script type="text/javascript">
jQuery(function($) { 
$.datepicker.regional['es'] =  
  {  
    clearText: 'Borra',  
    clearStatus: 'Borra fecha actual',  
    closeText: 'Cerrar',  
    closeStatus: 'Cerrar sin guardar',  
    prevStatus: 'Mostrar mes anterior',  
    prevBigStatus: 'Mostrar año anterior',  
    nextStatus: 'Mostrar mes siguiente',  
    nextBigStatus: 'Mostrar año siguiente',  
    currentText: 'Hoy',  
    currentStatus: 'Mostrar mes actual',  
    monthNames:  ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],  
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],  
    monthStatus: 'Seleccionar otro mes',  
    yearStatus: 'Seleccionar otro año',  
    weekHeader: 'Sm',  
    weekStatus: 'Semana del año',  
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],  
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],  
    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],  
    dayStatus: 'Set DD as first week day',  
    dateStatus: 'Select D, M d',  
    dateFormat: 'dd/mm/yy',  
    firstDay: 1,  
    initStatus: 'Seleccionar fecha',  
    isRTL: false  
  };  

  $.datepicker.setDefaults($.datepicker.regional['es']); 
});
$(document).ready(function()
{   
    $(".date-picker").datepicker({
        dateFormat: 'mm/yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,

        onClose: function(dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month, 1)));
        }
    });

    $(".monthPicker").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
    });
});

</script>
<style>
.ui-datepicker-calendar {
	display: none;
}
</style>
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
</br>
</br>
</br>
<table width="493" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="80" rowspan="4"><img src="../Imagenes/IMG_6902.jpg" width="80" height="80" /></td>
    <td width="13">&nbsp;</td>
    <td width="400">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><strong>Consultar Cantidades de Excursiones </strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="TituloFecha">&nbsp;</td>
    <td height="22" class="TituloFecha">&nbsp;</td>
  </tr>
</table>
<p>&nbsp;</p>
</br>
<div id="Layer3" style=" left:11px; top:150px; width:805px; height:61px; z-index:3">
  <form name="form1" method="post" action="">
    <table width="352" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="96" class="celdalazul">Excursion</td>
        <td width="256" class="celdalCELEST"><?PHP 
		$SQL  ="SELECT EO.NOMBRE_EO, EO.CODIGO_EO from EXCURSION_ORIGEN EO ";
		$SQL .="WHERE EO.ESTADO_EO = 1 ORDER BY EO.NOMBRE_EO ASC ";
		$miConexion->Conectar();
		$QUERY=$miConexion->EjecutaConsulta($SQL);
		?>
          <select    name="ex" id="ex" style="font-family:Arial; font-size:10px;">
            <option value="0"> ---TODAS LAS EXCURSIONES--- </option>
            <? while($row=mysql_fetch_array($QUERY)) {?>
            <option value="<?=$row[1]?>">
            <?=$row[0]?>
            </option>
            <? }?>
          </select></td>
      </tr>
      <tr>
        <td class="celdalazul">Fecha</td>
        <td class="celdalCELEST"><input name="fecha" id="fecha" class="date-picker" />
          <input name="consulta" type="submit" class="BotonTurist_Azul" id="consulta2" value="Consultar"  /></td>
      </tr>
    </table>
  </form>
</div>
</br>
</br>
</br>
<? 
if(!isset($_POST['fecha'])) {
	exit();
	
}
function getUltimoDiaMes($elAnio,$elMes) {
  return date("d",(mktime(0,0,0,$elMes+1,1,$elAnio)-1));
}

$f = explode("/", $_POST['fecha']);
//defino fecha 1 
$ano1 = $f[1]; 
$mes1 = $f[0]; 
$dia1 = 1; 
$diaU = getUltimoDiaMes($ano1,$mes1); 
/*
if(date("m/Y") ==$_POST['fecha'])
{
 $diaU = date("d");
}
*/
//defino fecha 2 
$ano2 = $f[1]; 
$mes2 = $f[0]; 
$dia2 =$diaU; 
//calculo timestam de las dos fechas 
$timestamp1 = mktime(0,0,0,$mes1,$dia1,$ano1); 
$timestamp2 = mktime(0,0,0,$mes2,$dia2,$ano2); 
//resto a una fecha la otra 
$segundos_diferencia = $timestamp1 - $timestamp2; 
//echo $segundos_diferencia; 
//convierto segundos en días 
$dias_diferencia = ($segundos_diferencia / (60 * 60 * 24)) *-1 ; 
$dias_diferencia += 1;
$datax = $_POST['ex'];
$sq2 = " RE.CODIGO_EO = ".$datax." AND ";
if($datax ==0)
{
$sq2 ="";
}

$SQL  =
"SELECT EO.NOMBRE_EO, EO.CODIGO_EO from RESERVA_EXCURSION RE , EXCURSION_ORIGEN EO
WHERE RE.FECHA_RE 
BETWEEN '".$ano1."-".$mes1."-01' AND '".$ano2."-".$mes2."-".$dia2."' 
AND RE.COD_CIERRE = 2 AND ".$sq2."
RE.CODIGO_EO = EO.CODIGO_EO
GROUP BY RE.CODIGO_EO
ORDER BY EO.NOMBRE_EO ASC";

$miConexion->Conectar();
$QUERY2=$miConexion->EjecutaConsulta($SQL);

?>
<div  style="float:left; width:<? echo $dias_diferencia*60+600; ?>px;">
  <div style="float:left;font-size:13px; position:relative;">
    <ul>
      <li style="height:30px;" class="celdaAZJ" >Fecha Inicial: <?="01/".$mes1."/".$ano1." Hasta: ".$dia2."/".$mes2."/".$ano2.""?></li>
      <? 	 while($row2=mysql_fetch_array($QUERY2)) { 
	  		$DiasC[] = array("Excursion" => $row2[1], "Cantidad"=>0);
	  
	  ?>
      <li class="celdaAZJ">
        <?=$row2[0]?>
      </li>
      <? } ?>
         <li style=" height:30px;" class="celdaAZJ" >TOTALES EXCURSIONES</li> 
    </ul>
  </div>
  

  
  <? for($i=1;$i<$dias_diferencia+1;$i++) 
    { 
	?>
  <div style="float:left;  width:40px;background-color:#D3D5FE; font-size:13px; text-align:center;">
    <ul style="list-style:none; font-weight:bold; "  >
      <li style=" height:30px; " class="celdaAZJ" ><?=$i?></li>
      <?  
	  		$PRI  ="SELECT EO.NOMBRE_EO, EO.CODIGO_EO from RESERVA_EXCURSION RE , EXCURSION_ORIGEN EO
		    WHERE RE.FECHA_RE 
		    BETWEEN '".$ano1."-".$mes1."-01' AND '".$ano2."-".$mes2."-".$dia2."' 
		    AND RE.COD_CIERRE = 2 AND ".$sq2."
		    RE.CODIGO_EO = EO.CODIGO_EO
		    GROUP BY RE.CODIGO_EO
		    ORDER BY EO.NOMBRE_EO ASC";
			$miConexion->Conectar();
			$RS=$miConexion->EjecutaConsulta($PRI);
		    $DiasReales = array();
		    $sumt=0;
					while($rt=mysql_fetch_array($RS)) 
					{ 
					 
							$SQL  ="SELECT EO.NOMBRE_EO, EO.CODIGO_EO,count(1) as CANTIDAD from RESERVA_EXCURSION RE , EXCURSION_ORIGEN EO
							WHERE RE.FECHA_RE 
							BETWEEN '".$ano1."-".$mes1."-".$i."' AND '".$ano2."-".$mes2."-".$i."'
							AND RE.COD_CIERRE = 2 AND EO.CODIGO_EO=".$rt[1]."
							AND EO.CODIGO_EO = RE.CODIGO_EO
							GROUP BY RE.CODIGO_EO
							ORDER BY EO.NOMBRE_EO ASC";
								$s=0;
							$miConexion->Conectar();
							$QUERY3=$miConexion->EjecutaConsulta($SQL);
							if(strlen($i) ==1)
							{
							 $i = "0".$i;
							}
							
							$fecha1 = $ano1."-".$mes1."-".$i;
							$fecha2 = $i."/".$mes1."/".$ano1;
							
							while($row3=mysql_fetch_array($QUERY3)) { 
							
								for($ii = 0; $ii <= count($DiasC); $ii++)
								{
									if($DiasC[$ii]['Excursion'] == $row3[1])
									{
										$DiasC[$ii]['Cantidad']+=1;
										
									}
								}
								if($NIVEL_USUARIO != "3") { 
									$dx = "";
								
								}
								echo "<li class=\"celdacelesteJ\"><a href=\"Detalle_Reservas".$dx.".php?u=".base64_encode($USUARIO)."&df=".base64_encode($fecha1)."&f=".base64_encode($fecha2)."&ex=".base64_encode($row3["CODIGO_EO"])."\" target=\"_blank\">".$row3[2]."</a></li>";
								$s++;
								$sumt+=$row3[2] ;
							}
							if($s==0) echo "<li class=\"celdacelesteJ\">-</li>";
								
    				  }?>
                   <li style=" height:30px;" class="celdaAZJ" ><?=$sumt?></li>   
    </ul>
  </div>
  <? }
  



  
  
$SQL  =
"SELECT EO.NOMBRE_EO, EO.CODIGO_EO, count(1), EO.PRECIO_EO * count(1) from RESERVA_EXCURSION RE , EXCURSION_ORIGEN EO
WHERE RE.FECHA_RE 
BETWEEN '".$ano1."-".$mes1."-01' AND '".$ano2."-".$mes2."-".$dia2."' 
AND RE.COD_CIERRE = 2 AND ".$sq2."
RE.CODIGO_EO = EO.CODIGO_EO
GROUP BY RE.CODIGO_EO
ORDER BY EO.NOMBRE_EO ASC";
  
  ?>
  <div style="float:left;  width:100px;background-color:#D3D5FE; font-size:13px; text-align:center;">
    <ul style="list-style:none; font-weight:bold; "  >
      <li style=" height:30px;" class="celdaAZJ" >Totales</li>
      <?
		$miConexion->Conectar();
		$QUERY2=$miConexion->EjecutaConsulta($SQL);
		$s=0;
		while($row3=mysql_fetch_array($QUERY2)) { 
		 $s+=$row3[2];
		?>
      <li class="celdacelesteJ">
        <?=$row3[2]?>
      </li>
      <? } ?>
      <li style=" height:30px;" class="celdaAZJ" ><?=$s?></li> 
    </ul>
  </div>
  <div style="float:left;  width:100px;background-color:#D3D5FE; font-size:13px; text-align:center;">
    <ul style="list-style:none; font-weight:bold; "  >
      <li style=" height:30px;" class="celdaAZJ" >Promedio Exc.</li>
      <?
		$miConexion->Conectar();
		$QUERY2=$miConexion->EjecutaConsulta($SQL);
		while($row3=mysql_fetch_array($QUERY2)) { 
		  $diasServReales=0;
			foreach($DiasC as $v)
			{
				if($v["Excursion"] == $row3[1])
				{
					$diasServReales = $v["Cantidad"];
					//echo"<l1>J: ".$diasServReales."</li>";
				}
			
			}
		
		
		?>
      <li class="celdacelesteJ">
        <?=round($row3[2]/$diasServReales,2)?>
      </li>
      <? } ?>
       <li style=" height:30px;" class="celdaAZJ" >---</li> 
    </ul>
  </div>
  <div style="float:left;  width:100px;background-color:#D3D5FE; font-size:13px; text-align:center;">
    <ul style="list-style:none; font-weight:bold; "  >
      <li style=" height:30px;" class="celdaAZJ" >Promedio Ing.</li>
      <?

		$miConexion->Conectar();
		$QUERY2=$miConexion->EjecutaConsulta($SQL);
		$suma = 0;
		while($row3=mysql_fetch_array($QUERY2)) { 
		  $diasServReales=0;
		  $suma+=$row3[3];
		  foreach($DiasC as $v)
			{
				if($v["Excursion"] == $row3[1])
				{
					$diasServReales = $v["Cantidad"];
					//echo"<l1>J: ".$diasServReales."</li>";
				}
			
			}
		?>
      <li class="celdacelesteJ">
        <?=number_format(round($row3[3]/$diasServReales,0), 0, ',', '.')?>
      </li>
      <? } ?>
       <li style=" height:30px;" class="celdaAZJ" ><?=number_format($suma, 0, ',', '.')?></li> 
    </ul>
  </div>
</div>

<form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
<button class="celdaAZJ" style="height: 30px;width: 215px;" id="excel">Exportar a Excel</pButton>
<input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form>

<script language="javascript">
$(document).ready(function() {
     $("#excel").click(function(event) {
     $("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
     $("#FormularioExportacion").submit();
});
});
</script>
<style>
	.celdacelesteJ {
		BORDER-BOTTOM: silver 1px solid;
		BORDER-LEFT: white 1px solid;
		BORDER-RIGHT: silver 1px solid;
		BORDER-TOP: white 1px solid;
			font-size: x-small;
	
		font-weight: bold;
		
		}
	.celdaAZJ {
		background-color:#010356;color:#FFF;
  		BORDER: silver 1px solid;
		font-size: x-small;
		color: #FFFFFF;
		font-weight: bold;
   
		
		}
</style>

<div id="oculto" style="display: none;">
<? 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if(!isset($_POST['fecha'])) {
	exit();
	
}


$f = explode("/", $_POST['fecha']);
//defino fecha 1 
$ano1 = $f[1]; 
$mes1 = $f[0]; 
$dia1 = 1; 
$diaU = getUltimoDiaMes($ano1,$mes1); 
/*
if(date("m/Y") ==$_POST['fecha'])
{
 $diaU = date("d");
}
*/
//defino fecha 2 
$ano2 = $f[1]; 
$mes2 = $f[0]; 
$dia2 =$diaU; 
//calculo timestam de las dos fechas 
$timestamp1 = mktime(0,0,0,$mes1,$dia1,$ano1); 
$timestamp2 = mktime(0,0,0,$mes2,$dia2,$ano2); 
//resto a una fecha la otra 
$segundos_diferencia = $timestamp1 - $timestamp2; 
//echo $segundos_diferencia; 
//convierto segundos en días 
$dias_diferencia = ($segundos_diferencia / (60 * 60 * 24)) *-1 ; 
$dias_diferencia += 1;
$datax = $_POST['ex'];
$sq2 = " RE.CODIGO_EO = ".$datax." AND ";
if($datax ==0)
{
$sq2 ="";
}

$SQL  =
"SELECT EO.NOMBRE_EO, EO.CODIGO_EO from RESERVA_EXCURSION RE , EXCURSION_ORIGEN EO
WHERE RE.FECHA_RE 
BETWEEN '".$ano1."-".$mes1."-01' AND '".$ano2."-".$mes2."-".$dia2."' 
AND RE.COD_CIERRE = 2 AND ".$sq2."
RE.CODIGO_EO = EO.CODIGO_EO
GROUP BY RE.CODIGO_EO
ORDER BY EO.NOMBRE_EO ASC";

$miConexion->Conectar();
$QUERY2=$miConexion->EjecutaConsulta($SQL);

?>
<table id="Exportar_a_Excel" style="float:left; width:<? echo $dias_diferencia*35+600; ?>px;">
  <td style="float:left;font-size:13px; position:relative;">
    <ul>
      <li style="height:30px;" class="celdaAZJ" >Fecha Inicial: <?="01/".$mes1."/".$ano1." Hasta: ".$dia2."/".$mes2."/".$ano2.""?></li>
      <? 	 while($row2=mysql_fetch_array($QUERY2)) { 
	  		$DiasC[] = array("Excursion" => $row2[1], "Cantidad"=>0);
	  
	  ?>
      <li class="celdaAZJ" >
        <?=$row2[0]?>
      </li>
      <? } ?>
         <li style=" height:30px;" class="celdaAZJ" >TOTALES EXCURSIONES</li> 
    </ul>
  </td>
  

  
  <? for($i=1;$i<$dias_diferencia+1;$i++) 
    { 
	?>
  <div style="float:left;  width:40px;background-color:#D3D5FE; font-size:13px; text-align:center;">
    <td style="list-style:none; font-weight:bold;"  >
      <li style=" height:30px;" class="celdaAZJ" ><?=$i?></li>
      <?  
	  		$PRI  ="SELECT EO.NOMBRE_EO, EO.CODIGO_EO from RESERVA_EXCURSION RE , EXCURSION_ORIGEN EO
		    WHERE RE.FECHA_RE 
		    BETWEEN '".$ano1."-".$mes1."-01' AND '".$ano2."-".$mes2."-".$dia2."' 
		    AND RE.COD_CIERRE = 2 AND ".$sq2."
		    RE.CODIGO_EO = EO.CODIGO_EO
		    GROUP BY RE.CODIGO_EO
		    ORDER BY EO.NOMBRE_EO ASC";
			$miConexion->Conectar();
			$RS=$miConexion->EjecutaConsulta($PRI);
		    $DiasReales = array();
		    $sumt=0;
					while($rt=mysql_fetch_array($RS)) 
					{ 
					 
							$SQL  ="SELECT EO.NOMBRE_EO, EO.CODIGO_EO,count(1) as CANTIDAD from RESERVA_EXCURSION RE , EXCURSION_ORIGEN EO
							WHERE RE.FECHA_RE 
							BETWEEN '".$ano1."-".$mes1."-".$i."' AND '".$ano2."-".$mes2."-".$i."'
							AND RE.COD_CIERRE = 2 AND EO.CODIGO_EO=".$rt[1]."
							AND EO.CODIGO_EO = RE.CODIGO_EO
							GROUP BY RE.CODIGO_EO
							ORDER BY EO.NOMBRE_EO ASC";
								$s=0;
							$miConexion->Conectar();
							$QUERY3=$miConexion->EjecutaConsulta($SQL);
							if(strlen($i) ==1)
							{
							 $i = "0".$i;
							}
							
							$fecha1 = $ano1."-".$mes1."-".$i;
							$fecha2 = $i."/".$mes1."/".$ano1;
							
							while($row3=mysql_fetch_array($QUERY3)) { 
							
								for($ii = 0; $ii <= count($DiasC); $ii++)
								{
									if($DiasC[$ii]['Excursion'] == $row3[1])
									{
										$DiasC[$ii]['Cantidad']+=1;
										
									}
								}
								if($NIVEL_USUARIO != "3") { 
									$dx = "";
								
								}
								echo "<li class=\"celdacelesteJ\">".$row3[2]."</li>";
								$s++;
								$sumt+=$row3[2] ;
							}
							if($s==0) echo "<li class=\"celdacelesteJ\">-</li>";
								
    				  }?>
                   <li style=" height:30px;" class="celdaAZJ" ><?=$sumt?></li>   
    </td>
  </div>
  <? }
  



  
  
$SQL  =
"SELECT EO.NOMBRE_EO, EO.CODIGO_EO, count(1), EO.PRECIO_EO * count(1) from RESERVA_EXCURSION RE , EXCURSION_ORIGEN EO
WHERE RE.FECHA_RE 
BETWEEN '".$ano1."-".$mes1."-01' AND '".$ano2."-".$mes2."-".$dia2."' 
AND RE.COD_CIERRE = 2 AND ".$sq2."
RE.CODIGO_EO = EO.CODIGO_EO
GROUP BY RE.CODIGO_EO
ORDER BY EO.NOMBRE_EO ASC";
  
  ?>
  <div style="display:none;float:left;  width:100px;background-color:#D3D5FE; font-size:13px; text-align:center;">
    <ul style="list-style:none; font-weight:bold; "  >
      <li style=" height:30px;" class="celdaAZJ" >Totales</li>
      <?
		$miConexion->Conectar();
		$QUERY2=$miConexion->EjecutaConsulta($SQL);
		$s=0;
		while($row3=mysql_fetch_array($QUERY2)) { 
		 $s+=$row3[2];
		?>
      <li class="celdacelesteJ">
        <?=$row3[2]?>
      </li>
      <? } ?>
      <li style=" height:30px;" class="celdaAZJ" ><?=$s?></li> 
    </ul>
  </div>
  <div style="display:none;float:left;  width:100px;background-color:#D3D5FE; font-size:13px; text-align:center;">
    <ul style="list-style:none; font-weight:bold; "  >
      <li style=" height:30px;" class="celdaAZJ" >Promedio Exc.</li>
      <?
		$miConexion->Conectar();
		$QUERY2=$miConexion->EjecutaConsulta($SQL);
		while($row3=mysql_fetch_array($QUERY2)) { 
		  $diasServReales=0;
			foreach($DiasC as $v)
			{
				if($v["Excursion"] == $row3[1])
				{
					$diasServReales = $v["Cantidad"];
					//echo"<l1>J: ".$diasServReales."</li>";
				}
			
			}
		
		
		?>
      <li class="celdacelesteJ">
        <?=round($row3[2]/$diasServReales,2)?>
      </li>
      <? } ?>
       <li style=" height:30px;" class="celdaAZJ" >---</li> 
    </ul>
  </div>
  <div style="display:none; float:left;  width:100px;background-color:#D3D5FE; font-size:13px; text-align:center;">
    <ul style="list-style:none; font-weight:bold; "  >
      <li style=" height:30px;" class="celdaAZJ" >Promedio Ing.</li>
      <?

		$miConexion->Conectar();
		$QUERY2=$miConexion->EjecutaConsulta($SQL);
		$suma = 0;
		while($row3=mysql_fetch_array($QUERY2)) { 
		  $diasServReales=0;
		  $suma+=$row3[3];
		  foreach($DiasC as $v)
			{
				if($v["Excursion"] == $row3[1])
				{
					$diasServReales = $v["Cantidad"];
					//echo"<l1>J: ".$diasServReales."</li>";
				}
			
			}
		?>
      <li class="celdacelesteJ">
        <?=number_format(round($row3[3]/$diasServReales,0), 0, ',', '.')?>
      </li>
      <? } ?>
       <li style=" height:30px;" class="celdaAZJ" ><?=number_format($suma, 0, ',', '.')?></li> 
    </ul>
  </div>
</table>


</div>
