<?php

include("menus.php"); 
$_Util=new Util;
ini_set('display_errors', 0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Estadisticas de Ventas</title>
<head>
    <link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
	<link href="../../css/menu_small.css" rel="stylesheet" type="text/css">
	<link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
	<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
	<script type="text/javascript" src="../../js/ui.core.js"></script>
	<script type="text/javascript" src="../../js/ui.datepicker.js"></script>
	<link type="text/css" href="../../css/demos.css" rel="stylesheet" />
	<script type="text/javascript">
	$(function() {
		$("#datepicker").datepicker();
		$("#datepicker1").datepicker();
	});
    
        $(document).ready(function() {
	$("#botonExcel").click(function(event) {
	   alert("Exportando");
		$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
		$("#FormularioExportacion").submit();
});
});
	</script>
    

</head>

<body>


<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>
<div class="TituloPlomo" id="Layer3">
  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr height="50"><td><br></td></tr>
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Reservas PP - Pagadas y Pendientes</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    
	<tr><td><br><br><br></td></tr>
  </table>



<form  name="formEstadistica" method="GET"  action="ReportesPP.php">
<input type="hidden" id="buscar" name="buscar" value="true" />
<table width="700" height="52" border="0" cellspacing="0" >
  <tr>
    <td width="100" class="celdalazul_peque">Fecha Venta Desde </td>
    <td width="100" class="celdalCELEST">     
	   <input type="text" value="<? 
       if($_GET['buscar']==true) 
            echo $_GET['fecha_rango_1'];
       else
            echo date('d/m/Y');
 ?>" id="datepicker" name="fecha_rango_1" class="seleccionTurist_180" maxlength="15"/>
    </td> 
    <td width="100" class="celdalazul_peque">Fecha Venta Hasta</td>    
    <td width="250" class="celdalCELEST">	
		<input type="text" value="<? 
        if($_GET['buscar']==true) 
            echo $_GET['fecha_rango_2'];
       else
            echo date('d/m/Y');
        ?>" id="datepicker1" name="fecha_rango_2" class="seleccionTurist_180" maxlength="15" />
    </td>   
  </tr>
  <tr>
    <td width="100" class="celdalazul_peque">Estado de Pago</td>
    
    <td width="100" class="celdalCELEST">
 
		<select id="xEstado" name="xEstado" class="seleccionTurist_180">
            <? if($_GET['buscar']==true)
               {
                    $select = "selected";
                    
                    if($_GET['xEstado']==0)
                    {
                        echo '<option value="0" selected>Pendiente de Pago</option>
                    <option value="1">Pagado</option>';  
                    }
                    else
                    {
                          echo '<option value="0">Pendiente de Pago</option>
                    <option value="1" selected>Pagado</option>';
                    }
               }
               else
               {
                    echo '<option value="0">Pendiente de Pago</option>
                    <option value="1">Pagado</option>';
               }
               ?>
        </select>
		
   </td>
    

   <td class="celdalazul_peque"></td> 
       <td width="150" class="celdalCELEST">
		   <input type="submit"  class="BotonAzul2"  value="Consultar" />
   </td>

</tr>

</table>
</form>


</body>
</html>


<div id="resultado_listar" style=" PADDING-TOP: 10px; TEXT-ALIGN: left; PADDING-BOTTOM: 3px; FONT-FAMILY: Arial; FONT-SIZE: 8pt; left:20px; top:258px; width:1170px; height:330px;" >

<?php
$Total = 0;
$Cantidad = 0;
if($_GET['buscar']==true)
{
    $fecha_1 = $_Util->DesformatearFecha($_GET['fecha_rango_1']);
    $fecha_2 =  $_Util->DesformatearFecha($_GET['fecha_rango_2']);
    

    $miConexion = new ClaseConexion;
    $miConexion->Conectar(); 
 $query=$miConexion->EjecutaSP("Consulta_Reservas_PP_Pagadas_Pendientes","'".$fecha_1."','".$fecha_2."','".$_GET['xEstado']."'");  

   
}

?>
<form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
<input class="BotonTurist_Azul" style="width:120px;" type="button" name="botonExcel" id="botonExcel" value="Exportar a Excel" />
<input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
<table width="100%" id="Exportar_a_Excel">
    <thead>
        <tr>
            <td class="celdalazul_peque">Fecha Reserva</td>
            <td class="celdalazul_peque">Servicio</td>
            <td class="celdalazul_peque">Fecha Pago</td>
            <td class="celdalazul_peque">Pasajero</td>
            <td class="celdalazul_peque">Distribuidor</td>
            <td class="celdalazul_peque">Usuario Reserva</td>
            <td class="celdalazul_peque">Observacion</td>
            <td class="celdalazul_peque">Comision</td>
        </tr>
    </thead>
    <tbody>
    <?
    while($r=mysql_fetch_array($query))
    {?>
            <tr>
            <td class="celdalCELEST"><?=$_Util->FormatearFecha($r['FECHA_RESERVA'])?></td>
            <td class="celdalCELEST"><?=$r['SERVICIO']?></td>
            <td class="celdalCELEST"><?=$_Util->FormatearFecha($r['FECHA_PAGO'])?></td>
            <td class="celdalCELEST"><?=$r['PASAJERO']?></td>
            <td class="celdalCELEST"><?=$r['DISTRIBUIDOR']?></td>
            <td class="celdalCELEST"><?=$r['USUARIO_RESERVA']?></td>
            <td class="celdalCELEST"><?=$r['OBSERVACION']?></td>
            <td class="celdalCELEST"><?=$r['COMISION']?></td>
        </tr>
    <? } ?>
    </tbody>
    
</table>

</form>

</div>'



</div>