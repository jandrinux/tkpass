<?php
    session_start();
    include("menus.php");
    $_Util=new Util;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<link href="../../css/menu_small.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>


</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">
  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Descuentos Especiales</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    

  </table>
  
    

<div id="Layer1">

<div id="menuh">
        <ul>
           <li><a href="Descuentos.php" >Descuentos</a></li>
           <li><a href="descuentos_especiales.php" id="primero">Especiales</a></li>
        </ul>
</div>

<!-- Aqui va el cuerpo -->
<form name="form1" method="post" onSubmit="carga_descuentos_especiales(); return false">
<table width="400"  border="0" cellspacing="0">
  <tr>
    <td width="150"  class="celdalazul_peque">Monto:</td>
    <td width="200"  class="celdalCELEST">    
	<input type="text" id="monto" name="monto" class="seleccionTurist_180" onkeypress="return validarP3(event);"	/>
    </td>  

  </tr>
  
   <tr>
    <td class="celdalazul_peque">Rango Descuento:</td>
    <td class="celdalCELEST" >	
   	   <select name="rango" class="seleccionTurist_180" id="rango"  > 
        <option value="0">--SELECCIONE--</option>
        <option value="5">1% - 5%</option>
        <option value="10">1% - 10%</option>
        <option value="15">1% - 15%</option>
        <option value="20">1% - 20%</option>
        <option value="25">1% - 25%</option>
        <option value="30">1% - 30%</option>
        <option value="35">1% - 35%</option>
        <option value="40">1% - 40%</option>
        <option value="45">1% - 45%</option>
        <option value="50">1% - 50%</option>
        
       </select>	
  </td>  
  </tr>
  
</table>



<br />
<input type="submit" style="margin-left: 320px;" class="BotonTurist_Celest" name="btnAceptar" value="Aceptar" />
</form>


<div id="resultado"><? include("CONSULTA_descuentos_especiales.php") ?></div>

</div>
</body>
</html>