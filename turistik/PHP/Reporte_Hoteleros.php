<?php
    session_start();
    include("menus.php");
    $_Util=new Util;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/demos.css" rel="stylesheet" />
<link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script type="text/javascript" src="../../js/ui.core.js"></script>
<script type="text/javascript" src="../../js/ui.datepicker.js"></script>
<script type="text/javascript">
$(function() {
	$("#datepicker1").datepicker();
    $("#datepicker2").datepicker();
});
</script>

</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">

  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Reservas Pagadas / Embarcadas</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    
  </table>
  

<div id="Layer1">

<form name="form1" method="post" onsubmit="reporte_hoteleros(); return false" >
  <table width="550" border="0" cellpadding="0" cellspacing="0">

    <tr>
      <td width="100" class="celdalazul_peque">Fecha Desde</td>
      <td width="264" class="celdalCELEST">        
        <input  id="datepicker1" value="<?=date('d/m/Y')?>" class="seleccionTurist_180" readonly=""  name="fecha" />
      </td>
    </tr>
    <tr>
      <td width="100" class="celdalazul_peque">Fecha Hasta</td>
      <td width="264" class="celdalCELEST">
       <input  id="datepicker2" value="<?=date('d/m/Y')?>" class="seleccionTurist_180" readonly="" name="fecha" />
       
      </td>
    </tr>
    <tr>
      <td width="100" class="celdalazul_peque">Tipo Reserva</td>
      <td width="264" class="celdalCELEST">        
        <select name="tipo_reserva" id="tipo_reserva" class="seleccionTurist_200">
            <option value="0" selected="">-- TODAS --</option>
            <option value="1">-- PP --</option>
            <option value="2">-- CC --</option>
        </select>
        <input align="right" name="aceptar" type="submit" class="BotonTurist_Azul" value="Aceptar" /> 
      </td>
      
    </tr>
  </table>
</form>

<br />

<div id="resultado_listar"></div>

</div>
</body>
</html>