<?php
    session_start();
    include("menus.php");
    $_Util=new Util;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/demos.css" rel="stylesheet" />
<link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../../js/ui.core.js"></script>
<script type="text/javascript" src="../../js/ui.datepicker.js"></script>

<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script type="text/javascript">
$(document).ready(function()
{   
    $("#datepicker1").datepicker({
        dateFormat: 'mm/yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,

        onClose: function(dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month, 1)));
        }
    });

    $("#monthPicker").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
    });
});
</script>
<script language="javascript">
$(document).ready(function() {
     $("#botonExcel").click(function(event) {
     $("#datos_a_enviar").val( $("<div>").append( $("#resultado_listar").eq(0).clone()).html());
     $("#FormularioExportacion").submit();
});
});
</script>
<style>
.ui-datepicker-calendar {
	display: none;
}
</style>
</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">

  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Reporte Shopping</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    
  </table>
  

<div id="Layer1">

<form name="form1" method="post" onsubmit="listado_shopping(); return false" >
  <table width="550" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="100" class="celdalazul_peque">Mes</td>
      <td width="264" class="celdalCELEST">        
        <input  id="datepicker1" value="<?=date('m/Y')?>" class="seleccionTurist_180" readonly=""  name="fecha" />
      </td>
    </tr>
    <tr>
      <td class="celdalazul_peque" width="100">Excursiones</td>
      <td class="celdalCELEST" >     
      <select name="excursion" id="excursion" class="seleccionTurist_200">
      <option value="0" selected="">-- TODOS --</option>

        <?
            $miConexion = new ClaseConexion;      
            $miConexion->Conectar();
            $query=$miConexion->EjecutaConsulta(" SELECT * FROM EXCURSION_ORIGEN WHERE
                                                  CODIGO_EO IN(56,57,58,67) AND
                                                  ESTADO_EO = 1 ORDER BY NOMBRE_EO");
            while ($row = mysql_fetch_assoc($query))
            {
                echo "<option value=".$row['CODIGO_EO']." >".$row['NOMBRE_EO']."</option>" ; 
            }
        
            mysql_free_result($query); 
            mysql_close();
        ?>
        </select> 
         <input align="right" name="aceptar" type="submit" class="BotonTurist_Azul" value="Aceptar" />   
      </td>

    </tr>

  </table>
</form>
<form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
  <table width="550" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="58" class="celdalazul_peque">
      </td>
      <td width="264" class="celdalazul_peque">
            <input type="button"  class="BotonTurist_largo" id="botonExcel" value="Exportar a Excel" />
      </td>
    </tr>
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar"  />
  </table>
</form>
<br />

<div id="resultado_listar"></div>

</div>
</body>
</html>