<?php

require_once('lib/nusoap.php');
$webServer = new soap_server(); 
$webServer->soap_defencoding = 'UTF-8';
$namespace = 'http://www.tkpass.cl/turistik/Webservice/index.php?wsdl'; 
$webServer->configureWSDL('ws'); 
$webServer->wsdl->schemaTargetNamespace = $namespace; 

$webServer->wsdl->addComplexType('Usuario','complexType','struct','all','',array(

				'Rut' => array('name' => 'Rut','type' => 'xsd:string'),
				'Nombre' => array('name' => 'Nombre','type' => 'xsd:string'),
				'Apellido' => array('name' => 'Apellido','type' => 'xsd:string'),
				'Email' => array('name' => 'Apellido','type' => 'xsd:string'),
				'Tkpass' => array('name' => 'Tkpass','type' => 'xsd:string'),
				'Nivel' => array('name' => 'Nivel','type' => 'xsd:string')
				
				        )
);

$webServer->wsdl->addComplexType('Sucursal','complexType','struct','all','',array(

				'Codigo' => array('name' => 'Rut','type' => 'xsd:string'),
				'Nombre' => array('name' => 'Nombre','type' => 'xsd:string'),
				'Direccion' => array('name' => 'Apellido','type' => 'xsd:string'),
				'Telefono' => array('name' => 'Apellido','type' => 'xsd:string'),
				
				        )
);

$webServer->wsdl->addComplexType('Usuarios','complexType','array','','SOAP-ENC:Array',array(),
		array(array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:Usuario[]')),'tns:Usuario');
		
$webServer->wsdl->addComplexType('Sucursales','complexType','array','','SOAP-ENC:Array',array(),
		array(array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:Sucursal[]')),'tns:Sucursal');
		
		
$webServer->register('UsuariosList',array(),array('return' => 'tns:Usuarios'));
function UsuariosList() {
		require ("../Clases/ClaseConexion.inc.php");   
		$miConexion= new ClaseConexion;
		$miConexion->Conectar();
		$query=$miConexion->EjecutaConsulta(" SELECT * FROM USUARIO WHERE COD_TUSUA=3 or COD_TUSUA=7");
		while ($r = mysql_fetch_array($query))
		{
			$RUT = $r['RUT_USUA'];
			$NOMBRE = $r['NOMBRE_USUA'];
			$APELLIDO = $r['APELLIDO_USUA'];
			$EMAIL = $r['EMAIL_USUA'];
			$TK = $r['COD_USUA'];
			$NIVEL = $r['COD_TUSUA'];
			
			$elements[] = array(					
							"Rut" => $RUT ,
							"Nombre" => utf8_decode($NOMBRE) ,
                            "Apellido" => utf8_decode($APELLIDO),
							"Email" => $EMAIL ,
							"Tkpass" => $TK,
							"Nivel" => $NIVEL
                            );		
			
		}
		mysql_free_result($query); 
		mysql_close();          
		
		return $elements;
	
}

$webServer->register('SucursalesList',array(),array('return' => 'tns:Sucursales'));
function SucursalesList() {
		require ("../Clases/ClaseConexion.inc.php");   
		$miConexion= new ClaseConexion;
		$miConexion->Conectar();
		$query=$miConexion->EjecutaConsulta(" SELECT * FROM SUCURSAL");
		while ($r = mysql_fetch_array($query))
		{
			$CODIGO = $r['CODIGO_SUCURSAL'];
			$NOMBRE = $r['SUCURSAL'];
			$DIRECCION = $r['DIRECCION'];
			$TELEFONO = $r['TELEFONO'];
		
			
			$elements[] = array(					
							"Codigo" => $CODIGO ,
							"Nombre" => $NOMBRE ,
                            "Direccion" => $DIRECCION,
							"Telefono" => $TELEFONO 
                            );		
			
		}
		mysql_free_result($query); 
		mysql_close();          
		
		return $elements;
	
}


$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$webServer->service($HTTP_RAW_POST_DATA); 
?>