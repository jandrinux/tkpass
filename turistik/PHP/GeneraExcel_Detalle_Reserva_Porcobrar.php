<?
header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=Detalle_Reserva_Cobrada.xls");
header("Pragma: no-cache");
header("Expires: 0");

require ("../Clases/ClaseConexion.inc.php");
include( "../Clases/class.TemplatePower.inc.php"); 
require ("../Clases/ClaseUtil.inc.php");

$tpl = new TemplatePower( "../Plantillas/GeneraExcel_Detalle_Reserva_Porcobrar.tpl" );
$_Util=new Util;

$miConexion= new ClaseConexion;
$tpl->prepare();

$tpl->assign("fecha", $_Util->Fecha());
$tpl->assign("excursion", $_GET['excursion']);
$tpl->assign("fecha", $_GET['fecha']);
$tpl->assign("fecha_format", $_GET['fecha']);
$tpl->assign("usuario", $_GET['usuario']);
$usuario=$_GET['usuario'];



if($_GET[fecha_f]<>'')
{
	$miConexion->Conectar();
	$queryT1=$miConexion->EjecutaSP("Consulta_Lista_PasajerosPorCobrar","'".$_GET['fecha_f']."','".$_GET['fecha_ff']."','".$_GET['tipo']."','".$_GET['distribuidor']."'");
	while ($rowT1 = mysql_fetch_assoc($queryT1))
	{
    $tpl->newBlock("tabla");	
	$tpl->assign("pasajero", strtoupper($rowT1['PASAJERO_RE']));
	$tpl->assign("neto", ' $'.number_format($rowT1['neto'], 0, ",", "."));
	$tpl->assign("observaciones", $rowT1['OBSERVACION_RE']);
	
	$tpl->assign("fecha_tabla", strtoupper($rowT1['FECHA_RE']));
	if($rowT1['LUGAR_RE']=='0')
	{
	$ubicacion=$rowT1['OTRO_LUGAR_RE'];
	}
	else
	{
	$ubicacion=$rowT1['ubicacion'];
	}
	if($rowT1['COBRADA_RE']=='0')
	{
		$tpl->assign("check0", $rowT1['COBRADA_RE']);
		$tpl->assign("check", "<input type='checkbox' name='checksol[]' value='");
		$tpl->assign("check2", strtoupper($rowT1['CODIGO_RE']));
		$tpl->assign("check3", "'>");
	}	
	$tpl->assign("lugar", strtoupper($ubicacion));	
	$tpl->assign("facturar", $rowT1['FACTURA_RE']);		
	if($rowT1['FACTURA_RE']<>'')
	{
		$tpl->assign("f1", '<img src="../Imagenes/ico_alpha_Search2_16x16.png" width="16" alt="Modifica Factura" height="16" style="cursor:hand" onClick="Modificar(');
		$tpl->assign("f2", strtoupper($rowT1['CODIGO_RE']));				
		$tpl->assign("f3",')">' );		
	}
	$tpl->assign("excursion_desc", $rowT1['NOMBRE_EO']);		
	$tpl->assign("telefono", strtoupper($rowT1['TELEFONO_RE']));			
	$tpl->assign("codigo_re", strtoupper($rowT1['CODIGO_RE']));		
	$tpl->assign("user", strtoupper($rowT1['NOMBRE_USUA']));		
	if(strtoupper($rowT1['GLOSA_TR'])=='CUENTA CORRIENTE')
	{
	$tipo="CC";
	}
	else
	{
		if(strtoupper($rowT1['GLOSA_TR'])=='POR PAGAR')
		{
		$tipo="PP";
		}
		else
		{
		$tipo=strtoupper($rowT1['GLOSA_TR']);
		}
	}	
	$tpl->assign("tipo", $tipo);						
	$tpl->assign("nacion", strtoupper($rowT1['nacionalidad']));			
	$tpl->assign("pasaporte", strtoupper($rowT1['PASAPORTE_RE']));				
	$tpl->assign("hotel", strtoupper($rowT1['hotel']).'# '.strtoupper($rowT1['HABITACION_RE']));			
	if(strtoupper($rowT1['GLOSA_CIERRE'])=='ANULADA')
	{
		$tpl->assign("estilo", "class='celdacca_rojo_peque'");
	}
	else
	{
		$tpl->assign("estilo", "class='celdalCELEST_peque'");	
		$tpl->assign("FI0", '<img src="../Imagenes/configurar19x19.png" width="16" alt="Consulta Ficha" height="16" style="cursor:hand" onClick="Modificar(');
		$tpl->assign("FI1", strtoupper($rowT1['CODIGO_RE']));				
		$tpl->assign("FI2",')">' );		
	}
	if(strtoupper($rowT1['GLOSA_CIERRE'])=='PENDIENTE')
	{
	$GLOSA="PEND.";
	}
	else
	{
		if(strtoupper($rowT1['GLOSA_CIERRE'])=='PAGADA')
		{
		$GLOSA="PAG.";
		}
		else
		{
		$GLOSA=strtoupper($rowT1['GLOSA_CIERRE']);
		}
	}
	$tpl->assign("cierre", $GLOSA);
	$tpl->assign("login", strtoupper($rowT1['LOGIN_USUA_CIERRE']));
	$tpl->assign("voucher", strtoupper($rowT1['VOUCHER_RE']));
       $RAZON=$rowT1['RAZON_SOCIAL_DIST'];
    }
}
$tpl->newBlock("tabla1");
$suma_to=number_format('$'.$suma_t, 0, ",", ".");
$tpl->assign("suma_totales", $suma_t);
$tpl->assign("razon_social", $RAZON);

mysql_free_result($queryT1); 
mysql_close(); 


$tpl->printToScreen();
?>
