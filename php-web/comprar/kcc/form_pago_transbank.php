<?
	include_once("class/config.php");

	$Aux = new DBCompra();

	$TBK_TIPO_TRANSACCION = "TR_NORMAL";
	$TBK_ORDEN_COMPRA=$TBK_ID_SESION;
	
	$descripcion_prod = $Aux->ElDato($producto,"ProductoTur","CODIGO_PROD","DESCRIPCION_PROD");
	$TBK_URL_EXITO="http://www.tur.pointpay.cl/comprar/kcc/exito.php3";
	$TBK_URL_FRACASO="http://www.tur.pointpay.cl/comprar/kcc/fracaso.php3";

	$losproductos = $Aux->TraerLosDatos("ProductoTur","CODIGO_PROD","NOMBRE_PROD"," order by NOMBRE_PROD ");
	$lasmonedas = $Aux->TraerLosDatos("Moneda","id","descripcion");
?>
<script>

</script>

<div id="pago">
<form method="POST" action="/cgi-bin/tbk_bp_pago.cgi">
<INPUT TYPE="hidden" NAME="TBK_TIPO_TRANSACCION"    VALUE="<?=$TBK_TIPO_TRANSACCION?>">
<INPUT TYPE="hidden" NAME="TBK_ORDEN_COMPRA"        VALUE="<?=$TBK_ORDEN_COMPRA?>">
<INPUT TYPE="hidden" NAME="TBK_ID_SESION"           VALUE="<?=$id_session?>">
<INPUT TYPE="hidden" NAME="TBK_URL_EXITO" SIZE=40   VALUE="<?=$TBK_URL_EXITO?>">
<INPUT TYPE="hidden" NAME="TBK_URL_FRACASO" SIZE=40 VALUE="<?=$TBK_URL_FRACASO?>">
<INPUT TYPE="hidden" NAME="TBK_MONTO" SIZE=40 VALUE="<?=number_format($TBK_MONTO,0,'','').',00'?>">
<font face="arial,helvetica" size=2>
A continuacion encontrara un resumen de su compra. Esta correcta toda la informaci&oacute;n? 
<br><br>
<table >
<tr><TD align="left" colspan="2"><b><?=nl2br($descripcion_prod)?></b><br><br></TD></tr>
<tr>
	<TD align="left">Nombre:</TD>
	<td align="left"><?=$nombre?></td>
</tr>
<tr>
	<TD align="left">Fecha Servicio</TD>
	<td align="left"><?=$fecha?>
	</td>
</tr>
<tr>
	<TD align="left">Direcci&oacute;n/Hotel:</TD>
	<td align="left"><?=$dir_hotel?></td>
</tr>
<tr>
	<TD align="left">EMail:</TD>
	<td align="left"><?=$email?></td>
</tr>
<tr>
	<TD align="left">Producto:</TD>
	<td align="left"><?=$Aux->ElDato($producto,"ProductoTur","CODIGO_PROD","NOMBRE_PROD")?> ($<?=number_format($Aux->ElDato($producto,"ProductoTur","CODIGO_PROD","PRECIO_NAC_PROD"),0,',','.')?>)</td>
</tr>
<tr><TD align="left">Cantidad:</TD><td align="left"><?=$cantidad?></td></tr>
<TR><TD align="left">Precio Total:</td><td align="left"><b>$<?=number_format($TBK_MONTO,0,',','.');?>.-</b></TR>
</table>
</font>
<br><input type="submit" value="Pagar con Transbank Webpay" SIZE=20 >
<input type="button" name=cc value="Cancelar" onclick="MiSubmit('/comprar/kcc/pg_trans.php');">
</form>
</div>