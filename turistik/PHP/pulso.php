<?php
    session_start();
    include("menus.php");
    $_Util=new Util;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/ui.core.js"></script>
<script type="text/javascript" src="../../js/ui.datepicker.js"></script>

<script>

    $(document).ready(function()
    {   
        $("#datepicker1").datepicker();
         $("#datepicker2").datepicker();
    
    
    });


    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    
    function Graficar()
    {
        debugger;
        var datos = new Array();
        var fecha_desde = $('#datepicker1').val();
        var fecha_hasta = $('#datepicker2').val();
    
   	    $.ajax({
    		url:'CONSULTA_pulso.php',
    		type:'POST',
            data: { fecha_desde:fecha_desde, fecha_hasta:fecha_hasta },
    		dataType:'json',
    		async:true    		
    	}).done(function(datos){
    	    //datos = JSON.parse(datos);
            drawChart(datos) ;
    	});
        
       
    }            

      

      function drawChart(datos) 
      {
            var data = google.visualization.arrayToDataTable(datos);
            
            var options = {
              	title: 'GRAFICO DE VENTAS',
              	hAxis: {title: 'FECHA', titleTextStyle: {color: 'green'}},
                
              	vAxis: {title: 'MILES DE PESOS', titleTextStyle: {color: '#209928'}},
              	backgroundColor:'#fff',
              	legend:{position: 'bottom', textStyle: {color: 'blue', fontSize: 11}},
              	width:1200,
                height:400
            	};
            
            var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
            chart.draw(data, options);
       }

        
</script>

</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">
  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/graph.jpg" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Pulso</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    

  </table>
  
    

<div id="Layer1">


<table width="700" border="0" cellspacing="0" >
    <tr>
      <td width="100" class="celdalazul_peque">Fecha Desde</td>
      <td width="264" class="celdalCELEST">        
        <input  id="datepicker1" value="<?=date('d/m/Y')?>" class="seleccionTurist_180" readonly=""  name="fecha_desde" />
      </td>
      <td width="100" class="celdalazul_peque">Fecha Hasta</td>
      <td width="264" class="celdalCELEST">        
        <input  id="datepicker2" value="<?=date('d/m/Y')?>" class="seleccionTurist_180" readonly=""  name="fecha_hasta" />
      </td>
      
      <td width="100" class="celdalCELEST"><input type="button" value="Consultar" class="BotonTurist_Azul" onclick="Graficar();"  name="aceptar" /></td>
    </tr>
</table>


<div id="chart_div" style="clear: both;"></div>

</div>
</body>
</html>