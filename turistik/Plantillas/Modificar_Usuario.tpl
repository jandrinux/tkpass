<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../../js/ui.core.js"></script>
<script type="text/javascript" src="../../js/ui.datepicker.js"></script>
<link type="text/css" href="../../css/demos.css" rel="stylesheet" />
<link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:16px;
	top:159px;
	width:823px;
	height:231px;
	z-index:13;
}
#Layer3 {
	position:absolute;
	left:21px;
	top:11px;
	width:314px;
	height:62px;
	z-index:14;
}
-->
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Modificar Usuario</title>
<style type="text/css">
<!--
#Layer2 {	position:absolute;
	left:703px;
	top:8px;
	width:132px;
	height:116px;
	z-index:1;
}
-->
</style>
<script type="text/JavaScript">

 $(function() {
        $( "#datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
        
        
    });


function Popup()
{
 miPopup = window.open("../PHP/Ingreso_Ubicacion.php","miwin","top=200,left=350,width=550,height=200,scrollbars=yes,titlebar=no,location=no");
 miPopup.focus();	
}
function pres_num()
	{
		if((event.keyCode<48) || (event.keyCode>57))
			{
				event.returnValue = false;
			}		
	}
	
function validar()
	{
    	// Primer paso: Obtener el rut que ingreso el usuario
   		 var rutCompleto = document.form1.rutusuario.value;
		// Eliminamos los caracteres raros, espacios, puntos, guiones.
    	// Pasamos a minusculas, y separamos el rut y el digito verificador
    	rutCompl = rutCompleto.toLowerCase();
   		var dv=document.form1.dvusuario.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
   		 valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   		 valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
        		alert("El Rut no es Valido.Ingreselo Nuevamente");
        		document.form1.rutusuario.value="";
				document.form1.dvusuario.value="";
				document.form1.rutusuario.focus();
   			 }
	}

function verificarDV(rut, dv)
	{
   		var multiplicador = 9;
    	var aux = rut;
    	var suma = 0;
    	while(aux > 0)
			{
        		var unidades = aux % 10 ;
        		aux = (aux - unidades) / 10;
        		suma += (multiplicador * unidades);
        		multiplicador--;
        		if (multiplicador < 4)
					{
            			multiplicador = 9;
       				 }
    		}
    	digito = suma % 11;
    	if (digito == 10)
			{
        		digito = "k";
    		}
    	if (dv == digito)
			{
        		return true;
    		}
    	else 
			{
       			 return false;
    		}
	} 

function isEmailAddress(email)
	{
		var s = email;
		var filter=/^[A-Za-z][A-Za-z0-9_]*@[A-Za-z0-9_]+\.[A-Za-z0-9_.]+[A-za-z]$/;
		if (s.length == 0 ) return true;
		if (filter.test(s))
		return true;
		else
		alert("Ingrese una direcci�n de correo v�lida");
		document.form1.emailusuario.focus();
		return false;
	}
function ModificarTipoUsuario()
	{
		miPopup = window.open("../PHP/ListaTipoUsuario.php?condicion=1","tipousuario","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}

function ModificarOperador()
	{
	miPopup = window.open("../PHP/ListaOperador.php","tipousuario","top=200,left=400,width=350,height=250,scrollbars=yes,titlebar=no,location=no");
	miPopup.focus();
	}
		
function ModificarDistribuidor()
	{
		miPopup = window.open("../PHP/Lista_Distribuidor.php","cue","top=200,left=400,width=450,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
function ModificarUsuarioSistema()
	{
		miPopup = window.open("../PHP/Lista_Usuario_Sistema.php","cue","top=200,left=400,width=350,height=200,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}	
function pres_espacio()
	{
		if(event.keyCode==32)
			{
				event.returnValue = false;
				return;
			}
		
		event.returnValue = true;
	}		
	
	
function BuscarUsuario()
	{
		//alert(document.form1.rutusuario.value);
		if (document.form1.rutusuario.value=='' && document.form1.dvusuario.value=='')
			{
				alert("Debe Ingresar el Rut y el digito verificador para realizar la busqueda");
				document.form1.rutusuario.focus();
				return;
			}
		// Primer paso: Obtener el rut que ingreso el usuario
   		 var rutCompleto = document.form1.rutusuario.value;
		// Eliminamos los caracteres raros, espacios, puntos, guiones.
    	// Pasamos a minusculas, y separamos el rut y el digito verificador
    	rutCompl = rutCompleto.toLowerCase();
   		var dv=document.form1.dvusuario.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
   		 valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   		 valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
        		alert("El Rut no es Valido.Ingreselo Nuevamente");
        		document.form1.rutusuario.value="";
				document.form1.dvusuario.value="";
				document.form1.rutusuario.focus();
   			 }
		document.form1.buscar.value=1;
		document.form1.submit();
	}
function ModificarUsuario()
	{
		if (document.form1.rutusuario.value=='')
			{
				alert("Debe Ingresar el Rut del Usuario que desea Modificar");
				document.form1.rutusuario.focus();
				return;
			}
		if (document.form1.dvusuario.value=='')
			{
				alert("Debe Ingresar el Digito Verificador del Rut del usuario que desea modificar");
				document.form1.dvusuario.focus();
				return;
			}
			
		if (document.form1.usuario.value=='')
			{
				alert("Debe Ingresar el Usuario que desea Modificar");
				document.form1.usuario.focus();
				return;
			}
		if (document.form1.password.value=='')
			{
				alert("Debe Ingresar la Contrase�a para el usuario que desea Modificar");
				document.form1.password.focus();
				return;
			}
		
		
		if (document.form1.tipo_usuario.value=='')
			{
				alert("Debe Ingresar el Tipo de Usuario que desea Modificar");
				document.form1.tipo_usuario.focus();
				return;
			}
		if (document.form1.nombreusuario.value=='')
			{
				alert("Debe Ingresar el Nombre");
				document.form1.nombreusuario.focus();
				return;
			}
		if (document.form1.apellido.value=='')
			{
				alert("Debe Ingresar el Apellido");
				document.form1.apellido.focus();
				return;
			}
		
		if (document.form1.direccion.value=='')
			{
				alert("Debe Ingresar la Direcci�n del usuario que desea modificar");
				document.form1.direccion.focus();
				return;
			}
		if (document.form1.comuna.value=='')
			{
				alert("Debe Ingresar la Comuna a la cual pertenece el usuario");
				document.form1.comuna.focus();
				return;
			}
		if (document.form1.ciudad.value=='')
			{
				alert("Debe Ingresar la Ciudad a la cual pertenece el usuario");
				document.form1.ciudad.focus();
				return;
			}
		if (document.form1.distribuidor.value=='')
			{
				alert("Debe Ingresar el Distribuidor");
				document.form1.distribuidor.focus();
				return;
			}
		if (document.form1.usuariosistema.value=='')
			{
				alert("Debe Ingresar si Usuario del Sistema");
				document.form1.usuariosistema.focus();
				return;
			}
		if (document.form1.cuentacorrinte.value=='')
			{
				alert("Debe Ingresar Cuenta Corriente para el usuario");
				document.form1.cuentacorrinte.focus();
				return;
			}

		if (document.form1.banco.value=='')
			{
				alert("Debe Ingresar el Banco");
				document.form1.banco.focus();
				return;
			}
		if (document.form1.estado.value=='')
			{
				alert("Debe Ingresar el Estado del Usuario que desea Modificar");
				document.form1.estado.focus();
				return;
			}
		if (document.form1.sucursal.value=='')
			{
				alert("Debe Ingresar la Sucursal de la que pertence el Usuario");
				document.form1.sucursal.focus();
				return;
			}
            
		
		
		if (document.form1.VALIDACION.value==1)
			{
				if (document.form1.codigomanager.value=='')
					{
						alert("Debe Ingresar el Codigo Manager para este tipo de Usuario");
						document.form1.codigomanager.focus();
						return;
					}
			}
    	var rutCompleto = document.form1.rutusuario.value;
		rutCompl = rutCompleto.toLowerCase();
    	var dv=document.form1.dvusuario.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
    	valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   	 	valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
       	 		alert("El Rut no es Valido.Ingreselo Nuevamente");
        		document.form1.rutusuario.value="";
				document.form1.dvusuario.value="";
    		}
		
		document.form1.guardar.value="1";
		document.form1.submit();
	}
function ModificarEstado()
	{
		miPopup = window.open("../PHP/Lista_Estados.php","cue","top=200,left=400,width=350,height=200,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
/*
function CuentaContable()
	{
		miPopup = window.open("../PHP/Lista_CuentaContable.php","cuentacontable","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
*/
function Limpiar()
	{
		//document.form1.rutusuariovalue="";
		//document.form1.dvusuario.value="";
		//document.form1.tipousuario.value="0";
		//document.form1.tipocuenta.value="0";
		//document.form1.usuario.value="";
		//document.form1.passwordusuario.value="";
		//document.form1.nombreusuario.value="";
		//document.form1.apellido.value="";
		//document.form1.direccion.value="";
		//document.form1.telefono.value="";
		//document.form1.comuna.value="";
		//document.form1.ciudad.value="";
		//document.form1.contactodistribuidor.value="";
		//document.form1.distribuidor.value="0";
		//document.form1.cuentacorriente.value="";
		//document.form1.email.value="";
		//document.form1.celular.value="";
	//	document.form1.usuariosistema.value="0";
	window.close();
	}
function ModificarBanco()
	{
		miPopup = window.open("../PHP/ListaBancos.php","bancos","top=200,left=400,width=350,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();

	}
</script>
</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<form id="form1" name="form1" method="post" action="">
<div id="etiqutaprimera" style="position:absolute; left:17px; top:129px; width:831px; height:380px; border:0pt black solid;">
  
    <table width="822" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="119" class="celdalazul">Rut Usuario </td>
        <td width="294" class="celdalCELEST">
          <input name="rutusuario" type="text" class="seleccionTurist_180" onKeyPress="pres_num();" maxlength="10" {RUT}/>
          -
        <input name="dvusuario" type="text" class="seleccionT80_a" maxlength="1" onChange="validar()" {DV}/>
        <!--<img src="../Imagenes/120px-Crystal_Clear_action_build19x19.png" width="19" height="19" alt="Buscar Usuario"  onclick="BuscarUsuario()" style="cursor:hand"/>--></td>
        <td width="123" class="celdalazul">Usuario</td>
        <td width="286" class="celdalCELEST"><input name="usuario" type="text"  {LOGIN} onkeypress="pres_espacio()"/></td>
      </tr>
      <tr>
        <td class="celdalazul">Contrase&ntilde;a </td>
        <td class="celdalCELEST"><input name="password" type="password" onkeypress="pres_espacio()" value="{PASSWORD}"/></td>
        <td class="celdalazul">Tipo de Usuario </td>
        <td class="celdalCELEST"><label>
          <input name="tipo_usuario" type="text" class="seleccionTurist_250" id="tipo_usuario" {TIPOUSUARIO} readonly="true" />
        </label>
        <img src="../Imagenes/modify.gif" width="16" height="16"  onclick="ModificarTipoUsuario()" alt="Modificar Tipo Usuario" style="cursor:hand"/></td>
      </tr>
      <tr>
        <td class="celdalazul">Nombre Usuario </td>
        <td class="celdalCELEST"><label>
          <input name="nombreusuario" type="text" class="seleccionTurist_250" {NOMBREUSAURIO}/>
        </label></td>
        <td class="celdalazul">Apellido Usuario </td>
        <td class="celdalCELEST"><input name="apellido" type="text" class="seleccionTurist_250" {APELLIDO}/></td>
      </tr>
      <tr>
        <td class="celdalazul">Direcci&oacute;n</td>
        <td class="celdalCELEST"><label>
          <input name="direccion" type="text" class="seleccionTurist_250" id="direccion" {DIRECCION} />
        </label></td>
        <td class="celdalazul">Telefono</td>
        <td class="celdalCELEST"><input name="telefono" type="text" class="seleccionTurist_180" onkeypress="pres_num();" {TELEFONO} /></td>
      </tr>
      <tr>
        <td class="celdalazul">Email</td>
        <td class="celdalCELEST"><input name="emailusuario" type="text" class="seleccionTurist_250"  {EMAIL}/></td>
        <td class="celdalazul">Celular </td>
        <td class="celdalCELEST"><input name="celular" type="text" class="seleccionTurist_180" onkeypress="pres_num();" {CELULAR} /></td>
      </tr>
      <tr>
        <td class="celdalazul">Comuna</td>
        <td class="celdalCELEST"><label>
          <input name="comuna" type="text" class="seleccionTurist_180" id="comuna" {COMUNA} />
        </label></td>
        <td class="celdalazul">Ciudad</td>
        <td class="celdalCELEST"><label>
          <input name="ciudad" type="text" class="seleccionTurist_180" id="ciudad" {CIUDAD} />
        </label></td>
      </tr>
      <tr>
        <td class="celdalazul">Distribuidor</td>
        <td class="celdalCELEST"><input type="text" class="seleccionTurist_250" {DISTRIBUIDOR} name="distribuidor" readonly="true" />
        <img src="../Imagenes/modify.gif" width="16" height="16" alt="Modificar Distribuidor" onclick="ModificarDistribuidor()" style="cursor:hand"/></td>
        <td class="celdalazul">Usuario Sistema </td>
        <td class="celdalCELEST"><input name="usuariosistema" type="text" class="seleccionTurist_80" id="usuariosistema" {USUARIOSISTEMA} readonly="true"/>
        <img src="../Imagenes/modify.gif" width="16" height="16" style="cursor:hand" alt="Modificar Usuario Sistema" onclick="ModificarUsuarioSistema()"/></td>
      </tr>
      <tr>
        <td class="celdalazul">Cuenta Corriente</td>
        <td class="celdalCELEST"><label>
          <input name="cuentacorrinte" type="text" class="seleccionTurist_180" {CUENTACORRIENTE}/>
        </label></td>

        <td class="celdalazul">Fecha Nac.</td>
        <td class="celdalCELEST">
           <input name="fecha_nac" type="text" readonly="" id="datepicker" class="seleccionTurist_180" {FECHA_NACIMIENTO} /> 
         <!-- <img src="../Imagenes/configurar19x19.png" width="19" height="19" alt="Ingresar Nueva Cuenta Contable" style="cursor:hand"  onclick="IngresarCuentaContable()"/>    -->
         </td>
      </tr>
      
      <tr>
        <td class="celdalazul">Banco</td>
        <td class="celdalCELEST"><label>
          <input name="banco" type="text" class="seleccionTurist_180" id="banco" {BANCO} readonly="true" />
          <img src="../Imagenes/modify.gif" width="16" height="16"  alt="Modificar Banco" onclick="ModificarBanco()" style="cursor:hand"/>
          <input name="codigo_operador" type="hidden" id="codigo_operador2" {CODIGO_OPERADOR}/>
        </label></td>
        <td class="celdalazul">Operador</td>
        <td class="celdalCELEST"><input name="operador" type="text" class="seleccionTurist_180" id="operador2" {OPERADOR}/> 
          <img src="../Imagenes/modify.gif" width="16" height="16"  alt="Modificar Operador" onclick="ModificarOperador()" style="cursor:hand"/> </td>
      </tr>
      <tr>
        <td class="celdalazul">C&oacute;digo Sucursal </td>
        <td class="celdalCELEST"><input name="sucursal" type="text" class="seleccionTurist_80" id="sucursal" value="{sucursal}" /></td>
        <td class="celdalazul">Codigo Vendedor</td>
        <td class="celdalCELEST"><input name="vendedor" type="text" class="seleccionTurist_80" id="vendedor" value="{vendedor}" /></td>
      </tr>
      <tr>
        <td class="celdalazul">Estado</td>
        <td  class="celdalCELEST"><input name="estado" type="text" class="seleccionTurist_80" id="estado" value="{estado}" readonly="true" {ESTADO} />
        &nbsp;<img src="../Imagenes/modify.gif" width="16" height="16" alt="Modificar Estado" style="cursor:hand"  onclick="ModificarEstado()"/></td>
        
        <td class="celdalazul">Recogida</td>
        <td class="celdalCELEST">
        <select name="lugar_re"   class="seleccionTurist_250"  >
        <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>   
        <!-- START BLOCK : lugar_re -->
        <option value="{CODIGO}" class="seleccion2" {SELECCIONAR_LUGAR}>{DESCRIPCION_LUGAR}</option>
        <!-- END BLOCK : lugar_re -->
        </select>
        </td>
      </tr>
    </table>
    <p>&nbsp;</p>
  </div>

<div class="TituloPlomo" id="Layer3">
  <table width="267" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="83" rowspan="4"><img src="../Imagenes/Crystal_Clear_app_Login_Manager80x80_marco.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="171"><p>&nbsp;</p>      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong>Modificar Usuario </strong><strong></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha">{fecha}</span></td>
    </tr>
    
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
  </table>
</div>
<div id="boton" style="position:absolute; left:348px; top:480px; width:306px; height:25px; z-index:3; border:0pt black solid;">
<input name="Submit" type="button" class="BotonTurist_Celest" value="Modificar" onclick="ModificarUsuario()" style="cursor:hand"/>
<label>
<input name="button" type="submit" class="BotonTurist_Celest" value="Salir" onclick="Limpiar()"/>
</label>
<input type="hidden" name="guardar" value="0" />
<input type="hidden" name="buscar" value="0" />
<input type="hidden" name="CODIGOTIPOUSUARIO"  value="codnulo"/>
<input type="hidden" name="CODIGODISTRIBUIDOR" value="codnulo"/>
<input type="hidden" name="CODIGOUSAURIOSISTEMA" value="codnulo"/>
<input name="CODIGOESTADO" type="hidden" value="{CODIGOESTADO}"/>
<input type="hidden" name="CODIGOCUENTACONTABLE" value="codnulo"/>
<input type="hidden" name="CODIGOBANCO" value="codnulo"/>

<input type="hidden" name="CODIGOUSUARIO" {CODIGOUSUARIO}/>
<input type="hidden" name="CODIGOTU" {CODIGOTU} />
<input type="hidden" name="COD_TUSUA" {COD_TUSUA} />
<input type="hidden" name="CODIGOD" {CODIGOD}/>
<input type="hidden" name="CODIGOUS" {CODIGOUS}/>
<input type="hidden" name="CODIGOE" {CODIGOE}/>
<input type="hidden" name="CODIGOCC" {CODIGOCC}/>
<input type="hidden" name="CODIGOBAN" {CODIGOBAN}/>
<input type="hidden" name="VALIDACION" {VALIDACION} />
</div>
</form>

</body>
</html>
