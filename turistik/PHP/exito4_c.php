<?
//ini_set('display_errors', 1);
session_start();
if ($_SESSION["autentificado"] != "SI"){
    header("location: Login.php");
}

include ('../../lib/MPDF/mpdf.php');
require ("../Clases/ClaseConexion.inc.php");
require ("../Clases/ClaseUtil.inc.php"); 
require ("../Clases/Idiomas_Boarding.php");
require ("../functions/tkpass.php");
require ("../functions/consulta_descuento.php");

$_Util=new Util;

$vouchers = $_GET['v'];
$lista_voucher = explode("/",$vouchers);
$lista_voucher =array_filter($lista_voucher); // elimina array nulos
$lista_documentos = array();

$miConexion = new ClaseConexion;
$idioma = new Idiomas_Boarding;

$excursiones_stk = array(6,54,55,63,64);
$logo = "<img src='../Imagenes/turistik-2pisos.JPG' border='0'  width='100px'>";

if(!empty($vouchers))
{
    
$valor = 0;
$cont_adt = 0;
$cont_adt = 0;
$cant = 0;
$cant_imprimir = 1;

for ( $i=0; $i<count($lista_voucher); $i++) 
{

    $fecha_bp = $_Util->bp_to_date($lista_voucher[$i]);
   
        $miConexion->Conectar();
    	$queryRegistrosReserva=$miConexion->EjecutaSP("Consulta_Voucher4","  '".$lista_voucher[$i]."', '".$fecha_bp."' ");
        $cant_registros = mysql_num_rows($queryRegistrosReserva);
        
            while ($rowL = mysql_fetch_assoc($queryRegistrosReserva))
            {
    	  
            
	              	$usuario_imprime = "VENDEDOR: ".strtoupper($_SESSION["nombre_usu"]." ".$_SESSION["apellido_usu"]);
                    
            		if($rowL['LUGAR_RE'] == 80){
            		  $lugares=' '."OTRO LUGAR: ".$rowL['OTRO_LUGAR_RE'];
            		}else{
            		  $lugares=' '.htmlentities($rowL['ubicacion']);
            		}
                    
                    $idioma_boarding = $rowL['GRUPO_IDIOMA'];
                    
                    if ($idioma_boarding == 0 || $idioma_boarding == 1)
                    {
                        $condiciones = nl2br(htmlspecialchars($rowL['RESUMEN_EO']));
                    }
                    if ($idioma_boarding == 2)
                    {
                        $condiciones = nl2br(htmlentities($rowL['RESUMEN_EO_ENG']));
                    }
                    if ($idioma_boarding == 3)
                    {
                        $condiciones = nl2br(htmlentities($rowL['RESUMEN_EO_POR']));
                    }
                    
                    $minutos_recogida = $_Util->SumarHoras($rowL['HORA_EO'], $rowL['MINUTOS'])." HRS.";
                    $cod_vendedor = $rowL['CODIGO_VENDEDOR_RE'];
                    $cod_usua = $rowL['COD_USUA'];
                    $cod_tusua = $rowL['COD_TUSUA'];
                    $cod_cierre = $rowL['COD_CIERRE'];
                    $ObjTransformaFecha = new tkpass(); 
                    $fecha = $ObjTransformaFecha->transforma_fecha_sql($rowL['FECHA_RE']);         
                    $fecha_qr = str_replace("/", "-", $fecha);
            		$precio = $rowL['PRECIO_RE'];	
                    $codigo_eo = $rowL['CODIGO_EO'];  
                    $telefono = $rowL['TELEFONO_RE'];
                    $fop = $rowL['FOP'];
                    $tipoReserva = $rowL['TIPO_RE'];
                    $voucher = $rowL['VOUCHER_RE'];      
                    $valor = $precio + $valor;	
                    $tipo_pax = $rowL['TIPO_PASAJERO'];
                    $precio_total = $precio + $precio_total;  
                        
                    $n_excursion = strtoupper($rowL['NOMBRE_EO']);
                   	
                    $observacion = $rowL['OBSERVACION_RE'];
                    $estado_reagendado = $rowL['ESTADO_REAGENDADO'];
                    $comision = $rowL['COMISION_RE'];
                    
                    $total_comision = $comision + $total_comision;
                    $cant_pasajeros = $rowL['CANT'];
                    
                    $dcto_pax = $rowL['DSCTO_PAX'];
                    $dcto_lugar = $rowL['DSCTO_LUGAR'];
                    $dcto_lugar_reserva = $rowL['DSCTO_LRESERVA'];
                    $dcto_excursiones = $rowL['DSCTO_EXCURSIONES'];
                    $dcto_cliente = $rowL['DSCTO_CLIENTE'];
                    
                    
                    $codigo_promocion = $rowL['PROMOCION'];
                    $nombre_promocion = $rowL['NOMBRE_PROMOCION'];
        
                    //Se deja condicion si es vacio ya que Tomar_Reserva_Aeropuesrto_Traslado no tiene tipo pax
                    if ($tipo_pax == ''){ 
                        $pasajero = $rowL['PASAJERO_RE']." ".$rowL['APASAJERO_RE'];
                        $tipos_de_pasajeros = $cant_pasajeros. " PASAJEROS";
                    }
        
                    if ($tipo_pax == 'ADT')
                    {  
                        
                        if($rowL['PAQUETE'] == 1 || $rowL['PRECIO_RE'] == 0)
                        {
                            if($rowL['PAQUETE'] == 1)
                            {
                                $pasajero = $rowL['PASAJERO_RE']." ".$rowL['APASAJERO_RE'];
                            }
                            else
                            {
                                $pasajero = $rowL['PASAJERO_RE']." ".$rowL['APASAJERO_RE'];
                            }
                            
                            $cant_adt = $cant_pasajeros;
                        }
                        else
                        {

                                $pasajero = $rowL['PASAJERO_RE']." ".$rowL['APASAJERO_RE'];
                                $valor_pax = $rowL['PRECIO_EO'];
                                $cant_adt = $cant_pasajeros; 
                        }
                    }
                    
                    if ($tipo_pax == 'CHD')
                    {

                        if($rowL['PAQUETE'] == 1 || $rowL['PRECIO_RE'] == 0)
                        {
                            if($rowL['PAQUETE'] == 1)
                            {
                                $cant_chd = $cant_pasajeros;
                            }
                            
                            $cant_chd = $cant_pasajeros;
                        }
                        else
                        {
                            $cant_chd = $cant_pasajeros; 
                            $valor_pax = $rowL['VALOR_CHILD'];  
                        } 
                         
                    }
                    
                    
                    if ($cant_chd != 0) { $tipos_de_pasajeros = $cant_chd." CHD"; }
                    if ($cant_adt != 0) { $tipos_de_pasajeros = $cant_adt." ADT"; } 
                    if ($cant_chd != 0 && $cant_adt != 0) { $tipos_de_pasajeros = $cant_adt." ADT"." / ".$cant_chd." CHD"; }
                    
                    
                    
                    if ($dcto_cliente != 0)
                    {
                        $dcto_tipo_cliente = $rowL['TIPO_CLIENTE'];
                        $descuento_c = $dcto_tipo_cliente.", ";
                    }

                    
                    $idioma->Idioma($cod_tusua, $tipoReserva, $fop, $idioma_boarding, $dcto_pax, $dcto_lugar, $dcto_lugar_reserva, $dcto_excursiones, $total_comision, $precio_total,$estado_reagendado,$cod_cierre);
                    
                    if (in_array($codigo_eo , $excursiones_stk))
                    {
                        $CodeQR = "";
                    }
                    else
                    {
                        $CodeQR = "<img src='https://chart.googleapis.com/chart?chs=110x110&cht=qr&chl=".$lista_voucher[$i]." ' />";
                    }


                    $descuentos_aplicados = '';
                    $descuento_p = '';
                    $descuento_l = '';
                    $descuento_lr = '';
                    $descuento_ex = '';
                    
            ### Cantidad de impresiones con discriminacion a la excursion 6
               $cant++;
               if ($cant_registros == $cant)
               {
                 $cant_imprimir++;
                 include('boarding_format2.php');
               }

                   
        }  
        $precio_total = 0;
        $cant = 0;
        mysql_free_result($queryRegistrosReserva); 
        mysql_close(); 
   
}            
            

for ($i=0; $i<count($lista_voucher); $i++){
    $documentos = $lista_documentos[$i]. " " .$documentos;
}
        

    $html='
    <html>
    <head>
    <meta name="tipo_contenido"  content="text/html;" http-equiv="content-type" charset="utf-8">
    
    <style type="text/css">
     body {
        margin:0px;
     }
    .celda_bordes {
    	font-family: Arial;
    	border: 3px solid #c3c3c3;
    }
    .celda_dos
    {
        margin-left:20px;
        vertical-align:text-top;
    }
    .celda_peque {
        
    	font-family: Arial;
    	font-size: 12px;
    	line-height: 16px;
    	color: #000000;
    	background-color: #FFFFFF;
    	background-repeat: repeat-x;
        padding-left:20px;
        padding-right:20px;
    }
    .celda {
    	font-family: Arial;
    	font-size: 10px;
        margin-left: 50px;
    	line-height: 16px;
    	color: #000000;
    	background-color: #FFFFFF;
    	background-repeat: repeat-x;
         vertical-align:text-top;
    }
    .celda_titulo {
        font-family: Arial;
    	font-size: 12px;
        }
    </style>
    </head>
    <body>
    '.$documentos.'
    </body>
    </head>
    ';
  
$mpdf=new mPDF('c');
$mpdf->SetDisplayMode('fullpage');
$stylesheet = file_get_contents('mpdfstyleA4.css');
$mpdf->WriteHTML($stylesheet,1);
$stylesheet = file_get_contents('mpdfstylePaged.css');
$mpdf->WriteHTML($html);
$mpdf->Output();


}
else
{
    echo "No existe voucher";
}


?>
