<?PHP
class Bitacora {
var $ID_BITACORA;
	var $FECHA;
	var $USUARIO;
	var $EVENTO;
	var $DESCRIPCION;
	var $ARCHIVO;
	var $NUEVO;
	var $CODIGO_RE;
    var $LUGAR_RE;
    var $OBS_RE;
	var $conectas;

	function __construct() {
		$this->conectas = new ClaseConexion();
		$this->conectas->Conectar(); 
	}
	public function Lista() {
		$sql = "select * from BITACORA";
		$res = $this->conectas->EjecutaConsulta($sql);
		while($row=mysql_fetch_array($res)) {
			$c = new BITACORA();
			$c->ID_BITACORA= $row["ID_BITACORA"];
			$c->FECHA= $row["FECHA"];
			$c->USUARIO= $row["USUARIO"];
			$c->EVENTO= $row["EVENTO"];
			$c->DESCRIPCION= $row["DESCRIPCION"];
			$c->ARCHIVO= $row["ARCHIVO"];
			$c->NUEVO= $row["NUEVO"];
			$c->CODIGO_RE= $row["CODIGO_RE"];
            $c->LUGAR_RE= $row["LUGAR_RE"];
            $c->OBS_RE= $row["OBS_RE"];
			$c->conectas=null;
			$array[] = $c;
		}
		return $array;
	}

	public function ListaJSON() {
		$data = $this->Lista();
		return json_encode($data);
	}

	public function ListarID_BITACORA($ID_BITACORAId) {
		$this->ID_BITACORA = $ID_BITACORAId;
		$sql = "select * from BITACORA where ID_BITACORA=$ID_BITACORAId";
		$res = $this->conectas->EjecutaConsulta($sql);
		while($row=mysql_fetch_array($res)) {
			$c = new BITACORA();
			$this->ID_BITACORA= $row["ID_BITACORA"];
			$c->ID_BITACORA= $row["ID_BITACORA"];
			$this->FECHA= $row["FECHA"];
			$c->FECHA= $row["FECHA"];
			$this->USUARIO= $row["USUARIO"];
			$c->USUARIO= $row["USUARIO"];
			$this->EVENTO= $row["EVENTO"];
			$c->EVENTO= $row["EVENTO"];
			$this->DESCRIPCION= $row["DESCRIPCION"];
			$c->DESCRIPCION= $row["DESCRIPCION"];
			$this->ARCHIVO= $row["ARCHIVO"];
			$c->ARCHIVO= $row["ARCHIVO"];
			$this->NUEVO= $row["NUEVO"];
			$c->NUEVO= $row["NUEVO"];
			$this->CODIGO_RE= $row["CODIGO_RE"];
			$c->CODIGO_RE= $row["CODIGO_RE"];
			$c->conectas=null;
            $c->LUGAR_RE= $row["LUGAR_RE"];
            $c->OBS_RE= $row["OBS_RE"];
            $this->LUGAR_RE= $row["LUGAR_RE"];
            $this->OBS_RE= $row["OBS_RE"];
			$array[] = $c;
		}
		return $array;
	}

	public function ListarID_BITACORAJSON($ID_BITACORAId) {
		$data = $this->ListarID_BITACORA($ID_BITACORAId);
		return json_encode($data);
	}
	public function Where($vars) 
    {
		$sql = "select * from BITACORA where $vars"; 
		$res = $this->conectas->EjecutaConsulta($sql);
		while($row=mysql_fetch_array($res)) {
			
			$c = new BITACORA();
			$c->ARCHIVO= $row["ARCHIVO"];
			$c->CODIGO_RE= $row["CODIGO_RE"];
			$c->conectas=null;
			$c->DESCRIPCION= $row["DESCRIPCION"];
			$c->EVENTO= $row["EVENTO"];
			$c->FECHA= $row["FECHA"];
			$c->ID_BITACORA= $row["ID_BITACORA"];
			$c->NUEVO= $row["NUEVO"];
			$c->USUARIO= $row["USUARIO"];
            $c->LUGAR_RE= $row["LUGAR_RE"];
            $c->OBS_RE= $row["OBS_RE"];
            $array[] = $c;
		}
		return $array;
	}

	public function WhereJSON($vars) {
		$data = $this->Where($vars);
		return json_encode($data);
	}

	public function Add() {
        date_default_timezone_set("America/Santiago");
        
           $sql2 = "select * from RESERVA_EXCURSION where CODIGO_RE =".$this->CODIGO_RE;
       	   $res2 = $this->conectas->EjecutaConsulta($sql2);
           while($row=mysql_fetch_array($res2)) 
           {
                 $this->LUGAR_RE = $row["LUGAR_RE"]; 
                 $this->OBS_RE = $row["OBSERVACION_RE"]; 
           }
           
           if ($this->LUGAR_RE != '')
           {
               $sql2 = "SELECT * FROM HOTEL_LUGAR WHERE CODIGO_HL=".$this->LUGAR_RE;
           	   $res3 = $this->conectas->EjecutaConsulta($sql2);
               while($row=mysql_fetch_array($res3)) 
               {
                     $this->LUGAR_RE = $row["DESCRIPCION_HL"]; 
    
               }   
           }
           

        
        $this->FECHA = date('Y-m-d H:i:s');
		$sql = "INSERT INTO BITACORA (FECHA,USUARIO,EVENTO,DESCRIPCION,ARCHIVO,NUEVO,CODIGO_RE,LUGAR_RE,OBS_RE)"; 
		$sql.= "VALUES('".$this->FECHA."','".$this->USUARIO."','".$this->EVENTO."','".$this->DESCRIPCION."','".$this->ARCHIVO."','".$this->NUEVO."','".$this->CODIGO_RE."','".$this->LUGAR_RE."','".$this->OBS_RE."')";
		$res = $this->conectas->EjecutaConsulta($sql);
		return "OK";
	}

	public function Update() {
      
		$sql = "Update  BITACORA set FECHA='".$this->FECHA."',USUARIO='".$this->USUARIO."',EVENTO='".$this->EVENTO."',DESCRIPCION='".$this->DESCRIPCION."',ARCHIVO='".$this->ARCHIVO."',NUEVO='".$this->NUEVO."',CODIGO_RE='".$this->CODIGO_RE."' where ID_BITACORA='".$this->ID_BITACORA."' "; 
		$res = $this->conectas->EjecutaConsulta($sql);
		return "OK";
	}

	public function delete() {
		$sql= "delete from  BITACORA where ID_BITACORA='".$this->ID_BITACORA."' ";
		$res = $this->conectas->EjecutaConsulta($sql);
		return "OK";
	}

}
?>
