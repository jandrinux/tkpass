<?php 
require ("../Clases/ClaseConexion.inc.php");
include( "../Clases/class.TemplatePower.inc.php"); 
require ("../Clases/ClaseUtil.inc.php");

$tpl = new TemplatePower( "../Plantillas/DetalleTarjetasComision.tpl" );

$_Util=new Util;
$miConexion= new ClaseConexion;
$tpl->prepare();
$tpl->assign("fecha", $_Util->Fecha());
$miConexion->Conectar();

$queryProducto=$miConexion->EjecutaSP("Consulta_Producto","'".$_GET['CODIGOPRODUCTO']."'");
$rowProducion= mysql_fetch_assoc($queryProducto);
$tpl->assign("_ROOT.CODIGOPROD",$rowProducion['NOMBRE_PROD']);
$tpl->assign("_ROOT.CODIGOPRODUCTOREAL",$_GET['CODIGOPRODUCTO']);

$miConexion->Conectar();
$queryDistribuidor=$miConexion->EjecutaSP("Consulta_Distribuidor_Codigo","'".$_GET['CODIGODISTRIBUIDOR']."'");
$rowDistribuidor= mysql_fetch_assoc($queryDistribuidor);
$tpl->assign("_ROOT.CODIGODIST",$rowDistribuidor['NOMBRE_FANTASIA_DIST']);
$tpl->assign("_ROOT.CODIGODISTRIBUIDORREAL",$_GET['CODIGODISTRIBUIDOR']);

$PRECION=number_format($_GET['PRECIONACIONAL'],2,",",".");
$PRECION_PESO="$".$PRECION;
$tpl->assign("_ROOT.PRECIONACIONAL2",$_GET['PRECIONACIONAL']);

$PRECIOI=number_format($_GET['PRECIOINTERNACIONAL'],2,",",".");
$PRECIOI_PESO="$".$PRECIOI;
$tpl->assign("_ROOT.PRECIOINTERNACIONAL2",$_GET['PRECIOINTERNACIONAL']);

$TOTALPRECION=number_format($_GET['SUMAPRECIONACIONAL'],2,",",".");
$TOTALPRECION_PESO="$".$TOTALPRECION;
$tpl->assign("_ROOT.SUMAPRECIONACIONAL2",$_GET['SUMAPRECIONACIONAL']);

$TOTALPRECIOI=number_format($_GET['SUMAPRECIOINTERNACIONAL'],2,",",".");
$TOTALPRECIOI_PESO="$".$TOTALPRECIOI;
$tpl->assign("_ROOT.SUMAPRECIOINTERNACIONAL2",$_GET['SUMAPRECIOINTERNACIONAL']);

$tpl->assign("_ROOT.PRECIONACIONAL",$PRECION_PESO);
$tpl->assign("_ROOT.PRECIOINTERNACIONAL",$PRECIOI_PESO);
$tpl->assign("_ROOT.SUMAPRECIONACIONAL",$TOTALPRECION_PESO);
$tpl->assign("_ROOT.SUMAPRECIOINTERNACIONAL",$TOTALPRECIOI_PESO);
/*$miConexion->Conectar();
$queryEstadoTarjeta=$miConexion->EjecutaSP("Consulta_Estado_Tarjeta","1,".$_GET['CODIGOESTADO']."");
$rowEstadoTarjeta= mysql_fetch_assoc($queryEstadoTarjeta);
$tpl->assign("_ROOT.CODIGOEST",$rowEstadoTarjeta['DESCRIPCION_EST']);
$tpl->assign("_ROOT.CODIGOESTADOREAL",$_GET['CODIGOESTADO']);
*/
$miConexion->Conectar();
$queryDetalleComision=$miConexion->EjecutaSP("Consulta_Detalle_Ventas","".$_GET['CODIGODISTRIBUIDOR'].",'".$_GET['CODIGOPRODUCTO']."',".$_GET['PRECIONACIONAL'].",".$_GET['PRECIOINTERNACIONAL']."");
while ($rowDetalleComision = mysql_fetch_assoc($queryDetalleComision))
	{
		$tpl->newBlock("detallecomision");
		$tpl->assign("FOLIO",$rowDetalleComision['FOLIO_TAR']);
		
		
		$miConexion->Conectar();
		$queryUsuarioEmpresa=$miConexion->EjecutaSP("Consulta_Usuario_Codigo","".$rowDetalleComision['USR_EMPRESA_TC']."");
		$rowUsuarioEmpresa= mysql_fetch_assoc($queryUsuarioEmpresa);
		$tpl->assign("EMPRESA",$rowDetalleComision['NOMBRE_FANTASIA_DIST']);
		
		$miConexion->Conectar();
		$queryUsuarioVendedor=$miConexion->EjecutaSP("Consulta_Usuario_Codigo","".$rowDetalleComision['USR_VENDEDOR_TC']."");
	    $rowUsuarioVendedor= mysql_fetch_assoc($queryUsuarioVendedor);
		$tpl->assign("VENDEDOR",$rowUsuarioVendedor['NOMBRE_USUA']." ".$rowUsuarioVendedor['APELLIDO_USUA']);
		
		$miConexion->Conectar();
		$queryUsuarioPromotor=$miConexion->EjecutaSP("Consulta_Promotor_porCodigo","".$rowDetalleComision['USR_PROMOTOR_TC']."");
		$rowUsuarioPromotor= mysql_fetch_assoc($queryUsuarioPromotor);
		$tpl->assign("PROMOTOR",$rowUsuarioPromotor['NOMBRE_USUA']." ".$rowUsuarioPromotor['APELLIDO_USUA']);
		
		$Fecha=$_Util->FormatearFecha($rowDetalleComision['FECHA_TC']);	
		$tpl->assign("FECHA",$Fecha);
		if ($rowDetalleComision['ESTADO_TC']==1)
			{
				$tpl->assign("ESTADO","HABILITADA");
			}
		else
			{
				$tpl->assign("ESTADO","DESHABILITADA");
			}
	}

//	{
//		$tpl->newBlock("masterpines");
//       	$tpl->assign("CODIGO",$rowMasterPines['CODIGO_MP']);
//		$tpl->assign("CARACTERES",$rowMasterPines['CARACTERES_MP']);
//		$tpl->assign("MES",$rowMasterPines['MES_MP']);
//		$tpl->assign("ANNO",$rowMasterPines['ANO_MP']);
//	}		

$tpl->printToScreen();
?>
