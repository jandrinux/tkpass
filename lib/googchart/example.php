<div style="width: 730px; margin: 20px auto; font-family:sans-serif;">
<?php
session_start();
/** Include class */
include( 'GoogChart.class.php' );

/** Create chart */
$chart = new GoogChart();
$sucursales = array ("","Casa Matriz", "Mercado Central", "Aeropuerto", "Patio Bellavista", "Parque Arauco"); 

/*

		Example 1
		Pie chart

*/
//echo $_SESSION['valor1'];
//echo $_SESSION['valor2'];
$total = $_SESSION['valor1'] + $_SESSION['valor2'] + $_SESSION['valor3'] + $_SESSION['valor4']+$_SESSION['valor5'];
$valor1 = (($_SESSION['valor1'] * 100)/ $total);
$valor2 = (($_SESSION['valor2'] * 100)/ $total);
$valor3 = (($_SESSION['valor3'] * 100)/ $total);
$valor4 = (($_SESSION['valor4'] * 100)/ $total);
$valor5 = (($_SESSION['valor5'] * 100)/ $total);
//$valor6 = (($_SESSION['valor6'] * 100)/ $total);
//$valor7 = (($_SESSION['valor7'] * 100)/ $total);
//$valor8 = (($_SESSION['valor8'] * 100)/ $total);

// Set graph data
$data = array(
			'Casa Matriz '.round($valor1,1).'%' => $valor1,
			'Mercado Central '.round($valor2,1).'%' => $valor2,
			'Aeropuerto '.round($valor3,1).'%' => $valor3,
			'Patio Bellavista '.round($valor4,1).'%' => $valor4,
			'Parque Arauco '.round($valor5,1).'%' => $valor5,

		);

// Set graph colors
$color = array(
			'#32BA86',
			'#13568C',
			'#df1313',
            '#DCEC29',
            '#ECBE29',
            
		);

/* # Chart 1 # */
echo '<center><h4>Montos v/s Sucursales</h4></center>';
$chart->setChartAttrs( array(
	'type' => 'pie',
	'title' => 'Gr�fico de Torta',
	'data' => $data,
	'size' => array( 500, 250 ),
	'color' => $color
	));
// Print chart
echo "<center>";
echo $chart;
echo "</center>";
echo "<br><br>";
//$color='#e5e4e3';
//$orden=1; 
echo "<table align='center'  style='border:1px solid #000; color:#000; width:270px; background-color:#FFF;'>";
       echo "<tr style='background:#CCC;'>";
          echo "<td>";
             echo "Sucursales";
          echo "</td>";
         echo "<td>";
           echo "Montos";
         echo "</td>";
         echo "<td>";
           echo "Forma de pago";
         echo "</td>";
         echo "<td>";
           echo "Tipo Servicio";
         echo "</td>";
      echo "</tr>"; 
      
     
      echo "<tr>"; 
      for ($i=1; $i<=5; $i++){  
            if($i%2==0)
				$color='#e5e4e3';
				else
				$color='#ededec';
                		
				echo "<tr style='background-color:".$color."'>";  
                 echo "<td align='center'>";       
                     echo $sucursales[$i]; 
                 echo "</td>";
                 echo "<td align='center'>";       
                     echo number_format($_SESSION['valor'.$i]); 
                 echo "</td>";
                 echo "<td align='center'>";       
                     echo $_SESSION['fop']; 
                 echo "</td>";
                 echo "<td align='center'>";       
                     echo $_SESSION['servicio']; 
                 echo "</td>";
            echo "</tr>";  
      }

      echo "</tr>";   
echo "</table>"; 


?>
</div>