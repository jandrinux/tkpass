<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cuentas Contables</title>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#Layer3 {position:absolute;
	left:14px;
	top:50px;
	width:409px;
	height:46px;
	z-index:14;
}
#Layer2 {position:absolute;
	left:831px;
	top:53px;
	width:132px;
	height:116px;
	z-index:1;
}
-->
</style>
</head>
<script>
function salir()
	{
		window.close();
	}

			
function IngresarCuenta()
	{
	 	if (document.form1.tipocuenta.value=='0')
			{
				alert("Debe Seleccionar el Tipo de Cuenta que desea Ingresar");
				document.form1.tipocuenta.focus();
				return;
			}
		
		if (document.form1.cuenta.value=='')
			{
				alert("Debe Ingresar Cuenta");
				document.form1.cuenta.focus();
				return;
			}
		
		if (document.form1.descripcion.value=='')
			{
				alert("Debe Ingresar Descripción para la cuenta que desea ingresar");
				document.form1.descripcion.focus();
				return;
			}		
		document.form1.guardar.value=1;
		document.form1.submit;
}
function TipoCuenta()
	{
		miPopup = window.open("../PHP/Ingresar_Tipo_Cuenta.php","cue","top=200,left=400,width=550,height=200,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
	
function Filtrar()
	{
		document.form1.filtrar.value=1;
		document.form1.submit();
	}
	
function Paginador(dato)
	{
		form1.pag.value = dato;
		form1.submit();
	}
function ModificarCuentaContable(CODIGO)
	{
		miPopup = window.open("../PHP/Modificar_Cuenta_Contable2.php?CODIGO="+CODIGO,"modificarcuentacontable","top=200,left=400,width=500,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
</script>
<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>
<form id="form1" name="form1" method="post" action="">
  <div class="TituloPlomo" id="Layer3">
    <table width="351" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="82" rowspan="4"><img src="../Imagenes/Crystal_Clear_app_korganizer80x80.png" width="80" height="80" /></td>
        <td width="13" rowspan="4">&nbsp;</td>
        <td width="256"><p>&nbsp;</p></td>
      </tr>
      <tr>
        <td height="22" class="TituloPlomo"><strong>Modificar Cuentas Contables </strong><strong></strong></td>
      </tr>
      <tr>
        <td height="22" class="TituloFecha">{fecha}</td>
      </tr>
      <tr>
        <td height="22" class="TituloFecha">&nbsp;</td>
      </tr>
    </table>
  </div>
  <p>&nbsp;</p>
 <p>&nbsp;</p>
 <p>&nbsp;</p>

<div id="segundodiv" style="position:absolute; left:16px; top:242px; width:710px; height:185px; z-index:3; border:0pt black solid; overflow:auto">
  <table width="698" border="0" cellspacing="0">
    
    <tr>
      <td width="66" class="celdalazul">Codigo</td>
      <td width="150" class="celdalazul">Tipo Cuenta </td>
      <td width="161" class="celdalazul">Cuenta</td>
      <td width="289" class="celdalazul">Descripci&oacute;n</td>
      <td width="22" class="celdalazul">&nbsp;</td>
    </tr>
	 <!-- START BLOCK : cuentascontables -->
    <tr>
      <td {CLASES} >&nbsp;{CODIGO}</td>
      <td {CLASES} >&nbsp;{CODIGOTIPOCUENTA}</td>
      <td {CLASES} >&nbsp;{CUENTA}</td>
      <td {CLASES} >&nbsp;{DESCRIPCION}</td>
      <td {CLASES} ><img src="../Imagenes/modify.gif" width="16" height="16"  alt="Modificar Cuenta Contable" style="cursor:hand" onclick="ModificarCuentaContable({CODIGO})"/></td>
    </tr>
	 <!-- END BLOCK : cuentascontables -->
  </table>
  <p>&nbsp;</p>
</div>
<div id="segundodiv" style="position:absolute; left:16px; top:242px; width:710px; height:28px; z-index:3; border:0pt black solid;">
 <table width="698" border="0" cellspacing="0">
    
    <tr>
      <td width="66" class="celdalazul">Codigo</td>
      <td width="151" class="celdalazul">Tipo Cuenta </td>
      <td width="160" class="celdalazul">Cuenta</td>
      <td width="313" class="celdalazul">Descripci&oacute;n</td>
      </tr>
	</table>
</div>
<div id="layer" style="position:absolute; left:15px; top:144px; width:700px; height:74px; z-index:3; border:0pt black solid;">
  <table width="697" border="0" cellspacing="0">
    <tr>
      <td width="154" class="celdalazul">Tipo Cuenta </td>
      <td width="209" class="celdalCELEST">&nbsp;
	  <select name="tipocuenta" class="seleccionTurist_180">
          <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
	  			<!-- START BLOCK : cuenta -->
       			<option value="{CODIGOTIPOCUENTA}" class="seleccion2" {SELECCIONAR}>{DESCRIPCIONTIPOCUENTA}</option>
	   			<!-- END BLOCK : cuenta-->
        </select></td>
      <td width="156" class="celdalazul">Cuenta</td>
      <td width="170" class="celdalCELEST"><label>
        <input name="cuenta" type="text" class="seleccionTurist_180" id="cuenta" maxlength="12" />
      </label></td>
    </tr>
    <tr>
      <td class="celdalazul">Descripci&oacute;n</td>
      <td colspan="3" class="celdalCELEST"><label>
      <input name="descripcion" type="text" class="seleccionTurist_250" id="descripcion" />
      </label>
      <div style="position:absolute; left:2px; top:307px; width:719px; height:38px; z-index:3; border:0pt black solid;">
            <table width="696" border="0" cellspacing="0">
              <tr>
                <!-- START BLOCK : tablageneral -->
                <td width="159" class="celdalazul">Total</td>
                <td width="805" class="celdalCELEST"><strong>{paginador}&nbsp; </strong></td>
                <!-- END BLOCK : tablageneral -->
              </tr>
            </table>
          </div>
        <img src="../Imagenes/120px-Crystal_Clear_action_build19x19.png" width="19" height="19" alt="Activar Filtros" style="cursor:hand" onclick="Filtrar()" />
          <input type="hidden" name="filtrar" value="0" />
          <input type="hidden" name="pag" /></td>
    </tr>
  </table>
</div>
</form>
</body>
</html>
