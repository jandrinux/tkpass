<?php
/**
 * Clase que contiene utilidades variadas.
 *
 * @category		Utils
 * @version 		1.0
 */
Class Util
{
	/**
     * Funci�n que devuelve la fecha actual 
     *
     * @param  null
     * @return date fecha actual
     */
    function bp_to_date($bp)
    {
        //consulto y selecciono registros
        $a�o = substr($bp, 0, 2);
        $mes = substr($bp, 2, 2);
        $dia = substr($bp, 4, 2);

        $fecha_bp = "20".$a�o."-".$mes."-".$dia;
        return $fecha_bp;
    }

    
	function Fecha()
    {
        // setlocale(LC_ALL,"es_CL.UTF8"); acentos
		// setlocale(LC_ALL,"es_ES.ISO8859-15"); //ingles
		setlocale(LC_ALL,"es_ES");
        $salida="%A, %d de  %B  de %Y";
        $actual="%Y%m%d";
        $factual="%d/%m/%Y";		
        $horini = getdate();
        $fechaini=mktime($horini["hours"],$horini["minutes"],$horini["seconds"],$horini["mon"],$horini["mday"],$horini["year"]);
        $Fecdia = strftime($salida,$fechaini);		
        $Actual = strftime($actual,$fechaini);
		return ucFirst($Fecdia);
    }
	
		function FechaHoy()
    {
        setlocale(LC_ALL,"es_CL.UTF8");
        $salida="%d/%m/%Y";
        $actual="%Y%m%d";
        $factual="%d/%m/%Y";		
        $horini = getdate();
        $fechaini=mktime($horini["hours"],$horini["minutes"],$horini["seconds"],$horini["mon"],$horini["mday"],$horini["year"]);
        $Fecdia = strftime($salida,$fechaini);		
        $Actual = strftime($actual,$fechaini);
		return ucFirst($Fecdia);
    }
	
	function FechaDos()
    {
        setlocale(LC_ALL,"es_CL.UTF8");
        $actual="%Y%m%d";
        $factual="%d/%m/%Y";		
        $horini = getdate();
        $fechaini=mktime($horini["hours"],$horini["minutes"],$horini["seconds"],$horini["mon"],$horini["mday"],$horini["year"]);
        $Actual = strftime($factual,$fechaini);
		return ucFirst($Actual);
    }
    
	/*
	Funcion que recibe la fecha en formato a�o-mes-dia y
	la devuelve dia/mes/a�o
	*/
	function FormatearFecha($variable)
	{
	$fecha=str_replace("-","",$variable);
		$diaFecha=substr(trim($fecha),6,2);
		$mesFecha=substr(trim($fecha),4,2);
		$anoFecha=substr($fecha,0,4);
		$fechaDesplegada=$diaFecha."/".$mesFecha."/".$anoFecha;
		return $fechaDesplegada;
	}
    
   	function FormatearFechaHora($variable)
	{
        $FechaHora = explode(" ", $variable);
        $variable = $FechaHora[0];
        $hora = $FechaHora[1]; 

	    $fecha=str_replace("-","",$variable);
		$diaFecha=substr(trim($fecha),6,2);
		$mesFecha=substr(trim($fecha),4,2);
		$anoFecha=substr($fecha,0,4);
		$fechaDesplegada=$diaFecha."-".$mesFecha."-".$anoFecha;
  
		return $fechaDesplegada." ".$hora;

	}
    

    
   	function DesformatearFechaHora($variable)
	{
        $FechaHora = explode(" ", $variable);
        $variable = $FechaHora[0];
        $hora = $FechaHora[1]; 

	    $fecha=str_replace("-","",$variable);
        
		$diaFecha=substr(trim($fecha),6,2);
		$mesFecha=substr(trim($fecha),4,2);
		$anoFecha=substr($fecha,0,4);
        
		$fechaDesplegada=$anoFecha."-".$mesFecha."-".$diaFecha;
  
		return $fechaDesplegada." 00:00:00";

	}
    
   	function DesformatearFechaHora2($variable)
	{
        $FechaHora = explode(" ", $variable);
        $variable = $FechaHora[0];
        $hora = $FechaHora[1]; 

	    $fecha=str_replace("/","-",$variable);
        
        $partes = explode("-", $fecha);
        $anoFecha = $partes[2];
        $mesFecha = $partes[1];
        $diaFecha = $partes[0];
		 $fechaDesplegada=$anoFecha."-".$mesFecha."-".$diaFecha." ".$hora;
  
		return $fechaDesplegada;

	}
	
    function sumar($h1,$h2) //suma horas
    {
        $h2h = date('H', strtotime($h2));
        $h2m = date('i', strtotime($h2));
        $h2s = date('s', strtotime($h2));
        $hora2 =$h2h." hour ". $h2m ." min ".$h2s ." second";
        
        $horas_sumadas= $h1." + ". $hora2;
        $text=date('H:i:s', strtotime($horas_sumadas)) ;
        
        return $text;
    }
	
	/*
	Funcion que recibe la fecha en formato DiaMesA�o y
	la devuelve dia/mes/a�o
	*/
	function FormatearFecha_DMA($variable)
	{
	$fecha=$variable;
		$diaFecha=substr(trim($fecha),0,2);
		$mesFecha=substr(trim($fecha),2,2);
		$anoFecha=substr($fecha,4,4);
		$fechaDesplegada=$diaFecha."/".$mesFecha."/".$anoFecha;
		return $fechaDesplegada;
	}
	/*
	Funcion que recibe la fecha en formato dia/mes/a�o y
	la devuelve a�o-mes-dia
	*/
	function DesformatearFecha($variable)
	{
	$fecha=str_replace("/","",$variable);
		$diaFecha=substr($fecha,0,2);
		$mesFecha=substr($fecha,2,2);
		$anoFecha=substr($fecha,4,4);
		$fechaDesformateada=$anoFecha."-".$mesFecha."-".$diaFecha;
		return $fechaDesformateada;
	}
    
	function DesformatearFechaJunto($variable)
	{
	$fecha=str_replace("/","",$variable);
		$diaFecha=substr($fecha,0,2);
		$mesFecha=substr($fecha,2,2);
		$anoFecha=substr($fecha,4,4);
		$fechaDesformateadaJunto=$anoFecha.$mesFecha.$diaFecha;
		return $fechaDesformateadaJunto;
	}
    
    
    
    function SumarHoras($hora_ex, $intervalo)
    {
        if ($intervalo >= 60)
        {
            $res1 = $intervalo / 60;
            $hora = (integer)$res1;
            $minutos = $intervalo % 60;
          
        }
        else
        {
            $hora = 00;
            $minutos = $intervalo;
        }
        
        $minuit = strtotime("00:00:00.00"); 
        $hora_ex= $hora_ex.".00"; 
        $timestamp1 = strtotime($hora_ex)-$minuit; 
        
        $hora2=$hora.":".$minutos.":"."00.00"; 
        $timestamp2 = strtotime($hora2)-$minuit; 
        
        $SUMA=$timestamp1-$timestamp2+$minuit; 
        $hora_rec = date("H:i",$SUMA);
        return $hora_rec;
    }
    
    function validaRut($rut)
    {
        if(strpos($rut,"-")==false){
            $RUT[0] = substr($rut, 0, -1);
            $RUT[1] = substr($rut, -1);
        }else{
            $RUT = explode("-", trim($rut));
        }
        $elRut = str_replace(".", "", trim($RUT[0]));
        $factor = 2;
        for($i = strlen($elRut)-1; $i >= 0; $i--):
            $factor = $factor > 7 ? 2 : $factor;
            $suma += $elRut{$i}*$factor++;
        endfor;
        $resto = $suma % 11;
        $dv = 11 - $resto;
        if($dv == 11){
            $dv=0;
        }else if($dv == 10){
            $dv="k";
        }else{
            $dv=$dv;
        }
       if($dv == trim(strtolower($RUT[1]))){
           return "valido";
       }else{
           return "invalido";
       }
   
   }
   
   function fecha_pagos()
   {
        $fecha = date('Y-m-j');
        $dia_hoy = date('d');
        $mes_hoy = date('m');
        
        
        if ($mes_hoy == 12)
        {
            
            if (($dia_hoy > 20) && ($dia_hoy<=31))
            {
                $fecha_desde = date('Y-m-21');
                
                $fecha_hasta = strtotime ( '+1 month' , strtotime ( $fecha ) ) ;
                $fecha_hasta = date ( 'Y-m-20' , $fecha_hasta );
                 
            }
            else
            {
        
                 $fecha_desde = strtotime ( '-1 month' , strtotime ( $fecha ) ) ;
                 $fecha_desde = date ( 'Y-m-21' , $fecha_desde );
                 
                 $fecha_hasta = strtotime ( $fecha );
                 $fecha_hasta = date ( 'Y-m-20', $fecha_hasta );
        
            }
            
        }
        else
        {
            if (($dia_hoy > 20) && ($dia_hoy<31))
            {
                $fecha_desde = date('Y-m-21');
                
                $fecha_hasta = strtotime ( '+1 month' , strtotime ( $fecha ) ) ;
                $fecha_hasta = date ( 'Y-m-20' , $fecha_hasta );
                 
            }
        	else
        	{
                 $fecha_desde = strtotime ( '-1 month' , strtotime ( $fecha ) ) ;
                 $fecha_desde = date ( 'Y-m-21' , $fecha_desde );
                 
                 
                 $fecha_hasta = strtotime ( $fecha );
                 $fecha_hasta = date ( 'Y-m-20' , $fecha_hasta );
                
        
        	}
        }
        
        return array('fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta);
   }
    
    function array_recibe($url_array) 
    { 
    // $tmp = stripslashes($url_array); 
    // $tmp = urldecode($tmp); 
     echo $tmp = unserialize($url_array); 

    return $tmp; 
    }
    
    
	/*
	Funcion para realizar una paginaci�n de registros
	*/
	function paginar($actual, $total, $porPagina, $enlace, $maxpags=0) 
    {
        if ($actual=="")
            {
                $actual=1;
            }
        $totalPaginas = ceil($total/$porPagina);
        $anterior = $actual - 1;
        $posterior = $actual + 1;
        # condicion ? entonces : sino;
        $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
        $maximo = $maxpags ? min($totalPaginas, $actual+floor($maxpags/2)): $totalPaginas;
        if ($actual>1)
            {
                $texto = "<a href=\"$enlace\" onclick='Paginador($anterior);' >&laquo;Anterior</a> ";
            }
        else
            {
                $texto = "<b>&laquo;";
            }
        # $var = $var  expresion equivalente $var .=
		if ($minimo!=1)
            {
                $texto.= "... ";
            }
        for ($i=$minimo; $i<$actual; $i++)
            {
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($i);\">$i</a> ";
            }
        $texto .= "<b>$actual</b> ";
        for ($i=$actual+1; $i<=$maximo ; $i++)
            {
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($i);\">$i</a> ";
            }
        if ($maximo!=$totalPaginas)
            {
                $texto.= "... ";
            }
        if ($actual<$totalPaginas)
            {
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($posterior);\">Siguiente&raquo;</a>";
            }
        else
            {
                $texto .= "<b>&raquo;</b>";
            }
        return $texto;
}
/*
	Funcion para realizar una paginaci�n por filtros
*/
function paginarporfechas($actual, $total, $porPagina, $enlace, $maxpags=0,$actualiza,$fechauno,$fechados) 
    {
	//echo "<br>asdasd".$actual;
        if ($actual=="")
            {
                $actual=1;
            }
        $totalPaginas = ceil($total/$porPagina);
        $anterior = $actual - 1;
        $posterior = $actual + 1;
        # condicion ? entonces : sino;
        $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
        $maximo = $maxpags ? min($totalPaginas, $actual+floor($maxpags/2)): $totalPaginas;
        if ($actual>1)
            {
                if ($fechados=='')
                    {
                        $texto = "<a href=\"$enlace\" onclick=\"PaginadorDos($anterior,$actualiza,'$fechauno');\" >&laquo;Anterior</a> ";
                    }
                else
                    {
                        $texto = "<a href=\"$enlace\" onclick=\"Paginador($anterior,$actualiza,'$fechauno','$fechados');\" >&laquo;Anterior</a> ";
                    }
           }
        else
           {
               $texto = "<b>&laquo;";
           }
        # $var = $var  expresion equivalente $var .=
		if ($minimo!=1)
            {
                $texto.= "... ";
            }
        for ($i=$minimo; $i<$actual; $i++)
            {
				if ($fechados=='')
				{
				$texto .= "<a href=\"$enlace\" onclick=\"PaginadorDos($i,$actualiza,'$fechauno');\">$i</a> ";
				}
				else
				{
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($i,$actualiza,'$fechauno','$fechados');\">$i</a> ";
				}
            }
        $texto .= "<b>$actual</b> ";
        for ($i=$actual+1; $i<=$maximo ; $i++)
            {
			if ($fechados=='')
			{
				$texto .= "<a href=\"$enlace\" onclick=\"PaginadorDos($i,$actualiza,'$fechauno');\">$i</a> ";
			}
			else
			{
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($i,$actualiza,'$fechauno','$fechados');\">$i</a> ";
			}	
            }
        if ($maximo!=$totalPaginas)
            {
                $texto.= "... ";
            }
        if ($actual<$totalPaginas)
            {
			if ($fechados=='')
			{
			$texto .= "<a href=\"$enlace\" onclick=\"PaginadorDos($posterior,$actualiza,'$fechauno');\">Siguiente&raquo;</a>";
			}
			else
			{
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($posterior,$actualiza,'$fechauno','$fechados');\">Siguiente&raquo;</a>";
				}
            }
        else
            {
                $texto .= "<b>&raquo;</b>";
            }
        return $texto;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function paginar_comisiones($actual,$total,$porPagina,$enlace,$maxpags=0,$filtrar,$producto,$categoria,$precio) 
    {
	//echo $producto;
	//echo $categoria;
	//echo $precio;
	//echo $actual;
	 	if ($actual=="")
            {
                $actual=1;
            }
		 $totalPaginas = ceil($total/$porPagina);
         $anterior = $actual - 1;
         $posterior = $actual + 1;
         # condicion ? entonces : sino;
         $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
         $maximo = $maxpags ? min($totalPaginas, $actual+floor($maxpags/2)): $totalPaginas;
         if ($actual>1)
            {
				if ($producto!='0' and $categoria=='0' and $precio=='')
                    {
                        $texto = "<a href=\"$enlace\" onclick=\"Paginador1($anterior,$filtrar,'$producto');\" >&laquo;Anterior</a> ";
                    }
				else
					{
						if ($producto!='0' and $categoria!='0' and $precio=='')
							{
								 $texto = "<a href=\"$enlace\" onclick=\"Paginador2($anterior,$filtrar,'$producto','$categoria');\" >&laquo;Anterior</a> ";
							}
						else
							{
							//echo $producto;
							//echo $categoria;
							//echo $precio;
							//exit;
								if ($producto!='0' and $categoria!='0' and $precio!='')
									{
										 $texto = "<a href=\"$enlace\" onclick=\"Paginador3($anterior,$filtrar,'$producto','$categoria',$precio);\" >&laquo;Anterior</a> ";
									}
								else
									{
										if ($producto=='0' and $categoria!='0' and $precio=='')
											{
												$texto = "<a href=\"$enlace\" onclick=\"Paginador4($anterior,$filtrar,'$categoria');\" >&laquo;Anterior</a> ";
											}
										else
											{
												if ($producto=='0' and $categoria!='0' and $precio!='')
													{
														$texto = "<a href=\"$enlace\" onclick=\"Paginador5($anterior,$filtrar,'$categoria',$precio);\" >&laquo;Anterior</a> ";
													}
												else
													{
														if ($producto=='0' and $categoria=='0' and $precio!='')
															{
															//echo 'primero';
																$texto = "<a href=\"$enlace\" onclick=\"Paginador6($anterior,$filtrar,$precio);\" >&laquo;Anterior</a> ";
															}
														else
															{
																if ($producto!='0' and $categoria=='0' and $precio!='')
																	{
																$texto = "<a href=\"$enlace\" onclick=\"Paginador7($anterior,$filtrar,'$producto',$precio);\" >&laquo;Anterior</a> ";
																}
																else
																{
																	$texto = "<a href=\"$enlace\" onclick=\"Paginador8($anterior,$filtrar);\" >&laquo;Anterior</a> ";
																}
															}
													}
											}
									}
							}
					}
			}
		else
           {
               $texto = "<b>&laquo;";
           }
		if ($minimo!=1)
            {
                $texto.= "... ";
            }
		for ($i=$minimo; $i<$actual; $i++)
            {
				if ($producto!='0' and $categoria=='0' and $precio=='')
					{
						$texto.= "<a href=\"$enlace\" onclick=\"Paginador1($i,$filtrar,'$producto');\">$i</a> ";
					}
				else
					{
						if ($producto!='0' and $categoria!='0' and $precio=='')
							{
                				$texto.= "<a href=\"$enlace\" onclick=\"Paginador2($i,$filtrar,'$producto','$categoria');\">$i</a> ";
							}
						else
							{
							//echo $producto;
							//echo $categoria;
							///echo $precio;
							//exit;
								if ($producto!='0' and $categoria!='0' and $precio!='')
									{
											$texto.= "<a href=\"$enlace\" onclick=\"Paginador3($i,$filtrar,'$producto','$categoria',$precio);\">$i</a> ";
									}
								else
									{
										if ($producto=='0' and $categoria!='0' and $precio=='')
											{
												$texto.= "<a href=\"$enlace\" onclick=\"Paginador4($i,$filtrar,'$categoria');\">$i</a> ";
											}
										else
											{
												if ($producto=='0' and $categoria!='0' and $precio!='')
													{
														$texto.= "<a href=\"$enlace\" onclick=\"Paginador5($i,$filtrar,'$categoria',$precio);\">$i</a> ";
													}
												else
													{
														if ($producto=='0' and $categoria=='0' and $precio!='')
															{
															//echo 'segundo';
																$texto.= "<a href=\"$enlace\" onclick=\"Paginador6($i,$filtrar,$precio);\">$i</a> ";
																//echo $texto;
															}
														else
															{
															
															if ($producto!='0' and $categoria=='0' and $precio!='')
															{
																$texto.= "<a href=\"$enlace\" onclick=\"Paginador7($i,$filtrar,'$producto',$precio);\">$i</a> ";
															}
															else
															{
															$texto.= "<a href=\"$enlace\" onclick=\"Paginador8($i,$filtrar);\">$i</a> ";
															}
															}
													}	
											}
									}
									
							}
					}
            }
		$texto .= "<b>$actual</b> ";
        for ($i=$actual+1; $i<=$maximo ; $i++)
            {
				if ($producto!='0' and $categoria=='0' and $precio=='')
					{
						$texto.= "<a href=\"$enlace\" onclick=\"Paginador1($i,$filtrar,'$producto');\">$i</a> ";
					}
				else
					{
						if ($producto!='0' and $categoria!='0' and $precio=='')
							{
                				$texto.= "<a href=\"$enlace\" onclick=\"Paginador2($i,$filtrar,'$producto','$categoria');\">$i</a> ";
							}
						else
							{
								if ($producto!='0' and $categoria!='0' and $precio!='')
									{
									
								//	echo $producto;
							//echo $categoria;
						//	echo $precio;
						//	exit;
											$texto.= "<a href=\"$enlace\" onclick=\"Paginador3($i,$filtrar,'$producto','$categoria',$precio);\">$i</a> ";
									}
								else
									{
										if ($producto=='0' and $categoria!='0' and $precio=='')
											{
												$texto.= "<a href=\"$enlace\" onclick=\"Paginador4($i,$filtrar,'$categoria');\">$i</a> ";
											}
										else
											{
												if ($producto=='0' and $categoria!='0' and $precio!='')
													{
														$texto.= "<a href=\"$enlace\" onclick=\"Paginador5($i,$filtrar,'$categoria',$precio);\">$i</a> ";
													}
												else
													{
														if ($producto=='0' and $categoria=='0' and $precio!='')
															{
															//echo 'tercero';
																$texto.= "<a href=\"$enlace\" onclick=\"Paginador6($i,$filtrar,$precio);\">$i</a> ";
															}
														else
															{
															if ($producto=='0' and $categoria=='0' and $precio!='')
															{
																$texto.= "<a href=\"$enlace\" onclick=\"Paginador7($i,$filtrar,'$producto',$precio);\">$i</a> ";
																}
															else
															{
																$texto.= "<a href=\"$enlace\" onclick=\"Paginador8($i,$filtrar);\">$i</a> ";
															}
															}
													}	
											}
									}
									
							}
					}
            }
		if ($maximo!=$totalPaginas)
            {
                $texto.= "... ";
            }
        if ($actual<$totalPaginas)
            {
				if ($producto!='0' and $categoria=='0' and $precio=='')
					{
						$texto.= "<a href=\"$enlace\" onclick=\"Paginador1($posterior,$filtrar,'$producto');\">Siguiente&raquo;</a>";
					}
				else
					{
						if ($producto!='0' and $categoria!='0' and $precio=='')
							{
                				$texto.= "<a href=\"$enlace\" onclick=\"Paginador2($posterior,$filtrar,'$producto','$categoria');\">Siguiente&raquo;</a>";
							}
						else
							{
						//	echo $producto;
						//	echo $categoria;
						//	echo $precio;
						//	exit;
								if ($producto!='0' and $categoria!='0' and $precio!='')
									{
										$texto.= "<a href=\"$enlace\" onclick=\"Paginador3($posterior,$filtrar,'$producto','$categoria',$precio);\">Siguiente&raquo;</a>";
									}
								else
									{
										if ($producto=='0' and $categoria!='0' and $precio=='')
											{
												$texto.= "<a href=\"$enlace\" onclick=\"Paginador4($posterior,$filtrar,'$categoria');\">Siguiente&raquo;</a>";
											}
										else
											{
												if ($producto=='0' and $categoria!='0' and $precio!='')
													{
														$texto.= "<a href=\"$enlace\" onclick=\"Paginador5($posterior,$filtrar,'$categoria',$precio);\">Siguiente&raquo;</a>";
													}
												else
													{
														if ($producto=='0' and $categoria=='0' and $precio!='')
															{
																$texto.= "<a href=\"$enlace\" onclick=\"Paginador6($posterior,$filtrar,$precio);\">Siguiente&raquo;</a>";
															}
														else
															{
															if ($producto=='0' and $categoria=='0' and $precio!='')
															{
																$texto.= "<a href=\"$enlace\" onclick=\"Paginador7($posterior,$filtrar,'$producto',$precio);\">Siguiente&raquo;</a>";
																}
															else
															{
															$texto.= "<a href=\"$enlace\" onclick=\"Paginador8($posterior,$filtrar);\">Siguiente&raquo;</a>";
															}
															}
													}
											}
									}
							}
					}
            }
        else
            {
                $texto.= "<b>&raquo;</b>";
            }
	// echo "eww".$texto;
        return $texto;
	}
       /* if ($actual=="")
            {
                $actual=1;
            }
        $totalPaginas = ceil($total/$porPagina);
        $anterior = $actual - 1;
        $posterior = $actual + 1;
        # condicion ? entonces : sino;
        $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
        $maximo = $maxpags ? min($totalPaginas, $actual+floor($maxpags/2)): $totalPaginas;
        if ($actual>1)
            {
                if ($producto!='' and $categoria=='' and  $precio=='')
                    {
                        $texto = "<a href=\"$enlace\" onclick=\"Paginador1($anterior,$filtrar,'$producto');\" >&laquo;Anterior</a> ";
                    }
                else
                    {
						 if ($producto!='' and $categoria!='' and  $precio=='')
						 	{
                        		 $texto = "<a href=\"$enlace\" onclick=\"Paginador1($anterior,$filtrar,'$producto','$categoria');\" >&laquo;Anterior</a> ";
							}
						else
							{
								if ($producto!='' and $categoria!='' and  $precio!='')
									{
										 $texto = "<a href=\"$enlace\" onclick=\"Paginador1($anterior,$filtrar,'$producto','$categoria',$precio);\" >&laquo;Anterior</a> ";
									}
								else
									{
										if ($producto=='' and $categoria!='' and  $precio=='')
											{
												$texto = "<a href=\"$enlace\" onclick=\"Paginador1($anterior,$filtrar,'$categoria');\" >&laquo;Anterior</a> ";
											}
										else
											{
											}
									}
							}
                    }
           }
        else
           {
               $texto = "<b>&laquo;";
           }
        # $var = $var  expresion equivalente $var .=
		/*if ($minimo!=1)
            {
                $texto.= "... ";
            }
        for ($i=$minimo; $i<$actual; $i++)
            {
				if ($fechados=='')
				{
				$texto .= "<a href=\"$enlace\" onclick=\"PaginadorDos($i,$actualiza,'$fechauno');\">$i</a> ";
				}
				else
				{
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($i,$actualiza,'$fechauno','$fechados');\">$i</a> ";
				}
            }
        $texto .= "<b>$actual</b> ";
        for ($i=$actual+1; $i<=$maximo ; $i++)
            {
			if ($fechados=='')
			{
				$texto .= "<a href=\"$enlace\" onclick=\"PaginadorDos($i,$actualiza,'$fechauno');\">$i</a> ";
			}
			else
			{
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($i,$actualiza,'$fechauno','$fechados');\">$i</a> ";
			}	
            }
        if ($maximo!=$totalPaginas)
            {
                $texto.= "... ";
            }
        if ($actual<$totalPaginas)
            {
			if ($fechados=='')
			{
			$texto .= "<a href=\"$enlace\" onclick=\"PaginadorDos($posterior,$actualiza,'$fechauno');\">Siguiente&raquo;</a>";
			}
			else
			{
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($posterior,$actualiza,'$fechauno','$fechados');\">Siguiente&raquo;</a>";
				}
            }
        else
            {
                $texto .= "<b>&raquo;</b>";
            }
        return $texto;*/
/*}*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function paginarporfiltros($actual, $total, $porPagina, $enlace, $maxpags=0,$consultar,$tipousuario,$etarjeta,$producto,$servicio,$fechainicial,$fechafinal) 
{
  if ($actual=="")
     {
        $actual=1;
     }
 $totalPaginas = ceil($total/$porPagina);
 $anterior = $actual - 1;
 $posterior = $actual + 1;
 # condicion ? entonces : sino;
 $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
 $maximo = $maxpags ? min($totalPaginas, $actual+floor($maxpags/2)): $totalPaginas;
 //echo $actual;
 //echo  $anterior;
 //echo $posterior;
 if ($actual>1)
	 {
		if (($tipousuario=='' or $tipousuario==0) and ($etarjeta=='' or $etarjeta==0) and ($producto=='' or $producto=='0x') and ($servicio=='' or $servicio==0) and $fechainicial=='' and  $fechafinal=='')
		  {
			$texto = "<a href=\"$enlace\" onclick=\"Paginador_Uno($anterior,$consultar);\" >&laquo;Anterior</a> ";
		  }
		else
		 {
			if ($tipousuario!=0 and $etarjeta==0 and $producto=='0x' and $servicio==0 and $fechainicial=='' and  $fechafinal=='')
			  {
				 $texto = "<a href=\"$enlace\" onclick=\"PaginadorUno($anterior,$consultar,$tipousuario);\" >&laquo;Anterior</a> ";
			  }
			else
			 {
			   if ($tipousuario!=0 and $etarjeta!=0 and $producto=='0x' and $servicio==0 and $fechainicial=='' and  $fechafinal=='')
			    {
				   $texto = "<a href=\"$enlace\" onclick=\"PaginadorDos($anterior,$consultar,$tipousuario,$etarjeta);\" >&laquo;Anterior</a> ";
				}
			   else
				{
				  if ($tipousuario!=0 and $etarjeta!=0 and $producto!='0x' and $servicio==0 and $fechainicial=='' and  $fechafinal=='')
				  	{
					   $texto = "<a href=\"$enlace\" onclick=\"PaginadorTres($anterior,$consultar,$tipousuario,$etarjeta,'$producto');\" >&laquo;Anterior</a> ";
					}
				  else
					{
					   if ($tipousuario!=0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
					   	{
						  $texto = "<a href=\"$enlace\" onclick=\"PaginadorCuatro($anterior,$consultar,$tipousuario,$etarjeta,'$producto','$servicio');\" >&laquo;Anterior</a> ";
						}
					   else
						{
							if ($tipousuario!=0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
							 {
								$texto = "<a href=\"$enlace\" onclick=\"PaginadorCinco($anterior,$consultar,$tipousuario,$etarjeta,'$producto','$servicio','$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
							 }
							else
							 {
								if ($tipousuario==0 and $etarjeta!=0 and $producto=='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
								 {
								   $texto = "<a href=\"$enlace\" onclick=\"PaginadorSeis($anterior,$consultar,$etarjeta);\" >&laquo;Anterior</a> ";
								 }
								else
								 {
									if ($tipousuario==0 and $etarjeta!=0 and $producto!='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
									 {
									   $texto = "<a href=\"$enlace\" onclick=\"PaginadorSiete($anterior,$consultar,$etarjeta,'$producto');\" >&laquo;Anterior</a> ";
									 }
									else
									 {
									   if ($tipousuario==0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
										{
										  $texto = "<a href=\"$enlace\" onclick=\"PaginadorOcho($anterior,$consultar,$etarjeta,'$producto','$servicio');\" >&laquo;Anterior</a> ";
										}
										else
										{
										 if ($tipousuario==0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
										 {
											$texto = "<a href=\"$enlace\" onclick=\"PaginadorNueve($anterior,$consultar,$etarjeta,'$producto','$servicio','$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
										 }
										else
										 {
										  if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
											{
											 $texto = "<a href=\"$enlace\" onclick=\"PaginadorDiez($anterior,$consultar,'$producto');\" >&laquo;Anterior</a> ";
											}
										   else
											{
											 if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
											 {
												$texto = "<a href=\"$enlace\" onclick=\"PaginadorOnce($anterior,$consultar,'$producto','$servicio');\" >&laquo;Anterior</a> ";
											 }
											else
											 {
											  if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
											  {
												$texto = "<a href=\"$enlace\" onclick=\"PaginadorDoce($anterior,$consultar,'$producto','$servicio','$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
											  }
											  else
												{
												 if ($tipousuario==0 and $etarjeta==0 and $producto=='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
												 {
												  $texto = "<a href=\"$enlace\" onclick=\"PaginadorTrece($anterior,$consultar,'$servicio');\" >&laquo;Anterior</a> ";
												 }
												else
												 {
												   if ($tipousuario==0 and $etarjeta==0 and $producto=='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
													{
													  $texto = "<a href=\"$enlace\" onclick=\"PaginadorCatorce($anterior,$consultar,'$servicio','$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";																																		
													}
												  else
													{
													if ($tipousuario!=0 and $etarjeta==0 and $producto!='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
													{
													 $texto = "<a href=\"$enlace\" onclick=\"PaginadorQuince($anterior,$consultar,$tipousuario,'$producto');\" >&laquo;Anterior</a> ";
													 }
													 else
													  {
													  	if ($tipousuario!=0 and $etarjeta==0 and $producto=='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
														 {
														 	$texto = "<a href=\"$enlace\" onclick=\"PaginadorDiezSeis($anterior,$consultar,$tipousuario,'$servicio');\" >&laquo;Anterior</a> ";
														 }
														else
														 {
														 	if ($tipousuario!=0 and $etarjeta==0 and $producto=='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
															 {
															   $texto = "<a href=\"$enlace\" onclick=\"PaginadorDiezSiete($anterior,$consultar,$tipousuario,'$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
															 }
															else
															{
																if ($tipousuario==0 and $etarjeta!=0 and $producto=='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
																{
																	$texto = "<a href=\"$enlace\" onclick=\"PaginadorDiezOcho($anterior,$consultar,$etarjeta,'$servicio');\" >&laquo;Anterior</a> ";
																}
																else
																{
																	if ($tipousuario==0 and $etarjeta!=0 and $producto=='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
																	{
																	 $texto = "<a href=\"$enlace\" onclick=\"PaginadorDiezNueve($anterior,$consultar,$etarjeta,'$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
																	}
																	else
																	{
																		if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
																		{
																			 $texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinte($anterior,$consultar,'$producto','$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
																		}
																	   else
																	    {
																			if ($tipousuario==0 and $etarjeta==0 and $producto=='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
																				{
																					 $texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteUno($anterior,$consultar,'$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
																				}
																		}
																		
																	}
																}
															}
														 } 
													  }
																																	
																													
																																}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
	else
           {
               $texto = "<b>&laquo;";
           }		
	# $var = $var  expresion equivalente $var .=
	if ($minimo!=1)
       {
           $texto.= "... ";
       }
	 for ($i=$minimo; $i<$actual; $i++)
       {
	   	if (($tipousuario=='' or $tipousuario==0) and ($etarjeta=='' or $etarjeta==0) and ($producto=='' or $producto=='0x') and ($servicio=='' or $servicio==0) and $fechainicial=='' and  $fechafinal=='')
		  {
			$texto.= "<a href=\"$enlace\" onclick=\"Paginador_Uno($i,$consultar);\" >$i</a> ";
		  }
		else
		 {
			if ($tipousuario!=0 and $etarjeta==0 and $producto=='0x' and $servicio==0 and $fechainicial=='' and  $fechafinal=='')
			  {
				 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorUno($i,$consultar,$tipousuario);\" >$i</a> ";
			  }
			else
			 {
			   if ($tipousuario!=0 and $etarjeta!=0 and $producto=='0x' and $servicio==0 and $fechainicial=='' and  $fechafinal=='')
			    {
				   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDos($i,$consultar,$tipousuario,$etarjeta);\" >$i</a> ";
				}
			   else
				{
				  if ($tipousuario!=0 and $etarjeta!=0 and $producto!='0x' and $servicio==0 and $fechainicial=='' and  $fechafinal=='')
				  	{
					   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorTres($i,$consultar,$tipousuario,$etarjeta,'$producto');\" >$i</a> ";
					}
				  else
					{
					   if ($tipousuario!=0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
					   	{
						  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCuatro($i,$consultar,$tipousuario,$etarjeta,'$producto','$servicio');\" >$i</a> ";
						}
					   else
						{
							if ($tipousuario!=0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
							 {
								$texto.= "<a href=\"$enlace\" onclick=\"PaginadorCinco($i,$consultar,$tipousuario,$etarjeta,'$producto','$servicio','$fechainicial','$fechafinal');\" >$i</a> ";
							 }
							else
							 {
								if ($tipousuario==0 and $etarjeta!=0 and $producto=='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
								 {
								   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSeis($i,$consultar,$etarjeta);\" >$i</a> ";
								 }
								else
								 {
									if ($tipousuario==0 and $etarjeta!=0 and $producto!='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
									 {
									   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSiete($i,$consultar,$etarjeta,'$producto');\" >$i</a> ";
									 }
									else
									 {
									   if ($tipousuario==0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
										{
										  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorOcho($i,$consultar,$etarjeta,'$producto','$servicio');\" >$i</a> ";
										}
										else
										{
										 if ($tipousuario==0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
										 {
											$texto.= "<a href=\"$enlace\" onclick=\"PaginadorNueve($i,$consultar,$etarjeta,'$producto','$servicio','$fechainicial','$fechafinal');\" >$i</a> ";
										 }
										else
										 {
										  if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
											{
											 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiez($i,$consultar,'$producto');\" >$i</a> ";
											}
										   else
											{
											 if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
											 {
												$texto.= "<a href=\"$enlace\" onclick=\"PaginadorOnce($i,$consultar,'$producto','$servicio');\" >$i</a> ";
											 }
											else
											 {
											  if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
											  {
												$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDoce($i,$consultar,'$producto','$servicio','$fechainicial','$fechafinal');\" >$i</a> ";
											  }
											  else
												{
												 if ($tipousuario==0 and $etarjeta==0 and $producto=='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
												 {
												  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorTrece($i,$consultar,'$servicio');\" >$i</a> ";
												 }
												else
												 {
												   if ($tipousuario==0 and $etarjeta==0 and $producto=='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
													{
													  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCatorce($i,$consultar,'$servicio','$fechainicial','$fechafinal');\" >$i</a> ";																																		
													}
												  else
													{
													if ($tipousuario!=0 and $etarjeta==0 and $producto!='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
													{
													 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorQuince($i,$consultar,$tipousuario,'$producto');\" >$i</a> ";
													 }
													 else
													  {
													  	if ($tipousuario!=0 and $etarjeta==0 and $producto=='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
														 {
														 	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezSeis($i,$consultar,$tipousuario,'$servicio');\" >$i</a> ";
														 }
														else
														 {
														 	if ($tipousuario!=0 and $etarjeta==0 and $producto=='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
															 {
															   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezSiete($i,$consultar,$tipousuario,'$fechainicial','$fechafinal');\" >$i</a> ";
															 }
															else
															{
																if ($tipousuario==0 and $etarjeta!=0 and $producto=='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
																{
																	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezOcho($i,$consultar,$etarjeta,'$servicio');\" >$i</a> ";
																}
																else
																{
																	if ($tipousuario==0 and $etarjeta!=0 and $producto=='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
																	{
																	 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezNueve($i,$consultar,$etarjeta,'$fechainicial','$fechafinal');\" >$i</a> ";
																	}
																	else
																	{
																		if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
																		{
																			 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorVeinte($i,$consultar,'$producto','$fechainicial','$fechafinal');\" >$i</a> ";
																		}
																		else
																			{
																				if ($tipousuario==0 and $etarjeta==0 and $producto=='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
																					{
																						$texto.= "<a href=\"$enlace\" onclick=\"PaginadorVeinteUno($i,$consultar,'$fechainicial','$fechafinal');\" >$i</a> ";
																					}
																			}
																		
																	}
																}
															}
														 } 
													  }
																																	
																													
																																}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
	   }
	   $texto.= "<b>$actual</b> ";
       for ($i=$actual+1; $i<=$maximo ; $i++)
         {
		 if (($tipousuario=='' or $tipousuario==0) and ($etarjeta=='' or $etarjeta==0) and ($producto=='' or $producto=='0x') and ($servicio=='' or $servicio==0) and $fechainicial=='' and  $fechafinal=='')
		  {
			$texto.= "<a href=\"$enlace\" onclick=\"Paginador_Uno($i,$consultar);\" >$i</a> ";
		  }
		else
		 {
			if ($tipousuario!=0 and $etarjeta==0 and $producto=='0x' and $servicio==0 and $fechainicial=='' and  $fechafinal=='')
			  {
				 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorUno($i,$consultar,$tipousuario);\" >$i</a> ";
			  }
			else
			 {
			   if ($tipousuario!=0 and $etarjeta!=0 and $producto=='0x' and $servicio==0 and $fechainicial=='' and  $fechafinal=='')
			    {
				   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDos($i,$consultar,$tipousuario,$etarjeta);\" >$i</a> ";
				}
			   else
				{
				  if ($tipousuario!=0 and $etarjeta!=0 and $producto!='0x' and $servicio==0 and $fechainicial=='' and  $fechafinal=='')
				  	{
					   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorTres($i,$consultar,$tipousuario,$etarjeta,'$producto');\" >$i</a> ";
					}
				  else
					{
					   if ($tipousuario!=0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
					   	{
						  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCuatro($i,$consultar,$tipousuario,$etarjeta,'$producto','$servicio');\" >$i</a> ";
						}
					   else
						{
							if ($tipousuario!=0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
							 {
								$texto.= "<a href=\"$enlace\" onclick=\"PaginadorCinco($i,$consultar,$tipousuario,$etarjeta,'$producto','$servicio','$fechainicial','$fechafinal');\" >$i</a> ";
							 }
							else
							 {
								if ($tipousuario==0 and $etarjeta!=0 and $producto=='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
								 {
								   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSeis($i,$consultar,$etarjeta);\" >$i</a> ";
								 }
								else
								 {
									if ($tipousuario==0 and $etarjeta!=0 and $producto!='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
									 {
									   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSiete($i,$consultar,$etarjeta,'$producto');\" >$i</a> ";
									 }
									else
									 {
									   if ($tipousuario==0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
										{
										  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorOcho($i,$consultar,$etarjeta,'$producto','$servicio');\" >$i</a> ";
										}
										else
										{
										 if ($tipousuario==0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
										 {
											$texto.= "<a href=\"$enlace\" onclick=\"PaginadorNueve($i,$consultar,$etarjeta,'$producto','$servicio','$fechainicial','$fechafinal');\" >$i</a> ";
										 }
										else
										 {
										  if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
											{
											 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiez($i,$consultar,'$producto');\" >$i</a> ";
											}
										   else
											{
											 if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
											 {
												$texto.= "<a href=\"$enlace\" onclick=\"PaginadorOnce($i,$consultar,'$producto','$servicio');\" >$i</a> ";
											 }
											else
											 {
											  if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
											  {
												$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDoce($i,$consultar,'$producto','$servicio','$fechainicial','$fechafinal');\" >$i</a> ";
											  }
											  else
												{
												 if ($tipousuario==0 and $etarjeta==0 and $producto=='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
												 {
												  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorTrece($i,$consultar,'$servicio');\" >$i</a> ";
												 }
												else
												 {
												   if ($tipousuario==0 and $etarjeta==0 and $producto=='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
													{
													  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCatorce($i,$consultar,'$servicio','$fechainicial','$fechafinal');\" >$i</a> ";																																		
													}
												  else
													{
													if ($tipousuario!=0 and $etarjeta==0 and $producto!='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
													{
													 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorQuince($i,$consultar,$tipousuario,'$producto');\" >$i</a> ";
													 }
													 else
													  {
													  	if ($tipousuario!=0 and $etarjeta==0 and $producto=='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
														 {
														 	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezSeis($i,$consultar,$tipousuario,'$servicio');\" >$i</a> ";
														 }
														else
														 {
														 	if ($tipousuario!=0 and $etarjeta==0 and $producto=='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
															 {
															   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezSiete($i,$consultar,$tipousuario,'$fechainicial','$fechafinal');\" >$i</a> ";
															 }
															else
															{
																if ($tipousuario==0 and $etarjeta!=0 and $producto=='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
																{
																	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezOcho($i,$consultar,$etarjeta,'$servicio');\" >$i</a> ";
																}
																else
																{
																	if ($tipousuario==0 and $etarjeta!=0 and $producto=='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
																	{
																	 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezNueve($i,$consultar,$etarjeta,'$fechainicial','$fechafinal');\" >$i</a> ";
																	}
																	else
																	{
																		if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
																		{
																			 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorVeinte($i,$consultar,'$producto','$fechainicial','$fechafinal');\" >$i</a> ";
																		}
																		else
																		{
																			if ($tipousuario==0 and $etarjeta==0 and $producto=='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
																			{
																			$texto.= "<a href=\"$enlace\" onclick=\"PaginadorVeinteUno($i,$consultar,'$fechainicial','$fechafinal');\" >$i</a> ";
																			}
																		}
																		
																	}
																}
															}
														 } 
													  }
																																	
																													
																																}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
		  }
   if ($maximo!=$totalPaginas)
            {
                $texto.= "... ";
            }
	if ($actual<$totalPaginas)
            {
			if (($tipousuario=='' or $tipousuario==0) and ($etarjeta=='' or $etarjeta==0) and ($producto=='' or $producto=='0x') and ($servicio=='' or $servicio==0) and $fechainicial=='' and  $fechafinal=='')
		  {
			$texto.= "<a href=\"$enlace\" onclick=\"Paginador_Uno($posterior,$consultar);\" >Siguiente&raquo;</a> ";
		  }
		else
		 {
			if ($tipousuario!=0 and $etarjeta==0 and $producto=='0x' and $servicio==0 and $fechainicial=='' and  $fechafinal=='')
			  {
				 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorUno($posterior,$consultar,$tipousuario);\" >Siguiente&raquo;</a> ";
			  }
			else
			 {
			   if ($tipousuario!=0 and $etarjeta!=0 and $producto=='0x' and $servicio==0 and $fechainicial=='' and  $fechafinal=='')
			    {
				   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDos($posterior,$consultar,$tipousuario,$etarjeta);\" >Siguiente&raquo;</a> ";
				}
			   else
				{
				  if ($tipousuario!=0 and $etarjeta!=0 and $producto!='0x' and $servicio==0 and $fechainicial=='' and  $fechafinal=='')
				  	{
					   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorTres($posterior,$consultar,$tipousuario,$etarjeta,'$producto');\" >Siguiente&raquo;</a> ";
					}
				  else
					{
					   if ($tipousuario!=0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
					   	{
						  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCuatro($posterior,$consultar,$tipousuario,$etarjeta,'$producto','$servicio');\" >Siguiente&raquo;</a> ";
						}
					   else
						{
							if ($tipousuario!=0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
							 {
								$texto.= "<a href=\"$enlace\" onclick=\"PaginadorCinco($posterior,$consultar,$tipousuario,$etarjeta,'$producto','$servicio','$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
							 }
							else
							 {
								if ($tipousuario==0 and $etarjeta!=0 and $producto=='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
								 {
								   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSeis($posterior,$consultar,$etarjeta);\" >Siguiente&raquo;</a> ";
								 }
								else
								 {
									if ($tipousuario==0 and $etarjeta!=0 and $producto!='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
									 {
									   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSiete($posterior,$consultar,$etarjeta,'$producto');\" >Siguiente&raquo;</a> ";
									 }
									else
									 {
									   if ($tipousuario==0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
										{
										  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorOcho($posterior,$consultar,$etarjeta,'$producto','$servicio');\" >Siguiente&raquo;</a> ";
										}
										else
										{
										 if ($tipousuario==0 and $etarjeta!=0 and $producto!='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
										 {
											$texto.= "<a href=\"$enlace\" onclick=\"PaginadorNueve($posterior,$consultar,$etarjeta,'$producto','$servicio','$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
										 }
										else
										 {
										  if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
											{
											 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiez($posterior,$consultar,'$producto');\" >Siguiente&raquo;</a> ";
											}
										   else
											{
											 if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
											 {
												$texto.= "<a href=\"$enlace\" onclick=\"PaginadorOnce($posterior,$consultar,'$producto','$servicio');\" >Siguiente&raquo;</a> ";
											 }
											else
											 {
											  if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
											  {
												$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDoce($posterior,$consultar,'$producto','$servicio','$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
											  }
											  else
												{
												 if ($tipousuario==0 and $etarjeta==0 and $producto=='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
												 {
												  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorTrece($posterior,$consultar,'$servicio');\" >Siguiente&raquo;</a> ";
												 }
												else
												 {
												   if ($tipousuario==0 and $etarjeta==0 and $producto=='0x' and $servicio!='0' and $fechainicial!='' and  $fechafinal!='')
													{
													  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCatorce($posterior,$consultar,'$servicio','$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";																																		
													}
												  else
													{
													if ($tipousuario!=0 and $etarjeta==0 and $producto!='0x' and $servicio=='0' and $fechainicial=='' and  $fechafinal=='')
													{
													 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorQuince($posterior,$consultar,$tipousuario,'$producto');\" >Siguiente&raquo;</a> ";
													 }
													 else
													  {
													  	if ($tipousuario!=0 and $etarjeta==0 and $producto=='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
														 {
														 	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezSeis($posterior,$consultar,$tipousuario,'$servicio');\" >Siguiente&raquo;</a> ";
														 }
														else
														 {
														 	if ($tipousuario!=0 and $etarjeta==0 and $producto=='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
															 {
															   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezSiete($posterior,$consultar,$tipousuario,'$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
															 }
															else
															{
																if ($tipousuario==0 and $etarjeta!=0 and $producto=='0x' and $servicio!='0' and $fechainicial=='' and  $fechafinal=='')
																{
																	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezOcho($posterior,$consultar,$etarjeta,'$servicio');\" >Siguiente&raquo;</a> ";
																}
																else
																{
																	if ($tipousuario==0 and $etarjeta!=0 and $producto=='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
																	{
																	 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezNueve($posterior,$consultar,$etarjeta,'$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
																	}
																	else
																	{
																		if ($tipousuario==0 and $etarjeta==0 and $producto!='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
																		{
																			 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorVeinte($posterior,$consultar,'$producto','$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
																		}
																	else
																		{
																			if ($tipousuario==0 and $etarjeta==0 and $producto=='0x' and $servicio=='0' and $fechainicial!='' and  $fechafinal!='')
																			{
																				$texto.= "<a href=\"$enlace\" onclick=\"PaginadorVeinteUno($posterior,$consultar,'$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
																			}
																			
																		}
																		
																	}
																}
															}
														 } 
													  }
																																	
																													
																																}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
            }
        else
            {
                $texto .= "<b>&raquo;</b>";
            }
        return $texto;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
function paginarporfiltros2($actual, $total, $porPagina, $enlace, $maxpags=0,$ver,$nombre,$apellido,$rut,$dv,$tipo_usuario) 
{
  if ($actual=="")
     {
        $actual=1;
     }
 $totalPaginas = ceil($total/$porPagina);
 $anterior = $actual - 1;
 $posterior = $actual + 1;
 # condicion ? entonces : sino;
 $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
 $maximo = $maxpags ? min($totalPaginas, $actual+floor($maxpags/2)): $totalPaginas;
 //echo $actual;
 //echo  $anterior;
 //echo $posterior;
if ($actual>1)
	{
		if (($nombre=='' or $nombre==0) and ($apellido=='' or $apellido==0) and ($rut=='' ) and ($dv=='' or $dv==0) and $tipo_usuario==0)
			{
				$texto = "<a href=\"$enlace\" onclick=\"Paginador_Uno($anterior,$ver);\" >&laquo;Anterior</a> ";
			}
		else
			{
				if ($nombre!='' and $apellido=='' and $rut=='' and $dv=='' and $tipo_usuario==0)
					{
						$texto = "<a href=\"$enlace\" onclick=\"PaginadorUno($anterior,$ver,'$nombre');\" >&laquo;Anterior</a> ";
					}
				else
					{
						if ($nombre!='' and $apellido!='' and $rut=='' and $dv=='' and $tipo_usuario==0)
							{
								$texto = "<a href=\"$enlace\" onclick=\"PaginadorDos($anterior,$ver,'$nombre','$apellido');\" >&laquo;Anterior</a> ";
							}
						else
							{
								if ($nombre!='' and $apellido!='' and $rut!='' and $dv!='' and  $tipo_usuario==0)
									{
										$texto = "<a href=\"$enlace\" onclick=\"PaginadorTres($anterior,$ver,'$nombre','$apellido','$rut','$dv');\" >&laquo;Anterior</a> ";
									}
								else	
									{
										if ($nombre!='' and $apellido!='' and $rut!='' and $dv!='' and $tipo_usuario!=0)
											{
												 $texto = "<a href=\"$enlace\" onclick=\"PaginadorCuatro($anterior,$ver,'$nombre','$apellido','$rut','$dv',$tipo_usuario);\" >&laquo;Anterior</a> ";
											}
										else
											{
												if ($nombre=='' and $apellido!='' and $rut=='' and $dv=='' and $tipo_usuario==0)
													{
														$texto = "<a href=\"$enlace\" onclick=\"PaginadorCinco($anterior,$ver,'$apellido');\" >&laquo;Anterior</a> ";
													}
												else
													{
														if ($nombre=='' and $apellido!='' and $rut!='' and $dv!='' and $tipo_usuario==0)
															{
																$texto = "<a href=\"$enlace\" onclick=\"PaginadorSeis($anterior,$ver,'$apellido','$rut','$dv');\" >&laquo;Anterior</a> ";
															}
														else
															{
																if ($nombre=='' and $apellido!=0 and $rut!='' and $dv!='' and $tipo_usuario!=0)
																	{
																		 $texto = "<a href=\"$enlace\" onclick=\"PaginadorSiete($anterior,$ver,'$apellido','$rut','$dv',$tipo_usuario);\" >&laquo;Anterior</a> ";
																	}
																else
																	{
																		if ($nombre=='' and $apellido=='' and $rut!='' and $dv!='' and $tipo_usuario==0)
																			{
																				 $texto = "<a href=\"$enlace\" onclick=\"PaginadorOcho($anterior,$ver,'$rut','$dv');\" >&laquo;Anterior</a> ";
																			}
																		else
																			{
																				if ($nombre=='' and $apellido=='' and $rut!='' and $dv!='' and $tipo_usuario!=0)
																					{
																						$texto = "<a href=\"$enlace\" onclick=\"PaginadorNueve($anterior,$ver,$rut,'$dv',$tipo_usuario);\" >&laquo;Anterior</a> ";
																					}
																				else
																					{
																						 if ($nombre=='' and $apellido=='' and $rut=='' and $dv=='' and $tipo_usuario!=0)
																						 	{
																								$texto = "<a href=\"$enlace\" onclick=\"PaginadorDiez($anterior,$ver,$tipo_usuario);\" >&laquo;Anterior</a> ";
																							}
																						else
																							{
																								if ($nombre!='' and $apellido=='' and $rut=='' and $dv=='' and $tipo_usuario!=0)
																									{
																										$texto = "<a href=\"$enlace\" onclick=\"PaginadorDiezUno($anterior,$ver,'$nombre',$tipo_usuario);\" >&laquo;Anterior</a> ";
																									}
																								else
																									{
																										 if ($nombre!='' and $apellido=='' and $rut!='' and $dv!='' and $tipo_usuario==0)
																										 	{
																												$texto = "<a href=\"$enlace\" onclick=\"PaginadorDiezDos($anterior,$ver,'$nombre','$rut','$dv',$tipo_usuario);\" >&laquo;Anterior</a> ";
																											}
																										else
																											{
																												if ($nombre=='' and $apellido!='' and $rut=='' and $dv=='' and $tipo_usuario!=0)
																													{
																														$texto = "<a href=\"$enlace\" onclick=\"PaginadorDiezTres($anterior,$ver,'$apellido',$tipo_usuario);\" >&laquo;Anterior</a> ";
																													}
																											}
																									}
																						
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
	}
	else
    {
    	$texto = "<b>&laquo;";
		# $var = $var  expresion equivalente $var .=
		if ($minimo!=1)
       		{
           		$texto.= "... ";
       		}
		for ($i=$minimo; $i<$actual; $i++)
			{
					if (($nombre=='' or $nombre==0) and ($apellido=='' or $apellido==0) and ($rut=='' or $rut=='') and ($dv=='' or $dv==0) and $tipo_usuario==0)
						{
							$texto.= "<a href=\"$enlace\" onclick=\"Paginador_Uno($i,$ver);\" >$i</a> ";
						}
					else
						{
							if ($nombre!='' and $apellido=='' and $rut=='' and $dv=='' and  $tipo_usuario==0)
								{
									$texto.= "<a href=\"$enlace\" onclick=\"PaginadorUno($i,$ver,'$nombre');\" >$i</a> ";
								}
							else
								{
									if ($nombre!='' and $apellido!='' and $rut=='' and $dv=='' and $tipo_usuario==0)
										{
											 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDos($i,$ver,'$nombre','$apellido');\" >$i</a> ";
										}
									else
										{
											if ($nombre!='' and $apellido!='' and $rut!='' and $dv!='' and $tipo_usuario==0)
												{
													$texto.= "<a href=\"$enlace\" onclick=\"PaginadorTres($i,$ver,'$nombre','$apellido','$rut','$dv');\" >$i</a> ";
												}
											else
												{
													 if ($nombre!='' and $apellido!='' and $rut!='' and $dv!='' and $tipo_usuario!=0 )
													 	{
															 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCuatro($i,$ver,'$nombre','$apellido','$rut','$dv',$tipo_usuario);\" >$i</a> ";
														}
													else
														{
															if ($nombre=='' and $apellido!=0 and $rut=='' and $dv=='' and $tipo_usuario==0)
																{
																	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorCinco($i,$ver,'$apellido');\" >$i</a> ";
																}
															else
																{
																	if ($nombre=='' and $apellido!='' and $rut!='' and $dv!='' and $tipo_usuario==0)
																		{
																			$texto.= "<a href=\"$enlace\" onclick=\"PaginadorSeis($i,$ver,'$apellido','$rut','$dv');\" >$i</a> ";
																		}
																	else
																		{
																			if ($nombre=='' and $apellido!='' and $rut!='' and $dv!='' and $tipo_usuario!=0)
																				{
																					 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSiete($i,$ver,'$apellido','$rut','$dv',$tipo_usuario);\" >$i</a> ";
																				}
																			else
																				{
																					 if ($nombre=='' and $apellido=='' and $rut!='' and $dv!='' and $tipo_usuario==0)
																					 	{
																							 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorOcho($i,$ver,'$rut','$dv');\" >$i</a> ";
																						}
																					else
																						{
																							if ($nombre=='' and $apellido=='' and $rut!='' and $dv!='' and $tipo_usuario!=0)
																								{
																									$texto.= "<a href=\"$enlace\" onclick=\"PaginadorNueve($i,$ver,'$rut','$dv',$tipo_usuario);\" >$i</a> ";
																								}
																							else
																								{
																									if ($nombre=='' and $apellido=='' and $rut=='' and $dv=='' and $tipo_usuario!=0)
																										{
																											 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiez($i,$ver,$tipo_usuario);\" >$i</a> ";
																										}
																									else
																										{
																											if ($nombre!='' and $apellido==0 and $rut=='' and $dv=='' and $tipo_usuario!=0)
																												{
																													$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezUno($i,$ver,'$nombre',$tipo_usuario);\" >$i</a> ";
																												 }
																											else
																												{
																													if ($nombre!='' and $apellido==0 and $rut!='' and $dv!='' and $tipo_usuario==0)
																														{
																															$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezDos($i,$ver,'$nombre','$rut','$dv');\" >$i</a> ";
																														}
																													else
																														{
																															if ($nombre=='' and $apellido!=0 and $rut=='' and $dv=='' and $tipo_usuario!=0)
																																{
																																	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezTres($i,$ver,'$apellido',$tipo_usuario);\" >$i</a> ";
																																}
																															else
																																{
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
			}
    }
	$texto.= "<b>$actual</b> ";
	for ($i=$actual+1; $i<=$maximo ; $i++)
		{
			if (($nombre=='' or $nombre==0) and ($apellido=='' or $apellido==0) and ($rut=='' or $rut=='') and ($dv=='' or $dv==0) and $tipo_usuario==0)
						{
							$texto.= "<a href=\"$enlace\" onclick=\"Paginador_Uno($i,$ver);\" >$i</a> ";
						}
					else
						{
							if ($nombre!='' and $apellido=='' and $rut=='' and $dv=='' and  $tipo_usuario==0)
								{
									$texto.= "<a href=\"$enlace\" onclick=\"PaginadorUno($i,$ver,'$nombre');\" >$i</a> ";
								}
							else
								{
									if ($nombre!='' and $apellido!='' and $rut=='' and $dv=='' and $tipo_usuario==0)
										{
											 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDos($i,$ver,'$nombre','$apellido');\" >$i</a> ";
										}
									else
										{
											if ($nombre!='' and $apellido!='' and $rut!='' and $dv!='' and $tipo_usuario==0)
												{
													$texto.= "<a href=\"$enlace\" onclick=\"PaginadorTres($i,$ver,'$nombre','$apellido','$rut','$dv');\" >$i</a> ";
												}
											else
												{
													 if ($nombre!='' and $apellido!='' and $rut!='' and $dv!='' and $tipo_usuario!=0 )
													 	{
															 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCuatro($i,$ver,'$nombre','$apellido','$rut','$dv',$tipo_usuario);\" >$i</a> ";
														}
													else
														{
															if ($nombre=='' and $apellido!=0 and $rut=='' and $dv=='' and $tipo_usuario==0)
																{
																	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorCinco($i,$ver,'$apellido');\" >$i</a> ";
																}
															else
																{
																	if ($nombre=='' and $apellido!='' and $rut!='' and $dv!='' and $tipo_usuario==0)
																		{
																			$texto.= "<a href=\"$enlace\" onclick=\"PaginadorSeis($i,$ver,'$apellido','$rut','$dv');\" >$i</a> ";
																		}
																	else
																		{
																			if ($nombre=='' and $apellido!='' and $rut!='' and $dv!='' and $tipo_usuario!=0)
																				{
																					 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSiete($i,$ver,'$apellido','$rut','$dv',$tipo_usuario);\" >$i</a> ";
																				}
																			else
																				{
																					 if ($nombre=='' and $apellido=='' and $rut!='' and $dv!='' and $tipo_usuario==0)
																					 	{
																							 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorOcho($i,$ver,'$rut','$dv');\" >$i</a> ";
																						}
																					else
																						{
																							if ($nombre=='' and $apellido=='' and $rut!='' and $dv!='' and $tipo_usuario!=0)
																								{
																									$texto.= "<a href=\"$enlace\" onclick=\"PaginadorNueve($i,$ver,'$rut','$dv',$tipo_usuario);\" >$i</a> ";
																								}
																							else
																								{
																									if ($nombre=='' and $apellido=='' and $rut=='' and $dv=='' and $tipo_usuario!=0)
																										{
																											 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiez($i,$ver,$tipo_usuario);\" >$i</a> ";
																										}
																									else
																										{
																											if ($nombre!='' and $apellido==0 and $rut=='' and $dv=='' and $tipo_usuario!=0)
																												{
																													$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezUno($i,$ver,'$nombre',$tipo_usuario);\" >$i</a> ";
																												 }
																											else
																												{
																													if ($nombre!='' and $apellido==0 and $rut!='' and $dv!='' and $tipo_usuario==0)
																														{
																															$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezDos($i,$ver,'$nombre','$rut','$dv');\" >$i</a> ";
																														}
																													else
																														{
																															if ($nombre=='' and $apellido!=0 and $rut=='' and $dv=='' and $tipo_usuario!=0)
																																{
																																	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezTres($i,$ver,'$apellido',$tipo_usuario);\" >$i</a> ";
																																}
																															else
																																{
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
			//}
		}
	 if ($maximo!=$totalPaginas)
     	{
        	$texto.= "... ";
        }
	if ($actual<$totalPaginas)
		{
			if (($nombre=='' or $nombre==0) and ($apellido=='' or $apellido==0) and ($rut=='' or $rut=='0x') and ($dv=='' or $dv==0) and $tipo_usuario==0)
				{
					$texto.= "<a href=\"$enlace\" onclick=\"Paginador_Uno($posterior,$ver);\" >Siguiente&raquo;</a> ";
				}
			else
				{
					if ($nombre!='' and $apellido=='' and $rut=='' and $dv=='' and $tipo_usuario==0)
						{
							 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorUno($posterior,$ver,'$nombre');\" >Siguiente&raquo;</a> ";
						}
					else
						{
							if ($nombre!='' and $apellido!='' and $rut=='' and $dv=='' and $tipo_usuario==0)
								{
									 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDos($posterior,$ver,'$nombre','$apellido');\" >Siguiente&raquo;</a> ";
								}
							else
								{
									if ($nombre!='' and $apellido!='' and $rut!='' and $dv!='' and $tipo_usuario==0)
										{
											$texto.= "<a href=\"$enlace\" onclick=\"PaginadorTres($posterior,$ver,'$nombre','$apellido','$rut','$dv');\" >Siguiente&raquo;</a> ";
										}
									else
										{
											if ($nombre!='' and $apellido!='' and $rut!='' and $dv!='' and $tipo_usuario!=0)
												{
													$texto.= "<a href=\"$enlace\" onclick=\"PaginadorCuatro($posterior,$ver,'$nombre','$apellido','$rut','$dv',$tipo_usuario);\" >Siguiente&raquo;</a> ";
												}
											else
												{
													if ($nombre=='' and $apellido!='' and $rut=='' and $dv=='' and $tipo_usuario==0)
														{
															$texto.= "<a href=\"$enlace\" onclick=\"PaginadorCinco($posterior,$ver,'$apellido');\" >Siguiente&raquo;</a> ";
														}
													else
														{
															if ($nombre=='' and $apellido!='' and $rut!='' and $dv!='' and $tipo_usuario==0)
																{
																	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorSeis($posterior,$ver,'$apellido','$rut','$dv');\" >Siguiente&raquo;</a> ";
																}
															else
																{
																	if ($nombre=='' and $apellido!='' and $rut!='' and $dv!='' and $tipo_usuario!=0)
																		{
																			$texto.= "<a href=\"$enlace\" onclick=\"PaginadorSiete($posterior,$ver,'$apellido','$rut','$dv',$tipo_usuario);\" >Siguiente&raquo;</a> ";
																		}
																	else
																		{
																			if ($nombre=='' and $apellido=='' and $rut!='' and $dv!='' and $tipo_usuario==0)
																				{
																					$texto.= "<a href=\"$enlace\" onclick=\"PaginadorOcho($posterior,$ver,'$rut','$dv');\" >Siguiente&raquo;</a> ";
																				}
																			else
																				{
																					if ($nombre=='' and $apellido=='' and $rut!='' and $dv!='' and $tipo_usuario!=0)
																						{
																							$texto.= "<a href=\"$enlace\" onclick=\"PaginadorNueve($posterior,$ver,'$rut','$dv',$tipo_usuario);\" >Siguiente&raquo;</a> ";
																						}
																					else
																						{
																							if ($nombre=='' and $apellido=='' and $rut=='' and $dv=='' and $tipo_usuario!=0)
																								{
																									$texto.= "<a href=\"$enlace\" onclick=\"PaginadorNueve($posterior,$ver,$tipo_usuario);\" >Siguiente&raquo;</a> ";
																								}
																							else
																								{
																									if ($nombre!='' and $apellido=='' and $rut=='' and $dv=='' and $tipo_usuario!=0)
																										{
																											$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiez($posterior,$ver,'$nombre',$tipo_usuario);\" >Siguiente&raquo;</a> ";
																										}
																									else
																										{
																											if ($nombre!='' and $apellido=='' and $rut!='' and $dv!='' and $tipo_usuario==0)
																												{
																													$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezDos($posterior,$ver,'$nombre','$rut','$dv');\" >Siguiente&raquo;</a> ";
																												}
																											else
																												{
																													if ($nombre!='' and $apellido=='' and $rut!='' and $dv!='' and $tipo_usuario==0)
																														{
																															$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezTres($posterior,$ver,'$nombre','$rut','$dv');\" >Siguiente&raquo;</a> ";
																														}
																													else
																														{
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
													
														}
												}
										}
								}
						}
				}
		}
	else
            {
                $texto .= "<b>&raquo;</b>";
            }
        return $texto;
	
}


//////////////////////////////////////////////////////////////////////////////////////////////////
function paginarporfechasdos($actual, $total, $porPagina, $enlace, $maxpags=0,$fechauno,$fechados) 
    {
        if ($actual=="")
            {
                $actual=1;
            }
        $totalPaginas = ceil($total/$porPagina);
        $anterior = $actual - 1;
        $posterior = $actual + 1;
        # condicion ? entonces : sino;
        $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
        $maximo = $maxpags ? min($totalPaginas, $actual+floor($maxpags/2)): $totalPaginas;
        if ($actual>1)
            {
                if ($fechados=='')
                    {
                        $texto = "<a href=\"$enlace\" onclick=\"PaginadorDos($anterior,'$fechauno');\" >&laquo;Anterior</a> ";
                    }
                else
                    {
                        $texto = "<a href=\"$enlace\" onclick=\"Paginador($anterior,'$fechauno','$fechados');\" >&laquo;Anterior</a> ";
                    }
           }
        else
           {
               $texto = "<b>&laquo;";
           }
        # $var = $var  expresion equivalente $var .=
		if ($minimo!=1)
            {
                $texto.= "... ";
            }
        for ($i=$minimo; $i<$actual; $i++)
            {
				if ($fechados=='')
				{
				$texto .= "<a href=\"$enlace\" onclick=\"PaginadorDos($i,'$fechauno');\">$i</a> ";
				}
				else
				{
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($i,'$fechauno','$fechados');\">$i</a> ";
				}
            }
        $texto .= "<b>$actual</b> ";
        for ($i=$actual+1; $i<=$maximo ; $i++)
            {
			if ($fechados=='')
			{
				$texto .= "<a href=\"$enlace\" onclick=\"PaginadorDos($i,'$fechauno');\">$i</a> ";
			}
			else
			{
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($i,'$fechauno','$fechados');\">$i</a> ";
			}	
            }
        if ($maximo!=$totalPaginas)
            {
                $texto.= "... ";
            }
        if ($actual<$totalPaginas)
            {
			if ($fechados=='')
			{
			$texto .= "<a href=\"$enlace\" onclick=\"PaginadorDos($posterior,'$fechauno');\">Siguiente&raquo;</a>";
			}
			else
			{
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($posterior,'$fechauno','$fechados');\">Siguiente&raquo;</a>";
				}
            }
        else
            {
                $texto .= "<b>&raquo;</b>";
            }
        return $texto;
}

function paginarporfiltro($actual, $total, $porPagina, $enlace, $maxpags=0,$actualiza) 
    {
        if ($actual=="")
            {
                $actual=1;
            }
        $totalPaginas = ceil($total/$porPagina);
        $anterior = $actual - 1;
        $posterior = $actual + 1;
        # condicion ? entonces : sino;
        $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
        $maximo = $maxpags ? min($totalPaginas, $actual+floor($maxpags/2)): $totalPaginas;
        if ($actual>1)
            {
             
               $texto = "<a href=\"$enlace\" onclick=\"Paginador($anterior,$actualiza);\" >&laquo;Anterior</a> ";
            }
        else
           {
               $texto = "<b>&laquo;";
           }
        # $var = $var  expresion equivalente $var .=
		if ($minimo!=1)
            {
                $texto.= "... ";
            }
        for ($i=$minimo; $i<$actual; $i++)
            {
				$texto .= "<a href=\"$enlace\" onclick=\"Paginador($i,$actualiza);\">$i</a> ";
			}
        $texto .= "<b>$actual</b> ";
        for ($i=$actual+1; $i<=$maximo ; $i++)
            {
		
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($i,$actualiza);\">$i</a> ";
				
            }
        if ($maximo!=$totalPaginas)
            {
                $texto.= "... ";
            }
        if ($actual<$totalPaginas)
            {
			
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($posterior,$actualiza);\">Siguiente&raquo;</a>";
				
            }
        else
            {
                $texto .= "<b>&raquo;</b>";
            }
        return $texto;
}

/*
Funcion para sepearar la Fecha
*/
function hbuscar($strSource,$strSep,$ele)
{
	$strSource = $strSep.$strSource.$strSep;
	$x=split($strSep,$strSource);
	$l=count($x);
	if ($ele > $l){
		$z="";
	}else{
		$z=$x[$ele];
	}
	return $z;
}

function Auntentificar()
{
session_name("loginUsuario"); 
session_start(); 
//cambiamos la duraci�n a la cookie de la sesi�n
session_set_cookie_params(0, "/", $HTTP_SERVER_VARS["HTTP_HOST"], 0);

if ($_SESSION["autentificado"] != "SI") 
    { 
        //se envia a la p�gina de autentificaci�n  porque no esta logeado
        header("Location: Login.php"); 
    } 
else 
    { 
        //calculamos el tiempo transcurrido 
        $fechaGuardada = $_SESSION["ultimoAcceso"]; 
        $ahora = date("Y-n-j H:i:s"); 
        $tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada)); 
        //comparamos el tiempo transcurrido 
        if($tiempo_transcurrido >= 1) 
            { 
                //si pasaron 10 minutos o m�s 
                session_destroy(); // destruyo la sesi�n 
                header("Location: Login.php"); //env�o al usuario a la pag. de autenticaci�n 
                //sino, actualizo la fecha de la sesi�n 
            }
        else 
           { 
               $_SESSION["ultimoAcceso"] = $ahora; 
           } 
} 
}


function crypt_md5($msg,$heslo)
  {   
   $key=$heslo;$sifra="";
   $key1=$this->binmd5($key);
   //echo "<br>key1:".$key1;
   while($msg) {
     $m=substr($msg,0,16);
     // echo "<br>m:".$m;
     $msg=substr($msg,16);
     $sifra.=$this->bytexor($m,$key1);//$sifra.=$this->bytexor($m,$key1,16);//se cae
      //echo "<br>sifra:".$sifra;
     $key1=$this->binmd5($key.$key1.$m);
   }
  // echo "\n";
   return($sifra);
  }
 function binmd5($val)
  {
   return(pack("H*",md5($val)));
  } 
  
  function bytexor($a,$b)//function bytexor($a,$b,$l)
  {  
   $c=""; 
   $la = strlen($a);
   $lb = strlen($b);
   if($la < $lb){
  for($i=0;$i<$la;$i++) {	
	 $c .= $a{$i}^$b{$i};
	 }
   return($c);
  }else{
  for($i=0;$i<$lb;$i++) {	
	 $c .= $a{$i}^$b{$i};
	 }
  }}
  
function decrypt_md5($msg,$heslo)
  {
   $key=$heslo;$sifra="";
   $key1=$this->binmd5($key);
   while($msg){
     $m=substr($msg,0,16);
     $msg=substr($msg,16);
     $sifra.=$m=$this->bytexor($m,$key1);
     $key1=$this->binmd5($key.$key1.$m);	
   }
   //echo "\n";
   return($sifra);
  }
  
function getFormatoMonto($monto)
    {
	$monto=trim($monto);
	if((strlen($monto))==4)
		{
			$m1 = substr($monto,-4,1);
			$m2 = substr($monto,-3,3);				
			$monto = $m1.".".$m2;
		}
	else 
		if((strlen($monto))==5)
			{
				$m1 = substr($monto,-5,2);
				$m2 = substr($monto,-3,3);				
				$monto = $m1.".".$m2;
			}
		else 
			if((strlen($monto))==6)
				{
					$m1 = substr($monto,-6,3);
					$m2 = substr($monto,-3,3);				
					$monto = $m1.".".$m2;
				}
			else 
				if((strlen($monto))==7)
					{
						$m1 = substr($monto,-7,1);
						$m2 = substr($monto,-6,3);
						$m3 = substr($monto,-3,3);				
						$monto = $m1.".".$m2.".".$m3;
					}
				else 
					if((strlen($monto))==8)
						{
							$m1 = substr($monto,-8,2);
							$m2 = substr($monto,-6,3);
							$m3 = substr($monto,-3,3);				
							$monto = $m1.".".$m2.".".$m3;
						}
					else 
						if((strlen($monto))==9)
							{
								$m1 = substr($monto,-9,3);
								$m2 = substr($monto,-6,3);
								$m3 = substr($monto,-3,3);				
								$monto = $m1.".".$m2.".".$m3;
							}
						else
							{
								
							}
	$monto=" $ ".$monto;
	return $monto;
	}

function get_formatoMonto_decimal($monto)
    {
	//echo "<br>monto".$monto;
        $entero = (double)$monto;
		//	echo "<br>entero".$entero;
			 
        if($entero!='0' and strlen($entero)>=4)
            {
                if((strlen($entero))==4)
                    {
                        $m1 = substr($entero,-4,1);
                        $m2 = substr($entero,-3,3);				
                        $entero = $m1.".".$m2;
                        $decimal= substr($monto,5,strlen($monto));
                    }
               else 
                   if((strlen($entero))==5)
                       {
                           $m1 = substr($entero,-5,2);
                           $m2 = substr($entero,-3,3);				
                           $entero = $m1.".".$m2;
                           $decimal= substr($monto,6,strlen($monto));
                       }
					else 
                        if((strlen($entero))==6)
                           {
                               $m1 = substr($entero,-6,3);
                               $m2 = substr($entero,-3,3);				
                               $entero = $m1.".".$m2;
                               $largo=strlen($monto);
                               $decimal= substr($monto,7,strlen($monto));
                           }
                        else 
						   if((strlen($entero))==7)
                              {
                                  $m1 = substr($entero,-7,1);
                                  $m2 = substr($entero,-6,3);
                                  $m3 = substr($entero,-3,3);				
                                  $entero = $m1.".".$m2.".".$m3;
								  $decimal= substr($monto,8,strlen($monto));
                              }
                          else 
                             if((strlen($entero))==8)
                                {
                                   $m1 = substr($entero,-8,2);
                                   $m2 = substr($entero,-6,3);
                                   $m3 = substr($entero,-3,3);				
                                   $entero = $m1.".".$m2.".".$m3;
                                   $decimal= substr($monto,9,strlen($monto));
                                }
                            else 
                                if((strlen($entero))==9)
                                    {
                                       $m1 = substr($entero,-9,3);
                                       $m2 = substr($entero,-6,3);
                                       $m3 = substr($entero,-3,3);				
                                       $entero = $m1.".".$m2.".".$m3;
                                       $decimal= substr($monto,10,strlen($monto));
                                    }
                                else
                                   {
								   	if((strlen($entero))==10)
									{
								//	echo "<br>entero".$entero;
									   $m1 = substr($entero,-10,1);
                                       $m2 = substr($entero,-9,3);
                                       $m3 = substr($entero,-6,3);
									   $m4 = substr($entero,-3,3);
								//	   echo "<br>".$m1;
								//	   echo "<br>".$m2;
								//	   echo "<br>".$m3;
								//	   echo "<br>".$m4;
									   $entero = $m1.".".$m2.".".$m3.".".$m4;
								//	   echo "<br>".$entero;
                                       $decimal= substr($monto,11,strlen($monto));
									}
									else
										{
											if((strlen($monto))==11)
												{
													$m1 = substr($entero,-11,2);
                                       				$m2 = substr($entero,-9,3);
                                       				$m3 = substr($entero,-6,3);
									   				$m4 = substr($entero,-3,3);
									   				$entero = $m1.".".$m2.".".$m3.".".$m4;
                                       				$decimal= substr($monto,12,strlen($monto));
												}
											else
												{
													if((strlen($monto))==12)
														{
															$m1 = substr($entero,-12,3);
															echo "<br>".$m1;
                                       						$m2 = substr($entero,-9,3);
															echo "<br>".$m2;
                                       						$m3 = substr($entero,-6,3);
															echo "<br>".$m3;
									   						$m4 = substr($entero,-3,3);
															echo "<br>".$m4;
									   						$entero = $m1.".".$m2.".".$m3.".".$m4;
                                       						$decimal= substr($monto,13,strlen($monto));
														}
													else
														{
															$m1 = substr($entero,-13,1);
															$m2 = substr($entero,-12,3);
                                       						$m3 = substr($entero,-9,3);
                                       						$m4 = substr($entero,-6,3);
									   						$m5 = substr($entero,-3,3);
									   						$entero = $m1.".".$m2.".".$m2.".".$m3.".".$m4;
                                       						$decimal= substr($monto,14,strlen($monto));
														}	
												}
										}
                                   }
                                if ($decimal=='')
                                   {
                                       $decimal='0';
                                   }	
                              $entero=" $ ".$entero.",".$decimal;
                              return $entero;
	
                            }
	                else
	                     {
		                     $monto=trim($monto);
		                     $monto=" $ ".$monto;
	                          return $monto;
		}
}

function ultimoDia($mes,$ano){ 
    $ultimo_dia=28; 
    while (checkdate($mes,$ultimo_dia + 1,$ano)){ 
       $ultimo_dia++; 
    } 
    return $ultimo_dia; 
} 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function paginarporfiltros_rc($actual, $total, $porPagina, $enlace, $maxpags=0,$consultar,$ntarjeta,$tipousuario,$vendedor,$promotor,$fechainicial,$fechafinal) 
{
  if ($actual=="")
     {
        $actual=1;
     }
 $totalPaginas = ceil($total/$porPagina);
 $anterior = $actual - 1;
 $posterior = $actual + 1;
 # condicion ? entonces : sino;
 $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
 $maximo = $maxpags ? min($totalPaginas, $actual+floor($maxpags/2)): $totalPaginas;
 //echo $actual;
 //echo  $anterior;
 //echo $posterior;
 if ($actual>1)
	 {
		if (($ntarjeta=='' or $ntarjeta==0) and ($tipousuario=='' or $tipousuario==0) and ($vendedor=='' or $vendedor==0) and ($promotor=='' or $promotor==0) and $fechainicial=='' and  $fechafinal=='')
		  {
			$texto = "<a href=\"$enlace\" onclick=\"Paginador_Uno($anterior,$consultar);\" >&laquo;Anterior</a> ";
		  }
		else
		 {
			if ($ntarjeta!='' and $tipousuario==0 and $vendedor==0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
			  {
				 $texto = "<a href=\"$enlace\" onclick=\"PaginadorUno($anterior,$consultar,'$ntarjeta');\" >&laquo;Anterior</a> ";
			  }
			else
			 {
			   if ($ntarjeta!='' and $tipousuario!=0 and $vendedor==0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
			    {
				   $texto = "<a href=\"$enlace\" onclick=\"PaginadorDos($anterior,$consultar,'$ntarjeta',$tipousuario);\" >&laquo;Anterior</a> ";
				}
			   else
				{
				  if ($ntarjeta!='' and $tipousuario!=0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
				  	{
					   $texto = "<a href=\"$enlace\" onclick=\"PaginadorTres($anterior,$consultar,'$ntarjeta',$tipousuario,$vendedor);\" >&laquo;Anterior</a> ";
					}
				  else
					{
					   if ($ntarjeta!='' and $tipousuario!=0 and $vendedor!=0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
					   	{
						  $texto = "<a href=\"$enlace\" onclick=\"PaginadorCuatro($anterior,$consultar,'$ntarjeta',$tipousuario,$vendedor,$promotor);\" >&laquo;Anterior</a> ";
						}
					   else
						{
							if ($ntarjeta!='' and $tipousuario!=0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
							 {
								$texto = "<a href=\"$enlace\" onclick=\"PaginadorCinco($anterior,$consultar,'$ntarjeta',$tipousuario,$vendedor,$promotor,'$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
							 }
							else
							 {
								if ($ntarjeta=='' and $tipousuario!=0 and $vendedor==0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
								 {
								   $texto = "<a href=\"$enlace\" onclick=\"PaginadorSeis($anterior,$consultar,$tipousuario);\" >&laquo;Anterior</a> ";
								 }
								else
								 {
									if ($ntarjeta=='' and $tipousuario!=0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
									 {
									   $texto = "<a href=\"$enlace\" onclick=\"PaginadorSiete($anterior,$consultar,$tipousuario,$vendedor);\" >&laquo;Anterior</a> ";
									 }
									else
									 {
									   if ($ntarjeta=='' and $tipousuario!=0 and $vendedor!=0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
										{
										  $texto = "<a href=\"$enlace\" onclick=\"PaginadorOcho($anterior,$consultar,$tipousuario,$vendedor,$promotor);\" >&laquo;Anterior</a> ";
										}
										else
										{
										 if ($ntarjeta=='' and $tipousuario!=0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
										 {
											$texto = "<a href=\"$enlace\" onclick=\"PaginadorNueve($anterior,$consultar,$tipousuario,$vendedor',$promotor,'$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
										 }
										else
										 {
										  if ($ntarjeta=='' and $tipousuario==0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
											{
											 $texto = "<a href=\"$enlace\" onclick=\"PaginadorDiez($anterior,$consultar,$vendedor);\" >&laquo;Anterior</a> ";

											}
										   else
											{
											 if ($ntarjeta=='' and $tipousuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
											 {
												$texto = "<a href=\"$enlace\" onclick=\"PaginadorOnce($anterior,$consultar,$vendedor,$promotor);\" >&laquo;Anterior</a> ";
											 }
											else
											 {
											  if ($ntarjeta=='' and $tipousuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
											  {
												$texto = "<a href=\"$enlace\" onclick=\"PaginadorDoce($anterior,$consultar,$vendedor,$promotor,'$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
											  }
											  else
												{
												 if ($ntarjeta=='' and $tipousuario==0 and $vendedor==0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
												 {
												  $texto = "<a href=\"$enlace\" onclick=\"PaginadorTrece($anterior,$consultar,$promotor);\" >&laquo;Anterior</a> ";
												 }
												else
												 {
												   if ($ntarjeta=='' and $tipousuario==0 and $vendedor==0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
													{
													  $texto = "<a href=\"$enlace\" onclick=\"PaginadorCatorce($anterior,$consultar,$promotor,'$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";																																		
													}
												  else
													{
													if ($ntarjeta!='' and $tipousuario==0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
													{
													 $texto = "<a href=\"$enlace\" onclick=\"PaginadorQuince($anterior,$consultar,'$ntarjeta',$vendedor);\" >&laquo;Anterior</a> ";
													 }
													 else
													  {
													  	if ($ntarjeta!='' and $tipousuario==0 and $vendedor==0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
														 {
														 	$texto = "<a href=\"$enlace\" onclick=\"PaginadorDiezSeis($anterior,$consultar,'$ntarjeta',$promotor);\" >&laquo;Anterior</a> ";
														 }
														else
														 {
														 	if ($ntarjeta!='' and $tipousuario==0 and $vendedor==0 and $promotor==0 and $fechainicial!='' and  $fechafinal!='')
															 {
															   $texto = "<a href=\"$enlace\" onclick=\"PaginadorDiezSiete($anterior,$consultar,'$ntarjeta','$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
															 }
															else
															{
																if ($ntarjeta=='' and $tipousuario!=0 and $vendedor==0 and $promotor!='0' and $fechainicial=='' and  $fechafinal=='')
																{
																	$texto = "<a href=\"$enlace\" onclick=\"PaginadorDiezOcho($anterior,$consultar,$tipousuario,$promotor);\" >&laquo;Anterior</a> ";
																}
																else
																{
																	if ($ntarjeta=='' and $tipousuario!=0 and $vendedor==0 and $promotor==0 and $fechainicial!='' and  $fechafinal!='')
																	{
																	 $texto = "<a href=\"$enlace\" onclick=\"PaginadorDiezNueve($anterior,$consultar,$tipousuario,'$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
																	}
																	else
																	{
																		if ($ntarjeta=='' and $tipousuario==0 and $vendedor!=0 and $promotor=='0' and $fechainicial!='' and  $fechafinal!='')
																		{
																			 $texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinte($anterior,$consultar,$vendedor,'$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
																		}
																	   else
																	    {
																			if ($ntarjeta=='' and $tipousuario==0 and $vendedor==0and $promotor==0 and $fechainicial!='' and  $fechafinal!='')
																				{
																					 $texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteUno($anterior,$consultar,'$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
																				}
																			else
																				{
																					if ($ntarjeta!='' and $tipousuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
																					{
																						$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteDos($anterior,$consultar,'$ntarjeta',$vendedor,$promotor);\" >&laquo;Anterior</a> ";
																					}
																					else
																					{
																						if ($ntarjeta!='' and $tipousuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
																						{
																							$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteTres($anterior,$consultar,'$ntarjeta',$vendedor,$promotor,'$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
																						}
																						else
																						{
																							if ($ntarjeta!='' and $tipousuario!=0 and $vendedor==0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
																							{
																								$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteCuatro($anterior,$consultar,'$ntarjeta',$tipousuario,$promotor);\" >&laquo;Anterior</a> ";
																							}
																							else
																							{
																								if ($ntarjeta!='' and $tipousuario!=0 and $vendedor==0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
																								{
																									$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteCinco($anterior,$consultar,'$ntarjeta',$tipousuario,$promotor,'$fechainicial','$fechafinal');\" >&laquo;Anterior</a> ";
																								}
																							}
																						}
																					}
																				}
																		}
																		
																	}
																}
															}
														 } 
													  }
																																	
																													
																																}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
	else
           {
               $texto = "<b>&laquo;";
           }		
	# $var = $var  expresion equivalente $var .=
	if ($minimo!=1)
       {
           $texto.= "... ";
       }
	 for ($i=$minimo; $i<$actual; $i++)
       {
	   	if (($ntarjeta=='' or $ntarjeta==0) and ($tipo_usuario=='' or $tipo_usuario==0) and ($vendedor=='' or $vendedor==0) and ($promotor=='' or $promotor==0) and $fechainicial=='' and  $fechafinal=='')
		  {
			$texto.= "<a href=\"$enlace\" onclick=\"Paginador_Uno($i,$consultar);\" >$i</a> ";
		  }
		else
		 {
			if ($ntarjeta!='' and $tipo_usuario==0 and $vendedor==0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
			  {
				 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorUno($i,$consultar,'$ntarjeta');\" >$i</a> ";
			  }
			else
			 {
			   if ($ntarjeta!='' and $tipo_usuario!=0 and $vendedor==0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
			    {
				   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDos($i,$consultar,'$ntarjeta',$tipo_usuario);\" >$i</a> ";
				}
			   else
				{
				  if ($ntarjeta!='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
				  	{
					   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorTres($i,$consultar,'$ntarjeta',$tipo_usuario,$vendedor);\" >$i</a> ";
					}
				  else
					{
					   if ($ntarjeta!='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
					   	{
						  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCuatro($i,$consultar,'$ntarjeta',$tipo_usuario,$vendedor,$promotor);\" >$i</a> ";
						}
					   else
						{
							if ($ntarjeta!='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
							 {
								$texto.= "<a href=\"$enlace\" onclick=\"PaginadorCinco($i,$consultar,'$ntarjeta',$tipo_usuario,$vendedor,$promotor,'$fechainicial','$fechafinal');\" >$i</a> ";
							 }
							else
							 {
								if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor==0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
								 {
								   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSeis($i,$consultar,$tipo_usuario);\" >$i</a> ";
								 }
								else
								 {
									if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
									 {
									   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSiete($i,$consultar,$tipo_usuario,$vendedor);\" >$i</a> ";
									 }
									else
									 {
									   if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor!='0' and $fechainicial=='' and  $fechafinal=='')
										{
										  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorOcho($i,$consultar,$tipo_usuario,$vendedor,$promotor);\" >$i</a> ";
										}
										else
										{
										 if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
										 {
											$texto.= "<a href=\"$enlace\" onclick=\"PaginadorNueve($i,$consultar,$tipo_usuario,$vendedor,$promotor,'$fechainicial','$fechafinal');\" >$i</a> ";
										 }
										else
										 {
										  if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
											{
											 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiez($i,$consultar,$vendedor);\" >$i</a> ";
											}
										   else
											{
											 if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
											 {
												$texto.= "<a href=\"$enlace\" onclick=\"PaginadorOnce($i,$consultar,$vendedor,$promotor);\" >$i</a> ";
											 }
											else
											 {
											  if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
											  {
												$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDoce($i,$consultar,$vendedor,$promotor,'$fechainicial','$fechafinal');\" >$i</a> ";
											  }
											  else
												{
												 if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor==0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
												 {
												  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorTrece($i,$consultar,$promotor);\" >$i</a> ";
												 }
												else
												 {
												   if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor==0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
													{
													  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCatorce($i,$consultar,$promotor,'$fechainicial','$fechafinal');\" >$i</a> ";																																		
													}
												  else
													{
													if ($ntarjeta!='' and $tipo_usuario==0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
													{
													 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorQuince($i,$consultar,'$ntarjeta',$vendedor);\" >$i</a> ";
													 }
													 else
													  {
													  	if ($ntarjeta!='' and $tipo_usuario==0 and $vendedor==0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
														 {
														 	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezSeis($i,$consultar,'$ntarjeta',$promotor);\" >$i</a> ";
														 }
														else
														 {
														 	if ($ntarjeta!='' and $tipo_usuario==0 and $vendedor==0 and $promotor==0 and $fechainicial!='' and  $fechafinal!='')
															 {
															   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezSiete($i,$consultar,'$ntarjeta','$fechainicial','$fechafinal');\" >$i</a> ";
															 }
															else
															{
																if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor==0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
																{
																	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezOcho($i,$consultar,$tipo_usuario,$promotor);\" >$i</a> ";
																}
																else
																{
																	if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor==0 and $promotor==0 and $fechainicial!='' and  $fechafinal!='')
																	{
																	 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezNueve($i,$consultar,$tipo_usuario,'$fechainicial','$fechafinal');\" >$i</a> ";
																	}
																	else
																	{
																		if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor!=0 and $promotor==0 and $fechainicial!='' and  $fechafinal!='')
																		{
																			 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorVeinte($i,$consultar,$vendedor,'$fechainicial','$fechafinal');\" >$i</a> ";
																		}
																		else
																			{
																				if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor==0 and $promotor==0 and $fechainicial!='' and  $fechafinal!='')
																					{
																						$texto.= "<a href=\"$enlace\" onclick=\"PaginadorVeinteUno($i,$consultar,'$fechainicial','$fechafinal');\" >$i</a> ";
																					}
																				else
																					{
																						if ($ntarjeta!='' and $tipousuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
																							{
																								$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteDos($i,$consultar,'$ntarjeta',$vendedor,$promotor);\" >$i</a> ";
																							}
																						else
																							{
																								if ($ntarjeta!='' and $tipousuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
																									{
																										$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteTres($i,$consultar,'$ntarjeta',$vendedor,$promotor,'$fechainicial','$fechafinal');\" >$i</a> ";
																									}
																								else
																									{
																										if ($ntarjeta!='' and $tipousuario!=0 and $vendedor==0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
																											{
																												$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteCuatro($i,$consultar,'$ntarjeta',$tipousuario,$promotor);\" >$i</a> ";
																											}
																									else
																											{
																												if ($ntarjeta!='' and $tipousuario!=0 and $vendedor==0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
																												{
																													$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteCinco($i,$consultar,'$ntarjeta',$tipousuario,$promotor,'$fechainicial','$fechafinal');\" >$i</a> ";
																												}
																											}
																									}
																							}
																					}
																			}
																		
																	}
																}
															}
														 } 
													  }
																																	
																													
																																}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
	   }
	   $texto.= "<b>$actual</b> ";
       for ($i=$actual+1; $i<=$maximo ; $i++)
         {
		 if (($ntarjeta=='' or $ntarjeta==0) and ($tipo_usuario=='' or $tipo_usuario==0) and ($vendedor=='' or $vendedor==0) and ($promotor=='' or $promotor==0) and $fechainicial=='' and  $fechafinal=='')
		  {
			$texto.= "<a href=\"$enlace\" onclick=\"Paginador_Uno($i,$consultar);\" >$i</a> ";
		  }
		else
		 {
			if ($ntarjeta!='' and $tipo_usuario==0 and $vendedor==0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
			  {
				 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorUno($i,$consultar,'$ntarjeta');\" >$i</a> ";
			  }
			else
			 {
			   if ($ntarjeta!='' and $tipo_usuario!=0 and $vendedor==0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
			    {
				   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDos($i,$consultar,'$ntarjeta',$tipo_usuario);\" >$i</a> ";
				}
			   else
				{
				  if ($ntarjeta!='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
				  	{
					   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorTres($i,$consultar,'$ntarjeta',$tipo_usuario,$vendedor);\" >$i</a> ";
					}
				  else
					{
					   if ($ntarjeta!='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor!='0' and $fechainicial=='' and  $fechafinal=='')
					   	{
						  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCuatro($i,$consultar,'$ntarjeta',$tipo_usuario,$vendedor,$promotor);\" >$i</a> ";
						}
					   else
						{
							if ($ntarjeta!='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
							 {
								$texto.= "<a href=\"$enlace\" onclick=\"PaginadorCinco($i,$consultar,'$ntarjeta',$tipo_usuario,$vendedor,$promotor,'$fechainicial','$fechafinal');\" >$i</a> ";
							 }
							else
							 {
								if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor==0 and $promotor=='0' and $fechainicial=='' and  $fechafinal=='')
								 {
								   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSeis($i,$consultar,$tipo_usuario);\" >$i</a> ";
								 }
								else
								 {
									if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
									 {
									   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSiete($i,$consultar,$tipo_usuario,$vendedor);\" >$i</a> ";
									 }
									else
									 {
									   if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor!='0' and $fechainicial=='' and  $fechafinal=='')
										{
										  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorOcho($i,$consultar,$tipo_usuario,$vendedor,$promotor);\" >$i</a> ";
										}
										else
										{
										 if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
										 {
											$texto.= "<a href=\"$enlace\" onclick=\"PaginadorNueve($i,$consultar,$tipo_usuario,$vendedor,$promotor,'$fechainicial','$fechafinal');\" >$i</a> ";
										 }
										else
										 {
										  if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
											{
											 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiez($i,$consultar,$vendedor);\" >$i</a> ";
											}
										   else
											{
											 if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
											 {
												$texto.= "<a href=\"$enlace\" onclick=\"PaginadorOnce($i,$consultar,$vendedor,$promotor);\" >$i</a> ";
											 }
											else
											 {
											  if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
											  {
												$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDoce($i,$consultar,$vendedor,$promotor,'$fechainicial','$fechafinal');\" >$i</a> ";
											  }
											  else
												{
												 if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor==0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
												 {
												  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorTrece($i,$consultar,$promotor);\" >$i</a> ";
												 }
												else
												 {
												   if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor==0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
													{
													  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCatorce($i,$consultar,$promotor,'$fechainicial','$fechafinal');\" >$i</a> ";																																		
													}
												  else
													{
													if ($ntarjeta!='' and $tipo_usuario==0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
													{
													 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorQuince($i,$consultar,'$ntarjeta',$vendedor);\" >$i</a> ";
													 }
													 else
													  {
													  	if ($ntarjeta!='' and $tipo_usuario==0 and $vendedor==0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
														 {
														 	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezSeis($i,$consultar,'$ntarjeta',$promotor);\" >$i</a> ";
														 }
														else
														 {
														 	if ($ntarjeta!='' and $tipo_usuario==0 and $vendedor==0 and $promotor==0 and $fechainicial!='' and  $fechafinal!='')
															 {
															   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezSiete($i,$consultar,'$ntarjeta','$fechainicial','$fechafinal');\" >$i</a> ";
															 }
															else
															{
																if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor==0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
																{
																	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezOcho($i,$consultar,$tipo_usuario,$promotor);\" >$i</a> ";
																}
																else
																{
																	if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor==0 and $promotor==0 and $fechainicial!='' and  $fechafinal!='')
																	{
																	 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezNueve($i,$consultar,$tipo_usuario,'$fechainicial','$fechafinal');\" >$i</a> ";
																	}
																	else
																	{
																		if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor!=0 and $promotor==0 and $fechainicial!='' and  $fechafinal!='')
																		{
																			 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorVeinte($i,$consultar,$vendedor,'$fechainicial','$fechafinal');\" >$i</a> ";
																		}
																		else
																		{
																			if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor=='0x' and $promotor=='0' and $fechainicial!='' and  $fechafinal!='')
																			{
																			$texto.= "<a href=\"$enlace\" onclick=\"PaginadorVeinteUno($i,$consultar,'$fechainicial','$fechafinal');\" >$i</a> ";
																			}
																			else
																			{
																			if ($ntarjeta!='' and $tipousuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
																							{
																								$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteDos($i,$consultar,'$ntarjeta',$vendedor,$promotor);\" >$i</a> ";
																							}
																						else
																							{
																								if ($ntarjeta!='' and $tipousuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
																									{
																										$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteTres($i,$consultar,'$ntarjeta',$vendedor,$promotor,'$fechainicial','$fechafinal');\" >$i</a> ";
																									}
																								else
																									{
																										if ($ntarjeta!='' and $tipousuario!=0 and $vendedor==0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
																											{
																												$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteCuatro($i,$consultar,'$ntarjeta',$tipousuario,$promotor);\" >$i</a> ";
																											}
																									else
																											{
																												if ($ntarjeta!='' and $tipousuario!=0 and $vendedor==0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
																												{
																													$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteCinco($i,$consultar,'$ntarjeta',$tipousuario,$promotor,'$fechainicial','$fechafinal');\" >$i</a> ";
																												}
																											}
																									}
																							}
																			}
																		}
																		
																	}
																}
															}
														 } 
													  }
																																	
																													
																																}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
		  }
   if ($maximo!=$totalPaginas)
            {
                $texto.= "... ";
            }
	if ($actual<$totalPaginas)
            {
			if (($ntarjeta=='' or $ntarjeta==0) and ($tipo_usuario=='' or $tipo_usuario==0) and ($vendedor=='' or $vendedor==0) and ($promotor=='' or $promotor==0) and $fechainicial=='' and  $fechafinal=='')
		  {
			$texto.= "<a href=\"$enlace\" onclick=\"Paginador_Uno($posterior,$consultar);\" >Siguiente&raquo;</a> ";
		  }
		else
		 {
			if ($ntarjeta!='' and $tipo_usuario==0 and $vendedor==0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
			  {
				 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorUno($posterior,$consultar,'$ntarjeta');\" >Siguiente&raquo;</a> ";
			  }
			else
			 {
			   if ($ntarjeta!='' and $tipo_usuario!=0 and $vendedor==0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
			    {
				   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDos($posterior,$consultar,'$ntarjeta',$tipo_usuario);\" >Siguiente&raquo;</a> ";
				}
			   else
				{
				  if ($ntarjeta!='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
				  	{
					   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorTres($posterior,$consultar,'$ntarjeta',$tipo_usuario,$vendedor);\" >Siguiente&raquo;</a> ";
					}
				  else
					{
					   if ($ntarjeta!='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
					   	{
						  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCuatro($posterior,$consultar,'$ntarjeta',$tipo_usuario,$vendedor,$promotor);\" >Siguiente&raquo;</a> ";
						}
					   else
						{
							if ($ntarjeta!='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
							 {
								$texto.= "<a href=\"$enlace\" onclick=\"PaginadorCinco($posterior,$consultar,'$ntarjeta',$tipo_usuario,$vendedor,$promotor,'$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
							 }
							else
							 {
								if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor==0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
								 {
								   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSeis($posterior,$consultar,$tipo_usuario);\" >Siguiente&raquo;</a> ";
								 }
								else
								 {
									if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
									 {
									   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSiete($posterior,$consultar,$tipo_usuario,$vendedor);\" >Siguiente&raquo;</a> ";
									 }
									else
									 {
									   if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
										{
										  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorOcho($posterior,$consultar,$tipo_usuario,$vendedor,$promotor);\" >Siguiente&raquo;</a> ";
										}
										else
										{
										 if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
										 {
											$texto.= "<a href=\"$enlace\" onclick=\"PaginadorNueve($posterior,$consultar,$tipo_usuario,$vendedor,$promotor,'$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
										 }
										else
										 {
										  if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
											{
											 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiez($posterior,$consultar,$vendedor);\" >Siguiente&raquo;</a> ";
											}
										   else
											{
											 if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
											 {
												$texto.= "<a href=\"$enlace\" onclick=\"PaginadorOnce($posterior,$consultar,$vendedor,$promotor);\" >Siguiente&raquo;</a> ";
											 }
											else
											 {
											  if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
											  {
												$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDoce($posterior,$consultar,$vendedor,$promotor,'$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
											  }
											  else
												{
												 if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor==0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
												 {
												  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorTrece($posterior,$consultar,$vendedor);\" >Siguiente&raquo;</a> ";
												 }
												else
												 {
												   if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor==0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
													{
													  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCatorce($posterior,$consultar,$promotor,'$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";																																		
													}
												  else
													{
													if ($ntarjeta!='' and $tipo_usuario==0 and $vendedor!=0 and $promotor==0 and $fechainicial=='' and  $fechafinal=='')
													{
													 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorQuince($posterior,$consultar,'$ntarjeta',$vendedor);\" >Siguiente&raquo;</a> ";
													 }
													 else
													  {
													  	if ($ntarjeta!='' and $tipo_usuario==0 and $vendedor==0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
														 {
														 	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezSeis($posterior,$consultar,'$ntarjeta','$promotor');\" >Siguiente&raquo;</a> ";
														 }
														else
														 {
														 	if ($ntarjeta!='' and $tipo_usuario==0 and $vendedor==0 and $promotor==0 and $fechainicial!='' and  $fechafinal!='')
															 {
															   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezSiete($posterior,$consultar,'$ntarjeta','$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
															 }
															else
															{
																if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor==0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
																{
																	$texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezOcho($posterior,$consultar,$tipo_usuario,$promotor);\" >Siguiente&raquo;</a> ";
																}
																else
																{
																	if ($ntarjeta=='' and $tipo_usuario!=0 and $vendedor==0 and $promotor==0 and $fechainicial!='' and  $fechafinal!='')
																	{
																	 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDiezNueve($posterior,$consultar,$tipo_usuario,'$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
																	}
																	else
																	{
																		if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor!=0 and $promotor=='0' and $fechainicial!='' and  $fechafinal!='')
																		{
																			 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorVeinte($posterior,$consultar,$vendedor,'$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
																		}
																	else
																		{
																			if ($ntarjeta=='' and $tipo_usuario==0 and $vendedor==0 and $promotor==0 and $fechainicial!='' and  $fechafinal!='')
																			{
																				$texto.= "<a href=\"$enlace\" onclick=\"PaginadorVeinteUno($posterior,$consultar,'$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
																			}
																			else
																			{
																				if ($ntarjeta!='' and $tipousuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
																							{
																								$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteDos($posterior,$consultar,'$ntarjeta',$vendedor,$promotor);\" >Siguiente&raquo;</a> ";
																							}
																						else
																							{
																								if ($ntarjeta!='' and $tipousuario==0 and $vendedor!=0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
																									{
																										$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteTres($posterior,$consultar,'$ntarjeta',$vendedor,$promotor,'$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
																									}
																								else
																									{
																										if ($ntarjeta!='' and $tipousuario!=0 and $vendedor==0 and $promotor!=0 and $fechainicial=='' and  $fechafinal=='')
																											{
																												$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteCuatro($posterior,$consultar,'$ntarjeta',$tipousuario,$promotor);\" >Siguiente&raquo;</a> ";
																											}
																									else
																											{
																												if ($ntarjeta!='' and $tipousuario!=0 and $vendedor==0 and $promotor!=0 and $fechainicial!='' and  $fechafinal!='')
																												{
																													$texto = "<a href=\"$enlace\" onclick=\"PaginadorVeinteCinco($posterior,$consultar,'$ntarjeta',$tipousuario,$promotor,'$fechainicial','$fechafinal');\" >Siguiente&raquo;</a> ";
																												}
																											}
																									}
																							}
																			}
																			
																		}
																		
																	}
																}
															}
														 } 
													  }
																																	
																													
																																}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
            }
        else
            {
                $texto .= "<b>&raquo;</b>";
            }
        return $texto;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function paginarporfiltros_rr($actual, $total, $porPagina, $enlace, $maxpags=0,$consultar,$servicio,$fechainicio,$numerotarjeta) 
{
  if ($actual=="")
     {
        $actual=1;
     }
 $totalPaginas = ceil($total/$porPagina);
 $anterior = $actual - 1;
 $posterior = $actual + 1;
 # condicion ? entonces : sino;
 $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
 $maximo = $maxpags ? min($totalPaginas, $actual+floor($maxpags/2)): $totalPaginas;
 //echo $actual;
 //echo  $anterior;
 //echo $posterior;
 if ($actual>1)
	 {
		if (($servicio=='' or $servicio=='0x') and ($fechainicio=='' or $fechainicio==0) and ($numerotarjeta=='' or $numerotarjeta==0))
		  {
			$texto = "<a href=\"$enlace\" onclick=\"Paginador_Uno($anterior,$consultar);\" >&laquo;Anterior</a> ";
		  }
		else
		 {
			if ($servicio!='0x' and $fechainicio=='' and $numerotarjeta=='')
			  {
				 $texto = "<a href=\"$enlace\" onclick=\"PaginadorUno($anterior,$consultar,'$servicio');\" >&laquo;Anterior</a> ";
			  }
			else
			 {
			   if ($servicio!='0x' and $fechainicio!='' and $numerotarjeta=='')
			    {
				   $texto = "<a href=\"$enlace\" onclick=\"PaginadorDos($anterior,$consultar,'$servicio','$fechainicio');\" >&laquo;Anterior</a> ";
				}
			   else
				{
				  if ($servicio!='0x' and $fechainicio!=0 and $numerotarjeta!=0)
				  	{
					   $texto = "<a href=\"$enlace\" onclick=\"PaginadorTres($anterior,$consultar,'$servicio','$fechainicio','$numerotarjeta');\" >&laquo;Anterior</a> ";
					}
				  else
					{
					   if ($servicio=='0x' and $fechainicio!=0 and $numerotarjeta==0)
					   	{
						  $texto = "<a href=\"$enlace\" onclick=\"PaginadorCuatro($anterior,$consultar,'$fechainicio');\" >&laquo;Anterior</a> ";
						}
					   else
						{
							if ($servicio=='0x' and $fechainicio!=0 and $numerotarjeta!=0)
							 {
								$texto = "<a href=\"$enlace\" onclick=\"PaginadorCinco($anterior,$consultar,'$fechainicio','$numerotarjeta');\" >&laquo;Anterior</a> ";
							 }
							else
							 {
								if ($servicio!='0x' and $fechainicio==0 and $numerotarjeta!=0)
								 {
								   $texto = "<a href=\"$enlace\" onclick=\"PaginadorSeis($anterior,$consultar,'$servicio','$numerotarjeta');\" >&laquo;Anterior</a> ";
								 }
							}
						}
					}
									}
							}
					}
			}
	else
           {
               $texto = "<b>&laquo;";
           }		
	# $var = $var  expresion equivalente $var .=
	if ($minimo!=1)
       {
           $texto.= "... ";
       }
	 for ($i=$minimo; $i<$actual; $i++)
       {
	   	if (($servicio=='' or $servicio=='0x') and ($fechainicio=='' or $fechainicio==0) and ($numerotarjeta=='' or $numerotarjeta==0))
		  {
			$texto.= "<a href=\"$enlace\" onclick=\"Paginador_Uno($i,$consultar);\" >$i</a> ";
		  }
		else
		 {
			if ($servicio!='0x' and $fechainicio=='' and $numerotarjeta=='')
			  {
				 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorUno($i,$consultar,'$servicio');\" >$i</a> ";
			  }
			else
			 {
			   if ($servicio!='0x' and $fechainicio!='' and $numerotarjeta=='')
			    {
				   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDos($i,$consultar,'$servicio','$fechainicio');\" >$i</a> ";
				}
			   else
				{
				 if ($servicio!='0x' and $fechainicio!=0 and $numerotarjeta!=0)
				  	{
					   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorTres($i,$consultar,'$servicio','$fechainicio','$numerotarjeta');\" >$i</a> ";
					}
				  else
					{
					   if ($servicio=='0x' and $fechainicio!=0 and $numerotarjeta==0)
					   	{
						  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCuatro($i,$consultar,'$fechainicio');\" >$i</a> ";
						}
					   else
						{
							if ($servicio=='0x' and $fechainicio!=0 and $numerotarjeta!=0)
							 {
								$texto.= "<a href=\"$enlace\" onclick=\"PaginadorCinco($i,$consultar,'$fechainicio','$numerotarjeta');\" >$i</a> ";
							 }
							else
							 {
								if ($servicio!='0x' and $fechainicio==0 and $numerotarjeta!=0)
								 {
								   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSeis($i,$consultar,'$servicio','$numerotarjeta');\" >$i</a> ";
								 }
															}
													}
											}
									}
							}
					}
	   }
	   $texto.= "<b>$actual</b> ";
       for ($i=$actual+1; $i<=$maximo ; $i++)
         {
		 if (($servicio=='' or $servicio=='0x') and ($fechainicio=='' or $fechainicio==0) and ($numerotarjeta=='' or $numerotarjeta==0))
		  {
			$texto.= "<a href=\"$enlace\" onclick=\"Paginador_Uno($i,$consultar);\" >$i</a> ";
		  }
		else
		 {
			if ($servicio!='0x' and $fechainicio=='0' and $numerotarjeta=='0')
			  {
				 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorUno($i,$consultar,'$servicio');\" >$i</a> ";
			  }
			else
			 {
			   if ($servicio!='0x' and $fechainicio!='0' and $numerotarjeta=='0')
			    {
				   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDos($i,$consultar,'$servicio','$fechainicio');\" >$i</a> ";
				}
			   else
				{
				  if ($servicio!='0x' and $fechainicio!='0' and $numerotarjeta!='0')
				  	{
					   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorTres($i,$consultar,'$servicio','$fechainicio','$numerotarjeta');\" >$i</a> ";
					}
				  else
					{
					   if ($servicio=='0x' and $fechainicio!='0' and $numerotarjeta=='0')
					   	{
						  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCuatro($i,$consultar,'$fechainicio');\" >$i</a> ";
						}
					   else
						{
							if ($servicio=='0x' and $fechainicio!='0' and $numerotarjeta!='0')
							 {
								$texto.= "<a href=\"$enlace\" onclick=\"PaginadorCinco($i,$consultar,'$fechainicio','$numerotarjeta');\" >$i</a> ";
							 }
							else
							 {
								if ($servicio!='0x' and $fechainicio=='0' and $numerotarjeta!='0')
								 {
								   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSeis($i,$consultar,'$servicio','$numerotarjeta');\" >$i</a> ";
								 }
															}
													}
											}
									}
							}
					}
		  }
   if ($maximo!=$totalPaginas)
            {
                $texto.= "... ";
            }
	if ($actual<$totalPaginas)
            {
			if (($servicio=='' or $servicio=='0x') and ($fechainicio=='' or $fechainicio==0) and ($numerotarjeta=='' or $numerotarjeta==0))
		  {
			$texto.= "<a href=\"$enlace\" onclick=\"Paginador_Uno($posterior,$consultar);\" >Siguiente&raquo;</a> ";
		  }
		else
		 {
			if ($servicio!='0x' and $fechainicio=='0' and $numerotarjeta=='0')
			  {
				 $texto.= "<a href=\"$enlace\" onclick=\"PaginadorUno($posterior,$consultar,'$servicio');\" >Siguiente&raquo;</a> ";
			  }
			else
			 {
			   if ($servicio!='0x' and $fechainicio!='0' and $numerotarjeta=='0')
			    {
				   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorDos($posterior,$consultar,'$servicio','$fechainicio');\" >Siguiente&raquo;</a> ";
				}
			   else
				{
				  if ($servicio!='0x' and $fechainicio!='0' and $numerotarjeta!='0')
				  	{
					   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorTres($posterior,$consultar,'$servicio','$fechainicio','$numerotarjeta');\" >Siguiente&raquo;</a> ";
					}
				  else
					{
					   if ($servicio=='0x' and $fechainicio!='0' and $numerotarjeta=='0')
					   	{
						  $texto.= "<a href=\"$enlace\" onclick=\"PaginadorCuatro($posterior,$consultar,'$fechainicio');\" >Siguiente&raquo;</a> ";
						}
					   else
						{
							if ($servicio=='0x' and $fechainicio!=0 and $numerotarjeta!=0)
							 {
								$texto.= "<a href=\"$enlace\" onclick=\"PaginadorCinco($posterior,$consultar,'$fechainicio','$numerotarjeta');\" >Siguiente&raquo;</a> ";
							 }
							else
							 {
								if ($servicio!='0x' and $fechainicio=='0' and $numerotarjeta=='0')
								 {
								   $texto.= "<a href=\"$enlace\" onclick=\"PaginadorSeis($posterior,$consultar,'$servicio','$numerotarjeta');\" >Siguiente&raquo;</a> ";
								 }
															}
													}
											}
									}
							}
					}
            }
        else
            {
                $texto .= "<b>&raquo;</b>";
            }
        return $texto;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function paginarconsultaT($actual, $total, $porPagina, $enlace, $maxpags=0,$consulta,$fecha1,$fecha2,$fecha3,$fecha4,$distribuidor,$estado,$tarjeta) 
    {
        if ($actual=="")
            {
                $actual=1;
            }
        $totalPaginas = ceil($total/$porPagina);
        $anterior = $actual - 1;
        $posterior = $actual + 1;
        # condicion ? entonces : sino;
        $minimo = $maxpags ? max(1, $actual-ceil($maxpags/2)): 1;
        $maximo = $maxpags ? min($totalPaginas, $actual+floor($maxpags/2)): $totalPaginas;
        if ($actual>1)
            {
             
               $texto = "<a href=\"$enlace\" onclick=\"Paginador($anterior,$consulta,$fecha1,$fecha2,$fecha3,$fecha4,$distribuidor,$estado,'$tarjeta');\" >&laquo;Anterior</a> ";
            }
        else
           {
               $texto = "<b>&laquo;";
           }
        # $var = $var  expresion equivalente $var .=
		if ($minimo!=1)
            {
                $texto.= "... ";
            }
        for ($i=$minimo; $i<$actual; $i++)
            {
				$texto .= "<a href=\"$enlace\" onclick=\"Paginador($i,$consulta,$fecha1,$fecha2,$fecha3,$fecha4,$distribuidor,$estado,$tarjeta);\">$i</a> ";
			}
        $texto .= "<b>$actual</b> ";
        for ($i=$actual+1; $i<=$maximo ; $i++)
            {
		
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($i,$consulta,$fecha1,$fecha2,$fecha3,$fecha4,$distribuidor,$estado,$tarjeta);\">$i</a> ";
				
            }
        if ($maximo!=$totalPaginas)
            {
                $texto.= "... ";
            }
        if ($actual<$totalPaginas)
            {
			
                $texto .= "<a href=\"$enlace\" onclick=\"Paginador($posterior,$consulta,$fecha1,$fecha2,$fecha3,$fecha4,$distribuidor,$estado,$tarjeta);\">Siguiente&raquo;</a>";
				
            }
        else
            {
                $texto .= "<b>&raquo;</b>";
            }
        return $texto;
}

}
?>
