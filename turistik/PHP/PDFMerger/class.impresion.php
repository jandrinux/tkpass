<?php

	/**
	* 
	*/
	class Impresion 
	{
		public $bp;
		public $afecta;
		public $exenta;
		public $save_to  = "PDFMerger/pdftemp/";
		public $url_final ="";
		public $url =true;
		public $estaAfecta =false;

		function d($file_url) {
			$ran = $this->save_to."".$this->random();
	        $content = file_get_contents($file_url);
	        file_put_contents($ran, $content);
	        return $ran;
   		}

   		function random() {
   			$numbers = range(1, 19000);
			shuffle($numbers);
			$numbers [0];
   			return $numbers [0].".pdf";
   		}

   		function union(){
   			include 'PDFMerger.php';
			$pdf = new PDFMerger;
			if ($this->url) {
				//$bp = $this->d($this->bp);
				if ($this->estaAfecta) {
					$afecta = $this->d($this->afecta);
				}
				$exenta = $this->d($this->exenta);
			}
			else{
				// $bp = $this->bp;
				 if ($this->estaAfecta) {
				 	$afecta = $this->afecta;
				 }
				 
				 $exenta = $this->exenta;
			}
			
			$this->url_final =$this->save_to.$this->random();

			if ($this->estaAfecta) {
				$pdf->addPDF($exenta, 'all')			
				->addPDF($afecta, 'all')
				->merge('bowser', $this->url_final);
			}
			else{
				$pdf->addPDF($exenta, 'all')
					->merge('bowser', $this->url_final);
			}
			

			return $this->url_final;
   		}

	}