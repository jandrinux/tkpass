<?php	
require ("../Clases/ClaseConexion.inc.php");
include( "../Clases/class.TemplatePower.inc.php"); 
require ("../Clases/ClaseUtil.inc.php");
//make a new TemplatePower object
$tpl = new TemplatePower( "../Plantillas/ListaMeses.tpl" );
$miConexion= new ClaseConexion;
$_Util=new Util;
$tpl->prepare();
$tpl->assign("fecha", $_Util->Fecha());

$meses=array('ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE');
$i=0;
foreach ($meses as $actual ) 
    {
        $i++;
        $tpl->newBlock("bloquemes");
        $tpl->assign("CODIGOMES", $i );
        $tpl->assign("DESCRIPCIONMES", $actual );
		if ($_POST['mes'] == $i ) 
           {
               $tpl->assign("SELECCIONAR", 'selected' );
           }
       
    }	
$tpl->printToScreen();
?>
