var ieversion=/*@cc_on function(){ switch(@_jscript_version){ case 1.0:return 3;
		      case 3.0:return 4;case 5.0:return 5; case 5.1:return 5; case 5.5:return 5.5;
		      case 5.6:return 6; case 5.7:return 7; }}()||@*/0;

var _vROFonChange = [];
var ROF = function(){
	var _vObjROF = [];
	var _vObjROFset = [];
	
	var DI=document.getElementsByTagName('INPUT');		
	for(var i=0;i<DI.length; i++){
		var _obj = DI[i];		
		if(_obj.className){
			var _clas = _obj.className.toString().toLowerCase();
			if(_clas.indexOf('input')>-1){
				_obj.parentNode.className = 'rofInput';								
			}else if(_clas.indexOf('select')>-1){
				_obj.parentNode.className = 'rofSelect';
				var o = document.createElement('input');
				o.setAttribute('type', 'hidden');
				o.setAttribute('value', _obj.value);
				o.setAttribute('id', _obj.id);
				o.setAttribute('name', _obj.name);
						
				if(_obj.onchange){
					var _f = _obj.onchange.toString()
					_f = _f.substring(_f.indexOf("{")+1,_f.lastIndexOf("}"));
					_vROFonChange[i] = _f;
				}else{
					_vROFonChange[i] = null;
				}				
				
				_vObjROF.push(_obj.id);
				
				_obj.parentNode.appendChild(o);		
						
				_obj.parentNode.onclick = new Function('Combo(this.childNodes[0],\'' + _obj.name + '\',\'' + _obj.name + 'Hrof\',' + i + ');');

			
				_obj.setAttribute('id', _obj.id + 'Hrof');
				_obj.setAttribute('name', _obj.name + 'Hrof');
				_obj.setAttribute('readOnly', true);									
			}
		}				
	}
	var _aCount = 0;
	for(var a=0;a<_vObjROF.length; a++){
		var _cROF = $(_vObjROF[a]).parentNode.parentNode;
		_vObjROFset[_aCount] = false;
		_aCount++;
		
		for(var n=0;n<_cROF.childNodes.length;n++){
			if(_cROF.childNodes[n].className){
				if(_cROF.childNodes[n].className.toString().toLowerCase().indexOf('rofoptions')>-1){
					var _cROFa = _cROF.childNodes[n].childNodes;
					
					for(var t=0;t<_cROFa.length;t++){
						if(_cROFa[t].className){
							if(_cROFa[t].className.toString().toLowerCase().indexOf('selectedrof')>-1){								
								document.getElementById(_vObjROF[a]).value = _cROFa[t].rel;
								document.getElementById(_vObjROF[a]+'Hrof').value = _cROFa[t].innerHTML;
								_vObjROFset[_aCount] = true;							
							}
						}else{						
							if(!_vObjROFset[_aCount] && typeof(_cROFa[t].rel) !== 'undefined'){								
								document.getElementById(_vObjROF[a]).value = _cROFa[t].rel;
								document.getElementById(_vObjROF[a]+'Hrof').value = _cROFa[t].innerHTML;								
								_vObjROFset[_aCount] = true;
							}
						}
					}
				}
			}
		}
	}
}
_c = 'nodis';
var _i1, _i2, _d;
var _zindex = 1;
var _memD = null;
var _fixIE = (navigator.appName == "Microsoft Internet Explorer") ? 12 : 15;
Combo = function(d, i1, i2, f){
	var _objROFopt = null;
	var _cROF = d.parentNode.parentNode;
	for(var n=0;n<_cROF.childNodes.length;n++){
		if(_cROF.childNodes[n].className){
			if(_cROF.childNodes[n].className.toString().toLowerCase().indexOf('rofoptions')>-1){
				_objROFopt = _cROF.childNodes[n];
				n = 500000;
			}
		}
	}
	if(!_objROFopt) return false;
	
	//
	if(_objROFopt.style.display == 'block'){
		_objROFopt.style.display = 'none';
	}else{
		
		_zindex++;
		_objROFopt.style.zIndex = _zindex;
		_objROFopt.style.display = 'block';
		
		var _tm = ($(i2).getPosition().y - $(_objROFopt).getParent().getPosition().y)+$(i2).getSize().y-1;
		if (ieversion == 7) 
		{
		_objROFopt.style.marginTop = (_tm -23) + 'px'
		}
		else
		{
		_objROFopt.style.marginTop = _tm + 'px'
		}
		
		var _tx=$(_objROFopt).getParent().getPosition().x;

		_objROFopt.style.width = (GE(i2).getSize().x)+'px';			
			
		if (ieversion == 7) 
		{
		_objROFopt.style.left = _tx - 190;
		}
		
		var _nodos = _objROFopt.childNodes;
		if(_nodos){
			for(x in _nodos){
				if(_nodos[x]){
					if(_nodos[x].nodeName){
						if(_nodos[x].nodeName == 'A'){
							var _f = (_vROFonChange[f]) ? _vROFonChange[f] : "";
							_nodos[x].onclick = new Function("ValorItem(this);" + _f + ";return false;");
							_nodos[x].onkeypress = new Function("ValorItem(this);" + _f + ";return false;");
						}
					}
				}
			}
		}
	}
	
	_i1 = i1;
	_i2 = i2;
		
	if(_memD){
		if(_memD !== _objROFopt){			
			_memD.style.display = 'none';
		}
	}
	_memD = _objROFopt;
}

ValorItem = function(o){
	GE(_i1).value = o.rel;
	GE(_i2).value = o.innerHTML;
	
	GE(_i2).focus();
	GE(_i2).blur();
	o.parentNode.style.display = 'none';
	////	PATRICIO N.
	if (o.parentNode.id=="paisDIV")
	{
		var xhReq = new XMLHttpRequest();
		xhReq.open("GET", "/includes/web_combobox_regiones_div.asp?_value="+o.rel, false);
		xhReq.send(null);
		var serverResponse = xhReq.responseText;
		document.getElementById("regionDIV").innerHTML=serverResponse;
				
		valor = -1
		if(o.rel == 40)
		{
		valor = 13;
		GE('region').value=0;
		GE('regionHrof').value = 'Elija Regi�n (Chile)';
		}
		else
		{
		GE('region').value=0;
		GE('regionHrof').value = 'Elija Regi�n (No Aplica)';		
		}
		
		var xhReq = new XMLHttpRequest();
		xhReq.open("GET", "/includes/web_combobox_comunas_div.asp?value_=" +valor, false);
		xhReq.send(null);
		var serverResponse = xhReq.responseText;
		document.getElementById("comunaDIV").innerHTML=serverResponse;
		GE('comuna').value=0;
		if(valor == 13)
		{
		GE('comunaHrof').value = 'Elija Comuna (Metropolitana RM)';
		}
		else
		{
		GE('comunaHrof').value = 'Elija Comuna (No Aplica)';
		}
	}
	
	if (o.parentNode.id=="regionDIV")
	{
		var xhReq = new XMLHttpRequest();
		xhReq.open("GET", "/includes/web_combobox_comunas_div.asp?_value=" +o.rel, false);
		xhReq.send(null);
		var serverResponse = xhReq.responseText;
		document.getElementById("comunaDIV").innerHTML=serverResponse;
		GE('comuna').value=0;
		GE('comunaHrof').value = 'Elija Comuna ('+ o.innerHTML+')';

	}
	
	
}
function findPos(obj){
	var posX = obj.offsetLeft;var posY = obj.offsetTop;
	while(obj.offsetParent){
		posX+=obj.offsetParent.offsetLeft;
		posY+=obj.offsetParent.offsetTop;
		if(obj==document.getElementsByTagName('body')[0]){break}
		else{obj=obj.offsetParent;}
	}
	return [posX,posY]
}
ReValuarROF = function(s){
	var _cROF = document.getElementById(s).parentNode.parentNode;
	var _vObjROFset = false;
	for(var n=0;n<_cROF.childNodes.length;n++){
		
		if(_cROF.childNodes[n].className){
			if(_cROF.childNodes[n].className.toString().toLowerCase().indexOf('rofoptions')>-1){
				var _cROFa = _cROF.childNodes[n].childNodes;
				for(var t=0;t<_cROFa.length;t++){
					if(_cROFa[t].className){
						if(_cROFa[t].className.toString().toLowerCase().indexOf('selectedrof')>-1){								
							document.getElementById(s).value = _cROFa[t].rel;
							document.getElementById(s+'Hrof').value = _cROFa[t].innerHTML;	
							_vObjROFset = true;
						}
					}else{						
						if(!_vObjROFset && typeof(_cROFa[t].rel) !== 'undefined'){								
							document.getElementById(s).value = _cROFa[t].rel;
							document.getElementById(s+'Hrof').value = _cROFa[t].innerHTML;
							_vObjROFset = true;
						}
					}
				}
			}
		}
	}
}
document.writeln('<link rel="stylesheet" type="text/css" href="/base/ROF/combo.css" />');
