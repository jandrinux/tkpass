<?
//ini_set('display_errors', 1);
//include("menus.php");
session_start();
date_default_timezone_set("America/Santiago");
require ("../Clases/ClaseConexion.inc.php");
include( "../Clases/class.TemplatePower.inc.php"); 
require ("../Clases/ClaseUtil.inc.php");
require ("../functions/tkpass.php");
$_Util=new Util;


$fecha = $_Util->Fecha();
$excursion = $_GET['excursion'];
$fecha = $_GET['fecha'];
$fecha_format = $_GET['fecha'];
$usuario = $_GET['user'];
$user_excel =  $_GET['usuario'];

$fecha_f = $_GET['fecha_f'];
$fecha_ff = $_GET['fecha_ff'];
$tipo = $_GET['tipo'];
$distribuidor = $_GET['distribuidor'];

?>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Reserva Excursiones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" type="text/JavaScript">

function Aceptar()
{
document.form1.guardar.value='1';
document.form1.submit();
}

function Cancelar()
{
window.parent.close();
}

function Modificar(codigo)
{
var user;
user=document.form1.usuario.value;
miPopup = window.open("../PHP/Modificar_Factura.php?codigo="+codigo+"&usuario="+user+"","miwin","top=200,left=350,width=400,height=300,scrollbars=no,titlebar=no,location=no");
}

function imprime()
{
document.getElementById("imprimirpag").style.visibility = 'hidden';
window.print();
window.close();
}
/*
function Exportar_Excel(){
	var user;
	var fecha_f;
	var fecha_ff;
	var tipo;
	var distribuidor;
	user         = document.form1.usuario.value;
	fecha_f      = document.form1.fecha_f.value;
	fecha_ff     = document.form1.fecha_ff.value;
	tipo         = document.form1.tipo.value;
	distribuidor = document.form1.distribuidor.value;
	
	miPopup = window.open("../PHP/GeneraExcel_Detalle_Reserva_Cobrada.php?usuario="+user+"&fecha_f="+fecha_f+"&fecha_ff="+fecha_ff+"&tipo="+tipo+"&distribuidor="+distribuidor+"","miwin","top=200,left=350,width=400,height=300,scrollbars=no,titlebar=no,location=no");
	
	
}
*/
function formatNumber(num,prefix){
   prefix = prefix || '';
   num += '';
   var splitStr = num.split('.');
   var splitLeft = splitStr[0];
   var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
   var regx = /(\d+)(\d{3})/;
   while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
   }
   return prefix + splitLeft + splitRight;
}

function validate()
{

var cantidad = document.getElementById('cantidad').value;

var total = 0;
var cant = 0;
document.form1.precio.value=total;

for (var i = 1; i <= cantidad; i++)
{
 var names = "precio_re" + i; 
 var n = "check" + i; 


   if (document.getElementById(n).checked){
    var v = document.getElementById(names).value;
        total =parseInt(total) + parseInt(v); 
        cant++;
    }
    

}

if (cant > 1)
{
    document.getElementById('parcial').style.visibility="hidden";
    document.getElementById('rechazado').style.visibility="hidden";
}
else
{
    document.getElementById('parcial').style.visibility="visible"; 
    document.getElementById('rechazado').style.visibility="visible"; 
}

total = formatNumber(total,''); 
document.form1.precio.value=total;
return true;
}

function Pagar()
{

    total = document.form1.precio.value;

    if(total == "0")
    {
      alert("Seleccione al menos un registro");
    }  
    else
    {
        alert("Registros Actualizados");
    }  

        
}

function busca_codigo(){
    total = document.form1.precio.value;

    if (total != "0")
    {
       miPopup = window.open("../PHP/pago_parcial.php?total="+total+"","miwin","top=350,left=430,width=470,height=70,scrollbars=no,titlebar=no,location=no");
 		miPopup.focus(); 
    }
    else
    {
        alert("Seleccione al menos un registro");
    }
}

</script>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
-->
</style>
</head>

<body>

 <div id="Layer1" style="position:absolute; left:5px; top:124px; width:991px; height:240px; z-index:1">
  <form name="form1" method="post" action="validar_pago_reserva.php">
    <table width="1280" border="0" cellpadding="0" cellspacing="0">
      <tr class="celdalazul_peque">
      
        <td width="60" class="celdalazul_peque"><div align="center">Fecha Trans.</div></td>
        <td class="celdalazul_peque"><div align="center">Medio de pago</div></td>
        <td class="celdalazul_peque"><div align="center">Num. Documento</div></td>
        <td class="celdalazul_peque" width="111"><div align="center">Banco</div></td>
        <td class="celdalazul_peque"><div align="center">Num. Factura</div></td>
        <td class="celdalazul_peque"><div align="center">Fecha Factura</div></td>
        
        <td class="celdalazul_peque" width="60">Fecha Servicio
        <input name="usuario" type="hidden" id="usuario" value="<? echo $usuario ?>"></td>
		<input name="fecha_f" type="hidden" id="fecha_f" value="<? echo $fecha_f ?>"></td>
		<input name="fecha_ff" type="hidden" id="fecha_ff" value="<? echo $fecha_ff ?>"></td>
		<input name="tipo" type="hidden" id="tipo" value="<? echo $tipo ?>"></td>
		<input name="distribuidor" type="hidden" id="distribuidor" value="<? echo $distribuidor ?>"></td>
        
        <td class="celdalazul_peque" width="155">Excursion</td>
        <td class="celdalazul_peque">Valor<br>NETO</td>
        <td class="celdalazul_peque" style="display: none;">Valor<br>Abonado</td>
        <td width="94" class="celdalazul_peque">Pasajero</td>
        <td width="94" class="celdalazul_peque">Apellido</td>
        <td class="celdalazul_peque">Tipo Pasajero</td>
        <td class="celdalazul_peque"># Reserva</td> 
        <td class="celdalazul_peque" >Solicitante</td>
        <td class="celdalazul_peque" >Fecha solicitud</td>
        <td class="celdalazul_peque" width="50"><p>Estado</p> </td>
        <td class="celdalazul_peque">Agente Embarque </td>  
        <td class="celdalazul_peque">Observaciones</td>  
        <td class="celdalazul_peque"><div align="center">Seleccion</div></td>

      </tr>
      
      <?
        $doc = 0;
        
        $i=0;
        $miConexion= new ClaseConexion;
    	$miConexion->Conectar();
    	$queryT1=$miConexion->EjecutaSP("Consulta_Lista_Pasajeros_por_Validar","'".$fecha_f."','".$fecha_ff."','".$_GET['tipo']."','".$_GET['distribuidor']."'");
    	
    	
    	while ($rowT1 = mysql_fetch_assoc($queryT1))
        {
          $RAZON=$rowT1['NOMBRE_FANTASIA_DIST'];
          $documento = $rowT1['NUM_DOCUMENTO'];
          $mdp = $rowT1['MEDIO_DE_PAGO'];
          $i = $i+1;
          
            if ($doc != $rowT1['NUM_DOCUMENTO'])
            {
                echo "<tr>";
                echo "<td colspan = '18' class='celdalazul_peque'></td>";
                echo "</tr>";
            }
          
            
          echo "<tr>";
            
            $fecha_tran=$_Util->FormatearFechaHora($rowT1['FECHA_TRANSACCION']);
            echo "<td class='celdalCELEST' width='60' >".$fecha_tran."</td>";

            $med = $rowT1['MEDIO_DE_PAGO'];
            if ($med == "D")
            {
                $med = "Dep�sito";
            }
            if ($med == "T")
            {
                $med = "Transferencia";
            }

            echo "<td class='celdalCELEST'>".$med."</td>";
            echo "<td class='celdalCELEST'>".$rowT1['NUM_DOCUMENTO']."</td>";
            $banco = $rowT1['BANCO'];
            
            if ($banco == "0")
            {
                $banco = "-";
            }
            echo "<td class='celdalCELEST'>".$banco."</td>";
            
            
            echo "<td class='celdalCELEST'>".$rowT1['FACTURA_RE']."</td>";
           // $fecha_fact=$_Util->FormatearFecha($rowT1['FECHA_FACTURA_RE']);
            $fecha_fact=$_Util->FormatearFechaHora($rowT1['FECHA_FACTURA_RE']);
            
 
            echo "<td class='celdalCELEST'>".$fecha_fact."</td>";
            $fecha_re=$_Util->FormatearFecha($rowT1['FECHA_RE']);
            
            echo "<td class='celdalCELEST' width='60' >".$fecha_re."</td>";
            echo "<td class='celdalCELEST'>".$rowT1['NOMBRE_EO']."</td>";
            echo "<td class='celdalCELEST'>".number_format($rowT1['neto'])."</td>"; 
           // echo "<td class='celdalCELEST'>".number_format($rowT1['PRECIO_ABONADO_RE'] )."</td>"; 
                    
            echo "<td class='celdalCELEST'>".$rowT1['PASAJERO_RE']."</td>";
            echo "<td class='celdalCELEST'>".$rowT1['APASAJERO_RE']."</td>";
            echo "<td class='celdalCELEST'>".$rowT1['tpasajero']."</td>";
            echo "<td class='celdalCELEST'>".$rowT1['VOUCHER_RE']."</td>";
            echo "<td class='celdalCELEST'>".$rowT1['USUARIO_COBRADA_RE']."</td>";
            $fecha_co=$_Util->FormatearFecha($rowT1['FECHA_COBRADA_RE']);
            echo "<td class='celdalCELEST'>".$fecha_co."</td>";
           

            if(strtoupper($rowT1['ESTADO_EMARQUE'])=='1')
        	{
 	          $tipo="EMB.";
        	}
        	else
        	{
                $tipo="PEND.";
        	}	
            
            echo "<td class='celdalCELEST'>".$tipo."</td>";
            echo "<td class='celdalCELEST'>".$rowT1['LOGIN_USUA_EMBARQUE']."</td>";
            echo "<td class='celdalCELEST'>".$rowT1['OBSERVACIONES_CONTABLES']."</td>";
            
            //echo "<td class='celdalCELEST'><a href='Dealle_reservas_group_by_doc.php?doc=".$rowT1['NUM_DOCUMENTO']."'><img src='../Imagenes/ico_alpha_Search2_16x16.png' alt='Modifica Factura' style='cursor:hand'>".$rowT1['NUM_DOCUMENTO']."</a></td>";

            ?>
            <td class="celdalCELEST">
            
            <input type="hidden" id="precio_re<?=$i?>" value="<? echo $rowT1['neto']-$rowT1['PRECIO_ABONADO_RE'];  ?>"/>
            <input type="checkbox" id="check<?=$i?>" name="check[]"   value="<? echo $rowT1['CODIGO_RE']; ?>" onclick="validate();" />
            
            </td>
            <?


            $doc = $rowT1['NUM_DOCUMENTO'];
            
          echo "</tr>";
       }
            mysql_free_result($queryT1); 
            mysql_close();  
        ?>
    </table>
    <p>
    
    <table>
        <tr class="celdalCELEST">
            <input type="hidden" value="<? echo $i; ?>" id="cantidad" />
            <td width="136" class="celdalazul_peque"><label>Total :</label></td>
            <td width="200" class="celdalCELEST"><input type="text" readonly="" id="precio" name="precio" /></td>
        </tr>
    </table>
    
    <p>
    
      <input name="aprobado" type="submit" class="BotonTurist_Celest2" value="Aprobado" onclick="Pagar();" >
      <input name="rechazado" id="rechazado" type="submit" class="BotonTurist_Celest2" value="Rechazado" onclick="Pagar();" >   
      <input name="parcial" id="parcial" type="button" class="BotonTurist_Celest2" onClick="busca_codigo()" value="Aprobado parcial" >
      <input name="valor" type="hidden" id="valor"  />

      <input type="hidden" name="usuario" value="<? echo $_GET['usuario'] ?>">
      <input type="hidden" name="fecha_f" value="<? echo $_GET['fecha_f'] ?>">
      <input type="hidden" name="fecha_ff" value="<? echo $_GET['fecha_ff'] ?>" >
      <input type="hidden" name="tipo" value="<? echo $_GET['tipo'] ?>" >
      <input type="hidden" name="distribuidor" value="<? echo $_GET['distribuidor'] ?>" >
      
      <input name="documento" type="hidden" value="<? echo $documento ?>" />
    </p>

    <p>&nbsp;    </p>
  </form>
</div>
<div id="Layer2" style="position:absolute; left:7px; top:28px; width:500px; height:89px; z-index:2">
  <table width="493" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="80" rowspan="4"><img src="../Imagenes/IMG_6770-2.jpg"  width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="400">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="Estilo1">Listado de Pasajeros Pagados por validar </span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="TituloFecha">&nbsp;</td>
<!-- START BLOCK : tabla1 -->
      <td height="22" class="TituloFecha"> <? echo $RAZON ?></td>
<!-- END BLOCK : tabla1 -->
    </tr>
  </table>
</div>
<div id="Layer4" style="position:absolute; left:1000px; top:4px; width:128px; height:80px; z-index:4"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>
</body>
</html>