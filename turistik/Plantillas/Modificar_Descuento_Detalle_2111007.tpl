<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:19px;
	top:149px;
	width:501px;
	height:102px;
	z-index:13;
}
#Layer3 {
	position:absolute;
	left:18px;
	top:60px;
	width:372px;
	height:62px;
	z-index:0;
}
#Layer4 {
	position:absolute;
	left:17px;
	top:400px;
	width:296px;
	height:95px;
	{VISIBLE_FORM}
	
}
#Layer5 {
	position:absolute;
	left:17px;
	top:350px;
	width:296px;
	height:95px;
	z-index:14;
}
-->
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Ingreso de Descuento</title>
<style type="text/css">
<!--
#Layer2 {	position:absolute;
	left:831px;
	top:51px;
	width:126px;
	height:116px;
	z-index:12;
}
-->
</style>
<script language='JavaScript'  src='../utils.js'></script>
<script type="text/JavaScript">
function Limpiar()
{
document.form1.descuento.value='';
document.form1.descripcion.value='';
document.form1.proveedor.value='';
document.form1.precio.value='';
document.form1.cantidad.value='';
document.form1.tope.value='';
document.form1.porcentaje.value='';
document.form1.estado.value='';
}

function pres_num()
{
	if(event.keyCode!=44)
	{
		if((event.keyCode<48) || (event.keyCode>57))
		{
				event.returnValue = false;
			}		
	}
}	

function Popup()
{
 miPopup = window.open("../PHP/Seleccionar_Proveedor.php","miwin","top=200,left=350,width=400,height=230,scrollbars=yes,titlebar=no,location=no");
 miPopup.focus();	
}
function PopupEstado()
{
 miPopup = window.open("../PHP/Seleccionar_Estado.php","miwin","top=200,left=350,width=400,height=230,scrollbars=yes,titlebar=no,location=no");
 miPopup.focus();	
}

function Ver()
{
if(document.form1.descripcion.value=='')
{
	alert('Ingrese la descripción del descuento');
	document.form1.descripcion.focus();
	return;
}
if(document.form1.proveedor.value=='')
{
	alert('Selecciones el Proveedor');
	document.form1.proveedor.focus();
	return;
}
if(document.form1.descripcion.value=='')
{
	alert('Ingrese la descripción del descuento');
	document.form1.descripcion.focus();
	return;
}
if(document.form1.precio.value=='')
{
	alert('Ingrese el precio del descuento');
	document.form1.precio.focus();
	return;
}
if(document.form1.cantidad.value=='')
{
	alert('Ingrese la cantidad de veces que se entrega el descuento');
	document.form1.cantidad.focus();
	return;
}
if(document.form1.tope.value=='')
{
	alert('Ingrese el tope del valor del descuento');
	document.form1.tope.focus();
	return;
}
if(document.form1.porcentaje.value=='')
{
	alert('Ingrese el procentaje de descuento');
	document.form1.porcentaje.focus();
	return;
}
document.form1.guardar.value='1';
document.form1.submit();
opener.document.form1.submit();
}
</script>
</head>

<body>

<div id="Layer1">
  <form id="form1" name="form1" method="post" action="">
    <table width="496" border="0" cellpadding="0" cellspacing="0">
      
      <tr>
        <td>&nbsp;</td>
        <td><label></label></td>
      </tr>
      <tr>
        <td class="celdalazul">Descuento</td>
        <td class="celdalCELEST"><label>
          <select name="descuento" class="seleccionTurist_250" id="descuento" onchange="submit();">
            <option value="0x">-Seleccionar-</option>
            <!--START BLOCK : descuento -->
            <option value="{CODIGO_DESC}" {SELECCIONAR}>{DESCRIPCION_DESC}</option>
            <!--END BLOCK : descuento -->
          </select>
        </label></td>
      </tr>
      <tr>
        <td width="181" class="celdalazul">Descripci&oacute;n del Descuento </td>
        <td width="264" class="celdalCELEST"><input name="descripcion" type="text" class="seleccionTurist_250" id="descripcion" value="{DESCRIPCION_DESC}" maxlength="39" />
        <input name="codigo" type="hidden" id="codigo" value="{CODIGO_DESC}" /></td>
      </tr>
      <tr>
        <td class="celdalazul">Proveedor</td>
        <td class="celdalCELEST"><label>
          <input name="proveedor" type="text" class="seleccionTurist_180" id="proveedor" value="{NOMBRE}" />
          <img src="../Imagenes/modify.gif" width="16" height="16" onclick="Popup()" />
          <input name="codigo_prov" type="hidden" id="codigo_prov" value="{CODIGO_PROV}" />
        </label></td>
      </tr>
      <tr>
        <td class="celdalazul">Precio</td>
        <td class="celdalCELEST"><label>
          <input name="precio" type="text" class="seleccionTurist_180" id="precio" onchange="this.value=FormatNumber(this.value,4,true,false,true);" onKeyPress="pres_num()" value="{PRECIO_DESC}" maxlength="11" />
        </label></td>
      </tr>
      <tr>
        <td class="celdalazul">Cantidad</td>
        <td class="celdalCELEST"><label>
          <input name="cantidad" type="text" class="seleccionTurist_80" id="cantidad" onkeypress="pres_num()" value="{CANTIDAD_DESC}" maxlength="11" />
        </label></td>
      </tr>
      <tr>
        <td class="celdalazul">Tope del Descuento </td>
        <td class="celdalCELEST"><input name="tope" type="text" class="seleccionTurist_80" id="tope" onkeypress="pres_num()" value="{TOPE_DESC}" maxlength="11" /></td>
      </tr>
      <tr>
        <td class="celdalazul">Porcentaje del Descuento </td>
        <td class="celdalCELEST"><input name="porcentaje" type="text" class="seleccionTurist_80" id="porcentaje" onkeypress="pres_num()" value="{PORCENTAJE_DESC}" maxlength="11" />
          <label></label></td>
      </tr>
      <tr>
        <td class="celdalazul">Estado</td>
        <td class="celdalCELEST"><label>
          <input name="estado" type="text" class="seleccionTurist_180" id="estado" value="{ESTADO}" />
          <input name="codigo_estado" type="hidden" class="seleccionTurist_180" id="codigo_estado" value="{ESTADO_DESC}" />
          <img src="../Imagenes/modify.gif" width="16" height="16" onclick="PopupEstado()" /></label></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><span class="celdalCELEST">
          <input name="guardar" type="hidden" id="guardar" />
        </span></td>
      </tr>
      
      
      <tr>
        <td><input name="Submit2" type="button" class="BotonTurist_Celest" value="Aceptar" onclick="Ver()" />
        &nbsp;
        <label>
        <input name="Submit" type="submit" class="BotonTurist_Celest" onclick="Limpiar()" value="Cancelar"/>
        </label></td>
        <td><label></label></td>
      </tr>
    </table>
  </form>
</div>



<div class="TituloPlomo" id="Layer3">
  <table width="345" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="82" rowspan="4"><label><img src="../Imagenes/descuento.jpg" width="81" height="80" /></label></td>
      <td width="13">&nbsp;</td>
      <td width="250">&nbsp;</td>
    </tr>
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloPlomo"><strong>Modificar Descuento </strong></td>
    </tr>
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">{fecha}</td>
    </tr>
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
  </table>
</div>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>
</body>
</html>
