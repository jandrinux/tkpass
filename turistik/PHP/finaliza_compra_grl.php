<?
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
session_start();
date_default_timezone_set("America/Santiago");
if ($_SESSION["autentificado"] != "SI" )
{
    header("location: Login.php");
}
require ("set_timeout.php");
require ("../Clases/ClaseConexion.inc.php");
require ("../Clases/ClaseUtil.inc.php");
include ("../functions/tkpass.php");
include ("../functions/check_descuentos.php");
include ("../functions/consulta_descuento.php");
include ("../Clases/ClaseFinCompra.php");
require ("../Clases/class.boletas_manager.php");
include ("libmail.php");

$Obj_Tkpass = new tkpass(); 

$tipoReserva = $_POST['tipoReserva'];
$fop = $_POST['fop'];
$fop_manager = $_POST['fop'];
$boletaTTS = $_POST['boletaTTS'];
$boletaTTK = '';
$voucherTBK = $_POST['voucherTBK'];
$codSocio = ''; 
$tipo_tarjeta = $_POST['tipo_tarjeta'];
$ultimos_digitos = $_POST['ultimos_digitos'];
$total_tarjeta = $_POST['total_t'];
$total_efectivo = $_POST['total_e'];
$redondea = false;
$fecha_uso_boleta = date('Y-m-d H:m:i');
$estado_boleta = $_POST['estado'];
$dscto_coordinador = $_POST['dscto_cordinador'];
$tipo_opcion = $_POST['radio']; //combo = 2 // ninguno = 4 // 

$tot_exento = $_POST['tot_exento'];

$tot_exento =  str_replace("$", "", $tot_exento);
$tot_exento =  str_replace(",", "", $tot_exento);


$promocion_act = $_POST['id_promo']; // id de la promocion o combo
$combo_promocion = $_POST['id_combo']; // valor 1 es combo valor 0 es promocion



if ($combo_promocion == 1)
{
    $promo_adt = unserialize($_POST['promo_adt']);
    $promo_chd = unserialize($_POST['promo_chd']);
    
    $total_combo_adt = $_POST['total_combo_adt'];
    $total_combo_chd = $_POST['total_combo_chd'];
    
    $arreglo_promocion = unserialize($_POST['lista_promo']); // arreglo de promocion o combo  
    $excursiones_dscto = $arreglo_promocion[$promocion_act]; // excursiones a aplicar combo y descto
    $num_excursiones_arreglo = count($arreglo_promocion[$promocion_act]); // numerpo de excursiones
    
    $cantidad_promociones = unserialize($_POST['cant_promo']); 
    $cant_promo_adt = $cantidad_promociones[$promocion_act]['adt']; // cantidad de promociones para adt
    $cant_promo_chd = $cantidad_promociones[$promocion_act]['chd']; // cantidad de promociones para chd
    
    $precios_totales = unserialize($_POST['precios_totales']); 
    $precio_total_adt = $precios_totales[$promocion_act]['suma_adt'];
    $precio_total_chd = $precios_totales[$promocion_act]['suma_chd'];

    $comision_promocion = $_POST['comision_promocion'];
    
    $dscto_unitario_combo_adt =  round(($precio_total_adt - $total_combo_adt)/$num_excursiones_arreglo);
    $dscto_unitario_combo_chd =  round(($precio_total_chd - $total_combo_chd)/$num_excursiones_arreglo);
  
}

if ($fop != "W")
{
    
    $FinCompra = new FinCompra($_SESSION['codigo_suc'], $tipoReserva, $codSocio, $fop);
    
    $cod_usuario = $FinCompra->cod_usuario;
    
    $suc_reserva = $FinCompra->suc_reserva;
    $suc_venta = $FinCompra->suc_venta;
    $fop = $FinCompra->fop;
    $cod_cierre = $FinCompra->cod_cierre;
    $login_usua_cierre = $FinCompra->login_cierre;
    $tipoReserva = $FinCompra->tipoReserva;
    $codigo_distribuidor = $FinCompra->codigo_distribuidor;
    

    include('aplica_descuentos_grl.php');
      

    $lista_v = explode("/",$voucher_generado);
    

    include("respuesta_compra.php");  ## webservice de brazaletes   
    


    if ($dscto_coordinador > 0)
    {
         $Obj_Tkpass->Ingresa_Boletas_grl($boletaTTS,0);
        
        if ($estado_boleta == 1)
        {
        
            $Obj_Tkpass->Actualiza_Estado_Boletas_grl($boletaTTS, $fecha_uso_boleta, $estado_boleta, 'E');   
       
        }
        if ($estado_boleta == 2)
        {
            
            $Obj_Tkpass->Actualiza_Estado_Boletas_grl($boletaTTS, $fecha_uso_boleta, 1, 'E'); 
            
            for ($k=$_POST['boletaTTSCorrelativa']; $k<$boletaTTS; $k++)
            {
                $Obj_Tkpass->Actualiza_Estado_Boletas_grl($k, $fecha_uso_boleta, 2, 'E'); 
            }
                  
        }       
    }

      
    if ($redondea == true)
    {
        $redondeo = $total_tarjeta - $total_efectivo;
        $resultado = ($redondeo / $cantidad_adt);
        $valor_actualizar = (integer)($valor_adt - $resultado);

        $miConexion->Conectar();
        $query=$miConexion->EjecutaSP("Actualiza_Precio_Re"," 2, '".$lista_v[0]."', ".$valor_actualizar." ");
    }
    

  
header("location:boarding_pass.php?v=$voucher_generado");

//echo '<script type="text/javascript">window.location.href="boarding_pass.php?v='.$voucher_generado.'";</script>'; 
 }    
?>