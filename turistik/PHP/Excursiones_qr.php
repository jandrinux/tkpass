<?php
include("menus.php");

require ("../functions/tkpass.php");
$_Util=new Util;
$fecha = date('d/m/Y');
$c=$_GET['c'];
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<title>Excursiones QR</title>

 <meta charset="utf-8" />
    <link rel="stylesheet" href="../../css/plantilla_matriz.css" type="text/css" media="all">
    <link href="../estilos1.css" rel="stylesheet" type="text/css">
    <link href="../../css/noticias.css" rel="stylesheet" type="text/css" />
    <link type='text/css' href='../../css/basic_simplemodal_qr.css' rel='stylesheet' media='screen' /><!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>

    <script type='text/javascript' src='../../js/jquery.simplemodal.js'></script>
    <script type='text/javascript' src='../../js/basic_simplemodal.js'></script>

    <script type="text/javascript">
  
    function refresh()
    {
        location.reload(true);
    }

    
    function cerrar()
    {
        window.close();
    }

    var auto_refresh = setInterval( function (){
        $('#ReloadThis').load('estado_carrito.php?a=1');
    }, 2000); 

    var auto_refresh2 = setInterval( function (){
        $('#principal').load('matriz_excursiones_qr.php?c='+<?=$c?>);
    }, 30000); 

        
    function Tomar(dia, fecha, exc, acc)
    {
        var boarding = $("#boarding").val();

        if (acc == 1 && boarding != "")
        {

            $("#wpc").click();

            $.post("inserta_venta_qr.php",{ df:dia, ex:exc, v:boarding },function(data){

                if(data == 1)
                {
                    $(".modalCloseImg").click();
                    window.open("ver_carrito_qr",'_blank');
                }
                if(data == 2)
                {
                    alert("Boarding Pass Invalido")
                    $(".modalCloseImg").click();
                }
                if(data == 3)
                {
                    alert("sin respuesta")
                    $(".modalCloseImg").click();
                }
            });
            
        }

    }
    </script>

    
 
    
</head>
<body>
 <div id="global">

 <div id='basic-modal'>
  <input type='button' name='wpc' id="wpc" class='basic' style="display: none;"/>
</div>
 
 <div id="ReloadThis" style="margin: 20 0 0 520px; position: absolute;"><? include ('estado_carrito.php?a=1') ?></div>

                
 <div id="cabecera">

      <table width="482" border="0" cellpadding="0" cellspacing="0">  
    	<tr>
          <td width="80" rowspan="4"><img src="../Imagenes/IMG_6902.jpg" width="80" height="80" /></td>
          <td width="13">&nbsp;</td>
          <td width="389">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><strong id="TituloPlomo"><b>Consulta de Excursiones QR </b></strong></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
        </tr>    
    
      </table>

 </div>
 

  
 <div id="navegacion" style="display: none">
   

 </div><!--cierre navegacion -->
 
 
 
 <div id="Layer4" style="position:absolute; left:835px; top:52px; width:128px; height:80px; z-index:4">
    <img src="../Imagenes/turistik107_118.jpg" width="107" height="118" />

    
 </div>
 

  <div id="principal">
    <?
        include ("matriz_excursiones_qr.php");
    ?>
  </div>
 
 <div class="anuncios1">

    <div id="titulo_noticia" style="height:30px; background: #D70000;"><div style="float: left; color: #FFF; padding: 5px 0 0 5px;"><b>Turistik Informa </b></div></div>
    
    <div>
        <div id="cont_6d8b98575023d062d911ccdca935eef3">
            <span id="h_6d8b98575023d062d911ccdca935eef3">
            <script type="text/javascript" src="http://www.meteored.cl/wid_loader/6d8b98575023d062d911ccdca935eef3"></script>
        </div>
    </div>

    <ul id="widget">
    
        <?
        $ObjConsultaCredito = new tkpass(); 
        $TipoDistribuidor = $ObjConsultaCredito->Consulta_tipo_distribuidor_original($_SESSION["codigo_dist"]);
       
        if ($TipoDistribuidor == 1 || $TipoDistribuidor == 2 || $TipoDistribuidor == 3)
        {
            

            if ($_SESSION["cod_tusua"] == 4)
            {
                $ObjConsultaCredito = new tkpass(); 
                $Linea = $ObjConsultaCredito->Consulta_linea_credito($_SESSION["codigo_usu"]);
                
                $ObjConsultaSaldoPrepago = new tkpass(); 
                $linea_prepago = $ObjConsultaSaldoPrepago->Consulta_Saldo_linea_prepago($_SESSION["codigo_usu"]);
                $dip_lc = $Linea[0] - $Linea[1];
                ECHO  	"<li>";
                // linea    <   transaccion
    
                if ($Linea[0] <= $Linea[1] )
                {
                    
                    if ($TipoDistribuidor != 2)
        			{ 
                	 echo "<div class='titulo'>SALDO INSUFICIENTE</div>";               
                	 echo '	<h4><strong style="color:#D70000">TODA RESERVA SERA CON ABONO Y EL SALDO A PAGAR EN OFICINA DE VENTAS TURISTIK</h4>';
                     echo "<br>";

                     echo '<h4><strong style="color:#D70000">DISPONIBLE PARA RESERVAS : <div style="position:right;float:right; margin-right:10px">$ 0</div></strong></h4>';
					}
                     echo '<h4><strong style="color:#D70000">SU DEUDA PENDIENTE ES : <div style="position:right;float:right; margin-right:10px">$ '.number_format($Linea[1], 0, ",", ".").'</div></strong></h4>';		
                }
                else
                {
                    if ($TipoDistribuidor == 3)
        			{              
                	 echo '	<h4><strong style="color:#D70000">TODA RESERVA SERA CON ABONO Y EL SALDO A PAGAR EN OFICINA DE VENTAS TURISTIK</h4>';
                     echo "<br>";

                     echo '<h4><strong style="color:#D70000">DISPONIBLE PARA RESERVAS : <div style="position:right;float:right; margin-right:10px">$ 0</div></strong></h4>';
					}
                    
                    
                    if ($TipoDistribuidor == 2)
        			{
                	 echo "<div class='titulo'>INFORMACION DE CUENTA</div>";               
                     echo "<br>";
                     echo '<h4><strong style="color:#D70000">DISPONIBLE PARA RESERVAS : <div style="position:right;float:right; margin-right:10px">$ '.number_format($dip_lc, 0, ",", ".").'</div></strong></h4>';
                    }
                   if ($Linea[1] > 0)
                   {
                    echo '<h4><strong style="color:#D70000">SU DEUDA PENDIENTE ES : <div style="position:right;float:right; margin-right:10px">$ '.number_format($Linea[1], 0, ",", ".").'</div></strong></h4>';		
                   }
                     
                }
                
                if ($linea_prepago > 0)
                {                
                     echo "<br>";
                     echo '<h4><strong style="color:#D70000">DISPONIBLE PREPAGO : <div style="position:right;float:right; margin-right:10px">$ '.number_format($linea_prepago, 0, ",", ".").'</div></strong></h4>';
                     
                      $val01x = $dip_lc + $linea_prepago;
                
                
                        if( $val01x>0)
                        {
                             echo '<h4><strong style="color:#D70000">TOTAL DISPONIBLE PARA RESERVAS: <div style="position:right;float:right; margin-right:10px">$ '.number_format($dip_lc+$linea_prepago, 0, ",", ".").'</div></strong></h4>';		
                        }
                }
               
                
                echo '</li>';
            }
            
        } 

            $objStock = new ClaseConexion;
            $objStock->Conectar();
            $queryStock=$objStock->EjecutaConsulta("select * from noticias where activa=1 and Fecha > '".date("Y/m/d")."'  order by idnoticias desc");        
        	while($row_listar_stock=mysql_fetch_array($queryStock))
        	{
             	     if($row_listar_stock['Externos']==1)
                     {
                            if($_SESSION['cod_tusua']==4)
                            {
                                 ECHO  	"<li>";
                            	 echo "<div class='titulo'>".$row_listar_stock['titulo']. "</div>";
                               
                            	 echo '	<h4><strong style="color:#D70000">'.substr($row_listar_stock["fecha"],11)." |</strong>  ". $row_listar_stock["contenido"].'</h4>';
                            			
                            	 echo '	</li>';
                            } 
                     }
                     else
                     {
                        if($row_listar_stock['Turistik']==1)
                        {
                                 ECHO  	"<li>";
                                 echo "<div class='titulo'>".$row_listar_stock['titulo']. "</div>";
                             	 echo '	<h4><strong style="color:#D70000">'.substr($row_listar_stock["fecha"],11)." |</strong>  ". $row_listar_stock["contenido"].'</h4>';
                            		
                            	 echo '	</li>';
                        }
                        
                     }
                     
                        if($row_listar_stock['Turistik']==1 && $row_listar_stock['Externos']==1)
                        {
                                 ECHO  	"<li>";
                                 echo "<div class='titulo'>".$row_listar_stock['titulo']. "</div>";
                             	 echo '	<h4><strong style="color:#D70000">'.substr($row_listar_stock["fecha"],11)." |</strong>  ". $row_listar_stock["contenido"].'</h4>';
                            		
                            	 echo '	</li>';
                        }
             
               
        	}
        	mysql_free_result($queryStock); 
        	mysql_close();
            ?>
    
    </ul>

 </div> <!--cierre anuncios -->
 

 <div id="pie">
    <!--Pie-->
 </div>

 <input type="hidden" value="<?=base64_encode($_GET['v'])?>" id="boarding" />
 
 </div>
</body>
</html>
