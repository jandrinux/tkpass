<?
class DBCompra extends DBNucleo {
	function DBCompra() {$this->conecta();}

	function add($tmp){
		$codprod	=$tmp->getCodProd();
		$idsession	=$tmp->getIdSession();
		$valor		=$tmp->getValorUnitario();
		$valor = str_replace(",",".",$valor);
		$valor=($valor)?$valor:0;
		$moneda		=$tmp->getMoneda();
		$fecha		=$tmp->getFecha();
		$cantidad	=$tmp->getCantidad();$cantidad=($cantidad)?$cantidad:1;
		$nombre_comprador=$tmp->getNombreComprador();
		$direccion	=$tmp->getDireccion();
		$email		=$tmp->getEmail();
		$cod_autoriza	=$tmp->getCodAutoriza();
		$orden_compra	=$tmp->getOrdenCompra();
		$monto		=$tmp->getMonto();
		$monto = str_replace(",",".",$monto);

		$sql="insert Operacion(cod_prod,id_session,valor_unitario,moneda,fecha,cantidad,nombre_comprador,direccion,email,cod_session,cod_autoriza,orden_compra,monto) values('$codprod','$idsession',$valor,$moneda,'$fecha',$cantidad,'$nombre_comprador','$direccion','$email','$cod_session','$cod_autoriza','$orden_compra',$monto)";
		echo "<p>SQL: $sql</p>";
		$rs=$this->query($sql);
		if($rs) return "OK"; else return "PROBLEMAS";
	}
	function del(){}
	function update(){}
	function get(){}
	function EsHabil($fecha,$tipo_feriado=""){
		$tipoDia = $this->getTipoDia($fecha);
		if ($tipoDia!=6 && $tipoDia!=7){
			if ($tipo_feriado==2){
				if ($this->getFeriado($fecha,1)==0) return true;
				else return false;
			} 
			if ($this->getFeriado($fecha,1)==0) return true;
			else return false;
		} else return false;
	}
	function getTipoDia($fecha){
		return strftime("%u",strtotime($fecha));
	}
	function getDiffDate($fecha1,$fecha2){
		$sql = "select datediff('$fecha1','$fecha2')";
		$rs = $this->query($sql);
		if ($rs){
			$row = $this->getRows($rs);
			return $row[0];
		}
	}
	function getAddFecha($fecha,$dias){
		$sql = "select date_add('$fecha',interval $dias day)";
		//echo "<p>SQL: $sql </p>";
		$rs=$this->query($sql);
		if ($rs){
			$row = $this->getRows($rs);
			return $row[0];
		}		
	}

	function getSubFecha($fecha,$dias){
		$sql = "select date_sub('$fecha',interval $dias day)";
		//echo "<p>SQL: $sql </p>";
		$rs=$this->query($sql);
		if ($rs){
			$row = $this->getRows($rs);
			return $row[0];
		}		
	}

	function diasHabiles($fecha){
		$hoy = date('Y-m-d');
		$obs = $this->getAddFecha($fecha,1);
	
		$fecha_next = $this->diaHabilSiguiente($obs);
		//echo "<p>Fecha Hoy:$hoy Fecha Next:$fecha_next</p>";
		$dias = $this->getDiffDate($fecha_next,$hoy);
		//echo "<p>Dias:$dias</p>";
		return  $dias;
	}
	function diaHabilSiguiente($fecha){//Fixing CHILE
		$next = $this->getAddFecha($fecha,1);
		$tipoDia = $this->getTipoDia($next);

		$feriado_nac = $this->getFeriado($next,1);
			//echo "<p>FN:$feriado_nac FU:$feriado_usa</p>";
		//while ($feriado_nac>0||$feriado_usa>0||$tipoDia==6 || $tipoDia==7){
		while ($feriado_nac>0||$tipoDia==6 || $tipoDia==7){
			switch($tipoDia){
			case 6://Sabado
				$next = $this->getAddFecha($next,2);
				break;
			case 7://Domingo
				$next = $this->getAddFecha($next,1);
				break;
			default:
				$next = $this->getAddFecha($next,1);
				break;
			}
			//echo "<p>NEXT:$next</p>";
			$tipoDia = $this->getTipoDia($next);
			$feriado_nac = $this->getFeriado($next,1);
			}
		
		return $next;
	}
	function diaHabilSiguiente2($fecha){//Tomando en cuenta Feriados USA
		$next = $this->getAddFecha($fecha,1);
		$tipoDia = $this->getTipoDia($next);

		$feriado_nac = $this->getFeriado($next,1);
		$feriado_usa = $this->getFeriado($next,2);
		//echo "<p>FN:$feriado_nac FU:$feriado_usa</p>";
		//while ($feriado_nac>0||$feriado_usa>0||$tipoDia==6 || $tipoDia==7){
		while ($feriado_nac>0||$feriado_usa>0 || $tipoDia==6 || $tipoDia==7){
			switch($tipoDia){
			case 6://Sabado
				$next = $this->getAddFecha($next,2);
				break;
			case 7://Domingo
				$next = $this->getAddFecha($next,1);
				break;
			default:
				$next = $this->getAddFecha($next,1);
				break;
			}
			//echo "<p>NEXT:$next</p>";
			$tipoDia = $this->getTipoDia($next);
			$feriado_nac = $this->getFeriado($next,1);
			$feriado_usa = $this->getFeriado($next,2);
		}
		
		return $next;
	}
	function diaHabilAnterior($fecha){
		$next = $this->getSubFecha($fecha,1);
		$tipoDia = $this->getTipoDia($next);

		while (($tipoDia==6 || $tipoDia==7)||$this->getFeriado($next,1)||$this->getFeriado($next,2)){
			switch($tipoDia){
			case 6://Sabado
				$next = $this->getSubFecha($next,1);
				break;
			case 7://Domingo
				$next = $this->getSubFecha($next,2);
				break;
			default:
				$next = $this->getSubFecha($next,1);
				break;
			}
			
			$tipoDia = $this->getTipoDia($next);
		}
		return $next;
	}
}
?>