<?
	include_once("class/config.php");
	session_start();
	ob_start();

	$session_id = $_SESSION['ID'];
	
	$Aux = new DBCompra();
	$filename = "./cgi-bin/log/temporal.txt";
	if (file_exists($filename)) {
		$fp = fopen($filename, "r");
		$linea = fread($fp, filesize($filename));
		fclose($fp);
	}		
?>
<style>
.celda_bordes {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	line-height: 13px;
	color: #000000;
	background-color: #FFFFFF;
	background-repeat: repeat-x;
	border: 1px solid #c3c3c3;
}
</style>
<br>
<table align="center" bgcolor="#c3c3c3" width="90%"><tr><td class="celda_bordes">
<table align="center" bgcolor="#FFFFFF">
	<tr><td align="center"><img src="http://www.tur.pointpay.cl/images/logo_turistik.jpg" border="0"></td></tr>
	<tr><td align="center"><b>VOUCHER DE SERVICIO</b></td></tr>
	<tr><td align="center">Cod. Autorizaci&oacute;n <?=$linea?></td></tr>
<?

  //echo "ID SESSION: " . $TBK_ID_SESION. "\n<br>";
  //echo "CODIGO DE AUTORIZACION: " . $linea. "\n<br>";
  //echo "ORDEN DE COMPRA       : " . $TBK_ORDEN_COMPRA . "\n<br>";
	$sql = "select * from Operacion where id_session='$TBK_ID_SESION' ";
	$rs = $Aux->query($sql);
	if ($rs){
		$row=$Aux->getRows($rs);
		$nombre_comprador 	= $row['nombre_comprador'];
		$email 			= $row['email'];
		$cantidad 		= $row['cantidad'];
		$cod_prod		= $row['cod_prod'];
		$valoru 		= $row['valor_unitario'];
		$monto 			= $row['monto'];$st_monto=number_format($monto,0,',','.');
		$cod_auto 		= $row['cod_autoriza'];
		$direccion 		= $row['direccion'];
		$fecha 			= $row['fecha'];
		
		$sql = "update Operacion set estado=1,cod_autoriza='$linea' where id_session='$TBK_ID_SESION' ";
		$rs = $Aux->query($sql);
		$elproducto = $Aux->ElDato($cod_prod,"ProductoTur","CODIGO_PROD","NOMBRE_PROD");
		$ladescripcion = $Aux->ElDato($cod_prod,"ProductoTur","CODIGO_PROD","DESCRIPCION_PROD");
		setlocale(LC_ALL,"es_ES");
?>
<tr><td><h3>Estimado(a) <?=strtoupper($nombre_comprador)?></h3></td></tr>
<tr><td align="center">
	<table>
		<tr><td>
			<p align="justify">
Agradecemos t&uacute; compra de <?=$cantidad?> ticket(s) <b>"<?=$elproducto?>"</b><br /> 
El valor total de tu compra es de <b><?=$st_monto?></b> pesos chilenos.<br />
El servicio ser&aacute; utilizado el <b><?=$Aux->FechaFmt($fecha,1);//strftime("%A %d ,%B %Y ",strtotime($fecha))?></b>.<br />
Tu direcci&oacute;n/ hotel el d&iacute;a que har&aacute;s uso del servicio es <?=$direccion?>.<br>
Si requerimos contactarte lo haremos a <b><u><?=$email?></u></b><br />
			</p>
<br /><br /></table>
</td></tr>
<tr><td><table>
	<tr><td colspan="2"><b><u><?=$elproducto?></u></b></td></tr>
	<tr><td width="50"></td><td><p align="justify"><?=nl2br($ladescripcion)?></p></td></tr>
	</table></td></tr>
<tr><td><p><b>INFORMACIONES: (56-2) 220 10 00</b></p></td></tr>
<tr><td><b>*** Para hacer uso del servicio deberas llevar una copia de esta confirmaci&oacute;n ***</b></td></tr>
<tr><td><center><input type="button" value="Imprimir" onClick="window.print();document.location='/index.htm';"></center></td></tr>

<tr><td>Para hacer uso del servicio deber&aacute;s llevar una copia de esta confirmaci&oacute;n.<br><br></td></tr>
<tr><td><h2>Gracias por su compra</h2></td></tr>

<?}?>

</td></tr></table>
</td></tr></table>

<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23249595-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>

<?
		$outhtml = ob_get_contents();
		ob_end_clean();
		//var_dump($outhtml);
		
		$para = $email;
		$from ="ventas@turistik.cl";
		$asunto = "Transaccion Exitosa";

		$mail = new htmlMimeMail5();
		$mail->setFrom($from);
		$mail->setSubject($asunto);
		$ma