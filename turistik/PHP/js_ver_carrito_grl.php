<script type="text/javascript">
function carrito()
{
    window.close();
}

$(document).ready(function(){
	$("#tipoReserva").change(function(){ 
		$.post("CARGA_dinamica.php?a=7",{ id:$(this).val() },function(data){$("#select_fop").html(data);})
        info_boleta_tr();
	});
})

function combos(afecto,exento,combo,afecto_combo,exento_combo,id_promo, total_sin_combo, comision, total_combo_adt, total_combo_chd)
{
    

    $('#id_combo').val(combo);
    $('#id_promo').val(id_promo);
    
    if(combo == 1) 
    {
   
        $('#resumen_promocion').hide();
        $('#resumen_combo').show();
        
        var af = $('#valor_afecto').val();
        var ex = $('#valor_exento').val();
        var subtotal = $('#subtotal').val();
        
              
        if (total_sin_combo == 0)
        {
          val_afecto = afecto_combo;
          val_exento = exento_combo;      
          total_tarjeta = val_afecto + val_exento;        
        }
        else
        {
          val_afecto = parseInt(afecto) + parseInt(afecto_combo);
          val_exento = parseInt(exento)+parseInt(exento_combo); 
          total_tarjeta = total_sin_combo + afecto_combo + exento_combo;         
        }

        
         $('#comision_promocion').val(comision);
         $('#total_combo_adt').val(total_combo_adt);
         $('#total_combo_chd').val(total_combo_chd);
        
        dscto_combo = subtotal - total_tarjeta;
        
      
        dscto_combo = formatNumber(dscto_combo,'');
        $('#dscto_combo').val('$ '+dscto_combo);
        
        
        t_tarjeta = formatNumber(total_tarjeta,'');
        $('#tot_tarjeta').val('$ '+t_tarjeta);
        
        t_efectivo = formatNumber(total_tarjeta,'');
        $('#tot_efectivo').val(t_efectivo);
        
        tot_afecto = formatNumber(val_afecto,'');
        $('#total_afecto').val('$ '+val_afecto);
        
        tot_exento = formatNumber(val_exento,'');
        $('#total_exento').val('$ '+val_exento);
        
        
    }
    else
    {
        var af2 = $('#valor_afecto_normal').val();
        var ex2 = $('#valor_exento_normal').val();
        t_efectivo = parseInt(af2)+parseInt(ex2);
        
        t_efectivo = formatNumber(t_efectivo,'');
        $('#tot_efectivo').val('$ '+t_efectivo);

        $('#resumen_promocion').show();
        $('#resumen_combo').hide();
    }
        
}


function info_boleta_tr()
{

  var select = document.getElementById("select_fop").value;
  var radio = 4;
  var selecTipo = document.getElementById("tipoReserva").value;


  if(select == "C" || select == "TC")
  {

    document.getElementById('total_efectivo').style.display = 'none';
    document.getElementById('total_tarjeta').style.display = '';
    
    document.getElementById("tituloTBK").style.visibility='visible';
    document.getElementById("info_boleta").style.visibility='visible';
    document.getElementById("tr_tarjeta").style.display='';

  }
  else
  {
    
        if(select == "D" )
        {
            document.getElementById("tituloTBK").style.visibility='visible';
            document.getElementById("info_boleta").style.visibility='visible';
            document.getElementById("tr_tarjeta").style.display='none';

        }
        else
        {
            document.getElementById("tr_tarjeta").style.display='none';
            document.getElementById('total_efectivo').style.display = '';
            document.getElementById('total_tarjeta').style.display = 'none';
            document.getElementById("tituloTBK").style.visibility='hidden';
            document.getElementById("info_boleta").style.visibility='hidden';
        }
 
  } 

}


function recalcular_porcentaje(dscto_cordinador)
{
    var dscto_cordinador = dscto_cordinador.value;
    var select = document.getElementById("select_fop").value;
    
    if (dscto_cordinador != 0)
    {
        var tot_efectivo = ef;
        var tot_tarjeta  = ta;
        var tot_exento = ex;
        var tot_afecto = af;
    }
    else
    {
        
        var tot_efectivo = $('#tot_efectivo').val();
        var tot_tarjeta = $('#tot_tarjeta').val();
        var tot_exento = $('#tot_exento').val();
        var tot_afecto = $('#tot_afecto').val();
    }
    
    
    tot_efectivo = tot_efectivo.replace("$","").replace(',','');
    tot_tarjeta = tot_tarjeta.replace("$","").replace(',','');
    tot_exento = tot_exento.replace("$","").replace(',','');
    tot_afecto = tot_afecto.replace("$","").replace(',','');
    

    tot_e = (parseInt(dscto_cordinador)*parseInt(tot_efectivo))/100;
    tot_t = (parseInt(dscto_cordinador)*parseInt(tot_tarjeta))/100;
    tot_ex = parseInt(tot_e) - parseInt(tot_afecto);
    
    if (tot_ex < 0)
    {
        tot_ex = 0;
    }
    

    efectivo = formatNumber(tot_e, '');
    tarjeta = formatNumber(tot_t, '');
    exento = formatNumber(tot_ex, '');
    
    
    $('#tot_efectivo').val("$ "+efectivo)
    $('#tot_tarjeta').val("$ "+tarjeta);
    $('#tot_exento').val("$ "+exento);
    
    if (dscto_cordinador == 0)
    {
        $('#boletaTTS').hide(); 
        $('#tipo_venta').val(3); 
    }
    else
    {
        $('#tipo_venta').val(4); 
        $('#boletaTTS').show(); 
    }
    
}

function RevisarBoletas(boletaTTS)
{
    if (boletaTTS == '')
    {
        alert("El Ingreso de Boleta TTS es obligatoria.");
        document.getElementById("boletaTTS").focus();
        return false;
    }

    if (boletaTTS != '')
    {
        if ($('#boletaTTSCorrelativa').val() != boletaTTS )
        {
            var answer= confirm("Boleta TTS incorrecta, desea saltar el correlativo?");
        
             if ( answer ) 
             {
                document.getElementById("estado").value = 2;
                
             }              
              else
             {
                document.getElementById("estado").value = 1;
                return false;
             }
        }
        else
        {
            document.getElementById("estado").value = 1;
        }

    }
}

function ExistenBoletas()
{
    if ($('#boletaTTSCorrelativa').val() == '')
    {
        alert("No existe Boleta TTS, recepcione un folio antes de realizar una venta");
        return false;
    }
    
    return true;
}


function valida_tarjetas()
{
    var tbk = document.getElementById('info_boleta').value;
    var ultimos_digitos = $('#ultimos_digitos').val();
    var tipo_tarjeta = $('#tipo_tarjeta').val();
    
    if(tbk == '')
    {
        alert("Ingrese Voucher Transbank.");
        document.getElementById('info_boleta').focus();
        return false;
    }
    
    if (ultimos_digitos.length < 4)
    {
        alert("Complete los 4 ultimos digitos de la Tarjeta Bancaria");
        document.getElementById('ultimos_digitos').focus();
        return false; 
    }
    
    
    if (tipo_tarjeta == '')
    {
        alert("Seleccione Tipo de Tarjeta");
        document.getElementById('tipo_tarjeta').focus();
        return false;
    }
    
    return true;
}

function finalizar()
{     
    var TipoReserva = document.getElementById("tipoReserva").value;
    var fop = document.getElementById("select_fop").value;
    var tipoUsuario = document.getElementById("tipoUsuario").value;
    var boletaTTS = document.getElementById("boletaTTS").value;   
    var tbk = document.getElementById('info_boleta').value;
    var ultimos_digitos = $('#ultimos_digitos').val();
    var tipo_tarjeta = $('#tipo_tarjeta').val();
    var radio = 4;
    var tipo = $('#tipo_venta').val();
    
    
    if (fop == "S")
    {
        alert("Seleccione Forma de Pago");
        document.getElementById("select_fop").focus();
        return false;
    }

   	$.post("consulta_excursiones.php",function(data)  //consulta si existe una excursion codigo 6, para boletas
    { 
         
       var vaciar= confirm("Esta seguro de finalizar la compra");
         if ( vaciar ) 
         {
         
            if (data != '')
            {
            
              if (data == 1 || data == 2)
              {  
                    
                    if (TipoReserva == 1 && tipo == 4)
                    {

                        if(RevisarBoletas(boletaTTS) == false)
                        {
                            return false;
                        }

                    }
               }

                    
               	$.post("consulta_boletas_registradas.php", {boleta_re:boletaTTS, boleta_ttk:'', tipo:tipo}, function(ResBol)  //consulta si existe boleta ingresada
                {  

                  if (ResBol == 'ok')
                  {       
                    
                        if (fop == 'C')
                        {
                          if (valida_tarjetas() == false)
                          {
                            return false;
                          }
                        }
                        
                        if (fop == 'D')
                        {
                            if(tbk == '')
                            {
                                alert("Ingrese Voucher Transbank.");
                                document.getElementById('info_boleta').focus();
                                return false;
                            }
                        }

                        document.formCompra.submit();
                        //alert('comprando');
                         
                   }
                   if (ResBol == 'no_tts')
                   {
                        alert("Boleta TTS existente, pruebe con una valida\n Los registros de boletas seran actualizados");
                        document.getElementById("boletaTTS").focus();
                        window.location.reload( true );
                        //return false;
                   }   
                   
                   if (ResBol == 'no_existe_tts')
                   {
                        alert("La Boleta TTS ingresada no se encuentra en nuestra base de datos.\n Pruebe con una valida");
                        document.getElementById("boletaTTS").focus();
                        
                        return false;
                   }
                   

                   if (ResBol == 'no_bols')
                   {
                        document.formCompra.submit();
                        //alert('comprando');
                   }

                 });
 
              }
              else// end  if (data != '')
              {
                 alert("No existen servicios para realizar una compra");
                 return false;
              }

          }
    });   
  
}

function vaciar_carrito(){
var vaciar= confirm("Esta seguro de vaciar el carrito?");
     if ( vaciar ) {
        location.href='vaciar_carrito.php?a=1';
     }
}


</script>