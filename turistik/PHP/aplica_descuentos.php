<?
include ("../Clases/class.bitacora.php");
$voucher_generado = '';
$suma_sub = 0;
$total_precio_chd  = 0;
$total_precio_adt = 0; 
$suma_total = 0;
$descuento_pax = 0;
$dscto_lugar_reserva = 0;
$monto_total = 0;
$sub = 0;
$precio_real = 0;
$suma_subtotal = 0;
$total_afecto = 0;
$cant_pax_adt =  0;
$cant_pax_chd = 0;
$resultado_dsctos = array();
$pasajeros_cant = array();
$voucher_servicio = array();



if ($tipo_opcion == 1)
{
    /// CONSULTA DESCUENTOS ACTIVOS
    for ($i=1; $i<=5; $i++)
    {
        $Obj_Dsctos_Activos = new descuentos(); 
        $resultado_dsctos[]=$Obj_Dsctos_Activos->Consulta($i);
    }
    $cant_min_dscto = $Obj_Dsctos_Activos->Consulta_Cantidad_min_excursiones();
}


$cant_pasajeros = 0;
$cant_exc = 0;

$miConexion = new ClaseConexion;
$miConexion->conectar();
$Query = $miConexion->EjecutaSP("Consulta_Pax_Carrito_by_Excursion", " ".$_SESSION['codigo_suc'].", ".$_SESSION["codigo_usu"].", 1 ");
            
$num_excursiones = mysql_num_rows($Query);
while($row=mysql_fetch_array($Query))
{
    	      
    $miConexion->conectar();
    $query=$miConexion->EjecutaSP("Generar_Voucher","@voucher"); 	
    $parametrosSalida_v=$miConexion->ObtenerResultadoSP("@voucher");
    $row_voucher= mysql_fetch_assoc($parametrosSalida_v);
    $voucher = $row_voucher['@voucher'];
    if ($row['COD_EXCURSION'] != 6 && $row['COD_EXCURSION'] != 54 && $row['COD_EXCURSION'] != 55)
    {
        $voucher_servicio[] = $voucher; 
    }
    
    $voucher_generado = $voucher."/".$voucher_generado;
    
    $codigo_excursion[] = $row['COD_EXCURSION'];
    $excursion_array[] = $row['COD_EXCURSION'];

    $fecha_array[] = $row['FECHA'];
    $excursion = $row['COD_EXCURSION']; // en el caso de que un distribuidor, finalize compra con la opcion de ninguno tipo_opcion 0
    $aplica_desc_cant_pax = $row['APLICA_DESC_CANT_PAX'];
    $aplica_desc_lugar_recogida = $row['APLICA_DESC_LUGAR_REC'];
    $aplica_desc_lugar_reserva= $row['APLICA_DESC_LUGAR_RES'];
    $aplica_desc_excursion = $row['APLICA_DESC_EXCURSION'];
    if ($aplica_desc_excursion == 1)
    {
        $cant_exc++;
    }

    $miConexion->conectar();
    $QuerySelect = $miConexion->EjecutaSP("Consulta_Temporal_Carrito", " ".$row['COD_EXCURSION'].", ".$_SESSION['codigo_suc'].", ".$_SESSION["codigo_usu"].", '".$row['FECHA']."' ");
    $cantidadPax[] = mysql_num_rows($QuerySelect);
    
     while($row_temp=mysql_fetch_array($QuerySelect))
     {
        
            $codigo_re = $row_temp['CODIGO_RE'];
            $valor_pax = $row_temp['PRECIO_RE'];
            
            if($row_temp['TIPO_PASAJERO'] == "ADT" )
            {
                $cant_pasajeros++;
                $cant_pax_adt++;
                $id_lugar = $row_temp['LUGAR_RE']; 
                $monto_total = $row_temp['PRECIO_RE'] + $monto_total;
                $sub = $row_temp['PRECIO_RE'] + $sub;
                $total_precio_adt = $row_temp['PRECIO_RE'] + $total_precio_adt;// en el caso que un distribuidor, finalize compra con la opcion de ninguno tipo_opcion 0
                $precio_real = $row_temp['PRECIO_EO'] + $precio_real;
                $redondea_codigo_re = $row_temp['CODIGO_RE'];
            }
            else
            {
                $cant_pax_chd++;
                $total_precio_chd = $row_temp['PRECIO_RE'] + $total_precio_chd;// en el caso que un distribuidor, finalize compra con la opcion de ninguno tipo_opcion 0
                $precio_real = $row_temp['VALOR_CHILD'] + $precio_real;
            }
            
            
            $suma_subtotal = $row_temp['PRECIO_RE'] + $suma_subtotal; 
            $total_afecto = $row_temp['VALOR_EXENTO'] + $total_afecto;
            $pasajero = $row_temp['PASAJERO'];
            $id_temporal = $row_temp['ID_TEMPORAL'];
            $array_temporal[] = $row_temp['ID_TEMPORAL'];

            
            
            $Obj_Tkpass->Actualiza_Reserva_Excursion(1, $id_temporal, $tipoReserva, $fop, $voucherTBK, $voucher, $_SESSION['usuario'], $codSocio, $cod_usuario, $cod_cierre, $suc_venta, $suc_reserva, $login_usua_cierre, $tipo_mayorista, $tipo_tarjeta, $ultimos_digitos);
               
            
            $b = new Bitacora();
            $b->FECHA = date("Y-m-d  h:i:s");
            $b->NUEVO = true;
            $b->USUARIO = $_SESSION['usuario'];
            $b->EVENTO ="TOMA RESERVA";
            $b->DESCRIPCION ="";
            $b->ARCHIVO ="aplica_descuentos.php";
            $b->CODIGO_RE = $codigo_re;
            $b->add();
        
            
     }
     mysql_free_result($QuerySelect);
     
     $pasajeros_cant[$excursion] = array("adt"=>$cant_pax_adt, "chd"=>$cant_pax_chd);


    $Obj_BuscaDescuentos = new BuscaDescuentos(); 
     
    if($tipo_opcion == 1 ) // descuentos
    {
        
          if ($resultado_dsctos[0] == 1  && $aplica_desc_cant_pax == 1)
          {
            
            $dscto_pax = $Obj_BuscaDescuentos->ConsultaDsctoPax($cant_pasajeros, $monto_total);
            $monto_total = $monto_total - $dscto_pax[0];
            // monto de descuento segun cant. pasajeros
            // descuento $dscto_pax[1] a aplicar en %; 
          }
                  
          if ($resultado_dsctos[2] == 3 && $aplica_desc_lugar_recogida == 1)
          {
                if ( $id_lugar != '')
                {
                    $dscto_lugar = $Obj_BuscaDescuentos->ConsultaDsctoLugarSalida($monto_total, $id_lugar);
                    $monto_total = $monto_total - $dscto_lugar[0];
                }
          }  
          
          if ($resultado_dsctos[3] == 4 && $aplica_desc_lugar_reserva == 1)
          {
                $dscto_lugar_reserva = $Obj_BuscaDescuentos->ConsultaDsctoLugarReserva($monto_total, $_SESSION['codigo_suc']);
    
          }

        $total_x_excursion = $sub - $dscto_lugar[0] - $dscto_lugar_reserva[0] - $dscto_pax[0];
        $total_x_excursiones[] = $total_x_excursion ;
        $suma_total = $total_x_excursion + $suma_total;
        
        $cant_pax[] = $cant_pasajeros;

        $descuento_tot = $dscto_pax[0] + $dscto_lugar[0] + $descuento_tot;
        $descuento_pax = $dscto_pax[0] + $descuento_pax;
        $descuento_lugar = $dscto_lugar[0] + $descuento_lugar;
        $cant_pasajeros = 0;

        $j=1;
        
        $Obj_BuscaDescuentos->InsertDescuentos($voucher, $dscto_pax[0], $dscto_lugar[0], $dscto_lugar_reserva[0], 0); 
        
        $dscto_pax[0] = 0;
        $dscto_lugar[0] = 0;
        $dscto_lugar_reserva[0] = 0;
        $total_x_excursion = 0;
    }
    

    
    $suma_sub = $sub + $suma_sub;
    $total_exento = $suma_sub - $total_afecto;
    $suma_subtotal = 0;
    $monto_total = 0;
    $sub = 0;
    $cant_pax_adt = 0;
    $cant_pax_chd = 0;

}
mysql_free_result($Query);

## Solo para aplicar descuentos
if ($resultado_dsctos[1] == 2 && $num_excursiones > 1 && $cant_min_dscto <= $cant_exc && $tipo_opcion == 1)
{

    for ($i=0; $i<count($codigo_excursion); $i++)
    {
        $dscto_excursion = $Obj_BuscaDescuentos->ActualizaConDescuentoDeExcursiones($total_x_excursiones[$i] , $codigo_excursion[$i], $cant_pax[$i], $codigo_distribuidor);
    }
    
}
else
{
    
    if ($tipo_opcion == 0) //RA
    {

        $dscto_excursion = $Obj_BuscaDescuentos->ActualizaSinDescuentoDeExcursionesDistribuidores($total_precio_adt, $total_precio_chd, $cant_pax_adt, $cant_pax_chd, $excursion, $codigo_distribuidor);
        
    }
    if ($tipo_opcion == 1)
    {
        for ($i=0; $i<count($codigo_excursion); $i++)
        {
           $dscto_excursion = $Obj_BuscaDescuentos->ActualizaSinDescuentoDeExcursiones($total_x_excursiones[$i], $codigo_excursion[$i], $cant_pax[$i], $codigo_distribuidor);
        } 
    }
    
  
    if ($tipo_opcion == 4) // Mayorista
    { // Calcula comision chd
    
     
      if ($dscto_especial > 0 && $dscto_especial !='')
      {
          $dscto_especial = 100 - $dscto_especial;
          
            for ($i=0; $i<count($codigo_excursion); $i++)
            {
                $dscto_excursion = $Obj_BuscaDescuentos->ActualizaDsctoEspecial($codigo_excursion[$i], $codigo_distribuidor, $dscto_especial);
            } 
            $flag_correo_descuento = true;
      }
      else
      {
            if($dscto_coordinador==100)
            {

                for ($i=0; $i<count($codigo_excursion); $i++)
                {
                    $dscto_excursion = $Obj_BuscaDescuentos->ActualizaSinDescuentoDeExcursionesNinguno($codigo_excursion[$i], $codigo_distribuidor);
                } 
            }
            else
            {

                for ($i=0; $i<count($codigo_excursion); $i++)
                {
                   $dscto_excursion = $Obj_BuscaDescuentos->ActualizaDsctoCoordinador($codigo_excursion[$i], $codigo_distribuidor, $dscto_coordinador);
                }  
            }
      }

    }
  
}

## solo para aplicar beneficio
if ( $tipo_opcion == 3 && ($beneficio_act != '') )
{
    $telefono = $_POST['telefono'];
    $rut = $_POST['rut'];
    
    $beneficio = new Beneficios();
    $lista = $beneficio->Consulta_descuentos_beneficios($beneficio_act);
    
    for ($i=0; $i<count($lista); $i++)
    {
        $desc_beneficio = ($lista[$i]['precio'] * $lista[$i]['descuento']) /100; 
        $precio_ex = $lista[$i]['precio'] - $desc_beneficio;
        
        $comision = $beneficio->Consulta_Comisionado($lista[$i]['codigo_re']);
        $comisionado = $precio_ex - $comision;
        
        $beneficio->Actualiza_Reserva_Excursion_Beneficio($lista[$i]['codigo_re'], $desc_beneficio, $beneficio_act, $precio_ex, $comisionado );
    }
    
    if ($beneficio_act == 2 || $beneficio_act == 6)
    {
        $beneficio->Actualiza_datos_Beneficio($lista[0]['codigo_re'], $telefono, $rut);
    }
    
    
    Beneficios::close();
}
   

## solo para aplicar promocion
if ( ($tipo_opcion == 5 ) && ($promocion_act != '') )
{
    $promociones = new Consulta_Promociones(); 
    $promociones->Ingresa_Promocion($promocion_act, $voucher_servicio[0]);
}

## solo para aplicar combos en la pesta�a de promociones
if ( $tipo_opcion == 2  && $combo_promocion == 1)
{

    foreach($excursiones_dscto as $codigos)
    {  
      $Obj_BuscaDescuentos->ActualizaDsctoCombo($codigos,$promocion_act,$cant_promo_adt,$cant_promo_chd,$dscto_unitario_combo_adt, $dscto_unitario_combo_chd,$comision_promocion,$num_excursiones_arreglo);
    }

}   

## solo para aplicar ofertas
if ( $tipo_opcion == 6 )
{
    for ($i=0; $i<count($codigo_excursion); $i++)
    {
       $Obj_BuscaDescuentos->ActualizaDsctoOferta($codigo_excursion[$i], $fecha_array[$i], $codigo_distribuidor, 1);
    } 
}

## solo para aplicar VENTAS QR
if ( $tipo_opcion == 7 )
{
    for ($i=0; $i<count($codigo_excursion); $i++)
    {
       $Obj_BuscaDescuentos->ActualizaDsctoOferta($codigo_excursion[$i], $fecha_array[$i], $codigo_distribuidor, 2);
    } 
}

## VENTA RA HOTELEROS
if ( $tipo_opcion == 8 )
{
    $Obj_BuscaDescuentos->ActualizaHotelerosRA($excursion, $codigo_distribuidor);
}

mysql_close();

?>