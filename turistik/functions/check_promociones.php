<?php
Class Promociones
{

    function Consulta_promociones()
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $SQL = " SELECT ID_PROMO, CANT_EXCURSIONES, COMBO FROM PROMOCIONES
                 WHERE ESTADO =1 and COMBO = 1";
        $query=$miConexion->EjecutaConsulta($SQL);
        
        while($row = mysql_fetch_array($query))
        {            
            $promos[] = array('id_promocion'=>$row['ID_PROMO'],
                              'cant_excursiones'=>$row['CANT_EXCURSIONES'],
                              'combo'=>$row['COMBO']);
        
        }
        
        return $promos;
    }
    
 
    function Consulta_Cantidad_excursiones($sucursal, $cod_usua, $promo)
    {
        
    
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
       
        $SQL = "SELECT distinct(RT.COD_EXCURSION), RE.LUGAR_RE
                FROM RESERVA_EXCURSION_TEMPORAL RT
                INNER JOIN RESERVA_EXCURSION RE ON RE.ID_TEMPORAL = RT.ID_TEMPORAL
                LEFT JOIN LUGARES_REC_PROMOCIONES LEP ON LEP.ID_LUGAR_REC = RE.LUGAR_RE OR RE.LUGAR_RE = ''
                WHERE 
                RT.CODIGO_SUCURSAL = $sucursal and
                RT.COD_USUA = $cod_usua and
                LEP.ID_PROMO = $promo AND
                RT.ESTADO = 1";
        
        $query=$miConexion->EjecutaConsulta($SQL);
            while($row = mysql_fetch_array($query))
            {
                //echo $row['COD_EXCURSION'];
                if ($row['COD_EXCURSION'] != 6 && $row['COD_EXCURSION'] != 54 && $row['COD_EXCURSION'] != 55 && $row['COD_EXCURSION'] != 63 && $row['COD_EXCURSION'] != 64)
                {
                    $sql2 = "SELECT ID_LUGAR_REC FROM LUGARES_REC_PROMOCIONES WHERE ID_PROMO = $promo AND ID_LUGAR_REC = ".$row['LUGAR_RE']." ";
                    $query2=$miConexion->EjecutaConsulta($sql2);
                    $contador = mysql_num_rows($query2);
                    $suma = $contador + $suma;
                }
                else
                {
                    $contador =1;
                    $suma = $contador + $suma;
                }

            }
        mysql_free_result($query); 
        
        return $suma;
    }   
    
    function Consulta_promocion_suc_nac($sucursal, $nacionalidad)
    {

        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $sql = "SELECT distinct(SP.PROMOCION)
        FROM SUCURSALES_PROMOCIONES SP
        WHERE SP.SUCURSAL = ".$sucursal." AND
        SP.ESTADO =  1 AND
        SP.PROMOCION IN (SELECT NP.ID_PROMO
                          FROM NACIONALIDAD_PROMOCIONES NP
                          WHERE 
                          NP.ID_PROMO = SP.PROMOCION and 
                          NP.ID_NACIONALIDAD = ".$nacionalidad.")";
                          
        $query=$miConexion->EjecutaConsulta($sql);
            while($row = mysql_fetch_array($query))
            {            
                $promo[] = $row['PROMOCION'];

            }
        mysql_free_result($query); 
        
        return $promo;
    }
    
    
    function Consulta_Nacionalidad($sucursal, $cod_usua)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $SQL = " SELECT NACIONALIDAD FROM RESERVA_EXCURSION_TEMPORAL 
                 WHERE 
                 CODIGO_SUCURSAL = $sucursal and
                 COD_USUA = $cod_usua and
                 ESTADO = 1
                 GROUP BY NACIONALIDAD";
        
        $query=$miConexion->EjecutaConsulta($SQL);
            while($row = mysql_fetch_array($query))
            {            
                $nacionalidad = $row['NACIONALIDAD'];

            }
        mysql_free_result($query); 
        
        return $nacionalidad;
    } 
    
    function Consulta_promocion_cliente($cliente)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $sql = "SELECT ID_PROMO
                FROM CLIENTES_PROMOCIONES
                WHERE ID_CLIENTE = ".$cliente." ";
             
        $query=$miConexion->EjecutaConsulta($sql);
        while($row = mysql_fetch_array($query))
        {   
            $promo[] = $row['ID_PROMO'];
        }
        mysql_free_result($query);       
        return $promo;
    }
    
 
    function Consulta_promocion_excursiones($promo)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $sql = "SELECT ID_EXCURSION
                FROM EXCURSIONES_PROMOCIONES
                WHERE ID_PROMO = ".$promo." ";
                          
        $query=$miConexion->EjecutaConsulta($sql);
        while($row = mysql_fetch_array($query))
        {   
            $excursiones[] = $row['ID_EXCURSION']; 
        }
        
        mysql_free_result($query); 
        
        return $excursiones;
    }
    
    
    function Consulta_excursiones($sucursal, $cod_usua)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
     
        $SQL = "SELECT RT.COD_EXCURSION,COUNT(COD_EXCURSION) AS CANTIDAD
                FROM RESERVA_EXCURSION_TEMPORAL RT
                WHERE 
                RT.CODIGO_SUCURSAL = $sucursal and
                RT.COD_USUA = $cod_usua and
                RT.ESTADO = 1
                GROUP BY RT.COD_EXCURSION
                ORDER BY RT.ID_TEMPORAL";
        
        $query=$miConexion->EjecutaConsulta($SQL);
 
        
        return $query;
    } 
    
    function consulta_montos_excursion_origen($excursion)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
     
        $SQL = "SELECT VALOR_EXENTO, PRECIO_EO
                FROM EXCURSION_ORIGEN
                WHERE 
                CODIGO_EO = $excursion";
        
        $query=$miConexion->EjecutaConsulta($SQL);

        return $query;
    }
    
    function Consulta_excursiones_mandatorias($promo)
    {
        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
     
        $SQL = "SELECT ID_EXCURSION 
                FROM EXCURSIONES_PROMOCIONES
                WHERE ID_PROMO = $promo and
                EX_MANDATORIA = 1";
        
        $query=$miConexion->EjecutaConsulta($SQL);
        while($row = mysql_fetch_array($query))
        {   
            $excursiones[] = $row['ID_EXCURSION']; 
        }


        return $excursiones;   
    }
    
    function Consulta_cantidad_pasajeros($sucursal, $usuario, $codigo_eo)
    {
         $sql = "SELECT RT.TIPO_PASAJERO,
          count(RT.TIPO_PASAJERO) as CANTIDAD
          FROM RESERVA_EXCURSION_TEMPORAL RT
          WHERE 
          RT.CODIGO_SUCURSAL = $sucursal and
          RT.COD_USUA = $usuario and
          RT.COD_EXCURSION = $codigo_eo and
          RT.ESTADO = 1
          GROUP BY RT.TIPO_PASAJERO";

        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
              
        $query=$miConexion->EjecutaConsulta($sql);

        
        return $query; 
    }
    
    
    function Consulta_afecto_exento($sucursal, $usuario, $excursion)
    {
      $sql = "SELECT RT.TIPO_PASAJERO, EO.PRECIO_EO, EO.VALOR_EXENTO, EO.VALOR_CHILD
              FROM RESERVA_EXCURSION_TEMPORAL RT
              INNER JOIN EXCURSION_ORIGEN EO ON EO.CODIGO_EO = RT.COD_EXCURSION
              WHERE 
              RT.CODIGO_SUCURSAL = $sucursal and
              RT.COD_USUA = $usuario and
              RT.ESTADO = 1 and
              RT.COD_EXCURSION = $excursion
              ORDER BY RT.COD_EXCURSION";

        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
              
        $query=$miConexion->EjecutaConsulta($sql);

        
        return $query;
    }
    
    
    
    
}

Class Beneficios
{
    
    var $sucursal;
    var $cod_usua;
    var $miConexion;
    
    function __construct()
    {
        $this->miConexion = new ClaseConexion;
        $this->miConexion->Conectar();
        $this->sucursal = $_SESSION['codigo_suc'];
        $this->cod_usua = $_SESSION['codigo_usu'];
    }
    
    
    function Consulta_tipos_clientes($excursion, $nacionalidad, $sucursal)
    {
        
        $sql = "SELECT ID_TIPO_CLIENTE
                FROM DSCTOS_TIPOS_CLIENTES DTC
                WHERE DTC.ID_EXCURSION = ".$excursion." AND
                DTC.ID_NACIONALIDAD = ".$nacionalidad." AND
                DTC.ID_SUCURSAL = ".$sucursal." ";
                          
        $query = $this->miConexion->EjecutaConsulta($sql);
         while($row = mysql_fetch_array($query))
            {   
                $tipo_beneficio[] = $row['ID_TIPO_CLIENTE'];

            }
        
        mysql_free_result($query); 

        
        return $tipo_beneficio;
    }
    
    function Consulta_excursiones()
    {
        
        $SQL = " SELECT COD_EXCURSION FROM RESERVA_EXCURSION_TEMPORAL 
                WHERE 
                CODIGO_SUCURSAL = ".$this->sucursal." and
                COD_USUA = ".$this->cod_usua." and
                ESTADO = 1
                GROUP BY COD_EXCURSION";
                          
        $query = $this->miConexion->EjecutaConsulta($SQL);
         while($row = mysql_fetch_array($query))
            {   

                $excursion[] = $row['COD_EXCURSION'];

            }
        
        mysql_free_result($query); 

        
        return $excursion;
    }
    
    
    public function Consulta_estado_tipo_usuario()
    {
        if ($_SESSION["cod_tusua"] == 4)
        {
            $tipo = "ESTADO_EXTERNO";
        }
        else
        {
            $tipo = "ESTADO_TURISTIK";
        }

        return $tipo;
    }
    
    function Consulta_descuentos_beneficios($id)
    {

        $query = $this->miConexion->EjecutaSP("Consulta_descuentos_beneficios"," ".$id.", ".$this->cod_usua.", ".$this->sucursal." ");
        while ($row = mysql_fetch_assoc($query))
        {
            $resumen[] = array ('precio'=> $row['PRECIO'],
                                'descuento' => $row['DESCUENTO'],
                                'codigo_re' => $row['CODIGO_RE'],
                                'codigo_eo' => $row['COD_EXCURSION']);
        }
        
        
        mysql_free_result($query); 
        
        if (count($resumen) == 0)
        {
            return 0;
        }
        else
        {
            return $resumen;
        }
        
        
        
    }
    
    function Actualiza_Reserva_Excursion_Beneficio($codigo_re, $descuento, $tipo_beneficio, $precio, $precio_comisionado)
    {
        $miConexion = new ClaseConexion;
        $miConexion->Conectar();
        $SQL = " UPDATE RESERVA_EXCURSION 
                 SET DSCTO_CLIENTE = $descuento,
                 TIPO_CLIENTE = $tipo_beneficio,
                 PRECIO_RE = $precio,
                 PRECIO_COMISIONADO_RE = $precio_comisionado
                 WHERE CODIGO_RE = $codigo_re";
    
    
        $miConexion->EjecutaConsulta($SQL);

    }
    
    
    function Actualiza_datos_Beneficio($codigo_re, $telefono, $rut)
    {
        $miConexion = new ClaseConexion;
        $miConexion->Conectar();
        $SQL = " UPDATE RESERVA_EXCURSION 
                 SET PASAPORTE_RE = '".$rut."',
                 TELEFONO_RE = '".$telefono."'
                 WHERE CODIGO_RE = $codigo_re";

        $miConexion->EjecutaConsulta($SQL);
    }
    
    
    function Consulta_Comisionado($codigo_re)
    {
        $miConexion = new ClaseConexion;
        $miConexion->Conectar();
        $SQL = " SELECT COMISION_RE FROM RESERVA_EXCURSION
                 WHERE CODIGO_RE = $codigo_re";
    
        $query = $miConexion->EjecutaConsulta($SQL);
        while ($row = mysql_fetch_assoc($query))
        {
            $comision = $row['COMISION_RE'];
        }
        mysql_free_result($query); 
        
        
        return $comision;
    }
    
    
    public static function close()
    {
        mysql_close();
    }
    

    
}
?>