
<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);

	function GeneracionBoardingPASS($Voucher, 
									$FechaEmbarque, $ExcursionID, $ExcursionName, $UsuarioID, $Cliente, $NombreCliente,
									$TipoPasajero, $EstadoPago, $Monto, $Descuento, $Total, $TextoEspanol, $TextoIngles, 
									$TextoPortuges, $Subtitulo, $SucursalID, $session_compra, $idioma, $boletaTTS, $minutos_recogida, $condiciones, $serial)
	{

		//$client = new SoapClient("http://localhost:5000/service1.asmx?WSDL");	
	   $client = new SoapClient("http://172.19.1.15/service1.asmx?WSDL", array('cache_wsdl' => 0,'encoding'=>'ISO-8859-1'));	
		//$client = new SoapClient("http://localhost:9090/service1.asmx?WSDL");	
	//	$client = new SoapClient("http://192.168.1.70:8089/service1.asmx?WSDL", array('cache_wsdl' => 0,'encoding'=>'ISO-8859-1'));
		$dat = array (  'Voucher'=>$Voucher,
						'FechaEmbarque'=>$FechaEmbarque ,
						'ExcursionID'=> $ExcursionID,
						'ExcursionNombre'=>$ExcursionName,
						'UsuarioID'=> $UsuarioID,
						'Cliente'=>$Cliente,
						'NombreCliente'=>$NombreCliente,
						'TipoPasajero'=>$TipoPasajero,
						'Estado_Pago'=>$EstadoPago,
						'Monto'=>$Monto, 
						'descuento'=>$Descuento,
						'total'=> $Total,
						'TxtES'=>$TextoEspanol,
						'TxtENG'=>$TextoIngles,
						'TxtPOR'=>$TextoPortuges,
						'SubTituloInf'=>$Subtitulo, 
						'Sucuarsal'=>$SucursalID,
                        'Correlativo' =>$session_compra,
                        'Idioma' =>$idioma,
                        'Boleta' => $boletaTTS,
                        'Hora_reco' => $minutos_recogida,
                        'Contenido' => $condiciones,
                        'Serial' => $serial);					
	   //print_r($dat);
    	$data =  $client->BoardingPass($dat);

		return  $data->BoardingPassResult;
	}
    
    
    
    function finalizar($Correlativo, $Afecto, $Exento)
    {

	   $client = new SoapClient("http://172.19.1.15/service1.asmx?WSDL", array('encoding'=>'ISO-8859-1'));	

		
		$dat = array (  'Correlativo'=>$Correlativo,
                        'Afecto'=>$Afecto,
                        'Exento' =>$Exento);					

    	$data = $client->Finalizar($dat);

		return  $data->FinalizarResult;
    }
    

  
?>
