var ControlFecha = function(num) {
	switch(num){
		case 1:
			var objD = document.MotorFrmHoteles.HT_fEntrada;
			var objH = document.MotorFrmHoteles.HT_fSalida;
			var objN = document.MotorFrmHoteles.HT_Noches;
			var objM = document.MotorFrmHoteles.HT_EstadiaMaxima;
			var objNhrof = document.MotorFrmHoteles.HT_NochesHrof;
			var difDias = dateDiff(objD.value, objH.value);			
			if(difDias<0){
				objH.value = dateAdd(objD.value, objN.value);		
			}else if(difDias == 0){
				objN.value = difDias+1;
				objNhrof.value = difDias+1;				
				objH.value = dateAdd(objD.value, objN.value);
			}else{
				objN.value = difDias;	
				objNhrof.value = difDias;
			}		
			
		break;
		
		case 2:
			var objD = document.MotorFrmVuelos.fechaSalida;
			var objH = document.MotorFrmVuelos.fechaRegreso;
			difDias = dateDiff(objD.value, objH.value);
			if(difDias<0){
				objH.value = objD.value;		
			}
		break;
		
		case 3:
			var objD = document.MotorFrmAutos.FechaRetiro;
			var objH = document.MotorFrmAutos.FechaEntrega;
			difDias = dateDiff(objD.value, objH.value);
			if(difDias<0){
				objH.value = objD.value;		
			}
		break;
		
		case 4:
			var objD = document.frm.fechaD;
			var objH = document.frm.fechaH;
			difDias = dateDiff(objD.value, objH.value);
			if(difDias<0){
				objH.value = dateAdd(objD.value,1);		
			}
		break;
		
		case 5:
			var objD = document.frm.FechaIn;
			var objH = document.frm.FechaOut;			
			var objN = GE('diass');
			var objNhrof = document.frm.diassHrof;
			var difDias = dateDiff(objD.value, objH.value);			
			if(difDias<0){
				objH.value = dateAdd(objD.value, objN.value);		
			}else if(difDias == 0){
				objN.value = difDias+1;
				objNhrof.value = difDias+1;				
				objH.value = dateAdd(objD.value, objN.value);
			}else{
				objN.value = difDias;	
				objNhrof.value = difDias;
			}
		break;
	}
}
function CNhotel() {
	document.MotorFrmHoteles.Hasta.value = dateAdd(document.MotorFrmHoteles.Desde.value, GE('diass').value);
}
function CNConsultaPaquete() {
	document.frm.FechaOut.value = dateAdd(document.frm.FechaIn.value, GE('diass').value);
}
//Scripts para Hoteles
var xrof = 'Hrof';
function HsetRooms(id,tot){
	num = $(id).value;
	for(var i=1;i<=tot;i++){
		$('HT_ADTs' + i + xrof).getParent().setStyle('visibility', 'hidden');
		$('HT_CHDs' + i + xrof).getParent().setStyle('visibility', 'hidden');

		$('HT_CHD' + i + '_A1').setStyle('visibility', 'hidden');
		$('HT_CHD' + i + '_A2').setStyle('visibility', 'hidden');	
		
		$('filaHab' + i).style.display = 'none';
	}
	for(var i=1;i<=num;i++){
		$('HT_ADTs' + i + xrof).getParent().setStyle('visibility', 'visible');
		$('HT_CHDs' + i + xrof).getParent().setStyle('visibility', 'visible');
		
		$('filaHab' + i).style.display = 'block';
	}
	for(var i=num;i<tot;i++){
		$('HT_ADTs' + i + xrof).value = 1;
		$('HT_ADTs' + i).value = 1;
		$('HT_CHDs' + i + xrof).value = 0;
		$('HT_CHDs' + i).value = 0;
		
		$('HT_CHD' + i + '_A1'+xrof).value = 0;
		$('HT_CHD' + i + '_A1').value = 0;
		$('HT_CHD' + i + '_A2'+xrof).value = 0;
		$('HT_CHD' + i + '_A2').value = 0;		
	}
	if(num<tot){
		$('HT_CHD' + (Number(num)+1) + '_A1' + xrof).getParent().setStyle('visibility', 'hidden');
		$('HT_CHD' + (Number(num)+1) + '_A2' + xrof).getParent().setStyle('visibility', 'hidden');
	}
	_textAge1=0;	
	for(var i=1;i<=3;i++){
		if($('HT_CHD'+i+'_A1'+xrof).getParent().getStyle('visibility') == 'hidden'){
			_textAge1++;
		}
	}
	if(_textAge1>2){
		$('ht_textAge1').setStyle('visibility', 'hidden');
	}
	//
	_textAge2=0;
	for(var i=1;i<=3;i++){
		if($('HT_CHD'+i+'_A2'+xrof).getParent().getStyle('visibility') == 'hidden'){
			_textAge2++;
		}
	}
	if(_textAge2>2){
		$('ht_textAge2').setStyle('visibility', 'hidden');
	}
}
function HsetAge(id,t,n){
	var _totHab = Number(t);
	obj = $(id);
	$('ht_textAge1').setStyle('visibility', 'visible');
	$('ht_textAge2').setStyle('visibility', 'visible');
	var num = obj.value;
	switch(num){
		case "0":
			$('HT_CHD' + n + '_A1'+xrof).getParent().setStyle('visibility', 'hidden');
			$('HT_CHD' + n + '_A1'+xrof).value=0;
			$('HT_CHD' + n + '_A1').value=0;
			
			$('HT_CHD' + n + '_A2'+xrof).getParent().setStyle('visibility', 'hidden');
			$('HT_CHD' + n + '_A2'+xrof).value=0;
			$('HT_CHD' + n + '_A2').value=0;
		break;
		
		case "1":
			$('HT_CHD' + n + '_A1'+xrof).getParent().setStyle('visibility', 'visible');
			
			$('HT_CHD' + n + '_A2'+xrof).getParent().setStyle('visibility', 'hidden');
			$('HT_CHD' + n + '_A2'+xrof).value=0;
			$('HT_CHD' + n + '_A2').value=0;
		break;
		
		case "2":
			$('HT_CHD' + n + '_A1'+xrof).getParent().setStyle('visibility', 'visible');		
			$('HT_CHD' + n + '_A2'+xrof).getParent().setStyle('visibility', 'visible');
		break;
	}
	_textAge1=0;	
	for(var i=1;i<=_totHab;i++){
		if($('HT_CHD'+i+'_A1'+xrof).getParent().getStyle('visibility')=='hidden'){
			_textAge1++;
		}		
	}
	if(_textAge1>2){
		$('ht_textAge1').setStyle('visibility', 'hidden');
	}

	_textAge2=0;
	for(var i=1;i<=_totHab;i++){
		if($('HT_CHD'+i+'_A2'+xrof).getParent().getStyle('visibility')=='hidden'){
			_textAge2++;
		}
	}
	if(_textAge2>2){
		$('ht_textAge2').setStyle('visibility', 'hidden');
	}
}
function setAgeCHDs(t){
	var _totHab = t;
	for(var i=1;i<=_totHab;i++){		
		for(var x=2;x>GE('HT_CHDs'+i).value;x--){
			$('HT_CHD' + i + '_A'+x+xrof).value=0;	
			$('HT_CHD' + i + '_A'+x).value=0;	
		}
	}
}
function setTip(obj,texto) {
	if(obj.value==texto){
		obj.value='';
		obj.style.color='#6D7783';
	}	
}
function getTip(obj,texto) {
	if(obj.value==''){
		obj.value=texto;
		obj.style.color='#999999';
	}	
}
//fin Scripts Hoteles

//Aereos
contarPax = function(){
	var _totPax = Number(GE('adt').value) + Number(GE('chd').value) + Number(GE('inf').value);
	if(_totPax>9){
		alert('La cantidad de pasajeros no puede ser mayor a 9.')	
	}
}