<?php
session_start();
include("menus.php");
$_Util=new Util;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css"/>
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<link href="../../css/menu_small.css" rel="stylesheet" type="text/css"/>
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>

<link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/ui.core.js"></script>
<script type="text/javascript" src="../../js/ui.datepicker.js"></script>
<link type="text/css" href="../../css/demos.css" rel="stylesheet" />

<script type="text/javascript">
	$(function() {
		$("#fecha_desde").datepicker();
        $("#fecha_hasta").datepicker();
	});
</script>
<script>

    function Reembolsar()
    {
        if (document.getElementById('precio').value == 0)
        {
            alert('Seleccione un servicio para reembolsar');
            return false;
        }
        if (document.getElementById('tipo').value == 'C' && document.getElementById('digitos_tarjeta').value == '')
        {
            alert('Ingrese ultimos 4 digitos de Tarjeta de Credito.');
            document.getElementById('digitos_tarjeta').focus();
            return false;
        }
        
        if (document.getElementById('mail').value == '')
        {
            alert('Ingrese E-mail de cliente.\n Campo Obligatorio (*)');
            document.getElementById('mail').focus();
            return false;
        }
        
        if (document.getElementById('mail').value != document.getElementById('mail2').value)
        {
               alert('Confirme E-mail de cliente.\n Campo Obligatorio (*)');
                document.getElementById('mail2').focus();
                return false;
        }
        
        if (document.getElementById('observacion').value == '')
        {
            alert('Ingrese observacion para reembolsar.\n Campo Obligatorio (*)');
            document.getElementById('observacion').focus();
            return false;
        }

           document.getElementById('porcentaje').value = document.getElementById('porc').value;
           document.getElementById('boleta_ttk').value = document.getElementById('boletaTTK').value;
           document.getElementById('boleta_tts').value = document.getElementById('boletaTTS').value;

           document.getElementById('form2').submit();
        
    }

    function validate()
    {
    
    var porcentaje = document.getElementById('porc').value;
    var cantidad = document.getElementById('cantidad').value;

    var total = 0;

    for (var i = 1; i <= cantidad; i++)
    {
     var names = "precio_re"+i; 
     var n = "check"+i; 
    
        
       if (document.getElementById(n).checked)
       {
        
        var v = document.getElementById(names).value;
 
            total =parseInt(total) + parseInt(v); 
           
        }
        
    
    }
    sub = (porcentaje * total) / 100;

    total = formatNumber(sub,''); 
    document.getElementById('precio').value=total;
    
    return true;
    }
</script>
</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">
  <table width="482" border="0" cellpadding="0" cellspacing="0">  
  <tr>
      <td width="80" rowspan="4"><img src="../Imagenes/money_back.jpg" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Reembolso ADM</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    

  </table>

</div>

<div id="Layer1">

<form name="form1" onsubmit="consulta_reembolso_adm(); return false" method="post" >
  <table width="700"  border="0" cellspacing="0">
    <tr>
      <td width="100" height="30px" class="celdalazul_peque">Fecha Desde:</td>
      <td width="200" height="30px" class="celdalCELEST">    	
      <input type="text" value="<? echo date('d/m/Y');?>" id="fecha_desde" name="fecha_desde" class="seleccionTurist_180" readonly=""/>
      </td>   
       <td width="100" height="30px" class="celdalazul_peque">Estado:</td>
        <td width="200" height="30px" class="celdalCELEST">     
           <select name="estado" id="estado" class="seleccionTurist_180">
              <option value="0" SELECTED>-- TODOS --</option>
              <option value="1">PENDIENTE</option>
              <option value="2">FINALIZADA</option>
           </select>
        </td> 
    </tr>
    <tr>
      <td width="100" height="30px" class="celdalazul_peque">Fecha Hasta:</td>
      <td width="200" height="30px" class="celdalCELEST">     
      <input type="text" value="<? echo date('d/m/Y');?>" id="fecha_hasta" name="fecha_hasta" class="seleccionTurist_180" readonly=""/>
      </td> 

      <td colspan="2" width="200" height="30px"  class="celdalCELEST">  
          <input type="submit" name="consultar" value="Consultar" class="BotonTurist_Azul"/>    
      </td>
    </tr>
  </table>
</form>


  <form  name="form2" id="form2" method="post" action="reembolsar.php">
  <?php
  echo '<div id="resultado_listar" style=" PADDING-TOP: 10px; TEXT-ALIGN: left; PADDING-BOTTOM: 3px; FONT-FAMILY: Arial; FONT-SIZE: 8pt; left:20px; top:258px; width:1170px; height:330px;" >';
  //include('CONSULTA_reembolso_adm.php');
  echo '</div>';
  ?>
   <input type="hidden" id="boleta_tts" name="boleta_tts"/>
   <input type="hidden"  id="boleta_ttk" name="boleta_ttk"/>
   <input type="hidden" id="porcentaje" name="porcentaje"/>

  </form>
</div>

</body>
</html>