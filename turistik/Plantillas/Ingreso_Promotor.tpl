<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:22px;
	top:156px;
	width:822px;
	height:232px;
	z-index:13;
}
#Layer3 {
	position:absolute;
	left:18px;
	top:56px;
	width:379px;
	height:62px;
	z-index:14;
}
-->
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Ingreso Usuario</title>
<style type="text/css">
<!--
#Layer2 {	position:absolute;
	left:831px;
	top:53px;
	width:132px;
	height:116px;
	z-index:1;
}
-->
</style>
<script type="text/JavaScript">

function pres_espacio()
	{
		if(event.keyCode==32)
			{
				event.returnValue = false;
				return;
			}
		
		event.returnValue = true;
	}		

function Popup()
	{
 		miPopup = window.open("../PHP/Ingreso_Ubicacion.php","miwin","top=200,left=350,width=550,height=200,scrollbars=yes,titlebar=no,location=no");
 		miPopup.focus();	
	}
	
function pres_num()
	{
		if((event.keyCode<48) || (event.keyCode>57))
			{
				event.returnValue = false;
			}		
	}
	
function validar()
	{
    	// Primer paso: Obtener el rut que ingreso el usuario
   		 var rutCompleto = document.form1.rutusuario.value;
		// Eliminamos los caracteres raros, espacios, puntos, guiones.
    	// Pasamos a minusculas, y separamos el rut y el digito verificador
    	rutCompl = rutCompleto.toLowerCase();
   		var dv=document.form1.dvusuario.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
   		 valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   		 valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
        		alert("El Rut no es Valido.Ingreselo Nuevamente");
        		document.form1.rutusuario.value="";
				document.form1.dvusuario.value="";
				document.form1.rutusuario.focus();
   			 }
	}

function verificarDV(rut, dv)
	{
   		var multiplicador = 9;
    	var aux = rut;
    	var suma = 0;
    	while(aux > 0)
			{
        		var unidades = aux % 10 ;
        		aux = (aux - unidades) / 10;
        		suma += (multiplicador * unidades);
        		multiplicador--;
        		if (multiplicador < 4)
					{
            			multiplicador = 9;
       				 }
    		}
    	digito = suma % 11;
    	if (digito == 10)
			{
        		digito = "k";
    		}
    	if (dv == digito)
			{
        		return true;
    		}
    	else 
			{
       			 return false;
    		}
	} 

function isEmailAddress(email)
	{
		var s = email;
		var filter=/^[A-Za-z][A-Za-z0-9_]*@[A-Za-z0-9_]+\.[A-Za-z0-9_.]+[A-za-z]$/;
		if (s.length == 0 ) return true;
		if (filter.test(s))
		return true;
		else
		alert("Ingrese una direcci�n de correo v�lida");
		document.form1.email.focus();
		return false;
}

function IngresarUsuario()
	{
		if (document.form1.tipo_usuario.value==0)
			{
				alert("Debe Seleccionar el Tipo de Usuario que desea Ingresar");
				document.form1.tipo_usuario.focus();
				return;
			}
			
		if (document.form1.rutusuario.value=='')
			{
				alert("Debe Ingresar el Rut del Usuario que desea Ingresar");
				document.form1.rutusuario.focus();
				return;
			}
		if (document.form1.dvusuario.value=='')
			{
				alert("Debe Ingresar el Digito Verificador del Rut del usuario que desea agregar");
				document.form1.dvusuario.focus();
				return;
			}
		if (document.form1.usuario.value=='')
			{
				alert("Debe Ingresar el Usuario que desea Agregar");
				document.form1.usuario.focus();
				return;
			}
		if (document.form1.passwordusuario.value=='')
			{
				alert("Debe Ingresar la Contrase�a para el usuario que desea ingresar");
				document.form1.passwordusuario.focus();
				return;
			}
		
		
		
		if (document.form1.nombreusuario.value=='')
			{
				alert("Debe Ingresar el Nombre");
				document.form1.nombreusuario.focus();
				return;
			}
		if (document.form1.apellido.value=='')
			{
				alert("Debe Ingresar el Apellido");
				document.form1.apellido.focus();
				return;
			}
		if (document.form1.direccion.value=='')
			{
				alert("Debe Ingresar la Direcci�n para el Usuario que desea Ingresar");
				document.form1.direccion.focus();
				return;
			}
		if (document.form1.comuna.value=='')
			{
				alert("Debe Ingresar la Comuna a la cual pertenece el Usuario");
				document.form1.comuna.focus();
				return;
			}
		if (document.form1.ciudad.value=='')
			{
				alert("Debe Ingresar la Ciudad a la cual pertenece el Usuario");
				document.form1.ciudad.focus();
				return;
			}
		if (document.form1.distribuidor.value==0)
			{
				alert("Debe Seleccionar el Distribuidor");
				document.form1.distribuidor.focus();
				return;
			}
		if (document.form1.usuariosistema.value=='0')
			{
				alert("Debe Seleccionar si Usuario del Sistema");
				document.form1.usuariosistema.focus();
				return;
			}
		if (document.form1.cuentacorriente.value=='')
			{
				alert("Debe Ingresar Cuenta para el Usuario que desea ingresar");
				document.form1.cuentacorriente.focus();
				return;
			}
		
		if (document.form1.tipocuenta.value=='0')
			{
				alert("Debe Seleccionar el Tipo de Cuenta Contable que desea ingresar para el Usuario");
				document.form1.tipocuenta.focus();
				return;
			}
		if (document.form1.BANCO.value=='0')
			{
				alert("Debe Seleccionar el Banco a la cual pertenece la Cuenta Corriente");
				document.form1.BANCO.focus();
				return;
			}
	//	if (document.form1.validacion.value==1)
	//		{
	//			if (document.form1.cmanager.value=='')
	//				{
	//					alert("Debe Ingresar el C�digo de manager para el Usuario que desea ingresar");
	//					document.form1.cmanager.focus();
	//					return;
	//				}
	//		}
		
		
		
		
		
    	var rutCompleto = document.form1.rutusuario.value;
		rutCompl = rutCompleto.toLowerCase();
    	var dv=document.form1.dvusuario.value;
    	var rut = rutCompl;
    	var valido = true;
    	// Primero comprobamos que el dv sea o un digito o una k
    	valido = valido && dv.match(/^[\dk]$/);
    	// luego vemos que el rut solo contenga digitos
    	valido = valido && rut.match(/^\d+$/);
    	// y por ultimo aplicamos la regla de calculo del DV
   	 	valido = valido && verificarDV(rut, dv);

    	if (valido)
			{}
    	else
			{
       	 		alert("El Rut no es Valido.Ingreselo Nuevamente");
        		document.form1.rutusuario.value="";
				document.form1.dvusaurio.value="";
    		}
		
		document.form1.guardar.value="guardar";
		document.form1.submit();
	}

function NuevoTipoUsuario()
	{
		miPopup = window.open("../PHP/Mantenedor_TipoUsuario.php?flag=1","cue","top=200,left=400,width=550,height=250,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
		
function NuevoDistribuidor()
	{
		miPopup = window.open("../PHP/Ingreso_Cliente.php?flag=1","cue","top=200,left=400,width=950,height=500,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
function IngresarCuentaContable()
	{
		miPopup = window.open("../PHP/Ingresar_Cuenta_Contable.php?flag=1","cuentacontable","top=200,left=400,width=550,height=350,scrollbars=NO,titlebar=no,location=no");
		miPopup.focus();
	}
function tipo()
	{
		document.form1.tipo.value=1;
		document.form1.submit();
	}
function SubmitFormulario()
	{	
		document.form1.tipo.value=1;
		document.form1.submit();
	}
function Limpiar()
	{
		document.form1.rutusuariovalue="";
		document.form1.dvusuario.value="";
		document.form1.tipo_usuario.value="0";
		document.form1.tipocuenta.value="0";
		document.form1.usuario.value="";
		document.form1.passwordusuario.value="";
		document.form1.nombreusuario.value="";
		document.form1.apellido.value="";
		document.form1.direccion.value="";
		document.form1.telefono.value="";
		document.form1.comuna.value="";
		document.form1.ciudad.value="";
		//document.form1.contactodistribuidor.value="";
		document.form1.distribuidor.value="0";
		document.form1.cuentacorriente.value="";
		document.form1.email.value="";
		document.form1.celular.value="";
		document.form1.usuariosistema.value="0";
		document.form1.BANCO.value="0";
	}
	
function NuevoTipoUsuario()
	{
		miPopup = window.open("../PHP/Mantenedor_TipoUsuario.php?flag=1&condicion=2","cue","top=200,left=400,width=550,height=300,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
	
function NuevoBanco()
	{
		miPopup = window.open("../PHP/Ingresar_Banco.php?flag=1","cuentacontable","top=200,left=400,width=550,height=350,scrollbars=NO,titlebar=no,location=no");
		miPopup.focus();
	}
</script>
</head>

<body oncontextmenu="return false">
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

  <form id="form1" name="form1" method="post" action="">
<div id="tablageneral" style="position:absolute; left:17px; top:156px; width:884px; height:251px; z-index:3; border:0pt black solid;">

    <table width="859" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="151" class="celdalazul">Tipo Usuario </td>
        <td width="281" class="celdalCELEST"><select name="tipo_usuario" class="seleccionTurist_180"   >
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : tipousuario -->
            <option value="{CODIGOTIPOUSAURIO}" class="seleccion2" {SELECCIONAR}>{DESCRIPCIONUSUARIO}</option>
            <!-- END BLOCK : tipousuario -->
          </select>
&nbsp;           <img src="../Imagenes/configurar19x19.png" width="19" height="19" alt="Nuevo tipo Usuario" style="cursor:hand" onclick="NuevoTipoUsuario()"/>
          <input type="hidden" name="tipo" /></td>
        <td width="125" class="celdalazul">Rut Promotor </td>
        <td width="302" class="celdalCELEST"><input name="rutusuario" type="text" class="seleccionTurist_180" onkeypress="pres_num();" maxlength="10" {VALORRUT}  />
        -
        <input name="dvusuario" type="text" class="seleccionT80_a" maxlength="1" onchange="validar()" {VALORDV}/></td>
      </tr>
      <tr>
        <td class="celdalazul">Usuario</td>
        <td class="celdalCELEST"><label>
          <input name="usuario" type="text"  maxlength="20" {VALORUSUARIO} onkeypress="pres_espacio()" />
        </label></td>
        <td class="celdalazul">Contrase&ntilde;a</td>
        <td class="celdalCELEST"><input name="passwordusuario" type="password" maxlength="20" {VALORPASSWORDUSUARIO} onkeypress="pres_espacio()" /></td>
      </tr>
      
      <tr>
        <td class="celdalazul">Nombre Promotor </td>
        <td class="celdalCELEST">
          <input name="nombreusuario" type="text" class="seleccionTurist_250" maxlength="39" {VALORNOMBREUSUARIO} />      </td>
        <td class="celdalazul">Apellido Promotor </td>
        <td class="celdalCELEST"><input name="apellido" type="text" class="seleccionTurist_250" maxlength="39" {VALORAPELLIDO} /></td>
      </tr>
      <tr>
        <td class="celdalazul">Direcci&oacute;n </td>
        <td class="celdalCELEST"><label>
          <input name="direccion" type="text" class="seleccionTurist_250" id="direccion" maxlength="39" {VALORDIRECCION} />
        </label></td>
        <td class="celdalazul">Telefono</td>
        <td class="celdalCELEST"><input name="telefono" type="text" class="seleccionTurist_180" onkeypress="pres_num();" maxlength="11" {VALORTELEFONO} /></td>
      </tr>
      <tr>
        <td class="celdalazul">Email</td>
        <td class="celdalCELEST"><input name="email" type="e_mail"  onchange="isEmailAddress(this.value)" {VALOREMAIL} /></td>
        <td class="celdalazul">Celular</td>
        <td class="celdalCELEST"><input name="celular" type="text" class="seleccionTurist_180" onkeypress="pres_num();" maxlength="11" {VALORCELULAR} /></td>
      </tr>
      <tr>
        <td class="celdalazul">Comuna</td>
        <td class="celdalCELEST"><label>
          <input name="comuna" type="text" class="seleccionTurist_180" id="comuna" maxlength="39" {VALORCOMUNA} />
        </label></td>
        <td class="celdalazul">Ciudad</td>
        <td class="celdalCELEST"><label>
          <input name="ciudad" type="text" class="seleccionTurist_180" id="ciudad" maxlength="39" {VALORCIUDAD} />
        </label></td>
      </tr>
      <tr>
        <td class="celdalazul">Distribuidor</td>
        <td class="celdalCELEST"><select name="distribuidor" class="seleccionTurist_250">
          <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
          <!-- START BLOCK : bloquedistribuidor -->
          <option value="{CODIGODISTRIBUIDOR}" class="seleccion2" {SELECCIONAR}>{DESCRIPCIONDISTRIBUIDOR}</option>
          <!-- END BLOCK : bloquedistribuidor -->
        </select></td>
        <td class="celdalazul">Usuario Sistema </td>
        <td class="celdalCELEST"><select name="usuariosistema" class="seleccionTurist_180">
          <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
          <!-- START BLOCK : bloqueusauriosistema -->
          <option value="{CODIGOUSUARIOSISTEMA}" class="seleccion2" {SELECCIONAR}>{DESCRIPCIONUSAURIOSISTEMA}</option>
          <!-- END BLOCK : bloqueusauriosistema -->
        </select>
        <input name='cmanager' type='hidden' class='seleccionTurist_180' id='cmanager' value="0" maxlength='3' /></td>
      </tr>
      <tr>
        <td class="celdalazul">Cuenta Corriente</td>
        <td class="celdalCELEST">
          <input name="cuentacorriente" type="text" class="seleccionTurist_180" {VALORCUENTACORRIENTE} />        </td>
        <td class="celdalazul">Cuenta Contable</td>
        <td class="celdalCELEST"><label>
          <select name="tipocuenta" class="seleccionTurist_250">
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : bloquetipocuenta -->
            <option value="{CODIGOCUENTA}" class="seleccion2" {SELECCIONAR}>{DESCRIPCIONCUENTA}</option>
            <!-- END BLOCK : bloquetipocuenta -->
          </select>
&nbsp;          <img src="../Imagenes/configurar19x19.png" width="19" height="19" alt="Ingresar Nueva Cuenta Contable" style="cursor:hand"  onclick="IngresarCuentaContable()"/></label></td>
      </tr>
      
      <tr>
        <td class="celdalazul">Banco</td>
        <td colspan="3" class="celdalCELEST"><select name="BANCO" class="seleccionTurist_180">
            <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
            <!-- START BLOCK : banco -->
            <option value="{CODIGO}" class="seleccion2" {SELECCIONAR}>{DESCRIPCIONBANCO}</option>
            <!-- END BLOCK : banco -->
          </select><img src="../Imagenes/configurar19x19.png" width="19" height="19"  onclick="NuevoBanco()" style="cursor:hand" alt="Nuevo Banco"/></td>
      </tr>
      

      
      
      <tr>
        <td><input type="hidden" name="validacion" {VALIDACION} /></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
    <table width="502" border="0" cellspacing="0">
      
      <tr>
        <td width="232">&nbsp;</td>
        <td width="205">&nbsp;</td>
        <td width="59">&nbsp;</td>
      </tr>
    </table>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>

</div>

<div class="TituloPlomo" id="Layer3">
  <table width="312" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="82" rowspan="4"><img src="../Imagenes/Promotor_80x80png.png" width="80" height="80" /></td>
      <td width="13" rowspan="4">&nbsp;</td>
      <td width="217"><p>&nbsp;</p>      </td>
    </tr>
    
    <tr>
      <td height="22" class="TituloPlomo"><strong>Ingreso Promotor </strong></td>
    </tr>
    <tr>
      <td height="22" class="TituloFecha">{fecha}</td>
    </tr>
    <tr>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
  </table>
</div>
<div id="boton" style="position:absolute; left:396px; top:436px; width:196px; height:25px; z-index:3; border:0pt black solid;">
<input name="Submit" type="button" class="BotonTurist_Celest" value="Ingresar" onclick="IngresarUsuario()" />
<input name="button" type="submit" class="BotonTurist_Celest" value="Limpiar" onclick="Limpiar()" />
<input type="hidden" name="guardar" value="0" />
</div>

  </form>
</body>
</html>
