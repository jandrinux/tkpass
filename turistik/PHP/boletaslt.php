<?

	 if ($_GET['json']!=1) {
		ini_set('display_errors', 0);
		include("menus.php");
		$_Util=new Util;
	 }
	 else {
		$client = new SoapClient("http://172.19.1.15:8080/sii.asmx?WSDL", array('encoding'=>'ISO-8859-1'));
		$envio  = array('CodigoToken' => "123123123",
                    'rut'=>$_GET["rut"],
                    'numero'=>$_GET["numero"]);
		$data = $client->consultarBoleta($envio);

		$datax = json_encode($data->consultarBoletaResult);
		header('Content-Type: application/json');
		echo ($datax);
		exit();
	}
?>

<html ng-app="sii">
<head >
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="../estilos1.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
    <script language="javascript" src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
    <link href="../../css/menu_small.css" rel="stylesheet" type="text/css">

    <style type="text/css" >
    #Layer3 {
     margin-top: 50px;
     margin-left: 20px;
     width:501px;
     height:62px;
 }
 #TituloPlomo
 {
   COLOR: #010356;
   FONT-FAMILY: Arial, Helvetica, Sans-Serif;
   FONT-SIZE: 12pt;
   FONT-STYLE: normal;
   FONT-VARIANT: normal;
   FONT-WEIGHT: normal;
   TEXT-DECORATION: none;
   TEXT-TRANSFORM: none
}
</style>
</head>
<body>



    <div class="TituloPlomo" id="Layer3">
      <table width="482" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="80" rowspan="4"><img src="../Imagenes/IMG_6716.jpg" width="80" height="80" /></td>
          <td width="13">&nbsp;</td>
          <td width="389">&nbsp;</td>
      </tr>
      <tr>
          <td>&nbsp;</td>
          <td><strong>SII</strong></td>
      </tr>
      <tr>
          <td>&nbsp;</td>
          <td><span class="TituloFecha"><?php echo $_Util->Fecha(); ?></span></td>
      </tr>

      <tr>
          <td class="TituloFecha">&nbsp;</td>
          <td height="22" class="TituloFecha">&nbsp;</td>
      </tr>
  </table>
  

</div>



<div id="Layer4" style="  margin-left: 20px;width:500px; height:96px; z-index:15">
    <div id="menuh">
        <ul>
           <li><a  href="siilt.php" >CAF</a></li>
           <li><a id="primero" href="boletaslt.php" >Boletas</a></li>
           <li><a href="notacreditolt.php" >Notas de Credito</a></li>
          
       </ul>
   </div>
   <br><br>
   <div ng-controller="formularioController">
     <form ng-submit="consultar()">
  		<table>
  			<tr>
  				<td class="celdalazul_peque">Consultar Boleta</td>
  				<td class="celdalCELEST">
            <select 
              ng-model="fr.empresa"
              ng-options="t.nombre for t in empresas"
            
            >
           
            </select>
  					<input ng-model="fr.boleta" type="text" placeholder="Ingrese Boleta">
  				</td>  
  				<td class="celdalCELEST">
  					<button class="BotonTurist_Celest">consultar</button>
  				</td>
  			</tr>
  		</table>
  	</form>
     <br>
     <table ng-show="infoVisible" width="500px" >
     		<thead>
     			<tr>
     				<td class='celdalazul_peque' width="50%">PDF</td>
     				<td class='celdalCELEST' style="align-text: center"  width="50%">
    
            <a  class="BotonTurist_Celest" href="{{datos.Message}}" target="_black">Descargar</a>
            </td>
     			</tr>
     		</thead>
     	
     </table>
   </div>



</div>
<br>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
<script>
	var app = angular.module("sii",[]);

	app.controller('formularioController', ['$scope','$http', function($scope,$http){
		$scope.datos = {}; 
    $scope.fr = {};
    $scope.infoVisible = false;
    $scope.empresas=  [
                        {
                          rut: "76033430-8",
                          nombre: "TRANSPORTES TURISTICOS SANTIAGO LTDA"
                        },
                        {
                          rut: "76708000-K",
                          nombre: "TURISTIK"
                        }
                      ];
    $scope.fr.empresa = $scope.empresas[0];

    $scope.consultar = function() {
      var f = $scope.fr;
      var url = "rut=" + f.empresa.rut + "&numero=" + f.boleta;
      $http.get('boletaslt.php?json=1&' + url).success(function(data){
        if (data.Error =="1")
        {
          alert("Boleta No Encontrada");
          $scope.infoVisible = false;
        }
        else
        {
          $scope.datos = data;
          $scope.infoVisible = true;
        }
          
      });
    }
	}]);
</script>
</body>
</html>

