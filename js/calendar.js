function highlightButton(thisButton, action) {
	if (action == 'over') {
		thisButton.style.borderBottom = '1px solid #999';
		thisButton.style.borderLeft = '1px solid #ccc';
		thisButton.style.borderRight = '1px solid #999';
		thisButton.style.borderTop = '1px solid #ccc';
	}
	else if (action == 'down') {
		thisButton.style.borderBottom = '1px solid #ccc';
		thisButton.style.borderLeft = '1px solid #999';
		thisButton.style.borderRight = '1px solid #ccc';
		thisButton.style.borderTop = '1px solid #999';
	}
	else if (action == 'out') {
		thisButton.style.borderBottom = '1px solid #e8e8e8';
		thisButton.style.borderLeft = '1px solid #e8e8e8';
		thisButton.style.borderRight = '1px solid #e8e8e8';
		thisButton.style.borderTop = '1px solid #e8e8e8';
	}
}



	CalendarioJavaScript=function (fecha,nombreControl,nombreControlTexto,left,top,controldiv){
		this.controldiv=controldiv;
		this.nombre="Calendario"+nombreControl+"DIV";
		this.nombreControlTexto=nombreControlTexto;
		this.nombreControl=nombreControl;
		this.dia=fecha.getDate();
		this.mes=fecha.getMonth();
		this.agno=fecha.getFullYear();
		this.fechaSeleccionada=new Date();
		//estilo del fondo
		this.colorFondoDireccionales="ButtonFace";
		this.colorFondoBarra="navy";
		this.colorFondoBarraDias="#ffffff";
		this.colorFondoCuadricula="#ffffff";
		this.colorFondoDiaActual="navy";
		this.colorFondoSeleccion="buttonface";
		//estilo de la fuente
		this.colorFontDireccionales="#000000"
		this.colorFontBarra="#ffffff";
		this.colorFontBarraDias="navy";
		this.colorFontCuadricula="#000000";
		this.colorFontDiaActual="#ffffff";
		this.colorFontSeleccion="red";
		this.colorBorde="ButtonFace";
		//tamaño de la fuente
		
		this.FontSizeDireccionales=4;
		this.FontSizeBarra=1;
		this.FontSizeBarraDias=2;
		this.FontSizeCuadricula=2;
		this.FontSizeDiaActual=2;
		this.FontSizeSeleccion=2;
		
		this.FontSizeListas=3;
		//diseño de la tabla
		this.alto=136;
		this.ancho=220;
		
		this.anchoBordeCuadricula=0;
		this.anchoBorde=2;
		
		
	
	}
CalendarioJavaScript.CalendarioDias=new Array("lun","mar","mie","jue","vie","sab","dom");
CalendarioJavaScript.CalendarioNombreDias=new Array("lunes","martes","miercoles","jueves","viernes","sabado","domingo");
CalendarioJavaScript.CalendarioMeses=new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
CalendarioJavaScript.CalendarioVectorMeses=new Array(31,28,31,30,31,30,31,31,30,31,30,31);

CalendarioJavaScript.y2k=function (number) { return (number < 1000) ? number + 1900 : number; }

CalendarioJavaScript.getWeek=function (month,day,year) {
    var when = new Date(year,month,day);
    var newYear = new Date(year,0,1);
    var offset = 7 + 1 - newYear.getDay();
    if (offset == 8) offset = 1;
    var daynum = ((Date.UTC(CalendarioJavaScript.y2k(year),when.getMonth(),when.getDate(),0,0,0) - Date.UTC(CalendarioJavaScript.y2k(year),0,1,0,0,0)) /1000/60/60/24) + 1;
    var weeknum = Math.floor((daynum-offset+7)/7);
    if (weeknum == 0) {
        year--;
        var prevNewYear = new Date(year,0,1);
        var prevOffset = 7 + 1 - prevNewYear.getDay();
        if (prevOffset == 2 || prevOffset == 8) weeknum = 53; else weeknum = 52;
    }
    return weeknum;
}


 CalendarioJavaScript.prototype.highlightButton= function(thisButton, action,dia,clase) {
	if (action == 'over') {
		thisButton.className=clase;
		if(dia!=0){
			if ((navigator.userAgent.toLowerCase().indexOf('netscape') > -1) ||
				(navigator.userAgent.toLowerCase().indexOf('mozilla') > -1))
			 {
				DivCalendario = document.getElementById(this.nombreControl+'aviso');
				//alert(DivCalendario);
			 }
			else {
				DivCalendario = document.all[this.nombreControl+'aviso'];
				//alert(DivCalendario);
			}
			fechaAux=new Date();
			fechaAux.setDate(dia);
			fechaAux.setMonth(this.mes);
			fechaAux.setFullYear(this.agno);
			diaAux=fechaAux.getDay();
			if(diaAux==0)
				diaAux=7
			DivCalendario.innerHTML=CalendarioJavaScript.CalendarioNombreDias[diaAux-1]+", "+dia+" de "+CalendarioJavaScript.CalendarioMeses[this.mes]+" de "+this.agno;
	
		}
	}
	else if (action == 'down') {
		thisButton.className=clase;
	}
	else if (action == 'out') {
		thisButton.className=clase;
		if(dia!=0){
			if ((navigator.userAgent.toLowerCase().indexOf('netscape') > -1) ||
				(navigator.userAgent.toLowerCase().indexOf('mozilla') > -1))
			 {
				DivCalendario = document.getElementById(this.nombreControl+'aviso');
				
			 }
			else {
				DivCalendario = document.all[this.nombreControl+'aviso'];
			}
			DivCalendario.innerHTML="Seleccione un dia";
		}
	}
}



	CalendarioJavaScript.prototype.CalendarioMostrarCalendario = function(){
		
		var tabla;
		tabla="";
		//liinicio="<n onmouseover=\"highlightButton(this, 'over');\" onmousedown=\"highlightButton(this, 'down');\" onmouseup=\"highlightButton(this, 'over');\" onmouseout=\"highlightButton(this, 'out');\"><nobr>";
		//lifin="</nobr></n>";
		tabla+="<table class='calendar' cellSpacing='0' cellPadding='0'>";
		tabla+="<tr>";
		tabla+="<td>";
		tabla+="<TABLE class='calendar' cellSpacing='0' cellPadding='0'>";
		claseboton="button";
		javaeventosheader="onmouseover=\""+this.nombreControl+".highlightButton(this, 'over',0,'headerhilite');\" onmousedown=\""+this.nombreControl+".highlightButton(this, 'down',0,'headeractive');\" onmouseup=\""+this.nombreControl+".highlightButton(this, 'over',0,'headerhilite');\" onmouseout=\""+this.nombreControl+".highlightButton(this, 'out',"+contadorDia+",'button');\""; 

		tabla+="<tr><td onclick=\""+this.nombreControl+".Ayuda();\" class='button' "+javaeventosheader+">?</td><td colspan='6' align='center' class='title'><div id='"+this.nombreControl+"bar'>"+CalendarioJavaScript.CalendarioMeses[this.mes]+","+this.agno+"</div></td><td class='button' "+javaeventosheader+" onclick=\""+this.nombreControl+".Cerrar();\">x</td></tr>";
		tabla+="<tr class='headrow' align='center'><td class='button' "+javaeventosheader+" onclick=\""+this.nombreControl+".ClickIzquierdaAgno();\">&lt;&lt;</td><td class='button' "+javaeventosheader+" onclick=\""+this.nombreControl+".ClickIzquierda();\">&lt;</td><td class='button' colspan='4' "+javaeventosheader+" onclick=\""+this.nombreControl+".ClickHoy();\">Hoy</td><td class='button' "+javaeventosheader+" onclick=\""+this.nombreControl+".ClickDerecha();\">&gt;</td><td class='button' "+javaeventosheader+" onclick=\""+this.nombreControl+".ClickDerechaAgno();\">&gt;&gt;</td></tr>";
		
	
	
		tabla+="<TR  class='daynames'><td class='titlewn'></td>"
		
		for(i=0;i<7;i++){
			
			tabla+="<TD class='name'>";
			tabla+=""+CalendarioJavaScript.CalendarioDias[i]+"";
			tabla+="</TD>";
		}
		var contadorDia=0,fechaAux,diaAux,DiasDelmes,fechaActual;
		fechaAux=new Date();
		fechaActual=new Date();
		fechaAux.setDate(1);
		fechaAux.setMonth(this.mes);
		fechaAux.setFullYear(this.agno);
		diaAux=fechaAux.getDay();
		
		//numeroSemana=CalendarioJavaScript.getWeek(this.agno,this.mes,1);
		numeroSemana = '';
		
		if(diaAux==0){
			diaAux=7;
		}
		DiasDelmes=CalendarioJavaScript.CalendarioVectorMeses[this.mes];
		if((this.agno%4)==0&&this.mes==1){
			DiasDelmes++;
		}
		var terminomes=false;
		javaeventosfila="onmouseover=\""+this.nombreControl+".highlightButton(this, 'over',0,'rowhilite');\"  onmouseup=\""+this.nombreControl+".highlightButton(this, 'over',0,'rowhilite');\" onmouseout=\""+this.nombreControl+".highlightButton(this, 'out',"+contadorDia+",'');\""; 

		for(i=0;i<6;i++){
		
			tabla+="<TR "+javaeventosfila+"><td class='wn'>"+numeroSemana+"</td>";
			numeroSemana = '';
			if(numeroSemana>=52)
				numeroSemana=1;
			for(j=0;j<7;j++){
				if(contadorDia==0 && i==0 && (j+1)==diaAux){
					contadorDia=1;
				}
				else if(contadorDia!=0){
					contadorDia++;
				}
				else{ 
					contadorDia=0;
				}
				if(contadorDia>=DiasDelmes){
					terminomes=true;
				}
				if(contadorDia>DiasDelmes){
					contadorDia=0;
					
					
				}
				if(contadorDia!=0){
					
					var clase="";
					
					if(fechaActual.getDate()==contadorDia&&fechaActual.getMonth()==this.mes&&fechaActual.getFullYear()==this.agno){
						clase="today";
						
					}
					else if(this.fechaSeleccionada.getDate()==contadorDia&&this.fechaSeleccionada.getMonth()==this.mes&&this.fechaSeleccionada.getFullYear()==this.agno){
						clase="selected";
					}
					else{
						clase="day";
					}				
					javaeventos="onmouseover=\""+this.nombreControl+".highlightButton(this, 'over',"+contadorDia+",'hilite');\" onmousedown=\""+this.nombreControl+".highlightButton(this, 'down',"+contadorDia+",'active');\" onmouseup=\""+this.nombreControl+".highlightButton(this, 'over',"+contadorDia+",'hilite');\" onmouseout=\""+this.nombreControl+".highlightButton(this, 'out',"+contadorDia+",'"+clase+"');\""; 
					tabla+="<TD align='center' class='"+clase+"' onclick=\""+this.nombreControl+".Click("+contadorDia+")\" "+javaeventos+"'>";
					tabla+=contadorDia;
				}
				else{
				
					tabla+="<TD class='emptycell'>";
					tabla+="&nbsp;";
				}
				
				tabla+="</TD>"
				
			}
			tabla+="</TR>";
			if(terminomes){
				break;
			}
			
		}
		tabla+="<tr class='footrow' align='center'><td colspan=8 class='ttip'><div id='"+this.nombreControl+"aviso'>Seleccione un dia</div></td></tr>";
		tabla+="</table>";
		tabla+="</td>";
		tabla+="</tr>";
		tabla+="</table>";
		
		if ((navigator.userAgent.toLowerCase().indexOf('netscape') > -1) ||
			(navigator.userAgent.toLowerCase().indexOf('mozilla') > -1))
		 {
			DivCalendario = document.getElementById(this.controldiv);
			
		 }
		else {
			DivCalendario = document.all[this.controldiv];
		}
		
		//alert(DivCalendario);
		DivCalendario.innerHTML=tabla;
	}
	CalendarioJavaScript.prototype.ActualizarCalendarioAgno =function(agno){
		this.agno=agno;
		this.CalendarioMostrarCalendario();

	}
    CalendarioJavaScript.prototype.ActualizarCalendarioMes =function(mes){
		this.mes=mes-1;
	    this.CalendarioMostrarCalendario();
	
    }
	CalendarioJavaScript.prototype.Click= function(dia){
		var controlTexto;
		var fechaAux=new Date();
		
		if ((navigator.userAgent.toLowerCase().indexOf('netscape') > -1) ||
			(navigator.userAgent.toLowerCase().indexOf('mozilla') > -1))
		 {
			controlTexto = document.getElementById(this.nombreControlTexto);
			
		 }
		else {
			controlTexto = document.all[this.nombreControlTexto];
		}
		
		if (dia<10)
		  axxxdia="0";
		else
		  axxxdia="";
		if ((this.mes+1)<10)
		  axxxmes="0";
		else
		  axxxmes="";
		controlTexto.value=this.agno+"-"+axxxmes+(this.mes+1)+"-"+axxxdia+dia;
		fechaAux.setFullYear(this.agno);
		fechaAux.setMonth(this.mes);
		fechaAux.setDate(dia);
		this.fechaSeleccionada=fechaAux;
		
		if ((navigator.userAgent.toLowerCase().indexOf('netscape') > -1) ||
			(navigator.userAgent.toLowerCase().indexOf('mozilla') > -1))
		 {
			DivCalendario = document.getElementById(this.controldiv);
			
		 }
		else {
			DivCalendario = document.all[this.controldiv];
			
		}
	

		
		DivCalendario.innerHTML="";
	
	}
	CalendarioJavaScript.prototype.ClickDerecha = function(){
		
		if((this.mes+1)>11){
			this.mes=0;
			this.agno++;
		}
		else{
			this.mes++;
		}
		this.CalendarioMostrarCalendario();
			
	}
	 CalendarioJavaScript.prototype.ClickIzquierda=function(){
		if((this.mes-1)<0){
			this.mes=11;
			this.agno--;
		}
		else{
			this.mes--;
		}
		this.CalendarioMostrarCalendario();	
	
	}
    CalendarioJavaScript.prototype.ClickDerechaAgno = function(){
	
		this.agno++;
		this.CalendarioMostrarCalendario();
		
	}

    CalendarioJavaScript.prototype.ClickIzquierdaAgno = function(){
	
		this.agno--;
		this.CalendarioMostrarCalendario();
			
	}
    CalendarioJavaScript.prototype.ClickHoy = function(){
	
		fechaActual=new Date();
		this.mes=fechaActual.getMonth();
		this.agno=fechaActual.getFullYear();
		//alert(fechaActual.getDate());
		this.Click(fechaActual.getDate());
//		this.CalendarioMostrarCalendario();
		
	}
    CalendarioJavaScript.prototype.Cerrar= function(){

		if ((navigator.userAgent.toLowerCase().indexOf('netscape') > -1) ||
			(navigator.userAgent.toLowerCase().indexOf('mozilla') > -1))
		 {
			DivCalendario = document.getElementById(this.controldiv);
			
		 }
		else {
			DivCalendario = document.all[this.controldiv];
		
		}
	
		
		
		DivCalendario.innerHTML="";
	
	}


    CalendarioJavaScript.prototype.Ayuda= function(dia){
		alert("AYUDA\n"
		+">> ,<< para pasar agno\n"
		+">,< para pasar mes\n"
		+"HOY, para mostrar el dia actual\n"
		+"X, para cancelar\n"
		+"Click en el dia que va a seleccionar\n");
		
	
	}


