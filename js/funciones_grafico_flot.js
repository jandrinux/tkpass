
function pieHover(event, pos, obj) 
{ 
	if (!obj)
                return;
	percent = parseFloat(obj.series.percent).toFixed(2);
    
	$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
}

/*function pieClick(event, pos, obj) 
{
   
	if (!obj)
                return;
	percent = parseFloat(obj.series.percent).toFixed(2);
    alert(obj.series.label);
	alert(''+obj.series.label+': '+percent+'%');
}*/