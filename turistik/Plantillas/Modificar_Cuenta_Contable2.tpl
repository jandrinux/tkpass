<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Modificar Información Cuentas Contables</title>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#Layer3 {position:absolute;
	left:15px;
	top:22px;
	width:494px;
	height:46px;
	z-index:14;
}
-->
</style>
</head>
<script>
function salir()
	{
		window.close();
	}

			
function ModificarCuenta()
	{
	 	if (document.form1.tipocuenta.value=='0')
			{
				alert("Debe Ingresar el Tipo de Cuenta que desea Modificar");
				document.form1.tipocuenta.focus();
				return;
			}
		
		if (document.form1.cuenta.value=='')
			{
				alert("Debe Ingresar Cuenta");
				document.form1.cuenta.focus();
				return;
			}
		
		if (document.form1.descripcion.value=='')
			{
				alert("Debe Ingresar Descripción para la cuenta que desea modificar");
				document.form1.descripcion.focus();
				return;
			}	
		if (document.form1.estado.value=='')
			{
				alert("Debe Ingresar Estado para la cuenta que desea modificar");
				document.form1.estado.focus();
				return;
			}	
		document.form1.guardar.value=1;
		document.form1.submit;
}

function ModificarTipoCuenta()
	{
		miPopup = window.open("../PHP/ListasTipoCuenta.php","listas","top=200,left=400,width=350,height=200,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
function ModificarEstado()
	{
		miPopup = window.open("../PHP/ListaEstadosCuentaContable.php","estadoscuentacontable","top=200,left=400,width=350,height=200,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
</script>
<body>
<form id="form1" name="form1" method="post" action="">
  <div class="TituloPlomo" id="Layer3">
    <table width="490" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="82" rowspan="4"><img src="../Imagenes/Crystal_Clear_app_korganizer80x80.png" width="80" height="80" /></td>
        <td width="13" rowspan="4">&nbsp;</td>
        <td width="395"><p>&nbsp;</p></td>
      </tr>
      <tr>
        <td height="22" class="TituloPlomo"><strong>Modificar Informaci&oacute;n Cuentas Contables </strong><strong></strong></td>
      </tr>
      <tr>
        <td height="22" class="TituloFecha">{fecha}</td>
      </tr>
      <tr>
        <td height="22" class="TituloFecha">&nbsp;</td>
      </tr>
    </table>
  </div>
  <p>&nbsp;</p>
 <p>&nbsp;</p>
 <p>&nbsp;</p>
<div id="segundodiv" style="position:absolute; left:14px; top:117px; width:466px; height:47px; z-index:3; border:0pt black solid;">
  <table width="420" border="0" cellspacing="0">
    
    <tr>
      <td width="121" class="celdalazul">Tipo Cuenta</td>
      <td width="295" class="celdalCELEST"><label>
        <input name="tipocuenta" type="text" class="seleccionTurist_180" id="tipocuenta" {CODIGOTIPOCUENTA}  readonly="true" />
        <img src="../Imagenes/modify.gif" width="16" height="16"  alt="Modificar Tipo Cuenta" style="cursor:hand" onclick="ModificarTipoCuenta()"/></label></td>
    </tr>
    <tr>
      <td class="celdalazul">Cuenta</td>
      <td class="celdalCELEST"><label>
        <input name="cuenta" type="text" class="seleccionTurist_180" id="cuenta" maxlength="12" {CUENTA} />
      </label></td>
    </tr>
    <tr>
      <td class="celdalazul">Descripci&oacute;n</td>
      <td class="celdalCELEST"><label>
        <input name="descripcion" type="text" class="seleccionTurist_250" id="descripcion" {DESCRIPCION} />
      </label></td>
    </tr>
    <tr>
      <td class="celdalazul">Estado</td>
      <td class="celdalCELEST"><label>
        <input name="estado" type="text" class="seleccionTurist_180" id="estado" {ESTADO} readonly="true" />
        <img src="../Imagenes/modify.gif" width="16" height="16"  alt="Modificar Estado" style="cursor:hand" onclick="ModificarEstado()"/></label></td>
    </tr>
  </table>
  <div id="tercerdiv" style="position:absolute; left:134px; top:133px; width:219px; height:23px; z-index:3; border:0pt black solid;">
   <input name="guardar" type="hidden" value="0" />
    <input name="CODIGOCUENTA" type="hidden" value="codnulo" />
	<input name="CODIGOESTADO" type="hidden" value="codnulo" />
	
   
    <input name="button" type="submit" class="BotonTurist_Celest" value="Modificar"  onclick="ModificarCuenta()"/>
  </div>
  <p>&nbsp;</p>
</div>
</form>
</body>
</html>
