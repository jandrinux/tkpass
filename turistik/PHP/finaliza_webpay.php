<?php
//ini_set('display_errors', 1);
session_start();
require ("../Clases/ClaseConexion.inc.php");
include ("../Clases/class.bitacora.php");


$WebPay = new CompraWebPay($_GET['cod_vendedor'],$_GET['codigo_dist'], $_GET['fop'], $_GET['tipo_re'], $_GET['tts'], $_GET['ttk']);

$resultado =  $WebPay->Consulta_Cantidad_Excursiones($_SESSION['codigo_suc'], $_SESSION["codigo_usu"]);

if($resultado == 'OK')
{
     $WebPay->Ingresa_Reserva_Operacion($_SESSION['codigo_suc']);
}

   
Class CompraWebPay
{
    var $TBK_URL_EXITO;
    var $TBK_URL_FRACASO;
    var $TBK_ID_SESION;
    var $TBK_TIPO_TRANSACCION;
    var $TBK_ORDEN_COMPRA;
    var $TBK_MONTO;
    var $cantidad;
    var $producto; 
    var $dir_hotel;
    var $email;
    var $vendedor;
    var $fecha ; //fecha_hoy
    var $fecha_servicio;
    var $nacionalidad;
    var $nombre;
    var $valor_unitario;
    var $moneda;
    var $monto_total;
    var $contador;
    
    
    #Datos de Carrito de compras
    ###########################3
    var $tipoReserva;
    var $codigo_dist;
    var $fop;
    var $codigo_vendedor;


    
    
    public function __construct($codigo_vendedor, $codigo_dist, $fop, $tipoReserva, $tts, $ttk) 
    {
        $this->fecha = date("Y-m-d"); #fecha_hoy
        $this->TBK_URL_EXITO = "http://172.19.1.10/php-web/comprar_/kcc/exito.php";
        $this->TBK_URL_FRACASO = "http://172.19.1.10/php-web/comprar_/kcc/fracaso.php";
        $this->TBK_ID_SESION = '777'.time().date('Ymd');
        $this->TBK_TIPO_TRANSACCION = "TR_NORMAL";
        $this->TBK_ORDEN_COMPRA = "OC_".date("ymdis");
        $this->valor_unitario = 1;
        $this->moneda = 1;
        $this->vendedor = $_SESSION["usuario"];
        $this->email = 'reservas@turistik.cl';
        $this->dir_hotel = 'tkpas';

        $this->tipoReserva = $tipoReserva;
        $this->codigo_dist = $codigo_dist;
        $this->codigo_vendedor = $codigo_vendedor;
        $this->fop = $fop;
        $this->monto_total = 0;
        $this->contador = 0;
        $this->tts = $tts;
        $this->ttk = $ttk;
  
        
         
    }
    
    public function Consulta_Cantidad_Excursiones($codigo_suc, $codigo_usu)
    {
        $miConexion = new ClaseConexion;
        $miConexion->conectar();
        $SQL = "SELECT COD_EXCURSION, PASAJERO, NACIONALIDAD, FECHA
                FROM RESERVA_EXCURSION_TEMPORAL
                WHERE CODIGO_SUCURSAL = ".$codigo_suc." AND
                COD_USUA = ".$codigo_usu."
                GROUP BY COD_EXCURSION";
              
        $query = $miConexion->EjecutaConsulta($SQL);
        $cant = mysql_num_rows($query);   
        if ($cant == 1)
        {
              	while ($row = mysql_fetch_array($query))
                {           	   
            	   $this->producto = $row['COD_EXCURSION']; 
                   $this->nombre = $row['PASAJERO'];
                   $this->nacionalidad = $row['NACIONALIDAD'];
                   $this->fecha_servicio = $row['FECHA'];                  
            	} 
                
                
                   $miConexion->conectar();                         
                   $query = $miConexion->EjecutaSP("consulta_precio_comision_exc", " ".$this->producto.", ".$this->codigo_dist." ");
                   while($row_comision=mysql_fetch_array($query))
                   {       
                       $comision = $row_comision['comision'];
                   }
                   mysql_free_result($query);
                
                
                
                    $SQL2 = "SELECT ID_TEMPORAL, PRECIO, TIPO_PASAJERO
                            FROM RESERVA_EXCURSION_TEMPORAL
                            WHERE CODIGO_SUCURSAL = ".$codigo_suc." AND
                            COD_USUA = ".$codigo_usu." ";
                    
                    $query2 = $miConexion->EjecutaConsulta($SQL2);
                    while ($row2 = mysql_fetch_array($query2))
                    {           	     
                       ## ACTUALIZO REGISTROS DE RESERVA_EXCURSION CON ORDEN DE COMPRA, 
                       ## TOMANDO EL ID_TEMPORAL DE CADA PASAJERO, PARA ASAOCIARLO AL FINAL DE LA VENTA
                       $this->monto_total = $row2['PRECIO'] + $this->monto_total;
                       $this->contador = $this->contador + 1;
                       $precio_x_pax = $row2['PRECIO'];
                       
                        if ($row2['TIPO_PASAJERO'] == 'ADT')
                        {

                           $comision_upt = $comision;
                
                       }
                       else
                       {

                           $comision_upt = $comision / 2;
                       }
                        
                        $comisionado_update = $precio_x_pax - $comision_upt;
                       

                       $miConexion->conectar();
                       $SQL3 = "UPDATE RESERVA_EXCURSION
                                SET OC = '".$this->TBK_ORDEN_COMPRA."',
                                    CODIGO_DIST = '".$this->codigo_dist."',
                                    CODIGO_VENDEDOR_RE = '".$this->codigo_vendedor."',
                                    TIPO_RE = '".$this->tipoReserva."',
                                    FOP = '".$this->fop."',
                                    COMISION_RE = ".$comision_upt.",
                                    PRECIO_COMISIONADO_RE = ".$comisionado_update."
                                WHERE ID_TEMPORAL = ".$row2['ID_TEMPORAL']."  ";
                                
                       $query3 = $miConexion->EjecutaConsulta($SQL3);
                       
                       
                        $miConexion->Conectar();
                      	$sql = "select CODIGO_RE from RESERVA_EXCURSION where ID_TEMPORAL=".$row2['ID_TEMPORAL'];
            	      	$res = $miConexion->EjecutaConsulta($sql);
                        while($row3=mysql_fetch_array($res))
                        {
                            $b = new Bitacora();
                            $b->FECHA = date("Y-m-d  h:i:s");
                            $b->NUEVO = true;
                            $b->USUARIO = $_SESSION['usuario'];
                            $b->EVENTO ="TOMA RESERVA WEBPAY";
                            $b->DESCRIPCION ="";
                            $b->ARCHIVO ="finalizar_webpay.php";
                            $b->CODIGO_RE=$row3["CODIGO_RE"];
                            $b->add();
                    
                        }
                       
                 
                	} 
                    mysql_free_result($query3); 
                    mysql_free_result($query2); 
                    mysql_close();
                    
                    //$this->TBK_MONTO = 50;
                   
                    $this->TBK_MONTO = $this->monto_total;
                    $this->cantidad = $this->contador;
                    return 'OK';
        }
        else
        {
            $this->Consulta_Errores(2);
        }
     			
        mysql_free_result($query); 
        mysql_close();
        


    }
    
    
    public function Ingresa_Reserva_Operacion($codigo_suc)
    {

        $miConexion = new ClaseConexion;
        $miConexion->conectar();
        $query2=$miConexion->EjecutaSP("Ingresar_Operacion","'".$this->vendedor."','".$this->producto."','".$this->TBK_ID_SESION."','".$this->valor_unitario."','".$this->moneda."','".$this->fecha_servicio."','".$this->cantidad."','".$this->nombre."','".$this->dir_hotel."','".$this->email."','".$this->TBK_MONTO."','".$this->TBK_ORDEN_COMPRA."',@respuesta,@mensaje");

        
       	$parametrosSalida=$miConexion->ObtenerResultadoSP("@respuesta,@pmessage");
        $rowverificacion = mysql_fetch_assoc($parametrosSalida);	
    	
    	if($rowverificacion['@respuesta']=='1')
    	{
    	   //header('Location:/cgi-bin/tbk_bp_pago.cgi');
           $oc = $this->TBK_ORDEN_COMPRA."/".$this->tts."/".$this->ttk."/".$codigo_suc;
           ?>
            <body onload="javascript:form01.submit();">
            
            <form  name="form01" method="POST" action="/cgi-bin/tbk_bp_pago.cgi">
                <input type="hidden"  name="TBK_TIPO_TRANSACCION" value="<?=$this->TBK_TIPO_TRANSACCION?>"/>
                <input type="hidden" name="TBK_ORDEN_COMPRA" value="<?=$oc ?>"/>
                <input type="hidden" name="TBK_ID_SESION" value="<?=$this->TBK_ID_SESION?>"/>
                <input type="hidden" name="TBK_URL_EXITO" size="40" value="<?=$this->TBK_URL_EXITO?>"/>
                <input type="hidden"  name="TBK_URL_FRACASO" size="40" value="<?=$this->TBK_URL_FRACASO?>"/>
                <input type="hidden" name="cantidad" size="3" value="<?=$this->cantidad?>"/>
                <input type="hidden" name="producto" size="3" value="<?=$this->producto?>"/>
                <input type="hidden"  name="dir_hotel" size="3" value="<?=$this->dir_hotel?>"/>
                <input type="hidden" name="email" value="<?=$this->email?>"/>
                <input type="hidden" name="fecha"  value="<?=$this->fecha?>"/>
                <input type="hidden" name="nombre" value="<?=$this->nombre?>"/>
                <input type="hidden" name="TBK_MONTO" value="<?=number_format($this->TBK_MONTO,0,'','').',00'?>"/>
                <input type="hidden" name="vendedor" value="<?=$this->vendedor?>"/>
                <input type="hidden" name="nacionalidad" value="<?=$this->nacionalidad?>"/>
            </form>
            </body>
           <?
        }
        
        mysql_free_result($query2); 
        mysql_free_result($parametrosSalida); 
        mysql_close();
    }
    
    
 
    public function Consulta_Errores($error)
    {
        switch($error)
        {
            case 1;
                   print("<script>alert('Seleccione tipo de Reserva');</script>");
            break;
            
            case 2;
                   print("<script>alert('La compra con WebPay es v�lida para una excursi�n');</script>");
            break;
            
            default;
            break;
        }
    }
    
}

?>


