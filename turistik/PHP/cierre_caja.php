<?php
    session_start();
    include("menus.php");
    require ("../functions/tkpass.php");
    $_Util=new Util;
    $Obj_Tkpass = new tkpass(); 
    
    $codigo_dist_sucursal = $Obj_Tkpass->Consulta_Codigo_dist_Sucursal($_SESSION['codigo_suc']);
    
    if ($codigo_dist_sucursal == '')
    {

      echo "<script language='javascript'>alert(\"No puede hacer cierre de caja\\nEsta sucursal no esta definida como distribuidor\");</script>"; 
       // print("<script>window.parent.close();</script>");
       $url = "Inicio.php";

        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<link href="../../css/menu_small.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
<script src="../../lib/multi-upload/jquery.MultiFile.js" type="text/javascript" language="javascript"></script>
</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">
  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr height="50"><td><br></td></tr>
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Cierre de Caja</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    
	<tr><td><br><br><br></td></tr>
  </table>
 
 <form name="form1" method="post" action="save_cierre_caja.php" enctype="multipart/form-data">
  <table width="800" border="0" cellpadding="0" cellspacing="0">  
	<tr>
      <td width="25%" class="celdalazul_peque">Deposito Efectivo: (*)</td>
      <td width="25%" class="celdalCELEST"><input name="monto" class="seleccionTurist_180"  size="18" maxlength="17" id="monto" type="text" onkeypress="return validarP3(event);"/></td>
      <td width="25%" class="celdalazul_peque">Numero Deposito: (*)</td>
      <td width="25%" class="celdalCELEST"><input name="deposito" class="seleccionTurist_180"  size="18" maxlength="17" id="deposito" type="text" /></td>
    </tr>
	<tr>
      <td  class="celdalazul_peque">Observacion :</td>
      <td  class="celdalCELEST"><textarea name="observacion" id="observacion" rows="2" cols="25"  ></textarea></td>
      <td  class="celdalazul_peque">Banco: (*)</td>
      <td  class="celdalCELEST">
        <select name="banco" id="banco" class="seleccionTurist_180" >
            <option value="0" selected="">Seleccione</option>
            <option value='Banco Santander' >Banco Santander</option>
            <option value='Banco Santander Banefe'>Banco Santander Banefe</option>
            <option value='Banco BBVA'>Banco BBVA</option>
            <option value='Banco BICE'>Banco BICE</option>
            <option value='Banco Internacional'>Banco Internacional</option>
            <option value='Banco Itau'>Banco Itau</option>
            <option value='Banco de Chile / Edwards-Citi'>Banco de Chile / Edwards-Citi</option>
            <option value='Corpbanca'>Corpbanca</option>
            <option value='Banco Credito e Inversiones'>Banco Credito e Inversiones</option>
            <option value='Banco del Desarrollo'>Banco del Desarrollo</option>
            <option value='Banco Estado'>Banco Estado</option>
            <option value='Banco Falabella'>Banco Falabella</option>
            <option value='Banco Security'>Banco Security</option>
            <option value='Scotiabank'>Scotiabank</option>
            <option value='Rabobank'>Rabobank</option>
            <option value='HSBC Bank'>HSBC Bank</option>
            <option value='Banco Ripley'>Banco Ripley</option>
            <option value='Banco Paris'>Banco Paris</option>
            <option value='Banco Consorcio'>Banco Consorcio</option>
        </select>
      </td>
    </tr> 
   	 <tr>
      <td   class="celdalazul_peque">Colilla Deposito: </td>
      <td  colspan="3"  class="celdalCELEST"><input type="file" name="archivo[]" class="multi" accept="doc|pdf|xlsm" maxlength="3"/></td>
    </tr> 
     <tr>
      <td  colspan="4"  class="celdalCELEST"><input type="submit" value="Aceptar" class="BotonTurist_Celest" /></td>
      <input type="hidden" value="<?=$codigo_dist_sucursal?>" name="codigo_dist_sucursal" />
    </tr> 
  </table>
</form>
  
</div>

</body>
</html>