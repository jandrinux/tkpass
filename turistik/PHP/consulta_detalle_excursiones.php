<?php
require ("../Clases/ClaseConexion.inc.php");
require ("../Clases/ClaseUtil.inc.php");

$_Util=new Util;
$fecha_desde = $_GET['fd'];
$fecha_hasta = $_GET['fh'];
$suc = $_GET['suc'];
$nombre_suc = $_GET['ns'];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="../estilos1.css" rel="stylesheet" type="text/css"/>

<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>

<head>
<style>
body {
    FONT-FAMILY: Arial, Helvetica, Sans-Serif;
}
</style>
</head>
<body>

  <table width="482" border="0" cellpadding="0" cellspacing="0">  
  	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>RESERVAS <?=$nombre_suc?></b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    

  </table>


<table align="center" style="border:1px solid #000; margin-top: 20px; color:#000; width:100%; background-color:#FFF; ">
<tr style="background:#CCC;">
<td class="celdalazul_peque" align="center" width="100" >Excursiones</td>
<td class="celdalazul_peque" align="center" width="40" >Cant. Reservas</td>
<td class="celdalazul_peque" align="center" width="60" >Total</td>

</tr>
<?php
$orden=1;
$i = 1;
      

				
    $miConexion= new ClaseConexion;
    $miConexion->conectar();
    $sql = "SELECT O.CODIGO_EO, O.NOMBRE_EO, SUM(R.PRECIO_RE) AS TOTAL 
            FROM RESERVA_EXCURSION R
            INNER JOIN EXCURSION_ORIGEN O ON O.CODIGO_EO = R.CODIGO_EO
            WHERE 
            R.FECHA_BOLETA_RE BETWEEN '".$fecha_desde."' AND '".$fecha_hasta."' AND
            R.SUCURSAL_VENTA = ".$suc." 
            AND R.BOLETA_RE IS NOT NULL
            AND R.COD_CIERRE IN (2,5) 
            AND R.PRECIO_RE > 0 
            AND R.ID_REEMBOLSO = 0
            AND R.CODIGO_EO NOT IN (6,54,55)
            GROUP BY R.CODIGO_EO 
			ORDER BY TOTAL";
    $query = $miConexion->EjecutaConsulta($sql);
    
    while($row=mysql_fetch_array($query)){    
        
 	if($orden%2==0){
	   $color='#D3D5FF';
	}else{
	   $color='#F1F2FF';
	}
    
        $sql2 = "SELECT R.CODIGO_RE
                FROM RESERVA_EXCURSION R
                WHERE 
                R.FECHA_BOLETA_RE BETWEEN '".$fecha_desde."' AND '".$fecha_hasta."' AND
                R.SUCURSAL_VENTA = ".$suc." AND
                R.CODIGO_EO = ".$row['CODIGO_EO']."
                AND R.BOLETA_RE IS NOT NULL
                AND R.COD_CIERRE IN (2,5) 
                AND R.PRECIO_RE > 0 
                AND R.ID_REEMBOLSO = 0
                AND R.CODIGO_EO NOT IN (6,54,55)";
        $query2 = $miConexion->EjecutaConsulta($sql2);
        $cantidad = mysql_num_rows($query2);
        
        

        echo "<tr style='background-color:".$color."'>"; 
        echo "<td class='celdalPasajeros'>".$row['NOMBRE_EO']."</td>";
        echo "<td class='celdalPasajeros'>".$cantidad."</td>";
        echo "<td class='celdalPasajeros'>".number_format($row['TOTAL'])."</td>";
        echo "</tr>";
        
        $i++;
        $orden++;
        
        $total = $row['TOTAL'] + $total;
    
    }
    mysql_free_result($query); 


?>
</table>
<table style="border:1px solid #000; margin-top: 20px; color:#000; width:100%; background-color:#FFF; ">
    <tr>  
        <td align="center" class="celdalPasajeros">Total: $<?=number_format($total)?></td>
    </tr>
</table>
</body>
</html>