<?php
    require_once "../lib/nusoap.php";
      
 
    $URL       = "www.test.com";
    $namespace = $URL . '?wsdl';
    $server = new soap_server();
    $server->configureWSDL('JP_ZoneList Version 1.0', $namespace);    
    
        
    $server->wsdl->addComplexType('Zone','attribute','struct','all','',array(
				'Code' => array('name' => 'Code','type' => 'xsd:string'),
				'Name' => array('name' => 'Name','type' => 'xsd:string'),
				'Parent' => array('name' => 'Parent','type' => 'xsd:string'),
				'IATA' => array('name' => 'IATA','type' => 'xsd:string'),
				'Hotels' => array('name' => 'Hotels','type' => 'xsd:string'),
    ));
    
    $server->wsdl->addComplexType('Zonas','complexType','array','','SOAP-ENC:Array',array(),
		array(array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:Zone[]')),'tns:Zone');
        
    $server->register('JP_ZoneListRS',array(),array('return' => 'tns:Zonas'));
    
    function JP_ZoneListRS() 
    {
      $elements[] = array(					
       "Code" => "1" ,
       "Name" => "SANTIAGO" ,
       "Parent" => "",
       "IATA" => 'CL' ,
       "Hotels" => false 
       );	
        $elements[] = array(					
       "Code" => "2" ,
       "Name" => "VIÑA/VALPO" ,
       "Parent" => "",
       "IATA" => 'CL' ,
       "Hotels" => false 
        
       );	
       return $elements;
    }

    $server->service($HTTP_RAW_POST_DATA);
?>
