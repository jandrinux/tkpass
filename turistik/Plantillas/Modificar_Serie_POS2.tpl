<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Modificar Serie</title>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#Layer3 {position:absolute;
	left:15px;
	top:21px;
	width:316px;
	height:46px;
	z-index:14;
}
#Layer2 {position:absolute;
	left:831px;
	top:53px;
	width:132px;
	height:116px;
	z-index:1;
}
-->
</style>
</head>
<script>
function ModificarEstado()
	{
			miPopup = window.open("../PHP/ListaEstadosSerie.php","listaestado","top=200,left=400,width=300,height=200,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
	
function ModificarModo()
	{
		miPopup = window.open("../PHP/ListaEstadosSerie_Modo.php","listaestado","top=200,left=400,width=300,height=200,scrollbars=yes,titlebar=no,location=no");
		miPopup.focus();
	}
function ModificarSerie()
	{
		if (document.form1.serie.value=='')
			{
				alert("Debe Ingresar una Serie para Modificar");
				document.form1.serie.focus();
				return;
			}
		if (document.form1.estado.value=='')
			{
				alert("Debe Ingresar el Estado para Modificar");
				document.form1.estado.focus();
				return;
			}
		document.form1.guardar.value=1;
		document.form1.submit();
			
	}
</script>
<body>
<form id="form1" name="form1" method="post" action="">
  <div class="TituloPlomo" id="Layer3">
    <table width="312" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="82" rowspan="4"><img src="../Imagenes/SAGEM.jpg" width="80" height="80" /></td>
        <td width="13" rowspan="4">&nbsp;</td>
        <td width="217"><p>&nbsp;</p></td>
      </tr>
      <tr>
        <td height="22" class="TituloPlomo"><strong>Modificar Serie  </strong><strong></strong></td>
      </tr>
      <tr>
        <td height="22" class="TituloFecha">{fecha}</td>
      </tr>
      <tr>
        <td height="22" class="TituloFecha">&nbsp;</td>
      </tr>
    </table>
  </div>
  <p>&nbsp;</p>
 <p>&nbsp;</p>
 <p>&nbsp;</p>
<div id="segundodiv" style="position:absolute; left:14px; top:114px; width:448px; height:47px; z-index:3; border:0pt black solid;">
  <table width="439" border="0" cellspacing="0">
    
    <tr>
      <td width="165" class="celdalazul">Serie</td>
      <td width="270" class="celdalCELEST"><label>
        <input name="serie" type="text" class="seleccionTurist_250" id="serie" maxlength="16" {SERIE} readonly="" />
      </label></td>
    </tr>
    <tr>
      <td class="celdalazul">Caracteristica / Patente </td>
      <td class="celdalCELEST"><input name="patente" type="text" class="seleccionTurist_250" id="patente" {PATENTE} /></td>
    </tr>
    <tr>
      <td class="celdalazul">Modo de Operaci&oacute;n </td>
      <td class="celdalCELEST"><input name="DESCRIPCION_MODO" type="text" readonly="" class="seleccionTurist_180" {MODO} /> 
        &nbsp;<img src="../Imagenes/modify.gif" width="16" height="16" alt="Modificar Modo" onclick="ModificarModo()" style="cursor:hand"/></td>
    </tr>
    <tr>
      <td class="celdalazul">Estado</td>
      <td class="celdalCELEST"><label>
        <input name="estado" type="text" readonly="" class="seleccionTurist_180" id="estado" {ESTADO} />
        <img src="../Imagenes/modify.gif" width="16" height="16" alt="Modificar Estado" onclick="ModificarEstado()" style="cursor:hand"/></label></td>
    </tr>
  </table>
  <div id="tercerdiv" style="position:absolute; left:317px; top:97px; width:219px; height:23px; z-index:3; border:0pt black solid;">
   <input name="guardar" type="hidden" value="0" />
   <input name="CODIGOESTADO" type="hidden" value="codnulo" />
   <input name="CODIGOMODO" type="hidden" value="codnulo" />
   
    <input name="button" type="button" class="BotonTurist_Celest" value="Modificar"  onclick="ModificarSerie()"/>
  </div>
  <p>&nbsp;</p>
</div>
</form>
</body>
</html>
