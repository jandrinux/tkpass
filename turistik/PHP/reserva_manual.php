<?
//ini_set('display_errors', 1);
session_start();
date_default_timezone_set("America/Santiago");
require ("../Clases/ClaseConexion.inc.php");
require ("../functions/tkpass.php");
require ("SII/class.boletaGen.php");
require ("../Clases/class.boletas_manager.php");

$miConexion= new ClaseConexion;
$Obj_Tkpass = new tkpass();

$excursion_array = array();
$cant_pax_adt = 0;
$cant_pax_chd = 0;
$precio_real = 0;

$tipo_pax = $_POST['tipo_pax'];
$nacionalidad = $_POST['nacionalidad'];
$fecha_format = $_POST['fecha_format'];
$codigo_suc = $_POST['codigo_suc'];
$monto = $_POST['monto'];
$monto_child = $_POST['monto_child'];
$fop = $_POST['fop'];
$boletaTTS = "";
$cantidad = count($_POST['check']);


$miConexion->Conectar();
$query3=$miConexion->EjecutaSP("Generar_Voucher","@voucher"); 	
$parametrosSalida_v=$miConexion->ObtenerResultadoSP("@voucher");
$row_voucher= mysql_fetch_assoc($parametrosSalida_v);
$voucher = $row_voucher['@voucher'];
$voucher_generado = $voucher."/";


if ($tipo_pax == 'ADT')
{
    $precio = $monto;
    $cant_pax_adt = $cantidad;
}
else
{
    $precio = $monto_child;
    $cant_pax_chd = $cantidad;
}



    $Manager = new Boletas_Manager($precio_real, $excursion_array);
              
    $items =  array();
    $line = 1;
    $tipo_boleta = false;  

    ## GENERACION DE BOLETA EXENTA
    $adultos = $cant_pax_adt;
    $ninos = $cant_pax_chd;
    $cant_total = $cantidad;

    $miConexion->Conectar();
    $SQL = "SELECT NOMBRE_EO, PRECIO_EO, VALOR_EXENTO, VALOR_CHILD, MANAGER_TTS, MANAGER_CHD FROM EXCURSION_ORIGEN WHERE CODIGO_EO = 6";
    $query2=$miConexion->EjecutaConsulta($SQL);
    while($row = mysql_fetch_array($query2))
    {

         $codigoManagerADT = $row['MANAGER_TTS'];
         $codigoManagerCHD = $row['MANAGER_CHD'];
         $precioADT = $row['PRECIO_EO'];
         $precioCHD = $row['VALOR_CHILD'];
         $nombreEx = $row['NOMBRE_EO'];

         if ($adultos>0 && $precioADT > 0)
         {

            //item excursion solo adulto
            $i = new Item;
            $i->Linea=$line;
            $i->codigoMannager=$codigoManagerADT;
            $i->Descripcion=$nombreEx. " ADT";
            $i->Cantidad=$adultos;
            $i->PrecioUnitario=(int)$precioADT;
            $i->Descuento=0;
            $items[] = $i ;
            $line++;
            
         }
         if ($ninos>0 && $precioCHD > 0)
         {
            //item excursion solo chd
            $i = new Item;
            $i->Linea=$line;
            $i->codigoMannager= $codigoManagerCHD;
            $i->Descripcion=$nombreEx." CHD";
            $i->Cantidad=$ninos;
            $i->PrecioUnitario=(int)$precioCHD;
            $i->Descuento=0;
            $items[] = $i ;
            $line++;           

         } 

    }
     
    $g = new BoletaGen;
    $retorno =  $g->generar($tipo_boleta,$items, 0);
    $url_exento = $retorno->Message;
    $boleta_exenta = $retorno->numero;


	$descuento = 0;
    $excursion_array[] = 6;
    $cantidadPax[] = $cant_total;
    $pasajeros_cant[6] = array("adt"=>$adultos, "chd"=>$ninos);

    // INGRESAR BOLETAS MANAGER
    $Manager->Ingresar_Cabecera($boleta_exenta,$descuento,$fop,"E",$excursion_array,$cantidadPax, $pasajeros_cant);
    
    $url_exento = base64_encode($url_exento);
    $url_afecto = "";
    $link = "boarding_pass.php?v=$voucher_generado&be=$url_exento&ba=";


	for($i=0; $i<count($_POST['check']); $i++)
	{
	     $miConexion->Conectar();
	     $miConexion->EjecutaConsulta(" UPDATE DETALLE_STOCK SET ESTADO = 1  WHERE SERIAL = ".$_POST['check'][$i]." ");
	     
	     $miConexion->EjecutaSP("Ingresar_Reserva_Excursion_Manual_STK"," 6,'".$fecha_format."', '1', '', '1', '1','228201000', 'VENTA', 'MANUAL', '','".$nacionalidad."','".$_SESSION['usuario']."','1','".$precio."','VENTA STK','','".$_POST['check'][$i]."','' ,'".date('H:m:s')."', '".$tipo_pax."', 0, 0, '".$codigo_suc."', '".$fop."', '".$codigo_suc."', '".$voucher."','".$boleta_exenta."', @respuesta,@pmessage"); 	
	}


	echo '<script type="text/javascript">';
	echo 'window.location.href="'.$link.'";';
	echo '</script>';


?>