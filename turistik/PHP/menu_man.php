<?php
ini_set('display_errors', 0);
include("../Clases/ClaseConexion.inc.php");
include("../Clases/menu2.php");
$menu = new menu2();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:::Menu TkPass:::</title>
<link href="../../css/hoja.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Alef:400,700' rel='stylesheet' type='text/css'>
<link href="css/blitzer/jquery-ui-1.10.3.custom.css" rel="stylesheet">
<script src="../../js/jquery-1.9.1.js"></script>
<script src="../../js/jquery-ui-1.10.3.custom.js"></script>
</head>
<body>
<script type="text/javascript">
    	function modificar(id)
		{
			$("#titulo").text("Modificacion del Menu");
			$.getJSON("funmenu.php?op=res&id=" + id,function(data){ 
			
				$("#nombre").val(data['descripcion']);
				$("#nivel").val(data['Nivel']);
				$("#url").val(data['url']);
				$("#id").val(data['id']);
				$("#parentid").val(data['parentId']);
				$("#op").val('mod');
			});
	 	
		}
    
    </script>
<div id="container">
  <fieldset>
    <legend id="titulo">Creacion de Menu</legend>
    <form action="funmenu.php" method="post" name="form1" id="form1">
      <div id ="formulario">
        <label>Nombre del Menu</label>
        <input type="text" name="nombre" id="nombre" />
        <label>Nivel de Menu</label>
        <select name="parentid" id="parentid">
          <?php
			$datos = $menu->Where("parentid = 0 order by descripcion");
			$dat = new menu2();
			if($datos.count()==0)
			{
				echo  "<option value=\"0\">:::Sin Padre:::</option>";
			}
			foreach($datos as $dat)
			{
				echo  "<option value=\"".$dat->id."\">".$dat->descripcion."</option>";
				
				foreach($menu->Where("parentid =".$dat->id."  order by descripcion" ) as $dat2)
				{
					echo  "<option value=\"".$dat2->id."\">[".$dat->descripcion."]->".$dat2->descripcion."</option>";
					
						foreach($menu->Where("parentid =".$dat2->id." order by descripcion") as $dat3)
						{
							echo  "<option value=\"".$dat3->id."\">[".$dat->descripcion."]->[".$dat2->descripcion."]->".$dat3->descripcion."</option>";
						}
				}
			}
		?>
        </select>
        <label>Nivel de Usuario [1,2,3,4]</label>
        <input type="text" name="nivel" id="nivel" />
        <label>URL LINK</label>
        <input type="text" name="url" id="url" />
        <input name="op" type="hidden" id="op" value="save" />
        <input name="id" type="hidden" id="id" value="0" />
        <br />
        <br />
        <input id="boton" name="Enviar" type="submit" value="GENERAR MENU"/>
      </div>
    </form>
  </fieldset>
</div>
<br />
<fieldset style="width:380px; margin-left:auto; margin-right:auto;">
  <legend>Listado</legend>
  <style>
 li { list-style:none;
text-decoration:none;}
</style>
  <?php
	$datos = array();
	$datos = $dat->Where(" parentid=0 order by descripcion");
foreach($datos as $a)
{?>
  <ul>
    <li><a href="#" onclick="javascript:modificar('<?=$a->id?>'); return false"><span style="font-weight:bold;">
      <?=$a->descripcion?>
      [
      <?=$a->Nivel?>
      ]</span></a>
      <?php
	$datos2 = array();
	$datos2 = $dat->Where(" parentid=".$a->id." order by descripcion");

	if(count($datos) > 0) {
		echo "<ul>";
		foreach($datos2 as $a2)
		{
				echo "<li><a href=\"#\" onclick=\"javascript:modificar('".$a2->id."'); return false\">└ ".$a2->descripcion." [".$a2->Nivel."]</a></li>";
				
				$datos3 = array();
				$datos3 = $dat->Where(" parentid=".$a2->id." order by descripcion");
	
				if(count($datos) > 0) {
					echo "<ul>";
					foreach($datos3 as $a3)
					{
						echo "<li><a href=\"#\" onclick=\"javascript:modificar('".$a3->id."'); return false\">└ ".$a3->descripcion." [".$a3->Nivel."]</a></li>";
					}
					echo "</ul>";
				}
		}
		echo "</ul>";
	}
	?>
    </li>
  </ul>
  <?php
}
?>
</fieldset>
</body>
</html>