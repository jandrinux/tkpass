<?php


include( "../Clases/excelclass.php" );	
require ("../Clases/ClaseConexion.inc.php");
require ("../Clases/ClaseUtil.inc.php");

$excel = new ExcelGen("Detalle_Reporte_Comisiones_Detalle","prueba");
$_Util=new Util;
$miConexion = new ClaseConexion;

$excel->NewLine();
$excel->WriteText("MODALIDAD");
$excel->WriteText("TIPO");
$excel->WriteText("FOLIO TARJETA");
$excel->WriteText("FOLIO TARJETA ANTERIOR");
$excel->WriteText("PRECIO");
$excel->WriteText("DISTRIBUIDOR");
$excel->WriteText("VENDEDOR");
$excel->WriteText("PROMOTOR");
$excel->WriteText("INGRESO TURISTIK");
$excel->WriteText("COMISION DISTRIBUIDOR");
$excel->WriteText("COMISION VENDEDOR");
$excel->WriteText("COMISION PROMOTOR");
$excel->WriteText("FECHA");

$excel->NewLine();
//////////////////////////////////////QUE TIPO DE USUARIO ES///////////////////////////////////////////////
$miConexion->Conectar();
$queryPerfil=$miConexion->EjecutaSP("Consulta_Perfil_Usuario","'".$_SESSION['usuario']."',@tipo");
$parametrosSalidaPerfil=$miConexion->ObtenerResultadoSP("@tipo");
$rowPerfil= mysql_fetch_assoc($parametrosSalidaPerfil);
///////////////////////////////////////////////////////////////////////////////////////////////////////////

$miConexion->Conectar();
$queryComisionesDetalle=$miConexion->EjecutaSP("Comisiones_Detalle_Excel","1");
while ($rowDetalleComisionGeneral = mysql_fetch_assoc($queryComisionesDetalle))
	{		 
	  $excel->NewLine();
	  $excel->WriteText("SIN TARJETA PREVIA");
	   $excel->WriteText("");
	  $excel->WriteText($rowDetalleComisionGeneral['FOLIO_TAR_COM_TEMP']);
	  $excel->WriteText("");
	  $PRECION=number_format($rowDetalleComisionGeneral['PRECIO_REAL_COM_TEMP'],2,",",".");
	  $PRECION_PESO="$".$PRECION;
	  $excel->WriteText($PRECION_PESO);
	  
	  $miConexion->Conectar();
	  $queryDistribuidor=$miConexion->EjecutaSP("Consulta_Distribuidor_Codigo",$rowDetalleComisionGeneral['UDISTRIBUIDOR_COM_TEMP']);
      $rowDistribuidor= mysql_fetch_assoc($queryDistribuidor);
	  $excel->WriteText($rowDistribuidor['NOMBRE_FANTASIA_DIST']);
	  
	  $miConexion->Conectar();
	  $queryVendedor=$miConexion->EjecutaSP("Consulta_Usuario_Codigo",$rowDetalleComisionGeneral['UVENDEDOR_COM_TEMP']);	
	  $rowVendedor= mysql_fetch_assoc($queryVendedor);
      $excel->WriteText("".$rowVendedor['NOMBRE_USUA']." ".$rowVendedor['APELLIDO_USUA']."");
	 $miConexion->Conectar();
	 $queryPromotor=$miConexion->EjecutaSP("Consulta_Promotor_porCodigo",$rowDetalleComisionGeneral['UPROMOTOR_COM_TEMP']);
	 //$queryPromotor=$miConexion->EjecutaSP("Consulta_Usuario_Codigo","".$_GET['CODIGOPROMOTORREAL']."");	
      $rowPromotor= mysql_fetch_assoc($queryPromotor);
	  $excel->WriteText("".$rowPromotor['NOMBRE_USUA']." ".$rowPromotor['APELLIDO_USUA']."");
	  
	  if ($PERFIL=='3')
		{
			$excel->WriteText("");
			$excel->WriteText("");
		}
	 else
		{
			$INGRESO=number_format($rowDetalleComisionGeneral['INGRESO'],2,",",".");
			$INGRESO_PESO="$".$INGRESO;
			$excel->WriteText($INGRESO_PESO);
			$COMISIONDISTRIBUIDOR=number_format($rowDetalleComisionGeneral['I_COMISION_D_COM_TEMP'],2,",",".");
			$COMISIONDISTRIBUIDOR_PESO="$".$COMISIONDISTRIBUIDOR;
			$excel->WriteText($COMISIONDISTRIBUIDOR_PESO);
		}
	 $COMISIONVENDEDOR=number_format($rowDetalleComisionGeneral['I_COMISION_V_COM_TEMP'],2,",",".");
	 $COMISIONVENDEDOR_PESO="$".$COMISIONVENDEDOR;
	 $excel->WriteText($COMISIONVENDEDOR_PESO);
	 if ($PERFIL=='3')
	 	{
			$excel->WriteText("");
		}
	 else
		{
			$COMISIONPROMOTOR=number_format($rowDetalleComisionGeneral['I_COMISION_P_COM_TEMP'],2,",",".");
			$COMISIONPROMOTOR_PESO="$".$COMISIONPROMOTOR;
			$excel->WriteText($COMISIONPROMOTOR_PESO);
		}
	 $Fecha=$_Util->FormatearFecha($rowDetalleComisionGeneral['FECHA_COM_TEMP']);	
	 $excel->WriteText($Fecha);
}

$miConexion->Conectar();
$queryComisionesDetalle2=$miConexion->EjecutaSP("Comisiones_Detalle_Excel","2");
while ($rowDetalleComisionGeneral2 = mysql_fetch_assoc($queryComisionesDetalle2))
	{
		$excel->NewLine();
		$excel->WriteText("CON TARJETA PREVIA");
		if ($rowDetalleComisionGeneral2['VENTA_ANTERIOR']==2)
			{
				$excel->WriteText("ACTUAL");
			}
		else
			{
				$excel->WriteText("ANTERIOR");
			}
		$excel->WriteText($rowDetalleComisionGeneral2['FOLIO_TAR_COM_TEMP']);
		$excel->WriteText($rowDetalleComisionGeneral2['FOLIO_TAR_ANTERIOR_COM_TEMP']);
		$PRECION=number_format($rowDetalleComisionGeneral2['PRECIO_II_COM_TEMP'],2,",",".");
		$PRECION_PESO="$".$PRECION;
		$excel->WriteText($PRECION_PESO);
		$miConexion->Conectar();
	  $queryDistribuidor=$miConexion->EjecutaSP("Consulta_Distribuidor_Codigo",$rowDetalleComisionGeneral2['UDISTRIBUIDOR_COM_TEMP']);
      $rowDistribuidor= mysql_fetch_assoc($queryDistribuidor);
	  $excel->WriteText($rowDistribuidor['NOMBRE_FANTASIA_DIST']);
	  
	  $miConexion->Conectar();
	  $queryVendedor=$miConexion->EjecutaSP("Consulta_Usuario_Codigo",$rowDetalleComisionGeneral2['UVENDEDOR_COM_TEMP']);	
	  $rowVendedor= mysql_fetch_assoc($queryVendedor);
      $excel->WriteText("".$rowVendedor['NOMBRE_USUA']." ".$rowVendedor['APELLIDO_USUA']."");
	 $miConexion->Conectar();
	 $queryPromotor=$miConexion->EjecutaSP("Consulta_Promotor_porCodigo",$rowDetalleComisionGeneral2['UPROMOTOR_COM_TEMP']);
	 //$queryPromotor=$miConexion->EjecutaSP("Consulta_Usuario_Codigo","".$_GET['CODIGOPROMOTORREAL']."");	
      $rowPromotor= mysql_fetch_assoc($queryPromotor);
	  $excel->WriteText("".$rowPromotor['NOMBRE_USUA']." ".$rowPromotor['APELLIDO_USUA']."");
	   if ($PERFIL=='3')
	   {
	   	$excel->WriteText("");
		$excel->WriteText("");
	   }
	   else
	   {
	   	$INGRESO=number_format($rowDetalleComisionGeneral2['INGRESO'],2,",",".");
		$INGRESO_PESO="$".$INGRESO;
		$excel->WriteText($INGRESO_PESO);
		
		$COMISIONDISTRIBUIDOR=number_format($rowDetalleComisionGeneral2['II_COMISION_D_COM_TEMP'],2,",",".");
			$COMISIONDISTRIBUIDOR_PESO="$".$COMISIONDISTRIBUIDOR;
			$excel->WriteText($COMISIONDISTRIBUIDOR_PESO);
	   }
	   if ($PERFIL=='3')
	   	{
			$excel->WriteText("");
		}
		else
		{
			$COMISIONPROMOTOR=number_format($rowDetalleComisionGeneral2['II_COMISION_P_COM_TEMP'],2,",",".");
			$COMISIONPROMOTOR_PESO="$".$COMISIONPROMOTOR;
			$excel->WriteText($COMISIONPROMOTOR_PESO);
		}
	   $COMISIONVENDEDOR=number_format($rowDetalleComisionGeneral2['II_COMISION_V_COM_TEMP'],2,",",".");
			$COMISIONVENDEDOR_PESO="$".$COMISIONVENDEDOR;
			$excel->WriteText($COMISIONVENDEDOR_PESO);
		
			$Fecha=$_Util->FormatearFecha($rowDetalleComisionGeneral2['FECHA_COM_TEMP']);	
			$excel->WriteText($Fecha);
	}

  $excel->SendFile();

?>


