<?

require ("/var/www/tkpass.cl/www/turistik/Clases/ClaseConexion.inc.php");
require ("/var/www/tkpass.cl/www/turistik/Clases/ClaseUtil.inc.php");
require ("/var/www/tkpass.cl/www/lib/PHPMailer/class.phpmailer.php");
require ("/var/www/tkpass.cl/www/lib/PHPMailer/class.smtp.php");


    
    ## busca correos automaticos con estado de correo = 1
    $miConexion = new ClaseConexion;
    $miConexion->conectar();
    $query2=$miConexion->EjecutaSP("Consulta_correos_automaticos"," '".date('H:i:s')."', 1 ");
    
    while ($row2 = mysql_fetch_assoc($query2))
    {
        $fecha = date('Y-m-d');
        $correo =  $row2['CORREO_AUTO'];
        $excursion = $row2['NEMOTECNICO_PROV'];
        $cod_eo = $row2['CODIGO_EO'];
        
        envio_correo($correo, $excursion, $fecha, $cod_eo, $correo, 1 );
 
    }
    mysql_close();
    
 
    ## busca correos automaticos con hora preaviso = 1 con envio en dia anterior a preaviso
    $miConexion = new ClaseConexion;
    $miConexion->conectar();
    $query2=$miConexion->EjecutaSP("Consulta_correos_automaticos"," '".date('H:i:s')."', 2 ");
    
    while ($row2 = mysql_fetch_assoc($query2))
    {
        
        $fecha_hoy = date('Y-m-j');
        $fecha_2 = strtotime ( '+1 day' , strtotime ( $fecha_hoy ) ) ;
        $fecha_2 = date ( 'Y-m-j' , $fecha_2 );
        
        $correo =  $row2['CORREO_AUTO'];
        $excursion = $row2['NEMOTECNICO_PROV'];
        $cod_eo = $row2['CODIGO_EO'];
        
         envio_correo($correo, $excursion, $fecha_2, $cod_eo, $correo, 2 );
        
    }
    mysql_close();
    
    
    ## busca correos automaticos con hora preaviso = 1 del mismo dia
    $miConexion = new ClaseConexion;
    $miConexion->conectar();
    $query2=$miConexion->EjecutaSP("Consulta_correos_automaticos"," '".date('H:i:s')."', 3 ");
    
    while ($row2 = mysql_fetch_assoc($query2))
    {
        
        $fecha_2 = date('Y-m-j');
        
        $correo =  $row2['CORREO_AUTO'];
        $excursion = $row2['NEMOTECNICO_PROV'];
        $cod_eo = $row2['CODIGO_EO'];
        
         envio_correo($correo, $excursion, $fecha_2, $cod_eo, $correo, 2 );
        
    }
    mysql_close();
    
    
    function envio_correo($correo, $excursion, $fecha, $cod_eo, $correo, $accion)
    {
        
        $correo = explode(";",$correo);
        

        $miConexion = new ClaseConexion;
        $miConexion->conectar();
        $query3=$miConexion->EjecutaSP("Consulta_registros_correo_automatico"," '".$fecha."', '".$cod_eo."', ".$accion.", @reservas, @adt, @chd ");
        $Salida=$miConexion->ObtenerResultadoSP("@reservas, @adt, @chd");
        $row3= mysql_fetch_assoc($Salida);
        
        
        $reservas = $row3['@reservas'];
        $adt = $row3['@adt'];
        $chd = $row3['@chd'];
        $cantidad = $adt + $chd;
        /*
        $esp = $row3['@esp'];
        $eng = $row3['@eng'];
        $por = $row3['@por'];
        s
        Idiomas de Pasajeros:<br>
        -	Espa&ntilde;ol: '.$esp.'<br>
        -	Ingl&eacute;s: '.$eng.'<br>
        -	Portugu&eacute;s: '.$por.'<br><br><br>
        
        
        */
        $_Util=new Util;
        $fecha = $_Util->FormatearFecha($fecha);
        

        if ($accion==1 && $cantidad>0)
        {
            $miConexion->Conectar();
            $query4=$miConexion->EjecutaConsulta(" UPDATE EXCURSION_ORIGEN SET ESTADO_CORREO = 0 WHERE CODIGO_EO = ".$cod_eo." ");
            
            $asunto="Detalle de Pasajeros Turistik ".$fecha;
            
            $cuerpo='<head>
            <meta http-equiv="content-type" content="text/html; charset=iso-8899-1">
            
            <style type="text/css">
            body {
              font-family: Arial;
              font-size: 11px;
            }
            </style>
                                    
            </head>
                                    
            <body>
   
                Estimados Se&ntilde;ores,<br>
                A continuaci&oacute;n el detalle de pasajeros con los cuales asistiremos el '.$fecha.' al servicio '.$excursion.'<br><br>
    
                                    
                Cantidad de Reservas: '.$reservas.'<br>
                
                Cantidad Pasajeros:<br>
                -	Adultos: '.$adt.'<br>
                -	Ni&ntilde;os: '.$chd.'<br><br>

				Atte,<br>
				Equipo Turistik
    
            </body>
            </html>';
            
        }
        
        if ($accion==2 && $cantidad>0)
        {
            $miConexion->Conectar();
            $query4=$miConexion->EjecutaConsulta(" UPDATE EXCURSION_ORIGEN SET ESTADO_CORREO_PRE = 0 WHERE CODIGO_EO = ".$cod_eo." ");
            
            $asunto="Detalle preliminar de pasajeros ".$fecha;
            
            $cuerpo='<head>
            <meta http-equiv="content-type" content="text/html; charset=iso-8899-1">
            
            <style type="text/css">
            body {
              font-family: Arial;
              font-size: 11px;
            }
            </style>
                                    
            </head>
                                    
            <body>
            
                Estimados Se&ntilde;ores,<br>
                A continuaci&oacute;n detalle preliminar de pasajeros con los cuales asistiremos el '.$fecha.' al servicio '.$excursion.'<br><br>
    
                                    
                Cantidad de Reservas: '.$reservas.'<br>
                
                Cantidad Pasajeros:<br>
                -	Adultos: '.$adt.'<br>
                -	Ni&ntilde;os: '.$chd.'<br><br>
				
				
				Atte,<br>
				Equipo Turistik<br><br>
                
                Por favor, NO responda a este mensaje, es un env�o autom&aacute;tico.
    
            </body>
            </html>';
            
            
        }
        
        if (($accion==1 || $accion==2) && $cantidad>0)
        {

            //Crear una instancia de PHPMailer
            $mail = new PHPMailer();
    
            $mail->IsSMTP();
            $mail->SMTPDebug  = 0;
            $mail->Host       = 'smtp.gmail.com';
            $mail->Port       = 465;
            $mail->SMTPSecure = 'ssl';
            $mail->SMTPAuth   = true;
            $mail->Username   = "no-reply@turistiktours.cl";
            $mail->Password   = "tkcl.adm15";
            $mail->SetFrom('no-reply@turistiktours.cl', 'Turistik S.A');
            $mail->AddBCC('no-reply@turistiktours.cl');

            foreach($correo as $email)
            {
               $mail->AddAddress($email);
            }
            
            $mail->Subject = 'Detalle de Pasajeros';
            $mail->MsgHTML($cuerpo);
            
            //Enviamos el correo
            if(!$mail->Send()) {
              echo "Error: " . $mail->ErrorInfo;
            } else {
              echo "Enviado!";
            }
            
        }
            
 
    }
?>