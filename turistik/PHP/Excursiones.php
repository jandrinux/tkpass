<?php
include("menus.php");
/*
if ($_SESSION['CAJA_ABIERTA']!=true) {
    $url =$_SERVER['SERVER_NAME']."/turistik/PHP/Inicio.php";
    ?>
    <script>
        window.location.href = 'Inicio.php';
    </script>
    <?
    exit;
}

*/
require ("../functions/tkpass.php");
$_Util=new Util;
$fecha = date('d/m/Y');
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<title>Excursiones</title>

 <meta charset="utf-8" />
    <link rel="stylesheet" href="../../css/plantilla_matriz.css" type="text/css" media="all">
    <link href="../estilos1.css" rel="stylesheet" type="text/css">
    <link href="../../css/noticias.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
    <link type="text/css" href="../../css/style_leanmodal.css" rel="stylesheet" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
    <script type="text/javascript" src="../../js/ui.datepicker.js"></script>
    <script type="text/javascript" src="../../js/jquery.timer.js"></script>
    <script type="text/javascript" src="../../js/jquery.leanModal.min.js"></script>

    <script type="text/javascript">

    	$(function() {
    		$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });		
    	});
            
        addEventListener('keydown',function(event){

            if (event.keyCode == 13) {
                consulta_disponibilidad();
            }
            return false;
        });

        function consulta_disponibilidad()
        {

            var v = $("#boarding").val();

                if( v != '')
                {
            
                    boton_qr.style.display = 'none';
                    $("#loading").removeClass().html('<p style="font-size:16px; margin-left:250px;">Cargando...<img src="../Imagenes/loading_red.gif" width="40px" height="40px"/></p>').fadeIn(2000);
                                
                    
                    var timeoutId = setTimeout("",5000);

                    var v = $("#boarding").val();

                    var parametros = {
                            "v" : v
                    };


                    var valido = $.ajax({
                        type: "POST",
                        url: "consulta_disp.php",
                        data: parametros,
                        async: false,
                        success : function(data) {
                           $("#boarding").val(""); 
                        }
                    }).responseText;


                    if( isNaN(valido) ) {

                        if(valido=="no"){
                            alert( "No existe disponibilidad");
                            loading.style.display = 'none';
                            boton_qr.style.display = '';
                            return false;
                        }

                        if(valido=="fecha"){
                            alert("Boarding Pass con fecha invalida");
                            loading.style.display = 'none';
                            boton_qr.style.display = '';
                            return false;
                        }
                    }
                    else
                    {
                            var url = 'Excursiones_qr.php?v='+v+'&c='+valido;
                            window.location.href = url; 
                           // window.open(url, '_blank');
                           //alert("ok");
                        
                    }

                    $(".modal_close").click();
                    return false;
                }
                else
                {
                    alert("Ingrese un Boarding Pass que sea valido.");
                    return false;
                }
            
        }
        
    </script>

    <script type="text/javascript">
    
    var carro = 0;
    var cod_tusua = <?=$_SESSION['cod_tusua'];?>;
    var flag = 0;
    var TIEMPO = 60;
    
    $( document ).ready(function() {
        $("#fecha").datepicker();
        checking();
        if (cod_tusua == 4)
        {
            $("#filtro").hide();
            $("#titulo_tiempo").hide();
        }

    });
    
    function refresh()
    {
        location.reload(true);
    }


    function checking()
    {
        if (cod_tusua == "")
        {
            alert("Tiempo de Session Expirado");
            window.location.href = 'logout.php';
            return false;
        }
        
        
        if (revisa_carro() == true)
        {

            $('#principal_1').hide(); //faq
            $('#principal').show();  //matriz
            $('#navegacion').show();  //matriz 
            
            tipo_matriz();
            flag = 1;
            timer.play();
            
            return false;
        }

    }
    
    
    
    function checking2()
    {
        if (revisa_carro() == false)
        {
            
             // no hay pasajeros en carrito
             if (flag == 0)
             {
                $('#principal_1').show(); //faq
                $('#principal').hide();  //matriz
                $('#navegacion').hide(); 
                limpia();  
                $('#counter').hide();
                timer.stop();

                return false;
             }
             else
             {
                
                flag = 0;
               
                count = 0;
                $('#counter').html(count);
                timer.stop();
                
                $('#principal_1').show(); //faq
                $('#principal').hide();  //matriz
                $('#navegacion').hide(); 
                limpia(); 
               
             }
            
        }
        else
        {
            

            $('#principal_1').hide(); //faq
            $('#principal').show();  //matriz
            $('#navegacion').show(); 
            tipo_matriz();

        }

    }
    
    function revisa_carro()
    {
        
        if (cod_tusua == 4)
        {
            return true;
        }
        
        var dat = $.ajax({
                type: "POST",
                url: "cuenta_carrito.php",
                async: false,
                success : function(data) {
                   
                }
            }).responseText;
            if(dat==0){
                return false;
            }
            else
            {
                return true;
            };

    }
    
    function cerrar()
    {
        window.close();
    }

    var auto_refresh = setInterval( function (){
        $('#ReloadThis').load('estado_carrito.php');
        checking();
    }, 2000); 
    
    var count = 0,
    timer = $.timer(function() {
        count++;
        $('#counter').html(count);
        if (count > TIEMPO)
        {
        	checking2();
        }
        
    });
	timer.set({ time : 1000, autostart : false });
        
        
    function submit_faq()
    {

        var adt = $("#adt").val();
        var chd = $("#chd").val();          
        var cantidad = parseInt(adt) + parseInt(chd);
        var nac = $("#nac").val();
        var estadia = $("#estadia").val();
        var contador = $("#cant").val();
        var select = 0;
        var exc = "";
        
        
        if (cantidad > 0)
        {

            for (i=0; i<contador; i++) 
            {
                var v = $("#check"+i);

                if (v.attr('checked')) {
                    exc = v.val()+","+exc;
                }
            }
            
            exc = exc.substring(0, exc.length - 1);
            
            if (exc != '')
            {
                if (nac != 0 && estadia != 0)
                {
                    if (estadia > 6) estadia = 6;
                    
                    
                    $.post("guarda_faq.php",{adt:adt, chd:chd, estadia:estadia, exc:exc, nac:nac}, function(data)
                    {
                        
                        if (data=="no")
                        {
                            alert("Hubo un problema al guardar, intente nuevamente");
                            return false;
                        }
                        else
                        {
                            $("#estadia_glb").val(estadia);
                            $('#cadena_ex').val(exc);
                            $('#cantidad_pax').val(cantidad);
                            tipo_matriz();
                            $('#principal').show(); //matriz
                            $('#principal_1').hide();  //faq
                            $('#navegacion').show(); 
                            $('#counter').show();
                            count = 0;
                            $('#counter').html(count);
                            timer.play();
    
                        }
                    })
                }
                else
                {
                    if (nac == 0)
                    {
                        alert("Seleccione Nacionalidad");
                        return false;
                    }
                    if (estadia == 0)
                    {
                        alert("Seleccione d�as adicionales en Santiago");
                        return false;
                    }
                    
                }
            }
            else
            {
                alert("Seleccione excursiones de intereses");
                return false;
            }     
        }
        else
        {
            alert("Seleccione la cantidad de Adultos y Ni�os");
        }  
        

    }

    function Tomar(dia, fecha, exc, acc)
    {
        if (acc == 1)
        {
            var n = $('#nac').val();
            var url = 'Tomar_Reserva.php?df='+dia+'&f='+fecha+'&ex='+exc+'&n='+n;
        }
        if (acc == 2)
        {
            var url = 'encuesta_express.php?df='+dia+'&f='+fecha+'&ex='+exc+'&n='+n;
        }
        window.open(url,'_blank');
    }
    
    function cambia_grupo(g)
    {
        $('#cadena_ex').val(g);
        tipo_matriz();
    }
    
    function tipo_matriz()
    {

        if (cod_tusua == 3)
        {
           $('#principal').load('matriz_excursiones_ej.php?d='+$("#estadia_glb").val()+'&g='+$('#cadena_ex').val()+'&c='+$('#cantidad_pax').val()+'&f='+$("#fecha").val());
        }
        else
        {
            if (cod_tusua == 4)
            {
               $('#principal').load('matriz_excursiones_ej.php?d=00&g=0&c='+<?=$_COOKIE['cantidad']?>+'&f='+$("#fecha").val());
            }   
            else
            {
               $('#principal').load('matriz_excursiones.php?d='+$("#estadia_glb").val()+'&g='+$('#cadena_ex').val()+'&f='+$("#fecha").val());
            }
            
        }
    }
    
    function limpia()
    {
         $("#adt").val(0);
         $("#chd").val(0);          
         $("#nac").val(0);
         $("#estadia").val(0);
         $("#cantidad_pax").val(0);
         $("input:checkbox").attr('checked', false);   
    }
    
    function CambiaFecha()
    {
        
         $('#principal').load('matriz_excursiones_ej.php?d='+$("#estadia_glb").val()+'&g='+$('#cadena_ex').val()+'&f='+$("#fecha").val()+'&c='+$('#cantidad_pax').val());
        /*
        var fecha_hoy = $.datepicker.formatDate('dd-mm-yy', new Date()); 
        var fecha_calendar = $("#fecha").val();
        
        if (cod_tusua == 3 || cod_tusua == 4)
        {
            
            if(fecha_hoy > fecha_calendar){
                alert("No tiene permisos para realizar esta acci�n");
            }
            else
            {
                $('#principal').load('matriz_excursiones_ej.php?d='+$("#estadia_glb").val()+'&g='+$('#cadena_ex').val()+'&f='+$("#fecha").val()+'&c='+$('#cantidad_pax').val());
            
            }
        }
        else
        {
            $('#principal').load('matriz_excursiones_ej.php?d='+$("#estadia_glb").val()+'&g='+$('#cadena_ex').val()+'&f='+$("#fecha").val()+'&c='+$('#cantidad_pax').val());

            
        }
        */
        
    }
    
    </script>

    
 
    
</head>
<body>
 <div id="global">
 
 
<div id="signup">
	<div id="signup-ct">
		<div id="signup-header">
			<h2>Consulta QR <img src="../Imagenes/qrcode.png" align="right" height="50" width="50" /></h2>
			<p>Boarding Pass.</p>
			<a class="modal_close" href="#"></a>
		</div>

		  <div class="txt-fld">
		    <label for="">Boarding Pass:</label>
		    <input id="boarding" name="boarding" type="text" />

		  </div>
		  <div id="boton_qr" class="btn-fld">
		  <button type="button" onclick="consulta_disponibilidad()">Aceptar &raquo;</button>
          </div>

          <div class="btn-fld" id='loading' style='display:none'></div> 


	</div>
    
</div>
 
 <div id="ReloadThis" style="margin: 20 0 0 520px; position: absolute;"><? include ('estado_carrito.php') ?></div>

                
 <div id="cabecera">

      <table width="482" border="0" cellpadding="0" cellspacing="0">  
    	<tr>
          <td width="80" rowspan="4"><img src="../Imagenes/IMG_6902.jpg" width="80" height="80" /></td>
          <td width="13">&nbsp;</td>
          <td width="389">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><strong id="TituloPlomo"><b>Consulta de Excursiones </b></strong></td>
        </tr>
        <tr id="titulo_tiempo">
        <td>&nbsp;</td>
        <td>Tiempo de Toma de Reserva <span id='counter' style="display: none;">0</span> </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
        </tr>    
    
      </table>

 </div>
 

  
 <div id="navegacion" style="display: none">
     <table>
        <tr>
            <td>    
                <form name="form1"  >
                    <table width="357" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="100" class="celdalazul_peque">
                         Fecha
                        </td>
                        <td width="100" class="celdalCELEST">
                         <input type="text" class="seleccionTurist_180" name="fecha" id="fecha" value="<?=$fecha?>" readonly="">
                        </td>
                        <td width="100" class="celdalCELEST">
                         <input name="consulta" type="button" class="BotonTurist_Azul"  value="Consultar" onclick="CambiaFecha()" />
                        </td>
                      </tr>
                    
                    </table>
                </form>
            </td>
             <td>

                 <table id="filtro" align="left" width="357" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="42" class="celdalazul_peque">Excursiones </td>
                        <td width="120" class="celdalCELEST">
                            <select class="seleccionTurist_180" name="tipos_excursiones" onchange="cambia_grupo(this.value)">
                            <option value="0" selected="">--SELECCIONE--</option>
                            <option value="0">--TODOS--</option>
                            <?
                            $miConexion->Conectar();
                            $queryGrupos=$miConexion->EjecutaSP("Consulta_Grupos_Excursiones","");
                            while ($rowgrupos = mysql_fetch_assoc($queryGrupos))
                            {
                                if ($g == $rowgrupos['ID_GRUPO'])
                                {
                                    echo "<option selected value=".$rowgrupos['ID_GRUPO'].">".htmlentities($rowgrupos['NOMBRE_GRUPO'])."</option>" ;
                                }
                                else
                                {

                                    echo "<option value=".$rowgrupos['ID_GRUPO'].">".htmlentities($rowgrupos['NOMBRE_GRUPO'])."</option>" ;

                                }
                            }
                            mysql_free_result($queryGrupos); 
                            mysql_close();
                            ?>
                            </select>
                            
                            <input type="button" class="BotonTurist_Azul" value="Actualizar" onclick="refresh();"  />
                            
                        </td>
                    </tr>
                </table>
             </td>
        </tr>

     </table>
 

 </div><!--cierre navegacion -->
 
 
 
 <div id="Layer4" style="position:absolute; left:835px; top:52px; width:128px; height:80px; z-index:4">
    <img src="../Imagenes/turistik107_118.jpg" width="107" height="118" />

    
 </div>
 

  <div id="principal_1" >
    <?include ("faq.php") ?>
  </div>
 

  <div id="principal"  style="display: none;">
    <?
        if ($_SESSION['cod_tusua']==3 || $_SESSION['cod_tusua']==4 )
        {
            include ("matriz_excursiones_ej.php");
        }
        else
        {
            include ("matriz_excursiones.php");
        }
    ?>
  </div>
 
 <div class="anuncios1">

    <div id="titulo_noticia" style="height:30px; background: #D70000;"><div style="float: left; color: #FFF; padding: 5px 0 0 5px;"><b>Turistik Informa </b></div></div>
    
    <div>
        <div id="cont_6d8b98575023d062d911ccdca935eef3">
            <span id="h_6d8b98575023d062d911ccdca935eef3">
            <script type="text/javascript" src="http://www.meteored.cl/wid_loader/6d8b98575023d062d911ccdca935eef3"></script>
        </div>
    </div>

    <ul id="widget">
    
        <?
        $ObjConsultaCredito = new tkpass(); 
        $TipoDistribuidor = $ObjConsultaCredito->Consulta_tipo_distribuidor_original($_SESSION["codigo_dist"]);
       
        if ($TipoDistribuidor == 1 || $TipoDistribuidor == 2 || $TipoDistribuidor == 3)
        {
            

            if ($_SESSION["cod_tusua"] == 4)
            {
                $ObjConsultaCredito = new tkpass(); 
                $Linea = $ObjConsultaCredito->Consulta_linea_credito($_SESSION["codigo_usu"]);
                
                $ObjConsultaSaldoPrepago = new tkpass(); 
                $linea_prepago = $ObjConsultaSaldoPrepago->Consulta_Saldo_linea_prepago($_SESSION["codigo_usu"]);
                $dip_lc = $Linea[0] - $Linea[1];
                ECHO  	"<li>";
                // linea    <   transaccion
    
                if ($Linea[0] <= $Linea[1] )
                {
                    
                    if ($TipoDistribuidor != 2)
        			{ 
                	 echo "<div class='titulo'>SALDO INSUFICIENTE</div>";               
                	 echo '	<h4><strong style="color:#D70000">TODA RESERVA SERA CON ABONO Y EL SALDO A PAGAR EN OFICINA DE VENTAS TURISTIK</h4>';
                     echo "<br>";

                     echo '<h4><strong style="color:#D70000">DISPONIBLE PARA RESERVAS : <div style="position:right;float:right; margin-right:10px">$ 0</div></strong></h4>';
					}
                     echo '<h4><strong style="color:#D70000">SU DEUDA PENDIENTE ES : <div style="position:right;float:right; margin-right:10px">$ '.number_format($Linea[1], 0, ",", ".").'</div></strong></h4>';		
                }
                else
                {
                    if ($TipoDistribuidor == 3)
        			{              
                	 echo '	<h4><strong style="color:#D70000">TODA RESERVA SERA CON ABONO Y EL SALDO A PAGAR EN OFICINA DE VENTAS TURISTIK</h4>';
                     echo "<br>";

                     echo '<h4><strong style="color:#D70000">DISPONIBLE PARA RESERVAS : <div style="position:right;float:right; margin-right:10px">$ 0</div></strong></h4>';
					}
                    
                    
                    if ($TipoDistribuidor == 2)
        			{
                	 echo "<div class='titulo'>INFORMACION DE CUENTA</div>";               
                     echo "<br>";
                     echo '<h4><strong style="color:#D70000">DISPONIBLE PARA RESERVAS : <div style="position:right;float:right; margin-right:10px">$ '.number_format($dip_lc, 0, ",", ".").'</div></strong></h4>';
                    }
                   if ($Linea[1] > 0)
                   {
                    echo '<h4><strong style="color:#D70000">SU DEUDA PENDIENTE ES : <div style="position:right;float:right; margin-right:10px">$ '.number_format($Linea[1], 0, ",", ".").'</div></strong></h4>';		
                   }
                     
                }
                
                if ($linea_prepago > 0)
                {                
                     echo "<br>";
                     echo '<h4><strong style="color:#D70000">DISPONIBLE PREPAGO : <div style="position:right;float:right; margin-right:10px">$ '.number_format($linea_prepago, 0, ",", ".").'</div></strong></h4>';
                     
                      $val01x = $dip_lc + $linea_prepago;
                
                
                        if( $val01x>0)
                        {
                             echo '<h4><strong style="color:#D70000">TOTAL DISPONIBLE PARA RESERVAS: <div style="position:right;float:right; margin-right:10px">$ '.number_format($dip_lc+$linea_prepago, 0, ",", ".").'</div></strong></h4>';		
                        }
                }
               
                
                echo '</li>';
            }
            
        } 

            $objStock = new ClaseConexion;
            $objStock->Conectar();
            $queryStock=$objStock->EjecutaConsulta("select * from noticias where activa=1 and Fecha > '".date("Y/m/d")."'  order by idnoticias desc");        
        	while($row_listar_stock=mysql_fetch_array($queryStock))
        	{
             	     if($row_listar_stock['Externos']==1)
                     {
                            if($_SESSION['cod_tusua']==4)
                            {
                                 ECHO  	"<li>";
                            	 echo "<div class='titulo'>".$row_listar_stock['titulo']. "</div>";
                               
                            	 echo '	<h4><strong style="color:#D70000">'.substr($row_listar_stock["fecha"],11)." |</strong>  ". $row_listar_stock["contenido"].'</h4>';
                            			
                            	 echo '	</li>';
                            } 
                     }
                     else
                     {
                        if($row_listar_stock['Turistik']==1)
                        {
                                 ECHO  	"<li>";
                                 echo "<div class='titulo'>".$row_listar_stock['titulo']. "</div>";
                             	 echo '	<h4><strong style="color:#D70000">'.substr($row_listar_stock["fecha"],11)." |</strong>  ". $row_listar_stock["contenido"].'</h4>';
                            		
                            	 echo '	</li>';
                        }
                        
                     }
                     
                        if($row_listar_stock['Turistik']==1 && $row_listar_stock['Externos']==1)
                        {
                                 ECHO  	"<li>";
                                 echo "<div class='titulo'>".$row_listar_stock['titulo']. "</div>";
                             	 echo '	<h4><strong style="color:#D70000">'.substr($row_listar_stock["fecha"],11)." |</strong>  ". $row_listar_stock["contenido"].'</h4>';
                            		
                            	 echo '	</li>';
                        }
             
               
        	}
        	mysql_free_result($queryStock); 
        	mysql_close();
            ?>
    
    </ul>

 </div> <!--cierre anuncios -->
 

 <div id="pie">
    <!--Pie-->
 </div>
 
 </div>
</body>
</html>
