-- MySQL dump 10.11
--
-- Host: localhost    Database: webturdb
-- ------------------------------------------------------
-- Server version	5.0.51a-3ubuntu5.4-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Moneda`
--

DROP TABLE IF EXISTS `Moneda`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Moneda` (
  `id` int(11) NOT NULL auto_increment,
  `descripcion` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Moneda`
--

LOCK TABLES `Moneda` WRITE;
/*!40000 ALTER TABLE `Moneda` DISABLE KEYS */;
INSERT INTO `Moneda` VALUES (1,'Nacional'),(2,'Internacional');
/*!40000 ALTER TABLE `Moneda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Operacion`
--

DROP TABLE IF EXISTS `Operacion`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Operacion` (
  `id` int(11) NOT NULL auto_increment,
  `cod_prod` char(10) default NULL,
  `id_session` char(50) default NULL,
  `valor_unitario` double default NULL,
  `moneda` int(11) default NULL,
  `fecha` char(20) default NULL,
  `cantidad` int(11) default NULL,
  `nombre_comprador` char(80) default NULL,
  `direccion` text,
  `email` text,
  `cod_session` text,
  `cod_autoriza` char(50) default NULL,
  `orden_compra` char(50) default NULL,
  `monto` double default NULL,
  `estado` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Operacion`
--

LOCK TABLES `Operacion` WRITE;
/*!40000 ALTER TABLE `Operacion` DISABLE KEYS */;
INSERT INTO `Operacion` VALUES (79,'4','123781685348558700',21000,1,'2009-03-24',2,'sebastian','orly','scamus@intrans.cl','','281172','',42000,1),(80,'4','123784844262687200',21000,1,'2009-03-24',2,'Sebastian','Sheraton','scamus@intrans.cl','','','',42000,NULL),(81,'4','123784857948011400',21000,1,'2009-03-24',2,'Andrés Rogers','a','a@a.cl','','281172','',42000,1),(82,'2','123785231248671800',18000,1,'2009-03-28',1,'EPRO','Sheraton','eprosoft@gmail.com','','281172','',18000,1),(83,'2','123799813434572800',22000,1,'2009-03-28',9,'SEBSTIAN','ORLY','SCAMUS@INTRANS.CL','','281172','',198000,1),(84,'2','123799858945476600',22000,1,'2009-03-29',9,'SEBASTIAN CAMUS','HYATT','SCAMUS@INTRANS.CL','','281172','',198000,1),(85,'2','123811908250377200',22001,1,'2009-04-12',2,'Carolina Baier','Mi casas','carobaier@hotmail.com','','281172','',44002,1),(86,'2','123811990660827300',22003,1,'2009-03-28',3,'cbaier','direccion 554','carobaier@hotmail.com','','281172','',66009,1),(87,'2','123812002034876700',22003,1,'2009-04-05',4,'prueba 3','direccion 3','carobaier@hotmail.com','','281172','',88012,1),(88,'2','123812011152640700',22003,1,'2009-04-12',1,'prueba 4','direccion 4','carobaier@hotmail.com','','281172','',22003,1),(89,'3','123812017636599600',20000,1,'2009-04-13',2,'prueba 5','direccion 5','carobaier@hotmail.com','','281172','',40000,1),(90,'29','123812100573841400',700001,1,'2009-03-29',1,'sdfsdfsd sdfsdfksdlf lklkjlkjl klk jlkj lkj l kj lkj lk jlk jl klkjsdlfkjsl dfkj','skdfjls kdflskdjflskdjkdjflskdjflsdkjfslk jflksjd lk jlkjsldkfjsl dkjfslkdfj slkdfjsl dkfjsldkfjsldkjfsldkjfsldk carola','\'\'','','','',700001,NULL),(91,'29','123812146119881200',700001,1,'2009-03-27',1,'sd','d','carobaier@hotmail.com','','281172','',700001,1),(92,'29','123812149991421700',700001,1,'2009-03-27',1,'sd','as','\'\'','','281172','',700001,1),(93,'29','123812153506109100',700001,1,'2009-03-31',1,'kjkjh','sdf\'\'','sdf','','','',700001,NULL),(94,'4','123844216327621300',21000,1,'2009-03-31',3,'sebastian','orly','scamus@turistik.cl','','281172','',63000,1);
/*!40000 ALTER TABLE `Operacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Producto`
--

DROP TABLE IF EXISTS `Producto`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Producto` (
  `id` int(11) NOT NULL auto_increment,
  `cod_prod` char(10) default NULL,
  `descripcion` text,
  `valor_nac` double default NULL,
  `valor_int` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Producto`
--

LOCK TABLES `Producto` WRITE;
/*!40000 ALTER TABLE `Producto` DISABLE KEYS */;
/*!40000 ALTER TABLE `Producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProductoTur`
--

DROP TABLE IF EXISTS `ProductoTur`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ProductoTur` (
  `CODIGO_PROD` int(11) NOT NULL auto_increment,
  `CODIGO_COLO` int(11) default NULL,
  `NOMBRE_PROD` char(39) default NULL,
  `PRECIO_NAC_PROD` int(20) default NULL,
  `PRECIO_INT_PROD` decimal(20,3) default NULL,
  `PERIODO_VIGENCIA_PROD` int(11) default NULL,
  `DURACION_PROD` int(11) default NULL,
  `DESCRIPCION_PROD` text,
  `ESTADO_PROD` int(11) NOT NULL default '0',
  `DESCRIPCION_POS_PROD` char(20) default NULL,
  `DURACION_DESCUENTO_PROD` int(11) default NULL,
  PRIMARY KEY  (`CODIGO_PROD`,`ESTADO_PROD`),
  KEY `PRODUCTO_REL_COLOR_FK` (`CODIGO_COLO`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ProductoTur`
--

LOCK TABLES `ProductoTur` WRITE;
/*!40000 ALTER TABLE `ProductoTur` DISABLE KEYS */;
INSERT INTO `ProductoTur` VALUES (2,4,'TURISTIK SANTI 1 DIA!!3',22003,'30.000',360,15,'se puede modificar AHORA!!3',1,'TURISTIK SANTIAGO',60),(3,4,'SANTIAGO VINEDO 2 DIAS',20000,'40.000',360,15,'SANTIAGO Y VIñEDO DOS DíAS',1,'STGO Y VIñEDO 2 DIAS',15),(4,3,'SANTIAGO TURISTIK 2 DIAS',21000,'40.000',360,15,'SANTIAGO TURISTIK te permite conocer este maravilloso destino con mayor flexibilidad y adicional profundidad, abordo de los mismos buses de 2 pisos de última generación presentes en ciudades como Nueva York, Londres, Madrid, y Paris.\r\nPuedes subir y bajar las veces que quieras a lo largo del recorrido, así una vez que hayas visitado lo que te interesa, puedes continuar la ruta tomando alguno de los siguientes buses que pasan aproximadamente cada 30 minutos. Si es que no te bajas del bus, demorarás cerca de 2 horas en retornar al mismo punto donde iniciaste el viaje.\r\nPARADAS:\r\n1. PLAZA DE ARMAS: Calle Monjitas frente al # 821.\r\n2. MERCADO CENTRAL: Calle Puente frente al # 889.\r\n3. PLAZA DE LA CONSTITUCIÓN: Calle Teatinos frente al # 254.\r\n4. SANTA LUCÍA: Av. Libertador Bernardo O´Higgins frente al # 406.\r\n5. PROVIDENCIA: Calle Lota frente al # 2229.\r\n6. EL GOLF: Av. Vitacura frente a # 2841 (AFP Habitat).\r\n7. PARQUE ARAUCO: Boulevard Parque Arauco.\r\n8. ALONSO DE CORDOVA: Av. Alonso de Cordova frente al # 3107.\r\n9. SHERATON: Frontis Hotel Sheraton.\r\n10. BELLAVISTA: Av. Bellavista frente al # 112.\r\n11. MUSEO NACIONAL DE BELLAS ARTES: Frontis Museo.\r\n',1,'SANTIAGO 2 DIAS',15),(5,3,'SANTIAGO TURISTIK 1 DIA NInO',5000,'10.000',360,15,'SANTIAGO TURISTIK 1 DIA PARA NInOS',1,'SANTIAGO NINO 1 DIA',15),(6,3,'VINEDO 1 DIA',20000,'40.000',360,15,'VIñEDO POR UN DIA',1,'vinedo',15),(7,4,'CLUB DE LECTORES - SANTIAGO 1 DIA',7500,'20.000',360,15,'CLUB DE LECTORES - SANTIAGO 1 DIA',1,'TURISTIK SANTIAGO',60),(8,3,'CLUB DE LECTORES - WINES',10000,'22.000',360,15,'CLUB DE LECTORES - WINES',1,'',15),(10,4,'SANTIAGO TURISTIK COLEGIOS',5000,'10.000',365,2,'SANTIAGO TURISTIK COLEGIOS',1,'',7),(11,4,'VALLE NEVADO RIPLEY',7000,'15.000',100,2,'TRF PAK/VN/PAK',1,'TRF PAK/VN/PAK',7),(12,4,'VALLE NEVADO HOTEL',20000,'40.000',100,2,'HTL/ VN/ HTL',1,'HTL/ VN/ HTL',7),(13,4,'VALLE NEVADO CLUB DE LECTORES',8400,'15.000',100,2,'PAK/ VN/ PAK',1,'PAK/ VN/ PAK',7),(14,4,'VALLE NEVADO MODULO',14000,'30.000',100,2,'TRF PAK/VN/PAK',1,'TRF PAK/VN/PAK',7),(15,3,'VINA/ VALPO 1 DIA',24000,'40.000',180,15,'VINA/ VALPO 1 DIA',1,'VINA/ VALPO 1 DIA',15),(17,NULL,'TURISTIK SANTIAGO 1 DIA',19000,NULL,NULL,NULL,'SE PUEDE MODIFICAR',0,NULL,NULL),(18,NULL,'nuevo producto carola',100,NULL,NULL,NULL,'prueba carola',0,NULL,NULL),(20,NULL,'nuevo producto carola',100000,NULL,NULL,NULL,'prueba carola',0,NULL,NULL),(22,NULL,'nuevo producto 5',25,NULL,NULL,NULL,'producto 5',0,NULL,NULL),(23,NULL,'nuevo producto 6',7,NULL,NULL,NULL,'producto 6',0,NULL,NULL),(24,NULL,'nuevo producto 6',7,NULL,NULL,NULL,'producto 6',0,NULL,NULL),(25,NULL,'nuevo producto 6',7000000,NULL,NULL,NULL,'producto 6',0,NULL,NULL),(26,NULL,'nuevo producto 6',700000,NULL,NULL,NULL,'producto 6',0,NULL,NULL),(27,NULL,'nuevo producto 6',700000,NULL,NULL,NULL,'producto 6',0,NULL,NULL),(29,NULL,'nuevo producto 69',700001,NULL,NULL,NULL,'producto 69',0,NULL,NULL);
/*!40000 ALTER TABLE `ProductoTur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'webturdb'
--
DELIMITER ;;
/*!50003 DROP FUNCTION IF EXISTS `getPrecio` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eroman`@`%`*/ /*!50003 FUNCTION `getPrecio`(moneda int,codigo int) RETURNS int(11)
BEGIN
	declare valor double;
	if (moneda=1) then
		select PRECIO_NAC_PROD into valor from ProductoTur where CODIGO_PROD=codigo;
	else
		select PRECIO_INT_PROD into valor from ProductoTur where CODIGO_PROD=codigo;
	end if;
	return valor;
END */;;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE*/;;
DELIMITER ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-03-31 16:16:49
