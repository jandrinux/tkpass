/*
Class: SexyAlertBox
	Clone class of original javascript function : 'alert', 'confirm' and 'prompt'

Arguments:
	options - see Options below

Options:
	name - name of the box for use different style
	zIndex - integer, zindex of the box
	onReturn - return value when box is closed. defaults to false
	onReturnFunction - a function to fire when return box value
	BoxStyles - stylesheets of the box
	OverlayStyles - stylesheets of overlay
	showDuration - duration of the box transition when showing (defaults to 200 ms)
	showEffect - transitions, to be used when showing
	closeDuration - Duration of the box transition when closing (defaults to 100 ms)
	closeEffect - transitions, to be used when closing
	onShowStart - a function to fire when box start to showing
	onCloseStart - a function to fire when box start to closing
	onShowComplete - a function to fire when box done showing
	onCloseComplete - a function to fire when box done closing
*/
$(document).ready(function(){
  Sexy.initialize();
});

jQuery.bind = function(object, method){
  var args = Array.prototype.slice.call(arguments, 2);  
  return function() {
    var args2 = [this].concat(args, $.makeArray( arguments ));  
    return method.apply(object, args2);  
  };  
};  

jQuery.fn.delay = function(time,func){
	return this.each(function(){
		setTimeout(func,time);
	});
};


jQuery.fn.extend({
  $chain : [],
  chain: function(fn) {
    this.$chain.push(fn);
    return this;
  },
  callChain: function(context) {
    return (this.$chain.length) ? this.$chain.pop().apply(context, arguments) : false;
  },
  clearChain: function(){
    this.$chain.empty();
    return this;
  }
});

(function($) {

  Sexy = {
    getOptions: function() {
      return {
        name            : 'SexyAlertBox',
        zIndex          : 65555,
        onReturn        : false,
        onReturnFunction: function(e) {},
        BoxStyles       : { 'width': 500 },
        OverlayStyles   : { 'opacity': 0.7 },/*backgroundColor: #000,*/
        showDuration    : 200,
        closeDuration   : 100,
        moveDuration    : 500,
        onCloseComplete : $.bind(this, function() {
          this.options.onReturnFunction(this.options.onReturn);
        })
      };
    },


    initialize: function(options) {
      this.i=0;
      this.options = $.extend(this.getOptions(), options);
			$('body').append('<div id="BoxOverlay"></div><div id="'+this.options.name+'-Box"><div id="'+this.options.name+'-InBox"><div id="'+this.options.name+'-BoxContent"><div id="'+this.options.name+'-BoxContenedor"></div></div></div></div>');
			
			this.Content    = $('#'+this.options.name+'-BoxContenedor');
			this.Contenedor = $('#'+this.options.name+'-BoxContent');
			this.InBox      = $('#'+this.options.name+'-InBox');
			this.Box        = $('#'+this.options.name+'-Box');
			
			$('#BoxOverlay').css({
        position        : 'absolute',
        top             : 0,
        left            : 0,
		opacity         : this.options.OverlayStyles.opacity,
		backgroundColor : this.options.OverlayStyles.backgroundColor,
		'z-index'       : this.options.zIndex,
        height          : $(document).height(),
				width           : $(document).width()
			}).hide();
			
			this.Box.css({
        display         : 'none',
        position        : 'absolute',
        top             : 0,
        left            : 0,
		'z-index'       : this.options.zIndex + 2,
		width           : this.options.BoxStyles.width + 'px'
			});

      this.preloadImages();

      $(window).bind('resize', $.bind(this, function(){
        if(this.options.display == 1) {
          $('#BoxOverlay').css({
            height          : 0,
            width           : 0
          });
          $('#BoxOverlay').css({
            height          : $(document).height(),
            width           : $(document).width()
          });
          this.replaceBox();
        }
      }));

      this.Box.bind('keydown', $.bind(this, function(obj, event){
        if (event.keyCode == 27){
          this.options.onReturn = false;
          this.display(0);
        }      
      }));

      $(window).bind('scroll', $.bind(this, function(){
        this.replaceBox();
      }));
			
    },

    replaceBox: function() {
      if(this.options.display == 1) {
        
        this.Box.stop();
        
        this.Box.animate({
          left  : ( ($(document).width() - this.options.BoxStyles.width) / 2),
          top   : ( $(document).scrollTop() + ($(window).height() - this.Box.outerHeight()) / 2 )
        }, {
          duration  : this.options.moveDuration,
          easing    : 'easeOutBack'
        });

        $(this).delay(this.options.moveDuration, $.bind(this, function() {
          $('#BoxAlertBtnOk').focus();
          $('#BoxPromptInput').focus();
          $('#BoxConfirmBtnOk').focus();
        }));
      }
    },

    display: function(option) {
      if(this.options.display == 0 && option != 0 || option == 1) {


        if (!$.support.maxHeight) { //IE6
          $('embed, object, select').css({ 'visibility' : 'hidden' });
        }


        this.togFlashObjects('hidden');

        this.options.display = 1;


        $('#BoxOverlay').stop();
        $('#BoxOverlay').fadeIn(this.options.showDuration, $.bind(this, function(){
          this.Box.css({
            display         : 'block',
            left            : ( ($(document).width() - this.options.BoxStyles.width) / 2)
          });
          this.replaceBox();
        }));
      
      } else {

        this.Box.css({
          display         : 'none',
          top             : 0
        });

        this.options.display = 0;

        $(this).delay(500, $.bind(this, this.queue));

        $(this.Content).empty();
        this.Content.removeClass();

        if(this.i==1) {
          $('#BoxOverlay').stop();
          $('#BoxOverlay').fadeOut(this.options.closeDuration, $.bind(this, function(){
            $('#BoxOverlay').hide();
            if (!$.support.maxHeight) { //IE6
              $('embed, object, select').css({ 'visibility' : 'visible' });
            }

            this.togFlashObjects('visible');

            this.options.onCloseComplete.call();
          }));
        }
      }
    },

    messageBox: function(type, message, properties, input) {
        
        $(this).chain(function () {

        properties = $.extend({
          'textBoxBtnOk'        : 'OK',
          'textBoxBtnCancel'    : 'Cancelar',
          'textBoxInputPrompt'  : null,
          'password'            : false,
          'onComplete'          : function(e) {}
        }, properties || {});

        this.options.onReturnFunction = properties.onComplete;

        this.Content.append('<div id="'+this.options.name+'-Buttons"></div>');
        if(type == 'alert' || type == 'info' || type == 'error')
        {
            $('#'+this.options.name+'-Buttons').append('<input id="BoxAlertBtnOk" type="submit" />');
            
            $('#BoxAlertBtnOk').val(properties.textBoxBtnOk).css({'width':70});
            
            $('#BoxAlertBtnOk').bind('click', $.bind(this, function(){
              this.options.onReturn = true;
              this.display(0);
            }));
                      
            if(type == 'alert') {
              clase = 'BoxAlert';
            } else if(type == 'error') {
              clase = 'BoxError';
            } else if(type == 'info') {
              clase = 'BoxInfo';
            }
            
            this.Content.addClass(clase).prepend(message);
            this.display(1);

        }
        else if(type == 'confirm')
        {
      
            $('#'+this.options.name+'-Buttons').append('<input id="BoxConfirmBtnOk" type="submit" /> <input id="BoxConfirmBtnCancel" type="submit" />');
            $('#BoxConfirmBtnOk').val(properties.textBoxBtnOk).css({'width':70});
            $('#BoxConfirmBtnCancel').val(properties.textBoxBtnCancel).css({'width':70});

            $('#BoxConfirmBtnOk').bind('click', $.bind(this, function(){
              this.options.onReturn = true;
              this.display(0);
			  $('#registro').submit();
            }));

            $('#BoxConfirmBtnCancel').bind('click', $.bind(this, function(){
              this.options.onReturn = false;
              this.display(0);
            }));

            this.Content.addClass('BoxConfirm').prepend(message);
            this.display(1);
        }
        else if(type == 'prompt')
        {
            var espacio = '&nbsp;&nbsp;&nbsp;&nbsp;';
            var espacio1 = '&nbsp;&nbsp;&nbsp;&nbsp;';
           
            $('#'+this.options.name+'-Buttons').append('<input id="BoxPromptBtnOk" type="submit" /> <input id="BoxPromptBtnCancel" type="submit" />');
            $('#BoxPromptBtnOk').val(properties.textBoxBtnOk).css({'width':70});
            $('#BoxPromptBtnCancel').val(properties.textBoxBtnCancel).css({'width':70});
                        
            type = properties.password ? 'password' : 'text';
			
            boleta_ttk = $("#boleta_ttk").val();
            boleta_re = $("#boleta_re").val();            
            voucher_tbk = $("#voucher_tbk").val();
            fop = $("#fop").val();           
            /*$('#BoxPromptInput3').keydown(function(e){
              		  k = (document.all) ? e.keyCode : e.which;
           		       if (k==8 || k==0) return true;
            	        patron = /[0-9]/;
                       n = String.fromCharCode(k);
                       return patron.test(n);    			
    		          });  */
                      
            if(fop != 'E'){ 
                if(voucher_tbk != ''){
                    this.Content.prepend('Voucher TBK:&nbsp;&nbsp;'+espacio1+'<input id="BoxPromptInput3" type="'+type+'" readonly/>');
                    //$('#BoxPromptInput2').val(properties.input);                    
                    $('#BoxPromptInput3').css({'width':250}); 
                    $('#BoxPromptInput3').val(voucher_tbk); 
                 }else{
                   this.Content.prepend('Voucher TBK:&nbsp;&nbsp;'+espacio1+'<input id="BoxPromptInput3" type="'+type+'" />');
                    //$('#BoxPromptInput2').val(properties.input);                    
                    $('#BoxPromptInput3').css({'width':250});  
                    $('#BoxPromptInput3').val();
                 }                  
            }
            
            if(boleta_ttk != ''){
    			this.Content.prepend('Boleta Afecta: '+espacio1+'<input id="BoxPromptInput2" type="'+type+'" readonly/><br/><br/>');
                //$('#BoxPromptInput2').val(properties.input);
                $('#BoxPromptInput2').css({'width':250});
                $('#BoxPromptInput2').val(boleta_ttk);			
			}else{
    			 this.Content.prepend('Boleta Afecta: '+espacio1+'<input id="BoxPromptInput2" type="'+type+'" /><br/><br/>');
                //$('#BoxPromptInput2').val(properties.input);
                $('#BoxPromptInput2').css({'width':250});
                $('#BoxPromptInput2').val();
			}
            
            if(boleta_re != ''){
                this.Content.prepend('Boleta Exenta:'+espacio+'<input id="BoxPromptInput" type="'+type+'" maxlength="10" readonly/><br/><br/>');
    	        //$('#BoxPromptInput').val(properties.input);
                $('#BoxPromptInput').css({'width':250});
                $('#BoxPromptInput').val(boleta_re);
	        }else{
    	        this.Content.prepend('Boleta Exenta:'+espacio+'<input id="BoxPromptInput" type="'+type+'" maxlength="10"/><br/><br/>');
    	        //$('#BoxPromptInput').val(properties.input);
                $('#BoxPromptInput').css({'width':250});
                $('#BoxPromptInput').val();   
	        }              
           

            $('#BoxPromptBtnOk').bind('click', $.bind(this, function(){
              this.options.onReturn = $('#BoxPromptInput').val();
			  this.options.onReturn = $('#BoxPromptInput2').val();            
			  this.options.onReturn = $('#BoxPromptInput3').val();
              
              valor1 = $('#BoxPromptInput').val();
              valor2 = $('#BoxPromptInput2').val();
              valor3 = $('#BoxPromptInput3').val();			  
              $("#boleta_re").val(valor1);
        
              $("#boleta_ttk").val(valor2);
              $("#voucher_tbk").val(valor3);
              
			  $("#boleta_re").change(SubmitFormulario());
			  /*if(valor1 != '' && valor2 != ''){		  
			  $("#boleta_re").val(valor1);
			  $("#fecha_boleta_re").val(valor2);
			  $("#fecha_boleta_re").change(SubmitFormulario());
			  }	*/		  
			  //$('#registro').submit();
			  this.display(0);
            }));

            $('#BoxPromptBtnCancel').bind('click', $.bind(this, function(){
              this.options.onReturn = true;
              this.display(0);
            }));

            this.Content.addClass('BoxPrompt').prepend(message + '<br />');
            this.display(1);
        }
        else
        {
            this.options.onReturn = false;
            this.display(0);		
        }

      });

      this.i++;

      if(this.i==1) {
        $(this).callChain(this);
      }
    },

    queue: function() {
      this.i--;
      $(this).callChain(this);
    },

    chk: function (obj) {
      return !!(obj || obj === 0);
    },

    togFlashObjects: function(state) {
      var hideobj=new Array("embed", "iframe", "object");
      for (y = 0; y < hideobj.length; y++) {
       var objs = document.getElementsByTagName(hideobj[y]);
       for(i = 0; i < objs.length; i++) {
        objs[i].style.visibility = state;
       }
      }
    },

    preloadImages: function() {
      var img = new Array(2);
      img[0] = new Image();img[1] = new Image();img[2] = new Image();
      img[0].src = this.Box.css('background-image').replace(new RegExp("url\\('?([^']*)'?\\)", 'gi'), "$1");
      img[1].src = this.InBox.css('background-image').replace(new RegExp("url\\('?([^']*)'?\\)", 'gi'), "$1");
      img[2].src = this.Contenedor.css('background-image').replace(new RegExp("url\\('?([^']*)'?\\)", 'gi'), "$1");
    },
    	
    alert: function(message, properties) {
      this.messageBox('alert', message, properties);
    },
   	
    info: function(message, properties){
      this.messageBox('info', message, properties);
    },
    	
    error: function(message, properties){
      this.messageBox('error', message, properties);
    },
   
    confirm: function(message, properties){
      this.messageBox('confirm', message, properties);
    },
    
    prompt: function(message, input, properties){
      this.messageBox('prompt', message, properties, input);
    }

  };

})(jQuery);