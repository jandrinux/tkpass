<?php
//ini_set('display_errors', 1);
session_start();
date_default_timezone_set("America/Santiago");

require ("../Clases/ClaseConexion.inc.php");
include ("../functions/tkpass.php");
require ("SII/class.boletaGen.php");

$Obj_Tkpass = new tkpass(); 

$ncr_ttk = "";
$ncr_tts = "";
$fecha = date('Y-m-d H:m:i');
$porc = $_POST['porcentaje_reemb'];
$boleta_re = $_POST['boleta_re'];
$boleta_ttk = $_POST['boleta_ttk'];
$total_afecto = $_POST['total_afecto'];
$precio = $_POST['monto_servicio'];
$total_exento = $precio - $total_afecto;
$descp_tts = "Reembolso Boleta TTS ".$boleta_re; 
$descp_ttk = "Reembolso Boleta TTK ".$boleta_ttk; 
$items = array();


$total_tts = ($porc * $total_exento)/100;
$total_ttk = ($porc * $total_afecto)/100;

##EXENTO
$i = new Item;
$i->Linea=1;
$i->codigoMannager= "0";
$i->Descripcion=$descp_tts;
$i->Cantidad=1;
$i->PrecioUnitario=(int)$total_tts;
$i->Descuento=0;
$items[] = $i ;   

$g = new NotaCreditoGen;
$retorno = $g->generar(false, $items, 0, $descp_tts, $boleta_re);
$ncr_tts = $retorno->numero;
$url_tts = $retorno->Message;

unset($items);

if ($total_ttk > 0) {
	##AFECTO
	$i = new Item;
	$i->Linea=1;
	$i->codigoMannager= 0;
	$i->Descripcion=$descp_ttk;
	$i->Cantidad=1;
	$i->PrecioUnitario=(int)$total_ttk;
	$i->Descuento=0;
	$items[] = $i ;        

	$g = new NotaCreditoGen;
	$retorno = $g->generar(true, $items, 0, $descp_ttk, $boleta_ttk);
	$ncr_ttk = $retorno->numero;
	$url_ttk = $retorno->Message;
}


$id = $_POST['id'];

$Obj_Tkpass->Actualiza_Tabla_Reembolso($id, $ncr_tts, $ncr_ttk, $fecha, $_SESSION['usuario'], $total_tts, $total_ttk, $url_tts, $url_ttk ); 

print("<script>alert('Servicio Reembolsado Exitosamente');</script>");
print("<script>window.parent.close();</script>");


?>