var ProcessLight = new Class({
	opciones: {
		Opacidad: 0.6,
		Color: '#000',
		Cerrar: false,
		memPosScroll: [0,0],
		iframeSize: [0,0]
	},	
	initialize: function() {	
		this._status = 0;
		this.isIEsix = Browser.Engine.trident4;		
		$$('[id^=processlight_]').each(function(o,i){
			o.dispose();
		});
		this.opciones.memPosScroll = [window.getScroll().x, window.getScroll().y];
		document.body.style.overflow = "hidden";
		document.getElementsByTagName("html")[0].style.overflow = "hidden";
		this.overlay = new Element('div', {
			id : 'processlight_overlay',
			styles: {
				display : 'block',
				position : 'absolute',
				top : '0',
				left : '0',
				opacity : 0,
				'z-index' : 70000,
				'background-color' : this.opciones.Color,
				height : window.getScrollSize().y + 'px',
				width : window.getScrollSize().x + 'px'
			},
			events: {		 
				'dblclick': function(){
					if(this.opciones.Cerrar) this.close();
				}.bind(this),
				'contextmenu' : function(e){
					if(e) new Event(e).stop();
					return false;
				},
				'selectstart' : function(e){
					if(e) new Event(e).stop();
					return false;
				},
				'dragstart' : function(e){
					if(e) new Event(e).stop();
					return false;
				}
			}			
		}).injectInside(document.body);		
		if(this.opciones.Cerrar){
			document.addEvent('keydown', function(evt){
				if(evt.key == 'esc'){
					this.close();
				}
			}.bind(this));
		}
		var _opCerrar = this.opciones.Cerrar;
		window.removeEvents('resize').addEvent('resize', function(){	
			if($('processlight_overlay')  !== null){
				var _e = $('processlight_frame');				
							
				_h = (_wsy<this.opciones.iframeSize[0]) ? _wsy : this.opciones.iframeSize[0];				
				_w = (_wsx<this.opciones.iframeSize[1]) ? _wsx : this.opciones.iframeSize[1];
				
				_wsy = window.getSize().y-70;
				_wsx = window.getSize().x-70;
				//
				_hScroll = _wScroll = false;
				//
				if(_h>_wsy){
					_h = _wsy;
					_hScroll = true;
				}
				if(_w>_wsx){
					_w = _wsx;
					_wScroll = true;
				}
				if(_hScroll) _w += 17;
				if(_wScroll) _h += 17;
				
				_e.setStyles({
					'height': _h + 'px',
					'width': _w + 'px',
					margin: (this._isIEsix) ? ((_h*-0.5) + window.getScroll().y) +'px'+' 0 0 '+(_w*-0.5)+'px' : (_h*-0.5)+'px'+' 0 0 '+(_w*-0.5)+'px'
				});	
				//
				$('processlight_overlay').set({
					styles : {
						height : window.getScrollSize().y + 'px',
						width : window.getScrollSize().x + 'px'		 
					}
				});
				if($('processlight_cerrar')  !== null){					
					$('processlight_cerrar').set({
						styles : {
							top: (_e.getCoordinates().top.toInt()-35)+'px',
							right: (_e.getCoordinates().left.toInt()-35)+'px' 
						}
					});
				}			
			}
			
			this.opciones.memPosScroll = [window.getScroll().x, window.getScroll().y];		
		}.bind(this));
	},
	show : function() {
		_status = 1;
		this.overlay.setStyle('opacity',this.opciones.Opacidad);
		if(this.isIEsix) this.togSelect();
	},
	close : function() {
		_status = 0;
		var _e = $('processlight_overlay');
		var _p = this.opciones.memPosScroll;
		var transicion = new Fx.Morph(_e,{
			duration: 400,
			transition: Fx.Transitions.linear.easeOut,
			onComplete: function(){
				_e.destroy();
				document.body.style.overflow = "";
				document.getElementsByTagName("html")[0].style.overflow = "";
				window.scrollTo(_p[0], _p[1]);
			}
		}).start({'opacity': 0});
		$('processlight_frame').destroy();
		if(this.opciones.Cerrar) $('processlight_cerrar').destroy();
		if(this.isIEsix) this.togSelect();
		window.removeEvent('resize');
	},	
	url : function(u) {		
		var _ele = this.iframe;
		this.iframe_xhr = new Request.HTML({
			url: u,
			method: 'post',
			onSuccess: function(rTree, rElem, rHTML, rJS) {
				_ele.set('html', rHTML);
			}
		}).send();
	},	
	setURL : function(u) {
		this.iframe = new Element('div', {
		 	id: 'processlight_frame',		 
			styles: {
				position: (this.isIEsix) ? 'absolute' : 'fixed',
				'z-index' : 70001
			},		 	
			events: {		 			 
				'load': function(){					
					if(this.opciones.Cerrar) this.btncerrar.fade('show');
				}.bind(this)		 
			}			
		}).injectInside(document.body);
		this.callBack = function(){};
		var _thisPL = this;	
		var _ele = this.iframe;
		var _isIEsix = this.isIEsix;
		var _opCerrar = this.opciones.Cerrar;
		this.iframe_xhr = new Request.HTML({
			url: u,
			method: 'post',		
			onRequest: function() {},
			onSuccess: function(rTree, rElem, rHTML, rJS) {				
				rElem.each(function(e,i){
					if($chk(e.id)){
						if(e.id == 'PL_Contenido'){							
							_h = _thisPL.leerStyle(e,'height').toInt();						
							_w = _thisPL.leerStyle(e,'width').toInt();
							_thisPL.opciones.iframeSize = [_h, _w];
							//
							_wsy = window.getSize().y-70;
							_wsx = window.getSize().x-70;
							//
							_hScroll = _wScroll = false;
							//
							if(_h>_wsy){
								_h = _wsy;
								_hScroll = true;
							}
							if(_w>_wsx){
								_w = _wsx;
								_wScroll = true;
							}
							if(_hScroll) _w += 17;
							if(_wScroll) _h += 17;
						}
					}
				});						
				_ele.set({
					'html' : rHTML,
					styles : {
						'height': _h+'px',
						'width' : _w+'px',
						'overflow' : 'auto',
						top: '50%',
						left: '50%',
						margin: (_isIEsix) ? ((_h*-0.5) + window.getScroll().y) +'px'+' 0 0 '+(_w*-0.5)+'px' : (_h*-0.5)+'px'+' 0 0 '+(_w*-0.5)+'px'
					}
				});
				
				if(_opCerrar){
					_btncerrar.set({					
						styles : {
							top: (_ele.getCoordinates().top.toInt()-35)+'px',
							right: (_ele.getCoordinates().left.toInt()-35)+'px',
							visibility:'visible'
						}
					});	
				}
				_thisPL.callBack();
			}
		}).send();		
		if(_opCerrar){
			this.btncerrar = new Element('span', {
				id : 'processlight_cerrar',				
				styles: {					
					width: '35px',
					height: '35px',
					display : 'inline-block',
					visibility:'hidden',
					background : (_isIEsix) ? 'url(js/processlight/px.gif) no-repeat 0 0' : 'url(js/processlight/cerrar.png) no-repeat 0 0',
					position : (_isIEsix) ? 'absolute' : 'fixed',
					filter : (_isIEsix) ? 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'./js/processlight/cerrar.png\')' : 'none !important',
					'z-index' : 70002
				},
				events: {
					'click': function(){
						this.close();
					}.bind(this)
				}
			}).injectInside(document.body);	
			var _btncerrar = this.btncerrar;
		}		
	},
	togSelect : function() {
		this.vEsO = $$('select');
		if(this._status){
			this.vLogEsS = [];
			this.vEsO.each(function(o,i){
				vLogEsS.push(o.getStyle('visibility'));		
				o.setStyle('visibility', 'hidden');
			});			
		}else{
			this.vEsO.each(function(o,i){		
				o.setStyle('visibility', this.vLogEsS[i]);
			});			
		}		
	}.bind(this),
	leerStyle : function(el, style) {
		if(!document.getElementById) return;
		var value = el.style[this.toCamelCase(style)];
	  	if(!value)
		if(document.defaultView)
		   value = document.defaultView.getComputedStyle(el, "").getPropertyValue(style);
	   else if(el.currentStyle)
		  value = el.currentStyle[toCamelCase(style)];
	   return value;
	},
	toCamelCase : function( sInput ) {
		var oStringList = sInput.split('-');
		if(oStringList.length == 1) return oStringList[0];
		var ret = sInput.indexOf("-") == 0 ? oStringList[0].charAt(0).toUpperCase() + oStringList[0].substring(1) : oStringList[0];
		for(var i = 1, len = oStringList.length; i < len; i++){
			var s = oStringList[i];
			ret += s.charAt(0).toUpperCase() + s.substring(1)
		}
		return ret;
	}
});
//
var pl_cssDOM = document.createElement('link');
with(pl_cssDOM){
	setAttribute('rel', 'stylesheet');
	setAttribute('type', 'text/css');
	setAttribute('href', 'js/processlight/processlight.css');
	setAttribute('media', 'screen');
}
document.getElementsByTagName("head")[0].appendChild(pl_cssDOM);
//
window.addEvent('domready', function(){
	var pl_iframePrintHide = new Element('iframe', {
		id : 'pl_WINprintHide',


		name : 'pl_WINprintHide',
		src: '',
		styles: {
			top : '-200px',
			left : '-200px',
			width: '100px',
			height: '100px',
			position: 'absolute',
			'z-index' : 70003		
		}
	}).injectInside(document.body);
});