<?php 
include("menus.php");
//require ("../Clases/ClaseConexion.inc.php");
//include( "../Clases/class.TemplatePower.inc.php"); 
//require ("../Clases/ClaseUtil.inc.php");

$tpl = new TemplatePower( "../Plantillas/Modificacion_Excursiones.tpl" );

$_Util=new Util;
$miConexion= new ClaseConexion;

$tpl->prepare();
$tpl->assign("fechas", $_Util->Fecha());
$tpl->assign("precio", $_POST['precio']);
$tpl->assign("nombre", $_POST['nombre']);
$tpl->assign("nombre_abr", $_POST['nombre_abr']);
$tpl->assign("descripcion_corta", $_POST['descripcion_corta']);
$tpl->assign("descripcion", $_POST['descripcion']);
$tpl->assign("descripcion_eng", $_POST['descripcion_eng']);
$tpl->assign("descripcion_por", $_POST['descripcion_por']);

$tpl->assign("cat1", $_POST['cat1']);
$tpl->assign("cat2", $_POST['cat2']);
$tpl->assign("cat3", $_POST['cat3']);
$tpl->assign("cat4", $_POST['cat4']);
$tpl->assign("cat5", $_POST['cat5']);
$tpl->assign("hora", $_POST['hora']);
$tpl->assign("mini", $_POST['mini']);
$tpl->assign("hora_correo", $_POST['hora_correo']);
$tpl->assign("min_correo", $_POST['min_correo']);
$tpl->assign("limite", $_POST['limite']);
$tpl->assign("limite_t", $_POST['limite_t']);
$tpl->assign("vendedor", $_POST['vendedor']);
$tpl->assign("vendedor_int", $_POST['vendedor_int']);
$tpl->assign("codigo_eo", $_POST['codigo_eo']);
$tpl->assign("correo", $_POST['correo']);
$tpl->assign("correo_auto", $_POST['correo_auto']);
$tpl->assign("guardar", $_POST['guardar']);
$tpl->assign("descuento", $_POST['descuento']);

$tpl->assign("nemotec", $_POST['nemotec']);


$tpl->assign("desc_juniper", $_POST['desc_juniper']);
$tpl->assign("polit_juniper", $_POST['polit_juniper']);


$miConexion= new ClaseConexion;
if($_POST['codigo_eo']<>'')
{
$miConexion->Conectar();
$queryTipoUsuarios=$miConexion->EjecutaSP("Consultar_Lista_ExcursionOrigen","'".$_POST['codigo_eo']."'");
while ($rowTipoUsuario = mysql_fetch_assoc($queryTipoUsuarios))
{
	    //$tpl->newBlock("excursiones");
        $tpl->assign("CODIGO", $rowTipoUsuario['CODIGO_EO']  );
        $tpl->assign("_ROOT.nombre_abr", $rowTipoUsuario['NOMBRE_ABR']  );
        $tpl->assign("_ROOT.descripcion_corta", $rowTipoUsuario['DESCRIPCION_EO']);
        $tpl->assign("_ROOT.descripcion", $rowTipoUsuario['RESUMEN_EO']);
        $tpl->assign("_ROOT.descripcion_eng", $rowTipoUsuario['RESUMEN_EO_ENG']);
        $tpl->assign("_ROOT.descripcion_por", $rowTipoUsuario['RESUMEN_EO_POR']);
		$tpl->assign("_ROOT.precio", $rowTipoUsuario['PRECIO_EO']  );
		$tpl->assign("_ROOT.cat1", $rowTipoUsuario['CAT1']  );		
		$tpl->assign("_ROOT.cat2", $rowTipoUsuario['CAT2']  );		
		$tpl->assign("_ROOT.cat3", $rowTipoUsuario['CAT3']  );		
		$tpl->assign("_ROOT.cat4", $rowTipoUsuario['CAT4']  );		
		$tpl->assign("_ROOT.cat5", $rowTipoUsuario['CAT5']  );										
		$tpl->assign("_ROOT.hora", substr($rowTipoUsuario['HORA_EO'],0,2));
		$tpl->assign("_ROOT.mini", substr($rowTipoUsuario['HORA_EO'],3,2));	
		$tpl->assign("_ROOT.hora_correo", substr($rowTipoUsuario['HORA_CORREO'],0,2));
		$tpl->assign("_ROOT.min_correo", substr($rowTipoUsuario['HORA_CORREO'],3,2));										
		$tpl->assign("_ROOT.limite_t", substr($rowTipoUsuario['LIMITE_T_EO'],0,2));		
		$tpl->assign("_ROOT.limite", substr($rowTipoUsuario['LIMITE_EO'],0,2));														
		$tpl->assign("_ROOT.codigo_vendedor", $rowTipoUsuario['VENDEDORES_EO']  );	
		$tpl->assign("_ROOT.descuento", $rowTipoUsuario['DESCUENTO_EO']  );			
		$tpl->assign("_ROOT.codigo_vendedor_int", $rowTipoUsuario['VENDEDORES_INT_EO']  );			
		$tpl->assign("_ROOT.correo", $rowTipoUsuario['CORREO_EO']  );	
        $tpl->assign("_ROOT.correo_auto", $rowTipoUsuario['CORREO_AUTO']  );		
		$tpl->assign("_ROOT.correo_alerta", $rowTipoUsuario['CORREO_ALERTA_EO']  );		
	
        $tpl->assign("_ROOT.DBASICO", $rowTipoUsuario['DESCTO_BASICO']  );		
        $tpl->assign("_ROOT.DPREMIUM", $rowTipoUsuario['DESCTO_PREMIUM_DEF']  );		
        $tpl->assign("_ROOT.PORMAX", $rowTipoUsuario['PORC_MAXIMO_DESC']  );		
        $tpl->assign("_ROOT.VALOR_AFECTO", $rowTipoUsuario['VALOR_EXENTO']  );	
        $tpl->assign("_ROOT.VALOR_CHILD", $rowTipoUsuario['VALOR_CHILD']  );
        
        
        $tpl->assign("_ROOT.nemotec", $rowTipoUsuario['NEMOTECNICO_PROV']  );		
        $tpl->assign("_ROOT.desc_juniper", $rowTipoUsuario['DESCRIPCION_FICHA']  );		
        $tpl->assign("_ROOT.polit_juniper", $rowTipoUsuario['POLITICAS']  );	
        
		$tpl->assign("_ROOT.hora_correo_preaviso", substr($rowTipoUsuario['HORA_PREAVISO'],0,2));
		$tpl->assign("_ROOT.min_correo_preaviso", substr($rowTipoUsuario['HORA_PREAVISO'],3,2));	
        
        if ($rowTipoUsuario['ENVIO_PREAVISO'] == 1){
            $tpl->assign("_ROOT.ENVIO_PRE1", 'checked'  );
        }else{
            $tpl->assign("_ROOT.ENVIO_PRE2", 'checked'  );
        }
            
        
        if ($rowTipoUsuario['PAQUETE'] == 1){
            $tpl->assign("_ROOT.PAQUETE", 'checked'  );
        }else{
             $tpl->assign("_ROOT.PAQUETE", ''  );
        }
        
        if ($rowTipoUsuario['DISP_PAX_CHD'] == 1){
            $tpl->assign("_ROOT.DISP_CHD", 'checked'  );
        }else{
             $tpl->assign("_ROOT.DISP_CHD", ''  );
        }
        
        if ($rowTipoUsuario['VENDEDOR_EXT_PP'] == 1){
            $tpl->assign("_ROOT.VENDEDOR_PP", 'checked'  );
        }else{
             $tpl->assign("_ROOT.VENDEDOR_PP", ''  );
        }	
        
        
		if($rowTipoUsuario['VENDEDORES_EO']=='1')
		{
		$tpl->assign("_ROOT.vendedor", 'SI');																		
		}													
		else
		{
		$tpl->assign("_ROOT.vendedor", 'NO');																				
		}
		
		if($rowTipoUsuario['VENDEDORES_INT_EO']=='1')
		{
		$tpl->assign("_ROOT.vendedor_int", 'SI');																		
		}													
		else
		{
		$tpl->assign("_ROOT.vendedor_int", 'NO');																				
		}
        
        if ($rowTipoUsuario['ESTADO_EO'] == 1)
        {
            $tpl->assign("_ROOT.estado_act", 'checked');
        }
        else
        {
            $tpl->assign("_ROOT.estado_des", 'checked');
        }
        
        if ($rowTipoUsuario['APLICA_DESC_CANT_PAX'] == 1 ) $desc_cant_pax = "CANTIDAD PAX /";
        if ($rowTipoUsuario['APLICA_DESC_EXCURSION'] == 1 ) $desc_desc_ex = "CANTIDAD EXCURSION /";
        if ($rowTipoUsuario['APLICA_DESC_LUGAR_REC'] == 1 ) $desc_desc_lugar_rec = "LUGAR RECOGIDA /";
        if ($rowTipoUsuario['APLICA_DESC_LUGAR_RES'] == 1 ) $desc_desc_lugar_res = "LUGAR RESERVA.";
        
        $tpl->assign("DESCUENTOS", $desc_cant_pax." ".$desc_desc_ex." ".$desc_desc_lugar_rec." ".$desc_desc_lugar_res);
        
        $cod_eo = $rowTipoUsuario['CODIGO_EO'];
        $files = $rowTipoUsuario['IMAGENES'];
        
        
        $partes_file=explode(",", $files);
        $file_1 = $partes_file[0];
        $file_2 = $partes_file[1];
        $file_3 = $partes_file[2];
        
        $cant_img = substr_count($files, ',');
        
        if ($file_1 != "") 
        {   
            $image_1 = "<a class='fancybox-effects-a' href='../Imagenes/img-juniper/$file_1' title='Click para imagen completa'><img src='../Imagenes/img-juniper/$file_1' width='80' height='80' /></a><img src='../Imagenes/unchecked.gif' title='Click para eliminar imagen' onclick=\"javascript:delete_image($cod_eo,'$file_1',1)\" />";
            $tpl->assign("_ROOT.img_1", $image_1 );
        }
        if ($file_2 != "") 
        {   
            $image_2 = "<a class='fancybox-effects-a' href='../Imagenes/img-juniper/$file_2' title='Click para imagen completa'><img src='../Imagenes/img-juniper/$file_2' width='80' height='80' /></a><img src='../Imagenes/unchecked.gif' title='Click para eliminar imagen' onclick=\"javascript:delete_image($cod_eo,'$file_2',1)\"  />";
            $tpl->assign("_ROOT.img_2", $image_2 );
        }
        if ($file_3 != "") 
        { 
            $image_3 = "<a class='fancybox-effects-a' href='../Imagenes/img-juniper/$file_3' title='Click para imagen completa'><img src='../Imagenes/img-juniper/$file_3' width='80' height='80' /></a><img src='../Imagenes/unchecked.gif' title='Click para eliminar imagen' onclick=\"javascript:delete_image($cod_eo,'$file_3',1)\"  />";
            $tpl->assign("_ROOT.img_3", $image_3 );
        }
        
        if ($cant_img == 0) $tpl->assign("_ROOT.cant_img", 3 );
        if ($cant_img == 1) $tpl->assign("_ROOT.cant_img", 2 );
        if ($cant_img == 2) $tpl->assign("_ROOT.cant_img", 1 );
        if ($cant_img == 3) $tpl->assign("_ROOT.disabled", "disabled" );
        
        $file_J = $rowTipoUsuario['IMAGEN_LISTADO'];
        if ($file_J != "") 
        {   
            $image_j = "<a class='fancybox-effects-a' href='../Imagenes/img-juniper/$file_J' title='Click para imagen completa'><img src='../Imagenes/img-juniper/$file_J' width='80' height='80' /></a><img src='../Imagenes/unchecked.gif' title='Click para eliminar imagen' onclick=\"javascript:delete_image($cod_eo,'$file_J',2)\" />";
            $tpl->assign("_ROOT.img_J", $image_j );
            $tpl->assign("_ROOT.disabledJ", "disabled" );
        }

}
	

mysql_free_result($queryTipoUsuarios); 
mysql_close(); 
	
}	

if ($_POST['guardar']=='1')
	{
	   
      if ($_POST['pack'] == "on"){
	       $pack = 1;
           
                $miConexion->Conectar();
                $query=$miConexion->EjecutaConsulta("SELECT ID_PACK FROM PAQUETES_EXCURSIONES WHERE ID_PACK = ".$_POST['codigo_eo']." ");
                $cant = mysql_num_rows($query);
                mysql_free_result($query); 

                if ($cant == 0)
                {
                    $miConexion->Conectar();
                    $query2=$miConexion->EjecutaConsulta("INSERT INTO PAQUETES_EXCURSIONES (ID_PACK, NOMBRE_PACK, ESTADO_PACK) VALUES (".$_POST['codigo_eo'].", '".$_POST['nombre']."', 1) ");
                    mysql_free_result($query2); 
                }

	   }
       else
       {
           $pack = 0;
           
            $miConexion->Conectar();
            $query=$miConexion->EjecutaConsulta("SELECT ID_PACK FROM PAQUETES_EXCURSIONES WHERE ID_PACK = ".$_POST['codigo_eo']." ");
            $cant = mysql_num_rows($query);
            mysql_free_result($query); 

            if ($cant > 0)
            {
                $query2=$miConexion->EjecutaConsulta("SELECT CODIGO_EXC FROM EXCURSION  WHERE CODIGO_EO =".$_POST['codigo_eo']." ORDER BY CODIGO_EXC LIMIT 1 ");
                while ($row2 = mysql_fetch_assoc($query2))
                {	
            	   $codigo_exc =$row2['CODIGO_EXC'];
                }
                
                if ($codigo_exc != '')
                {
                    $query3=$miConexion->EjecutaConsulta("DELETE FROM EXCURSION WHERE CODIGO_EXC = ".$codigo_exc." ");
                    $query4=$miConexion->EjecutaConsulta("DELETE FROM REL_EXCURSION_PROV WHERE CODIGO_EXC = ".$codigo_exc." ");
                }
                

            }
            
                $query5=$miConexion->EjecutaConsulta("DELETE FROM PAQUETES_EXCURSIONES WHERE ID_PACK = ".$_POST['codigo_eo']." ");
                $query6=$miConexion->EjecutaConsulta("DELETE FROM DETALLES_PAQUETES WHERE ID_PACK = ".$_POST['codigo_eo']." ");
            
           

            mysql_free_result($query); 
            mysql_free_result($query2);
            mysql_free_result($query3);
            mysql_free_result($query4);
            mysql_free_result($query5);
            mysql_free_result($query6);
       }
       
       
           #JUNIPER
        
        $nemotecnico = $_POST['nemotec'];
        
        $descripcion_juniper=str_replace("'","\'",$_POST['desc_juniper']);
        $politicas=str_replace("'","\'",$_POST['polit_juniper']);
        
        
        
        $archivoJ = $_FILES['archivoJ']['name'];
        $prefijoJ = substr(md5(uniqid(rand())),0,6);
        if ($archivoJ != "")
        {
            $destinoJ = "../Imagenes/img-juniper/".$prefijoJ."_".$archivoJ;
            $fileJ = $prefijoJ."_".$archivoJ;
                if (copy($_FILES['archivoJ']['tmp_name'],$destinoJ)) {
                    $file_concatJ = $fileJ.",".$file_concatJ;
                    $status = "Archivo subido: <b>".$archivoJ."</b>";
                }
                else
                {
                    $status = "Error al subir el archivo ".$archivoJ;
                }
        }
    
        
        ######END JUNIPER ########
       
           
        $status = "";
        for ($i=0; $i<=count($_FILES['archivo']['name']); $i++)
        {
            //$tamano = $_FILES["archivo"]['size'][$i];
           // $tipo = $_FILES["archivo"]['type'][$i];
            $archivo = $_FILES["archivo"]['name'][$i];
            $prefijo = substr(md5(uniqid(rand())),0,6);
            
            if ($archivo != "")
            {
                $destino = "../Imagenes/img-juniper/".$prefijo."_".$archivo;
                $file = $prefijo."_".$archivo;
                    if (copy($_FILES['archivo']['tmp_name'][$i],$destino)) {
                        $file_concat = $file.",".$file_concat;
                        $status = "Archivo subido: <b>".$archivo."</b>";
                    }
                    else
                    {
                        $status = "Error al subir el archivo ".$archivo;
                    }
            }
            else
            {
                $status = "Error al subir archivo ".$archivo;
            }
         }
         
         
         if ($_POST['vendedorpp'] == "on"){
	        $vendedorpp = 1;
         }else{
            $vendedorpp = 0;
         }
         
         if ($_POST['disp_chd'] == "on"){
            $disp_chd = 1;
        }else{
            $disp_chd = 0;
        }	
        
        if ($_POST['send'] == "1"){
            $send = 1;
        }else{
            $send = 0;
        }
         
       
        $img_concat = $files.$file_concat;

		$miConexion->Conectar();
		$lim=$_POST['limite']*10000;
		$lim_t=$_POST['limite_t']*10000;
        
        $descripciones_corta=str_replace("'","\'",$_POST['descripcion_corta']);
		$descripciones=str_replace("'","\'",$_POST['descripcion']);
        $descripciones_eng=str_replace("'","\'",$_POST['descripcion_eng']);
        $descripciones_por=str_replace("'","\'",$_POST['descripcion_por']);
        $hora_correo = $_POST['hora_correo'].":".$_POST['min_correo'];
        $hr_correo_preaviso = $_POST['hora_correo_preaviso'].":".$_POST['min_correo_preaviso'];
        
		$procMasterPines=$miConexion->EjecutaSP("Modificar_Excursion_Origen_","'".$_POST['codigo_eo']."','".$_POST['nombre']."','".$_POST['nombre_abr']."', '".$descripciones_corta."', '".$descripciones."', '".$descripciones_eng."', '".$descripciones_por."',
                                                                                '".$_POST['precio']."','".$_POST['cat1']."','".$_POST['cat2']."','".$_POST['cat3']."','".$_POST['cat4']."','".$_POST['cat5']."','".$_POST['codigo_vendedor']."',
                                                                                '".$_POST['codigo_vendedor_int']."','".$_POST['hora'].':'.$_POST['mini']."','".$lim_t."','".$lim."','".$_POST['correo']."','".$_POST['correo_alerta']."',
                                                                                '".$_POST['descuento']."', ".$_POST['DBASICO'].",".$_POST['DPREMIUM'].",".$_POST['PORMAX'].",".$_POST['VALOR_AFECTO'].",".$_POST['VALOR_CHILD'].", ".$_POST['estado_eo'].",
                                                                                 '".$pack."', '".$img_concat."', '".$hora_correo."', '".$_POST['correo_auto']."',
                                                                                 '".$nemotecnico."', '".$descripcion_juniper."', '".$politicas."', '".$hr_correo_preaviso."', '".$fileJ."', $vendedorpp, $disp_chd, $send, @respuesta,@mensaje");
		$parametrosSalida=$miConexion->ObtenerResultadoSP("@respuesta,@mensaje");
		$rowverificacion= mysql_fetch_assoc($parametrosSalida);
        
		if($rowverificacion['@mensaje']<>'')
		{  
		print("<script>alert('".$rowverificacion['@mensaje']."');</script>");
		}
        
        
        
		$tpl->assign("codigo_exc", $rowverificacion['@respuesta']);
		echo "<META HTTP-EQUIV='refresh' CONTENT='0; URL=$PHP_SELF'>"; 	
		$tpl->assign("guardar", "0");
		mysql_free_result($parametrosSalida); 
		mysql_close(); 
        
	}

		

$tpl->printToScreen();
?>