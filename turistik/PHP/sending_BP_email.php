<?
//ini_set('display_errors', 1);
session_start();
date_default_timezone_set("America/Santiago");

include ('../../lib/MPDF/mpdf.php');
require ("../Clases/ClaseConexion.inc.php");
require ("../Clases/ClaseUtil.inc.php");
require ("../Clases/Idiomas_Boarding.php");
require ("../functions/tkpass.php");
require ("../functions/consulta_descuento.php");
require ("../Clases/class.mail.php");


$miConexion = new ClaseConexion;
$idioma = new Idiomas_Boarding;
$_Util = new Util;

$session = "BP".time().$_SESSION['codigo_usu'];
$vouchers = $_POST['boarding'];
$destinatari = strtolower($_POST['destinatari']);
$lista_voucher = explode("/",$vouchers);
$lista_voucher =array_filter($lista_voucher); // elimina array nulos
$lista_documentos = array();
$excursiones_stk = array(6,54,55,63,64);
$logo = "<img src='../Imagenes/turistik-2pisos.JPG' border='0'  width='100px'>";

/*
$mail = explode(";",$destinatari);
for ( $i = 0 ; $i < count($mail); $i ++) 
{
    if($_Util->valida_email($mail[$i]))
    { 
       // echo "El mail es valido"; 
    } else { 
        echo '3'; 
        exit();
    } 
}
*/


if(!empty($vouchers))
{

    $valor = 0;
    $cont_adt = 0;
    $cont_adt = 0;
    $primer_pax = 0;


    foreach ($lista_voucher as $voucher) 
    {

        $miConexion->Conectar();
        $query1=$miConexion->EjecutaConsulta("SELECT * FROM CORREOS_RESERVAS WHERE VOUCHER = '".$voucher."' ");
        if(mysql_num_rows($query1) == 0)
        {
         $miConexion->Conectar();
         $query2=$miConexion->EjecutaConsulta("INSERT INTO CORREOS_RESERVAS (CORREO,VOUCHER) VALUES ('".$destinatari."','".$voucher."' )");  
        }

        $fecha_bp = $_Util->bp_to_date($voucher);
        
        $miConexion = new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaSP("Consulta_voucher_simple","  '".$voucher."', '".$fecha_bp."' ");
        while($row=mysql_fetch_array($query)) 
        {
            
            if (in_array($row['CODIGO_EO'], $excursiones_stk)) 
            {
                if (($_SESSION['codigo_suc'] == 7 || $_SESSION['codigo_suc'] == 15) && ($row['FOP'] == "W" || $row['FOP'] == "TC"))
                {
                    $bp[] = $voucher;
                   
                }
                
            }
            else
            {
                $bp[] = $voucher;
             
            }       
        }#end while
        
    }   
        
        
        
    foreach($bp as $boarding_pass)
    {    

            $fecha_bp = $_Util->bp_to_date($boarding_pass);
        
            $miConexion->Conectar();
        	$queryRegistrosReserva=$miConexion->EjecutaSP("Consulta_Voucher","  ".$boarding_pass.", '".$fecha_bp."' ");
            $cant_pasajeros = mysql_num_rows($queryRegistrosReserva);
            
                while ($rowL = mysql_fetch_assoc($queryRegistrosReserva))
                {
      	     
                   //if($rowL['OTRO_LUGAR_RE'] == 0) $lugares=' '.$rowL['DESCRIPCION_HL'];		
            		if($rowL['LUGAR_RE'] == 80){
            		  $lugares=' '."OTRO LUGAR: ".$rowL['OTRO_LUGAR_RE'];
            		}else{
            		  $lugares=' '.$rowL['ubicacion'];
            		}
                    
                    $idioma_boarding = $rowL['GRUPO_IDIOMA'];
                    
                    if ($idioma_boarding == 0 || $idioma_boarding == 1)
                    {
                        $condiciones = nl2br(htmlspecialchars($rowL['RESUMEN_EO']));
                    }
                    if ($idioma_boarding == 2)
                    {
                        $condiciones = nl2br(htmlentities($rowL['RESUMEN_EO_ENG']));
                    }
                    if ($idioma_boarding == 3)
                    {
                        $condiciones = nl2br(htmlentities($rowL['RESUMEN_EO_POR']));
                    }
                    
        
                    
                    $minutos_recogida = $_Util->SumarHoras($rowL['HORA_EO'], $rowL['MINUTOS'])." HRS.";
                    $cod_vendedor = $rowL['CODIGO_VENDEDOR_RE'];
                    $cod_usua = $rowL['COD_USUA'];
                    $cod_tusua = $rowL['COD_TUSUA'];
                    $cod_cierre = $rowL['COD_CIERRE'];
                    $ObjTransformaFecha = new tkpass(); 
                    $fecha = $ObjTransformaFecha->transforma_fecha_sql($rowL['FECHA_RE']);         
                    $fecha_qr = str_replace("/", "-", $fecha);
            		$precio = $rowL['PRECIO_RE'];	
                    $codigo_eo = $rowL['CODIGO_EO'];  
                    $telefono = $rowL['TELEFONO_RE'];
                    $fop = $rowL['FOP'];
                    $tipoReserva = $rowL['TIPO_RE'];
                    $voucher = $rowL['VOUCHER_RE'];      
                    $valor = $precio + $valor;	
                    $tipo_pax = $rowL['TIPO_PASAJERO'];
                    $precio_total = $precio + $precio_total;           
                    $n_excursion = strtoupper($rowL['NOMBRE_EO']);
                   	
                    $observacion = $rowL['OBSERVACION_RE'];
                    $estado_reagendado = $rowL['ESTADO_REAGENDADO'];
                    $comision = $rowL['COMISION_RE'];
                    
                    $total_comision = $comision + $total_comision;
                    
                   // $total_deuda = $precio - $comision;
                   // $deuda = $total_deuda + $deuda;
                    
                    $dcto_pax = $rowL['DSCTO_PAX'];
                    $dcto_lugar = $rowL['DSCTO_LUGAR'];
                    $dcto_lugar_reserva = $rowL['DSCTO_LRESERVA'];
                    $dcto_excursiones = $rowL['DSCTO_EXCURSIONES'];
                    $dcto_cliente = $rowL['DSCTO_CLIENTE'];
                    
                    
                    $codigo_promocion = $rowL['PROMOCION'];
                    $nombre_promocion = $rowL['NOMBRE_PROMOCION'];
        
                    //Se deja condicion si es vacio ya que Tomar_Reserva_Aeropuesrto_Traslado no tiene tipo pax
                    if ($tipo_pax == ''){ 
                        $pasajero = $rowL['PASAJERO_RE']." ".$rowL['APASAJERO_RE'];
                        $tipos_de_pasajeros = $cant_pasajeros. " PASAJEROS";
                    }
        
                    if ($tipo_pax == 'ADT')
                    {  
                        
                        if($rowL['PAQUETE'] == 1 || $rowL['PRECIO_RE'] == 0)
                        {
                            if($rowL['PAQUETE'] == 1)
                            {
                                $cont_adt = $cont_adt + 1;
                                $pasajero = $rowL['PASAJERO_RE']." ".$rowL['APASAJERO_RE'];
                            }
                            else
                            {
                                $cont_adt = $cont_adt + 1;
                                $pasajero = $rowL['PASAJERO_RE']." ".$rowL['APASAJERO_RE'];
                            }
                        }
                        else
                        {
                            $cont_adt = $cont_adt + 1;
                            if ($primer_pax == 0)
                            {
                               $pasajero = $rowL['PASAJERO_RE']." ".$rowL['APASAJERO_RE'];
                                $primer_pax = 1;  
                            }
                            $valor_pax = $rowL['PRECIO_EO']; 
                        }
                    }
                    
                    if ($tipo_pax == 'CHD')
                    {

                        if($rowL['PAQUETE'] == 1 || $rowL['PRECIO_RE'] == 0)
                        {
                            if($rowL['PAQUETE'] == 1)
                            {
                                $cont_chd = $cont_chd + 1;
                            }
                        }
                        else
                        {
                            $cont_chd = $cont_chd + 1; 
                            $valor_pax = $rowL['VALOR_CHILD'];  
                        } 
                    }
                    
                    
                    if ($cont_chd != 0) { $tipos_de_pasajeros = $cont_chd." CHD"; }
                    if ($cont_adt != 0) { $tipos_de_pasajeros = $cont_adt." ADT"; } 
                    if ($cont_chd != 0 && $cont_adt != 0) { $tipos_de_pasajeros = $cont_adt." ADT"." / ".$cont_chd." CHD"; }
                    
                    
                    
                    if ($dcto_cliente != 0)
                    {
                        $dcto_tipo_cliente = $rowL['TIPO_CLIENTE'];
                        $descuento_c = $dcto_tipo_cliente.", ";
                    }
                    

                    
                    $idioma->Idioma($cod_tusua, $tipoReserva, $fop, $idioma_boarding, $dcto_pax, $dcto_lugar, $dcto_lugar_reserva, $dcto_excursiones, $total_comision, $precio_total,$estado_reagendado,$cod_cierre);
                    

                    if ($estado_reagendado == 1)
                    {
                        $subtitulo = $idioma->subtitulo." - REAGENDADO";
                    }
                        
                      
                }
                
                if (in_array($codigo_eo , $excursiones_stk))
                {
                    $CodeQR = "";
                }
                else
                {
                    $CodeQR = "<img src='https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=".$boarding_pass." ' />";
                }
                
                if ($codigo_promocion != '')
                {
                    
                    include ("../Clases/Barcode39.php"); 
                
                    $bc = new Barcode39($codigo_promocion); 
                                            
                    header("Content-type: image/gif");
                    $image = $bc->draw();
                    
                    imagegif($image,"CodeBar/$codigo_promocion.gif");
                    
                    $BarCode = "<img src='CodeBar/$codigo_promocion.gif'>";
                    $titulo_promo = "Promocion: ".$nombre_promocion;
                    
                }
                
                
                
                    require('boarding_format2.php');

                    $cont_chd = 0;
                    $cont_adt = 0;
                    $precio_total=0;
                    $descuentos_aplicados = '';
                    $descuento_p = '';
                    $descuento_l = '';
                    $descuento_lr = '';
                    $descuento_ex = '';
                    $primer_pax = 0;
                   
              
                mysql_free_result($queryRegistrosReserva); 
               
        
           
    }            
           // echo $lista_documentos[1];    

    for ($i=0; $i<count($lista_documentos); $i++){
        
         $documentos = $lista_documentos[$i]. " " .$documentos;
    }


    $html='
    <html>
    <head>
    <meta name="tipo_contenido"  content="text/html;" http-equiv="content-type" charset="utf-8">
    
    <style type="text/css">
     body {
        margin:0px;
     }
    .celda_bordes {
    	font-family: Arial;
    	border: 3px solid #c3c3c3;
    }
    .celda_dos
    {
        margin-left:20px;
        vertical-align:text-top;
    }
    .celda_peque {
        
    	font-family: Arial;
    	font-size: 12px;
    	line-height: 16px;
    	color: #000000;
    	background-color: #FFFFFF;
    	background-repeat: repeat-x;
        padding-left:20px;
        padding-right:20px;
    }
    .celda {
    	font-family: Arial;
    	font-size: 10px;
        margin-left: 50px;
    	line-height: 16px;
    	color: #000000;
    	background-color: #FFFFFF;
    	background-repeat: repeat-x;
         vertical-align:text-top;
    }
    .celda_titulo {
        font-family: Arial;
    	font-size: 12px;
        }
    </style>
    </head>
    <body>
    '.$documentos.'
    </body>
    </head>
    ';


    $mpdf=new mPDF('c');
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->WriteHTML($html);
    //$mpdf->Output();
    $mpdf->Output("BoardingPassByEmail/$session.pdf",'F');


    if ($codigo_promocion != '')
    {
        unlink("CodeBar/$codigo_promocion.gif");     
        
    } 
   
    // include_once('send_bp_mail.php');  // envia correo con adjunto pdf
     $destinatari = explode(";",$destinatari);

    $asunto="Envio de Boarding Pass";       
    $cuerpo='Envio de Boarding Pass
            <br><br>Atte,<br>
            Equipo Turistik<br><br>
            
            Por favor, NO responda a este mensaje, es un envío autom&aacute;tico.'; 

    $attach = "BoardingPassByEmail/$session.pdf";
    $file = "$session.pdf";
    

    $resp = Correo::Email($asunto,$cuerpo,$destinatari, $attach, $file);

    if ($resp == 1) {

        unlink("BoardingPassByEmail/$session.pdf");  //elimino de ftp el BP
        echo $resp;
    }
    else
    {
        echo $resp;
    }

}else{
    
    echo '2';

}

?>
