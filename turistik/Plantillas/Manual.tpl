<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Manual</title>
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:13px;
	top:129px;
	width:688px;
	height:198px;
	z-index:1;
}
#Layer3 {	position:absolute;
	left:18px;
	top:20px;
	width:482px;
	height:62px;
	z-index:3;
}
#Layer2 {
	position:absolute;
	left:18px;
	top:163px;
	width:798px;
	height:121px;
	z-index:4;
}
#Layer4 {
	position:absolute;
	left:798px;
	top:43px;
	width:248px;
	height:95px;
	z-index:5;
}
-->
</style>
</head>

<body>
<div class="TituloPlomo" id="Layer3">
  <table width="327" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="80" rowspan="4"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></td>
      <td width="13">&nbsp;</td>
      <td width="234">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong>Manual Turistik </strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha">{fecha}</span></td>
    </tr>
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
  </table>
</div>
<div id="Layer2">
  <table width="743" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="2" class="celdalCELEST">Generaci&oacute;n de Tarjetas </td>
    </tr>
    <tr>
      <td width="48" class="celdalCELEST"><img src="../Imagenes/120px-Crystal_Clear_action_build19x19_derecha.png" width="19" height="19" /></td>
      <td width="695" class="celdalCELEST">Se ingresa la cantidad de Tarjetas que se desean crear de un producto determinado, aociandole el distribuidor que se le despachar&aacute;n las Tarjetas.<br />
        Al hacer clik en el bot&oacute;n Aceptar, La p&aacute;gina devuelve el rango de &quot;Numeros&quot; que permite imprimir las Tarjetas generadas. <br /></td>
    </tr>
  </table>
  <table width="743" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="2" class="celdalCELEST">Impresi&oacute;n de  Tarjetas </td>
    </tr>
    <tr>
      <td width="48" class="celdalCELEST"><img src="../Imagenes/120px-Crystal_Clear_action_build19x19_derecha.png" width="19" height="19" /></td>
      <td width="695" class="celdalCELEST">Se ingresa el rango de n&uacute;meros para generar el Archivo de Impresi&oacute;n. Luego hacer click en el bot&oacute;n Aceptar, se abre una ventana de donde se debe seleccionar <u>Descargar Archivo</u> para obtener el Archivo de texto. <br /></td>
    </tr>
  </table>
  <table width="743" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="2" class="celdalCELEST">Despachar Tarjeta </td>
    </tr>
    <tr>
      <td width="47" class="celdalCELEST"><img src="../Imagenes/120px-Crystal_Clear_action_build19x19_derecha.png" width="19" height="19" /></td>
      <td width="696" class="celdalCELEST">Para poder entregar las tarjetas fisicamente al distribuidor , se debe realizar el despacho a &eacute;ste.<br />
        Se selecciona el distribuidor para quien va el despacho y se ingresa la fecha de la guia
        que se emite<br />
        Existen dos posibilidades:<br />
        Si elige la opci&oacute;n de Pre-Activaci&oacute;n=SI, esto permite enviar las tarjetas activadas al distribuidor. Para esto, es necesario seleccionar el Distribuidor y el Vendedor a quien se le va a cargar la Comisi&oacute;n <br />
        Si elige la opci&oacute;n de Pre-Activaci&oacute;n=NO, se despachan las tarjetas en estado DESPACHADO,con lo que es necesario activarlas en alg&uacute;n momento.<br />
        Luego es necesario digitar los primeros siete digitos del Folio (Para poder hacer un despacho masivo, todas las tajetas tiene que ser del mismo producto y generadas el mismo mes, para que concuerden los cinco primeros digitos en todas las tarjetas a despachar) .<br />
        Luego se ingresan los cinco ultimos digitos del Folio.(Inicial y Final del Rango)<br />
        Si se desea cambiar el distribuidor a quien se le generaron las Tarjetas , hay que hacer click sobre el icono <img src="../Imagenes/ico_alpha_Replace_16x16.png" width="16" height="16" /> el cual abrira una p&aacute;gina con los datos que se cargaron en la P&aacute;gina de Despacho, as&iacute; que solo hay que seleccionar el distribuidor a quien se quiere asociar las Taretas y hacer click en Aceptar y el cambio esta realizado. No olvidar que al cerrarse la ventana que realiza el cambio del Distribuidor quedamos nuevamente en el Despacho de la Tarjeta, donde hay que seleccionar el nuevo Distribuidor de las Tarjetas. <br /></td>
    </tr>
  </table>
  <table width="743" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="2" class="celdalCELEST">Activar Tarjetas </td>
    </tr>
    <tr>
      <td width="48" class="celdalCELEST"><img src="../Imagenes/120px-Crystal_Clear_action_build19x19_derecha.png" width="19" height="19" /></td>
      <td width="695" class="celdalCELEST">La activaci&oacute;n de una Tarjeta, asocia los servicios al producto de la Tarjeta, junto con el cargo de las comisiones que se selecionan en la p&aacute;gina.<br />
        En esta p&aacute;gina se carga el usuario que realiza la activaci&oacute;n, permitiendole ganar una comisi&oacute;n. Si desea activar m&aacute;s de una Tarjeta, tiene que seleccionar Cantidad Tarjeta: MAS DE UNA
      lo que permitir&aacute; cargar una ventana donde se pueden ingresar mas tarjetas para activarlas con la misma informaci&oacute;n del N&uacute;mero y el Nombre del pasajero, es decir se cobran todas las tarjetas a la misma persona .</td>
    </tr>
  </table>
  <table width="743" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="2" class="celdalCELEST">Activar Tarjetas Turistik </td>
    </tr>
    <tr>
      <td width="48" class="celdalCELEST"><img src="../Imagenes/120px-Crystal_Clear_action_build19x19_derecha.png" width="19" height="19" /></td>
      <td width="695" class="celdalCELEST">Esta es una opci&oacute;n que la puede manejar Call Center, ya que en &eacute;sta hay que seleccionar el Usuario que realiza la venta y adem&aacute;s si existe una Tarjeta de una compra anterior hay que ingresar el Folio en , para tener un descuento adicional en la compra actual <br />
       Al igual que en la pagina de Activacion anterior, si se desea activar m&aacute;s de una Tarjeta, tiene que seleccionar Cantidad Tarjeta: MAS DE UNA
      lo que permitir&aacute; cargar una ventana donde se pueden ingresar mas tarjetas para activarlas con la misma informaci&oacute;n del N&uacute;mero y el Nombre del pasajero, es decir se cobran todas las tarjetas a la misma persona .</td>
    </tr>
  </table>
  <table width="743" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="2" class="celdalCELEST">Consultar Tarjetas</td>
    </tr>
    <tr>
      <td width="48" class="celdalCELEST"><img src="../Imagenes/120px-Crystal_Clear_action_build19x19_derecha.png" width="19" height="19" /></td>
      <td width="695" class="celdalCELEST">Esta pagina permite consultar en detalle por tarjeta en especifico </td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
</div>
</body>
</html>
