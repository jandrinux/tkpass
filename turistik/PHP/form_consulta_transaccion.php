<?php
session_start();
include("menus.php"); 
$_Util=new Util; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Emision de Reporte</title>
<head>
    <link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
	<link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
	<script type="text/javascript" src="../../js/ui.core.js"></script>
	<script type="text/javascript" src="../../js/ui.datepicker.js"></script>
	<link type="text/css" href="../../css/demos.css" rel="stylesheet" />
	<script type="text/javascript">
	$(function() {
		$("#datepicker").datepicker();
		$("#datepicker1").datepicker();
	});
    
    $(document).ready(function() {
	$("#botonExcel").click(function(event) {
	   alert("Exportando");
		$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
		$("#FormularioExportacion").submit();
        });
    });
	</script>
	
<script type="text/JavaScript">

function limpiar_input(){
    document.form1.pasajero.value = '';
}
</script>
</head>

<body>


<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">

  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Crystal_Clear_app_date80x80.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Transacciones WebPay</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    

  </table>
  <br />


<form name="form1" method="post" onSubmit="consulta_transaccion(); return false" action="">
<table width="800" height="52" border="0" cellspacing="0" >
  <tr>
    <td width="150"  class="celdalazul_peque">Nombre Pasajero (ventas exitosas)</td>
    <td width="587"  class="celdalCELEST">    
	<input name="pasajero" class="seleccionTurist_180" id="pasajero" value="<?php echo $_POST['pasajero'];?>" /> 	
    </td>  

  </tr>
  <tr>
    <td class="celdalazul_peque">Fecha Realizaci&oacute;n Compra - Desde</td>
    <td class="celdalCELEST">
	<input type="text" value="<? echo date('d/m/Y')?>" id="datepicker" name="fecha_rango_1" class="seleccionTurist_180" maxlength="15"  />
    </td>  
  </tr>
  <tr>
    <td class="celdalazul_peque">Fecha Realizaci&oacute;n Compra - Hasta</td>
    <td class="celdalCELEST" >	
		<input type="text" value="<? echo date('d/m/Y');?>" id="datepicker1" name="fecha_rango_2" class="seleccionTurist_180" maxlength="15" />		
    </td>  
  </tr>
  </table>
  <table width="800" height="52" border="0" cellspacing="0" >
  <tr>
    <td width="197" class="celdalazul_peque">Filtrar Transacci&oacute;n por</td>
    <td width="140" class="celdalCELEST" >Ventas Fracasadas<input type="radio" onclick="limpiar_input()" name="filtro" value="2" ></td>	
	<td width="140" class="celdalCELEST" >Ventas Exitosas<input type="radio" onclick="limpiar_input()" checked  name="filtro" value="1">	</td>
    <td width="140" class="celdalCELEST" >Intentos de compra<input type="radio" onclick="limpiar_input()"  name="filtro" value="3">	</td>
    <td class="celdalCELEST"><input type="submit"  class="BotonTurist_Celest"  value="Consultar" />
    </td> 
  </tr>

</table>

</form>



<div id="resultado_listar" style="PADDING-TOP: 0px;TEXT-ALIGN: leftPADDING-BOTTOM: 3px;FONT-FAMILY: Arial;
FONT-SIZE: 8pt; left:20px; top:280px; width:1000px; height:30px; z-index:3; border:0pt black solid; " >

<? include('CONSULTA_transacciones.php'); ?>


</div>


    <form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
        <p id="botonExcel"  class="BotonAzul2">Generar Reporte <img src="export_to_excel.gif" class="botonExcel" /></p>
        <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
    </form>


</div>

</body>
</html>
