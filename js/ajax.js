function objetoAjax(){
	var xmlhttp=false;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
		   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
  		}
	}

	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}

function isNumberKey(evt){ 
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
 
         return true;
}

function isNumberKey(evt)
{ // Solo acepta rut
 var charCode = (evt.which) ? evt.which : event.keyCode
 if ((charCode < 48 || charCode > 57) && charCode != 45 && charCode != 107)
    return false;

 return true;
}
      
// Solo acepta n�meros
function validarP3(e) { 
    tecla = (document.all) ? e.keyCode : e.which; 
    if (tecla==8) 
        return true; 
    patron = /\d/; 
    te = String.fromCharCode(tecla); 
    return patron.test(te); 
}
//  acepta letras, tambi�n las letras � y �
function validarP1(e) { 
    tecla = (document.all) ? e.keyCode : e.which; 
    if (tecla==8) 
        return true; 
    patron =/[A-Za-z��\s]/; 
    te = String.fromCharCode(tecla);
    return patron.test(te); 
}

// parpadeo de alerta
function vku_parpadeo(otro) {
var el = document.getElementById(otro);
if ( el.style.visibility != 'hidden' ) {
el.style.visibility = 'hidden';
}
else {
el.style.visibility = 'visible';
}
} 

function validarEmail( email ) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) )
    {
        alert("Error: La direccion de correo " + email + " es incorrecta.");
        return false;
    }
    return true;

}

function abrir_popup(url, ancho, alto)
{
    var posicion_x; 
    var posicion_y; 
    posicion_x=(screen.width/2)-(ancho/2); 
    posicion_y=(screen.height/2)-(alto/2); 
    window.open(url, "Turistik S.A", "width="+ancho+",height="+alto+",menubar=0,toolbar=0,directories=0,scrollbars=no,resizable=no,left="+posicion_x+",top="+posicion_y+"");
    
}


function formatNumber(num,prefix){
   prefix = prefix || '';
   num += '';
   var splitStr = num.split('.');
   var splitLeft = splitStr[0];
   var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
   var regx = /(\d+)(\d{3})/;
   while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
   }
   return prefix + splitLeft + splitRight;
}

function limpiar_form_reserva(){
  document.agregaExcursion.pasajero.value="";	
  document.agregaExcursion.pasaporte.value="";
//  document.agregaExcursion.codSerial.value="";
  document.agregaExcursion.codAutoriza.value="";
  document.agregaExcursion.observacion.value='';
  document.agregaExcursion.pasajero.focus();  
}
function limpiar_form_reserva_dos(){
  document.CompraExcursion.hotel.value="";	
  document.CompraExcursion.habitacion.value="";
  document.CompraExcursion.lRecogida.value="";
  document.CompraExcursion.lRecogidaOtro.value="";
  document.CompraExcursion.tipoReserva.value="";
  document.CompraExcursion.fop.value="";
  document.getElementById("codSocio").value="";
}


function limpiar_form_nuevo_stock(){
  document.nuevo_stock.serie.value="";
  document.nuevo_stock.serie_desde.value="";
  document.nuevo_stock.serie_hasta.value="";
  document.nuevo_stock.serie.focus();

}

// Función que recibe los parámetros enviados por grafico1.php, desde los enlaces de cada barra.
// Envía esos datos a grafico2.php para que los procese.
// Una vez procesados esos datos, se despliega en pantalla el gráfico detalle en el DIV "detalle_chart" haciendose ahora visible.
function detalleAnios(anio, semestre1, semestre2) {
	detalleDiv = document.getElementById('detalle_chart');
	detalleDiv.innerHTML = "";
	ajax = objetoAjax();
	ajax.open("POST", "graficos/grafico2.php");
	ajax.onreadystatechange = function() {
		if(ajax.readyState == 4) {
				detalleDiv.innerHTML = ajax.responseText
				detalleDiv.style.display="block";
		}
	}
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send("anio="+anio + "&semestre1="+semestre1 + "&semestre2="+semestre2)
}

function agregar_stock(){
 
	
            var sucursal=document.nuevo_stock.sucursal.value;
            var serie=document.nuevo_stock.serie.value;
            var serial_desde=document.nuevo_stock.serial_desde.value;	
            var serial_hasta=document.nuevo_stock.serial_hasta.value;	
			var  tipo =document.nuevo_stock.xTipo.value;
		
            if(parseFloat(serial_desde) >=parseFloat(serial_hasta))
            {
                alert("El Código Serial Desde no puede ser mayor al Código Serial Hasta");
                return;    
            }
	//alert(serial_desde);
                if(parseFloat(serial_desde)==0){
                    alert("Código Serial Desde No puede Ser '0'");
                    return;

                }
                
           if(confirm("Desea Realmente Agregar Este Stock?")){
               
           }
           else{ 
               return;
           }
                
                
                
         divResultado = document.getElementById('resultado');
  divResultado.innerHTML= '<div style="width:670px; text-align:center;float:left; margin:10px 0px 10px 0px;"><img src="../Imagenes/load.gif"></div>';
	        
                
	  accion="agregar_stock";
	  ajax=objetoAjax();
	  ajax.open("POST", "PROCESO_ajax.php",true);
	  ajax.onreadystatechange=function() {
    	  if (ajax.readyState==4) {
    	    divResultado.innerHTML = ajax.responseText
			// alert("Stock Cargado Exitsamente");
    	  }

	  }
       // if ( serie != '' && serial_desde != '' && serial_hasta != ''){
         // limpiar_form_nuevo_stock();              
        //}

   ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
   ajax.send("sucursal="+sucursal+"&accion="+accion+"&serial_desde="+serial_desde+"&serial_hasta="+serial_hasta+"&serie="+serie+"&xTipo=" + tipo);
   
   document.nuevo_stock.serie.value="";
   document.nuevo_stock.serial_desde.value="";	
   document.nuevo_stock.serial_hasta.value="";	
  
}

function cargaStock(){
	
  divResultado = document.getElementById('showStock');
  divResultado.innerHTML= '<div style="width:40px; margin:10px 0px 10px 280px; text-align:left;  "><img src="../Imagenes/load-input.gif"></div>';

    var serie = document.getElementById("serie").value;
    var serial_desde = document.getElementById("serial_desde").value;
    var serial_hasta = document.getElementById("serial_hasta").value;
    
        if (serial_desde!="")
        {
            var string=""
            for(i=1; i<6-serial_desde.length; i++)
            {
                string = string + "" + 0;
            }    
            document.getElementById("serial_desde").value = string + "" +serial_desde;
        }
        if (serial_hasta != "")
        {
            var string=""
            for(i=1; i<6-serial_hasta.length; i++)
            {
                string = string + "" + 0;
            }    
            document.getElementById("serial_hasta").value = string + "" +serial_hasta;
        }
      ajax=objetoAjax();
      ajax.open("POST", "showStock.php",true);
      ajax.onreadystatechange=function() {
      if (ajax.readyState==4) {
      divResultado.innerHTML = ajax.responseText
    
      }
      }
      ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      ajax.send("serial_desde="+serial_desde+"&serial_hasta="+serial_hasta+"&serie="+serie)  


}


function agregar_tkpass(){
	accion="agregar_tkpass";
	divResultado = document.getElementById('resultado_tkpass');
	
	var eliminar = confirm("Esta seguro de generar un codigo de autorizacion?");
	if ( eliminar ) {
		ajax=objetoAjax();
		ajax.open("GET", "../PHP/PROCESO_ajax.php?accion="+accion);
		divResultado.innerHTML= '<div style="width:500px; text-align:left;float:left; margin:10px 0px 10px 50px;"><img src="../Imagenes/load.gif"></div>';
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}

		ajax.send(null)
	}
}



function eliminar_promocion(id){
	accion="eliminar_promocion";
	divResultado = document.getElementById('resultado_listar');
	
	var eliminar = confirm("Esta seguro de eliminar la promocion?")
	if ( eliminar ) {
		ajax=objetoAjax();
		ajax.open("GET", "PROCESO_ajax.php?id="+id+"&accion="+accion);
		divResultado.innerHTML= '<div style="width:970px; text-align:center;float:left; margin:10px 0px 10px 0px;"><img src="imagenes/load.gif"></div>';
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}

		ajax.send(null)
	}
}

function eliminar_stock(id){
	accion="eliminar_stock";
	divResultado = document.getElementById('resultado');
	
	var eliminar = confirm("Esta seguro de eliminar el stock?")
	if ( eliminar ) {
		ajax=objetoAjax();
		ajax.open("GET", "PROCESO_ajax.php?id="+id+"&accion="+accion);
		divResultado.innerHTML= '<div style="width:970px; text-align:center;float:left; margin:10px 0px 10px 0px;"><img src="imagenes/load.gif"></div>';
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}

		ajax.send(null)
	}
}

function cargaTTS(){

   divResultado = document.getElementById('showTTS');
  //divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load-input.gif"></div>';
  
  var cantidad = document.getElementById("cantidad").value;
  var codigo_suc=document.getElementById("codigo_suc").value;

  
  ajax=objetoAjax();
  ajax.open("POST", "showTTS.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("cantidad="+cantidad+"&codigo_suc="+codigo_suc)
    
}

function cargar_pulso(){
	
   divResultado = document.getElementById('ReloadThis');
  divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';

  var reserva = document.getElementById("reserva").value;
  var fecha=document.getElementById("datepicker").value;

  ajax=objetoAjax();
  ajax.open("POST", "CONSULTA_pulso.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("reserva="+reserva+"&fecha="+fecha)
      
}

function cargaPASS(){
	
   divResultado = document.getElementById('showPass');
 // divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';

  var codAutoriza = document.getElementById("codAutoriza").value;

  //alert(codAutoriza);
  ajax=objetoAjax();
  ajax.open("POST", "showPass.php",true);
  ajax.onreadystatechange=function() {
      if (ajax.readyState==4) {
        divResultado.innerHTML = ajax.responseText
      }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("codAutoriza="+codAutoriza)
      
}

function consulta_voucher(){
	
   divResultado = document.getElementById('resultado_listar');
   divResultado.innerHTML= '<div style="width:0px; margin-left:380px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';

  var pasajero=document.form_voucher.pasajero.value;
  var voucher=document.form_voucher.voucher.value;
  var fecha=document.form_voucher.fecha.value;
  var excursion=document.form_voucher.excursion.value;

  accion="consulta_voucher";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
      if (ajax.readyState==4) {
        divResultado.innerHTML = ajax.responseText
      }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("pasajero="+pasajero+"&fecha="+fecha+"&excursion="+excursion+"&accion="+accion+"&voucher="+voucher)
      
}


function consulta_webpay(){
	
   divResultado = document.getElementById('resultado_listar');
   divResultado.innerHTML= '<div style="width:0px; margin-left:380px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';

  var pasajero=document.form_voucher.pasajero.value;
  var fecha=document.form_voucher.fecha.value;
  var excursion=document.form_voucher.excursion.value;

  accion="consulta_webpay";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
      if (ajax.readyState==4) {
        divResultado.innerHTML = ajax.responseText
      }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("pasajero="+pasajero+"&fecha="+fecha+"&excursion="+excursion+"&accion="+accion)
      
}


function consulta_venta_internet(){
	
   divResultado = document.getElementById('resultado_listar');
   divResultado.innerHTML= '<div style="width:0px; margin-left:380px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';

  var pasajero=document.form_voucher.pasajero.value;
  var voucher=document.form_voucher.voucher.value;
  var fecha=document.form_voucher.fecha.value;


  accion="consulta_venta_internet";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
      if (ajax.readyState==4) {
        divResultado.innerHTML = ajax.responseText
      }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("pasajero="+pasajero+"&accion="+accion+"&voucher="+voucher+"&fecha="+fecha)
      
}

function ltrim(str, filter) {
  var i = str.length;
  filter || ( filter = '' );
  for (var k = 0; k < i && filtering(str.charAt(k), filter); k++);
  return str.substring(k, i);
}

function agregar_reserva(){
  divResultado = document.getElementById('resultado_listar');
  //divResultado.innerHTML= '<div style="width:670px; text-align:center;float:left; margin:10px 0px 10px 150px;"><img src="../Imagenes/load.gif"></div>';
	var tipo_excursion=document.agregaExcursion.tipo_excursion.value;
    
    	 var pasajero=document.agregaExcursion.pasajero.value;
         var apasajero=document.agregaExcursion.apasajero.value;
         pasajero = pasajero.trim();
         apasajero = apasajero.trim();
         
 
         var pasaporte=document.agregaExcursion.pasaporte.value;	
         var observacion=document.agregaExcursion.observacion.value;
         var nacionalidad=document.agregaExcursion.nacionalidad.value;
         var codigo_suc=document.agregaExcursion.codigo_suc.value;
         var cod_usuario=document.agregaExcursion.cod_usuario.value;
         var tipoPasajero=document.agregaExcursion.tipoPasajero.value; 
         var descuento=document.agregaExcursion.descuento.value; 
         var fecha_format=document.agregaExcursion.fecha_format.value;         
         var email = $("#email").val();
         var email2 = $("#email2").val();
         var edad = $("#edad").val();
         var tipoCliente = 0;
         
         
         
         if (tipoPasajero == "ADT" && edad == 0 )
         {
            alert("Seleccione Rango de Edad.");
            $("#edad").focus();
            return false;
         }
         
        
         if (email != '')
         {
            
            
            if (validarEmail(email) == false)
            {
                return false;
            }
            else
            {
                if (email != email2)
                {
                    alert("Confirmacion de correo Erronea.");
                    $("#email2").focus();
                    return false;
                }
            }
            
         }

         
         
         /*
         var exc_rut_obligatorio = ["15", "35", "52", "23", "8", "49"];
         var index = exc_rut_obligatorio.indexOf(tipo_excursion);
 
          if (index != -1 && pasaporte == "")
          {
             alert("El ingreso de rut es mandatoria para esta excursion");
             document.agregaExcursion.pasaporte.focus();
             return false;
          }
          */

         
         if (nacionalidad == 0)
         {
             alert("Seleccione Nacionalidad");
             document.agregaExcursion.nacionalidad.focus();
             return false;
         }

   

 	  accion="agregar_reserva";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
     	  if (ajax.readyState==4) {
         	  divResultado.innerHTML = ajax.responseText
              
             document.agregaExcursion.pasaporte.value = '';
             document.agregaExcursion.nacionalidad.disabled = true;  
             document.agregaExcursion.apasajero.value = '';
             document.agregaExcursion.pasajero.value = '';
             document.agregaExcursion.pasajero.focus();

     	  }
 	  }

     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("pasajero="+pasajero+"&apasajero="+apasajero+"&accion="+accion+"&pasaporte="+pasaporte+"&observacion="+observacion+"&nacionalidad="+nacionalidad+"&codigo_suc="+codigo_suc+"&tipo_excursion="+tipo_excursion+"&cod_usuario="+cod_usuario+"&tipoPasajero="+tipoPasajero+"&descuento="+descuento+"&fecha_format="+fecha_format+"&tipoCliente="+tipoCliente+"&email="+email+"&edad="+edad)
    // ajax.send("pasajero="+pasajero+"&accion="+accion+"&pasaporte="+pasaporte+"&codSerial="+codSerial+"&observacion="+observacion+"&nacionalidad="+nacionalidad+"&codigo_suc="+codigo_suc+"&tipo_excursion="+tipo_excursion+"&codAutoriza="+codAutoriza+"&cod_usuario="+cod_usuario+"&tipoPasajero="+tipoPasajero+"&descuento="+descuento+"&fecha_format="+fecha_format)
    


}



function eliminar_pasajero_reagendar(id){	
 
    accion="eliminar_pasajero_reagendar";
	var eliminar = confirm("Desea eliminar el pasajero?")
	if ( eliminar ) {
	   
       	divResultado = document.getElementById('resultado_listar');
        divResultado.innerHTML= '<div style="width:1000px; text-align:center;float:left; margin:10px 0px 10px 0px;"><img src="../Imagenes/load.gif"></div>';
   
		ajax=objetoAjax();
		ajax.open("POST", "PROCESO_ajax.php",true);
		//divResultado.innerHTML= '<div style="width:970px; text-align:center;float:left; margin:10px 0px 10px 0px;"><img src="imagenes/load.gif"></div>';
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
                calcular_total_montos();
			}
		}
          ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		  ajax.send("accion="+accion+"&id="+id)
	}
    else
    {
        return false;
    }
}



function eliminar_servicio(id, opcion, serial)
{

    accion="eliminar_servicio";
	var eliminar = confirm("Desea eliminar el pasajero con su servicio?")
	if ( eliminar ) {

	   
   	  divResultado = document.getElementById('resultado_listar');
      divResultado.innerHTML= '<div style="width:900px; text-align:center;float:left; margin:10px 0px 10px 0px;"><img src="../Imagenes/load.gif"></div>';
    debugger;
		ajax=objetoAjax();
		ajax.open("POST", "PROCESO_ajax.php",true);
		//divResultado.innerHTML= '<div style="width:970px; text-align:center;float:left; margin:10px 0px 10px 0px;"><img src="imagenes/load.gif"></div>';
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
                window.location.reload();

			}
		}
        /*
        if (opcion == 3)
        {
            calcular_descuento();
        }
        */
        
          ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		  ajax.send("accion="+accion+"&opcion="+opcion+"&serial="+serial+"&id="+id)
	}    
    else
    {
        return false;
    }
 
}

function eliminar_pasajero(id,cant,serial){	
  //alert(cant);

    accion="eliminar_pasajero";
	var eliminar = confirm("Desea eliminar el pasajero?")
	if ( eliminar ) {


	   	divResultado = document.getElementById('resultado_listar');
        divResultado.innerHTML= '<div style="width:1000px; text-align:center;float:left; margin:10px 0px 10px 0px;"><img src="../Imagenes/load.gif"></div>';
        
		ajax=objetoAjax();
		ajax.open("POST", "PROCESO_ajax.php",true);
		//divResultado.innerHTML= '<div style="width:970px; text-align:center;float:left; margin:10px 0px 10px 0px;"><img src="imagenes/load.gif"></div>';
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}

         
            if (cant == 1)
            {
                 document.CompraExcursion.lRecogida.value= 0;
                 document.getElementById('nacionalidad').value = 0;                 
                 document.agregaExcursion.nacionalidad.disabled = false;  
                 window.location.reload();  
            }
            
            }
          ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		  ajax.send("accion="+accion+"&serial="+serial+"&id="+id)
          
	}
    else
    {
        return false;
    }

    
}


function retornar_seriales(id){
	accion="retornar_seriales";
	divResultado = document.getElementById('resultado_listar2');

	var eliminar = confirm("Desea retornar los seriales TTS?")
	if ( eliminar ) {
		ajax=objetoAjax();
		ajax.open("GET", "PROCESO_ajax.php?id="+id+"&accion="+accion);
		divResultado.innerHTML= '<div style="width:970px; text-align:center;float:left; margin:10px 0px 10px 0px;"><img src="imagenes/load.gif"></div>';
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}
          ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		  ajax.send(null)
	}
}


function Buscar_Cod_Socio(){
  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:400px; text-align:center;float:left; margin:10px 0px 10px 40px;"><img src="../Imagenes/load.gif"></div>';
	
      rut=document.formCodSocio.rut.value;
      dv=document.formCodSocio.dv.value;

      accion="Buscar_Cod_Socio";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
     	  if (ajax.readyState==4) {
         	  divResultado.innerHTML = ajax.responseText
     	  }
 	  }
 	  
     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("accion="+accion+"&rut="+rut+"&dv="+dv)
	   
}


function listar_seriales_tts(){
  divResultado = document.getElementById('resultado_listar2');
  //divResultado.innerHTML= '<div style="width:670px; text-align:center;float:left; margin:10px 0px 10px 0px;"><img src="../Imagenes/load.gif"></div>';

  lote=document.listar_seriales.lote.value;
	
	//alert(sucursal);

	  accion="listar_seriales";
	  ajax=objetoAjax();
	  ajax.open("POST", "PROCESO_ajax.php",true);
	  ajax.onreadystatechange=function() {
    	  if (ajax.readyState==4) {
    	    divResultado.innerHTML = ajax.responseText
    	  }
	  }
	  
   ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
   ajax.send("accion="+accion+"&lote="+lote)
}

function agregar_seriales_tts(){
  divResultado = document.getElementById('result');
  divResultado.innerHTML= '<div style="width:640px; text-align:center;float:left; margin:10px 0px 10px 0px;"><img src="../Imagenes/load.gif"></div>';
	
	
	sucursal=document.agrega_seriales.sucursal.value;
    serie=document.agrega_seriales.serie.value;
    serial_desde=document.agrega_seriales.serial_desde.value;
    serial_hasta=document.agrega_seriales.serial_hasta.value;
	
	//alert(serie);

	  accion="agregar_seriales_tts";
	  ajax=objetoAjax();
	  ajax.open("POST", "PROCESO_ajax.php",true);
	  ajax.onreadystatechange=function() {
    	  if (ajax.readyState==4) {
    	    divResultado.innerHTML = ajax.responseText
    	  }
	  }
	  
   ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
   ajax.send("accion="+accion+"&sucursal="+sucursal+"&serie="+serie+"&serial_desde="+serial_desde+"&serial_hasta="+serial_hasta)
}


function consulta_transaccion(){
divResultado = document.getElementById('resultado_listar');
divResultado.innerHTML= '<div style="width:1000px; text-align:center;float:left; margin:50px 0px 10px 0px;"><img src="../Imagenes/load.gif"></div>';

     var pasajero=document.form1.pasajero.value;
   	 var fecha_rango_1=document.form1.fecha_rango_1.value;
     var fecha_rango_2=document.form1.fecha_rango_2.value;

   	for (i=0;i<document.form1.filtro.length;i++){ 
      	 if (document.form1.filtro[i].checked) 
         	 break; 
   	} 
   	var filtro = document.form1.filtro[i].value; 


 	  accion="consulta_transaccion";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
     	  if (ajax.readyState==4) {
         	  divResultado.innerHTML = ajax.responseText
	      
     	  }
 	  }
 	  
     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("pasajero="+pasajero+"&accion="+accion+"&fecha_rango_1="+fecha_rango_1+"&fecha_rango_2="+fecha_rango_2+"&filtro="+filtro)
	
}
//2
function consulta_transaccion2(){
divResultado = document.getElementById('resultado_listar');
divResultado.innerHTML= '<div style="width:1000px; text-align:center;float:left; margin:50px 0px 10px 0px;"><img src="../Imagenes/load.gif"></div>';

     var pasajero=document.form1.pasajero.value;
   	 var fecha_rango_1=document.form1.fecha_rango_1.value;
     var fecha_rango_2=document.form1.fecha_rango_2.value;

   	for (i=0;i<document.form1.filtro.length;i++){ 
      	 if (document.form1.filtro[i].checked) 
         	 break; 
   	} 
   	var filtro = document.form1.filtro[i].value; 


 	  accion="consulta_transaccion2";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
     	  if (ajax.readyState==4) {
         	  divResultado.innerHTML = ajax.responseText
	      
     	  }
 	  }
 	  
     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("pasajero="+pasajero+"&accion="+accion+"&fecha_rango_1="+fecha_rango_1+"&fecha_rango_2="+fecha_rango_2+"&filtro="+filtro)
	
}
function consulta_estadisticas(){
   
divResultado = document.getElementById('resultado_listar');
divResultado.innerHTML= '<div style="width:800px; text-align:center;float:left; margin:50px 0px 10px 0px;"><img src="../Imagenes/load.gif"></div>';

     var sucursal=document.formEstadistica.sucursal.value;
     var sucursal_res=document.formEstadistica.sucursal_res.value;
     
   	 var fecha_rango_1=document.formEstadistica.fecha_rango_1.value;
     var fecha_rango_2=document.formEstadistica.fecha_rango_2.value;
     var fop=document.formEstadistica.fop.value;
     

 	  accion="consulta_estadisticas";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
     	  if (ajax.readyState==4) {
         	  divResultado.innerHTML = ajax.responseText
	      
     	  }
 	  }
 	  
     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("fop="+fop+"&sucursal="+sucursal+"&sucursal_res="+sucursal_res+"&accion="+accion+"&fecha_rango_1="+fecha_rango_1+"&fecha_rango_2="+fecha_rango_2)
	
}

function consulta_ticket(){
divResultado = document.getElementById('resultado_listar');
divResultado.innerHTML= '<div style="width:800px; text-align:center;float:left; margin:50px 0px 10px 0px;"><img src="../Imagenes/load.gif"></div>';
    
     var sucursal=document.form_consulta_ticket.sucursal.value;
     var box = document.getElementById("select2").value;
     var estado=document.form_consulta_ticket.estado.value;
   	 var fecha_rango_1=document.form_consulta_ticket.fecha_rango_1.value;
     var fecha_rango_2=document.form_consulta_ticket.fecha_rango_2.value;

 	  accion="consulta_ticket";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
     	  if (ajax.readyState==4) {
         	  divResultado.innerHTML = ajax.responseText
	      
     	  }
 	  }
 	  
     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("sucursal="+sucursal+"&accion="+accion+"&fecha_rango_1="+fecha_rango_1+"&fecha_rango_2="+fecha_rango_2+"&box="+box+"&estado="+estado)

}

function agregar_descuentos(){
divResultado = document.getElementById('resultado');
divResultado.innerHTML= '<div style="width:500px; text-align:center;float:left; margin:0px 0px 0px 0px;"><img src="../Imagenes/load.gif"></div>';
    
     var tipodcto=document.form1.tipodcto.value;
     var tipo=document.form1.tipo.value;
     var dcto=document.form1.dcto.value;

 	  accion="agregar_descuentos";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}

     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("tipodcto="+tipodcto+"&tipo="+tipo+"&dcto="+dcto+"&accion="+accion)


}

function eliminar_descuento(id){
	accion="eliminar_descuento";
	divResultado = document.getElementById('resultado');


	var eliminar = confirm("Desea eliminnar el descuento?")
	if ( eliminar ) {
		ajax=objetoAjax();
		ajax.open("GET", "PROCESO_ajax.php?id="+id+"&accion="+accion);
		divResultado.innerHTML= '<div style="width:500px; text-align:center;float:left; margin:10px 0px 10px 0px;"><img src="imagenes/load.gif"></div>';
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}
          ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		  ajax.send(null)
	}
}


function agregar_grupos(){
divResultado = document.getElementById('resultado_listar');
divResultado.innerHTML= '<div style="width:500px; text-align:center;float:left; margin:50px 0px 0px 0px;"><img src="../Imagenes/load.gif"></div>';
    
     var nombre=document.frm_grupos.nombre.value;

 	  accion="agregar_grupos";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}

     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("nombre="+nombre+"&accion="+accion)


}

function eliminar_grupo(id, acc){
	divResultado = document.getElementById('resultado_listar');
    accion="retornar_seriales";
 

    accion="eliminar_grupo";
	var eliminar = confirm("Esta seguro de eliminar el grupo?")
	if ( eliminar ) {
		ajax=objetoAjax();
		ajax.open("GET", "PROCESO_ajax.php?id="+id+"&acc="+acc+"&accion="+accion);
		divResultado.innerHTML= '<div style="width:500px; text-align:center;float:left; margin:10px 0px 10px 0px;"><img src="../Imagenes/load.gif"></div>';
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}
          ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		  ajax.send(null)
	}
}
/*
function cantidad_pasajeros(){
divResultado = document.getElementById('msg_dispon');
//divResultado.innerHTML= '<div style="width:500px; text-align:center;float:left; margin:50px 0px 0px 0px;"><img src="../Imagenes/load.gif"></div>';

     var cantidad = document.form_cant_pax.cant_pax.value;
     var codigo = document.form_cant_pax.tipo_excursion.value;
     var fecha = document.form_cant_pax.fecha_format.value;

     

 	  accion="cantidad_pasajeros";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}

     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("cantidad="+cantidad+"&codigo="+codigo+"&fecha="+fecha+"&accion="+accion)
     


}
*/
function consulta_funicular()
{

     var tipo_pax = document.getElementById("tipo_pax").value;
     var tramo = document.getElementById("tramo").value;
     var precio = document.getElementById("precio").value;
     
     if (tipo_pax != 0 && tramo != 0 && precio != '')
     {
  
          divResultado = document.getElementById('resultado_listar');
          divResultado.innerHTML= '<div style="width:500px; text-align:center;float:left; margin:50px 0px 0px 0px;"><img src="../Imagenes/load.gif"></div>';
    
          accion="consulta_funicular";
     	  ajax=objetoAjax();
     	  ajax.open("POST", "PROCESO_ajax.php",true);
     	  ajax.onreadystatechange=function() {
    			if (ajax.readyState==4) {
    				//mostrar resultados en esta capa
    				divResultado.innerHTML = ajax.responseText
    			}
    		}
    
         ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
         ajax.send("tipo_pax="+tipo_pax+"&tramo="+tramo+"&precio="+precio+"&accion="+accion)
     }
     else
     {
        alert("Complete campos obligatorios a consultar");
        return;
     }


}

function consulta_ventas_funicular(){
divResultado = document.getElementById('resultado_listar');
divResultado.innerHTML= '<div style="width:1000px; text-align:center;float:left; margin:50px 0px 10px 0px;"><img src="../Imagenes/load.gif"></div>';


   	 var fecha_rango_1=document.form1.fecha_rango_1.value;
     var fecha_rango_2=document.form1.fecha_rango_2.value;


 	  accion="consulta_ventas_funicular";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
     	  if (ajax.readyState==4) {
         	  divResultado.innerHTML = ajax.responseText
	      
     	  }
 	  }
 	  
     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("accion="+accion+"&fecha_rango_1="+fecha_rango_1+"&fecha_rango_2="+fecha_rango_2)
	
}


function consulta_usuarios_funicular(){
divResultado = document.getElementById('resultado_listar');
divResultado.innerHTML= '<div style="width:500px; text-align:center;float:left; margin:50px 0px 10px 0px;"><img src="../Imagenes/load.gif"></div>';


   	 var rut=document.form1.rut.value;



 	  accion="consulta_usuarios_funicular";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
     	  if (ajax.readyState==4) {
         	  divResultado.innerHTML = ajax.responseText
	      
     	  }
 	  }
 	  
     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("accion="+accion+"&rut="+rut)
	
}

function agregar_promociones(){
divResultado = document.getElementById('resultado_listar');
divResultado.innerHTML= '<div style="width:1000px; text-align:center;float:left; margin:50px 0px 10px 0px;"><img src="../Imagenes/load.gif"></div>';


   	 var nombre=document.form.nombre.value;
     var descripcion=document.form.descripcion.value;3
     var cant=document.form.cant.value;
     var dias=document.form.dias.value;


 	  accion="agregar_promociones";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
     	  if (ajax.readyState==4) {
         	  divResultado.innerHTML = ajax.responseText
	      
     	  }
 	  }
 	  
     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("accion="+accion+"&nombre="+nombre+"&descripcion="+descripcion+"&cant="+cant+"&dias="+dias)
	
}

function consulta_gestion_stk(){
divResultado = document.getElementById('resultado');
divResultado.innerHTML= '<div style="width:500px; text-align:center;float:left; margin:0px 0px 0px 0px;"><img src="../Imagenes/load.gif"></div>';
    
     var fecha_desde=document.form1.fecha_rango_1.value;
     var fecha_hasta=document.form1.fecha_rango_2.value;
     
     //alert(fecha_desde);

 	  accion="consulta_gestion_stk";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}

     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("accion="+accion+"&fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta)


}

function reembolso()
{
divResultado = document.getElementById('resultado_listar');
divResultado.innerHTML= '<div style="width:500px; text-align:center;float:left; margin:0px 0px 0px 0px;"><img src="../Imagenes/load.gif"></div>';
    
     var boletaTTS = document.form.boletaTTS.value;
     var boletaTTK = document.form.boletaTTK.value;
     
     //alert(boletaTTK);

 	  accion="reembolso";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}

     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("accion="+accion+"&boletaTTS="+boletaTTS+"&boletaTTK="+boletaTTK)


}


function consulta_reembolso_adm()
{

divResultado = document.getElementById('resultado_listar');
divResultado.innerHTML= '<div style="width:500px; text-align:center;float:left; margin:0px 0px 0px 0px;"><img src="../Imagenes/load.gif"></div>';

  
  var fecha_desde = $("#fecha_desde").val();
  var fecha_hasta = $("#fecha_hasta").val();
  var estado = $("#estado").val();

 	  accion="consulta_reembolso_adm";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}

     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("accion="+accion+"&fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta+"&estado="+estado)
}


function consulta_rut_dist()
{
divResultado = document.getElementById('resultado');
divResultado.innerHTML= '<div style="width:500px; text-align:center;float:left; margin:0px 0px 0px 0px;"><img src="../Imagenes/load.gif"></div>';

  
     var rut = document.consulta_rut_distribuidor.rut.value;

 	  accion="consulta_rut_dist";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}

     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("accion="+accion+"&rut="+rut)
}

function carga_descuentos_especiales()
{
     var monto = $('#monto').val();
     var rango = $('#rango').val();
     
     if (monto == "")
     {
        alert("Monto es un campo obligatorio");
        $('#monto').focus();
        return false;
     }
     if (rango == 0)
     {
        alert("Rango es un campo obligatorio");
        $('#rango').focus();
        return false;
     }
     
     divResultado = document.getElementById('resultado');
    divResultado.innerHTML= '<div style="width:500px; text-align:center;float:left; margin:0px 0px 0px 0px;"><img src="../Imagenes/load.gif"></div>';


 	  accion="carga_descuentos_especiales";
 	  ajax=objetoAjax();
 	  ajax.open("POST", "PROCESO_ajax.php",true);
 	  ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}

     ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
     ajax.send("accion="+accion+"&monto="+monto+"&rango="+rango)
}


function eliminar_descuento_especial(id){
	divResultado = document.getElementById('resultado');
    divResultado.innerHTML= '<div style="width:500px; text-align:center;float:left; margin:0px 0px 0px 0px;"><img src="../Imagenes/load.gif"></div>';

    accion="eliminar_descuento_especial";

	var eliminar = confirm("Esta seguro de eliminar el descuento ?")
	if ( eliminar ) {
		ajax=objetoAjax();
		ajax.open("GET", "PROCESO_ajax.php?id="+id+"&accion="+accion);
		divResultado.innerHTML= '<div style="width:500px; text-align:center;float:left; margin:10px 0px 10px 0px;"><img src="../Imagenes/load.gif"></div>';
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText
			}
		}
          ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		  ajax.send(null)
	}
}

function listado_beneficio(){

  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';

  var fecha_desde = $("#datepicker1").val();
  var fecha_hasta = $("#datepicker2").val();
  var tipo_beneficio = $("#tipo_beneficio").val();

  var accion = "listado_beneficio";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta+"&tipo_beneficio="+tipo_beneficio+"&accion="+accion)
      
}


function listado_shopping(){

  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';

  var fecha = $("#datepicker1").val();
  var excursion = $("#excursion").val();

  var accion = "listado_shopping";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("fecha="+fecha+"&excursion="+excursion+"&accion="+accion)
      
}

function ventas_por_vendedor(){

  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:center; margin-left:210px;  "><img src="../Imagenes/load.gif"></div>';

  var fecha_desde = $("#datepicker1").val();
  var fecha_hasta = $("#datepicker2").val();

  var accion = "ventas_por_vendedor";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta+"&accion="+accion)
      
}

function listado_gestion_cuentas_cc()
{
   divResultado = document.getElementById('resultado_listar');

  var fecha_desde = $("#datepicker1").val();
  var fecha_hasta = $("#datepicker2").val();
  var distribuidor = $("#distribuidor").val(); 
  var check1 = $("#check1").is(":checked");
  var check2 = $("#check2").is(":checked");
  var check3 = $("#check3").is(":checked");
  var check4 = $("#check4").is(":checked");
  var check5 = $("#check5").is(":checked");
  var valida = 0;

  if($("#check1").is(':checked')) 
  {  
    valida = 1; 
    } 
    else if ($("#check2").is(':checked'))
    {  
        valida = 1;   
    } 
    else if ($("#check3").is(':checked'))
    {  
        valida = 1;   
    }
    else if ($("#check4").is(':checked'))
    {  
        valida = 1;   
    }   
  
  
  if (valida == 0)
  {
     alert("Seleccione al menos una casilla para consultar.")
     return false;
  }

      divResultado.innerHTML= '<div style="width:40px; text-align:center; margin-left:280px;  "><img src="../Imagenes/load.gif"></div>';
      var accion = "listado_gestion_cuentas_cc";
      ajax=objetoAjax();
      ajax.open("POST", "PROCESO_ajax.php",true);
      ajax.onreadystatechange=function() {
      if (ajax.readyState==4) {
      divResultado.innerHTML = ajax.responseText
    
      }
      }
      ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      ajax.send("fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta+"&distribuidor="+distribuidor+"&check1="+check1+"&check2="+check2+"&check3="+check3+"&check4="+check4+"&check5="+check5+"&accion="+accion)   

}

function listado_gestion_cuentas_pp()
{
    debugger;
   divResultado = document.getElementById('resultado_listar');

  var fecha_desde = $("#datepicker1").val();
  var fecha_hasta = $("#datepicker2").val();
  var distribuidor = $("#distribuidor").val(); 


      divResultado.innerHTML= '<div style="width:40px; text-align:center; margin-left:280px;  "><img src="../Imagenes/load.gif"></div>';
      var accion = "listado_gestion_cuentas_pp";
      ajax=objetoAjax();
      ajax.open("POST", "PROCESO_ajax.php",true);
      ajax.onreadystatechange=function() {
      if (ajax.readyState==4) {
      divResultado.innerHTML = ajax.responseText
    
      }
      }
      ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      ajax.send("fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta+"&distribuidor="+distribuidor+"&accion="+accion)   

}


function listado_gestion_cuentas_ra()
{

   divResultado = document.getElementById('resultado_listar');

  var fecha_desde = $("#datepicker1").val();
  var fecha_hasta = $("#datepicker2").val();
  var distribuidor = $("#distribuidor").val(); 


      divResultado.innerHTML= '<div style="width:40px; text-align:center; margin-left:280px;  "><img src="../Imagenes/load.gif"></div>';
      var accion = "listado_gestion_cuentas_ra";
      ajax=objetoAjax();
      ajax.open("POST", "PROCESO_ajax.php",true);
      ajax.onreadystatechange=function() {
      if (ajax.readyState==4) {
      divResultado.innerHTML = ajax.responseText
    
      }
      }
      ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      ajax.send("fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta+"&distribuidor="+distribuidor+"&accion="+accion)   

}

function listado_gestion_cuentas_ma()
{
   divResultado = document.getElementById('resultado_listar');

  var fecha_desde = $("#datepicker1").val();
  var fecha_hasta = $("#datepicker2").val();
  var distribuidor = $("#distribuidor").val(); 
  var check1 = $("#check1").is(":checked");
  var check2 = $("#check2").is(":checked");
  var check3 = $("#check3").is(":checked");
  var check4 = $("#check4").is(":checked");
  var check5 = $("#check5").is(":checked");
  var valida = 0;

  if($("#check1").is(':checked')) 
  {  
    valida = 1; 
    } 
    else if ($("#check2").is(':checked'))
    {  
        valida = 1;   
    } 
    else if ($("#check3").is(':checked'))
    {  
        valida = 1;   
    }
    else if ($("#check4").is(':checked'))
    {  
        valida = 1;   
    }   
  
  
  if (valida == 0)
  {
     alert("Seleccione al menos una casilla para consultar.")
     return false;
  }

      divResultado.innerHTML= '<div style="width:40px; text-align:center; margin-left:280px;  "><img src="../Imagenes/load.gif"></div>';
      var accion = "listado_gestion_cuentas_ma";
      ajax=objetoAjax();
      ajax.open("POST", "PROCESO_ajax.php",true);
      ajax.onreadystatechange=function() {
      if (ajax.readyState==4) {
      divResultado.innerHTML = ajax.responseText
    
      }
      }
      ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      ajax.send("fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta+"&distribuidor="+distribuidor+"&check1="+check1+"&check2="+check2+"&check3="+check3+"&check4="+check4+"&check5="+check5+"&accion="+accion)   

}

function listado_embarcados(){

  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';

  var excursion = $("#excursion").val();

  var accion = "listado_embarcados";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("excursion="+excursion+"&accion="+accion)
      
}

function reporte_hoteleros(){

  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';

  var tipo_reserva = $("#tipo_reserva").val();
  var fecha_desde = $("#datepicker1").val();
  var fecha_hasta = $("#datepicker2").val();
  
  var accion = "reporte_hoteleros";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta+"&tipo_reserva="+tipo_reserva+"&accion="+accion)   
      
}

function conta_excursiones_grl(){

  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';

  var fecha = $("#datepicker1").val();
  var excursion = $("#excursion").val();

  var accion = "conta_excursiones_grl";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("fecha="+fecha+"&excursion="+excursion+"&accion="+accion)
      
}

function lista_disponibilidad(){

  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';

  var fecha = $("#datepicker1").val();
  var excursion = $("#excursion").val();

  var accion = "lista_disponibilidad";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("fecha="+fecha+"&excursion="+excursion+"&accion="+accion)
      
}

function excursiones_hotel(){

  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';

  var excursion = $("#excursion").val();

  var accion = "excursiones_hotel";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("excursion="+excursion+"&accion="+accion)
      
}

function conta_excursiones_transfer(){

  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';

  var fecha = $("#datepicker1").val();
  var excursion = 20;

  var accion = "conta_excursiones_transfer";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("fecha="+fecha+"&excursion="+excursion+"&accion="+accion)
      
}

function stk_impresos(){


  var fecha_desde = $("#datepicker1").val();
  var fecha_hasta = $("#datepicker2").val();
  var sucursal = $("#sucursal").val();
  
  if (fecha_desde == "" || fecha_hasta == "")
  {
    alert("Ingrese Intervalo de Fechas");
    return false;
  }
  
  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';


  var accion = "stk_impresos";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
      ajax.onreadystatechange=function() {
          if (ajax.readyState==4) {
          divResultado.innerHTML = ajax.responseText
          }
      }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta+"&sucursal="+sucursal+"&accion="+accion)
      
}



function ventas_ejecutivo(){


  var fecha_desde = $("#datepicker1").val();
  var fecha_hasta = $("#datepicker2").val();
  
  if (fecha_desde == "" || fecha_hasta == "")
  {
    alert("Ingrese Intervalo de Fechas");
    return false;
  }
  
  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';


  var accion = "ventas_ejecutivo";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
      ajax.onreadystatechange=function() {
          if (ajax.readyState==4) {
          divResultado.innerHTML = ajax.responseText
          }
      }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta+"&accion="+accion)
      
}


function ventas_sucursales(){


  var fecha_desde = $("#datepicker1").val();
  var fecha_hasta = $("#datepicker2").val();
  
  if (fecha_desde == "" || fecha_hasta == "")
  {
    alert("Ingrese Intervalo de Fechas");
    return false;
  }
  
  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';


  var accion = "ventas_sucursales";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
      ajax.onreadystatechange=function() {
          if (ajax.readyState==4) {
          divResultado.innerHTML = ajax.responseText
          }
      }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta+"&accion="+accion)
      
}


function consulta_resumen(){



  var fecha_desde = $("#datepicker").val();
  var fecha_hasta = $("#datepicker1").val();
  
  if (fecha_desde == "" || fecha_hasta == "")
  {
    alert("Ingrese Intervalo de Fechas");
    return false;
  }
  
  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';


  var accion = "consulta_resumen";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
      ajax.onreadystatechange=function() {
          if (ajax.readyState==4) {
          divResultado.innerHTML = ajax.responseText
          }
      }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta+"&accion="+accion)
      
}
function consulta_mantenedor_qr(){

  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left; margin-top:10px;"><img src="../Imagenes/load.gif"></div>';

  var fecha = $("#datepicker1").val();

  var accion = "consulta_mantenedor_qr";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("fecha="+fecha+"&accion="+accion)
      
}


function agregar_productos_merch(){

  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left; margin-top:10px;"><img src="../Imagenes/load.gif"></div>';

  var codigo_manager = $("#codigo_manager").val();
  var nombre = $("#nombre").val();
  var descripcion = $("#descripcion").val();
  var precio = $("#precio").val();
  var cantidad = $("#cantidad").val();
  var codigo = $("#codigo").val();

  var accion = "agregar_productos_merch";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("codigo_manager="+codigo_manager+"&nombre="+nombre+"&descripcion="+descripcion+"&precio="+precio+"&cantidad="+cantidad+"&codigo="+codigo+"&accion="+accion)
      
}

function listar_productos_merch()
{
  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left; margin-top:10px;"><img src="../Imagenes/load.gif"></div>';


  var filtro = $("#filtros").val();

  if (filtro == 1)
  {
    var parametro = $("#codigo").val();
  }
  if (filtro == 2)
  {
    var parametro = $("#codigo_manager").val();
  }
  if (filtro == 3)
  {
    var parametro = $("#sucursal").val();
  }



  var accion = "listar_productos_merch";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("parametro="+parametro+"&accion="+accion+"&filtro="+filtro)
     
}

function modifica_productos_merch()
{

  divResultado = document.getElementById('resultado_modifica');
  divResultado.innerHTML= '<div style="width:40px; text-align:left; margin-top:10px;"><img src="../Imagenes/load.gif"></div>';

  var codigo_manager = $("#mod_cod_manager").val();
  var nombre = $("#mod_nombre").val();
  var descripcion = $("#mod_descripcion").val();
  var precio = $("#mod_precio").val();
  var cantidad = $("#mod_cantidad").val();
  var codigo = $("#mod_cod").val();
  var sucursal = $("#mod_sucursal").val();

  var accion = "modifica_productos_merch";

  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
    divResultado.innerHTML = ajax.responseText
      alert("Producto modificado correctamente");
      $("#formulario").show();
      $("#asignar").hide();
      $("#modificar").hide();
      $("#aceptar").click();
    }
  }


  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("codigo_manager="+codigo_manager+"&nombre="+nombre+"&descripcion="+descripcion+"&precio="+precio+"&cantidad="+cantidad+"&codigo="+codigo+"&sucursal="+sucursal+"&accion="+accion)
    
}

function asignar_productos_merch()
{

  divResultado = document.getElementById('resultado_asignar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left; margin-top:10px;"><img src="../Imagenes/load.gif"></div>';

  var codigo_manager = $("#as_cod_manager").val();
  var nombre = $("#as_nombre").val();
  var descripcion = $("#as_descripcion").val();
  var precio = $("#as_precio").val();
  var cantidad = $("#as_cantidad").val();
  var codigo = $("#as_cod").val();
  var sucursal = $("#as_sucursal").val();
  var suc = $("#as_suc").val();

  var accion = "asignar_productos_merch";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
    divResultado.innerHTML = ajax.responseText
      alert("Productos asignados Correctamente");
      $("#formulario").show();
      $("#asignar").hide();
      $("#modificar").hide();
      $("#aceptar").click();
    }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("codigo_manager="+codigo_manager+"&nombre="+nombre+"&descripcion="+descripcion+"&precio="+precio+"&cantidad="+cantidad+"&codigo="+codigo+"&sucursal="+sucursal+"&accion="+accion+"&suc="+suc)
    
}

function consulta_canje()
{
  divResultado = document.getElementById('resultado');
  divResultado.innerHTML= '<div style="width:40px; text-align:left; margin-top:10px;"><img src="../Imagenes/load.gif"></div>';

  var voucher = $("#voucher").val();


  var accion = "consulta_canje";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
    if (ajax.readyState==4) {
      divResultado.innerHTML = ajax.responseText

    }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("voucher="+voucher+"&accion="+accion)
   
}

function listado_informe_x(){

  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';

  var fecha_desde = $("#datepicker1").val();
  var fecha_hasta = $("#datepicker2").val();
  var sucursal = $("#sucursal").val();

  var accion = "listado_informe_x";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta+"&sucursal="+sucursal+"&accion="+accion)
      
}


function listado_cierres(){

  divResultado = document.getElementById('resultado_listar');
  divResultado.innerHTML= '<div style="width:40px; text-align:left;  "><img src="../Imagenes/load.gif"></div>';

  var fecha_desde = $("#datepicker1").val();
  var fecha_hasta = $("#datepicker2").val();
  var sucursal = $("#sucursal").val();

  var accion = "listado_cierres";
  ajax=objetoAjax();
  ajax.open("POST", "PROCESO_ajax.php",true);
  ajax.onreadystatechange=function() {
  if (ajax.readyState==4) {
  divResultado.innerHTML = ajax.responseText

  }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  ajax.send("fecha_desde="+fecha_desde+"&fecha_hasta="+fecha_hasta+"&sucursal="+sucursal+"&accion="+accion)
      
}

function anular_reserva_ma(bp){
  accion="anular_reserva_ma";
  divResultado = document.getElementById('resultado_listar');
  
  var eliminar = confirm("Esta seguro de anular la reserva Mayorista "+bp+" ?");
  if ( eliminar ) {
    ajax=objetoAjax();
    divResultado.innerHTML= '<div style="width:900px; text-align:center;"><img src="../Imagenes/load.gif"></div>';
    ajax.open("GET", "../PHP/PROCESO_ajax.php?accion="+accion+"&bp="+bp);
    ajax.onreadystatechange=function() {
      if (ajax.readyState==4) {
        //mostrar resultados en esta capa
        divResultado.innerHTML = ajax.responseText
      }
    }

    ajax.send(null)
  }
}



