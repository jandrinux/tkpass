<?php
session_start();
include("menus.php");
$_Util=new Util;

$usuario = base64_decode($_GET['u']);
$tipo_excursion = base64_decode($_GET['ex']);
$fecha = base64_decode($_GET['f']);
$fecha_format = base64_decode($_GET['df']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script language="javascript">
$(document).ready(function() {
     $("#botonExcel").click(function(event) {
     $("#datos_a_enviar").val( $("<div>").append( $("#Layer1").eq(0).clone()).html());
     $("#FormularioExportacion").submit();
});
});
</script>
</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">

  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Listado de Pasajeros</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo $nombre;?></span></td>
    </tr>  
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    
  </table>
  

  

<div id="Layer1" style="margin-top: 50px;">

<table  width="1200" border="0" cellpadding="0" cellspacing="0">
  <tr class="celdalazul">
    <td class="celdalazul_peque" width="auto">N&deg;</td>
    <td class="celdalazul_peque" width="auto">Hora</td>
    <td class="celdalazul_peque" width="auto">Vuelo</td>
    <td class="celdalazul_peque" width="auto"># Reserva</td>
    <td class="celdalazul_peque" width="auto">Pasajero</td>
    <td class="celdalazul_peque" width="auto">Lugar de Destino</td>
    <td class="celdalazul_peque" width="auto">Cantidad</td>
    <td class="celdalazul_peque" width="auto">Observaci&oacute;n</td>
    <td class="celdalazul_peque" width="auto">Boleta</td>
    <td class="celdalazul_peque" width="100">Fecha Boleta</td>
    <td class="celdalazul_peque" width="auto">Agente Cierre</td>
    <td class="celdalazul_peque" width="auto">Sucursal Venta</td>
  </tr>

<?php

    echo "<tr class='celdalcelesteRojo'>"; 
    
    $miConexion = new ClaseConexion;
    $miConexion->Conectar();
    $queryT1=$miConexion->EjecutaSP("Consulta_Detalle_Transfer","'".$fecha."' ");
    $i = 1;
    while ($rowT1 = mysql_fetch_assoc($queryT1))
    {
        

        echo "<td class='celdalCELEST' >$i</td>"; 
        echo "<td class='celdalCELEST' >".$rowT1['HORA_LLEGADA_RET']."</td>"; 
        echo "<td class='celdalCELEST' >".$rowT1['VUELO_LLEGADA_RET']."</td>"; 
        echo "<td class='celdalCELEST' >".$rowT1['VOUCHER_RE']."</td>";   
        echo "<td class='celdalCELEST' >".$rowT1['PASAJERO_RE']." ".$rowT1['APASAJERO_RE']."</td>";

    	echo "<td class='celdalCELEST' >".$rowT1['ubicacion']."</td>";
        echo "<td class='celdalCELEST' >".$rowT1['cantidad']."</td>";
        echo "<td class='celdalCELEST' >".$rowT1['OBSERVACION_RE']."</td>";
        echo "<td class='celdalCELEST' >".$rowT1['BOLETA_RE']."</td>";
        echo "<td class='celdalCELEST' >".$rowT1['FECHA_BOLETA_RE']."</td>";
        echo "<td class='celdalCELEST' >".$rowT1['LOGIN_USUA_CIERRE']."</td>";
        echo "<td class='celdalCELEST' >".$rowT1['SUCURSAL']."</td>";
               
    echo "<tr>";

        $i++;
    }
    mysql_free_result($queryT1); 
    mysql_close();

      
?>
</table>
<br />

<form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
    <input type="button"  class="BotonTurist_largo" id="botonExcel" value="Exportar a Excel" />
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar"  />
</form>

</div>

</body>
</html>