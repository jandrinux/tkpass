<?php
session_start();
ini_set('display_errors', 1);
date_default_timezone_set("America/Santiago");
require ("../Clases/ClaseConexion.inc.php");
require ("../Clases/ClaseUtil.inc.php");
include ("../functions/consulta_descuento.php");
include ("../functions/tkpass.php");
require ("../Clases/class.boletas_manager.php");
require ("SII/class.boletaGen.php");
require ("../Clases/Tickets.php");
include ("../Clases/class.bitacora.php");

$ObjTotalRedondeado = new BuscaDescuentos();
$miConexion = new ClaseConexion;
$_Util = new Util;
$ticket = new tickets;
$Obj_Tkpass = new tkpass();

$recogida = $_POST['lRecogida'];
$codigo_suc = $_POST['codigo_suc'];
$cod_usuario = $_SESSION['codigo_usu'];
$tipo_excursion = $_POST['tipo_excursion'];
$usuario = $_POST['usuario'];
$fecha_format = $_POST['fecha_format'];
$recogidaOtro = $_POST['lRecogidaOtro'];
$tipoReserva = $_POST['tipoReserva'];
$fop = $_POST['fop'];
$valor_adt = $_POST['valor_adt'];
$monto_total = $_POST['monto_total'];
$cantidad_adt = $_POST['total_adt'];
$voucherTBK = $_POST['voucherTBK'];
$tipo_tarjeta = $_POST['tipo_tarjeta'];
$ultimos_digitos = $_POST['ultimos_digitos'];
$actualizar_redondeo = false;
$total_pasajeros = $_POST['total_pax'];
$hora = date(" H:i:s");


if ($monto_total > 0)
{
    $diferencia_pagar = 1;
}
else
{
    $diferencia_pagar = 0;
}


if ($monto_total > 0)
{
    if ($fop == 'E') {
        $redondeo = $ObjTotalRedondeado->RedondearValor($monto_total);
        $dif = $monto_total - $redondeo;
        $resultado = ($dif / $cantidad_adt);
        $valor_actualizar = (integer)($valor_adt - $resultado);
        
        $actualizar_redondeo = true;
    }
    else
    {
        $valor_actualizar = $monto_total;
    }

}

if ($diferencia_pagar == 1)
{
    if ($tipoReserva == '' || $fop == '')
    {
        print("<script>alert('Complete los campos requeridos marcados con (*)');</script>");
        echo '<script>parent.window.location.reload(true);</script>';
        $reservado = false;
    }
    else
    {
        if ($fop == 'C')
        {
            if ($ultimos_digitos == "" || $tipo_tarjeta == "" || $voucherTBK == "")
            {
                print("<script>alert('Complete los campos requeridos marcados con (*)');</script>");
                echo '<script>parent.window.location.reload(true);</script>';
                $reservado = false;
            }
        }
        if ($fop == 'D')
        {
            if ($voucherTBK == "" )
            {
                print("<script>alert('Complete los campos requeridos marcados con (*)');</script>");
                echo '<script>parent.window.location.reload(true);</script>';
                $reservado = false;
            }
        }
        
         $reservado = true;
    }
}
else
{
    $reservado = true;
}


if ($reservado == true)
{
  include('add_carrito_reagendar.php');
  include('finaliza_compra_reagendar.php');

      if ($compra_exitosa == true)
      {
        unset($_SESSION['voucher_a_reagendar']);
        unset($_SESSION['passed']);
        include ('elimina_registros_temporales.php'); 
        header("Location:".$link);
      }
    
}

?>
