<?php
session_start();
require ("../Clases/ClaseConexion.inc.php");
include("../functions/consulta_descuento.php");

$Busca_dscto = new BuscaDescuentos(); 

function transforma_fecha_sql($fecha){
    
    $fecha_sql = explode("-", $fecha);
    $anio=$fecha_sql[2]; // piece1
    $mes=$fecha_sql[1]; // piece2
    $dia=$fecha_sql[0]; // piece2
    $fecha = $anio."/".$mes."/".$dia;
    return $fecha;
}


?>

<html>
<head>
<link href="../estilos1.css" rel="stylesheet" type="text/css"/>
<link type='text/css' href='../../css/basic_simplemodal.css' rel='stylesheet' media='screen' /><!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>

<script type='text/javascript' src='../../js/jquery.simplemodal.js'></script>
<script type='text/javascript' src='../../js/basic_simplemodal.js'></script>

<script type="text/javascript">

$( document ).ready(function() {
  total_adulto = 0;
  total_chd = 0;
  
});



function reset()
{
    $('#redondeo').val(0);
    $('#afecto').val(0);
    $('#total').val(0);
    $('#exento').val(0);

}


function info_boleta_tr(){
    
   var precio = $('#precio').val(); 
   var select = $('#select_fop').val(); 
   var afecto = $('#afecto').val(); 
   var total = $('#precio').val(); 
   var redondeo = $('#redondeo').val(); 
   var exento = $('#exento').val(); 
   var sucursal = <?=$_SESSION['codigo_suc']?>;
   

  if(select == "E")
  {
      
    var tamano = (total.length)
    
    
    if (total > '0')
    {
        if (tamano == 4)
        {
            numbers = total.substring(1);
        }
        if (tamano == 5)
        {
            numbers = total.substring(2);
        }
        if (tamano == 6)
        {
            numbers = total.substring(3);
        }
        if (tamano == 7)
        {
            numbers = total.substring(4);
        }
        if (tamano == 8)
        {
            numbers = total.substring(5);
        }
        
    
        if (numbers < 500)
        {
            if (numbers < 100)
            {
                numbers = numbers.substr(1,2);
            }
            dif = numbers;
            
        }
        
        if (numbers > 500)
        {
            cifra = 500;
            dif = parseInt(numbers) - parseInt(cifra);
    
        }
        if (numbers == 0)
        {
            dif = 0;
        }
        

        var total_pagar = parseInt(total) - parseInt(dif);

        $('#redondeo').val(dif);
        $('#total').val(total_pagar);
        $('#redondeo').show();
        $("#cod_auto").hide();

        document.frmAgrupar.exento.value=exento-dif;
        
    }
    else
    {
        reset();
        $('#redondeo').hide();
        $("#cod_auto").show();
    }
    
    $("#for_digits").hide();
    $("#tarjeta").hide();
    
  }
  else
  {
    
    if (total > '0')
    {
      
        var j = 0;
        var cantidad = $("#cantidad").val();
        var total_afecto = 0;
        
        for (var i = 1; i <= cantidad; i++)
        {
          var names2 = "monto_afecto" + i;
          var n = "check" + i;
        
           if (document.getElementById(n).checked)
           {
                j++;
                var v2 = document.getElementById(names2).value;
                total_afecto = parseInt(total_afecto) + parseInt(v2);
            }
        }
        
        a = parseInt(precio) - (parseInt(afecto)+parseInt(exento));
        $("#afecto").val(total_afecto);
        $("#exento").val(parseInt(exento)+parseInt(a));
        $("#total").val($("#precio").val());
        $("#redondeo").hide();
        $("#cod_auto").show();
        
    }
    else
    {
        
        $("#afecto").val(total_afecto);
        $("#total").val(0);
        $("#redondeo").hide();
        $("#cod_auto").hide();
    }
    
        if (select == 'C')
        {
            $("#for_digits").show();
            $("#tarjeta").show();
        }
        else
        {
            $("#for_digits").hide();
            $("#tarjeta").hide();
        }
 
  }    
}



function Accion_anular()
{
  $("#pagar").hide();
    document.getElementById("observacion").style.visibility = "visible";
    document.getElementById("observacion").focus();
    
    if (document.getElementById("observacion").style.visibility == "visible" && document.getElementById("observacion").value != '')
    {
        document.getElementById("var").value = "1";
        document.frmAgrupar.submit();
    }
    
}



</script>
</head>
<body>

<div id='basic-modal'>
    <input type='button' name='wpc' id="wpc" class='basic' style="display: none;"/>
</div>

<form name="frmAgrupar" method="post" action="pagar_grupo_servicios.php">
    <table width="510" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="2" class="celdalazul">Servicio a pagar </td>
      </tr>
    <?
    $i=0;
    $miConexion= new ClaseConexion;
    $miConexion->Conectar();
    $queryServicio=$miConexion->EjecutaSP("Consulta_Agrupa_Voucher","'".$_GET['v']."'");
    $res = mysql_num_rows($queryServicio);
    if ($res==0)
    {
        print("<script>alert('Boarding Pass ingresado, incorrecto');</script>");
        print("<script>window.close();</script>");
    }
    while ($rowServicio= mysql_fetch_assoc($queryServicio))
    {       
            
            $i = $i+1;
            $tipo = $rowServicio['TIPO_RE']; 
            if ($rowServicio['TIPO_PASAJERO'] == 'ADT')
            {
                $valor_adt = $rowServicio['PRECIO_RE'];
            
                ?>
                  <tr class="celdalCELEST">
                   <input type="hidden" id="precio_re<?=$i?>" value="<? if ($tipo == 3){ echo ($rowServicio['PRECIO_RE']-$rowServicio['COMISION_RE']); }else{ echo ($rowServicio['PRECIO_RE']); } ?>"/>
                   <input type="hidden" id="monto_afecto<?=$i?>" value="<? echo $rowServicio['VALOR_EXENTO']; ?>"/>
                    <td width="34" class="celdalCELEST"><input type="checkbox" id="check<?=$i?>" name="check[]" checked="true" value="<? echo $rowServicio['CODIGO_RE']; ?>" onclick="validar();" /></td>
                    <td width="302" class="celdalCELEST"><label><? echo $rowServicio['APASAJERO_RE']. " $ "; if ($tipo == 3){ echo number_format($rowServicio['PRECIO_RE']-$rowServicio['COMISION_RE']); }else{ echo number_format($rowServicio['PRECIO_RE']); } echo "  - ".transforma_fecha_sql($rowServicio['FECHA_RE']);   ?></label></td>
                    
                  </tr>
                <?  

                
                if ($tipo == 3)
                {
                    $SUBtotal = $rowServicio['PRECIO_RE']-$rowServicio['COMISION_RE'] + $SUBtotal;
                }
                else
                {
                    $SUBtotal = $rowServicio['PRECIO_RE'] + $SUBtotal;
                }
    	  }
          else
          {

               ?>
                  <tr class="celdalCELEST">
                   <input type="hidden" id="precio_re<?=$i?>" value="<? if ($tipo == 3){ echo ($rowServicio['PRECIO_RE']-$rowServicio['COMISION_RE']); }else{ echo ($rowServicio['PRECIO_RE']); } ?>"/>
                   <input type="hidden" id="monto_afecto<?=$i?>" value="0"/>
                    <td width="34" class="celdalCELEST"><input type="checkbox" id="check<?=$i?>" name="check[]" checked="true" value="<? echo $rowServicio['CODIGO_RE']; ?>" onClick="validar();" /></td>
                    <td width="302" class="celdalCELEST"><label><? echo $rowServicio['APASAJERO_RE']. " $ "; if ($tipo == 3){ echo number_format($rowServicio['PRECIO_RE']-$rowServicio['COMISION_RE']); }else{ echo number_format($rowServicio['PRECIO_RE']); } echo "  - ".transforma_fecha_sql($rowServicio['FECHA_RE']);   ?></label></td>
                    
                  </tr>
                <?  
                
                if ($tipo == 3)
                {
                    $SUBtotal = $rowServicio['PRECIO_RE']-$rowServicio['COMISION_RE'] + $SUBtotal;
                }
                else
                {
                    $SUBtotal = $rowServicio['PRECIO_RE'] + $SUBtotal;
                }
          }

       $suma_precio = $rowServicio['PRECIO_RE']	+ $suma_precio;	
    }

    $total = $rowServicio['TOTAL'];

    mysql_free_result($queryServicio); 
    mysql_close();

    $redondeo = $Busca_dscto->RedondearValor($suma_precio);
    $diferencia = $suma_precio - $redondeo;
    ?>

    </table>

    <table width="510" border="0" cellpadding="0" cellspacing="0">

      <tr class="celdalCELEST">
        <td width="120" class="celdalCELEST"><label>Forma de pago: *</label></td>
        <td class="celdalCELEST">
            <select name="fop"  onchange="info_boleta_tr();" id="select_fop" class="seleccionTurist_180" >
            <?
            echo "<option value='C' selected  >TARJETA DE CREDITO</option>";
            echo "<option value='D' >TARJETA DE DEBITO</option>";
            echo "<option value='E' >EFECTIVO</option>";
            ?>
            </select>
        </td>
      </tr>

      <tr class="celdalCELEST">
        <input type="hidden" value="<? echo $i; ?>" id="cantidad" name="cantidad" />
        <td width="120" class="celdalCELEST"><label>Total:</label></td>
        <td class="celdalCELEST"><input type="text" readonly="" id="precio" name="precio" /></td>
      </tr>
      <tr class="celdalCELEST">
        <td width="120" class="celdalCELEST"><label>Descuento redondeo:</label></td>
        <td  class="celdalCELEST"><input type="text"  name="redondeo" id="redondeo" style="visibility: hidden;" onChange="Redondeo();" value="" readonly /></td>
      </tr>
      <tr class="celdalCELEST">
        <td width="120" class="celdalCELEST"><label>Total a pagar:</label></td>
        <td class="celdalCELEST"><input type="text"  name="total" id="total" readonly=""/></td>
      </tr>
      <tr class="celdalCELEST">
        <td width="120" class="celdalCELEST"><label>Total Exento TTS:</label></td>
        <td class="celdalCELEST">
        <input type="text"  name="exento" value="" id="exento" readonly />

      </tr>
      <tr class="celdalCELEST">
        <td width="120" class="celdalCELEST"><label>Total Afecto (Turistik):</label></td>
        <td class="celdalCELEST">
        <input type="text"  name="afecto" id="afecto" value="" readonly />
      </tr>
      <tr class="celdalCELEST" id="cod_auto">
        <td width="120" class="celdalCELEST"><label>C&oacute;digo de Autorizaci&oacute;n: (*)</label></td>
        <td class="celdalCELEST">
        <input type="text"  name="tbk" id="tbk" value="" />
        </td>
      </tr>
      
        <tr class="celdalCELEST" id="tarjeta">
        <td class="celdalCELEST">Tipo de Tarjeta: (*)</td>
        <td class="celdalCELEST">
            <select name="tipo_tarjeta" required="" title="Seleccione Tarjeta" id="tipo_tarjeta" class="seleccionTurist_180" >
            <option value="" selected>-- SELECCIONE --</option>
            <?
            echo "<option value='MASTERCARD'>MASTERCARD</option>";
            echo "<option value='VISA' >VISA</option>";
            echo "<option value='AMERICAN EXPRESS'>AMERICAN EXPRESS</option>";
            echo "<option value='OTRO'>OTRO</option>";
            ?>
            </select>
        </td>
      </tr>
      
        <tr class="celdalCELEST" id="for_digits">
        <td class="celdalCELEST" >4 ultimos digitos: (*)</td>
        <td class="celdalCELEST"><input name="ultimos_digitos" id="ultimos_digitos"  onkeypress="return validarP3(event);" maxlength="4" class="seleccionTurist_180" type="text"/></td>
        
      </tr>
      
      <tr>
          <td  class="celdalCELEST"></td>
          <td colspan="1" class="celdalCELEST">
            <textarea id="observacion"  name="observacion" placeholder="Ingrese observación" style="visibility: hidden;" cols="30" rows="2"></textarea>
            <input type="hidden" value="0" name="var" id="var" />
          </td>
      </tr>



    <input name="subtotal" id="subtotal" type="hidden" value="<? echo $SUBtotal ?>" />

      <tr class="celdalCELEST">
        <td class="celdalazul">
            <input type="button" onClick="Accion_anular()" class="BotonTurist_Rojo" value="Anular" id="anular"  name="anular" />
        </td>
        <td colspan="2" class="celdalazul">
        <p align="right">
         <input type="button" onClick="Pagar()" class="BotonTurist_Celest" value="Pagar" name="pagar"  id="pagar"/>
        </p>
        </td>
      </tr>

    </table>

<input type="hidden" value="" name="cantidad_adt" id="cantidad_adt" />
<input type="hidden" value="<?=$valor_adt?>" name="valor_adt" id="valor_adt" />
<input type="hidden" value="<?=$_GET['v']?>" name="boarding" id="boarding" />
<input type="hidden" value="<?=$tipo?>" name="tipo_reserva" id="tipo_reserva" />
<input type="hidden" value="<?=$diferencia;?>" name="diferencia" id="diferencia" />

</form>

<script type="text/javascript">

$( document ).ready(function() {
    validar();
});

function Pagar()
{
  
    var tipo = 1;
    var precio = $("#precio").val(); //document.getElementById('precio').value;
    var select = $("#select_fop").val(); //document.getElementById("select_fop").value;
    var tbk = $("#tbk").val(); //document.getElementById('tbk').value;
    var tipo_tarjeta = $("#tipo_tarjeta").val();
    var ultimos_digitos = $("#ultimos_digitos").val();
    var voucherTBK = $("#voucherTBK").val();
    
    if (precio > 0){
            
            
            if(select=="C")
            {
                if (tbk == "" || ultimos_digitos == "" || tipo_tarjeta == "")
                {
                    alert("Complete campos mandatorios (*)");
                    return false;
                }
            }
            if(select=="D")
            {
                if (voucherTBK == "")
                {
                    alert("Complete campos mandatorios (*)");
                    return false;
                }
            }

            //alert ('El pago se realizo exitosamente');
            $("#wpc").click();
            frmAgrupar.submit();
            

    }else{
        
        alert ('El total a pagar debe ser mayor a cero');
    }
    
    
}


function validar()
{
    debugger;

    var subtotal = $('#subtotal').val(); 
    var cantidad = $('#cantidad').val(); 

    var cont = 0;
    var chd = 0;
    var j = 0;
    var total = 0;
    var total_afecto = 0;
    var precio_adt = 0;
    var sucursal = <?=$_SESSION['codigo_suc']?>;
    document.frmAgrupar.precio.value=total;

    var precio_adt = 0;
    var precio_chd = 0;
    
    for (var i = 1; i <= cantidad; i++)
    {
        var names = "precio_re" + i; 
        var names2 = "monto_afecto" + i;
        var n = "check" + i; 
         
       if (document.getElementById(n).checked)
       {
        j++;
        var v = document.getElementById(names).value;
        var v2 = document.getElementById(names2).value;
        
            if (v2 != 0)
            {
                cont++;
                document.getElementById('cantidad_adt').value = cont;
                precio_adt = v;
            }
            else
            {
                chd++;
                precio_chd = v;
            }
        
            //total = parseInt(total) + parseInt(v); 
            total_afecto = parseInt(total_afecto) + parseInt(v2);
        }
       
    }
    
    total_adulto = parseInt(cont)*parseInt(precio_adt);
    total_chd = parseInt(chd)*parseInt(precio_chd);
    
    
    
    if (sucursal == 7 || sucursal == 15)
    {
        total = total_adulto + total_chd; 
        dscto = Math.round(total_adulto * 0.05);
        total = total - dscto;
        
    }
    else
    {
       total = total_adulto + total_chd; 
    }
    
    
    exento = total-total_afecto;


    $('#afecto').val(total_afecto);
    $('#precio').val(total);
    $('#exento').val(exento);
    $('#redondeo').val(total);
    $('#total').val(total);
    
    info_boleta_tr();
    
    return true;
 
}
</script>
</body>
</html>