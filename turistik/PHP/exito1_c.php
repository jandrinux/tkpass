<?php
//ini_set('display_errors', 1);
session_start();
require ("../Clases/ClaseConexion.inc.php");
require ("../Clases/ClaseUtil.inc.php");
require ("../Clases/Tickets.php");
require ("../functions/tkpass.php");

$voucher = $_GET['v'];
$respuesta = false;

$lista_voucher = explode("/",$voucher);
//$lista_voucher =array_filter($lista_voucher); // elimina array nulos

$_Util = new Util;
$Obj_Tkpass = new tkpass(); 
$ticket = new tickets;

$voucher = str_replace("/","",$voucher);
$ticket->Consulta_brazelete_reserva_excursion($voucher);
$serial = $ticket->serial;
$sucursal_venta = $ticket->sucursal_venta;
$cantidad_pax = $ticket->cant;
 
foreach($serial as $brazalete)
{
    if ($brazalete == '')
    {
        //encontro los campos brazaletes vacios por lo tanto es una reserva interntet en cuando esta en true
        $respuesta = true;
    }
}



 
if ($respuesta == false)  // los campos de brazalete en reserva excursion poseen correlativos
{
    $fecha_bp = $_Util->bp_to_date($voucher);

    $resultado = $ticket->Consulta_brazalete_si_impreso($voucher, $fecha_bp);
    //resultado = 0 el o los brazaletes no se han impreso
    
    if ($resultado == 0)
    {
        if ($_SESSION['codigo_suc'] != $sucursal_venta) // la reserva fue hecha en otra sucursal donde se quiere imprimir
        {

            $codigo_re = $Obj_Tkpass->Consulta_cantidad_pasajeros_ex_6_retorno($lista_voucher[0]);
            $cantidad_pax = count($codigo_re);
    
            $result = $Obj_Tkpass->Consulta_stock_brazaletes($_SESSION['codigo_suc'], $cantidad_pax);
            $cantidad_bra = count($result);
            
    
            if ($cantidad_pax >= 1)
            {
    
                if ( $cantidad_bra >= $cantidad_pax )
                {
                    
                    foreach($serial as $brazalete)
                    {
                        // se liberan los brazaletes de la sucursal donde se compro
                         $ticket->Actualiza_ticket_asignado($brazalete,0);      
                    }
                    
                    for($i=0; $i<$cantidad_pax; $i++)
                    {
                        // Actualiza serial brazaleete en la reserva excursion
                       $Obj_Tkpass->Actualiza_brazalete_reserva_excursion($codigo_re[$i],$result[$i]);
                    }
    
                    header('location:print/index.php?u='.$voucher.'');
       
                }
                else
                {
                    echo "<script language='javascript'>alert(\"No existe suficiente stock de brazaletes en esta sucursal\");</script>"; 
                    print("<script>window.parent.close();</script>");
                }
        
            }
            else
            {
                echo "<script language='javascript'>alert(\"Boarding Pass incorrecto\");</script>"; 
                print("<script>window.parent.close();</script>");
            }
        }
        else
        {
            header('location:print/index.php?u='.$voucher.'');
        }
    }
    
    if ($cantidad_pax == $resultado)
    {
        echo "<script language='javascript'>alert(\"Brazaletes ya fueron impresos, comuniquese con su coordinador\");</script>"; 
        print("<script>window.parent.close();</script>");
    }
    else
    {
        header('location:print/index.php?u='.$voucher.'');
    }


    
} // los campos brazaletes no contienen brazaletes asignados, por lo tanto no es una compra desde tkpass
else
{

    
        $codigo_re = $Obj_Tkpass->Consulta_cantidad_pasajeros_ex_6($lista_voucher[0]);
        $cantidad_pax = count($codigo_re);
    
        $result = $Obj_Tkpass->Consulta_stock_brazaletes($_SESSION['codigo_suc'], $cantidad_pax);
        $cantidad_bra = count($result);
    
    
        
        if ($cantidad_pax >= 1)
        {
        
            if ( $cantidad_bra >= $cantidad_pax )
            {
                // Actualiza serial brazaleete en la reserva excursion
                
                for($i=0; $i<$cantidad_pax; $i++)
                {
        
                   $Obj_Tkpass->Actualiza_brazalete_reserva_excursion($codigo_re[$i],$result[$i]);
                }
                
                header('location:print/index.php?u='.$voucher.'');
            
                    
            }
            else
            {
                echo "<script language='javascript'>alert(\"No existe suficiente stock de brazaletes en esta sucursal\");</script>"; 
                print("<script>window.parent.close();</script>");
            }
        
        }
        else
        {
             echo "<script language='javascript'>alert(\"Boarding Pass incorrecto\");</script>"; 

             print("<script>window.parent.close();</script>");
        }
    

}

?>