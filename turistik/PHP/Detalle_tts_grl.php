<?
//<s<ini_set('display_errors', 1);
require ("../Clases/ClaseConexion.inc.php");
require ("../Clases/ClaseUtil.inc.php");

$_Util=new Util;
$id = $_GET['id'];
$tipo = $_GET['tipo'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<link href="../../css/menu_small.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>

</head>
<body>
<table style=" color:#000; width:300; background-color:#FFF;" id="Exportar_a_Excel" >
<tr style="background:#CCC;">

    <td class="celdalazul_peque" width="90" style="text-align:center;">Boleta TTS</td>
    <td class="celdalazul_peque" width="80" style="text-align:center;">Estado</td>
    <td class="celdalazul_peque" width="90" style="text-align:center;">Fecha Uso</td>
    <td class="celdalazul_peque" width="80" style="text-align:center;">Usuario</td>
    <td class="celdalazul_peque" width="80" style="text-align:center;">Sucursal</td>
    <td class="celdalazul_peque" width="90" style="text-align:center;">Boleta Turistik</td>
    <td class="celdalazul_peque" width="80" style="text-align:center;">Precio Turistik</td>
    <td class="celdalazul_peque" width="80" style="text-align:center;">Precio TTS</td>
    <td class="celdalazul_peque" width="80" style="text-align:center;">Total</td>
    <td class="celdalazul_peque" width="80" style="text-align:center;">NCR</td>
    <td class="celdalazul_peque" width="80" style="text-align:center;">Link</td>
</tr>

<?php
    $miConexion= new ClaseConexion;
    $miConexion->Conectar();
    $query=$miConexion->EjecutaConsulta("SELECT S.SUCURSAL AS SUC,
                                                DB.ID_BOLETA,
                                                DB.FOLIO,
                                                DB.ESTADO,
                                                DB.FECHA_USO,
                                                DB.USUARIO_USO
                                         FROM DETALLES_BOLETAS_GRL DB 
                                         LEFT JOIN SUCURSAL S ON S.CODIGO_SUCURSAL = DB.SUCURSAL_USO 
                                         where 
                                         DB.ID_BOLETA = $id
                                         ");

	$orden=1;
    $i=1; 

	while($row_listar_stock=mysql_fetch_array($query)){
	   
		if($orden%2==0)
		$color='#D3D5FF';
		else
		$color='#F1F2FF';
                
		echo "<tr style='background-color:".$color."'>"; 
        
        
        if ($row_listar_stock['ESTADO'] == 0)
        {
            $estado = 'Sin Uso';
        }
        if ($row_listar_stock['ESTADO'] == 1)
        {
            $estado = 'Utilizado';
        }
        if ($row_listar_stock['ESTADO'] == 2)
        {
            $estado = 'Anulado';
        }
        
        
        $fecha_uso = $_Util->FormatearFechaHora($row_listar_stock['FECHA_USO']);
        if ($fecha_uso == '00-00-0000 00:00:00')
        {
            $fecha_uso = '';
        }
        
        echo "<td class='celdalPasajeros' style='text-align:center;'>".$row_listar_stock['ID_BOLETA']."-".$row_listar_stock['FOLIO']."</td>";
		echo "<td class='celdalPasajeros'>".$estado."</td>";
        echo "<td class='celdalPasajeros'>".$fecha_uso."</td>";
        echo "<td class='celdalPasajeros'>".$row_listar_stock['USUARIO_USO']."</td>";
        echo "<td class='celdalPasajeros'>".$row_listar_stock['SUC']."</td>";

        
        if ($row_listar_stock['FECHA_USO'] != '0000-00-00 00:00:00' && $row_listar_stock['ESTADO'] != 2) 
        {
            $fechaHoraDesformateada = $_Util->DesformatearFechaHora($row_listar_stock['FECHA_USO']);
            $fechaHoraDesformateada = str_replace("00:00:00","",$fechaHoraDesformateada);

               
                               
        $SQL = "SELECT RE.BOLETA_RE, sum(RE.PRECIO_RE) as precio, sum(EO.VALOR_EXENTO) as exento, RE.BOLETA_TTK
               FROM
               RESERVA_EXCURSION RE
               inner JOIN EXCURSION_ORIGEN EO ON EO.CODIGO_EO = RE.CODIGO_EO
               WHERE
               RE.BOLETA_RE = ".$row_listar_stock['FOLIO']." AND
               RE.FECHA_BOLETA_RE = '".$fechaHoraDesformateada."' ";
 
                 $miConexion->Conectar();  
                 $query2=$miConexion->EjecutaConsulta($SQL);
                               
                while($row=mysql_fetch_array($query2))
                {         
        
                    if($row['BOLETA_RE'] != '')
                    {
                         $boleta_ttk = $row['BOLETA_TTK'];
                         $precio = $row['precio'];
                         $afecto = $row['exento'];
                         
                         $precio_acum = $precio + $precio_acum;
                         $afecto_acum = $afecto + $afecto_acum;
                         $total = $precio_acum-$afecto_acum;

                    }


                }

                echo "<td class='celdalPasajeros'>".$boleta_ttk."</td>";
                echo "<td class='celdalPasajeros'>$".number_format($total)."</td>";
                echo "<td class='celdalPasajeros'>$".number_format($precio_acum-$total)."</td>";
                echo "<td class='celdalPasajeros'>$".number_format($precio_acum)."</td>";
                
            
            
                $total = 0;
                $afecto_acum = 0;
                $precio_acum = 0;
                $precio = 0;
                $afecto = 0;
                       
        }
        else
        {
            echo "<td class='celdalPasajeros'></td>";
            echo "<td class='celdalPasajeros'></td>";
            echo "<td class='celdalPasajeros'></td>";
            echo "<td class='celdalPasajeros'></td>";
        }
        

        echo "<td class='celdalPasajeros'></td>"; //ncr
        echo "<td class='celdalPasajeros'></td>";  //link
            
                
    $orden++;
    $i++;
	}
          
       

mysql_free_result($query); 
mysql_free_result($query2); 
mysql_close();	

?>
</table>
</body>
</html>