<?php
session_start();
include("menus.php");
$_Util=new Util;
$fecha = date('Y-m-d');
$excursion = $_GET['ex'];
$nombre = $_GET['nom'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script language="javascript">
$(document).ready(function() {
     $("#botonExcel").click(function(event) {
     $("#datos_a_enviar").val( $("<div>").append( $("#Layer1").eq(0).clone()).html());
     $("#FormularioExportacion").submit();
});
});
</script>
</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">

  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Detalle Embarcados</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo $nombre;?></span></td>
    </tr>  
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    
  </table>
  

  

<div id="Layer1" style="margin-top: 50px;">

<table  width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr class="celdalazul">
    <td class="celdalazul_peque" width="auto">N&deg;</td>
    <td class="celdalazul_peque" width="auto">Nombre</td>
    <td class="celdalazul_peque" width="auto">Apellido</td>
    <td class="celdalazul_peque" width="auto">Tipo Pax.</td>
    <td class="celdalazul_peque" width="auto">Lugar de Salida </td>
    <td class="celdalazul_peque" width="auto">Nacion.</td>
    <td class="celdalazul_peque" width="auto">Observaci&oacute;n</td>
    <td class="celdalazul_peque" width="auto">Pasaporte</td>
    <td class="celdalazul_peque" width="auto">Boleta Exenta</td>
    <td class="celdalazul_peque" width="auto">Boleta Afecta</td>
    <td class="celdalazul_peque" width="auto">Fecha Documento Boleta</td>
  </tr>

<?php

    echo "<tr class='celdalcelesteRojo'>"; 
    
    $miConexion = new ClaseConexion;
    $miConexion->Conectar();
    $queryT1=$miConexion->EjecutaSP("Consulta_Detalle_Embarcados","'".$fecha."', '".$excursion."'");
    $i = 1;
    while ($rowT1 = mysql_fetch_assoc($queryT1))
    {
        

        echo "<td class='celdalCELEST' >$i</td>"; 
        echo "<td class='celdalCELEST' >".$rowT1['PASAJERO_RE']."</td>"; 
        echo "<td class='celdalCELEST' >".$rowT1['APASAJERO_RE']."</td>"; 
        echo "<td class='celdalCELEST' >".$rowT1['TIPO_PASAJERO']."</td>"; 
        
        if($rowT1['LUGAR_RE']=='80')
        {
           $ubicacion="OTRO LUGAR: ".$rowT1['OTRO_LUGAR_RE'];
        }
        else
        {
            $ubicacion=$rowT1['ubicacion'];
        }
        
        echo "<td class='celdalCELEST' >".$ubicacion."</td>";
    	echo "<td class='celdalCELEST' >".$rowT1['nacionalidad']."</td>";
        echo "<td class='celdalCELEST' >".$rowT1['OBSERVACION_RE']."</td>";
        echo "<td class='celdalCELEST' >".$rowT1['PASAPORTE_RE']."</td>";
        echo "<td class='celdalCELEST' >".$rowT1['BOLETA_RE']."</td>";
        echo "<td class='celdalCELEST' >".$rowT1['BOLETA_TTK']."</td>";
        echo "<td class='celdalCELEST' >".$rowT1['FECHA_BOLETA_RE']."</td>";
               
    echo "<tr>";

        $i++;
    }
    mysql_free_result($queryT1); 
    mysql_close();

      
?>
</table>
<br />

<form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
    <input type="button"  class="BotonTurist_largo" id="botonExcel" value="Exportar a Excel" />
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar"  />
</form>

</div>

</body>
</html>