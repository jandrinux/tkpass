
var dCAL = {
	Fnc : null,
	iA : null,
	Multiple : 0,
	_estado : 0,
	Deshabilitado : {
		semana : '',
		dias : '1'
	},
	Opciones : {
		//path : 'calendarioflash/',
		path : '.js/js/js/',
		skin : '',
		idioma : 'es',
		id : 'dCAL_id',	
		fecha : {
			separador : "/",
			dia : new Date().getDate(),
			mes : new Date().getMonth(),
			anio : new Date().getFullYear(),
			inicio : '',
			final : '',
			totalanios : 1
		},
		colorcombo: '#FFE5BF'
	},
	Diccionario : {			
		en : {// Ingles
			semana : ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
			mes : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
		},
		pt : {// Portugues
			semana : ['Domingo', 'Segunda-feira ', 'Ter�a-feira ', 'Quarta-feira ', 'Quinta-feira', 'Sexta-feira', 'Sabado'],
			mes : ['Janeiro', 'Fevereiro', 'Mar�o', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
		},
		de : {// Aleman
			semana : ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
			mes : ['Januar', 'Februar', 'M�rz', 'April', 'Mag', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']
		},
		fr : {// Frances
			semana : ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
			mes : ['Janvier', 'F�vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'D�cembre']
		},
		de : {// Aleman
			semana : ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
			mes : ['Januar', 'Februar', 'M�rz', 'April', 'Mag', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']
		},			
		it : {// Italiano
			semana : ['Domenica', 'Lunedi', 'Martedi', 'Mercoledi', 'Giovedi', 'Venerdi', 'Sabato'],
			mes : ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre']
		},
		es : {// Espanol
			semana : ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
			mes : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
		}						
	},
	////(fecha_formateada, &#39;&#39;, &#39;&#39;, &#39;ControlFecha(2)&#39;)
	Init : function(f, i, b, fn){
		dCAL.Opciones.idioma =  i;
		dCAL.Fnc = fn;		
		//
		
		var _f = f.split(dCAL.Opciones.fecha.separador);
		//Bloqueo dias/semanas
		var _vB = b.split("#");
		if(_vB.length<2){
			if(_vB[0].indexOf(dCAL.Opciones.fecha.separador)>-1){
				dCAL.Deshabilitado.dias = _vB[0].split(","); 
			}else{
				dCAL.Deshabilitado.semana = _vB[0].split(",");
			} 
		}else{
			dCAL.Deshabilitado.semana = _vB[0].split(",");
			dCAL.Deshabilitado.dias = _vB[1].split(",");
		}
		if(_vB[0] == ""){			
			dCAL.Deshabilitado.semana = "";
			dCAL.Deshabilitado.dias = "";
		}
		//		
		dCAL.Crear.Calendario(_f[0], _f[1], Number(_f[2]));		
	},
	Crear : {
		Calendario : function(d, m, a){
			var _m = m.toInt();
			var _d = d.toInt();
			if(typeOf($(dCAL.Opciones.id)) == 'element') $(dCAL.Opciones.id).destroy();			
			//
			var _calP = new Element('div', {
				'class' : 'dCAL',
				'id' : dCAL.Opciones.id,
				'events' : {
					'mouseup' : function(e){
						e.stopPropagation();					
					},
					'contextmenu' : function(e){
						e.stop();	
					}
				}
			});
			$(document.body).grab(_calP);					
			var _cal = new Element('div', {
				'class' : 'dCAL_div'		
			});
			_calP.grab(_cal);
			////			
			dCAL.Crear.Header(_cal, _d, _m, a);
			//
			var _ta = new Element('table');
			_cal.grab(_ta);
			//
			dCAL.Crear.Semana(_ta);
			//
			var _tb = new Element('tbody', {'id' : 'dCAL_tbody1'});
			_ta.grab(_tb);
			//mes 1
			var mes_1 = d+'/'+m+'/'+a;
			dCAL.Crear.Mes(_m, a, 1, d, mes_1);
			
			if(dCAL.Multiple){
				var _taB = new Element('table');
				_cal.grab(_taB);
				//
				dCAL.Crear.Semana(_taB);
				//
				var _tb = new Element('tbody', {'id' : 'dCAL_tbody2'});
				_taB.grab(_tb);
				//mes 2
				var _vF = dCAL.Get.MesSiguiente(_m, a);
				dCAL.Crear.Mes(_vF[0], _vF[1], 2, d);
				//
				new Element('div', {'class' : 'dCAL_multiple'}).wraps(_ta).wraps(_taB);			
			}
			
			dCAL.Set.HTML.Header(_m, a);
									
			dCAL.Abrir();
		},
		Header : function(e, d, m, a){
			
			var _calH = new Element('div', {
				'class' : 'dCAL_header'
			});
			e.grab(_calH);
			//Navegacion > Anterior
			_calH.grab(
				new Element('a', {
					href : '#',
					rel:'A',
					html:'&laquo;',
					id:'dCAL_btn_a'
				}).addEvent('click', function(e){
					if(e) new Event(e).stop();
					dCAL.Set.HTML.Combo.Mes(this);
				})
			);
			if(dCAL.Multiple){
				_calH.grab(new Element('span', {id : 'dCAL_ftitulo_a'}));
				_calH.grab(new Element('span', {id : 'dCAL_ftitulo_s'}));
			}
			//////////Combos			
			//>Meses
			var _op = '';
			dCAL.Get.Idioma('mes').each(function(o, i){
				_op += '<option value="'+ (i+1) +'">'+o+'</option>\n';
			});
			_calH.grab(new Element('select', {	html : _op,'id' : 'dCAL_combo_mes'}).addEvent('change', function(){
					var _cm = this.get('value');
					var _ca = $('dCAL_combo_anio').get('value');					
					dCAL.Crear.Mes(_cm, _ca, 1);
					if(dCAL.Multiple){
						var _vF = dCAL.Get.MesSiguiente(_cm, _ca);
						dCAL.Crear.Mes(_vF[0], _vF[1], 2);						
					}
				}
			));			
			//>Anios			
			if(dCAL.Multiple){
				dCAL.Opciones.fecha.inicio = dCAL.Set.Redondear(d) + dCAL.Opciones.fecha.separador + dCAL.Set.Redondear(m) + dCAL.Opciones.fecha.separador + a;
				dCAL.Opciones.fecha.final = dCAL.Set.Redondear(d) + dCAL.Opciones.fecha.separador + dCAL.Set.Redondear(m) + dCAL.Opciones.fecha.separador + (a+dCAL.Opciones.fecha.totalanios);
			}else{
				alert(m+'/'+d);
				dCAL.Opciones.fecha.inicio = dCAL.Set.FormatearFecha((dCAL.Opciones.fecha.anio-100) + '.' + (dCAL.Opciones.fecha.mes+1) + '.' +dCAL.Opciones.fecha.dia);
				dCAL.Opciones.fecha.final = dCAL.Set.FormatearFecha((dCAL.Opciones.fecha.anio+5) + '.' + (dCAL.Opciones.fecha.mes+1) + '.' +dCAL.Opciones.fecha.dia);
			}
			if(dCAL.Deshabilitado.dias !== ""){
				var _vp =dCAL.Get.PeriodoMax();
				dCAL.Opciones.fecha.inicio = dCAL.Set.FechaFinal(_vp[0]);
				dCAL.Opciones.fecha.final = dCAL.Set.FechaFinal(_vp[1]);
			}
			var _fiV = dCAL.Opciones.fecha.inicio.split(dCAL.Opciones.fecha.separador);
			var _ffV = dCAL.Opciones.fecha.final.split(dCAL.Opciones.fecha.separador);
			//
			_op = '';
			for(var a=_fiV[2];a<=_ffV[2];a++){
				_op += '<option value="'+ a +'">'+a+'</option>\n';
			}
			_calH.grab(new Element('select', {	html : _op,'id' : 'dCAL_combo_anio'}).addEvent('change', function(){
					var _cm = $('dCAL_combo_mes').get('value');
					var _ca = this.get('value');					
					dCAL.Crear.Mes(_cm, _ca, 1);
					if(dCAL.Multiple){
						var _vF = dCAL.Get.MesSiguiente(_cm, _ca);
						dCAL.Crear.Mes(_vF[0], _vF[1], 2);						
					}					
				}	
			));
			//Navegacion > Siguiente
			_calH.grab(
				new Element('a', {
					href : '#',
					rel:'S',
					html:'&raquo;',
					id:'dCAL_btn_s'
				}).addEvent('click', function(e){
					if(e) new Event(e).stop();
					dCAL.Set.HTML.Combo.Mes(this);
				})
			);
		},
		Semana : function(e){
			var _th = new Element('thead');
			e.grab(_th);
			//
			var _s = '';
			var _c = '';
			dCAL.Get.Idioma('semana').each(function(o, i){
				_c = dCAL.Deshabilitado.semana[i] == '1' ? ' class="dCAL_semana_disable"' : ''; 
				_s += '<th title="' + o + '"' + _c +'>'+o.substr(0,2)+'</th>\n';
			});
			//
			_th.grab(new Element('tr', {html : _s}));	
		},
		Mes : function(m, a, i, d, mes_hoy){
			
			
			var dia_actual = d;					
			var anio_actual = a;	
						
			if(m < 10)
			{							
				var mes_actual = '0'+m;			
			}			
			
			
			var e = $('dCAL_tbody'+i).empty();
			var _f = new Date(a, m-1, 1);
			var _d = _f.getDay();
			var _udM = {
				anterior : dCAL.Get.UltimoDia(a, m-1),
				actual : dCAL.Get.UltimoDia(a, m),
				mesnombre : dCAL.Get.Idioma('mes')[m-1]
			}
			var _fiV = dCAL.Opciones.fecha.inicio.split(dCAL.Opciones.fecha.separador);
			var _ffV = dCAL.Opciones.fecha.final.split(dCAL.Opciones.fecha.separador);
			var _fLimite = {
				Inicio : {
					anio : 	_fiV[2].toInt(),
					mes : _fiV[1].toInt(),
					dia : _fiV[0].toInt()
				},
				Final : {
					anio : 	_ffV[2].toInt(),
					mes : _ffV[1].toInt(),
					dia : _ffV[0].toInt()
				}
			}			
			//
			var _s, _n, _css, _blockD, _blockM, _title;
			var _cd = 0;
			
			if(dCAL.Deshabilitado.dias !== "") _blockM = dCAL.Get.Periodo(_fLimite.Inicio.dia+dCAL.Opciones.fecha.separador+m+dCAL.Opciones.fecha.separador+a, [_fLimite.Inicio.dia+dCAL.Opciones.fecha.separador+_fLimite.Inicio.mes+dCAL.Opciones.fecha.separador+_fLimite.Inicio.anio+'>'+_fLimite.Final.dia+dCAL.Opciones.fecha.separador+_fLimite.Final.mes+dCAL.Opciones.fecha.separador+_fLimite.Final.anio]);
			//
			for(var x=1; x<=6; x++){
				_s = '';
				for(var y=1; y<=7; y++){					
					if(_d == 0 && x == 1){
						_n = _udM.anterior-(y*-1)-7;
						_s += '<td><span>'+_n+'</span></td>\n';
						_cd--;
					}else if(_cd >= _d && _cd<(_udM.actual+_d)){
						_n = (_cd-_d+1);
						//
						_css =  '';
						_title = dCAL.Get.Idioma('semana')[y-1] + ', ' + _n + ' ' + _udM.mesnombre + ' ' + a;
						//if(_n == dCAL.Opciones.fecha.dia && m-1 == dCAL.Opciones.fecha.mes && a == dCAL.Opciones.fecha.anio){
												
						
						/*if(_n == _n && m-1 == m && a == a){
							_css = 'dCAL_hoy';
						}*/
						if(_n+'/'+mes_actual+'/'+anio_actual ==  mes_hoy)
						{
							//alert('Fecha Correcta');
							_css = 'dCAL_hoy';
						}
						
						//Deshabilita dias
						if(dCAL.Deshabilitado.dias !== "") _blockM = dCAL.Get.Periodo(_n+dCAL.Opciones.fecha.separador+m+dCAL.Opciones.fecha.separador+a, dCAL.Deshabilitado.dias - 1);
						//
						if((dCAL.Deshabilitado.semana[y-1] == '1') || _blockM || (_n < _fLimite.Inicio.dia && a == _fLimite.Inicio.anio && m == _fLimite.Inicio.mes) || (_n > _fLimite.Final.dia && a == _fLimite.Final.anio && m == _fLimite.Final.mes)){			
							_s += '<td><span class="dCAL_dia_disable">'+_n+'</span></td>\n';
						}else{
							_s += '<td><a href="#" class="' + _css + '" title="' + _title + '" onclick="return dCAL.Set.Fecha('+ _n +','+ m +','+ a +');">'+_n+'</a></td>\n';					
						}
					}else{
						_n = (_cd < _d) ? y-_d+_udM.anterior : _cd-_udM.actual-_d+1;
						_s += '<td><span>'+_n+'</span></td>\n';
					}					
					_cd++;
				}
				var _tr = new Element('tr', {html : _s})
				e.grab(_tr);
			}
		}
	},	
	Ejecutar : function(){		
		if(typeOf(dCAL.Fnc) == "string") eval(dCAL.Fnc);
		dCAL.Fnc = null;				
	},
	Cerrar : function(){
		//dCAL.Set.CombosFix('show');
		if(dCAL._estado){
			$(dCAL.Opciones.id).destroy();
			dCAL._estado = 0;
			dCAL.Ejecutar();		
		}		
	},
	Abrir : function(){
		var _c = dCAL.Get.Pos(dCAL.iA);	
		$(dCAL.Opciones.id).setStyles({	
			left : _c[0]+'px',
			top : _c[1]+'px'
		});
		dCAL._estado = 1;

		dCAL.Set.CombosFix('hide');
	},
	Get : {
		Pos : function(a) {		
			return [a.getCoordinates().left, a.getCoordinates().bottom+1];
		},
		UltimoDia : function(a, m) {
			var _a = Number(a);
			var _m = Number(m);	
			if(_m>12){
				_m = 1;
				_a += 1;
			}else if(_m<1){
				_m = 12;
				_a -= 1;
			}
			//
			switch (_m) {
				case 1 : case 3 : case 5 : case 7 : case 8 : case 10 : case 12 : return 31;
				case 2 : return (_a % 4 == 0) ? 29 : 28;
			}
			return 30;
		},
		Idioma : function(k){
			var _i = dCAL.Opciones.idioma || 'es';
			return eval('dCAL.Diccionario.' + _i + '.' + k);
		},
		MesSiguiente : function(m, a){					
			var _a = Number(a);
			var _m = Number(m);				
			if(_m == 12){				
				_m = 1;
				_a += 1;
			}else if(_m<1){
				_m = 12;
				_a -= 1;
			}else{
				_m += 1;
			}
			return [_m, _a];
		},
		Periodo : function(f, ds){
			var _flag = 1;
			var _f = dCAL.Set.FechaInt(f);
			var _ld = ds.length;
			for(var d=0; d<_ld; d++){
				var _v = ds[d].split(">");
				if(_f>(dCAL.Set.FechaInt(_v[0])-1) && _f<(dCAL.Set.FechaInt(_v[1])+1)){
					_flag = 0;
				}
			}
			return _flag;
		},
		PeriodoMax : function(){
			var _vDD = [];
			var _dd = dCAL.Deshabilitado.dias;
			var _ld = _dd.length;
			for(var d=0; d<_ld; d++){	
				var _v = _dd[d].split(">");		
				_vDD.push(dCAL.Set.FechaInt(_v[0]));
				_vDD.push(dCAL.Set.FechaInt(_v[1]));
			}
			_vDD.sort();
			_max = _vDD[_vDD.length-1];
			_min = _vDD[0];
			return [_min, _max];
		}
	},
	Set : {
		CombosFix : function(t){
			if(Browser.ie6){				
				var _c = $(dCAL.Opciones.id).getCoordinates();
				$$('select').each(function(ele,ind){
					var _cs = ele.getCoordinates();
					if(_cs.top>=_c.top && _cs.left>=_c.left && _cs.bottom<=_c.bottom && _cs.right<=_c.right){
						//ele.fade(t);	
					}
				});
			}
		},
		//ENVIA FECHA
		Fecha : function(d, m, a, sn){
			var _f = dCAL.Set.Redondear(d) + dCAL.Opciones.fecha.separador + dCAL.Set.Redondear(m) + dCAL.Opciones.fecha.separador + a;			
			dCAL.iA.set({
				'value' : _f,
				'change' : SubmitFormulario()//window.setTimeout("location.reload(true)",6000) 
							
			});
			dCAL.Cerrar();	
			return false;		
		},
		HTML : {
			Header : function(m, a){
				var _op = 1
				if(dCAL.Multiple){
					var _fiV = dCAL.Opciones.fecha.inicio.split(dCAL.Opciones.fecha.separador);
					var _ffV = dCAL.Opciones.fecha.final.split(dCAL.Opciones.fecha.separador);
					var _fLimite = {
						Inicio : {
							anio : 	_fiV[2].toInt(),
							mes : _fiV[1].toInt(),
							dia : _fiV[0].toInt()
						},
						Final : {
							anio : 	_ffV[2].toInt(),
							mes : _ffV[1].toInt(),
							dia : _ffV[0].toInt()
						}
					}
					$('dCAL_ftitulo_a').set('html', dCAL.Get.Idioma('mes')[m-1] + ' ' + a);
					//
					var _vF = dCAL.Get.MesSiguiente(m, a);
					$('dCAL_ftitulo_s').set('html', dCAL.Get.Idioma('mes')[_vF[0]-1] + ' ' +  _vF[1]);					
					//
					$('dCAL_btn_a').setStyle('opacity', (m<=_fLimite.Inicio.mes && a == _fLimite.Inicio.anio) ? 0 : 1);
					$('dCAL_btn_s').setStyle('opacity', (m>=_fLimite.Final.mes-1 && a == _fLimite.Final.anio) ? 0 : 1);
					//
					_op = 0;
				}
				$('dCAL_combo_mes').set('value', m).setStyle('opacity', _op);
				$('dCAL_combo_anio').set('value', a).setStyle('opacity', _op);
				//
				if(Browser.ie && Browser.version<8) $$('div.dCAL_header')[0].setStyle('width', ((dCAL.Multiple) ? $$('div.dCAL_multiple')[0].getSize().x : $$('div.dCAL_div')[0].getSize().x)-2 +'px');
			},
			Combo : {
				Mes : function(e){
					var _e = $(e);
					var _t = _e.get('rel');
					var cm = $('dCAL_combo_mes');
					var ca = $('dCAL_combo_anio');
					//
					var _l = cm.options.length;
					var _si = cm.selectedIndex;
					//
					if(_t == 'S'){
						if(_si<_l-1){
							_si++;
						}else{
							_si = 0;
							dCAL.Set.HTML.Combo.Anio(_t, ca);
						}
					}else{
						if(_si == 0){
							_si = _l-1;
							dCAL.Set.HTML.Combo.Anio(_t, ca);
						}else{
							_si--;			
						}
					}
					//
					cm.highlight(dCAL.Opciones.colorcombo).selectedIndex = _si;
					
					dCAL.Crear.Mes(cm.get('value'), ca.get('value'), 1);
					
					dCAL.Set.HTML.Header(cm.get('value'), ca.get('value'));
					
					if(dCAL.Multiple){
						var _vF = dCAL.Get.MesSiguiente(cm.get('value'), ca.get('value'));
						dCAL.Crear.Mes(_vF[0], _vF[1], 2);												
					}
					
					return false;
				},
				Anio : function(t, ca){
					var _l = ca.options.length;
					var _si = ca.selectedIndex;
					var _v = Number(ca.get('value'));
					var _t = Number(ca.options[_si].text);
					//
					if(t == 'S'){
						if(_si<_l-1){
							_si++;
						}else{
							_op = new Element('option', {'value' : _v+1, 'text' : _t+1});
							_op.inject(ca);
							_si = _l;
						}
					}else{
						if(_si == 0){
							_op = new Element('option', {'value' : _v-1, 'text' : _t-1});
							_op.inject(ca, 'top');
							_si = 0;
						}else{
							_si--;
						}
					}
					//
					ca.selectedIndex = _si;
					$(ca).highlight(dCAL.Opciones.colorcombo);
				}
			}
		},
		Redondear : function(n){
			var _n = n.toInt();
			return _n<10 ? "0"+_n : _n;	
		},
		FormatearFecha : function(f){
			var _f = f.split('.');
			return dCAL.Set.Redondear(_f[2]) + dCAL.Opciones.fecha.separador + dCAL.Set.Redondear(_f[1]) + dCAL.Opciones.fecha.separador + _f[0];
		},
		FechaInt : function(f){
			var _f = f.split(dCAL.Opciones.fecha.separador);
			return Number(_f[2].toString() + dCAL.Set.Redondear(_f[1]).toString() + dCAL.Set.Redondear(_f[0]).toString());
		},
		FechaFinal : function(f){
			var _f = f.toString();
			return _f.slice(6, 8) + dCAL.Opciones.fecha.separador + _f.slice(4, 6) + dCAL.Opciones.fecha.separador + _f.slice(0, 4);
		}
	}
}

///////////-------

var Calendario = function(i, fi, la, d, fnc) {
	dCAL.Multiple = 0;
	dCAL.iA = $(i);
	var _arg = {
		d : d || '',
		fi : fi || dCAL.iA.get('value') || dCAL.Set.FormatearFecha(dCAL.Opciones.fecha.anio + '.' + (dCAL.Opciones.fecha.mes+1) + '.' +dCAL.Opciones.fecha.dia)
	}
	dCAL.Init(_arg.fi, la, _arg.d, fnc);
}

//var CalendarioV = function(r, i, i2, fi, la, d, fnc) {
//CAPTURO LOS PARAMETROS
//CalendarioV(true,document.form1.fecha,document.form1.fecha,&#39;&#39;,&#39;&#39;,&#39;ControlFecha(2)&#39;)

var CalendarioV = function(r, i, i2, fi, la, d, fnc) {
//var CalendarioV = function(r, i, i2, la, d, fnc) {
	var currentTime = new Date()
	var month = currentTime.getMonth() + 1
	var day = currentTime.getDate()
	var year = currentTime.getFullYear()
	//fi = day + "/" + month+ "/" + year;
	//alert(r+ i+ i2+ fa+ fm+ fd+ la+ d+ fnc);	
	//fi = fd+"/"+fm +"/"+fa;
	
	dCAL.Multiple = 1;
	dCAL.iA = $(i);
	var _arg = {
		d : d || '',
		fi : (r) ? fi : $(i2).get('value'),		
	}	
	dCAL.Init(_arg.fi, la, _arg.d, fnc);
	
}

window.addEvent('domready', function(){
	document.addEvent('mouseup', function(){
		dCAL.Cerrar();	
	}).getElement("head").grab(new Element('link', {
		'rel' : 'stylesheet',
		'type' : 'text/css',
		'href' : dCAL.Opciones.path + 'calendario.css',
		'media' : 'screen'
	})).grab(new Element('link', {
		'rel' : 'stylesheet',
		'type' : 'text/css',
		'href' : dCAL.Opciones.skin !== '' ? dCAL.Opciones.path + 'skins/' + dCAL.Opciones.skin + '.css' : dCAL.Opciones.path + 'skins/default.css',
		'media' : 'screen'
	}));
});