<?PHP
class BRAZALETES {
	var $ID_BRAZALETE;
	var $FOLIO;
	var $NUMERO;
	var $COD_USUA_PROMOTOR;
	var $ESTADO;
	var $COD_USUA_DISTRIBUIDOR;
	var $FECHA_ENTREGA;
	var $NUM_DOCUMENTO;
	var $FECHA_NUM_DOCUMENTO;
	var $conectas;
    var $VALOR;

	function __construct() {
		$this->conectas = new ClaseConexion();
		$this->conectas->Conectar(); 
	}
	public function Lista() {
		$sql = "select * from BRAZALETES";
		$res = $this->conectas->EjecutaConsulta($sql);
		while($row=mysql_fetch_array($res)) {
			$c = new BRAZALETES();
			$c->ID_BRAZALETE= $row["ID_BRAZALETE"];
			$c->FOLIO= $row["FOLIO"];
			$c->NUMERO= $row["NUMERO"];
			$c->COD_USUA_PROMOTOR= $row["COD_USUA_PROMOTOR"];
			$c->ESTADO= $row["ESTADO"];
			$c->COD_USUA_DISTRIBUIDOR= $row["COD_USUA_DISTRIBUIDOR"];
			$c->FECHA_ENTREGA= $row["FECHA_ENTREGA"];
			$c->NUM_DOCUMENTO= $row["NUM_DOCUMENTO"];
			$c->FECHA_NUM_DOCUMENTO= $row["FECHA_NUM_DOCUMENTO"];
			$array[] = $c;
		}
		return $array;
	}

	public function ListaJSON() {
		$data = $this->Lista();
		return json_encode($data);
	}

	public function ListarID_BRAZALETE($ID_BRAZALETEId) {
		$this->ID_BRAZALETE = $ID_BRAZALETEId;
		$sql = "select * from BRAZALETES where ID_BRAZALETE=$ID_BRAZALETEId";
		$res = $this->conectas->EjecutaConsulta($sql);
		while($row=mysql_fetch_array($res)) {
			$this->ID_BRAZALETE= $row["ID_BRAZALETE"];
			$this->FOLIO= $row["FOLIO"];
			$this->NUMERO= $row["NUMERO"];
			$this->COD_USUA_PROMOTOR= $row["COD_USUA_PROMOTOR"];
			$this->ESTADO= $row["ESTADO"];
			$this->COD_USUA_DISTRIBUIDOR= $row["COD_USUA_DISTRIBUIDOR"];
			$this->FECHA_ENTREGA= $row["FECHA_ENTREGA"];
			$this->NUM_DOCUMENTO= $row["NUM_DOCUMENTO"];
			$this->FECHA_NUM_DOCUMENTO= $row["FECHA_NUM_DOCUMENTO"];
		}
	}

	public function ListarID_BRAZALETEJSON($ID_BRAZALETEId) {
		$data = $this->Lista($ID_BRAZALETEId);
		return json_encode($data);
	}
	public function Where($vars) {
		$sql = "select * from BRAZALETES where $vars";
		$res = $this->conectas->EjecutaConsulta($sql);
		while($row=mysql_fetch_array($res)) {
			$c = new BRAZALETES();
			$c->ID_BRAZALETE= $row["ID_BRAZALETE"];
			$c->FOLIO= $row["FOLIO"];
			$c->NUMERO= $row["NUMERO"];
			$c->COD_USUA_PROMOTOR= $row["COD_USUA_PROMOTOR"];
			$c->ESTADO= $row["ESTADO"];
			$c->COD_USUA_DISTRIBUIDOR= $row["COD_USUA_DISTRIBUIDOR"];
			$c->FECHA_ENTREGA= $row["FECHA_ENTREGA"];
			$c->NUM_DOCUMENTO= $row["NUM_DOCUMENTO"];
			$c->FECHA_NUM_DOCUMENTO= $row["FECHA_NUM_DOCUMENTO"];
            $c->VALOR= $row["VALOR"];
			$array[] = $c;
		}
		return $array;
	}

	public function WhereJSON($vars) {
		$data = $this->Where($vars);
		return json_encode($data);
	}

	public function Add() {
		$sql = "INSERT INTO BRAZALETES (FOLIO,NUMERO,COD_USUA_PROMOTOR,ESTADO,COD_USUA_DISTRIBUIDOR,FECHA_ENTREGA,NUM_DOCUMENTO,FECHA_NUM_DOCUMENTO)"; 
		$sql.= "VALUES('".$this->FOLIO."','".$this->NUMERO."','".$this->COD_USUA_PROMOTOR."','".$this->ESTADO."','".$this->COD_USUA_DISTRIBUIDOR."','".$this->FECHA_ENTREGA."','".$this->NUM_DOCUMENTO."','".$this->FECHA_NUM_DOCUMENTO."')";
		$res = $this->conectas->EjecutaConsulta($sql);
		return "OK";
	}

	public function Update() {
		$sql = "Update  BRAZALETES set FOLIO='".$this->FOLIO."',NUMERO='".$this->NUMERO."',COD_USUA_PROMOTOR='".$this->COD_USUA_PROMOTOR."',ESTADO='".$this->ESTADO."',COD_USUA_DISTRIBUIDOR='".$this->COD_USUA_DISTRIBUIDOR."',FECHA_ENTREGA='".$this->FECHA_ENTREGA."',NUM_DOCUMENTO='".$this->NUM_DOCUMENTO."',FECHA_NUM_DOCUMENTO='".$this->FECHA_NUM_DOCUMENTO."' where ID_BRAZALETE='".$this->ID_BRAZALETE."' "; 
		$res = $this->conectas->EjecutaConsulta($sql);
		return "OK";
	}

	public function delete() {
		$sql= "delete from  BRAZALETES where ID_BRAZALETE='".$this->ID_BRAZALETE."' ";
		$res = $this->conectas->EjecutaConsulta($sql);
		return "OK";
	}
}
?>
