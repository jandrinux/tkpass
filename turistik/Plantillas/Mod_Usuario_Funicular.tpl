<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../estilos1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../../js/ui.core.js"></script>
<script type="text/javascript" src="../../js/ui.datepicker.js"></script>
<link type="text/css" href="../../css/demos.css" rel="stylesheet" />
<link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:16px;
	top:159px;
	width:823px;
	height:231px;
	z-index:13;
}
#Layer3 {
	position:absolute;
	left:21px;
	top:11px;
	width:314px;
	height:62px;
	z-index:14;
}
-->
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Modificar Usuario</title>
<style type="text/css">
<!--
#Layer2 {	position:absolute;
	left:703px;
	top:8px;
	width:132px;
	height:116px;
	z-index:1;
}
-->
</style>
<script type="text/JavaScript">
	

function ModificarUsuario()
	{
		
		document.form1.guardar.value="1";
		document.form1.submit();
	}

</script>
</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<form id="form1" name="form1" method="post" action="">
<div id="etiqutaprimera" style="position:absolute; left:17px; top:129px; width:831px; height:380px; border:0pt black solid;">
  
    <table width="822" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="119" class="celdalazul">Rut Usuario </td>
        <td width="294" class="celdalCELEST">
          <input name="rutusuario" type="text" disabled="" class="seleccionTurist_180" onKeyPress="pres_num();" maxlength="10" {RUT}/>
          -
        <input name="dvusuario" type="text" disabled="" class="seleccionT80_a" maxlength="1" onChange="validar()" {DV}/>
        <!--<img src="../Imagenes/120px-Crystal_Clear_action_build19x19.png" width="19" height="19" alt="Buscar Usuario"  onclick="BuscarUsuario()" style="cursor:hand"/>--></td>
        <td width="123" class="celdalazul">Usuario</td>
        <td width="286" class="celdalCELEST"><input name="usuario" type="text" disabled="" {LOGIN} onkeypress="pres_espacio()"/></td>
      </tr>
      <tr>
        <td class="celdalazul">Contrase&ntilde;a </td>
        <td class="celdalCELEST"><input name="password" type="password" onkeypress="pres_espacio()" disabled="" value="{PASSWORD}"/></td>
        <td class="celdalazul">Tipo de Usuario </td>
        <td class="celdalCELEST"><label>
          <input name="tipo_usuario" type="text" class="seleccionTurist_250" id="tipo_usuario" disabled="" {TIPOUSUARIO} readonly="true" />
        </label>
        <img src="../Imagenes/modify.gif" width="16" height="16"  onclick="ModificarTipoUsuario()" alt="Modificar Tipo Usuario" style="cursor:hand"/></td>
      </tr>
      <tr>
        <td class="celdalazul">Nombre Usuario </td>
        <td class="celdalCELEST"><label>
          <input name="nombreusuario" type="text" class="seleccionTurist_250" disabled="" {NOMBREUSAURIO}/>
        </label></td>
        <td class="celdalazul">Apellido Usuario </td>
        <td class="celdalCELEST"><input name="apellido" type="text" class="seleccionTurist_250" disabled="" {APELLIDO}/></td>
      </tr>
      <tr>
        <td class="celdalazul">Direcci&oacute;n</td>
        <td class="celdalCELEST"><label>
          <input name="direccion" type="text" class="seleccionTurist_250" id="direccion" disabled="" {DIRECCION} />
        </label></td>
        <td class="celdalazul">Telefono</td>
        <td class="celdalCELEST"><input name="telefono" type="text" class="seleccionTurist_180" onkeypress="pres_num();" disabled="" {TELEFONO} /></td>
      </tr>
      <tr>
        <td class="celdalazul">Email</td>
        <td class="celdalCELEST"><input name="emailusuario" type="text" class="seleccionTurist_250" disabled="" {EMAIL}/></td>
        <td class="celdalazul">Celular </td>
        <td class="celdalCELEST"><input name="celular" type="text" class="seleccionTurist_180" onkeypress="pres_num();" disabled="" {CELULAR} /></td>
      </tr>
      <tr>
        <td class="celdalazul">Comuna</td>
        <td class="celdalCELEST"><label>
          <input name="comuna" type="text" class="seleccionTurist_180" id="comuna" disabled="" {COMUNA} />
        </label></td>
        <td class="celdalazul">Ciudad</td>
        <td class="celdalCELEST"><label>
          <input name="ciudad" type="text" class="seleccionTurist_180" id="ciudad" disabled="" {CIUDAD} />
        </label></td>
      </tr>
      <tr>
        <td class="celdalazul">Distribuidor</td>
        <td class="celdalCELEST"><input type="text" class="seleccionTurist_250" disabled="" {DISTRIBUIDOR} name="distribuidor" readonly="true" />
        <img src="../Imagenes/modify.gif" width="16" height="16" alt="Modificar Distribuidor" onclick="ModificarDistribuidor()" style="cursor:hand"/></td>
        <td class="celdalazul">Usuario Sistema </td>
        <td class="celdalCELEST"><input name="usuariosistema" type="text" class="seleccionTurist_80" id="usuariosistema" disabled="" {USUARIOSISTEMA} readonly="true"/>
        <img src="../Imagenes/modify.gif" width="16" height="16" style="cursor:hand" alt="Modificar Usuario Sistema" onclick="ModificarUsuarioSistema()"/></td>
      </tr>
      <tr>
        <td class="celdalazul">Cuenta Corriente</td>
        <td class="celdalCELEST"><label>
          <input name="cuentacorrinte" type="text" class="seleccionTurist_180" disabled="" {CUENTACORRIENTE}/>
        </label></td>

        <td class="celdalazul">Fecha Nac.</td>
        <td class="celdalCELEST">
           <input name="fecha_nac" type="text" readonly="" id="datepicker" class="seleccionTurist_180" disabled="" {FECHA_NACIMIENTO} /> 
         <!-- <img src="../Imagenes/configurar19x19.png" width="19" height="19" alt="Ingresar Nueva Cuenta Contable" style="cursor:hand"  onclick="IngresarCuentaContable()"/>    -->
         </td>
      </tr>
      
      <tr>
        <td class="celdalazul">Banco</td>
        <td class="celdalCELEST"><label>
          <input name="banco" type="text" class="seleccionTurist_180" id="banco" disabled="" {BANCO} readonly="true" />
          <img src="../Imagenes/modify.gif" width="16" height="16"  alt="Modificar Banco" onclick="ModificarBanco()" style="cursor:hand"/>
          <input name="codigo_operador" type="hidden" id="codigo_operador2" disabled="" {CODIGO_OPERADOR}/>
        </label></td>
        <td class="celdalazul">Operador</td>
        <td class="celdalCELEST"><input name="operador" type="text" class="seleccionTurist_180" id="operador2" disabled="" {OPERADOR}/> 
          <img src="../Imagenes/modify.gif" width="16" height="16"  alt="Modificar Operador" onclick="ModificarOperador()" style="cursor:hand"/> </td>
      </tr>
      <tr>
        <td class="celdalazul">C&oacute;digo Sucursal </td>
        <td class="celdalCELEST"><input name="sucursal" type="text" class="seleccionTurist_80" id="sucursal" disabled=""  value="{sucursal}" /></td>
        <td class="celdalazul">Codigo Vendedor</td>
        <td class="celdalCELEST"><input name="vendedor" type="text" class="seleccionTurist_80" id="vendedor" disabled="" value="{vendedor}" /></td>
      </tr>
      <tr>
        <td class="celdalazul">Estado</td>
        <td  class="celdalCELEST"><input name="estado" type="text" class="seleccionTurist_80" id="estado" disabled=""  value="{estado}" readonly="true" {ESTADO} />
        &nbsp;<img src="../Imagenes/modify.gif" width="16" height="16" alt="Modificar Estado" style="cursor:hand"  onclick="ModificarEstado()"/></td>
        
        <td class="celdalazul">Recogida</td>
        <td class="celdalCELEST">
        <select name="lugar_re" disabled="" class="seleccionTurist_250"  >
        <option value="0" class="seleccion2" >--(SELECCIONAR)--</option>   
        <!-- START BLOCK : lugar_re -->
        <option value="{CODIGO}" class="seleccion2" {SELECCIONAR_LUGAR}>{DESCRIPCION_LUGAR}</option>
        <!-- END BLOCK : lugar_re -->
        </select>
        </td>
      </tr>
      <tr>
        <td class="celdalazul">Usuario funicular</td>
        <td class="celdalCELEST"><input name="funi" type="checkbox" id="funi" {FUNICULAR}  /></td>
        <td class="celdalazul">Password funicular</td>
        <td class="celdalCELEST"><input type="password" id="pass_funi"  {PASS_FUNICULAR} name="pass_funi"  class="seleccionTurist_80" maxlength="8"/></td>
      </tr>
    </table>
    <p>&nbsp;</p>
  </div>

<div class="TituloPlomo" id="Layer3">
  <table width="267" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="83" rowspan="4"><img src="../Imagenes/Crystal_Clear_app_Login_Manager80x80_marco.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="171"><p>&nbsp;</p>      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong>Modificar Usuario </strong><strong></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha">{fecha}</span></td>
    </tr>
    
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
  </table>
</div>
<div id="boton" style="position:absolute; left:348px; top:480px; width:306px; height:25px; z-index:3; border:0pt black solid;">
<input name="Submit" type="button" class="BotonTurist_Celest" value="Modificar" onclick="ModificarUsuario()" style="cursor:hand"/>
<input type="hidden" name="guardar" value="0" />

</div>
</form>

</body>
</html>
