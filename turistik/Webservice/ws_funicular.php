<? 
require ("../Clases/ClaseConexion.inc.php");   



    require_once('lib/nusoap.php'); 
  
        // create the server instance 
        $webServer = new soap_server(); 
        $webServer->soap_defencoding = 'UTF-8';
        $namespace = 'http://www.tkpass.cl/turistik/Webservice/ws_funicular.php?wsdl'; 
        $webServer->configureWSDL('ws'); 
        $webServer->wsdl->schemaTargetNamespace = $namespace; 
        
        
        
        	//Model Webservice :: Funcion TestPrint
        $methodName = 'GuardarVentas'; 
        
        
        $input = array('id' => 'xsd:string',
        'TipoPax' => 'xsd:string',
        'Fecha' => 'xsd:string',
        'IdUsuario' => 'xsd:string',
        'Monto' => 'xsd:string',
        'Tramo' => 'xsd:string',
        'Fop' => 'xsd:string',
        'Caja' => 'xsd:string'
         ); 
        
        
        $output = array('return' => 'xsd:string'); 
        
        $soapAction = false; 
        $style = 'rpc'; 
        $use = 'encoded'; 
        $description = 'Guardar ventas funicular'; 
        $webServer->register($methodName, $input, $output, $namespace, $soapAction, $style, $use, $description);
        
        
        function GuardarVentas($id, $TipoPax, $Fecha, $IdUsuario, $Monto, $Tramo, $Fop, $Caja)
        {
              //$Fecha = '2013-05-02';
              $pieces = explode("/", $Fecha);
              $Fecha = $pieces[2]."-".$pieces[1]."-".$pieces[0]; 

              
              
              $ObjIns= new ClaseConexion;
              $ObjIns->conectar();
              $sql = "INSERT INTO VENTAS_FUNICULAR (TIPO_PAX, FECHA, COD_USUA, MONTO, TRAMO, FOP, CAJA) VALUES ('".$TipoPax."', '".$Fecha."', ".$IdUsuario.", ".$Monto.", '".$Tramo."', '".$Fop."', ".$Caja.")";
              $queryIns = $ObjIns->EjecutaConsulta($sql);
              mysql_free_result($queryIns); 
              mysql_close();
          
               return "OK";
        }
        
        
        // WEBsERVICE Actualizacion de precios
        $webServer->wsdl->addComplexType(
        'Precios',
        'complexType',
        'struct',
        'all',
        '',
        array(
        		'Tipo_Pax' => array('name' => 'Tipo_Pax','type' => 'xsd:string'),
        		'Tramo' => array('name' => 'Tramo','type' => 'xsd:string'),
                'Precio' => array('name' => 'Precio','type' => 'xsd:string')
        )
        );

        $webServer->wsdl->addComplexType(
        'ListaPrecio',
        'complexType',
        'array',
        '',
        'SOAP-ENC:Array',
        array(),
        array
        (
        array(
             'ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:Precios[]'
        )
        ),
        'tns:Precios'
        );
        
                	//Model Webservice :: Funcion TestPrint
        $methodName = 'ActualizarPrecios'; 
        
        
        $input = array(); 
        
        
        $output = array('return' => 'tns:ListaPrecio'); 
        
        $soapAction = false; 
        $style = 'rpc'; 
        $use = 'encoded'; 
        $description = 'Actualizar precios funicular'; 
        $webServer->register($methodName, $input, $output, $namespace, $soapAction, $style, $use, $description);
        
        
        function ActualizarPrecios()
        {

        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta(" SELECT * FROM FUNICULAR");
            	while ($con = mysql_fetch_array($query))
				 {
					$tipo_pax = $con['TIPO_PAX'];
                    $tramo  = $con['TRAMO'];
                    $precio = $con['PRECIO'];
						
							
				$elements[] = array(					
							"Tipo_Pax" => $tipo_pax,
							"Tramo" => $tramo ,
                            "Precio" => $precio
                            
                            );		
			
				 }  
        mysql_free_result($query); 
        mysql_close();          
         
				return $elements;
        }
        
        
        
          // WEBsERVICE Actualizacion de Usuarios
        $webServer->wsdl->addComplexType(
        'Usuarios',
        'complexType',
        'struct',
        'all',
        '',
        array(
        		'Cod_usua' => array('name' => 'Cod_Usua','type' => 'xsd:string'),
        		'Nombre' => array('name' => 'Nombre','type' => 'xsd:string'),
                'Apellido' => array('name' => 'Apellido','type' => 'xsd:string'),
       		    'Login' => array('name' => 'Login','type' => 'xsd:string'),
        		'Rut' => array('name' => 'Rut','type' => 'xsd:string'),
                'Dv' => array('name' => 'Dv','type' => 'xsd:string'),
                'Password_Usua' => array('name' => 'Password_Usua','type' => 'xsd:string')
        )
        );

        $webServer->wsdl->addComplexType(
        'ListaUsuarios',
        'complexType',
        'array',
        '',
        'SOAP-ENC:Array',
        array(),
        array
        (
        array(
             'ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:Usuarios[]'
        )
        ),
        'tns:Usuarios'
        );
        
                	//Model Webservice :: Funcion TestPrint
        $methodName = 'ActualizarUsuarios'; 
        
        
        $input = array(); 
        
        
        $output = array('return' => 'tns:ListaUsuarios'); 
        
        $soapAction = false; 
        $style = 'rpc'; 
        $use = 'encoded'; 
        $description = 'Actualizar Usuarios funicular'; 
        $webServer->register($methodName, $input, $output, $namespace, $soapAction, $style, $use, $description);
        
        
        function ActualizarUsuarios()
        {

        $miConexion= new ClaseConexion;
        $miConexion->Conectar();
        $query=$miConexion->EjecutaConsulta(" SELECT COD_USUA,NOMBRE_USUA,APELLIDO_USUA,LOGIN_USUA,RUT_USUA,DIGITO_USUA,PASS_FUNICULAR FROM USUARIO WHERE USUA_FUNICULAR = 1");
            	while ($con = mysql_fetch_array($query))
				 {
					$cod_usua = $con['COD_USUA'];
                    $nombre  = $con['NOMBRE_USUA'];
                    $apeliido  = $con['APELLIDO_USUA'];
                    $login  = $con['LOGIN_USUA'];
                    $rut  = $con['RUT_USUA'];
                    $dv  = $con['DIGITO_USUA'];
                    $pass = $con['PASS_FUNICULAR'];
						
							
				$elements[] = array(					
							"Cod_usua" => $cod_usua,
							"Nombre" => $nombre ,
                            "Apellido" => $apeliido ,
                            "Login" => $login ,
                            "Rut" => $rut ,
                            "Dv" => $dv ,
                            "Password_Usua" => $pass
                            
                            );		
			
				 }  
        mysql_free_result($query); 
        mysql_close();          
         
				return $elements;
        }      
        
        
        
        $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
        $webServer->service($HTTP_RAW_POST_DATA); 
		