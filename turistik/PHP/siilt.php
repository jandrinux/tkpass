<?
 ini_set("display_errors", 1);
    error_reporting(E_ALL);
  if ($_GET['caf']==1) {

      $par = $_FILES["file"]["tmp_name"];
     

     $x = file_get_contents($par);
     $z = str_split($x);

     $xml=simplexml_load_file($par) or die("Error: Cannot create object");

     $r = $xml->CAF[0];
     $info = $r;

    foreach ($info as $key => $value) {
       echo $key. " ".$value ;
     }
 
    exit();
  }

	 if ($_GET['json']!=1) {
		ini_set('display_errors', 0);
		include("menus.php");
		$_Util=new Util;
	 }
	 else {
		$client = new SoapClient("http://172.19.1.15:8080/sii.asmx?WSDL", array('encoding'=>'ISO-8859-1'));
		$envio  = array('Token' => "123123123");
		$data = $client->ListarCAF($envio);

		$datax = json_encode($data->ListarCAFResult->CAF);
		header('Content-Type: application/json');
		echo ($datax);
		exit();
	}




?>

<html ng-app="sii">
<head >
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="../estilos1.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
    <script language="javascript" src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
    <link href="../../css/menu_small.css" rel="stylesheet" type="text/css">

    <style type="text/css" >
    #Layer3 {
     margin-top: 50px;
     margin-left: 20px;
     width:501px;
     height:62px;
 }
 #TituloPlomo
 {
   COLOR: #010356;
   FONT-FAMILY: Arial, Helvetica, Sans-Serif;
   FONT-SIZE: 12pt;
   FONT-STYLE: normal;
   FONT-VARIANT: normal;
   FONT-WEIGHT: normal;
   TEXT-DECORATION: none;
   TEXT-TRANSFORM: none
}
</style>
</head>
<body>



    <div class="TituloPlomo" id="Layer3">
      <table width="482" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="80" rowspan="4"><img src="../Imagenes/IMG_6716.jpg" width="80" height="80" /></td>
          <td width="13">&nbsp;</td>
          <td width="389">&nbsp;</td>
      </tr>
      <tr>
          <td>&nbsp;</td>
          <td><strong>SII</strong></td>
      </tr>
      <tr>
          <td>&nbsp;</td>
          <td><span class="TituloFecha"><?php echo $_Util->Fecha(); ?></span></td>
      </tr>

      <tr>
          <td class="TituloFecha">&nbsp;</td>
          <td height="22" class="TituloFecha">&nbsp;</td>
      </tr>
  </table>
  

</div>



<div id="Layer4" style="  margin-left: 20px;width:500px; height:96px; z-index:15">
    <div id="menuh">
        <ul>
           <li><a id="primero" href="siilt.php" >CAF</a></li>
           <li><a href="boletaslt.php" >Boletas</a></li>
           <li><a href="notacreditolt.php" >Notas de Credito</a></li>
          
       </ul>
   </div>
   <br><br>
   <form action="siilt.php?caf=1" method="POST" enctype="multipart/form-data">
		<table>
			<tr>
				<td class="celdalazul_peque">SUBIR CAF</td>
				<td class="celdalCELEST">
					<input type="hidden" name="f" id="f" value="123123123">
					<input type="file" name="file" id="file">
				</td>  
				<td class="celdalCELEST">
          <input type="submit" value="Subir" class="BotonTurist_Celest">

				</td>
			</tr>
		</table>
	</form>
   <br>
   <table width="700px" ng-controller="listaController" >
   		<thead>
   			<tr>
   				<td class='celdalazul_peque'>Empresa</td>
   				<td class='celdalazul_peque'>Tipo</td>
   				<td class='celdalazul_peque'>Folio Desde</td>
   				<td class='celdalazul_peque'>Folio Hasta</td>
   				<td class='celdalazul_peque'>Folios Consumidos</td>
   				<td class='celdalazul_peque'>Folios Restantes</td>
   				<td class='celdalazul_peque'>Fecha CAF</td>
   				<td class='celdalazul_peque'>Fecha Upload</td>
   			</tr>
   		</thead>
   		<tbody>
   			<tr ng-repeat="p in cafs">
   				<td class='celdalCELEST'>{{p.Rut}}</td>
   				<td class='celdalCELEST'> {{tipoDesc(p.Tipo)}}
   				</td>
   				<td class='celdalCELEST'>{{p.RangoDesde}}</td>
   				<td class='celdalCELEST'>{{p.RangoHasta}}</td>
   				<td class='celdalCELEST'>{{p.DocActual -1}}</td>
   				<td class='celdalCELEST'>{{ (p.RangoHasta + 1 - p.RangoDesde) -  p.DocActual  }}</td>
   				<td class='celdalCELEST'>{{p.FechaCAF | date :"MM/dd/yyyy"}}</td>
   				<td class='celdalCELEST'>{{p.FechaGEN | date :"MM/dd/yyyy   h:mma"}}</td>
   			</tr>
   		</tbody>
   </table>



</div>
<br>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
<script>
	var app = angular.module("sii",[]);

	app.controller('listaController', ['$scope','$http', function($scope,$http){
		$scope.cafs = []; 
		$http.get('siilt.php?json=1').success(function(data){
			$scope.cafs = data;
		});

		$scope.tipoDesc = function (dato) {
			if (dato == 39)
				return "BOLETA ELECTRONICA";
			else
				return "NOTA DE CREDITO";
		}
	}]);
</script>
</body>
</html>

