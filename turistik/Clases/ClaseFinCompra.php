<?
//session_start();

//$FinCompra = new FinCompra($_SESSION['codigo_suc'], $tipoReserva, $codSocio, $fop);
/*
$FinCompra = new FinCompra(7, 2, '', 'pp');

$cod_usuario = $FinCompra->cod_usuario;

$suc_reserva = $FinCompra->suc_reserva;
$suc_venta = $FinCompra->suc_venta;
$fop = $FinCompra->fop;
echo $cod_cierre = $FinCompra->cod_cierre;
$login_usua_cierre = $FinCompra->login_cierre;
*/


Class FinCompra
{
    var $sucursal;
    var $tipoReserva; //pp o cc
    var $cod_usuario;
    var $fop;
    var $cod_socio;
    var $cod_cierre;
    var $login_cierre;
    
    
    var $suc_reserva;
    var $suc_venta;
    var $codigo_distribuidor;
    var $codigo_vendedor;
    
    
    function __construct($sucursal,$tipoReserva,$cod_socio,$fop)
    {
        $this->sucursal = $sucursal;
        $this->tipoReserva = $tipoReserva;
        $this->cod_socio = $cod_socio;

        
        if ($this->sucursal == 15 || $this->sucursal == 7 || $this->sucursal == 18) // si es de sucursal de reservas
        {
            if ($this->tipoReserva == 1)
            {
                $this->cod_usuario = $_SESSION['codigo_usu'];
                $this->FormaPagoCC($fop);
                $this->cod_cierre = 2; // pagado
                $this->login_cierre = $_SESSION['usuario'];
                $this->codigo_distribuidor = $_SESSION["codigo_dist"];
            }
            
            if ($this->tipoReserva == 2) //tiporeserva pp
            {

                  
                    $this->cod_usuario  = $_SESSION['codigo_usu'];
                    
                    
                    if ($this->cod_socio == '')
                    {
                        if($fop == 'W' || $fop == 'TC')
                        {
                            $this->codigo_distribuidor = 1426;  //call center
                            $this->codigo_vendedor = 1426;
                            $this->suc_reserva = $_SESSION['codigo_suc'];  
                            $this->cod_cierre = 2; // pagada
                        }
                        else
                        {
                            $this->codigo_distribuidor = $_SESSION['codigo_dist'];
                            $this->suc_reserva = $_SESSION['codigo_suc']; 
                            $this->cod_cierre = 1; // pendiente                          
                        }
                        

                        
                    }
                    else
                    {
                        
                        $this->Distribuidor();  
                      
                        
                        if($fop == 'W' || $fop == 'TC')
                        {
                            $this->codigo_distribuidor = $this->codigo_distribuidor;
                            $this->codigo_vendedor = $this->cod_socio;
                            $this->cod_cierre = 2; // pagada
                            $this->suc_reserva = $_SESSION['codigo_suc']; 
                            
                        }
                        else
                        {
                              $this->suc_reserva = 9; // Hotelero
                              $this->cod_cierre = 1; // pendiente
                        }

                        // realizar consulta, rescatar codigo_dist teniendo el codigo_vendedor
                        //$this->tipoReserva = 2;

                       
                    }
                        
                
                $this->FormaPagoPP($fop);
                $this->login_cierre = '';

            }
            
            if ($this->tipoReserva == 3) //RA
            {

                    $this->Distribuidor();  
                    $this->codigo_vendedor = $this->cod_socio;
                    $this->cod_usuario = $_SESSION['codigo_usu']; //login_usua
                    $this->suc_reserva = $this->sucursal; // Sucursal de Usuario
                    $this->suc_venta = $this->sucursal;  // Sucursal de Usuario 
                    $this->cod_cierre = 1; // pagado      
                    $this->fop = $fop;     
                    $this->login_cierre = $_SESSION['usuario'];


            }
            
            
            if ($this->tipoReserva == 4) //MA
            {

                    $this->cod_usuario = $_SESSION['codigo_usu']; //login_usua
                    $this->suc_reserva = $this->sucursal; // Sucursal de Usuario
                    $this->suc_venta = $this->sucursal;  // Sucursal de Usuario 
                    $this->cod_cierre = 2; // pagado      
                    $this->fop = $fop;     
                    $this->login_cierre = $_SESSION['usuario'];
                    $this->Distribuidor(); 

            }


        }

        if ($this->sucursal == 9) // si sucursal es hotelero
        {
            if ($this->tipoReserva == 1) //tiporeserva cc
            {
                $this->suc_reserva = 9; // Hotelero
                $this->suc_venta = 9; // Hotelero  
                $this->cod_cierre = 2; // pagado
                $this->fop = 'C';
                $this->cod_usuario = $_SESSION['codigo_usu'];
                $this->login_cierre = $_SESSION['usuario'];
            }
            
            if ($this->tipoReserva == 2) //tiporeserva pp
            {
                $this->suc_reserva = 9; // Hotelero
                $this->suc_venta = '';   
                $this->cod_cierre = 1; // pendiente
                $this->fop = 'PP';
                $this->cod_usuario = $_SESSION['codigo_usu'];
                $this->login_cierre = '';
            }
            
            if ($this->tipoReserva == 3) //tiporeserva ra
            {
                $this->suc_reserva = 9; // Hotelero
                $this->suc_venta = '';   
                $this->cod_cierre = 1; // pendiente
                $this->fop = "RA";
                $this->cod_usuario = $_SESSION['codigo_usu'];
                $this->login_cierre = '';
            }
            
            $this->codigo_distribuidor = $_SESSION["codigo_dist"];

        }
        if ($this->sucursal == 8) // si sucursal es sheraton
        {
            if ($this->cod_socio == '')
            {
                $this->tipoReserva = 1;
                $this->codigo_distribuidor = $_SESSION["codigo_dist"];
            }
            else
            {
                $this->tipoReserva = 2;
                $this->Distribuidor();
                
            }
            
            
                $this->suc_reserva = 8; //Sheraton
                $this->suc_venta = 8;   
                $this->cod_cierre = 2; // pendiente
                $this->fop = $fop;
                $this->cod_usuario = $_SESSION['codigo_usu'];
                $this->login_cierre = $_SESSION['codigo_usu'];
            
        }
        
        
        
        if ($this->sucursal != 8 && $this->sucursal != 7 && $this->sucursal != 15 && $this->sucursal != 9 && $this->sucursal != 18)
        {

            $this->cod_usuario = $_SESSION['codigo_usu']; //login_usua
            $this->suc_reserva = $this->sucursal; // Sucursal de Usuario
            $this->suc_venta = $this->sucursal;  // Sucursal de Usuario 
            $this->cod_cierre = 2; // pagado      
            $this->fop = $fop;     
            $this->login_cierre = $_SESSION['usuario'];
            $this->codigo_distribuidor = $_SESSION["codigo_dist"];
        }

        
    }
    
    
    function Distribuidor()
    {
        $miConexion= new ClaseConexion;
        $miConexion->conectar();
        $query = $miConexion->EjecutaConsulta("SELECT CODIGO_DIST_USUA FROM USUARIO WHERE CODIGO_VENDEDOR = $this->cod_socio ");
        while($row=mysql_fetch_array($query))
        {
            $this->codigo_distribuidor = $row['CODIGO_DIST_USUA'];
        }
        mysql_free_result($query); 
                        
    }
    
    
    function FormaPagoPP($fop)
    {
        if ($fop == 'W' || $fop == 'TC') // forma de pago WebPay
        {
            
            //$this->suc_venta = 18;  // WebPay //mod CEE 14-05-2015
            $this->suc_venta = $_SESSION['codigo_suc'];
            $this->fop = $fop;
        }
        else
        {
            $this->suc_venta = '';     
            $this->fop = 'pp';
        }
    }
    
    
    function FormaPagoCC($fop)
    {
        if ($fop == 'W') // forma de pago WebPay
        {
            $this->suc_reserva = 7; // Reserva
            //$this->suc_venta = 18;  // WebPay //mod CEE 14-05-2015
            $this->suc_venta = 7;
        }
        else
        {
            $this->suc_reserva = $this->sucursal; 
            $this->suc_venta = $this->sucursal;            
        }
        $this->fop = $fop;
    }

}

?>