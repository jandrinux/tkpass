<?
//ini_set('display_errors', 1);
session_start();
date_default_timezone_set("America/Santiago");
if ($_SESSION["autentificado"] != "SI"){
    header("location: Login.php");
}

require ("../Clases/ClaseConexion.inc.php");
require ("../Clases/ClaseUtil.inc.php");
require ("../functions/tkpass.php");


$usuario = base64_decode($_GET['u']);
$tipo_excursion = base64_decode($_GET['ex']);
$fecha = base64_decode($_GET['f']);
$fecha_format = base64_decode($_GET['df']);


$_Util=new Util;
$miConexion = new ClaseConexion;
$Obj_Tkpass = new tkpass(); 
include_once('validaciones.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US">

<html>
<head>
<title>Reserva Excursiones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>

<link rel="stylesheet" media="screen" href="../../css/demo.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../../js/jquery.leanModal.min.js"></script>
<link href="../estilos1.css" rel="stylesheet" type="text/css"/>
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>


 
<script type="text/javascript">
$(function() {
	$("#fecha_llegada").datepicker();
});


function info_boleta_tr()
{
     var fop = $("#fop").val();

     if (fop == "C")
     {

        document.getElementById("tituloTipoTarjeta").style.visibility='visible';
        $("#tipo_tarjeta").show();
        document.getElementById("titulo4Ultimos").style.visibility='visible';
        $("#ultimos_digitos").show();
        document.getElementById("tituloTBK").style.visibility='visible';
        $("#info_boleta").show();
        
     }
     else
     {
         if (fop == "D")
         {
            document.getElementById("tituloTBK").style.visibility='visible';
            $("#info_boleta").show();
            document.getElementById("tituloTipoTarjeta").style.visibility='hidden';
            $("#tipo_tarjeta").hide();
            document.getElementById("titulo4Ultimos").style.visibility='hidden';
            $("#ultimos_digitos").hide();
         }
         else
         {
            document.getElementById("tituloTipoTarjeta").style.visibility='hidden';
            $("#tipo_tarjeta").hide();
            document.getElementById("titulo4Ultimos").style.visibility='hidden';
            $("#ultimos_digitos").hide();
            document.getElementById("tituloTBK").style.visibility='hidden';
            $("#info_boleta").hide();
         }
         

     }
     
}


function Reservar()
{
    var pasajero = $("#pasajero").val();
    var cantidad = $("#cantidad").val();
    var hora_llegada = $("#hora_llegada").val();
    var fecha_llegada = $("#fecha_llegada").val();
    var hotel = $("#hotel").val();
    var vuelo = $("#vuelo").val();
    var observacion = $("#observacion").val(); 
    var tipoReserva = $("#tipoReserva").val();
    var fop = $("#fop").val();
    var codigo_suc = $("#codigo_suc").val();
    var tipo_excursion = $("#tipo_excursion").val();
    var fecha_format = $("#fecha_format").val();
    var tipo = 2;
    var tbk = $("#info_boleta").val();
    var ultimos_digitos = $("#ultimos_digitos").val();
    var tipo_tarjeta = $("#tipo_tarjeta").val();
    
    
    if (pasajero == '' || hora_llegada == 0 || hotel=="" || vuelo == "")
    {
        alert('Complete campos mandatorios (*)');
        return false;
    }
    if (fop == 'S')
    {
        alert('Seleccione Forma de Pago');
        return false;
    }
    

    if (fop == 'C')
    {
        if(tbk == '')
        {
            alert("Ingrese Voucher Transbank.");
            document.getElementById('info_boleta').focus();
            return false;
        }

        if (ultimos_digitos.length < 4)
        {
            alert("Complete los 4 ultimos digitos de la Tarjeta Bancaria");
            document.getElementById('ultimos_digitos').focus();
            return false; 
        }
            
        
        if (tipo_tarjeta == '')
        {
            alert("Seleccione Tipo de Tarjeta");
            document.getElementById('tipo_tarjeta').focus();
            return false;
        }
    }
    if (fop == 'D')
    {
        if(tbk == '')
        {
            alert("Ingrese Voucher Transbank.");
            document.getElementById('info_boleta').focus();
            return false;
        }

    }

       document.formCompra.submit();

}
</script>

<? //include('js.php');?>

</head>
   
<body onload="info_boleta_tr();">

<div id="Layer1" style="position:absolute; left:4px; top:120px; width:997px; height:311px; z-index:1">


     <table width="990" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="95" class="celdalazul_peque">Nombre Excursion </td>
            <td width="208" class="celdalCELEST"><? echo $nombre ?></td>
            <td width="105" class="celdalazul_peque">Fecha de Excursi&oacute;n </td>
            <td class="celdalCELEST"><? echo base64_decode($_GET['f']) ?></td>
    	    <td width="100" class="celdalazul_peque">Valor por persona</td>
    	    <td class="celdalCELEST" >
    	   <input name="monto_paso" type="hidden" id="monto_paso" value="<? echo $monto ?>"/>
           <input name="monto" type="hidden" id="monto2" value="" />
            <label for="lbl_precio" id="lbl_precio"><? echo "$ ". number_format($monto)." ADT"; ?></label><br />
            <label for="lbl_precio" id="lbl_precio"><? echo "$ ". number_format($monto_child)." CHD"; ?></label>
            </td>

                <td width="96" class="celdalazul_peque"></td>
                <td width="96" class="celdalCELEST"></td>

          </tr>
          <tr>
    			<td colspan="8" style="font-family: Arial; font-size: 12px; "><a href="#resumen" name="reconfirme" rel="resumen" id="go">Resumen Excursi&oacute;n</a></td>
          </tr>
    
    </table>

    

    
    <form name="formCompra" id="formCompra" method="post" action="finalizar_reserva_apt.php">
        <fieldset id="fieldset">
        <legend class="celdalLeyend">Agregar Pasajero</legend>
         <table id="tabla" width="970" border="0" cellpadding="0" cellspacing="0">
         <tr>
            <td class="celdalazul_peque">Nombre Pasajero: *</td>
            <td class="celdalCELEST"><input name="pasajero" type="text" required="" class="seleccionTurist_180" id="pasajero" title="Nombre pax es obligatorio" onkeypress="return validarP1(event);" maxlength="25"  x-moz-errormessage="Nombre pax es obligatorio"/></td>
            <td class="celdalazul_peque">Cantidad de Pasajeros: *</td>
            <td class="celdalCELEST">
                <select name="cantidad" class="seleccionTurist_180" id="cantidad" required="" />
                    
                    <?
                     for($i=1; $i<=30; $i++)
                     {
                         echo "<option value=".$i." >".$i."</option>" ;  
                     }
                        

                    ?>
                </select>
            </td>
        
         </tr>
         <tr>
        
            <td class="celdalazul_peque">Fecha de llegada: *</td>
            <td class="celdalCELEST">
        
               <input id="fecha_llegada" name="fecha_llegada" type="text" class="seleccionTurist_180" readonly="" value="<? echo $fecha?>" /> 
        
            </td>
        
        
            <td class="celdalazul_peque">Hora de llegada: *</td>
            <td class="celdalCELEST">
        		<select name="hora_llegada" id="hora_llegada" class="seleccionTurist_180">
        		<option value="0" class="seleccion2" >--(SELECCIONAR)--</option>
        		 <option value="0600" class="seleccion2" 0600>06:00</option>
        		 <option value="0615" class="seleccion2" 0615>06:15</option>
        		 <option value="0630" class="seleccion2" 0630>06:30</option>
        		 <option value="0645" class="seleccion2" 0645>06:45</option>
        		 <option value="0700" class="seleccion2" 0700>07:00</option>
        		 <option value="0715" class="seleccion2" 0715>07:15</option>
        		 <option value="0730" class="seleccion2" 0730>07:30</option>
        		 <option value="0745" class="seleccion2" 0745>07:45</option>
        		 <option value="0800" class="seleccion2" 0800>08:00</option>
        		 <option value="0815" class="seleccion2" 0815>08:15</option>
        		 <option value="0830" class="seleccion2" 0830>08:30</option>
        		 <option value="0845" class="seleccion2" 0845>08:45</option>
        		 <option value="0900" class="seleccion2" 0900>09:00</option>
        		 <option value="0915" class="seleccion2" 0915>09:15</option>
        		 <option value="0930" class="seleccion2" 0930>09:30</option>
        		 <option value="0945" class="seleccion2" 0945>09:45</option>
        		 <option value="1000" class="seleccion2" 1000>10:00</option>
        		 <option value="1015" class="seleccion2" 1015>10:15</option>
        		 <option value="1030" class="seleccion2" 1030>10:30</option>
        		 <option value="1045" class="seleccion2" 1045>10:45</option>
        		 <option value="1100" class="seleccion2" 1100>11:00</option>
        		 <option value="1115" class="seleccion2" 1115>11:15</option>
        		 <option value="1130" class="seleccion2" 1130>11:30</option>
        		 <option value="1145" class="seleccion2" 1145>11:45</option>
        		 <option value="1200" class="seleccion2" 1200>12:00</option>
        		 <option value="1215" class="seleccion2" 1215>12:15</option>
        		 <option value="1230" class="seleccion2" 1230>12:30</option>
        		 <option value="1245" class="seleccion2" 1245>12:45</option>
        		 <option value="1300" class="seleccion2" 1300>13:00</option>
        		 <option value="1315" class="seleccion2" 1315>13:15</option>
        		 <option value="1330" class="seleccion2" 1330>13:30</option>
        		 <option value="1345" class="seleccion2" 1345>13:45</option>
        		 <option value="1400" class="seleccion2" 1400>14:00</option>
        		 <option value="1415" class="seleccion2" 1415>14:15</option>
        		 <option value="1430" class="seleccion2" 1430>14:30</option>
        		 <option value="1445" class="seleccion2" 1445>14:45</option>
        		 <option value="1500" class="seleccion2" 1500>15:00</option>
        		 <option value="1515" class="seleccion2" 1515>15:15</option>
        		 <option value="1530" class="seleccion2" 1530>15:30</option>
        		 <option value="1545" class="seleccion2" 1545>15:45</option>
        		 <option value="1600" class="seleccion2" 1600>16:00</option>
        		 <option value="1615" class="seleccion2" 1615>16:15</option>
        		 <option value="1630" class="seleccion2" 1630>16:30</option>
        		 <option value="1645" class="seleccion2" 1645>16:45</option>
        		 <option value="1700" class="seleccion2" 1700>17:00</option>
        		 <option value="1715" class="seleccion2" 1715>17:15</option>
        		 <option value="1730" class="seleccion2" 1730>17:30</option>
        		 <option value="1745" class="seleccion2" 1745>17:45</option>
        		 <option value="1800" class="seleccion2" 1800>18:00</option>
        		 <option value="1815" class="seleccion2" 1815>18:15</option>
        		 <option value="1830" class="seleccion2" 1830>18:30</option>
        		 <option value="1845" class="seleccion2" 1845>18:45</option>
        		 <option value="1900" class="seleccion2" 1900>19:00</option>
        		 <option value="1915" class="seleccion2" 1915>19:15</option>
        		 <option value="1930" class="seleccion2" 1930>19:30</option>
        		 <option value="1945" class="seleccion2" 1945>19:45</option>
        		 <option value="2000" class="seleccion2" 2000>20:00</option>
        		 <option value="2015" class="seleccion2" 2015>20:15</option>
        		 <option value="2030" class="seleccion2" 2030>20:30</option>
        		 <option value="2045" class="seleccion2" 2045>20:45</option>
        		 <option value="2100" class="seleccion2" 2100>21:00</option>
        		 <option value="2115" class="seleccion2" 2115>21:15</option>
        		 <option value="2130" class="seleccion2" 2130>21:30</option>
        		 <option value="2145" class="seleccion2" 2145>21:45</option>
        		 <option value="2200" class="seleccion2" 2200>22:00</option>
        		 <option value="2215" class="seleccion2" 2215>22:15</option>
        		 <option value="2230" class="seleccion2" 2230>22:30</option>
        		 <option value="2245" class="seleccion2" 2245>22:45</option>
        		 <option value="2300" class="seleccion2" 2300>23:00</option>
        		 <option value="2315" class="seleccion2" 2315>23:15</option>
        		 <option value="2330" class="seleccion2" 2330>23:30</option>
        		 <option value="2345" class="seleccion2" 2345>23:45</option>
        		 <option value="2400" class="seleccion2" 2400>24:00</option>
        		 </select>
            </td>
        
         </tr>
         <tr>
        
            <td class="celdalazul_peque">Hotel de llegada:</td>
            <td class="celdalCELEST">
                <select name="hotel" title="Selecciona un hotel" id="hotel"  class="seleccionTurist_180" >
                <option selected value="">Seleccione Hotel</option>
                    <?php
                    $miConexion = new ClaseConexion;
                    $miConexion->Conectar();
                    $queryTipoHotel=$miConexion->EjecutaSP("Consulta_Hotel_Lugar"," 1 ");//hotel
                    while($rowTipoHotel = mysql_fetch_array($queryTipoHotel)){
                        if ($hotel == $rowTipoHotel['CODIGO_HL'] || $recogida_hl == $rowTipoHotel['CODIGO_HL']){
                            echo "<option value=".$rowTipoHotel['CODIGO_HL']." selected>".$rowTipoHotel['DESCRIPCION_HL']."</option>" ;
                        }else{
                            echo "<option value=".$rowTipoHotel['CODIGO_HL'].">".$rowTipoHotel['DESCRIPCION_HL']."</option>" ;
                        }
                    } 
                    
                    
                    mysql_free_result($queryTipoHotel); 
                    mysql_close(); 
                    ?>
                </select>
            </td>

            <td class="celdalazul_peque">Vuelo de llegada:* </td>
            <td class="celdalCELEST"><input name="vuelo" class="seleccionTurist_180"  size="18" maxlength="17" id="vuelo" type="text" /></td>
            
         </tr>   
         <tr>   
            <td class="celdalazul_peque">Observaci&oacute;n: </td>
            <td class="celdalCELEST">
                <textarea name="observacion" id="observacion" rows="2" class="seleccionTurist_180" ><? echo $_POST['observacion'] ?></textarea>
            </td>

            <td class="celdalazul_peque"></td>
            <td class="celdalCELEST"></td>
 
         </tr>
         
        </table>      

        </fieldset>

    

    
    
      <fieldset id="fieldset">
        <legend class="celdalLeyend">Forma de Pago </legend>
        
         <table id="tabla" width="970" border="0" cellpadding="0" cellspacing="0">

         <tr>
            <td  class="celdalazul_peque" width="150">Tipo Reserva:(*)</td>
            <td class="celdalCELEST" width="300">
                <select name="tipoReserva" required="" title="Seleccione tipo de reserva" id="tipoReserva" class="seleccionTurist_180" >
                <?
                    $miConexion = new ClaseConexion;
                    $miConexion->Conectar();
                    $queryTipoConsulta=$miConexion->EjecutaSP("Consulta_Tipo_Reserva_Sucursal"," '".$_SESSION["codigo_suc"]."', 4 ");
                    while ($rowTipoConsulta = mysql_fetch_assoc($queryTipoConsulta)){
                         echo "<option value=".$rowTipoConsulta['CODIGO_TR']." >".htmlentities($rowTipoConsulta['TRESERVA'])."</option>" ;
                    }
                    mysql_free_result($queryTipoConsulta); 
                    mysql_close();
                    
    
    
                ?>
            
                 </select>
            </td>
            <td class="celdalazul_peque" width="150">Forma de pago:(*)</td>
            <td class="celdalCELEST" width="300">
                <select name="fop" required="" title="Seleccione forma de pago" id="fop" class="seleccionTurist_180" onchange="info_boleta_tr();">
                    <option value="S" selected="">-- SELECCIONE --</option>
                    <?
                        $miConexion = new ClaseConexion;
                        $miConexion->Conectar();
                        $queryTipoConsulta=$miConexion->EjecutaSP("Consulta_Tipo_Reserva_Fop_Sucursal"," '".$_SESSION["codigo_suc"]."', 1 ");
                        while ($row = mysql_fetch_assoc($queryTipoConsulta)){
                             echo "<option value=".$row['ID_FOP']." >".$row['Fop']."</option>" ;
                        }
                        mysql_free_result($queryTipoConsulta); 
                        mysql_close();
        
                    ?>
        
                </select>
            </td>
         </tr>
         <tr>
             <td class="celdalazul_peque"><div id="tituloTBK"  >C&oacute;digo Autorizaci&oacute;n: (*)</div></td>
            <td class="celdalCELEST" >
                <input name="voucherTBK" id="info_boleta"  onkeypress="return validarP3(event);"  class="seleccionTurist_180" type="text"/>
            </td>

             
            <td class="celdalazul_peque"></td>
            <td class="celdalCELEST"></td>
                 


         </tr>
         <tr>
            <td class="celdalazul_peque"><div id="titulo4Ultimos" >4 Ultimos Digitos Tarjeta: (*)</div></td>
            <td class="celdalCELEST" >
            <input name="ultimos_digitos" id="ultimos_digitos"  onkeypress="return validarP3(event);" class="seleccionTurist_180" type="text" maxlength="4"/>
            </td>
    
            <td class="celdalazul_peque"><div id="tituloTipoTarjeta" >Tipo de Tarjeta: (*)</div></td>	
            <td class="celdalCELEST">
                <select name="tipo_tarjeta" required="" title="Seleccione Tarjeta" id="tipo_tarjeta" class="seleccionTurist_180">
                <option value="" selected>-- SELECCIONE --</option>
                    <?
                    echo "<option value='VISA'>VISA</option>";
                    echo "<option value='MASTERCARD' >MASTERCARD</option>";
                    echo "<option value='DINERS CLUB'>DINERS CLUB</option>";
                    ?>
                </select>
            </td> 
            
            <td></td>
            <td></td>
     </tr>
     <tr>

        <td></td>
        <td></td>
        <td></td>
        <td></td>
     </tr>
     
        </table>
        </fieldset>
        
         <input name="codigo_suc" id="codigo_suc" type="hidden" value="<? echo $codigo_suc ?>"  />
         <input name="tipo_excursion" id="tipo_excursion" type="hidden" value="<? echo $tipo_excursion ?>" />
         <input name="usuario" id="usuario" type="hidden" value="<? echo $usuario ?>" />
         <input name="fecha_format" id="fecha_format" type="hidden" value="<? echo $fecha_format ?>" />
         <input type="hidden" name="estado" id="estado"/>
         <input type="hidden" name="monto" id="monto" value="<?=$monto?>"/>
             
        <table align="right">
            <tr>

                <td><input align="right" name="reservar" id="reservar" type="button" class="BotonTurist_Celest" value="Comprar" onclick="Reservar()"/></td>
            </tr>
        </table>  
        
</form>
         

</div>

<div id="Layer2" style="position:absolute; left:5px; top:0px; width:439px; height:89px; z-index:2">

<table>
    <tr>
      <td width="13"><img src="../Imagenes/IMG_6902.jpg"  width="80" height="80"  /></td>
      <td width="400">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="Titulo">Toma de Reservas de Excursiones</span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="TituloFecha">&nbsp;</td>
      <td height="22" class="TituloFecha">&nbsp;</td>
    </tr>
</table>
  

</div>
</body>
</html>