 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Reserva Excursiones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" type="text/JavaScript">
<!--
window.moveTo(0,0); 
window.resizeTo(screen.availWidth,screen.availHeight); 
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function Aceptar()
{
document.form1.guardar.value='1';
document.form1.submit();
}
function validarP(e) 
{ // 1
    tecla = (document.all) ? e.keyCode : e.which; // 2
    if (tecla==8) 
        return true; // 3
    patron = /\d/; // Solo acepta n?meros
    te = String.fromCharCode(tecla); // 5
    return patron.test(te); // 6
}
function Modificar(codigo)
{
var user;
user=document.form1.usuario.value;
miPopup = window.open("../PHP/Modificar_Factura.php?codigo="+codigo+"&usuario="+user+"","miwin","top=200,left=350,width=400,height=300,scrollbars=no,titlebar=no,location=no");
}

function AsociarMenu()
{
	   var factura = document.getElementById('factura').value;
       var refactura = document.getElementById('refactura').value;
       if (factura == "" || refactura == "")
       {
        alert("Ingrese campos obligatorios (*)");
       }
       else
       {
        
            if(factura == refactura)
            {
                document.form1.guardarmenu.value=1;
    	        document.form1.submit();
            }
            else
            {
               alert("Error. Las facturas deben ser las mismas");
               document.form1.factura.focus();
            }
        
       }
}
function imprime()
{
document.getElementById("imprimirpag").style.visibility = 'hidden';
document.getElementById("cobra").style.visibility = 'hidden';
window.print();
window.close();
}

function Cancelar()
{
window.parent.close();
}


function Exportar_Excel(){
	var user;
	var fecha_f;
	var fecha_ff;
	var tipo;
	var distribuidor;
	user         = document.form1.usuario.value;
	fecha_f      = document.form1.fecha_f.value;
	fecha_ff     = document.form1.fecha_ff.value;
	tipo         = document.form1.tipo.value;
	distribuidor = document.form1.distribuidor.value;
	
	miPopup = window.open("../PHP/GeneraExcel_Detalle_Reserva_Porcobrar.php?usuario="+user+"&fecha_f="+fecha_f+"&fecha_ff="+fecha_ff+"&tipo="+tipo+"&distribuidor="+distribuidor+"","miwin","top=200,left=350,width=400,height=300,scrollbars=no,titlebar=no,location=no");
	
	
}



//-->
</script>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
-->
</style>
</head>

<body>

 <div id="Layer1" style="position:absolute; left:5px; top:124px; width:991px; height:240px;">
  <form name="form1" method="post" action="">
    <table width="1200"  border="0" cellpadding="0" cellspacing="0">
      <tr class="celdalazul_peque">
        <td class="celdalazul_peque" width="50">Fecha
        <input name="usuario" type="hidden" id="usuario" value="{usuario}"></td>
		<input name="fecha_f" type="hidden" id="fecha_f" value="{fecha_f}"></td>
		<input name="fecha_ff" type="hidden" id="fecha_ff" value="{fecha_ff}"></td>
		<input name="tipo" type="hidden" id="tipo" value="{tipo}"></td>
		<input name="distribuidor" type="hidden" id="distribuidor" value="{distribuidor}"></td>
        <td class="celdalazul_peque">Excursion</td>
        <td class="celdalazul_peque">Valor<br>NETO</td>
        <td class="celdalazul_peque">Valor<br>Abonado</td>
        <td class="celdalazul_peque">Pasajero</td>
        <td class="celdalazul_peque" width="60"># Reserva</td>
        <td class="celdalazul_peque">Lugar de Salida </td>
        <td class="celdalazul_peque">Agente Reserva </td>
        <td class="celdalazul_peque">Boleta Afecta</td>
        <td class="celdalazul_peque">Boleta Exenta</td>
        <td class="celdalazul_peque">Agente Embarque </td>
		 <td class="celdalazul_peque">Observaciones</td>
        <td class="celdalazul_peque">Seleccion</td>
      </tr>
	  	<!-- START BLOCK : tabla -->
      <tr class="celdalcelesteRojo">
        <td width="50" {estilo}>{fecha_tabla}</td>
        <td {estilo}>{excursion_desc}</td>
        <td {estilo}>{neto}</td>
        <td {estilo}>{abonado}</td>
        <td {estilo}>{pasajero}</td>
        <td {estilo}>{voucher}&nbsp;</td>
        <td {estilo}>{lugar}&nbsp;</td>
        <td {estilo}>{user}&nbsp;</td>
         <td {estilo}>{BA}</td>
          <td {estilo}>{BE}</td>
        <td {estilo}>{login}&nbsp;</td>
		<td {estilo}>{observaciones}&nbsp;</td>
        <td {estilo}><div align="center">
          {check}{check2}{check3}{COB}        </div></td>
      </tr>
	  <!-- END BLOCK : tabla -->
    </table>
    <p>
    <table>
        <tr class="celdalCELEST">
            <td width="136" class="celdalazul_peque"><label>Factura: (*)</label></td>
            <td width="300" class="celdalCELEST"><input type="text" onkeypress="return validarP(event);"  id="factura" name="factura" /></td>
        </tr>
        <tr class="celdalCELEST">
            <td width="136" class="celdalazul_peque"><label>Repita Factura: (*)</label></td>
            <td width="300" class="celdalCELEST"><input type="text" onkeypress="return validarP(event);"  id="refactura" name="refactura" /></td>
        </tr>
       
    </table>
    
    
      <input name="Submit" type="submit" class="BotonTurist_Celest" value="Imprimir" onClick="imprime();" id="imprimirpag">
	  <input name="Submit22" type="submit" class="BotonTurist_Celest" value="Cancelar" onClick="Cancelar()">
	  <input name="Submit22" type="button" class="BotonTurist_Celest2" value="Exportar a Excel" onClick="Exportar_Excel()">
       <input name="Submit2" type="button" class="BotonTurist_Celest" value="Facturar" onclick="AsociarMenu()" id="cobra">
	  <input type="hidden" name="guardarmenu"  value="0"/>
   <input name="arreglo" type="hidden" id="arreglo">
  <input name="cantidad" type="hidden" id="cantidad"></td>
      <br>
      <input name="cargar" type="hidden" id="cargar" value="{cargar}">
    </p>
    <p>&nbsp;    </p>
  </form>
</div>
<div id="Layer2" style="position:absolute; left:6px; top:30px; width:500px; height:89px; z-index:2">
  <table width="493" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="80" rowspan="4"><img src="../Imagenes/IMG_6770-2.jpg" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="400">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="Estilo1">Listado de Pasajeros Por Facturar </span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="TituloFecha">&nbsp;</td>
<!-- START BLOCK : tabla1 -->
      <td height="22" class="TituloFecha">{razon_social}
<!-- END BLOCK : tabla1 -->	  
    </tr>
  </table>
</div>
<div id="Layer4" style="position:absolute; left:900px; top:4px; width:128px; height:80px; z-index:4"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>
</body>
</html>
