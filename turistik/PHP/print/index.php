﻿<?php

include("../../Clases/ClaseConexion.inc.php");
$miConexion = new ClaseConexion;
$boarding = $_GET['u'];
$Exo=array();
$Exo = explode("/",$boarding );


?>
<html>
<!-- License:  LGPL 2.1 or QZ INDUSTRIES SOURCE CODE LICENSE -->
<head><title>Impresion de Brazaletes</title>
 <link rel="stylesheet" href="css/hoja.css">
<script type="text/javascript" src="js/deployJava.js"></script>
	<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="js/html2canvas.js"></script>
	<script type="text/javascript" src="js/jquery.plugin.html2canvas.js"></script>
       <script type="text/javascript" src="http://momentjs.com/downloads/moment.min.js"></script>
<script type="text/javascript">

var imprimio = false;  
	/**
	* Optionally used to deploy multiple versions of the applet for mixed
	* environments.  Oracle uses document.write(), which puts the applet at the
	* top of the page, bumping all HTML content down.
	*/
	deployQZ();
	
	/**
	* Deploys different versions of the applet depending on Java version.
	* Useful for removing warning dialogs for Java 6.  This function is optional
	* however, if used, should replace the <applet> method.  Needed to address 
	* MANIFEST.MF TrustedLibrary=true discrepency between JRE6 and JRE7.
	*/
	function deployQZ() {
		var attributes = {id: "qz", code:'qz.PrintApplet.class', 
			archive:'qz-print.jar', width:1, height:1};
		var parameters = {jnlp_href: 'qz-print_jnlp.jnlp', 
			cache_option:'plugin', disable_logging:'false', 
			initial_focus:'false'};
		if (deployJava.versionCheck("1.7+") == true) {}
		else if (deployJava.versionCheck("1.6+") == true) {
			attributes['archive'] = 'jre6/qz-print.jar';
			parameters['jnlp_href'] = 'jre6/qz-print_jnlp.jnlp';
		}
		deployJava.runApplet(attributes, parameters, '1.5');
	}
	
	/**
	* Automatically gets called when applet has loaded.
	*/
	function qzReady() {
		// Setup our global qz object
		window["qz"] = document.getElementById('qz');
		var title = document.getElementById("title");
		if (qz) {
			try {
			//	title.innerHTML = title.innerHTML + " " + qz.getVersion();
				document.getElementById("content").style.background = "WHITE";
			} catch(err) { // LiveConnect error, display a detailed meesage
				document.getElementById("content").style.background = "#F5A9A9";
				alert("ERROR:  \nThe applet did not load correctly.  Communication to the " + 
					"applet has failed, likely caused by Java Security Settings.  \n\n" + 
					"CAUSE:  \nJava 7 update 25 and higher block LiveConnect calls " + 
					"once Oracle has marked that version as outdated, which " + 
					"is likely the cause.  \n\nSOLUTION:  \n  1. Update Java to the latest " + 
					"Java version \n          (or)\n  2. Lower the security " + 
					"settings from the Java Control Panel.");
		  }

          findPrinter('Zebra');
	  }
	}
	
	/**
	* Returns whether or not the applet is not ready to print.
	* Displays an alert if not ready.
	*/
	function notReady() {
		// If applet is not loaded, display an error
		if (!isLoaded()) {
			return true;
		}
		// If a printer hasn't been selected, display a message.
		else if (!qz.getPrinter()) {
			alert('Please select a printer first by using the "Detect Printer" button.');
			return true;
		}
		return false;
	}
	
	/**
	* Returns is the applet is not loaded properly
	*/
	function isLoaded() {
		if (!qz) {
			alert('Error:\n\n\tPrint plugin is NOT loaded!');
			return false;
		} else {
			try {
				if (!qz.isActive()) {
					alert('Error:\n\n\tPrint plugin is loaded but NOT active!');
					return false;
				}
			} catch (err) {
				alert('Error:\n\n\tPrint plugin is NOT loaded properly!');
				return false;
			}
		}
		return true;
	}
	
	/**
	* Automatically gets called when "qz.print()" is finished.
	*/
    var $btns;

	function qzDonePrinting() {
		// Alert error, if any
        debugger;
		if (qz.getException()) {
			alert('Error printing:\n\n\t' + qz.getException().getLocalizedMessage());
          
			qz.clearException();
			return; 
		}
		
		// Alert success message
         $btns.hide();
         imprimio = false;
		 alert("Impreso Correctamente"); 
         
        
        
	}
	
	/***************************************************************************
	* Prototype function for finding the "default printer" on the system
	* Usage:
	*    qz.findPrinter();
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
	***************************************************************************/
	function useDefaultPrinter() {
		if (isLoaded()) {
			// Searches for default printer
			qz.findPrinter();
			
			// Automatically gets called when "qz.findPrinter()" is finished.
			window['qzDoneFinding'] = function() {
				// Alert the printer name to user
				var printer = qz.getPrinter();
				alert(printer !== null ? 'Default printer found: "' + printer + '"':
					'Default printer ' + 'not found');
				
				// Remove reference to this function
				window['qzDoneFinding'] = null;
			};
		}
	}
	


	
	/***************************************************************************
	* Prototype function for finding the closest match to a printer name.
	* Usage:
	*    qz.findPrinter('zebra');
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinter()); };
	***************************************************************************/
	function findPrinter(name) {
		// Get printer name from input box
	
		
		if (isLoaded()) {
			// Searches for locally installed printer with specified name
			qz.findPrinter(name);
			
			// Automatically gets called when "qz.findPrinter()" is finished.
			window['qzDoneFinding'] = function() {
				var p = name;
				var printer = qz.getPrinter();
				
				// Alert the printer name to user
                /*
				alert(printer !== null ? 'Printer found: "' + printer + 
					'" after searching for "' + name + '"' : 'Printer "' + 
					name + '" not found.');
				*/
                if(printer ==null)
                {
                    alert("Impresora Zebra No Encontrada");
                }
                else
                {
                    $("#listado").show();
                }
				// Remove reference to this function
				window['qzDoneFinding'] = null;
			};
		}
	}
	
	/***************************************************************************
	* Prototype function for listing all printers attached to the system
	* Usage:
	*    qz.findPrinter('\\{dummy_text\\}');
	*    window['qzDoneFinding'] = function() { alert(qz.getPrinters()); };
	***************************************************************************/
	function findPrinters() {
		if (isLoaded()) {
			// Searches for a locally installed printer with a bogus name
			qz.findPrinter('\\{bogus_printer\\}');
			
			// Automatically gets called when "qz.findPrinter()" is finished.
			window['qzDoneFinding'] = function() {
				// Get the CSV listing of attached printers
				var printers = qz.getPrinters().split(',');
				for (i in printers) {
					alert(printers[i] ? printers[i] : 'Unknown');      
				}
				
				// Remove reference to this function
				window['qzDoneFinding'] = null;
			};
		}
	}

	 

	
	/***************************************************************************
	* Prototype function for printing raw ZPL commands
	* Usage:
	*    qz.append('^XA\n^FO50,50^ADN,36,20^FDHello World!\n^FS\n^XZ\n');
	*    qz.print();
	***************************************************************************/     
	function printZPL() {
		if (notReady()) { return; }
		 
		// Send characters/raw commands to qz using "append"
		// This example is for ZPL.  Please adapt to your printer language
		// Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
		qz.append('^XA\n');
        qz.append('^FO50,50^ADN,36,20^FDPRINTED USING QZ PRINT PLUGIN ' + qz.getVersion() + '\n');
		qz.appendImage(getPath() + 'img/image_sample_bw.png', 'ZPLII');
				
		// Automatically gets called when "qz.appendImage()" is finished.
		window['qzDoneAppending'] = function() {
			// Append the rest of our commands
			qz.append('^FS\n');
			qz.append('^XZ\n');  
			
			// Tell the apple to print.
			qz.print();
			
			// Remove any reference to this function
			window['qzDoneAppending'] = null;
		};
	}
	


	
	/***************************************************************************
	* Prototype function for logging a PostScript printer's capabilites to the
	* java console to expose potentially  new applet features/enhancements. 
	* Warning, this has been known to trigger some PC firewalls
	* when it scans ports for certain printer capabilities.
	* Usage: (identical to appendImage(), but uses html2canvas for png rendering)
	*    qz.setLogPostScriptFeatures(true);
	*    qz.appendHTML("<h1>Hello world!</h1>");
	*    qz.printPS();
	***************************************************************************/ 
	function logFeatures() {
		if (isLoaded()) {
			var logging = qz.getLogPostScriptFeatures();
			qz.setLogPostScriptFeatures(!logging);
			alert('Logging of PostScript printer capabilities to console set to "' + !logging + '"');
		}
	}
	
	/***************************************************************************
	* Prototype function to force Unix to use the terminal/command line for
	* printing rather than the Java-to-CUPS interface.  This will write the 
	* raw bytes to a temporary file, then execute a shell command. 
	* (i.e. lpr -o raw temp_file).  This was created specifically for OSX but 
	* may work on several Linux versions as well.
	*    qz.useAlternatePrinting(true);
	*    qz.append('\n\nHello World!\n\n');
	*    qz.print();
	***************************************************************************/ 
	function useAlternatePrinting() {
		if (isLoaded()) {
			var alternate = qz.isAlternatePrinting();
			qz.useAlternatePrinting(!alternate);
			alert('Alternate CUPS printing set to "' + !alternate + '"');
		}
	}
	
	

	
	/***************************************************************************
	****************************************************************************
	* *                          HELPER FUNCTIONS                             **
	****************************************************************************
	***************************************************************************/
	
	
	/***************************************************************************
	* Gets the current url's path, such as http://site.com/example/dist/
	***************************************************************************/
	function getPath() {
		var path = window.location.href;
		return path.substring(0, path.lastIndexOf("/")) + "/";
	}
	
	/**
	* Fixes some html formatting for printing. Only use on text, not on tags!
	* Very important!
	*   1.  HTML ignores white spaces, this fixes that
	*   2.  The right quotation mark breaks PostScript print formatting
	*   3.  The hyphen/dash autoflows and breaks formatting  
	*/
	function fixHTML(html) {
		return html.replace(/ /g, "&nbsp;").replace(/’/g, "'").replace(/-/g,"&#8209;"); 
	}
	
	/**
	* Equivelant of VisualBasic CHR() function
	*/
	function chr(i) {
		return String.fromCharCode(i);
	}
	
	/***************************************************************************
	* Prototype function for allowing the applet to run multiple instances.
	* IE and Firefox may benefit from this setting if using heavy AJAX to
	* rewrite the page.  Use with care;
	* Usage:
	*    qz.allowMultipleInstances(true);
	***************************************************************************/ 
	function allowMultiple() {
	  if (isLoaded()) {
		var multiple = qz.getAllowMultipleInstances();
		qz.allowMultipleInstances(!multiple);
		alert('Allowing of multiple applet instances set to "' + !multiple + '"');
	  }
	}
     
     
           
       function printFile(nombre,fecha,serial,tipo,boleta,sucursal,descuento,precio,codigo, btn,codigo_eo) {
             if (notReady()) {
                alert("Error al Imprimir Vuelva a Intentar")
                return; 
                }
       
       
       if(imprimio == false)
       {
        imprimio = true;
  
              $.post("imprimir.php",{ serial:serial, codigo: codigo_eo},function(data)
              {
            
                if (data == 'OK')
                {
              
	      	        
                       sleep(1000);
                      

                            fecha2 = moment(fecha).add('days', 1).format('DD/MM');
                            fecha3 =  moment(fecha).format('DDMMYYYY');
                            
                            var a = fecha.split("-");
                            fecha = a[0] + " " + a[2] + "/" + a[1];

                            if(codigo == 6)
                                  qz.append("^XA^FO32,2080^BQN,2,9^FDMM, " +codigo_eo + "-"+ fecha3 +"-006-986532-"+ nombre +  "^FS^FO 250,900^A0R,0,40^FDFECHA SERVICIO: " + fecha + " 9:30 AM | 6:00 PM^FS^FO 200,900^A0R,0,40^FDCLIENTE:" + nombre +"^FS^FO120,900^A0R,0,69^FDTURISTIK DESCUBRE: HOP ON/OFF^FS^FO75,900^A0R,0,40 ^FD["+ tipo +"] [Serial:"+serial+"] [BOL: " +boleta +" " + sucursal +"] ^FS^FO25,900^A0R,0,35^FD["+ descuento +"]^FS^FO25,2000^A0N,0,80^FD[$"+ precio +"]^FS^FO25,830^A0N,0,35^FDWWW.TURISTIK.CL^FS^XZ");
                            if(codigo == 54)
                                  qz.append("^XA^FO32,2080^BQN,2,9^FDMM, "+codigo_eo + "-" + fecha3 +"-054-986532-"+ nombre +  "^FS^FO 250,900^A0R,0,40^FDFECHA SERVICIO: " + fecha + " 3:20 PM | 6:00 PM^FS^FO 200,900^A0R,0,40^FDCLIENTE:" + nombre +"^FS^FO120,900^A0R,0,70^FDTURISTIK EXPRESS^FS^FO75,900^A0R,0,40 ^FD["+ tipo +"] [Serial:"+serial+"] [BOL: " +boleta +" " + sucursal +"] ^FS^FO25,900^A0R,0,35^FD["+ descuento +"]^FS^FO25,2000^A0N,0,80^FD[$"+ precio +"]^FS^FO25,830^A0N,0,35^FDWWW.TURISTIK.CL^FS^XZ");   
                            if(codigo == 55)
                                  qz.append("^XA^FO32,2080^BQN,2,9^FDMM, " +codigo_eo + "-"+ fecha3 +"-055-986532-"+ nombre +  "^FS^FO 250,900^A0R,0,40^FDFECHA SERVICIO: " + fecha + "-" + fecha2 +" 9:30 AM | 6:00 PM^FS^FO 200,900^A0R,0,40^FDCLIENTE:" + nombre +"^FS^FO120,900^A0R,0,70^FDVIVE TURISTIK: HOP ON/OFF^FS^FO75,900^A0R,0,40 ^FD["+ tipo +"] [Serial:"+serial+"][BOL: " +boleta +" " + sucursal +"] ^FS^FO25,900^A0R,0,35^FD["+ descuento +"]^FS^FO25,2000^A0N,0,80^FD[$"+ precio +"]^FS^FO25,830^A0N,0,35^FDWWW.TURISTIK.CL^FS^XZ");
                            if(codigo == 63)
                                  qz.append("^XA^FO32,2080^BQN,2,9^FDMM, "+codigo_eo + "-" + fecha3 +"-055-986532-"+ nombre +  "^FS^FO 250,900^A0R,0,40^FDFECHA SERVICIO: " + fecha + "-" + fecha2 +" 9:30 AM | 6:00 PM^FS^FO 200,900^A0R,0,40^FDCLIENTE:" + nombre +"^FS^FO120,900^A0R,0,70^FDVIVE TURISTIK: HOP ON/OFF^FS^FO75,900^A0R,0,40 ^FD["+ tipo +"] [Serial:"+serial+"][BOL: " +boleta +" " + sucursal +"] ^FS^FO25,900^A0R,0,35^FD["+ descuento +"]^FS^FO25,2000^A0N,0,80^FD[$"+ precio +"]^FS^FO25,830^A0N,0,35^FDWWW.TURISTIK.CL^FS^XZ");
                            if(codigo == 64)
                                  qz.append("^XA^FO32,2080^BQN,2,9^FDMM, " +codigo_eo + "-"+ fecha3 +"-006-986532-"+ nombre +  "^FS^FO 250,900^A0R,0,40^FDFECHA SERVICIO: " + fecha + " 9:30 AM | 6:00 PM^FS^FO 200,900^A0R,0,40^FDCLIENTE:" + nombre +"^FS^FO120,900^A0R,0,69^FDTURISTIK DESCUBRE: HOP ON/OFF^FS^FO75,900^A0R,0,40 ^FD["+ tipo +"] [Serial:"+serial+"] [BOL: " +boleta +" " + sucursal +"] ^FS^FO25,900^A0R,0,35^FD["+ descuento +"]^FS^FO25,2000^A0N,0,80^FD[$"+ precio +"]^FS^FO25,830^A0N,0,35^FDWWW.TURISTIK.CL^FS^XZ");
                            
                          
                    	
	
            			// Tell the apple to print.
            			qz.print();
            			
            			// Remove any reference to this function
            			window['qzDoneAppending'] = null;
                        $btns = $(btn);
                        
                       
                              
                }
                if (data == 'NO')
                {
                    alert("El Brazalete ya fue Impreso");
                    return false;
                }
                
              });
       }


        }
      function sleep(milliseconds) {
          var start = new Date().getTime();
          for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
              break;
            }
          }
        }
</script>

	</head>
	<body id="content" bgcolor="RED">
	<div ><h1 id="title"></h1></div>




	<!-- NEW QZ APPLET TAG USAGE -- RECOMMENDED -->
	<!--
	<applet id="qz" archive="./qz-print.jar" name="QZ Print Plugin" code="qz.PrintApplet.class" width="55" height="55">
	<param name="jnlp_href" value="qz-print_jnlp.jnlp">
	<param name="cache_option" value="plugin">
	<param name="disable_logging" value="false">
	<param name="initial_focus" value="false">
	</applet><br />
	-->
	
	<!-- OLD JZEBRA TAG USAGE -- FOR UPGRADES -->
    <!-- 
	<applet name="jzebra" archive="./qz-print.jar" code="qz.PrintApplet.class" width="55" height="55">
	<param name="jnlp_href" value="qz-print_jnlp.jnlp">
	<param name="cache_option" value="plugin">
	<param name="disable_logging" value="false">
	<param name="initial_focus" value="false">
	<param name="printer" value="zebra">
	</applet><br />
	-->



    <!-- 	<input type="button" onClick="printZPL()" value="Print ZPL" /> -->
 <fieldset id="listado" style="display: none;">
          <legend>LISTA DE PASAJEROS</legend>
          <center><img src="../../Imagenes/turistik107_118.jpg" width="75" height="85" /></center>
          
          <?
          $miConexion->Conectar();
            
            foreach($Exo as $key )    
            { 
              $tipos_descuentos = "";
              if($key!="")
              {  
            
                    $sql = "SELECT RE.FECHA_RE,
                                   RE.CODIGO_EO,
                                   RE.CODIGO_RE,
                                   RE.PASAJERO_RE,
                                   RE.APASAJERO_RE,
                                   RE.TARJETA_TTS,
                                   RE.BOLETA_RE,
                                   SU.SUCURSAL,
                                   RE.TIPO_PASAJERO,
                                   RE.PRECIO_RE,
                                   RE.DSCTO_PAX,
                                   RE.DSCTO_EXCURSIONES,
                                   RE.DSCTO_LUGAR,
                                   RE.DSCTO_LRESERVA,
                                   RE.DSCTO_CLIENTE
                                   FROM RESERVA_EXCURSION RE
                                   INNER JOIN SUCURSAL SU ON SU.CODIGO_SUCURSAL=RE.SUCURSAL_VENTA
                                   WHERE 
                                   RE.VOUCHER_RE =".$key." AND
                                   RE.CODIGO_EO IN (6,54,55,63,64)";
                    
                  	$res = $miConexion->EjecutaConsulta($sql);
                    while($row=mysql_fetch_array($res))
                    {
                          
                        $sql2="SELECT * FROM DETALLE_STOCK WHERE SERIAL = ".$row['TARJETA_TTS']."  AND IMPRESO = 0 ";
                        $res2 = $miConexion->EjecutaConsulta($sql2);
                        $contador = mysql_num_rows($res2);
                        $cont = $contador + $cont;
                        
                        if ($row['TIPO_PASAJERO'] == "ADT")
                        {
                            if ($row['DSCTO_PAX'] > 0)
                            {
                                $pax = "PAX";
                            }
                            if ($row['DSCTO_EXCURSIONES'] > 0)
                            {
                                $dscto_exc = " | EXCURSIONES";
                            }
                            if ($row['DSCTO_LUGAR'] > 0)
                            {
                                $dscto_rec = " | LUGAR REC.";
                            }
                            if ($row['DSCTO_LRESERVA'] > 0)
                            {
                                $dscto_lugar = " | LUGAR RE.";
                            }
                            if ($row['DSCTO_CLIENTE'] > 0)
                            {
                                $dscto_cliente = " | CLIENTE";
                            }
                            
                            $tipos_descuentos = "DSCTO. ".$pax."".$dscto_exc."".$dscto_rec."".$dscto_lugar."".$dscto_cliente;
                       
                        }
                        else
                        {
                            $tipos_descuentos = "";
                        }
                        
                       if($contador!=0)
                        {
                              $cantidad+=1;
                              $precio=$row['PRECIO_RE'];
                              $nombre= $row['PASAJERO_RE']." ".$row['APASAJERO_RE'];
                              $fecha= $row['FECHA_RE'];
                              $serial=$row['TARJETA_TTS'];
                              $tipo=$row['TIPO_PASAJERO'];
                              $sucursal=$row['SUCURSAL'];
                              $boleta=$row['BOLETA_RE'];
                              $codigo = $row['CODIGO_EO'];
                              $codigo_re = $row['CODIGO_RE'];
                              ?>
                              <button id="S<?=$row['TARJETA_TTS']?>" onclick="printFile('<?=$nombre?>','<?=$fecha?>','<?=$serial?>','<?=$tipo?>','<?=$boleta?>','<?=$sucursal?>','<?=$tipos_descuentos?>','<?=$precio?>','<?=$codigo?>',this,'<?=$codigo_re?>')" ><?=$nombre?></button>
                              <script>
                                        $( "#S<?=$row['TARJETA_TTS']?>" ).focus(function() {
                                          $("#ida").focus();
                                          return;
                                        });
                              </script>
                              <?
                              
                        }
                    	
                    }
              }
            
            }
            if ($cont == 0)
            {
                print("<script>alert('No existen brazaletes a imprimir');</script>");
                print("<script>window.parent.close();</script>");
            }
          ?>
          </fieldset>


	</body><canvas id="hidden_screenshot" style="display:none;"></canvas>
    <input id="ida" style="width: 1;height: 1;" />

</html>