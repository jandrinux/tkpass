<?
session_start();
if ($_SESSION["autentificado"] != "SI"){
    header("location: Login.php");
}

$descuento = $sub_total * ($dscto_especial/100);


	$asunto="Descuento Especial ".$dscto_especial."% - ".$nombre_usr."";			

    $cuerpo='<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    
    <style type="text/css">
    body {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
    }
    </style>
                            
    </head>
                            
    <body>
                            
    Pasajero Titular: : '.strtoupper($pasajero).'<br>
    Cantidad Pasajeros: '.$cant_adt." ADT / ".$cant_chd." CHD".'<br>
    
    Excursiones: '.strtoupper($excursiones_cadena).'<br>
    
    Descuento Especial: '.$dscto_especial.'%<br>
    Boleta TTS: '.$boleta.'<br>
    
    Sub Total:'.number_format($sub_total).'<br>
    Descuento: '.number_format($descuento).'<br>
    Total: '.number_format($sub_total-$descuento).'<br>


    </center>
    <br><br><br>      
    Por favor, NO responda a este mensaje, es un envío autom&aacute;tico.           
    
    <br>                        
    Este correo electr&oacute;nico ha sido enviado por Turistik.cl <br>
    Copyright @2015 Turistik, All Rights Reserved &reg;
    </center>
    </body>
    </html>';
    

    $destinatari = ['cgamerre@turistik.cl',
                    'mkresse@turistik.cl',
                    'dbraz@turistik.cl',
                    'rendiciones@turistik.cl'];

    $resp = Correo::Email($asunto,$cuerpo,$destinatari, '', '');


?>