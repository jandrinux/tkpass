<?
$fecha_cargo = date('Y-m-d H:i:s');

if ($_SESSION['accion_tr'] == "upt_1")
{

    $LC = $Obj_Tkpass->Consulta_linea_credito();
    $LP = $Obj_Tkpass->Consulta_Saldo_linea_prepago();
    
    $TLC = $LC[0] - $LC[1];
    
    
    if ($LP >= $precio_com)
    {
        $UPT_LP = $LP - $precio_com;
        
        $Obj_Tkpass->Actualiza_linea_prepago($UPT_LP);
        $cargo = $precio_com;
        
        $observacion_de_pago = "$".$cargo." cargo a linea de prepago";
    }
    else
    {
        
        $Obj_Tkpass->Actualiza_linea_prepago(0);
        
        $RESTA_LC = $precio_com - $LP;
        $UPT_LC = $LC[1] + $RESTA_LC;
       
                
        $Obj_Tkpass->Actualiza_linea_credito($UPT_LC);    
        $cargo = $LP;
        
        $observacion_de_pago = "$".$RESTA_LC." cargo a linea de credito, $".$cargo." cargo a linea de prepago";
        
    }
        

        $Obj_Tkpass->Ingresa_cargo_prepago($cargo, $fecha_cargo, $voucher, 4);       
        $precio_abonado_re = $cargo / $cantidad_total_pax ;
        $Obj_Tkpass->Ingresa_observacion_de_pago($observacion_de_pago, $voucher, $precio_abonado_re);
    

        UNSET($_SESSION['accion_tr']);
        UNSET($_SESSION['accion']);
}

if ($_SESSION['accion_tr'] == "upt_2")
{


    $LC = $Obj_Tkpass->Consulta_linea_credito();    
    $TLC = $LC[0] - $LC[1];
    
    if ($TLC >= $precio_com)
    {
        $UPT_LC = $LC[1] + $precio_com;
        $Obj_Tkpass->Actualiza_linea_credito($UPT_LC);
    }
    $precio_abonado_re = 0;
    $observacion_de_pago = "";
    

    $Obj_Tkpass->Ingresa_observacion_de_pago($observacion_de_pago, $voucher, $precio_abonado_re);
    $cargo = $precio_com;
    $Obj_Tkpass->Ingresa_cargo_prepago($cargo, $fecha_cargo, $voucher, 4);



    UNSET($_SESSION['accion_tr']);
    UNSET($_SESSION['accion']);
}

if ($_SESSION['accion_tr'] == "upt_3")
{

    $LC = $Obj_Tkpass->Consulta_linea_credito();
    
    $UPT_LC = $LC[1] + $precio_com;
    $Obj_Tkpass->Actualiza_linea_credito($UPT_LC);
    
    $precio_abonado_re = 0;
    $observacion_de_pago = "";
    
    $Obj_Tkpass->Ingresa_observacion_de_pago($observacion_de_pago, $voucher, $precio_abonado_re);
    
    $cargo = $precio_com;
    $Obj_Tkpass->Ingresa_cargo_prepago($cargo, $fecha_cargo, $voucher, 4);
    

    UNSET($_SESSION['accion_tr']);
    UNSET($_SESSION['accion']);
}

?>