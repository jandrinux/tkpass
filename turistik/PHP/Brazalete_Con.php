<?php
    session_start();
    include("menus.php");
    $_Util=new Util;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<link href="../../css/menu_small.css" rel="stylesheet" type="text/css">
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>

</head>
<body>

<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">
  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr height="50"><td><br></td></tr>
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Consulta de Brazaletes</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    
	<tr><td><br><br><br></td></tr>
  </table>
  
    

</div>
<br /><br /><br /><br /><br />
<div id="menuh" style="margin-left:20px;" > 
<form>
	<table width="350" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="133" class="celdalazul_peque">Serial</td>
    <td width="251"  class="celdalCELEST" ><input name="serial" type="text"  id="serial" /></td>
    </tr>
  <tr>
    <td class="celdalazul_peque">Boleta</td>
    <td  class="celdalCELEST"><input type="text" name="boleta" id="boleta" /> </td>
    </tr>
  <tr>
    <td class="celdalazul_peque">&nbsp;</td>
    <td  class="celdalCELEST"><input class="BotonTurist_Azul" type="button" id="enviar" name="button"  value="Enviar" /></td>
    </tr>
</table>
</form>
</div>
<div id="result"></div>
<br /><br />
<script>
	$(document).on("ready",inicio);
	function inicio()
	{
		$("#enviar").on("click", enviar);
		$("#serial").on("keyup", sKey);
		$("#boleta").on("keyup", sKey);
		
		$("#serial").keydown(function(event) {
			   if(event.shiftKey)
			   {
					event.preventDefault();
			   }
			 
			   if (event.keyCode == 46 || event.keyCode == 8)    {
			   }
			   else {
					if (event.keyCode < 95) {
					  if (event.keyCode < 48 || event.keyCode > 57) {
							event.preventDefault();
					  }
					} 
					else {
						  if (event.keyCode < 96 || event.keyCode > 105) {
							  event.preventDefault();
						  }
					}
				  }
			   });
		$("#boleta").keydown(function(event) {
			   if(event.shiftKey)
			   {
					event.preventDefault();
			   }
			 
			   if (event.keyCode == 46 || event.keyCode == 8)    {
			   }
			   else {
					if (event.keyCode < 95) {
					  if (event.keyCode < 48 || event.keyCode > 57) {
							event.preventDefault();
					  }
					} 
					else {
						  if (event.keyCode < 96 || event.keyCode > 105) {
							  event.preventDefault();
						  }
					}
				  }
			   });
	}
	
	function sKey(event)
	{

		var data =event.target.name;
		var Caja = $("#" + event.target.name).val();
		if( $("#" + event.target.name).val()=="")
		{
			$("#serial").attr('disabled', false);
			$("#boleta").attr('disabled', false);
			
		}
		else if(data=="serial")
		{
			$("#boleta").attr('disabled', true);
		
		}
		else
		{
			$("#serial").attr('disabled', true);
		
		}
		if(event.keycode==13)
		{
			enviar();
		}
	}
	
	
	function enviar()
	{
		console.log("Precion en enviar");
		var serial = $("#serial").val();
		var boleta =$("#boleta").val();
		$( "#result" ).load( "Brazalete_Con_Det.php", {serial:serial,boleta:boleta} );
	}
</script>
</body>
</html>