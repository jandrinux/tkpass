<?
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<!-- calendario-->
<link type="text/css" href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" />
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<!-- calendario-->

<script type="text/javascript">

    $(function() {
        $("#fecha").datepicker(
        {
            dateFormat: "dd/mm/yy",
            dayNames: [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" ],
            dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
            firstDay: 1,
            gotoCurrent: true,
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre" ]
        }
        );
    });
</script>

<title>Encuesta</title>
<style>
.ui-widget {
    font-family: Verdana,Arial,sans-serif;
    font-size: 0.7em;
}

body {
	background-color: #ED3B43;
	margin: 0px;
	padding: 0px;
	font-family: Arial;
	background-attachment: fixed;
}


h1 {
	font-size: 20px;
	color: #333
}
p {
	font-size: 13px;
}
#foto {
	width: 550px;
	float: left;
	background-color: #333;
	height: 1900x;
}
#principal {
	background-color: #ED3B43;
	width: 650px;
    margin: 0 auto;
    text-align: left;
}
#logo {
	width: 135px;
	height: 155px;
	margin-left: auto;
	margin-right: auto;
	padding-top: 30px;
	padding-bottom: 10px;
}
#contenedor {
    border-style:solid;
    border-color: black;
	background-color: #FFF;
	margin-left: 30px;
	margin-right: 30px;
	padding-left: 40px;

}
fieldset {
	margin-bottom: 20px;
	padding: 10px;
}
legend {
	color: #333;
	font-size: 16px;
	font-weight: bold;
}
#btn {
	width: 150px;
	height: 30px;
	margin: auto;
	margin-bottom: 20px;
}
input.submit-button {
	width: 150px;
	height: 30px;
	color: #666
}


#scroller {
	position: absolute;
	width:100%;
	height: 100%;
	top: 0;
	left:-6;
	overflow: auto;
	z-index: -1;
}
.alineadoimagen
{
     float: left; 
     padding-bottom: 10px;
     padding-right: 20px;
}

.titulo
{
    font-size: 34px;
    padding-top: 20px;

}
.how
{
    padding-top: 2px;
    font-size: 13px;
    font-weight: bold;
}

.derecha   { float: right; padding-bottom: 40px; }
.izquierda { float: left;  }

</style>
<script>
function validar()
{

    var c = $("#c").val();
    var b = $("#b").val();
    var g = $("#g").val();
    
    
    var servicio = $("#servicio").val();
    var pregunta_1 = $("#pregunta1").attr("rel");
    var pregunta_2 = $("#pregunta2").attr("rel");
    var pregunta_3 = $("#pregunta3").attr("rel");
    var pregunta_4 = $("#pregunta4").attr("rel");
    var comentario = $("#txt8").val();
    var captcha = $("#captcha-form").val();
    
    
    var pregunta_5 = $('input[name="txt5"]:checked').val();
    /*
    var pregunta_6 = $('input[name="txt6"]:checked').val();
    var pregunta_7 = $('input[name="txt7"]:checked').val();
    */
    if (servicio == 0)
    {
       alert("Seleccione servicio");
       return false; 
    }
   

    if (  pregunta_1 == '' || pregunta_2 == '' || pregunta_3 == '' || pregunta_4 == '')
    {
        alert("Complete la informacion  de la encuesta");
        return false;
    }
    
    if (pregunta_5 == "undefined")
    {
        alert("Complete la informacion  de la encuesta");
        return false;
    }
    else
    {

            $.post("envia_correo.php",{ servicio:servicio, pregunta_1:pregunta_1,  
              pregunta_2:pregunta_2,pregunta_3:pregunta_3,
              pregunta_4:pregunta_4,comentario:comentario,
              pregunta_5:pregunta_5, c:c, b:b, g:g, captcha:captcha },function(data)
            {
                
                if (data == 'ok')
                {
                    $('#opener').click();
                      
                }
                else
                {
                    if (data == 'no-captcha')
                    {
                        $("#msg").html("Captcha ivalido.");
                        $( "#msg" ).dialog();
                                      
                    }
                    else
                    {
                        $("#msg").html("No fue posible enviar la encuesta\n Intente nuevamente clickeando el link de su e-mail.");
                        $( "#msg" ).dialog();
                    }

                }
            })  
            
            
    }

    
}

function cambiar2(control, div) 
{
    debugger;
    $("#" + div + " > img").each(function(row,data){
        var id= $(data).attr("id");
        $(data).attr("src", "img/" + id + "-off.png");
    });

    var id =  $(control).attr("id");
    $("#" + div + " #" + id).attr("src","img/" + id + ".png");
    
    switch(id)
    {
        case "face-a":
        $("#" + div).attr("rel",1);
        break
        
        case "face-b":
        $("#" + div).attr("rel",2);
        break
        
        case "face-c":
        $("#" + div).attr("rel",3);
        break
        
        case "face-d":
        $("#" + div).attr("rel",4);
        break
        
        
        case "face-e":
        $("#" + div).attr("rel",5);
        break        
        
    }

    
}

  $(function() {
       
    $( "#dialog" ).dialog({
      autoOpen: false,
      resizable: false,
      height:280,
      modal: true,
      buttons: {
        "SI": function() {
          location.reload();
        },
        'NO': function() {
            window.close();
        }
      }
    });
    
 
    $( "#opener" ).click(function() {
      $( "#dialog" ).dialog( "open" );
    });
  });

</script>
</head>

<body>
<div id="msg" class="ui-dialog-titlebar ui-dialog-title-dialog" title="INFORMACION"></div>
<div id="dialog" title="Confirmación" style="z-index:2;">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><p>Gracias por su preferencia</p> ¿Desea evaluar otro servicio?</p>
</div>

 
<button id="opener" style="display: none;"></button>


<div>
<div id="scroller">
   	
        <div id="principal">
           
        <div id="contenedor">
        
        
            <p><img class="alineadoimagen" src="../turistik/Imagenes/turistik107_118.jpg" width="110" height="130"/>
                <div class="titulo" style="text-align:left">Encuesta de <br />Satisfacción de Servicio</div> 

            </p>
            
        <form name="form1"  method="post">    
            <h2>
            <p style="clear: both;">Selecciona el servicio que te gustaria evaluar: 
            
            <select id="servicio" name="servicio">
            <option value="0" selected="">-- SELECCIONE --</option>
            <?php
                require ("../turistik/Clases/ClaseConexion.inc.php");
                $miConexion = new ClaseConexion;      
                $miConexion->Conectar();
                $query=$miConexion->EjecutaConsulta(" SELECT * FROM EXCURSION_ORIGEN WHERE ESTADO_EO = 1 ORDER BY NOMBRE_EO ");
                while ($row = mysql_fetch_assoc($query))
                {
    
                    echo "<option value=".$row['CODIGO_EO']." >".$row['NOMBRE_EO']."</option>" ; 
    
                } 
                mysql_free_result($query); 
                mysql_close();
            ?>
            </select>
            </p>
            <p>Fecha Servicio: <input type="text" id="fecha" name="fecha" readonly="" value="<?=date('d/m/Y')?>"/></p> 
            
            </h2>
            

              <p><strong>¿Como evaluarías tu experiencia general en este servicio?</strong></p>
                <div id="pregunta1">
                <img id="face-a" name="cara_uno" src="img/face-a-off.png" onclick="cambiar2(this,'pregunta1')"/>
                <img id="face-b" name="cara_dos" src="img/face-b-off.png" onclick="cambiar2(this,'pregunta1')"/>
                <img id="face-c" name="cara_tres" src="img/face-c-off.png" onclick="cambiar2(this,'pregunta1')"/>
                <img id="face-d" name="cara_cuatro" src="img/face-d-off.png" onclick="cambiar2(this,'pregunta1')"/>
                <img id="face-e" name="cara_cinco" src="img/face-e-off.png" onclick="cambiar2(this,'pregunta1')"/>
                </div>

        	

              <p><strong>¿Como evaluarías el/la conductor(a) del vehículo en este servicio?</strong></p>
                <div id="pregunta2">
                <img id="face-a" name="cara_uno" src="img/face-a-off.png" onclick="cambiar2(this,'pregunta2')"/>
                <img id="face-b" name="cara_dos" src="img/face-b-off.png" onclick="cambiar2(this,'pregunta2')"/>
                <img id="face-c" name="cara_tres" src="img/face-c-off.png" onclick="cambiar2(this,'pregunta2')"/>
                <img id="face-d" name="cara_cuatro" src="img/face-d-off.png" onclick="cambiar2(this,'pregunta2')"/>
                <img id="face-e" name="cara_cinco" src="img/face-e-off.png" onclick="cambiar2(this,'pregunta2')"/>
                </div>
		

             <p><strong>¿Como evaluarías el/la guía de este servicio?</strong></p>
                <div id="pregunta3">
                <img id="face-a" name="cara_uno" src="img/face-a-off.png" onclick="cambiar2(this,'pregunta3')"/>
                <img id="face-b" name="cara_dos" src="img/face-b-off.png" onclick="cambiar2(this,'pregunta3')"/>
                <img id="face-c" name="cara_tres" src="img/face-c-off.png" onclick="cambiar2(this,'pregunta3')"/>
                <img id="face-d" name="cara_cuatro" src="img/face-d-off.png" onclick="cambiar2(this,'pregunta3')"/>
                <img id="face-e" name="cara_cinco" src="img/face-e-off.png" onclick="cambiar2(this,'pregunta3')"/>
                </div>



              <p><strong>¿Como evaluarías la atención del ejecutivo(a) que te vendió este servicio?</strong></p>
                <div id="pregunta4">
                <img id="face-a" name="cara_uno" src="img/face-a-off.png" onclick="cambiar2(this,'pregunta4')"/>
                <img id="face-b" name="cara_dos" src="img/face-b-off.png" onclick="cambiar2(this,'pregunta4')"/>
                <img id="face-c" name="cara_tres" src="img/face-c-off.png" onclick="cambiar2(this,'pregunta4')"/>
                <img id="face-d" name="cara_cuatro" src="img/face-d-off.png" onclick="cambiar2(this,'pregunta4')"/>
                <img id="face-e" name="cara_cinco" src="img/face-e-off.png" onclick="cambiar2(this,'pregunta4')"/>
                </div>

            

              <p><strong>¿Como te enteraste de nosotros?</strong></p>
              <div class="how" >
              <input type="radio" name="txt5" id="txt5" value="Amigo"/>Amigo
              <input type="radio" name="txt5" id="txt5" value="Internet"/>Internet
              <input type="radio" name="txt5" id="txt5" value="Tienda Turistik"/>Tienda Turistik
              <input type="radio" name="txt5" id="txt5" value="Hotel"/>Hotel
              <input type="radio" name="txt5" id="txt5" value="Otro"/>Otro
              </div>

              
               
              <br /><p><strong>Comentario:</strong></p>
                <textarea name="txt8" cols="53" id="txt8" rows="3"></textarea>
              </p>
               
              
            <span class="izquierda">
            <p>Ingresa el texto de seguridad</p>
            <input type="text" name="captcha" id="captcha-form" autocomplete="off" />
            
            </span>
            <span class="derecha">
            <img src="captcha.php" id="captcha" style="margin-right:45px"/>
            <a href="#" onclick="
                    document.getElementById('captcha').src='captcha.php?'+Math.random();
                    document.getElementById('captcha-form').focus();"
                    id="change-image" style="position: absolute; margin-left:50px">Actualizar.</a>
            </span>    

       
        <br />
          <div style="clear:both; margin:auto;"><input class="submit-button" name="enviar" type="button" onclick="validar();" value="Enviar" /></div>
          </fieldset>
          <input type="hidden" value="<?=$_GET['c']?>" name="c" id="c" />
          <input type="hidden" value="<?=$_GET['b']?>" name="b" id="b" />
          <input type="hidden" value="<?=$_GET['g']?>" name="g" id="g" />
          
        
    </form>
        
         <fieldset style="border:none; padding:2px;">  </fieldset>  

          </div>
         </div>
    </div>
</div>



</body>
</html>
