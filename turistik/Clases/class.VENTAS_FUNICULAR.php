<?PHP
class VENTAS_FUNICULAR {
	var $ID_VENTA;
	var $FECHA;
	var $CAJA;
	var $COD_USUA;
	var $TIPO_PAX;
	var $TRAMO;
	var $MONTO;
	var $CANTIDAD;
	var $TOTAL;
	var $NACIONALIDAD;
	var $conectas;

	function __construct() {
		$this->conectas = new ClaseConexion();
		$this->conectas->Conectar(); 
	}
	public function Lista() {
		$sql = "select * from VENTAS_FUNICULAR";
		$res = $this->conectas->EjecutaConsulta($sql);
		while($row=mysql_fetch_array($res)) {
			$c = new VENTAS_FUNICULAR();
			$c->ID_VENTA= $row["ID_VENTA"];
			$c->FECHA= $row["FECHA"];
			$c->CAJA= $row["CAJA"];
			$c->COD_USUA= $row["COD_USUA"];
			$c->TIPO_PAX= $row["TIPO_PAX"];
			$c->TRAMO= $row["TRAMO"];
			$c->MONTO= $row["MONTO"];
			$c->CANTIDAD= $row["CANTIDAD"];
			$c->TOTAL= $row["TOTAL"];
			$c->NACIONALIDAD= $row["NACIONALIDAD"];
			$array[] = $c;
		}
		return $array;
	}

	public function ListaJSON() {
		$data = $this->Lista();
		return json_encode($data);
	}

	public function ListarID_VENTA($ID_VENTAId) {
		$this->ID_VENTA = $ID_VENTAId;
		$sql = "select * from VENTAS_FUNICULAR where ID_VENTA=$ID_VENTAId";
		$res = $this->conectas->EjecutaConsulta($sql);
		while($row=mysql_fetch_array($res)) {
			$this->ID_VENTA= $row["ID_VENTA"];
			$this->FECHA= $row["FECHA"];
			$this->CAJA= $row["CAJA"];
			$this->COD_USUA= $row["COD_USUA"];
			$this->TIPO_PAX= $row["TIPO_PAX"];
			$this->TRAMO= $row["TRAMO"];
			$this->MONTO= $row["MONTO"];
			$this->CANTIDAD= $row["CANTIDAD"];
			$this->TOTAL= $row["TOTAL"];
			$this->NACIONALIDAD= $row["NACIONALIDAD"];
		}
	}

	public function ListarID_VENTAJSON($ID_VENTAId) {
		$data = $this->Lista($ID_VENTAId);
		return json_encode($data);
	}
	public function Where($vars) {
		$sql = "select * from VENTAS_FUNICULAR where $vars";
		$res = $this->conectas->EjecutaConsulta($sql);
		while($row=mysql_fetch_array($res)) {
			$c = new VENTAS_FUNICULAR();
			$c->ID_VENTA= $row["ID_VENTA"];
			$c->FECHA= $row["FECHA"];
			$c->CAJA= $row["CAJA"];
			$c->COD_USUA= $row["COD_USUA"];
			$c->TIPO_PAX= $row["TIPO_PAX"];
			$c->TRAMO= $row["TRAMO"];
			$c->MONTO= $row["MONTO"];
			$c->CANTIDAD= $row["CANTIDAD"];
			$c->TOTAL= $row["TOTAL"];
			$c->NACIONALIDAD= $row["NACIONALIDAD"];
			$array[] = $c;
		}
		return $array;
	}
	public function NACIONALIDAD() {
		$sql = "select NACIONALIDAD from VENTAS_FUNICULAR group by nacionalidad order by NACIONALIDAD asc";
		$res = $this->conectas->EjecutaConsulta($sql);
		while($row=mysql_fetch_array($res)) {
			$c = new VENTAS_FUNICULAR();
			$c->NACIONALIDAD= $row["NACIONALIDAD"];
			$array[] = $c;
		}
		return json_encode($array);
	}
	public function Z($fecha, $caja) {
		$sql = "select TIPO_PAX, TRAMO ,sum(CANTIDAD) AS CANTIDAD,sum(TOTAL) AS TOTAL from VENTAS_FUNICULAR where FECHA > '$fecha 00:00:00' and FECHA < '$fecha 23:59:59' group by TRAMO, TIPO_PAX";
		$res = $this->conectas->EjecutaConsulta($sql);
		while($row=mysql_fetch_array($res)) {
			$c = new VENTAS_FUNICULAR();
			$c->TIPO_PAX= $row["TIPO_PAX"];
			$c->TRAMO= $row["TRAMO"];
			$c->CANTIDAD= $row["CANTIDAD"];
			$c->TOTAL= $row["TOTAL"];
			$array[] = $c;
		}
		return json_encode($array);
	}
	public function WhereJSON($vars) {
		$data = $this->Where($vars);
		return json_encode($data);
	}


	public function Add() {
		$sql = "INSERT INTO VENTAS_FUNICULAR (FECHA,CAJA,COD_USUA,TIPO_PAX,TRAMO,MONTO,CANTIDAD,TOTAL,NACIONALIDAD)"; 
		$sql.= "VALUES('".$this->FECHA."','".$this->CAJA."','".$this->COD_USUA."','".$this->TIPO_PAX."','".$this->TRAMO."','".$this->MONTO."','".$this->CANTIDAD."','".$this->TOTAL."','".$this->NACIONALIDAD."')";
		$res = $this->conectas->EjecutaConsulta($sql);
		return "OK";
	}

	public function Update() {
		$sql = "Update  VENTAS_FUNICULAR set FECHA='".$this->FECHA."',CAJA='".$this->CAJA."',COD_USUA='".$this->COD_USUA."',TIPO_PAX='".$this->TIPO_PAX."',TRAMO='".$this->TRAMO."',MONTO='".$this->MONTO."',CANTIDAD='".$this->CANTIDAD."',TOTAL='".$this->TOTAL."',NACIONALIDAD='".$this->NACIONALIDAD."' where ID_VENTA='".$this->ID_VENTA."' "; 
		$res = $this->conectas->EjecutaConsulta($sql);
		return "OK";
	}

	public function delete() {
		$sql= "delete from  VENTAS_FUNICULAR where ID_VENTA='".$this->ID_VENTA."' ";
		$res = $this->conectas->EjecutaConsulta($sql);
		return "OK";
	}
}
?>
