
<?php
    session_start();
    include("menus.php");
    $NUsuario=$_SESSION['Nivel_Usuario'];
    $_Util=new Util;
    /*
    include ("../functions/consulta_promociones.php");
    $consulta_promociones = new Consulta_Promociones();
    $bol = $consulta_promociones->Cargar_Correlativo_Folio($_SESSION['codigo_suc']);
    */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.:TURISTIK - Stock Documentos:.</title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<link href="../../css/menu_small.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>


<script type="text/javascript">


function cargaStock()
{
    
    var folio_desde = document.getElementById("folio_desde").value;
    var cantidad = document.getElementById('cantidad').value;
    var constante = 50;
    folio_desde = folio_desde - 1;

    if (folio_desde != -1)
    {
        var suma = constante*cantidad;
        var result = parseInt(suma) + parseInt(folio_desde);
        document.getElementById("folio_hasta").value = result;
    }
    else
    {
        document.getElementById("folio_hasta").value = '';
    }


    
}

function BuscarCorrelativo()
{
    $.post("BuscarCorrelativoBoleta.php",{ tipoBoleta:$('#tipoBoleta').val() },function(resp)
    {
        
        document.getElementById("folio_desde").value = resp;
        cargaStock();
        
       
    })
}

function Agregar()
{
    
    tabla.style.display = 'none';
    $("#msgbox").removeClass().html('<p style="margin-left:280px; margin-top:50px"><img src="../Imagenes/load.gif"/></p>').fadeIn(1000);
    
        $.post("ingreso_boletas.php",{ folio_desde:$('#folio_desde').val(), cantidad:$('#cantidad').val(), tipoBoleta:$('#tipoBoleta').val() },function(data)
        {
            
            
            if (data == 'ok')
            {
                tabla.style.display = '';
                msgbox.style.display = 'none';
                alert("Boletas agregadas exitosamente");
                
            }
            if (data == 'no')
            {
                tabla.style.display = '';
                msgbox.style.display = 'none';
                alert("Folios invalidos, ya se encuentran registrados.\n Intente con otro nuevamente");
            }
            
            location.reload();
        })
        
}
</script>

</head>
<body onload="BuscarCorrelativo()">

<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>
<div class="TituloPlomo" id="Layer3">
  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr height="50"><td><br></td></tr>
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Ingreso Documentos </b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    
	<tr><td><br><br><br></td></tr>
  </table>
  
<div id="menuh" >
        <ul>
           <li><a href="stock_documentos.php" id="primero">Ingeso</a></li>
           <li><a href="detalle_boletas_tts.php" >Boletas TTS</a></li>
          <li><a href="detalle_boletas_ttk.php">Boletas Turistik</a></li>
          <li><a href="#">NCR TTS</a></li>
          <li><a href="#">NCR Turistik</a></li>
        </ul>
</div>
<br />


<div id='msgbox' style='display:none'></div> 
<table id="tabla" align="left" width="590" border="0" cellpadding="0" cellspacing="0">
    <form name="nuevo_stock"  method="POST">
    
    <tr>
        <td class="celdalazul_peque">Fecha:</td>
        <td class="celdalCELEST"><? echo date("d-m-Y"); ?></td>
        <td class="celdalazul_peque">Tipo Documento</td>
        <td class="celdalCELEST">
            <select name="tipoBoleta" id="tipoBoleta" class="seleccionTurist_180" onchange="BuscarCorrelativo();">
                <option value="E" selected="">Exenta (TTS)</option>
                <option value="A" >Afecta (TURISTIK)</option>
                <option value="NTTS" >NCR (TTS)</option>
                <option value="NTTK" >NCR (TURISTIK)</option>
            </select>
        </td>
    </tr>
    <tr>
        <td class="celdalazul_peque">Sucursal: *</td>
        <td class="celdalCELEST"><input type="hidden" name="sucursal" id="sucursal" value="1" />CASA MATRIZ</td>

        <td class="celdalazul_peque">Cantidad Talonarios</td>
        <td class="celdalCELEST">
            <select name="cantidad" id="cantidad" onchange="cargaStock()" class="seleccionTurist_180">
	        <?
	         $z=1;
	         for($z=1; $z<=100; $z++)
	         {
                echo "<option value='".$z."'>".$z."</option>";        
	            
	         }
	        ?> 
            </select>
        </td>
    </tr>
    <tr>
        <td class="celdalazul_peque">Documento Desde: *</td>
        <td class="celdalCELEST"><input type="text" class="seleccionTurist_180" onkeyup="cargaStock()"  name="folio_desde" id="folio_desde" maxlength="6" onkeypress="return validarP(event);"/></td>
        <td class="celdalazul_peque">Documento Hasta: </td>
        <td class="celdalCELEST"><input type="text" class="seleccionTurist_180"  readonly="" id="folio_hasta" maxlength="6" name="folio_hasta" /></td>

    </tr>  
                                     
        <td colspan="100%" align="center" valign="bottom" style="color: tomato;"><br>
    	  <input type="button" name="enviar" class="BotonTurist_Celest" value="Agregar" onclick="Agregar()"  />
        </td>
    </tr>
    </form>
    
</table>
    

</div>

</body>
</html>