function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function ValidarLogin(f){
	if (f.usuario.value == ''){
		alert('Debe ingresar el usuario')
		f.usuario.focus()
		return false;
	}
	if (f.clave.value == ''){
		alert('Debe ingresar la clave')
		f.clave.focus()
		return false;
	}		
}
function ValidarOlvido(f){
	email = f.mailOlvido;
	if(email.value.indexOf("@") < 1 || email.value.indexOf(".") < 1 || email.value.length < 6){
		alert('Formato de email no valido')
		return false;	
	}
	return true;
}

function expandir(obj){
	with(document.getElementById(obj)){
		if(style.display=="none"){
			style.display="block";
		}else{
			style.display="none";
		}
	}
}
function verFoto(obj){
	document.getElementById("imgCont").src=obj.src;
}
function Imprimir(pagina) {
	win_w=695;
	win_h=510;
	posLeft=(win_ancho/2)-(win_w/2);
	posTop=(win_alto/2)-(win_h/2);
	winB = window.open(pagina, 'ventanaF', 'width='+win_w+', height='+win_h+', top='+posTop+', left='+posLeft+', scrollbars=yes, status=no, resizable=no, toolbar=no');
	winB.focus();
}
function EnviarXmail(pagina) {
	win_w=300;
	win_h=195;
	posLeft=(screen.availWidth/2)-(win_w/2);
	posTop=(screen.availHeight/2)-(win_h/2);
	winB = window.open('enviarpormail.asp?pagina='+pagina, 'ventanaF', 'width='+win_w+', height='+win_h+', top='+posTop+', left='+posLeft+', scrollbars=no, resizable=no, toolbar=no');
	winB.focus();
}
win_ancho=screen.availWidth;
win_alto=screen.availHeight;

function verReglasTarifa(id){
	var w = window.open("ar_reglastarifas.asp?id="+id,"reglas","width=400,height=400,scrollbars=yes");
	w.focus();
	return false;
}



//////////////////////////////////////////////////
function VerificarRUT(rut)
{
  	var tmpstr = "";
  	var i;

	for ( i=0; i < rut.length ; i++ )
		if ( rut.charAt(i) != ' ' && rut.charAt(i) != '.' && rut.charAt(i) != '-' )
			tmpstr = tmpstr + rut.charAt(i);
	rut = tmpstr;
	
  	if ( rut == "" )
	{
		//alert( "RUT Incorrecto");
		return false;
	}
	if ( !checkRutField(rut) )
		return false;
		
   return true
	
}

function checkDV( crut )
{
	var largo, crp, dv, suma, mul; 
	
	largo = crut.length;
	
	if (largo < 2)
	{
		//alert("Rut no v�lido por extensi�n.")
		return false;
	}

	crp = crut.substring(0, largo - 1);
	dv = crut.charAt(largo-1);
	
	if ( crp == null || dv == null )
		return false;

	var dvr = '0'

	suma = 0
	mul  = 2

	for (i= crp.length -1 ; i >= 0; i--)
	{
		suma = suma + crp.charAt(i) * mul
		if (mul == 7)
			mul = 2
		else    
			mul++
	}

	res = suma % 11
	if (res==1)
		dvr = 'K'
	else if (res==0)
		dvr = '0'
	else
	{
		dvi = 11-res
		dvr = dvi + ""
	}

	if ( dvr != dv.toUpperCase() )
	{
		//alert("RUT Incorrecto")
		return false
	}
	
	//formatoRut(crp+dvr);
    return true;
}

///////////////////////////////////////////////////////

function checkRutField(texto)
{
  var tmpstr = "";
  var i, largo;
  for ( i=0; i < texto.length ; i++ )
    if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' )
      tmpstr = tmpstr + texto.charAt(i);
  texto = tmpstr;
  largo = texto.length;

  if ( largo < 2 )
  {
    //alert("Debe ingresar el rut completo")
    return false;
  }

  for (i=0; i < largo ; i++ )
  { 
    if ( texto.charAt(i) !="0" && texto.charAt(i) != "1" && texto.charAt(i) !="2" && texto.charAt(i) != "3" && texto.charAt(i) != "4" && texto.charAt(i) !="5" && texto.charAt(i) != "6" && texto.charAt(i) != "7" && texto.charAt(i) !="8" && texto.charAt(i) != "9" && texto.charAt(i) !="k" && texto.charAt(i) != "K" ) 
    {
      //alert("El valor ingresado no corresponde a un R.U.T v�lido");
      return false;
    }
  }
  
  if ( checkDV(texto) )
    return true;
  return false;
}
  
//////////////////////////////////////////////////
  
function formatoRut(objTxt){
  var tmpstr = '';
  var dtexto = '';
  var i;
  var texto;
  texto = limpiaRut(objTxt.value);
  
  largo = texto.length;
  if (largo==0)return false;
  
  tmpstr = texto.charAt(largo-1)
  texto = texto.substr(0, largo-1)
  dtexto = '';
   
  while(texto.length>0){
     if(texto.length>3){
 	dtexto = '.' +  texto.substr(texto.length-3) + dtexto
 	texto = texto.substr(0, texto.length-3);
     }else{
     	dtexto = texto + dtexto;
     	texto = '';
     }
  }
  
  objTxt.value = dtexto + '-' + tmpstr.toUpperCase();  
}

function formatoPuntos(objTxt){
  var dtexto;
  var texto;
  var largo;
  var i;
  var tmptexto;
  
  tmptexto = objTxt.value;
  largo = tmptexto.length; 
  texto = '';
  dtexto = '';
  
  
  
  for(i=0; i<largo; i++){
     if (tmptexto.charAt(i) != ' ' && tmptexto.charAt(i) != '.')
	texto += tmptexto.charAt(i);
  }
  
  while(texto.length>0){
     if(texto.length>3){
 	dtexto = '.' +  texto.substr(texto.length-3) + dtexto
 	texto = texto.substr(0, texto.length-3);
     }else{
     	dtexto = texto + dtexto;
     	texto = '';
     }
  }
  objTxt.value = dtexto;
}

function limpiaRut(strRut){
  var texto;
  var largo;
  var i;
  var tmptexto;
  
  tmptexto = strRut;
  largo = tmptexto.length; 
  texto = '';
  
   for(i=0; i<largo; i++){
     if (tmptexto.charAt(i) != ' ' && tmptexto.charAt(i) != '.' && tmptexto.charAt(i) != '-')
	texto += tmptexto.charAt(i);
   }
   return texto;
}	