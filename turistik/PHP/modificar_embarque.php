<?
session_start();
require ("../Clases/ClaseConexion.inc.php");
include ("../Clases/class.embarque.php");
include ("../Clases/class.bitacora.php");
require ("../functions/tkpass.php");

$codigo = $_POST['codigo'];
$cod_cierre = $_POST['cod_cierre'];
$embarque = $_POST['embarque'];
$tipo_re = $_POST['tipo_reserva'];
$estado_emb = $_POST['estado']; // es el estado que se selecciona en el radio
$muestra_accion = $_POST['muestra_accion'];
$boleta_re = $_POST['boleta_re'];
$comision = $_POST['comision'];




$Embarque = new Embarque;
$return = $Embarque->modificar_accion_embarque($muestra_accion,$estado_emb,$cod_cierre,$tipo_re,$boleta_re,$codigo,$comision);



switch ($return)
{
    case 1:
        $evento = "MODIFICA ESTADO EMBARQUE";
        $observacion_bitacora = "MODIFICA ESTADO EMB NO SHOW, CIERRE RESERVA NO SHOW";
        break;
    case 2:
        $evento = "MODIFICA ESTADO EMBARQUE";
        $observacion_bitacora = "MODIFICA ESTADO EMB NO SHOW";
        break;
    case 3:
        $evento = "MODIFICA ESTADO EMBARQUE";
        $observacion_bitacora = "MODIFICA ESTADO EMB NO SHOW, MODIFICA COMISION RA";  
        break;
    case 4:
        $evento = "MODIFICA ESTADO EMBARQUE";
        $observacion_bitacora = "MODIFICA ESTADO EMB EMBARCADO"; 
        break;
    case 5:
        $evento = "MODIFICA ESTADO EMBARQUE";
        $observacion_bitacora = "MODIFICA ESTADO EMB PENDIENTE, CIERRE RESERVA PENDIENTE";  
        break;
    case 6:
        $evento = "MODIFICA ESTADO EMBARQUE";
        $observacion_bitacora = "MODIFICA ESTADO EMB PENDIENTE, CIERRE RESERVA PAGADO";
        break;
    case 7:
        $evento = "MODIFICA ESTADO EMBARQUE";
        $observacion_bitacora = "MODIFICA ESTADO EMB PENDIENTE, CIERRE RESERVA PAGADO";
        break;
    case 8:
        $evento = "MODIFICA ESTADO EMBARQUE";
        $observacion_bitacora = "MODIFICA ESTADO EMB PENDIENTE, CIERRE RESERVA PENDIENTE, REVERSA COBRO NO SHOW";
        break;
    case 9:
        $error = "Accion incompleta, Administracion ya a procesado el resgistro";
        break;
}

if ($return != 9)
{
    $b = new Bitacora();
    $b->FECHA = date("Y-m-d  h:i:s");
    $b->NUEVO = true;
    $b->USUARIO = $_SESSION['usuario'];
    $b->ARCHIVO ="Modificar_Estado_Emabarque.php";
    $b->CODIGO_RE=$codigo;
    $b->EVENTO =$evento;
    $b->DESCRIPCION=$observacion_bitacora;
    $b->add();
    
    print("<script>window.opener.location.reload();</script>");
}
else
{
    print("<script>alert(".$error.");</script>");
}
        
print("<script>window.parent.close();</script>");
?>