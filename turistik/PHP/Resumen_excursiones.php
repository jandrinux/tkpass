<?php
    session_start();
    include("menus.php");
    $_Util=new Util;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../../js/ui.core.js"></script>
<script type="text/javascript" src="../../js/ui.datepicker.js"></script>
<link type="text/css" href="../../css/demos.css" rel="stylesheet" />
<script type="text/javascript">
$(function() {
	$("#datepicker").datepicker();
	$("#datepicker1").datepicker();
});


</script>

</head>

<body>
<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">
  <table width="482" border="0" cellpadding="0" cellspacing="0">  
	<tr>
      <td width="80" rowspan="4"><img src="../Imagenes/Archivero.png" width="80" height="80" /></td>
      <td width="13">&nbsp;</td>
      <td width="389">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong id="TituloPlomo"><b>Resumen Ventas</b></strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
    </tr>    

  </table>
  
    

<div id="Layer1">
<!-- Aqui va el cuerpo -->

<form  name="" method="post" onSubmit="consulta_resumen(); return false" action="">
<table width="700"  border="0" cellspacing="0" >
  <tr>
    <td width="100" class="celdalazul_peque">Fecha Venta Desde </td>
    <td width="100" class="celdalCELEST">     
	   <input type="text" value="<?echo date('d/m/Y');?>" id="datepicker" name="fecha_rango_1" class="seleccionTurist_180" maxlength="15"/>
    </td> 
    <td width="100" class="celdalazul_peque">Fecha Venta Hasta</td>    
    <td width="250" class="celdalCELEST">	
		<input type="text" value="<?echo date('d/m/Y');?>" id="datepicker1" name="fecha_rango_2" class="seleccionTurist_180" maxlength="15" />
        <input type="submit"  class="BotonAzul2"  value="Consultar" />
    </td>   
  </tr>

</table>
</form>

<div id="resultado_listar"></div>

</div>
</body>
</html>