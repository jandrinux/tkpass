<html>
<head>
<title>CALENDARIO</title>
<link  href="../Sheets.css" rel="stylesheet" type="text/css" >
<script>
function ponPrefijo(pref)
	{
		opener.document.form1.uso_f.value = pref;
		window.close();	
	}
</script>
<link href="../estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo2 {color: #1A50B8}
a:link {
	color: #ECE9D8;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #0000CC;
}
a:hover {
	text-decoration: underline;
}
a:active {
	text-decoration: none;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../estilos1.css" rel="stylesheet" type="text/css">
</head>
<body >
<div align="center">
<? 
function CalculanumeroDiaSemana($dia,$mes,$ano)
    {
        $numeroDiaSemana = date('w', mktime(0,0,0,$mes,$dia,$ano));
        if ($numeroDiaSemana == 0) 
           $numeroDiaSemana = 6;
        else
           $numeroDiaSemana--;
        return $numeroDiaSemana;
    }

//funcion que devuelve el �ltimo d�a de un mes y a�o dados
function ultimoDia($mes,$ano)
    { 
        $ultimoDia=28; 
        while (checkdate($mes,$ultimoDia + 1,$ano))
            { 
                $ultimoDia++; 
            } 
        return $ultimoDia; 
    } 

function DameNombreMes($mes)
    {
        switch ($mes)
            {
                case 1:
                $nombreMes="Enero";
                break;
                case 2:
                $nombreMes="Febrero";
                break;
                case 3:
                $nombreMes="Marzo";
                break;
                case 4:
                $nombreMes="Abril";
                break;
                case 5:
                $nombreMes="Mayo";
                break;
                case 6:
                $nombreMes="Junio";
                break;
                case 7:
                $nombreMes="Julio";
                break;
                case 8:
                $nombreMes="Agosto";
                break;
                case 9:
                $nombreMes="Septiembre";
                break;
                case 10:
                $nombreMes="Octubre";
                break;
                case 11:
                $nombreMes="Noviembre";
                break;
                case 12:
                $nombreMes="Diciembre";
                break;
       }
    return $nombreMes;
    }

function PonCero($xx)
    {
        if (strlen($xx)==1)
            {
                $xx =  "0".$xx;
            }
        return $xx;
    }


function mostrarCalendario($dia,$mes,$ano)
    {
        $mesHoy=date("m");
        $anoHoy=date("Y");
        if (($mesHoy <> $mes) || ($anoHoy <> $ano))
            {
                $hoy=0;
            }
        else
            {
                $hoy=date("d");
            }
		//tomo el nombre del mes que hay que imprimir
        $nombreMes = DameNombreMes($mes);
		//construyo la cabecera de la tabla
        echo "<table width=200 cellspacing=3 cellpadding=2 border=0><tr><td colspan=7 class=titulocentradodos>";
        echo "<table width=100% cellspacing=2 cellpadding=2 border=0><tr><td style=font-size:10pt;font-weight:bold;color:white><tr>";
		//calculo el mes y ano del mes anterior
		//$mesAnterior = $mes - 1;	
        $mesAnterior = $mes;
        $anoAnterior = $ano;
        $anoAnterior--;
		
        echo "<td><acronym title='(-)'><a style=color:white;text-decoration:none href=Calendario_Reporte_Uso_F.php?dia=1&nuevo_mes=$mesAnterior&nuevo_ano=$anoAnterior>&lt;&lt;</a></acronym></td>";
			 
        $mesAnterior = $mes - 1;
        $anoAnterior = $ano;
        if ($mesAnterior==0)
            {
                $anoAnterior--;
                $mesAnterior=12;
            }
       
	   echo "<td><acronym title='(-)'><a style=color:white;text-decoration:none href=Calendario_Reporte_Uso_F.php?dia=1&nuevo_mes=$mesAnterior&nuevo_ano=$anoAnterior>&lt;</a></acronym></td>";
       echo "<td class=titulocentradoblanco>$nombreMes $ano</td>";
	   //echo "<td align=right style=font-size:10pt;font-weight:bold;color:white>";
		//calculo el mes y ano del mes siguiente
		//$mesSiguiente = $mes + 1;	
	
        $mesSiguiente = $mes + 1;
        $anoSiguiente = $ano;
        if ($mesSiguiente==13)
            {
                $anoSiguiente++;
                $mesSiguiente=1;
            }
        echo "<td><acronym title='(+)'><a style=color:white;text-decoration:none href=Calendario_Reporte_Uso_F.php?dia=1&nuevo_mes=$mesSiguiente&nuevo_ano=$anoSiguiente>&gt;</a></acronym></td>";
	
        $mesSiguiente = $mes;
        $anoSiguiente = $ano;
        $anoSiguiente++;
		
        echo "<td><acronym title='(+)'><a style=color:white;text-decoration:none href=Calendario_Reporte_Uso_F.php?dia=1&nuevo_mes=$mesSiguiente&nuevo_ano=$anoSiguiente>&gt;&gt;</a></acronym></td></tr></table></td></tr>";
        echo '	<tr>
			    <td width=14% class=subtituloc>Lu</td>
			    <td width=14% class=subtituloc>Ma</td>
			    <td width=14% class=subtituloc>Mi</td>
			    <td width=14% class=subtituloc>Ju</td>
			    <td width=14% class=subtituloc>Vi</td>
			    <td width=14% class=subtituloc>Sa</td>
			    <td width=14% class=subtituloc>Do</td>

			</tr>';
	
		//Variable para llevar la cuenta del dia actual
        $diaActual = 1;
	
		//calculo el numero del dia de la semana del primer dia
        $numeroDia = CalculanumeroDiaSemana(1,$mes,$ano);
		//echo "Numero del dia de demana del primer: $numeroDia <br>";
	
		//calculo el �ltimo dia del mes
        $ultimoDia = ultimoDia($mes,$ano);
	
//escribo la primera fila de la semana
        echo "<tr>";
        for ($i=0;$i<7;$i++)
            {
                if ($i < $numeroDia)
                    {
						//si el dia de la semana i es menor que el numero del primer dia de la semana no pongo nada en la celda
                        echo "<td class=celdac></td>";
                    } 
                else 
                    {
                        if (($i == 5) || ($i == 6))
                            {
                                if ($diaActual == $hoy)
                                    {
                                        echo "<td class=celdacca><a href=#  onclick=\"ponPrefijo('".PonCero($diaActual)."/".PonCero($mes)."/".$ano."')\">$diaActual</a></td>";
                                    }
                                else
                                    {
                                        echo "<td class=suberrorc><a href=#  onclick=\"ponPrefijo('".PonCero($diaActual)."/".PonCero($mes)."/".$ano."')\">$diaActual</a></td>";
                                    }
                            }
                        else
                            {			
                                if ($diaActual == $hoy)
                                    {
                                        echo "<td class=celdacca><a href=#  onclick=\"ponPrefijo('".PonCero($diaActual)."/".PonCero($mes)."/".$ano."')\">$diaActual</a></td>";
                                    }
                                else
                                    {
                                        echo "<td class=celdac><a href=#  onclick=\"ponPrefijo('".PonCero($diaActual)."/".PonCero($mes)."/".$ano."')\">$diaActual</a></td>";
                                    }
                            }
                       $diaActual++;
                  }
            }
       echo "</tr>";
	   //recorro todos los dem�s d�as hasta el final del mes
	   $numeroDia = 0;
       while ($diaActual <= $ultimoDia)
           {
             //si estamos a principio de la semana escribo el <TR>
               if ($numeroDia == 0)
                   echo "<tr>";
                   //si es el u�timo de la semana, me pongo al principio de la semana y escribo el </tr>
                   if (($numeroDia == 5) || ($numeroDia == 6))
                       {
                           if ($diaActual == $hoy)
                               {
                                   echo "<td class=celdacca><a href=#  onclick=\"ponPrefijo('".PonCero($diaActual)."/".PonCero($mes)."/".$ano."')\">$diaActual</a></td>";
                               }
                          else
                               {
                                   echo "<td class=suberrorc><a href=#  onclick=\"ponPrefijo('".PonCero($diaActual)."/".PonCero($mes)."/".$ano."')\">$diaActual</a></td>";
                               }
                       }
                   else
                       {		
                           if ($diaActual == $hoy)
                               {
                                   echo "<td class=celdacca><a href=#  onclick=\"ponPrefijo('".PonCero($diaActual)."/".PonCero($mes)."/".$ano."')\">$diaActual</a></td>";
                               }
                           else
                               {
                                   echo "<td class=celdac><a href=#  onclick=\"ponPrefijo('".PonCero($diaActual)."/".PonCero($mes)."/".$ano."')\">$diaActual</a></td>";
                               }
                      }
                  $diaActual++;
                  $numeroDia++;
                  if ($numeroDia == 7)
                      {
                          $numeroDia = 0;
                          echo "</tr>";
                      }
            }
//compruebo que celdas me faltan por escribir vacias de la �ltima semana del mes
        for ($i=$numeroDia;$i<7;$i++)
            {
                echo "<td></td>";
            }
	    echo "</tr>";
        echo "</table>";
   }	

if (!$HTTP_POST_VARS && !$HTTP_GET_VARS)
   {
        $tiempoActual = time();
        $mes = date("n", $tiempoActual);
        $ano = date("Y", $tiempoActual);
        $dia=date("d");
        $fecha=$ano . "-" . $mes . "-" . $dia;
    }
else 
    {
	    $mes = $_GET['nuevo_mes'];
        $ano = $_GET['nuevo_ano'];
        $dia = $_GET['dia'];
        $fecha=$ano . "-" . $mes . "-" . $dia;
    }

mostrarCalendario($dia,$mes,$ano);
?>
</div>
<span class="Estilo2"></span>
</body>
</html>
