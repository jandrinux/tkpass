<?php
    include("menus.php");
    $miConexion = new ClaseConexion;
    $miConexion->Conectar();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Estadistica de Venta</title>
<link href="../estilos1.css" rel="stylesheet" type="text/css">
<link href="../../css/menu_small.css" rel="stylesheet" type="text/css">
	<link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
	<script type="text/javascript" src="../../js/ui.core.js"></script>
	<script type="text/javascript" src="../../js/ui.datepicker.js"></script>
    <script type="text/javascript" src="../../js/Order.js"></script> 
<script>
	$(function() {
		$("#fechai").datepicker();
        $("#fechai2").datepicker();
    });
function consultar()
{
    var Tipo= document.getElementById("Informe").value;

    if (Tipo=="0"){return;}
    if(Tipo=="2"){
          document.getElementById("Nacionalidad").disabled = false;
          document.getElementById("Vendedor").disabled = true;
    }else{
          document.getElementById("Nacionalidad").disabled = true;
          document.getElementById("Vendedor").disabled = false;
          }
    if (Tipo==3){
           document.getElementById("Nacionalidad").disabled = true;
          document.getElementById("Vendedor").disabled = true;
        
    }
}

function valida_fecha(){
      var fecha_1 = document.getElementById("fechai").value;
      var fecha_2 = document.getElementById("fechai2").value;
      fecha_1 = TFecha(fecha_1);
      fecha_2 = TFecha(fecha_2);
       

      fecha1 = new Date();
      fecha1.setTime(Date.parse(fecha_1));
      fecha2 = new Date();
      fecha2.setTime(Date.parse(fecha_2));


    if (compare_dates(fecha1, fecha2)){
        alert("La Fecha Final debe ser menor a la fecha inicial");
        return false;
    }
    else{
        return true;
    }
}
function TFecha(fecha){
    //var dt =fecha.substring(3,5)+"/"+fecha.substring(0, 2)+"/"+fecha.substring(6,10);
}
   function compare_dates(fecha, fecha2)
      {
        var xMonth=fecha.substring(3, 5);
        var xDay=fecha.substring(0, 2);
        var xYear=fecha.substring(6,10);
        var yMonth=fecha2.substring(3, 5);
        var yDay=fecha2.substring(0, 2);
        var yYear=fecha2.substring(6,10);
        if (xYear> yYear)
        {
            return(true)
        }
        else
        {
          if (xYear == yYear)
          { 
            if (xMonth> yMonth)
            {
                return(true)
            }
            else
            { 
              if (xMonth == yMonth)
              {
                if (xDay> yDay)
                  return(true);
                else
                  return(false);
              }
              else
                return(false);
            }
          }
          else
            return(false);
        }
    }

</script>
</head>

<body>
<br /><br /><br />
<form id="fo3" name="fo3" method="GET"      onsubmit="javascript:return valida_fecha() ">
    <table width="70%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td class="celdalazul_peque" width="167">Fecha Inicial:</td>
        <td  class="celdalCELEST" width="654"><label for="fechai"></label>
        <input type="text" name="fechai" id="fechai" /></td>
      </tr>
      <tr>
        <td class="celdalazul_peque">Fecha Final:</td>
        <td class="celdalCELEST"><input type="text" name="fechai2" id="fechai2" /></td>
      </tr>
      <tr>
        <td class="celdalazul_peque">Sucursal:</td>
        <td class="celdalCELEST"><label for="Sucursal"></label>
          <select name="Sucursal" id="Sucursal">
          <option value="0">SELECCIONE SUCURSAL</option>
          <option value="-1">:::::::TODOS::::::</option>
        <?php
     	    $query=$miConexion->EjecutaConsulta("select * FROM SUCURSAL");	
        	while ($row = mysql_fetch_assoc($query)){
                echo '<option value="'.$row['CODIGO_SUCURSAL'].'">'.$row['SUCURSAL'].'</option>';
             }
         ?>
            
        </select></td>
      </tr>
      <tr>
        <td class="celdalazul_peque">Tipo Informe:</td>
        <td class="celdalCELEST"><select name="Informe" id="Informe" onchange="javascript:consultar();">
         <option value="0">SELECCIONE TIPO INFORME</option>
         <option value="2">Ventas Por Nacionalidad</option>
         <option value="1">Ventas Por Vendedor</option>
          <option value="3">Ventas Por Horas</option>
          
        </select></td>
      </tr>
      <tr>
        <td class="celdalazul_peque">Nacionalidad:</td>
        <td class="celdalCELEST"><select name="Sucursal2" id="Nacionalidad" disabled="true" >
             <option value="0">SELECCIONE NACIONALIDAD</option>
          <option value="-1">:::::::TODOS::::::</option>
        <?php
     	    $query=$miConexion->EjecutaConsulta("select * FROM NACIONALIDAD");	
        	while ($row = mysql_fetch_assoc($query)){
                echo '<option value="'.$row['CODIGO_NAC'].'">'.$row['PAIS_NAC'].'</option>';
             }   
         ?>
            
        </select></td>
      </tr>
      <tr>
        <td  class="celdalazul_peque">Vendedor:</td>
        <td  class="celdalCELEST"><select name="Vendedor" id="Vendedor" disabled="true"  >
            <option value="0">SELECCIONE VENDEDOR</option>
          <option value="-1">:::::::TODOS::::::</option>
        <?php
     	    $query=$miConexion->EjecutaConsulta("select * from USUARIO WHERE COD_TUSUA=4");	
        	while ($row = mysql_fetch_assoc($query)){
                echo '<option value="'.$row['COD_USUA'].'">'.$row['NOMBRE_USUA'].' '.$row['APELLIDO_USUA'].  '</option>';
             }   
         ?>
        </select></td>
      </tr>
      <tr>
        <td class="celdalazul_peque">&nbsp;</td>
        <td class="celdalCELEST">&nbsp;</td> 
      </tr>
      <tr>
        <td class="celdalazul_peque">&nbsp;</td>
        <td class="celdalCELEST"> <input class="BotonTurist_Celest" type="submit" name="button" id="button" value="Enviar"/> </td>
      </tr>
    </table>
</form>
</body>
</html>
