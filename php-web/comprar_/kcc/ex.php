<?
ini_set('display_errors', 1);

date_default_timezone_set("America/Santiago");

include ('../../../lib/MPDF/mpdf.php');
require ("../../Clases/ClaseConexion.inc.php");
require ("../../Clases/ClaseUtil.inc.php");
require ("class/class.mail.php");
require ("../../../turistik/functions/tkpass.php");
require ("../../../turistik/functions/consulta_promociones.php");


$orden_compra = 'OC_1502024851';
$trs_id_session = '142662531620150317';
$excursiones_stk = array(6,54,55,63,64);

$session_id='';
$_Util = new Util;

$partes = strpos($trs_id_session, "/");

if ($partes == true)
{
    #echo "hay /";
    $boletas = explode("/",$trs_id_session);
    $tts = $boletas[1];
    $ttk = $boletas[2];  
    $sucursal = $boletas[3];  
}



$miConexion = new ClaseConexion;
$miConexion->Conectar();
$query=$miConexion->EjecutaSP("Consulta_Operacion","  '".$orden_compra."','".$session_id."' ");  
$res = mysql_num_rows($query);
while ($row = mysql_fetch_assoc($query))
{
    $interes = "";
	$pagado = "";
    $nombre_comprador = strtoupper($row['nombre_comprador']);
	$email = strtolower($row['email']);
    $destinatari = $email;
	$tipo_pago = $row['tipo_pago'];
	$cantidad = $row['cantidad'];
	$cod_prod = $row['cod_producto'];
    $id_session = strlen($row['id_session']);
    

	if($cod_prod == '20')
    {
		$hora = $row['hora_op'];
		$vuelo = $row['vuelo_op'];
	}
    
	$valoru = $row['valor_unitario'];
	
	$monto =  $row['monto'];
	$st_monto = number_format($monto,0,',','.');
	$cod_auto = $row['cod_autoriza'];
	$direccion = $row['DESCRIPCION_HL'];
    $fecha_servicio = $row['fecha'];
	$fecha = $_Util->FormatearFecha($row['fecha']);
	$resumen = $row['RESUMEN_EO'];
	$orden_compra = $row['orden_compra'];
	$nombre_eo = $row['NOMBRE_EO'];
   // $voucher = $row['VOUCHER_RE'];

	if($tipo_pago == 'VN')
    {
		$num_cuotas= "(00) SIN ";
	}
	if($tipo_pago == 'VC')
    {
		$pagado = " EN ";
		$num_cuotas=$row['cuotas'];
		$interes = " NORMALES";
	}

	if($tipo_pago == 'SI')
    {
		$num_cuotas=$row['cuotas'];
		$interes = " SIN INTERESES";
	}

	if($tipo_pago == 'VD')
    {
		$num_cuotas=$row['cuotas'];
		$interes = " VENTA DEBITO";
	}

	$numero = $row['f_num_tarjeta'];
	$id_tabla = $row['id_operacion'];

	if($row['vendedor']== '')
    {
		$vendedor = '0';
	}
    else
    {
		$vendedor=$row['vendedor'];
	}

    $bp = "BP_".$orden_compra;
	$valor_unitario = $monto/$cantidad;
    $compra = $cantidad." TICKETS(S) " .$nombre_eo; 
    $total = number_format($valor_unitario). " PESOS CHILENOS PAGADO " .$pagado." ".$num_cuotas." CUOTAS".$interes.",<BR> CON SU TRAJETA DE CREDITO QUE FINALIZA EN LOS DIGITOS ".$numero; 
}
   

mysql_free_result($query); 




if ($res > 0)
{
  
    $miConexion = new ClaseConexion;
    $miConexion->conectar();
    $query=$miConexion->EjecutaSP("Generar_Voucher","@voucher"); 	
    $parametrosSalida_v=$miConexion->ObtenerResultadoSP("@voucher");
    $row_voucher= mysql_fetch_assoc($parametrosSalida_v);
    $voucher = $row_voucher['@voucher'];
    mysql_free_result($query); 
    
    
    
    if($id_session != 21) 
    ## compra realizada en www.turistik.cl
    {
        if($cod_prod != 20)
        {
            
            for ($i = 1; $i <= $cantidad; $i++) 
        	{
        	   
                $id_temporal = (date(mktime(date("i"),date("s"),date("d"))));
               
        		$miConexion = new ClaseConexion;		
        		$miConexion->conectar();
        		$query_ing=$miConexion->EjecutaSP1("Ingresar_Reserva_Excursiones_transbank","'".$cod_prod."','".$fecha_servicio."','".$vendedor."','".$direccion."','dir','0','0','0','".strtoupper($nombre_comprador)."','_','0','1','10687','1','".$valor_unitario."','compra_internet','".$voucher."','1','0','0','".$orden_compra."', '".$id_temporal."', '".$cod_auto."',@respuesta,@pmessage,@lastid");
        		$parametrosSalida3=$miConexion->ObtenerResultadoSP("@respuesta,@pmessage,@lastid");
        		$rowverificacion3= mysql_fetch_assoc($parametrosSalida3);
            
            }
        
        }
        else
        {
        
        	$miConexion->conectar();
        	$query=$miConexion->EjecutaSP("Ingresar_Reserva_Excursiones_v_transfer_internet","'".$cod_prod."','".$fecha_servicio."','".$vendedor."','".$direccion."','0','','','','".strtoupper($nombre_comprador)."','','','1','10687','".$cantidad."','".$monto."','compra_internet','".$voucher."','".$hora."','".$vuelo."', 'C','".$orden_compra."', 1, @respuesta,@pmessage");
        	$parametrosSalida2=$miConexion->ObtenerResultadoSP("@respuesta,@pmessage");
        	$rowverificacion3= mysql_fetch_assoc($parametrosSalida2);
        }
        
        mysql_free_result($parametrosSalida2); 
        mysql_free_result($parametrosSalida3); 
        mysql_free_result($query); 
        mysql_close();
    }
    
    else
    ## compra realizada en www.tkpass.cl
    {
        /*
        $consulta_promociones = new Consulta_Promociones();

        $bolTTS = $consulta_promociones->Mostar_Boleta($sucursal); //boleta tts correspondiente
        $bolTTK = $consulta_promociones->Mostar_BoletaTTK($sucursal);//boleta ttk correspondiente
        
        
        $Obj_Tkpass = new tkpass(); 
        
        $Obj_Tkpass->Ingresa_Boletas($tts,$ttk);
        
        $Obj_Tkpass->Actualiza_Estado_Boletas($tts, date('Y-m-d H:i:s'), 1, 'E');
        
        if ($tts != $bolTTS)
        {           
            for ($k=$bolTTS; $k<$tts; $k++)
            {
                $Obj_Tkpass->Actualiza_Estado_Boletas($k, date('Y-m-d H:i:s'), 2, 'E'); 
            }  
        }
        
        $Obj_Tkpass->Actualiza_Estado_Boletas($ttk, date('Y-m-d H:i:s'), 1, 'A'); 
        
        if ($ttk != $bolTTK)
        {           
            for ($h=$bolTTK; $h<$ttk; $h++)
            {
                $Obj_Tkpass->Actualiza_Estado_Boletas($h, date('Y-m-d H:i:s'), 2, 'A'); 
            } 
        }
        */
        
        $miConexion = new ClaseConexion;
        $miConexion->conectar();
        //USUARIO CALL CENTER
        //COD_USUA = 1666
        //CODIGO_DIST = 1426
        //orden_compra, cod_cierre, login_usua_cierre, voucher_re, sucursal_venta, cod_cierre_original, cod_usua, login_usua_reserva
    	$query=$miConexion->EjecutaSP("Actualiza_Reserva_Excursion_Internet"," '".$orden_compra."', '2', '".$vendedor."', ".$voucher.", '18', '2', '1666', '".$vendedor."', '".$cod_auto."' ");
        mysql_free_result($query); 

        
        ## Elimina registros temporales segun orden de compra
        
        
        $miConexion = new ClaseConexion;
        $miConexion->conectar();   
        $queryEliminaPasajero=$miConexion->EjecutaSP("elimina_temporales_by_orden_compra"," '".$orden_compra."' ");                                                 
        mysql_free_result($queryEliminaPasajero); 
        mysql_close();
        
    }

    if (in_array($codigo_eo , $excursiones_stk))
    {
        $CodeQR = $voucher;
    }
    else
    {
        $CodeQR = "<img src='https://chart.googleapis.com/chart?chs=110x110&cht=qr&chl=".$voucher." ' />";
    }


    include_once('boarding_formato_turistik_cl.php');
     
    
    $mpdf=new mPDF('c');
    $mpdf->SetDisplayMode('fullpage');
    $stylesheet = file_get_contents('mpdfstyleA4.css');
    $mpdf->WriteHTML($stylesheet,1);
    $stylesheet = file_get_contents('mpdfstylePaged.css');
    $mpdf->WriteHTML($html);
    //$mpdf->Output();
    
    $mpdf->Output("../../../turistik/PHP/BoardingPassByEmail/$bp.pdf",'F');
    
      
    $destinatari = explode(";",$destinatari);

    $cuerpo='Comprobante de Reserva.';
    $asunto="Turistik - Boarding Pass"; 

    //Crear una instancia de PHPMailer
    $mail = new PHPMailer();

    $mail->IsSMTP();
    $mail->SMTPDebug  = 0;
    $mail->Host       = 'smtp.gmail.com';
    $mail->Port       = 465;
    $mail->SMTPSecure = 'ssl';
    $mail->SMTPAuth   = true;
    $mail->Username   = "no-reply@turistiktours.cl";
    $mail->Password   = "tkcl.adm15";
    $mail->SetFrom("no-reply@turistiktours.cl", 'Turistik S.A');

    foreach($destinatari as $email)
    {
       $mail->AddAddress($email);
    }
    
    $mail->Subject = $asunto;
    $mail->MsgHTML($cuerpo);
    $mail->AddAttachment("../../../turistik/PHP/BoardingPassByEmail/$bp.pdf", "$bp.pdf");
    if (!$mail->Send()) {

        unlink("../../../turistik/PHP/BoardingPassByEmail/$bp.pdf");  //elimino de ftp el BP
        print("<script>alert('Hubo un problema al enviar su boarding pass a la direccion indicada, comuniquese con nosotros a la brevedad');</script>");
   
    }
    else
    {
        unlink("../../../turistik/PHP/BoardingPassByEmail/$bp.pdf");  //elimino de ftp el BP
        print("<script>alert('Gracias por su compra, hemos enviado un comprobante de reserva a la direccion de correo indicada');</script>");

    }

}





?>
