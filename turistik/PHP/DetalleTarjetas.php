<?php 
require ("../Clases/ClaseConexion.inc.php");
include( "../Clases/class.TemplatePower.inc.php"); 
require ("../Clases/ClaseUtil.inc.php");

$tpl = new TemplatePower( "../Plantillas/DetalleTarjetas.tpl" );

$_Util=new Util;
$miConexion= new ClaseConexion;
$tpl->prepare();
$tpl->assign("fecha", $_Util->Fecha());
$miConexion->Conectar();
$queryProducto=$miConexion->EjecutaSP("Consulta_Producto","'".$_GET['CODIGOPRODUCTO']."'");
$rowProducion= mysql_fetch_assoc($queryProducto);
$tpl->assign("_ROOT.CODIGOPROD",$rowProducion['NOMBRE_PROD']);
$tpl->assign("_ROOT.CODIGOPRODUCTOREAL",$_GET['CODIGOPRODUCTO']);

$miConexion->Conectar();
$queryDistribuidor=$miConexion->EjecutaSP("Consulta_Distribuidor_Codigo","'".$_GET['CODIGODISTRIBUIDOR']."'");
$rowDistribuidor= mysql_fetch_assoc($queryDistribuidor);
$tpl->assign("_ROOT.CODIGODIST",$rowDistribuidor['NOMBRE_FANTASIA_DIST']);
$tpl->assign("_ROOT.CODIGODISTRIBUIDORREAL",$_GET['CODIGODISTRIBUIDOR']);

$miConexion->Conectar();
$queryEstadoTarjeta=$miConexion->EjecutaSP("Consulta_Estado_Tarjeta","1,".$_GET['CODIGOESTADO']."");
$rowEstadoTarjeta= mysql_fetch_assoc($queryEstadoTarjeta);
$tpl->assign("_ROOT.CODIGOEST",$rowEstadoTarjeta['DESCRIPCION_EST']);
$tpl->assign("_ROOT.CODIGOESTADOREAL",$_GET['CODIGOESTADO']);

$miConexion->Conectar();
$queryDetalle=$miConexion->EjecutaSP("Consulta_Detalle","".$_GET['CODIGODISTRIBUIDOR'].",'".$_GET['CODIGOPRODUCTO']."',".$_GET['CODIGOESTADO']."");
while ($rowDetalle = mysql_fetch_assoc($queryDetalle))
	{
		$tpl->newBlock("detalle");
		$tpl->assign("FOLIO",$rowDetalle['FOLIO_TAR']);
		$FechaCreacion=$_Util->FormatearFecha($rowDetalle['FECHA_CREACION_TAR']);
		$tpl->assign("CREACION",$FechaCreacion);
		if ($rowDetalle['FECHA_ACTIVACION_TAR']=='')
			{
				$tpl->assign("ACTIVACION","");
			}
		else
			{
				$FechaActivacion=$_Util->FormatearFecha($rowDetalle['FECHA_ACTIVACION_TAR']);
				$tpl->assign("ACTIVACION",$FechaActivacion);
			}
		if ($rowDetalle['FECHA_EXPIRACION_TAR']=='')
			{
				$tpl->assign("EXPIRACION","");
			}
		else
			{
				$FechaExpiracion=$_Util->FormatearFecha($rowDetalle['FECHA_EXPIRACION_TAR']);
				$tpl->assign("EXPIRACION",$FechaExpiracion);
			}
		$miConexion->Conectar();
		$queryUsuario=$miConexion->EjecutaSP("Consulta_Usuario_Codigo","".$rowDetalle['CODIGO_USR_CREACION_TAR']."");
		$rowUsuario= mysql_fetch_assoc($queryUsuario);
		$tpl->assign("USUARIOCREACION",$rowUsuario['NOMBRE_USUA']." ".$rowUsuario['APELLIDO_USUA']);
		if ($rowDetalle['FECHA_RECEPCION_TAR']=='')
			{
				$tpl->assign("RECEPCION","");
			}
		else
			{
				$FechaRecepcion=$_Util->FormatearFecha($rowDetalle['FECHA_RECEPCION_TAR']);
				$tpl->assign("RECEPCION",$FechaRecepcion);
			}
		$miConexion->Conectar();
		if ($rowDetalle['CODIGO_USR_RECEPCION_TAR']!='')
			{
				$queryUsuarioReceptor=$miConexion->EjecutaSP("Consulta_Usuario_Codigo","".$rowDetalle['CODIGO_USR_RECEPCION_TAR']."");
				$rowUsuarioReceptor= mysql_fetch_assoc($queryUsuarioReceptor);
				$tpl->assign("USUARIORECEPTOR",$rowUsuarioReceptor['NOMBRE_USUA']." ".$rowUsuarioReceptor['APELLIDO_USUA']);
			}
		else
			{
				$tpl->assign("USUARIORECEPTOR","");
			}
		$miConexion->Conectar();
		if ($rowDetalle['CODIGO_USR_ACTIVACION_TAR']!='')
			{
				$queryUsuarioActivador=$miConexion->EjecutaSP("Consulta_Usuario_Codigo","".$rowDetalle['CODIGO_USR_ACTIVACION_TAR']."");
				$rowUsuarioActivador= mysql_fetch_assoc($queryUsuarioActivador);
				$tpl->assign("USUARIOACTIVADOR",$rowUsuarioActivador['NOMBRE_USUA']." ".$rowUsuarioActivador['APELLIDO_USUA']);
			}
		else
			{
				$tpl->assign("USUARIOACTIVADOR","");
			}
		$tpl->assign("PASAJERO",$rowDetalle['PASAJERO_TAR']);
	}

//	{
//		$tpl->newBlock("masterpines");
//       	$tpl->assign("CODIGO",$rowMasterPines['CODIGO_MP']);
//		$tpl->assign("CARACTERES",$rowMasterPines['CARACTERES_MP']);
//		$tpl->assign("MES",$rowMasterPines['MES_MP']);
//		$tpl->assign("ANNO",$rowMasterPines['ANO_MP']);
//	}		

$tpl->printToScreen();
?>