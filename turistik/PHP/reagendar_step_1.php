<?php
session_start();
if ($_SESSION["autentificado"] != "SI"){
      header("location: Login.php");
}

require ("../functions/tkpass.php");
date_default_timezone_set("America/Santiago");
require ("../Clases/ClaseConexion.inc.php");
include( "../Clases/class.TemplatePower.inc.php"); 
require ("../Clases/ClaseUtil.inc.php");

$hora_now = date("H:i:s");
$codigo_re = $_GET['codigo'];
$fecha_hoy = date("Y-m-d"); 
$_Util=new Util;

$miConexion = new ClaseConexion;
$miConexion->Conectar();
$queryRegistrosReserva=$miConexion->EjecutaSP("Consulta_ReservaEx_By_Id_Reag","  ".$codigo_re." ");

   while ($rowL = mysql_fetch_assoc($queryRegistrosReserva))
   {
         $tipo_re = $rowL['TIPO_RE'];
         $voucher = $rowL['VOUCHER_RE'];
         $valor_servicio_ant = $rowL['PRECIO_EO'];
         $hora_venta = $rowL['HORA_VENTA'];
         $fecha_servicio =  $rowL['FECHA_RE']; 
         $codigo_eo = $rowL['CODIGO_EO']; 
         $suc_venta = $rowL['SUCURSAL_VENTA']; 
         $impreso = $rowL['IMPRESO'];
   }

   if($tipo_re == 4)
   {
      print('<script>alert("No es posible reagendar una reserva Mayorista.");</script>');  
      echo '<script>window.close();</script>';
   }
   
   $excursiones_stk = array(6,54,55,63,64);
    if (in_array($codigo_eo, $excursiones_stk)) 
    {

        if($impreso == 1)
        {
            print('<script>alert("No es posible reagendar pasajero STK\n Brazalete impreso!.");</script>');  
            echo '<script>window.close();</script>';
        }
    }
   
   

if ($_SESSION["cod_tusua"] == 3 || $_SESSION["cod_tusua"] == 4)
{
    if ($fecha_servicio == $fecha_hoy)
    {
        if ($hora_venta < $_Util->sumar($hora_now,"04:00:00") )
        {
            if ($_SESSION['passed'] != 'ok')
            {
                echo "no";
                header('Location:autorizacion.php?codigo='.$codigo_re.'');
            }
               
        }
    }
}


$_SESSION['voucher_a_reagendar'] = $voucher; 
$usuario=$_SESSION['usuario'];

$fecha = date('d/m/Y');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<title> </title>

 <meta charset="utf-8" />
   <link rel="stylesheet" href="../../css/plantilla_matriz.css" type="text/css" media="all">
    <link href="../estilos1.css" rel="stylesheet" type="text/css">
    <link type="text/css" href="../../css/ui.all.css" rel="stylesheet" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
    <script type="text/javascript" src="../../js/ui.datepicker.js"></script>
    
  
    <script type="text/javascript">
    

    function Tomar(dia, fecha, exc, acc)
    {
        if (acc == 1)
        {

            var url = 'reagendar_step_2.php?df='+dia+'&f='+fecha+'&ex='+exc+'&n=00';
        }
        
        if (acc == 2)
        {
            alert("No Es Posible Reagendar Esta Excursion");
            return false;
        }
        
        window.open(url,'_blank');
    }
    

    </script>
    
</head>
<body>
 <div id="global">
 

 <div id="cabecera">

      <table width="482" border="0" cellpadding="0" cellspacing="0">  
    	<tr>
          <td width="80" rowspan="4"><img src="../Imagenes/IMG_6902.jpg" width="80" height="80" /></td>
          <td width="13">&nbsp;</td>
          <td width="389">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><strong id="TituloPlomo"><b>Reagendamiento de Excursiones </b></strong></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><span class="TituloFecha"><?php echo htmlentities($_Util->Fecha());?></span></td>
        </tr>    
    
      </table>

 </div>

 <div id="navegacion" style="padding-top: 30px;">
   
                <form name="form1">
                    <table width="357" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="100" class="celdalazul_peque">
                         Fecha
                        </td>
                        <td width="100" class="celdalCELEST">
                         <input type="text" class="seleccionTurist_180" name="fecha" id="fecha" value="<?=$fecha?>" readonly="">
                        </td>
                        <td width="100" class="celdalCELEST">
                          <input name="consulta" type="button" class="BotonTurist_Azul"  value="Consultar" onclick="CambiaFecha()" />
                        
                        </td>
                      </tr>
                    
                    </table>
                </form>

 </div><!--cierre navegacion -->
 
 
 
 <div id="Layer4" style="position:absolute; left:835px; top:20px; width:128px; height:80px; z-index:4">
    <img src="../Imagenes/turistik107_118.jpg" width="107" height="118" />
 </div>
 
 

  <div id="principal" style="width: 900px; margin-top: 70px;">
    <?

            include ("matriz_excursiones_reag.php");

    ?>
  </div>
 
 
 
 <div class="anuncios1">

   

 </div> <!--cierre anuncios -->
 
 
 
 
 <div id="pie">
    <!--Pie-->
 </div>
 
 </div>
</body>
<script>
    $( document ).ready(function() {
        $("#fecha").datepicker(); 
        
        
    });
    
    function CambiaFecha()
    {
        
        var fullDate = new Date()
        var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
        var fecha_hoy = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();


        if (cod_tusua == 3)
        {
            
            if((Date.parse(fecha_hoy)) > (Date.parse($("#fecha").val()))){
                alert("No es posible realizar esta acción");
            }
            else
            {
                $('#principal').load('matriz_excursiones_reag.php?f='+$("#fecha").val());
            }
            
        }
        else
        {
            $('#principal').load('matriz_excursiones_reag.php?f='+$("#fecha").val());

        }

    }
    
    var cod_tusua = <?=$_SESSION['cod_tusua'];?>     
</script>
</html>