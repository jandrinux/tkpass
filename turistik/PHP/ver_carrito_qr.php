<?
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
session_start();
if ($_SESSION["autentificado"] != "SI"){
    header("location: Login.php");
}

require ("set_timeout.php");
include ("../Clases/ClaseConexion.inc.php");
include ("../functions/consulta_promociones.php");
require ("../Clases/ClaseUtil.inc.php");

$consulta_promociones = new Consulta_Promociones();
$bol = $consulta_promociones->Mostar_Boleta($_SESSION['codigo_suc'], "E"); //tts
$bolTTK = $consulta_promociones->Mostar_Boleta($_SESSION['codigo_suc'], "A"); //ttk

?>

<html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
.Estilo1 {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
    font-size: 18px;
}

 </style>
 
<head>
<title>.: TURISTIK - Carrito de compras :.</title>
<!--<script language="JavaScript" type="text/javascript" src="../../js/jquery-1.3.2.js"></script>-->
<link type='text/css' href='../../css/basic_simplemodal.css' rel='stylesheet' media='screen' /><!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
<link href="../../css/menu_small.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="../../js/ajax.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>


<script type='text/javascript' src='../../js/jquery.simplemodal.js'></script>
<script type='text/javascript' src='../../js/basic_simplemodal.js'></script>


<link href="../estilos1.css" rel="stylesheet" type="text/css">
<? include ('js_ver_carrito.php'); ?>


</head>

<body>

<div id='basic-modal'>
	<input type='button' name='wpc' id="wpc" class='basic' style="display: none;"/>
</div>

<div id="Layer2"><img src="../Imagenes/turistik107_118.jpg" width="107" height="118" /></div>

<div class="TituloPlomo" id="Layer3">
<img src= '../Imagenes/carrito.png' width="80" style="margin-left: 30px;"/>
<span class="Estilo1">Carrito de Compras</span>
</div>

<div id="Layer1">

<div id="menuh">
        <ul>
           <li><a href="#" name="radio">NINGUNO</a></li>
           <li><a href="#" name="radio">COMBOS</a></li>
           <li><a href="#" name="radio">DESCUENTOS</a></li>
           <li><a href="#" name="radio">BENEFICIOS</a></li>
           <li><a href="#" name="radio">PROMOCIONES</a></li>
           <li><a href="#" name="radio">OFERTAS</a></li>
           <li><a href="#" name="radio" id='primero' >VENTA QR</a></li>
        </ul>
</div>

<form name="formCompra" style="float: left;"  action="finaliza_compra.php"   method="post" enctype="multipart/form-data">
<div id="resultado_listar"  style="margin:20px 0px 20px 0px; float:left;">

    <? include('CONSULTA_carrito_qr.php'); ?>

</div>


<fieldset id="fieldset">
<legend class="celdalLeyend">Forma de Pago</legend>
 <table align="center" id="tabla" width="770" border="0" cellpadding="0" cellspacing="0">
 <tr>
    <td class="celdalazul_peque">Tipo Reserva: (*)</td>
    <td class="celdalCELEST">
        <select name="tipoReserva" required="" title="Seleccione tipo de reserva" id="tipoReserva" class="seleccionTurist_180" >
            <option value="1" selected >Cuenta Corriente</option>
        </select>
    </td>
    <td class="celdalazul_peque">Forma de Pago: (*)</td>
    <td class="celdalCELEST">
        <select name="fop" required="" title="Seleccione forma de pago" id="select_fop" class="seleccionTurist_180" onchange="info_boleta_tr();">
            <option value="S" selected="">-- SELECCIONE --</option>
            <option value="C">Tarjeta de Credito</option>
            <option value="D">Tarjeta de Debito</option>
            <option value="E">Efectivo</option>
        </select>
    </td>
    <td></td>
    <td></td>
 </tr>
  <tr >
    <td class="celdalazul_peque">
    </td>
    <td class="celdalCELEST">
    </td>
    
    <td class="celdalazul_peque"><div id="bolTTS">Boleta TTS: (*)</div> </td>
    <td class="celdalCELEST"><input name="boletaTTS" id="boletaTTS"  onkeypress="return validarP3(event);" class="seleccionTurist_180" type="text" value="<?=$bol?>" /></td>
    <td></td>
    <td></td>

 </tr>
 <tr >
    <td class="celdalazul_peque"></td>
    <td class="celdalCELEST" >
    </td>

    <td class="celdalazul_peque"><div id="bolTD">Boleta Turistik: (*)</div> </td>
    <td  class="celdalCELEST"><input name="boletaTTK" id="boletaTTK" required="" onkeypress="return validarP3(event);" class="seleccionTurist_180" type="text" value="<?=$bolTTK?>"/></td>
    <td></td>
    <td></td>

 </tr>
 <tr>
    <td class="celdalazul_peque"><div id="tituloTBK" style="visibility: hidden;" >C&oacute;digo Autorizaci&oacute;n: (*)</div></td>
    <td class="celdalCELEST" >
        <input name="voucherTBK" id="info_boleta" style="visibility: hidden;"  class="seleccionTurist_180" type="text"/>
    </td>
    <td class="celdalazul_peque">
    </td>	
    <td class="celdalCELEST">
    </td>     
    <td></td>
    <td></td>
 </tr>
  <tr>
    <td class="celdalazul_peque"><div id="titulo4Ultimos">4 Ultimos Digitos Tarjeta: (*)</div></td>
    <td class="celdalCELEST" >
    <input name="ultimos_digitos" id="ultimos_digitos"  onkeypress="return validarP3(event);"  class="seleccionTurist_180" type="text" maxlength="4"/>
    </td>
    <td class="celdalazul_peque"><div id="tituloTipoTarjeta">Tipo de Tarjeta: (*)</div></td>	
    <td class="celdalCELEST">
        <select name="tipo_tarjeta" required="" title="Seleccione Tarjeta" id="tipo_tarjeta" class="seleccionTurist_180" >
            <option value="" selected>-- SELECCIONE --</option>
            <option value="MASTERCARD" >MASTERCARD</option>
            <option value="VISA" >VISA</option>
            <option value="AMERICAN EXPRESS" >AMERICAN EXPRESS</option>
            <option value="OTRO" >OTRO</option>
        </select>
    </td>     
    <td></td>
    <td></td>
 </tr>

</table>

</fieldset>

    <div style="margin-left: 400px; margin-top: 15px;">

    <input name="tipoUsuario" id="tipoUsuario" type="hidden" value="<? echo $_SESSION["codigo_suc"]; ?>" />
    <input  type="button"  class="BotonTurist_largo" value="Vaciar carrito" onclick="vaciar_carrito()"/>
    <input type="button" name="continuar" id="continuar" class="BotonTurist_largo" onclick="carrito();" value="Seguir comprando" />
    <input type="button" name="comprar" class="BotonTurist_largo" onclick="finalizar()" value="Finalizar compra"/>
    <input type="hidden" name="radio" id="radio" value="7" />
    <input type="hidden" name="estado" id="estado"/>
    <input type="hidden" name="estado_ttk" id="estado_ttk"/>
    <input name="boletaTTSCorrelativa" id="boletaTTSCorrelativa"  type="hidden" value="<?=$bol?>" />
    <input name="boletaTTKCorrelativa" id="boletaTTKCorrelativa"  type="hidden" value="<?=$bolTTK?>" />
    <input type="hidden" id="tipo_venta" name="tipo_venta" /> 
    <input type="hidden" id="tipo_mayorista" name="tipo_mayorista" value="0" />  


    </div>
  
</form>


<script type="text/javascript">

var ef = $('#tot_efectivo').val();
var ta = $('#tot_tarjeta').val();
var ex = $('#tot_exento').val();
var af = $('#tot_afecto').val();
var radio = document.getElementById("radio").value;
var cod_tusua = <?=$_SESSION['cod_tusua']?> ;

</script>
</div> <!--end Layer1-->
</body>
</html>