<?PHP
class COLA_IMPRESION_CABECERA_BOLETA {
	var $ID_C_B;
	var $NUMERO_DOCUMENTO;
	var $FECHA_DOCUMENTO;
	var $DESCUENTO;
	var $CODIGO_SUCURSAL;
	var $NULA;
	var $OBSERVACIONES;
	var $FORMA_PAGO;
	var $TIPO_BOLETA;
	var $PROCESADO;
	var $conectas;

	function __construct() {
		$this->conectas = new ClaseConexion();
		$this->conectas->Conectar(); 
	}
	public function Lista() {
		$sql = "select * from COLA_IMPRESION_CABECERA_BOLETA";
		$res = $this->conectas->EjecutaConsulta($sql);
		while($row=mysql_fetch_array($res)) {
			$c = new COLA_IMPRESION_CABECERA_BOLETA();
			$c->ID_C_B= $row["ID_C_B"];
			$c->NUMERO_DOCUMENTO= $row["NUMERO_DOCUMENTO"];
			$c->FECHA_DOCUMENTO= $row["FECHA_DOCUMENTO"];
			$c->DESCUENTO= $row["DESCUENTO"];
			$c->CODIGO_SUCURSAL= $row["CODIGO_SUCURSAL"];
			$c->NULA= $row["NULA"];
			$c->OBSERVACIONES= $row["OBSERVACIONES"];
			$c->FORMA_PAGO= $row["FORMA_PAGO"];
			$c->TIPO_BOLETA= $row["TIPO_BOLETA"];
			$c->PROCESADO= $row["PROCESADO"];
			$c->conectas=null;
			$array[] = $c;
		}
		return $array;
	}

	public function ListaJSON() {
		$data = $this->Lista();
		return json_encode($data);
	}

	public function ListarID_C_B($ID_C_BId) {
		$this->ID_C_B = $ID_C_BId;
		$sql = "select * from COLA_IMPRESION_CABECERA_BOLETA where ID_C_B=$ID_C_BId";
		$res = $this->conectas->EjecutaConsulta($sql);
		while($row=mysql_fetch_array($res)) {
			$c = new COLA_IMPRESION_CABECERA_BOLETA();
			$this->ID_C_B= $row["ID_C_B"];
			$c->ID_C_B= $row["ID_C_B"];
			$this->NUMERO_DOCUMENTO= $row["NUMERO_DOCUMENTO"];
			$c->NUMERO_DOCUMENTO= $row["NUMERO_DOCUMENTO"];
			$this->FECHA_DOCUMENTO= $row["FECHA_DOCUMENTO"];
			$c->FECHA_DOCUMENTO= $row["FECHA_DOCUMENTO"];
			$this->DESCUENTO= $row["DESCUENTO"];
			$c->DESCUENTO= $row["DESCUENTO"];
			$this->CODIGO_SUCURSAL= $row["CODIGO_SUCURSAL"];
			$c->CODIGO_SUCURSAL= $row["CODIGO_SUCURSAL"];
			$this->NULA= $row["NULA"];
			$c->NULA= $row["NULA"];
			$this->OBSERVACIONES= $row["OBSERVACIONES"];
			$c->OBSERVACIONES= $row["OBSERVACIONES"];
			$this->FORMA_PAGO= $row["FORMA_PAGO"];
			$c->FORMA_PAGO= $row["FORMA_PAGO"];
			$this->TIPO_BOLETA= $row["TIPO_BOLETA"];
			$c->TIPO_BOLETA= $row["TIPO_BOLETA"];
			$this->PROCESADO= $row["PROCESADO"];
			$c->PROCESADO= $row["PROCESADO"];
			$c->conectas=null;
			$array[] = $c;
		}
		return $array;
	}

	public function ListarID_C_BJSON($ID_C_BId) {
		$data = $this->ListarID_C_B($ID_C_BId);
		return json_encode($data);
	}
	public function Where($vars) {
		$sql = "select * from COLA_IMPRESION_CABECERA_BOLETA where $vars";
		$res = $this->conectas->EjecutaConsulta($sql);
		while($row=mysql_fetch_array($res)) {
			$c = new COLA_IMPRESION_CABECERA_BOLETA();
			$c->ID_C_B= $row["ID_C_B"];
			$c->NUMERO_DOCUMENTO= $row["NUMERO_DOCUMENTO"];
			$c->FECHA_DOCUMENTO= $row["FECHA_DOCUMENTO"];
			$c->DESCUENTO= $row["DESCUENTO"];
			$c->CODIGO_SUCURSAL= $row["CODIGO_SUCURSAL"];
			$c->NULA= $row["NULA"];
			$c->OBSERVACIONES= $row["OBSERVACIONES"];
			$c->FORMA_PAGO= $row["FORMA_PAGO"];
			$c->TIPO_BOLETA= $row["TIPO_BOLETA"];
			$c->PROCESADO= $row["PROCESADO"];
			$c->conectas=null;
			$array[] = $c;
		}
		return $array;
	}

	public function WhereJSON($vars) {
		$data = $this->Where($vars);
		return json_encode($data);
	}

	public function Add() {
		$sql = "INSERT INTO COLA_IMPRESION_CABECERA_BOLETA (NUMERO_DOCUMENTO,FECHA_DOCUMENTO,DESCUENTO,CODIGO_SUCURSAL,NULA,OBSERVACIONES,FORMA_PAGO,TIPO_BOLETA,PROCESADO)"; 
		$sql.= "VALUES('".$this->NUMERO_DOCUMENTO."','".$this->FECHA_DOCUMENTO."','".$this->DESCUENTO."','".$this->CODIGO_SUCURSAL."','".$this->NULA."','".$this->OBSERVACIONES."','".$this->FORMA_PAGO."','".$this->TIPO_BOLETA."','".$this->PROCESADO."')";
		$res = $this->conectas->EjecutaConsulta($sql);
		return "OK";
	}

	public function Update() {
		$sql = "Update  COLA_IMPRESION_CABECERA_BOLETA set NUMERO_DOCUMENTO='".$this->NUMERO_DOCUMENTO."',FECHA_DOCUMENTO='".$this->FECHA_DOCUMENTO."',DESCUENTO='".$this->DESCUENTO."',CODIGO_SUCURSAL='".$this->CODIGO_SUCURSAL."',NULA='".$this->NULA."',OBSERVACIONES='".$this->OBSERVACIONES."',FORMA_PAGO='".$this->FORMA_PAGO."',TIPO_BOLETA='".$this->TIPO_BOLETA."',PROCESADO='".$this->PROCESADO."' where ID_C_B='".$this->ID_C_B."' "; 
		$res = $this->conectas->EjecutaConsulta($sql);
		return "OK";
	}

	public function delete() {
		$sql= "delete from  COLA_IMPRESION_CABECERA_BOLETA where ID_C_B='".$this->ID_C_B."' ";
		$res = $this->conectas->EjecutaConsulta($sql);
		return "OK";
	}
}
?>
