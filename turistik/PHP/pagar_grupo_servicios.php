<?php
ini_set('display_errors', 1);
session_start();
date_default_timezone_set("America/Santiago");
require ("../Clases/ClaseConexion.inc.php");
require ("../functions/tkpass.php");
include ("../Clases/class.bitacora.php");
require ("../Clases/class.boletas_manager.php");
require ("SII/class.boletaGen.php");
require ("../Clases/class.mail.php");

$Obj_Tkpass = new tkpass();  
$miConexion= new ClaseConexion;
 
$fop = $_POST['fop'];
$fop_manager = $fop;
$observacion = $_POST['observacion'];
$var = $_POST['var'];
$tbk = $_POST['tbk'];
$boarding  = $_POST['boarding'];
$tipo_tarjeta = $_POST['tipo_tarjeta'];
$ultimos_digitos = $_POST['ultimos_digitos'];
$diferencia = $_POST['diferencia'];
$cantidad_adt = $_POST['cantidad_adt'];
$valor_adt = $_POST['valor_adt'];
$cant_checkeados = $_POST['cantidad'];
$precio_real = 0;
$tipo_venta = 1;
$flag = false; # no esta pagado
$tipo_re = $_POST['tipo_reserva'];
$excursion_array = array();
$array = array();


if ($_SESSION['codigo_suc'] == 7 || $_SESSION['codigo_suc'] == 15)
{
    $total_adt = $valor_adt * $cantidad_adt;
    $dscto = $valor_adt * 0.05;
    $valor_actualizar = round($valor_adt - $dscto,0, PHP_ROUND_HALF_DOWN);
}
else
{
    $resultado = ($diferencia / $cantidad_adt);
    $valor_actualizar = (integer)($valor_adt - $resultado);
}


if ( $var == 1) 
{

    $accion = 3;

    for ($i=0; $i<count($_POST['check']); $i++) {

      
        $miConexion->conectar();   
      	$query=$miConexion->EjecutaSP("Modificar_Estado_Cierre_","'".$_POST['check'][$i]."','".$accion."','".$_SESSION["usuario"]."', '".$observacion."', @cod,@pmessage"); 	
        
        $b = new Bitacora();
        $b->FECHA = date("Y-m-d  h:i:s");
        $b->NUEVO = true;
        $b->USUARIO = $_SESSION['usuario'];
        $b->EVENTO ="RESERVA ANULADA";
        $b->DESCRIPCION =$observacion;
        $b->ARCHIVO ="pagar_grupo_servicios.php";
        $b->CODIGO_RE=$_POST['check'][$i];
        $b->add();
        
        include('envio_mail_anul_noshow.php');
        
             
    }
    
}
else
{

    $cant_pax_adt = 0;
    $cant_pax_chd = 0;
    $precio_adt = 0;
    $precio_chd = 0;
    $precio_re = 0;


    for ($i=0; $i<count($_POST['check']); $i++) {

        
        $miConexion->conectar();   
        $query=$miConexion->EjecutaSP("Consulta_ReservaEx_By_IdTemporal"," 2, '".$_POST['check'][$i]."' ");  
        while($row = mysql_fetch_array($query))
        {
            $codigo_eo = $row['CODIGO_EO'];

            if ($tipo_re == 3) {

                if ($row['TIPO_PASAJERO'] == "ADT") {

                    $cant_pax_adt++;
                    $precio_adt = $row['PRECIO_COMISIONADO_RE'] + $precio_adt;
                }
                else
                {
                    $precio_chd = $row['PRECIO_COMISIONADO_RE'] + $precio_chd;
                    $cant_pax_chd++;
                }
            }
            else
            {
                if ($row['TIPO_PASAJERO'] == "ADT") {

                    $cant_pax_adt++;
                    $precio_re = $row['PRECIO_RE'] + $precio_re;
                }
                else
                {
                    $precio_re = $row['PRECIO_RE'] + $precio_re;
                    $cant_pax_chd++;
                }
            }

        }

    }

    $total_excursion[] = $precio_re;
    $cantidadPax[] = $cant_pax_adt + $cant_pax_chd;
    $excursion_array[] = $codigo_eo;
    $pasajeros_cant[$codigo_eo] = array("adt"=>$cant_pax_adt,
                                        "chd"=>$cant_pax_chd);


    for ($i=0; $i<count($_POST['check']); $i++) {

        $miConexion->conectar();
        $miConexion->EjecutaSP("Actualiza_Precio_Re"," 1, '".$_POST['check'][$i]."', ".$valor_actualizar." ");                   
    
        if ($fop == 'C')
        {
            $miConexion->conectar();   
            $q = "update RESERVA_EXCURSION SET ULTIMOS_DIG_TARJETA = '".$ultimos_digitos."', TIPO_TARJETA = '".$tipo_tarjeta."' WHERE CODIGO_RE='".$_POST['check'][$i]."'";
            $miConexion->EjecutaConsulta($q);
        } 
    }


    $Manager = new Boletas_Manager($precio_real, $array);
    $descuento = 0;

    if ($tipo_re == 3) 
    {
        include_once("sii_ra.php");
    }
    else
    {
        include_once("sii.php");
    }


    //// INGRESAR BOLETAS MANAGER
    try {   
        
        //Boleta afecta TTK
        $tipo = 'E';
        $Manager->Ingresar_Cabecera($boleta_exenta,$descuento,$fop_manager,$tipo,$excursion_array,$cantidadPax, $pasajeros_cant);
        
        // Solo agregar boleta TTS
        
        for ($i=0; $i<count($excursion_array); $i++)
        {
            if ($excursion_array[$i] == 6)
            {
                unset($excursion_array[$i]);
                unset($cantidadPax[$i]);
    
            }
        }
        
        sort($excursion_array);
        sort($cantidadPax);
        
        
        //Boleta afecta TTK y TTS
        $tipo = 'A';
        if ($tipo_venta == 1 || $tipo_venta == 2)
        {
            $Manager->Ingresar_Cabecera($boleta_afecta,0,$fop_manager,$tipo,$excursion_array,$cantidadPax, $pasajeros_cant);
            
        }
        
    
   }catch (Exception $e) {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        
   }

    $url_exento = base64_encode($url_exento);
    $url_afecto = base64_encode($url_afecto);

    

    for ($i=0; $i<count($_POST['check']); $i++) {
        
        $miConexion->conectar();    
        $miConexion->EjecutaSP("Actualiza_Cod_cierre"," 1, '".$_POST['check'][$i]."', 2, '".$boleta_exenta."', '".$boleta_afecta."' , '".$_SESSION["usuario"]."', '".$fop."', '".$tbk."', ".$_SESSION['codigo_suc']." ");
        
        $b = new Bitacora();
        $b->FECHA = date("Y-m-d  h:i:s");
        $b->NUEVO = true;
        $b->USUARIO = $_SESSION['usuario'];
        $b->EVENTO ="RESERVA PAGADA";
        $b->DESCRIPCION ="Autorizacion TBK: ".$tbk;
        $b->ARCHIVO ="pagar_grupo_servicios.php";
        $b->CODIGO_RE=$_POST['check'][$i];
        $b->add();  

    }

    

    if ($cant_checkeados == count($_POST['check'])) {
        $flag = true;
    }
}  

if($flag == true)
{
    $link = "boarding_pp.php?v=$boarding&be=$url_exento&ba=$url_afecto";
    header("Location:".$link);  

}
else
{

    if ($accion == 3)
    {
        print("<script>alert('Reserva Anulada Correctamente');</script>"); 
        print("<script>window.parent.close();</script>");
    }
    else
    {
        $link = "imprimir_doc.php?ue=$url_exento&ua=$url_afecto";
        header("Location:".$link);   
    }
}
?>