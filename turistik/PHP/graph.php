<div style="width: 730px; margin: 20px auto; font-family:sans-serif;">
<?php
/** Include class */
include( '../../lib/googchart/GoogChart.class.php' );
//require ("../Clases/ClaseConexion.inc.php");
/** Create chart */
$chart = new GoogChart();
$j=0;

if ($_POST['sucursal'] == 0){
    
    $miConexion = new ClaseConexion;
    $miConexion->Conectar();
    $querySucursal=$miConexion->EjecutaSP("Consulta_Listar_Sucursales","'"."0"."'");//sucursalES
    $total_sucursales = mysql_num_rows($querySucursal);	
    	
        while($rowSucursal = mysql_fetch_array($querySucursal)){ 
        $nombre_sucursal[$j] = $rowSucursal['SUCURSAL'];
        $j++;                          
        }  
    mysql_free_result($querySucursal); 
    mysql_close();
}else{
    $num_suc = $_POST['sucursal'] - 1;
    $miConexion = new ClaseConexion;
    $miConexion->Conectar();
    $querySucursal=$miConexion->EjecutaSP("Consulta_Listar_Sucursales"," ".$_POST['sucursal']." ");//sucursalES
    $total_sucursales = mysql_num_rows($querySucursal);		
        while($rowSucursal = mysql_fetch_array($querySucursal)){
            $cod_suc = $rowSucursal['CODIGO_SUCURSAL'];
            $nombre_sucursal = array ($rowSucursal['SUCURSAL'], 'RESTO SUCURSALES');                          
        }  
    mysql_free_result($querySucursal); 
    mysql_close();
 
}

// Set graph colors

$color = array(
			'#E0ED59',
			'#257A5C',
			'#999999',
            '#837FDF',
            '#F09F43',
            '#E60D1C',
            '#1EEAEE',
            '#FF3788',
            '#1EfA33',
            '#2574AC',
            '#3E3781'
);

if ($j == 0){ // una sucursal

  for ($i=0; $i< 2; $i++){
    if ($i == 0){
        $porcentaje = ($total_array[$num_suc] * 100 ) / $Suma_total_venta ; 
        $variables2[$nombre_sucursal[$i]." $".number_format($total_array[$num_suc])." ".round($porcentaje)."%"] =  round($porcentaje);
    }
    if ($i == 1){
        $resta = $Suma_total_venta - $total_array[$num_suc];
        $porcentaje = ($resta * 100 ) / $Suma_total_venta ;
        //$diferencia = $Suma_total_venta - $total_array[$cod_suc - 1];
        $variables2[$nombre_sucursal[$i]." $".number_format($resta)." ( ".round($porcentaje)."% )" ] =  round($porcentaje);
    }
  } 
   
}else{ // todas

  for ($i=0; $i<= $j; $i++){ 
    if ($total_array[$i] == 0){
       $porcentaje = 0;
    }else{
       $porcentaje = ($total_array[$i] * 100 ) / $Suma_total_venta ;   
    }
    $variables2[$nombre_sucursal[$i]." $".number_format($total_array[$i])." ( ".round($porcentaje)."% )"] =  round($porcentaje) ;
  }
}
         
       

/* # Imprime el grafico de torta # */

$chart->setChartAttrs( array(
	'type' => 'pie',
	'title' => 'Valores ventas v/s Sucursales',
	'data' => $variables2,
	'size' => array( 700, 300 ),
	'color' => $color
	));
// Print chart

echo $chart;
///-----------------------------------------------------------------------------------------
$total_rango1 = $rango_1_venta_total;
$total_rango2 = $rango_1_venta_total + $rango_2_venta_total;
$total_rango3 = $rango_1_venta_total + $rango_2_venta_total + $rango_2_venta_total;
$total_rango4 = $rango_1_venta_total + $rango_2_venta_total + $rango_3_venta_total + $rango_4_venta_total;
$total_rango5 = $rango_1_venta_total + $rango_2_venta_total + $rango_3_venta_total + $rango_4_venta_total + $rango_5_venta_total;

$totales_ventas_horas = array(
                            $total_rango1,
                            $total_rango2,
                            $total_rango3,
                            $total_rango4,
                            $total_rango5
                            );
$mayor = 0;
if ($_POST['sucursal'] == 0){
    for ($k=0; $k< count($totales_ventas_horas); $k++){
        for ($i=0; $i< $j; $i++){       
            $variables[$nombre_sucursal[$i]] =  array(  
                '08:00-12:00' => round($rangoCant_1[$i]),
                '12:01-14:00' => round($rangoCant_2[$i]),
                '14:01-16:00' => round($rangoCant_3[$i]),
                '16:00-18:00' => round($rangoCant_4[$i]),
                '18:00-20:00' => round($rangoCant_5[$i]),
				'' => 0
                
        		
            );
        }
    }
            for ($i=0; $i< count($nombre_sucursal); $i++){  
                if($rangoCant_1[$i] > $mayor){
                    $mayor = $rangoCant_1[$i];
                }
                if($rangoCant_2[$i] > $mayor){
                    $mayor = $rangoCant_2[$i];
                }
                if($rangoCant_3[$i] > $mayor){
                    $mayor = $rangoCant_3[$i];
                }  
                if($rangoCant_4[$i] > $mayor){
                    $mayor = $rangoCant_4[$i];
                }       
                if($rangoCant_5[$i] > $mayor){
                    $mayor = $rangoCant_5[$i];
                } 
    }
}else{
       
            $variables[$nombre_sucursal[0]] =  array(  
                '08:00-12:00' => round($rangoCant_1[$num_suc]),
                '12:01-14:00' => round($rangoCant_2[$num_suc]),
                '14:01-16:00' => round($rangoCant_3[$num_suc]),
                '16:00-18:00' => round($rangoCant_4[$num_suc]),
                '18:00-20:00' => round($rangoCant_5[$num_suc]),
				'' => 0
                
            );
            
            for ($k=0; $k< count($totales_ventas_horas); $k++){ 
                
                if($rangoCant_1[$num_suc] > $mayor){
                    $mayor = $rangoCant_1[$num_suc];
                }
                if($rangoCant_2[$num_suc] > $mayor){
                    $mayor = $rangoCant_2[$num_suc];
                }
                if($rangoCant_3[$num_suc] > $mayor){
                    $mayor = $rangoCant_3[$num_suc];
                }  
                if($rangoCant_4[$num_suc] > $mayor){
                    $mayor = $rangoCant_4[$num_suc];
                }       
                if($rangoCant_5[$num_suc] > $mayor){
                    $mayor = $rangoCant_5[$num_suc];
                } 
   }
 
    
}

$dataTimeline = $variables;

   
    //$mayor = round(($mayor * 100) / $Suma_total_venta);

/* # Imprime grafico de linea# */
$chart->lTamano(round($mayor));
$chart->setChartAttrs( array(
	'type' => 'line',
	'title' => 'Valores de Ventas v/s Horas',
	'data' => $dataTimeline,
	'size' => array( 750, 350 ),
	'color' => $color,
	'labelsXY' => true
     //'fill' => array( '#eeeeee', '#aaaaaa' ),
	));
// Print chart
echo $chart;
?>
</div>