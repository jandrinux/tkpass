// JavaScript Document
function pres_num()
{

	if(event.keyCode!=44)
	{
		if((event.keyCode<48) || (event.keyCode>57))
		{
				event.returnValue = false;
		}	
		if(event.keyCode==58)
		{
				event.returnValue = true;
		}		
	}
}	


function Desformatear(valor)
{
	if(valor!='')
	{
		valor=valor.replace('.','');
		valor=valor.replace('.','');
		return valor;
	}
	}


function FormatFecha(variable)
{
	if(variable!='')
	{
		var dia= variable.substring(0,2);	
		var mes= variable.substring(2,4);	
		var ano= variable.substring(4,8);	
		var todo=dia+'/'+mes+'/'+ano;
		return todo;
	}
	else
	{
		return '';	
	}
}
function FormatHora(variable)
{
	if(variable!='')
	{
		var hora= variable.substring(0,2);	
		alert(hora);
		return hora;
	//	var minuto= variable.substring(2,4);	
	//	var segundo= variable.substring(4,8);	
	//	var todo=hora+':'+minuto+':'+segundo;
//		return hora;
	}
	else
	{
		return '';	
	}
}

function FormatNumber(num,decimalNum,bolLeadingZero,bolParens,bolCommas)
/**********************************************************************
	Entrada:
		NUM - Numero a formatear
		decimalNum - Cantidad de decimales para el numero
		bolLeadingZero - true / false - Poner un 0 delante para los numeros entre -1 y 1
		bolParens - true / false - Usar parentisis para expresar los numeros negativos
		bolCommas - Poner comas como separador de miles.
 
	Salida:
		El numero formateado!
	
	HTML
	<input name="numero" type="text" onchange="this.value=FormatNumber(this.value,2,true,false,true);"/>
 **********************************************************************/
{ 
	// Cambiamos los separadores de miles y decimales por los correctos para el proceso
	num=num.replace(/\./g,'#');
	num=num.replace(/\,/g,'.');
	num=num.replace(/\#/g,'');
	
    if (isNaN(parseInt(num))) return "NaN";

	var tmpNum = num;
	var iSign = num < 0 ? -1 : 1;		// obtenemos el signo
	
	// Ajustar solamente en numero de numeros despues del punto decimal
	tmpNum *= Math.pow(10,decimalNum);
	tmpNum = Math.round(Math.abs(tmpNum))
	tmpNum /= Math.pow(10,decimalNum);
	tmpNum *= iSign;					// Reajustar el signo
	
	// Creamos un objeto string para formatiar nuestro numero
	var tmpNumStr = new String(tmpNum);

	// vemos si ponesmos un 0 para los numeros entre -1 y 1
	if (!bolLeadingZero && num < 1 && num > -1 && num != 0)
		if (num > 0)
			tmpNumStr = tmpNumStr.substring(1,tmpNumStr.length);
		else
			tmpNumStr = "-" + tmpNumStr.substring(2,tmpNumStr.length);
		
	// Ponemos las comas
	if (bolCommas && (num >= 1000 || num <= -1000)) {
		var iStart = tmpNumStr.indexOf(".");
		if (iStart < 0)
			iStart = tmpNumStr.length;

		iStart -= 3;
		while (iStart >= 1) {
			tmpNumStr = tmpNumStr.substring(0,iStart) + "," + tmpNumStr.substring(iStart,tmpNumStr.length)
			iStart -= 3;
		}		
	}

	// Ver si se nesecita usar parentesis para expersar los valores negativos
	if (bolParens && num < 0)
		tmpNumStr = "(" + tmpNumStr.substring(1,tmpNumStr.length) + ")";
	
	// Cambiamos los separadores de miles y decimales por los correctos para nosotros
	tmpNumStr=tmpNumStr.replace(/\,/g,'#');
	tmpNumStr=tmpNumStr.replace(/\./g,',');
	tmpNumStr=tmpNumStr.replace(/\#/g,'.');

	return tmpNumStr;
}

function NumToDB(num){
/**********************************************************************
	Entrada:
		NUM - Numero para la DB
	Salida:
		El numero listo para la DB
**********************************************************************/
	num=num.replace(/\./g,'#');
	num=num.replace(/\,/g,'.');
	num=num.replace(/\#/g,'');
	
	return num;
}

function checkDV( crut,obj )

{

  largo = crut.length;
  
  if ( largo >= 6 )
    rut = crut.substring(0, largo - 1);
  else
    rut = crut.charAt(0);
  dv = crut.charAt(largo-1);
  

 if ( rut == null || dv == null )
      return 0;

  var dvr = '0';

  suma = 0;
  mul  = 2;

  for (i= rut.length -1 ; i >= 0; i--)
  {
    suma = suma + rut.charAt(i) * mul;
    if (mul == 7)
      mul = 2;
    else    
      mul++;
  }


  res = suma % 11;
  if (res==1)
    dvr = 'k';
  else if (res==0)
    dvr = '0';
  else
  {
    dvi = 11-res;
    dvr = dvi + "";
  }

  if ( dvr != dv.toLowerCase() )
  {
    alert("EL rut es invalido"); // El rut es invalido
    obj.value = "";
    obj.focus();
    return false;
  }

      return true;
}


function checkRutField(texto,obj)
{

  var tmpstr = "";
  for ( i=0; i < texto.length ; i++ )
    if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' )
      tmpstr = tmpstr + texto.charAt(i);
      
  texto = tmpstr;
  largo = texto.length;


  if ( largo < 8 )
  {
    alert("Debe ingresar un rut valido");
	obj.value="";
	obj.focus();
    return false;
  }

  var invertido = "";

  for ( i=(largo-1),j=0; i>=0; i--,j++ )
    invertido = invertido + texto.charAt(i);


  var dtexto = "";

  dtexto = dtexto + invertido.charAt(0);
  dtexto = dtexto + '-';
  cnt = 0;

  for ( i=1,j=2; i<largo; i++,j++ )
  {    
    if ( cnt == 3 )
    {
      dtexto = dtexto + '.';
      j++;
      dtexto = dtexto + invertido.charAt(i);
      cnt = 1;
    }
    else
    { 
      dtexto = dtexto + invertido.charAt(i);
      cnt++;
    }
  }

  invertido = "";

  for ( i=(dtexto.length-1),j=0; i>=0; i--,j++ )
    invertido = invertido + dtexto.charAt(i);

  obj.value = invertido;
  if ( checkDV(texto,obj) )
    return true;
    
  // El rut es incorrecto
  return false;
}